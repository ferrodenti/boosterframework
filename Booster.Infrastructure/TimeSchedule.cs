using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Booster.Collections;
using Booster.DI;
using Booster.Interfaces;
using Booster.Log;
using Booster.Synchronization;
using JetBrains.Annotations;

namespace Booster;

[PublicAPI] 
public class TimeSchedule : BaseBackgroundExecutor
{
	[PublicAPI] 
	public sealed class Subscription : BaseStartStop
	{
		readonly TimeSchedule _owner;
		internal readonly WeakReference<Subscription> SelfRef;

		readonly WeakReference _target;

		[PublicAPI]
		public object Target => _target?.Target;

		DateTime? _next;
		[PublicAPI] public DateTime Next
		{
			get
			{
				if (!_next.HasValue)
				{
					if (IsWorking)
					{
						if (_interval > default(TimeSpan))
							_next = _prev + _interval;
						else if (_scheduleProvider != null)
							_next = _scheduleProvider.GetNext(_prev);
						else
							_next = DateTime.MinValue;
						//Console.WriteLine($"Next at {_next:ss\\:fff}");
					}
					else
						_next = DateTime.MinValue;
				}
				return _next.Value;
			}
			set
			{
				if (_next != value)
				{
					_next = value;
					_owner.Update(this);
				}
			}
		}

		DateTime _prev;
		TimeSpan _interval;
		[PublicAPI] public TimeSpan Interval
		{
			get => _interval;
			set
			{
				if (_interval != value)
				{
					_scheduleProvider = null;
					_interval = value;
					_next = null;
					_owner.Update(this);
				}
			}
		}

		IScheduleProvider _scheduleProvider;
		public IScheduleProvider ScheduleProvider
		{
			get => _scheduleProvider;
			set
			{
				if (!Equals(_scheduleProvider, value))
				{
					_scheduleProvider = value;
					_interval = default;
					_next = null;
					_owner.Update(this);
				}
			}
		}

		public event EventHandler CallbackEvent;
		public event ErrorEventHandler Error;

		Action _callback;
		Func<Task> _asyncCallback;

		Subscription(TimeSchedule owner, object target)
		{
			_owner = owner;
			_prev = DateTime.Now;
			_target = new WeakReference(target);
			SelfRef = new WeakReference<Subscription>(this);
		}

		Subscription(TimeSchedule owner, object target, Action callback) : this(owner, target) 
			=> _callback = callback;

		Subscription(TimeSchedule owner, object target, Func<Task> callback) : this(owner, target) 
			=> _asyncCallback = callback;

		public Subscription(TimeSchedule owner, object target, TimeSpan interval, Action callback)
			:this(owner, target, callback) 
			=> Interval = interval;

		public Subscription(TimeSchedule owner, object target, IScheduleProvider scheduleProvider, Action callback)
			:this(owner, target, callback) 
			=> ScheduleProvider = scheduleProvider;

		public Subscription(TimeSchedule owner, object target, TimeSpan interval, Func<Task> callback)
			:this(owner, target, callback) 
			=> Interval = interval;

		public Subscription(TimeSchedule owner, object target, IScheduleProvider scheduleProvider, Func<Task> callback)
			:this(owner, target, callback) 
			=> ScheduleProvider = scheduleProvider;

		public void SetCallback(Action callback) => _callback = callback;
		public void SetCallback(Func<Task> callback) => _asyncCallback = callback;

		public async Task DoWork()
		{
			if (!IsWorking)
				return;

			using (AsyncContext.Push())
			{
				_next = null;
				_prev = DateTime.Now;

				try
				{
					CallbackEvent?.Invoke(this, EventArgs.Empty);

					if (_asyncCallback != null)
						await _asyncCallback().NoCtx();
					else
						_callback?.Invoke();
				}
				catch (Exception ex)
				{
					//Console.WriteLine($"Exception: {ex.Message}");
					Error?.Invoke(this, new ErrorEventArgs(ex));
				}

				_owner.Update(this);
			}
		}

		protected override void OnStart() => UpdateNext();
		protected override void OnStop() => UpdateNext();

		void UpdateNext()
		{
			_next = null;
			_owner?.Update(this);
		}
	}

	readonly PriorityQueue<WeakReference<Subscription>, DateTime> _queue = new();
	//readonly ManualResetEventSlim _interruptDelay = new ManualResetEventSlim(false);
	readonly SemaphoreSlim _interruptDelay = new(0, 1);
	public TimeSpan MaxDelay { get; set; } = TimeSpan.MaxValue;
	DateTime _nextWake;

	public Subscription this[int index] => index >= 0 && index < _queue.Count && _queue[index].TryGetTarget(out var result) ? result : null;

	public Subscription Subscribe(object target)
	{
		var result = new Subscription(this, target, default(TimeSpan), null);
		Update(result);
		return result;
	}

	public Subscription Subscribe(object target, TimeSpan interval)
	{
		var result = new Subscription(this, target, interval, null);
		Update(result);
		return result;
	}

	public Subscription Subscribe(object target, Action callback)
	{
		var result = new Subscription(this, target, default(TimeSpan), callback);
		Update(result);
		return result;
	}

	public Subscription Subscribe(object target, TimeSpan interval, Action callback)
	{
		var result = new Subscription(this, target, interval, callback);
		Update(result);
		return result;
	}

	public Subscription Subscribe(object target, IScheduleProvider scheduleProvider, Action callback = null)
	{
		var result = new Subscription(this, target, scheduleProvider, callback);
		Update(result);
		return result;
	}
	public Subscription Subscribe(object target, Func<Task> callback)
	{
		var result = new Subscription(this, target, default(TimeSpan), callback);
		Update(result);
		return result;
	}
	public Subscription Subscribe(object target, TimeSpan interval, Func<Task> callback)
	{
		var result = new Subscription(this, target, interval, callback);
		Update(result);
		return result;
	}

	public Subscription Subscribe(object target, IScheduleProvider scheduleProvider, Func<Task> proc)
	{
		var result = new Subscription(this, target, scheduleProvider, proc);
		Update(result);
		return result;
	}

	protected void Update(Subscription subscription)
	{
		var next = subscription.Next;
		if (next.IsSignificant())
		{
			_queue.Change(subscription.SelfRef, next);

			if(EnsureTaskRunning() && next < _nextWake)
				_interruptDelay.ReleaseSafe();
		}
		else
			_queue.TryRemove(subscription.SelfRef);

		//Console.WriteLine($"Queue: {_queue.Count}");
	}

	protected override async Task<bool> ProccessOne()
	{
		var next = _queue.MinPriority;
		if (next.IsSignificant())
		{
			if (next > DateTime.Now)
			{
				_nextWake = next;
				if (MaxDelay != TimeSpan.MaxValue)
				{
					var dt = DateTime.Now + MaxDelay;
					if (dt < _nextWake)
						_nextWake = dt;
				}

				var toWait = _nextWake - DateTime.Now;
				if (toWait > TimeSpan.Zero)
					await _interruptDelay.WaitAsync(toWait).NoCtx();
			}
			else if(IsWorking)
			{
				var rf = _queue.FirstOrDefault();
				if (rf != null)
				{
					if (rf.TryGetTarget(out var target))
						await target.DoWork().NoCtx();
					else
						_queue.TryRemove(rf);
				}
			}
		}

		return !_queue.IsEmpty;
	}

	protected override bool CheckDone() => _queue.IsEmpty;
	protected override void OnStop() => _interruptDelay.ReleaseSafe();
	public void Clear() => _queue.Clear();

	static readonly Lazy<TimeSchedule> _gc = new(() => new TimeSchedule("GC Schedule"));
	public static TimeSchedule GC => _gc.Value;

	public TimeSchedule(string name = null) : this(InstanceFactory.Get<ILog>(false), name)
	{
	}

	public TimeSchedule(ILog log, string name = null) : base(log.Create(name ?? "Nameless time schedule"))
	{
	}
}