using System;
using System.Security.Cryptography;
using System.Text;

#pragma warning disable 612

namespace Booster.Obsolete;

[Obsolete]
public interface IPasswordValidation //TODO: remove
{
	string PasswordHash { get; set; }
	string PasswordSalt { get; set; }
}

// ReSharper disable once InconsistentNaming
public static class IPasswordValidationExpander
{
	public static bool ValidatePassword(this IPasswordValidation passwordValidation, string password)
	{
		if (!string.IsNullOrEmpty(password) &&
		    !string.IsNullOrEmpty(passwordValidation.PasswordSalt) &&
		    !string.IsNullOrEmpty(passwordValidation.PasswordHash))
		{
			var hash = HashPassword(password, Convert.FromBase64String(passwordValidation.PasswordSalt));
			var orig = Convert.FromBase64String(passwordValidation.PasswordHash);

			return hash.CompareConstantTime(orig) == 0;
		}
		return false;
	}

	public static void SetPassword(this IPasswordValidation passwordValidation, string password)
	{
		var salt = Rnd.Bytes(16);
		var hash = HashPassword(password, salt);

		passwordValidation.PasswordHash = Convert.ToBase64String(hash);
		passwordValidation.PasswordSalt = Convert.ToBase64String(salt);

		Array.Clear(salt, 0, salt.Length);
		Array.Clear(hash, 0, hash.Length);
	}

	static byte[] HashPassword(string password, byte[] salt)
	{
		var utf8 = Encoding.UTF8;
		var clearText = password.ToCharArray();       
		var data = new byte[salt.Length + utf8.GetMaxByteCount(clearText.Length)];
		byte[] hash;
            
		try
		{
			Array.Copy(salt, 0, data, 0, salt.Length);

			var byteCount = utf8.GetBytes(clearText, 0, clearText.Length, data, salt.Length);

			using var alg = SHA256.Create();
			hash = alg.ComputeHash(data, 0, salt.Length + byteCount);
		}
		finally
		{
			Array.Clear(clearText, 0, clearText.Length);
			Array.Clear(data, 0, data.Length);
		}

		return hash;
	}
}