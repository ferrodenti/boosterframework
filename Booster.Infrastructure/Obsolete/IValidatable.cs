using System;
using Booster.Interfaces;

namespace Booster.Obsolete;

[Obsolete("Use GridValidationAttribute")]
public interface IValidatable //TODO: remove
{
	void Validate(object changes, IValidationResponse response);
}