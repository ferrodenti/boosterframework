using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Booster.Interfaces;

namespace Booster.Obsolete;

[Obsolete("Use BaseLazyIndexedCollection")]
public abstract class BaseInlineList<TItem,TKey> : IConvertiableFrom<string>, IList<TItem> //TODO: unit tests
{
	readonly IEqualityComparer _itemComparer;
	readonly IEqualityComparer _keyComparer;

	protected readonly StringEscaper StringEscaper = new("\\", ";");

	readonly bool _unique;
	readonly object _lock = new();

	volatile string _value;
	public string Value
	{
		get
		{
			if (_value == null)
				lock (_lock)
					if (_value == null)
					{
						if (_keys != null)
							_value = Stringify(_keys);
						else if (_items != null)
							_value = Stringify(KeysInt); 
						else
							_value = "";
					}

			return _value;
		}
		set
		{
			lock (_lock)
				if (_value != value)
				{
					_value = int.TryParse(value, out _) ? $";{value};" : value;
					ResetItems();
					ResetKeys();
				}
		}
	}

	volatile List<TItem> _items;
	protected List<TItem> Items
	{
		get
		{
			if (_items == null)
				lock (_lock)
					if (_items == null)
					{
						var noItem = false;
						if (_keys != null)
							_items = LoadItems(_keys, out noItem);
						else if (_value != null)
							_items = LoadItems(KeysInt, out noItem);
						else
							_items = new List<TItem>();

						if (noItem)
							ResetValue();
					}

			return _items;
		}
	}

	volatile List<TKey> _keys;
	protected List<TKey> KeysInt
	{
		get
		{
			if (_keys == null)
				lock (_lock)
					if (_keys == null)
					{
						if (_items != null)
							_keys = _items.Select(GetItemKey).ToList();
						else if (_value != null)
							_keys = StringEscaper.Split(_value, ";", unescape: true).Select(k => StringConverter.Default.ToObject<TKey>(k)).ToList();
						else
							_keys = new List<TKey>();
					}

			return _keys;
		}
	}

	public TKey[] Keys
	{
		get => KeysInt.ToArray();
		set
		{
			_keys = value.ToList();
			ResetValue();
			ResetItems();
		}
	}

	public bool IsEmpty => Value.IsEmpty();

	protected BaseInlineList(bool unique = true, IEqualityComparer itemComparer = null, IEqualityComparer keyComparer = null)
	{
		_unique = unique;
		_itemComparer = itemComparer;
		_keyComparer = keyComparer;
	}

	protected abstract TItem LoadItem(TKey id);
	protected abstract TKey GetItemKey(TItem entity);

	protected virtual void ResetItems()				
		=> _items = null;

	protected virtual void ResetValue() 
		=> _value = null;

	protected virtual void ResetKeys() 
		=> _keys = null;

	protected virtual List<TItem> LoadItems(ICollection<TKey> keys, out bool noItem)
	{
		noItem = false;

		var items = new List<TItem>();

		foreach (var key in keys.ToArray())
		{
			var item = LoadItem(key);

			if (!Equals(item ,null))
				items.Add(item);
			else
			{
				noItem = true;
				keys.Remove(key);
			}
		}

		return items;
	}

	protected virtual string Stringify(ICollection<TKey> keys) 
		=> keys.Count > 0 ? $";{StringEscaper.Join(keys.Select(k => StringConverter.Default.ToString(k)), ";")};" : "";

	public override string ToString() 
		=> Items.ToString(new EnumBuilder(", "));

	#region IList implementation

	public virtual int Count => Items.Count;

	public virtual bool IsReadOnly => false;

	public virtual bool Contains(TItem item)
	{
		lock (_lock)
			return Items.Contains(item, _itemComparer);
	}
	public virtual bool ContainsKey(TKey key)
	{
		lock (_lock)
			return KeysInt.Contains(key, _keyComparer);
	}

	public virtual bool Remove(TItem item)
	{
		lock (_lock)
		{
			var res = false;

			if (_items != null)
				res = _items.Remove(item, _itemComparer);

			if (_keys != null || _items == null)
				res |= KeysInt.Remove(GetItemKey(item), _keyComparer);

			if(res)
				ResetValue();
					
			return res;
		}
	}

	public virtual bool RemoveKey(TKey key)
	{
		lock (_lock)
		{
			var res = false;

			if (_items != null)
			{
				var i = IndexOfKey(key);
				if (i >= 0)
				{
					_items.RemoveAt(i);
					res = true;
				}
			}

			if(_keys != null || _items == null)
				res |= KeysInt.Remove(key, _keyComparer);

			if(res)
				ResetValue();
					
			return res;
		}
	}

	public virtual void RemoveAt(int index)
	{
		Items.RemoveAt(index);
		ResetValue();
		ResetKeys();
	}

	public virtual void Clear()
	{
		lock (_lock)
		{
			_items.Do(o => o.Clear());
			_keys.Do(o => o.Clear());
			_value = "";
		}
	}

	public virtual void CopyTo(TItem[] array, int arrayIndex) 
		=> Items.CopyTo(array, arrayIndex);

	public virtual void Add(TItem item)
	{
		lock (_lock)
			if (!_unique || !Contains(item))
			{
				Items.Add(item);

				_keys?.Add(GetItemKey(item));

				ResetValue();
			}
	}

	public virtual void AddKey(TKey key)
	{
		lock (_lock)
			if (!_unique || !ContainsKey(key))
			{
				KeysInt.Add(key);

				_items?.Add(LoadItem(key));

				ResetValue();
			}
	}

	public virtual int IndexOf(TItem item)
	{
		lock (_lock)
			return Items.IndexOf(item, _itemComparer);
	}

	public virtual int IndexOfKey(TKey key)
	{
		var cmp = _keyComparer ?? EqualityComparer<TKey>.Default;

		lock (_lock)
			for (var i = 0; i < Items.Count; i++)
				if (cmp.Equals(GetItemKey(Items[i]), key))
					return i;

		return -1;
	}

	public virtual void Insert(int index, TItem item)
	{
		lock (_lock)
			if (!_unique || !Contains(item))
			{
				Items.Insert(index, item);
				ResetValue();
				ResetKeys();
			}
	}

	public virtual void InsertKey(int index, TKey key)
	{
		lock (_lock)
			if (!_unique || !ContainsKey(key))
				Insert(index, LoadItem(key));
	}


	public virtual TItem this[int index]
	{
		get => Items[index];
		set
		{
			lock (_lock)
				if (!Equals(Items[index], value) && (!_unique || !Contains(value)))
				{
					Items[index] = value;
					ResetValue();
					ResetKeys();
				}
		}
	}

	public virtual TKey[] GetKeys() 
		=> KeysInt.ToArray();

	public virtual IEnumerator<TItem> GetEnumerator() 
		=> Items.GetEnumerator();

	IEnumerator IEnumerable.GetEnumerator()
		=> GetEnumerator();

	#endregion
}