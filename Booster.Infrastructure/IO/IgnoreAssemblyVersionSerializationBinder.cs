﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using Booster.Collections;
using Booster.Helpers;
using Booster.Log;
using JetBrains.Annotations;

#nullable enable

namespace Booster.IO;

[PublicAPI]
public class IgnoreAssemblyVersionSerializationBinder : SerializationBinder
{
	protected class AssemblyCache
	{
		readonly ILog? _log;
		readonly Assembly _assembly;
		readonly bool _ignoreCase;
		readonly Dictionary<string, Type?> _typeCache;

		public AssemblyCache(Assembly assembly, bool ignoreCase, ILog? log)
		{
			_assembly = assembly;
			_ignoreCase = ignoreCase;
			_log = log;
			_typeCache = new Dictionary<string, Type?>(_ignoreCase ? StringComparer.OrdinalIgnoreCase : StringComparer.Ordinal);
		}

		public Type? GetType(string name)
			=> _typeCache.GetOrAdd(name, _ =>
			{
				var result = _assembly.GetType(name, false, _ignoreCase);
				if (result != null)
					return result;

				_log.Warn($"Type not found: {_assembly.GetName().Name}::{name}");
				return null;
			});
	}

	static readonly ListDictionary<StringComparer, Dictionary<string, AssemblyCache?>> _assemblyComparerCache = new();

	protected static Dictionary<string, AssemblyCache?> GetCache(StringComparer comparer)
		=> _assemblyComparerCache.GetOrAdd(comparer, cmp => new Dictionary<string, AssemblyCache?>(cmp));

	readonly ILog? _log;
	readonly bool _ignoreCase;
	readonly Dictionary<string, AssemblyCache?> _assemblyCache;

	public IgnoreAssemblyVersionSerializationBinder(ILog? log = null, bool ignoreCase = false)
	{
		_log = log.Create(this);
		_ignoreCase = ignoreCase;
		_assemblyCache = GetCache(_ignoreCase ? StringComparer.Ordinal : StringComparer.OrdinalIgnoreCase);
	}

	AssemblyCache? GetAssemblyCache(string name)
		=> _assemblyCache.GetOrAdd(name, _ =>
		{
			var shortName = name;
			var i = shortName.IndexOf(',');
			if (i > 0)
				shortName = shortName.Substring(0, i).Trim();

			var assy = AssemblyHelper.GetAssembly(shortName, _ignoreCase);
			if (assy != null)
				return new AssemblyCache(assy, _ignoreCase, _log);

			_log.Warn($"Assembly not found: {name}");
			return null;
		});

	public override Type? BindToType(string assemblyName, string typeName)
		=> GetAssemblyCache(assemblyName)?.GetType(typeName);
}