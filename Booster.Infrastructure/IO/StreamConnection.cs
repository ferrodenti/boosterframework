﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Booster.Synchronization;
using JetBrains.Annotations;

#nullable enable

namespace Booster.IO;

[PublicAPI]
public class StreamConnection : IDisposable
{
	readonly AsyncLock _sendLock = new();
	readonly CancellationTokenSource _cancellationTokenSource = new();

	readonly Stream _stream;
	protected CancellationToken Token;
	protected int MaxMessageSize = 1024 * 1024 * 100; /*100Mb*/
	public event EventHandler<DataEventArgs>? DataReceived; 
	public event EventHandler<ExceptionEventArgs>? Error;
	public event EventHandler? Disposed;

	public StreamConnection(Stream stream)
	{
		_stream = stream;
		Token = _cancellationTokenSource.Token;
	}

	public void StartReceive(bool async = true) 
		=>  Utils.ForgetTaskNoFlow(() => async ? ReceiveProcAsync() : ReceiveProc(), token: Token);

	async Task ReceiveProc()
	{
		try
		{
			while (!Token.IsCancellationRequested)
			{
				var data = await _stream.ReadPascal(MaxMessageSize, Token).NoCtx();
				await OnDataReceived(new DataEventArgs(data, Token)).NoCtx();
			}
		}
		catch (Exception e)
		{
			if (!Token.IsCancellationRequested)
				await OnError(new ExceptionEventArgs(e)).NoCtx();
		}
	}

	async Task ReceiveProcAsync()
	{
		try
		{
			var data = await _stream.ReadPascal(MaxMessageSize, Token).NoCtx();

			if (!Token.IsCancellationRequested)
			{
				Utils.ForgetTaskNoFlow(ReceiveProcAsync, token: Token);
				await OnDataReceived(new DataEventArgs(data, Token)).NoCtx();
			}
		}
		catch (Exception e)
		{
			if (!Token.IsCancellationRequested)
				await OnError(new ExceptionEventArgs(e)).NoCtx();
		}
	}

	public async Task Send(byte[] data, CancellationToken token = default)
	{
		token = token.Join(Token);

		using var _ = await _sendLock.LockAsync(token).NoCtx();
		await _stream.WritePascal(data, token).NoCtx();
	}

	public virtual void Dispose()
	{
		_cancellationTokenSource.Cancel();
		Disposed?.Invoke(this, EventArgs.Empty);
	}

	protected virtual Task OnDataReceived(DataEventArgs e)  //TODO: async
	{
		DataReceived?.Invoke(this, e);
		return Task.CompletedTask;
	}

	protected virtual Task OnError(ExceptionEventArgs e)
	{
		Error?.Invoke(this, e);
		return Task.CompletedTask;
	}
}