﻿using System;
using System.Threading;

namespace Booster.IO;

public class DataEventArgs : EventArgs
{
	public byte[] Data;
	public CancellationToken Token;

	public DataEventArgs(byte[] data, CancellationToken token)
	{
		Data = data;
		Token = token;
	}
}