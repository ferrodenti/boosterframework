using System;
using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace Booster.Debug;

public class ReadOnlyCollectionDebugView<T>
{
	readonly IReadOnlyCollection<T> _collection;

	public ReadOnlyCollectionDebugView(IReadOnlyCollection<T> collection)
		=> _collection = collection ?? throw new ArgumentNullException(nameof(collection));

	[PublicAPI]
	[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
	public T[] Items
	{
		get 
		{
			var result = new T[_collection.Count];
			var cnt = 0;
			foreach (var item in _collection)
				result[cnt++] = item;
				
			return result;
		}
	}
}