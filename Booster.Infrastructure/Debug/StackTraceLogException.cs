using System;
using System.Linq;
using JetBrains.Annotations;

namespace Booster.Debug;

public class StackTraceLogException : Exception
{
	readonly StackTraceLog _log;
	readonly StackTraceLog _commandLog;

	[PublicAPI] 
	public StackTraceLogRecord[] History => _history ??= _log.Reverse().ToArray();
	StackTraceLogRecord[] _history;

	public StackTraceLogException(StackTraceLog log, string message) : base(message)
	{
		_log = log;
		SafeBreak.Break();
	}

	public StackTraceLogException(StackTraceLog log, StackTraceLog commandLog, string message) : base(message)
	{
		_log = log;
		_commandLog = commandLog;
		SafeBreak.Break();
	}
		
	public string ToStringExtended()
	{
		var bld = new EnumBuilder("\n");
		bld.Append($"=============================== {base.ToString()}");

		if (_commandLog != null)
		{
			bld.Append("======================== COMMAND HISTORY:");
			foreach (var rec in _commandLog.Reverse().ToArray())
			{
				bld.Append($"================== {rec.ToString().ToUpper()}");
				bld.Append(rec.StackTrace);
			}
			bld.Append("\n");
		}
		bld.Append("======================== CONNECTION HISTORY:");
		foreach (var rec in History)
		{
			bld.Append($"================== {rec.ToString().ToUpper()}");
			bld.Append(rec.StackTrace);
		}
		bld.Append("\n");
		return bld;
	}
}