using System;
using System.Collections;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace Booster.Debug;

public class DictionaryDebugView
{
	readonly IDictionary _dictionary;

	public DictionaryDebugView(IDictionary dictionary)
		=> _dictionary = dictionary ?? throw new ArgumentNullException(nameof(dictionary));

	[PublicAPI]
	[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
	public KeyValuePairDebugView[] Keys =>
		_dictionary.Keys
			.Cast<object>()
			.Select(key => new KeyValuePairDebugView(key, _dictionary[key]))
			.ToArray();
}
