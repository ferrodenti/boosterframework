using System;
using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace Booster.Debug;

public class CollectionDebugView<T>
{
	readonly ICollection<T> _collection;

	public CollectionDebugView(ICollection<T> collection)
		=> _collection = collection ?? throw new ArgumentNullException(nameof(collection));

	[PublicAPI]
	[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
	public T[] Items
	{
		get 
		{
			var array = new T[_collection.Count];
			_collection.CopyTo(array, 0);
			return array;
		}
	}
}