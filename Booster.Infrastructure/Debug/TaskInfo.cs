using System;
using Booster.DI;

namespace Booster.Debug;

public static class TaskInfo
{
	static readonly CtxLocal<string> _taskName = new();
	static volatile int _cnt;
		
	public static string TaskName
	{
		get => _taskName.Value ?? ( _taskName.Value = $"Task {++_cnt}");
		set => _taskName.Value = value;
	}

	public static void Init(string name = null, bool check = false)
	{
		if (check && _taskName.Value != null)
		{
			SafeBreak.Break();
			throw new Exception($"A newly initialized task already has a name {_taskName.Value}");
		}

		if (name.IsSome())
			TaskName = name + ++_cnt;
		else
			Utils.Nop(TaskName);
	}
}