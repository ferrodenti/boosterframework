using System.Diagnostics;

namespace Booster.Debug;

[DebuggerDisplay("{Value}", Name = "{Key}")]
public class KeyValuePairDebugView
{
	public KeyValuePairDebugView(object key, object value)
	{
		Value = value;
		Key = key;
	}

	public object Key { get; set; }
	public object Value { get; set; }
}
