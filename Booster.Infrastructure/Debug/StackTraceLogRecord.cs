using System;
using System.Diagnostics;
using JetBrains.Annotations;

namespace Booster.Debug;

[PublicAPI] 
public class StackTraceLogRecord
{
	public readonly string Message;
	public readonly DateTime DateTime;
	readonly string _rawStackTrace;

	string _stackTrace;

	public string StackTrace
	{
		get
		{
			if (_stackTrace == null)
			{
				var res = new EnumBuilder("\n");
				foreach (var line in _rawStackTrace.SplitNonEmpty("\n"))
                    if(line.Contains(":line "))
                        res.Append(line);
                _stackTrace = res;
			}

			return _stackTrace;
		}
	}
	public readonly string TaskName;
	public readonly string Id;

	public StackTraceLogRecord(string id, string message, int skipFrames = 2)
	{
		Id = id;
		Message = message;
		_rawStackTrace = skipFrames == int.MaxValue ? "" : new StackTrace(skipFrames, true).ToString();
		DateTime = DateTime.Now;
		TaskName = TaskInfo.TaskName;
	}

	public override string ToString()
		=> $"{DateTime:HH:mm:ss}|{TaskName}|Conn{Id}|{Message}";
}