using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace Booster.Debug;

[Serializable]
[DebuggerTypeProxy(typeof(ReadOnlyCollectionDebugView<StackTraceLogRecord>))] 
[DebuggerDisplay($"ID = {{{nameof(_id)}}}, COUNT = {{{nameof(Count)}}}, LAST = {{{nameof(Last)}}}")]
public class StackTraceLog : IReadOnlyCollection<StackTraceLogRecord>
{
	readonly string _id;
	readonly int _skipFrames;
	readonly int _maxRecords;
		
	readonly Queue<StackTraceLogRecord> _history = new();

	[UsedImplicitly]
	protected string Last => _history.LastOrDefault()?.ToString();

	public StackTraceLog(object id = null, int maxRecords = 30, int skipFrames = 3)
	{
		_skipFrames = skipFrames;
		_maxRecords = maxRecords;
		_id = id?.ToString();
	}

	public void Log(string message, bool noStackTrace = false)
	{
		_history.Enqueue(new StackTraceLogRecord(_id, message, noStackTrace ? int.MaxValue : _skipFrames));
		while (_history.Count > _maxRecords)
			_history.Dequeue();
	}

	public IEnumerator<StackTraceLogRecord> GetEnumerator()
		=> _history.GetEnumerator();

	IEnumerator IEnumerable.GetEnumerator()
		=> GetEnumerator();	

	public int Count => _history.Count;
	public StackTraceLogRecord this[int index] => _history.ToArray()[index];
}