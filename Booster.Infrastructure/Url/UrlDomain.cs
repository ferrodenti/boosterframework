using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Booster.Parsing;
using JetBrains.Annotations;
using static Booster.FastLazyStatic;

#nullable enable

namespace Booster;

public class UrlDomain : BaseUrlHost, IEnumerable<string?>
{
	internal static readonly Regex Parser = Utils.CompileRegex(@"
^
(
	(
		((?<d3>[^:/\?#\.]+)\.)?
		((?<d2>[^:/\?#\.]+)\.)
		(?<d1>\b(aero|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|xxx|works|
ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|
ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cs|cu|cv|cx|cy|cz|dd|de|dj|dk|dm|do|dz|ec|ee|eg|er|es|et|eu|fi|fj|fk|fm|
fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|
je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|mn|mo|
mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|
py|qa|re|ro|rs|ru|рф|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|
tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|za|zm|zw)\b)
	)|(?<d1>\blocalhost\b)
)
");
	readonly Url? _owner;

	FastLazy<string?[]>? _domains;
	public string?[] Domains
	{
		get => _domains ??= FastLazy(() =>
		{
			if (_value != null)
			{
				var m = Parser.Match(_value);
				if (!m.Success)
					throw new ParsingException($"\"{_value}\" seems not a valid domain");

				return FromStrings(m.GetString("d3"), m.GetString("d2"), m.GetString("d1"));
			}

			return Array.Empty<string>();
		});
		set
		{
			if (_domains?.Value != value)
			{
				_domains = FromStrings(
					value.Length > 0 ? value[0] : null,
					value.Length > 1 ? value[1] : null,
					value.Length > 2 ? value[2] : null);

				_value = null;
				_owner?.Changed();
			}
		}
	}


	string? _value;

	public override string? Value
	{
		get => _value ??= _domains?.Value.Where(d => d != null).ToString(new EnumBuilder("."));
		set
		{
			if (_value == value)
			{
				_value = value;
				_domains = null;
				_owner?.Changed();
			}
		}
	}

	protected UrlDomain(Url? owner)
		=> _owner = owner;

	[PublicAPI]
	public UrlDomain(string value, Url? owner)
	{
		_owner = owner;
		_value = value;
	}
		
	public UrlDomain(string? d3, string? d2, string? d1, Url? owner)
	{
		_owner = owner;
		_domains = FromStrings(d3, d2, d1);
	}
		
		
	public override void Refresh()
	{
		Utils.Nop((object) Domains);
		_value = null;
	}

	public override void Reparse()
		=> _domains = null;

	protected static string?[] FromStrings(string? d3, string? d2, string? d1)
	{
		var list = new List<string?>(3);
		if (d3.IsSome()) list.Add(d3);
		if (d2.IsSome()) list.Add(d2);
		if (d1.IsSome()) list.Add(d1);

		if (list.Count < 3) list.Insert(0, null);
		if (list.Count < 3) list.Insert(2, null);

		return list.ToArray();
	}

	public override string? ToString()
		=> Value;

	public override int GetHashCode()
		=> Value.With(s => s.ToLower().GetHashCode());

	public override BaseUrlHost Clone(Url owner)
		=> new UrlDomain(owner) {_value = _value, _domains = _domains.With(s => FastLazy(s.Value.ToArray()))};

	public override bool  Equals(object obj)
		=> obj is UrlIp4 b && Value.EqualsIgnoreCase(b.Value);

	public IEnumerator<string?> GetEnumerator()
		=> Domains.Where( d => !string.IsNullOrWhiteSpace(d)).GetEnumerator();

	IEnumerator IEnumerable.GetEnumerator()
		=> GetEnumerator();

	public string? this[int index]
	{
		get => Domains[index];
		set
		{
			Domains[index] = value;
			_value = null;
			_owner?.Changed();
		}
	}

	public static implicit operator UrlDomain(string url)
		=> new(url);

	public static implicit operator string?(UrlDomain urlDomain)
		=> urlDomain.With(u => u.ToString());
}