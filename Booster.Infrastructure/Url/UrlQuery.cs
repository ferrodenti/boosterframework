using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Booster.Interfaces;
using JetBrains.Annotations;
using static Booster.FastLazyStatic;

#nullable enable

namespace Booster;

public class UrlQuery : IDictionary<string, string?>, IStringValue, IConvertiableFrom<string>
{
	readonly Url? _owner;

	FastLazy<Dictionary<string, string?>>? _inner;
	protected Dictionary<string, string?> Inner => _inner ??= FastLazy(() =>
	{
		var res = new Dictionary<string, string?>(StringComparer.OrdinalIgnoreCase);
		var str = _value;
		if (!string.IsNullOrWhiteSpace(str))
		{
			if (str.StartsWith('?'))
				str = str.Substring(1);

			foreach (var v in str.SplitNonEmpty('&'))
			{
				var i = v.IndexOf('=');
				if (i >= 0)
					res[Url.Decode(v.Substring(0, i).Trim())] = Url.Decode(v.Substring(i + 1));
				else
					res[Url.Decode(v)] = null;
			}
		}

		return res;
	});

	string? _value;
	public string Value
	{
		get
		{
			if (_value == null)
			{
				var res = new EnumBuilder("&");

				foreach (var pair in Inner)
					res.Append(pair.Value == null
						? Url.Encode(pair.Key)
						: $"{Url.Encode(pair.Key)}={Url.Encode(pair.Value)}");

				_value = res;
			}

			return _value;
		}
		set
		{
			if (_value != value)
			{
				_value = value;
				_inner = null;
				_owner?.Changed();
			}
		}
	}

	public bool IsEmpty => ToString().IsEmpty();

	public void Refresh()
	{
		Utils.Nop(Inner);
		_value = null;
	}

	[PublicAPI]
	public void Reparse() 
		=> _inner = null;

	protected UrlQuery(Url? owner)
		=> _owner = owner;

	public UrlQuery(string? str, Url? owner)
	{
		_owner = owner;
		_value = str;
	}

	public UrlQuery Clone(Url? owner) => new(owner)
	{
		_value =  _value,
		_inner = _inner.With(i => FastLazy(new Dictionary<string, string?>(i.Value, StringComparer.OrdinalIgnoreCase)))
	};

	[PublicAPI]
	public NameValueCollection ToNameValueCollection()
	{
		var res = new NameValueCollection();
		foreach (var pair in this)
			res[pair.Key] = pair.Value;

		return res;
	}

	public override string ToString() 
		=> Value;

	public override bool Equals(object obj) 
		=> obj is UrlQuery q && q.Value == Value;

	public override int GetHashCode() 
		=> Value.GetHashCode();

	public string this[int index] 
		=> Keys.ToList()[index];

	public static implicit operator UrlQuery(string query) 
		=> new(query, null);

	public static implicit operator string(UrlQuery query) 
		=> query.ToString();

	public string? this[string key] 
	{
		get => Inner.SafeGet(key);
		set
		{
			Inner[key] = value;
			_owner?.Changed();
			_value = null;
		}
	}

	public int Count 
		=> Inner.Count;
		
	public bool IsReadOnly 
		=> false;
		
	public ICollection<string> Keys 
		=> Inner.Keys;
		
	public ICollection<string?> Values 
		=> Inner.Values;

	public IEnumerator<KeyValuePair<string, string?>> GetEnumerator()
		=> Inner.GetEnumerator();
		
	IEnumerator IEnumerable.GetEnumerator()
		=> GetEnumerator();

	void ICollection<KeyValuePair<string, string?>>.Add(KeyValuePair<string, string?> item)
	{
		((IDictionary<string, string?>)Inner).Add(item); 
		_owner?.Changed();
		_value = null;
	}
	bool ICollection<KeyValuePair<string, string?>>.Contains(KeyValuePair<string, string?> item)
		=> ((IDictionary<string, string?>)Inner).Contains(item);
		
	void ICollection<KeyValuePair<string, string?>>.CopyTo(KeyValuePair<string, string?>[] array, int arrayIndex)
		=> ((IDictionary<string, string?>) Inner).CopyTo(array, arrayIndex);

	bool ICollection<KeyValuePair<string, string?>>.Remove(KeyValuePair<string, string?> item)
	{
		if (((IDictionary<string, string?>) Inner).Remove(item))
		{
			_owner?.Changed();
			_value = null;
			return true;
		}
		return false;
	}

	public void Clear()
	{
		Inner.Clear();
		_owner?.Changed();
		_value = null;
	}
		
	public bool ContainsKey(string key)
		=> Inner.ContainsKey(key);

	public void Add(string key, string? value)
	{
		Inner.Add(key, value);
		_owner?.Changed();
		_value = null;
	}

	public bool Remove(string key)
	{
		if (Inner.Remove(key))
		{
			_owner?.Changed();
			_value = null;
			return true;
		}
		return false;
	}
		
	public bool TryGetValue(string key, out string value)
		=> Inner.TryGetValue(key, out value!);
}