using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Booster.Interfaces;
using JetBrains.Annotations;

#nullable enable

namespace Booster;

public class UrlPath : IList<string>, IStringValue, IConvertiableFrom<string>
{
	readonly Url? _owner;

	List<string>? _inner;
	protected List<string> Inner
	{
		get
		{
			if (_inner == null)
			{
				var res = new List<string>();
				var str = _value;
				if (!string.IsNullOrWhiteSpace(str))
				{
					str = str.Replace('\\', '/');

					if (str.EndsWith('/'))
						str = str.Substring(0, str.Length - 1); //TODO: refact here
					else
						_endsWithFile = true;

					foreach (var v in str.Split(new[] {'/'}, StringSplitOptions.None))
						res.Add(Url.Decode(v));
				}
				_inner = res;
			}
			return _inner;
		}
	}

	string? _value;
	public string Value
	{
		get
		{
			if (_value == null)
			{
				var res = new EnumBuilder("/");
				foreach (var str in Inner)
					res.Append(Url.Encode(str));

				if (!_endsWithFile)
					res.Append("");

				_value = res;
			}
			return _value;
		}
		set
		{
			if (_value != value)
			{
				_value = value;
				_inner = null;
			}
		}
	}

	public bool IsEmpty => ToString().IsEmpty();

	bool _endsWithFile;
	public bool EndsWithFile
	{
		get => Inner.Count > 0 && _endsWithFile;
		set
		{
			_endsWithFile = value;
			_owner?.Changed();
			_value = null;
		}
	}

	public string? File
	{
		get => EndsWithFile ? Inner.LastOrDefault() : null;
		set
		{
			if (!string.IsNullOrWhiteSpace(value))
			{
				if (File != null)
					Inner[Inner.Count - 1] = value;
				else
				{
					Inner.Add(value);
					_endsWithFile = true;
				}
			}
			else
			{
				if (File != null)
				{
					Inner.RemoveAt(Inner.Count - 1);
					_endsWithFile = false;
				}
				else
					return;
			}
			_owner?.Changed();
		}
	}

	public void Refresh()
	{
		Utils.Nop(Inner);
		_value = null;
	}

	[PublicAPI]
	public void Reparse() 
		=> _inner = null;

	protected UrlPath(Url owner) 
		=> _owner = owner;

	public UrlPath(string str, Url? owner)
	{
		_owner = owner;
		_value = str;
	}

	public UrlPath Clone(Url owner)
		=> new(owner)
		{
			_inner = _inner.With(i => new List<string>(i)), 
			_value = _value, 
			_endsWithFile = _endsWithFile
		};

	public override bool Equals(object obj) 
		=> obj is UrlPath q && string.Equals(q.Value, Value, StringComparison.OrdinalIgnoreCase);

	public override int GetHashCode() 
		=> Value.ToLower().GetHashCode();

	public override string ToString() 
		=> Value;

	public static implicit operator UrlPath(string path) 
		=> new(path, null);

	public static implicit operator string(UrlPath path) 
		=> path.ToString();

	public int Count => Inner.Count;
	public bool IsReadOnly => false;

	public string this[int index]
	{
		get => Inner[index];
		set
		{
			if (!string.IsNullOrWhiteSpace(value))
				Inner[index] = value;
			else
				RemoveAt(index);

			_owner?.Changed();
			_value = null;
		}
	}

	public void Add(UrlPath path)
	{
		Inner.AddRange(path.Inner);
		_endsWithFile = path._endsWithFile;
		_owner?.Changed();
		_value = null;
	}

	public void Add(string item)
	{
		if (!string.IsNullOrWhiteSpace(item))
		{
			var path = new UrlPath(item);
			Add(path);
			_owner?.Changed();
			_value = null;
		}
	}

	public void Insert(int index, string item)
	{
		if (!string.IsNullOrWhiteSpace(item))
		{
			Inner.Insert(index, item);
			_owner?.Changed();
			_value = null;
		}
	}

	public void Clear()
	{
		Inner.Clear();
		_owner?.Changed();
		_value = null;
	}

	public bool Contains(string item) 
		=> Inner.Contains(item);
		
	public void CopyTo(string[] array, int arrayIndex) 
		=> Inner.CopyTo(array, arrayIndex);

	public bool Remove(string item)
	{
		if (Inner.Remove(item))
		{
			_owner?.Changed();
			_value = null;
			return true;
		}
		return false;
	}
		
	public int IndexOf(string item) 
		=> Inner.IndexOf(item);

	public void RemoveAt(int index)
	{
		Inner.RemoveAt(index);
		_owner?.Changed();
		_value = null;
	}

	public IEnumerator<string> GetEnumerator() 
		=> Inner.GetEnumerator();
		
	IEnumerator IEnumerable.GetEnumerator() 
		=> GetEnumerator();
}