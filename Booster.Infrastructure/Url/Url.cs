﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Booster.Interfaces;
using Booster.Parsing;
using JetBrains.Annotations;

using static Booster.FastLazyStatic;

namespace Booster;

#nullable enable

// https://tools.ietf.org/html/rfc3986
[PublicAPI] 
public class Url : IStringValue, IConvertiableFrom<string>
{
	static readonly Regex _parser = Utils.CompileRegex($@"
^((?<protocol>[^:/\?#]+)://)?
(
	(
		(?<login>[^:@/\?#]+):
		(?<pass>[^@/\?#]+)@
	)?(({UrlIp4.Parser.ToString().Substring(1)})|({UrlDomain.Parser.ToString().Substring(1)}))
	(:(?<port>\d{{1,5}}))?
)?
(?<path>[^\?#]*)?
(\?(?<query>[^#]*))?
(#(?<hash>.*))?
$"); //TODO: this is slow at first run!!!

	FastLazy<BaseUrlHost?>? _host; 
	public BaseUrlHost? Host
	{
		get => _host ??= FastLazy<BaseUrlHost?>(() =>
		{
			if (Match != null)
			{

				if (!string.IsNullOrWhiteSpace(Match.GetString("ip1")))
					return new UrlIp4(Match.GetInt("ip1"), Match.GetInt("ip2"), Match.GetInt("ip3"), Match.GetInt("ip4"), this);

				if (!string.IsNullOrWhiteSpace(Match.GetString("d1")))
					return new UrlDomain(Match.GetString("d3"), Match.GetString("d2"), Match.GetString("d1"), this);
			}

			return null;
		});
		set
		{
			_host = value?.Clone(this);
			Changed();
		}
	}

	FastLazy<string?>? _login;
	public string? Login
	{
		get => _login ??= Host.With(_ => Match?.GetString("login"));
		set
		{
			_login = value;
			Changed();
		}
	}

	FastLazy<string?>? _password;
	public string? Password
	{
		get => _password ??= Host.With(_ => Match?.GetString("pass"));
		set
		{
			_password = value;
			Changed();
		}
	}

	FastLazy<string?>? _protocol;
	public string? Protocol
	{
		get => _protocol ??= Host.With(_ => Match?.GetString("protocol"));
		set
		{
			_protocol = value;
			Changed();
		}
	}

	FastLazy<int?>? _port;
	public int? Port
	{
		get => _port ??= FastLazy<int?>(() =>
		{
			var str = Match?.GetString("port");
			if (str.IsSome() && int.TryParse(str, out var result))
				return result;

			return null;
		});
		set
		{
			_port = value;
			Changed();
		}
	}

	FastLazy<ObservableCollection<UrlCommand>>? _commands;
	public ObservableCollection<UrlCommand> Commands
	{
		[NotNull]
		get
		{
			if (_commands == null)
				ParsePath();
				
			return _commands!.Value;
		}
		set
		{
			if (_commands != null)
			{
				if (_commands.Value == value)
					return;

				_commands.Value.CollectionChanged -= Changed;
			}

			_commands = new ObservableCollection<UrlCommand>(value.With(v => v, Enumerable.Empty<UrlCommand>()));
			_commands.Value.CollectionChanged += Changed;
			Changed();
		}
	}

	FastLazy<UrlPath>? _path;
	public UrlPath Path
	{
		get
		{
			if (_path == null)
				ParsePath();
				
			// ReSharper disable once AssignNullToNotNullAttribute
			return _path!.Value;
		}
		set
		{
			_path = value.With(v => v.Clone(this)) ?? new UrlPath("", this);
			Changed();
		}
	}

	protected void ParsePath()
	{
		var commands = new ObservableCollection<UrlCommand>();
		var path = Match?.GetString("path").Replace('\\', '/') ?? "";

		if (Host != null)
		{
			if (path.StartsWith('/'))
				path = path.Substring(1);
		}
		else
		{
			if (path.StartsWith('/'))
			{
				commands.Add(UrlCommand.SiteRoot);
				path = path.Substring(1);
			}
			else if (path.StartsWith("~/"))
			{
				commands.Add(UrlCommand.AppRoot);
				path = path.Substring(2);
			}
			else
				while (path.StartsWith("../"))
				{
					commands.Add(UrlCommand.Up);
					path = path.Substring(3);
				}
		}

		commands.CollectionChanged += Changed;

		_path = new UrlPath(path, this);
		_commands = new ObservableCollection<UrlCommand>(commands);
	}
	
	UrlQuery? _query;
	public UrlQuery Query
	{
		get => _query ??= new UrlQuery(Match?.GetString("query"), this);
		set
		{
			_query = value.With(v => v.Clone(this)) ?? new UrlQuery(null, this);
			Changed();
		}
	}

	FastLazy<string?>? _hash;
	public string? Hash
	{
		get => _hash ??= Match?.GetString("hash");
		set
		{
			_hash = value;
			Changed();
		}
	}

	public bool IsAbsolute => Host != null;
	public bool IsEmpty => Value.IsEmpty();
	public bool IsSome => Value.IsSome();

	Match? _match;
	protected Match? Match
	{
		get
		{
			if (_match == null && _value != null)
			{
				Parse(_value);
					
				_match = _parser.Match(_value);
				if (!_match.Success)
					throw new ParsingException($"\"{Value}\" seems not a valid url");
			}
			return _match;
		}
	}

	public void Parse(string value)
	{
		var parser = new StringParser(value);
		var schemePtr = -1;
		var queryPtr = -1;
		var fragmentPtr = -1;
			
		parser.SearchFor(new Term("://", s =>
			{
				if (schemePtr < 0) 
					schemePtr = s.Position;
			}),
			new Term("?", s =>
			{
				if (queryPtr < 0)
					queryPtr = s.Position;
			}),
			new Term("#", s =>
			{
				fragmentPtr = s.Position;
			}));

		var rest = value;
		string? scheme = null, query = null, fragment = null;
			
		if (fragmentPtr >= 0)
		{
			fragment = rest.Substring(fragmentPtr);
			rest = rest.Substring(0, fragmentPtr);
		}

		if (queryPtr >= 0)
		{
			query = rest.Substring(queryPtr);
			rest = rest.Substring(0, queryPtr - 1);
		}

		if (schemePtr >= 0)
		{
			scheme = rest.Substring(0, schemePtr - "://".Length);
			rest = rest.Substring(schemePtr);
		}

		var segments = rest.SplitNonEmpty('/', '\'');
		if (segments.Length > 0)
		{
				
		}

		Utils.Nop(scheme, query, fragment);
		//TODO: может, все-таки написать тут универсальный парсер?
	}


	public static bool TryParse(string source, out Url result)
	{
		try
		{
			result = new Url(source);
			return true;
		}
		catch
		{
			result = null!;
			return false;
		}
	}

	public Url()
	{
	}

	public Url(string url) 
		=> Value = url;

	internal void Changed(object? sender = null, EventArgs? e = null)
	{
		if (_value != null)
			Stringify(); // ensure parsed

		_value = null;
		_match = null;
	}

	public void Refresh()
	{
		_host?.Value?.Refresh();
		_path?.Value.Refresh();
		_query?.Refresh();

		_value = null;
		_match = null;
	}

	public void Reparse()
	{
		if (_value != null)
		{
			_match = null;
			_commands = null;
			_hash = null;
			_host = null;
			_login = null;
			_password = null;
			_path = null;
			_port = null;
			_protocol = null;
			_query = null;
		}
	}

	public Url Clone()
	{
		var res = new Url {_value = _value, _match = _match};

		if (_commands != null)
		{
			var cmd = new ObservableCollection<UrlCommand>(_commands.Value);
			cmd.CollectionChanged += res.Changed;
			res._commands = cmd;
		}
		if (_hash != null) res._hash = _hash.Value;
		if (_host != null) res._host = _host.Value?.Clone(res);
		if (_login != null) res._login = _login.Value;
		if (_password != null) res._password = _password.Value;
		if (_path != null) res._path = _path.Value.Clone(res);
		if (_port != null) res._port = _port.Value;
		if (_protocol != null) res._protocol = _protocol.Value;
		if (_query != null) res._query = _query?.Clone(res);
		if (_login != null) res._login = _login.Value;

		return res;
	}


	string Stringify()
	{
		var res = new StringBuilder();

		if (!string.IsNullOrWhiteSpace(Protocol)) res.AppendFormat("{0}://", Protocol);

		if (Host != null)
		{
			if (!string.IsNullOrWhiteSpace(Login))
			{
				res.Append(Encode(Login));
				if (!string.IsNullOrWhiteSpace(Password))
				{
					res.Append(':');
					res.Append(Encode(Password));
				}
				res.Append('@');
			}
			res.Append(Host);

			if (Port.HasValue)
			{
				res.Append(':');
				res.Append(Port.Value);
			}

			if (Path.Count > 0)
				res.Append("/");
		}
		else
			foreach (var cmd in Commands)
				switch (cmd)
				{
				case UrlCommand.Up:
					res.Append("../");
					break;
				case UrlCommand.AppRoot:
					res.Append("~/");
					break;
				case UrlCommand.SiteRoot:
					res.Append("/");
					break;
				}

		if (Path.Count > 0)
			res.Append(Path);

		var needSlash = Path.Count == 0 && res.Length > 0 && res[res.Length - 1] != '/';

		if (Query.Count > 0)
		{
			if (needSlash)
				res.Append('/');

			res.Append('?');
			res.Append(Query);
			needSlash = false;
		}

		if (!string.IsNullOrWhiteSpace(Hash))
		{
			if (needSlash)
				res.Append('/');

			res.Append('#');
			res.Append(Encode(Hash));
		}
		return res.ToString();
	}

	public Url PathQueryAndHash()
	{
		var res = Clone();
		res.Host = null;
		res.Login = null;
		res.Password = null;
		res.Protocol = null;
		res.Port = 0;
		return res;

		//return new Url //TODO: так почему-то не работает
		//{
		//	Path = Path,
		//	Query = Query,
		//	Hash = Hash
		//};
	}

	public Url Combine(Url right)
	{
		var res = new Url();
		var useRight = false;

		if (!string.IsNullOrWhiteSpace(right.Protocol))
		{
			res.Protocol = right.Protocol;
			useRight = true;
		}
		if (!useRight && !string.IsNullOrWhiteSpace(Protocol))
			res.Protocol = Protocol;

		if (!string.IsNullOrWhiteSpace(right.Login) || !string.IsNullOrWhiteSpace(right.Password))
		{
			res.Login = right.Login;
			res.Password = right.Password;
			useRight = true;
		}
		if (!useRight && (!string.IsNullOrWhiteSpace(Login) || !string.IsNullOrWhiteSpace(Password)))
		{
			res.Login = Login;
			res.Password = Password;
		}
		if (right.Host != null)
		{
			res.Host = right.Host.Clone(res);
			useRight = true;
		}
		if (!useRight && Host != null)
			res.Host = Host.Clone(res);

		if (right.Port > 0)
		{
			res.Port = right.Port;
			useRight = true;
		}
		if (!useRight && Port > 0)
			res.Port = Port;

		if (useRight)
		{
			res.Commands = new ObservableCollection<UrlCommand>(right.Commands);
			res.Path = right.Path.Clone(res);
		}
		else
		{
			res.Path = Path.Clone(res);
			res.Commands = new ObservableCollection<UrlCommand>(Commands);
			var comms2 = right.Commands.ToList();

			while (res.Path.Count > 0 && comms2.Count > 0)
			{
				useRight = true;
				if (!string.IsNullOrWhiteSpace(res.Path.File))
				{
					res.Path.File = null;
					continue;
				}
				switch (comms2[0])
				{
				case UrlCommand.AppRoot:
				case UrlCommand.SiteRoot:
					res.Path.Clear();
					res.Commands.Clear();
					res.Commands.Add(comms2[0]);
					comms2.RemoveAt(0);
					break;

				case UrlCommand.Up:
					res.Path.RemoveAt(res.Path.Count - 1);
					comms2.RemoveAt(0);
					break;
				}
			}
			res.Commands.AddRange(comms2);
			res.Path.Add(right.Path);
		}

		if (right.Query.Count > 0)
		{
			res.Query = right.Query.Clone(res);
			useRight = true;
		}
		else if (!useRight)
			res.Query = Query.Clone(res);

		if (!string.IsNullOrWhiteSpace(right.Hash))
			res.Hash = right.Hash;
		else if (!useRight)
			res.Hash = Hash;

		return res;
	}

	public override bool Equals(object obj) 
		=> obj is Url q && q.Value == Value;

	public override int GetHashCode() 
		=> Value.GetHashCode();
		
	public int GetDeterministicHashCode() 
		=> Value.GetDeterministicHashCode();

	public override string ToString() 
		=> _value ??= Stringify();

	public static implicit operator Url(string url) 
		=> new(url);

	public static implicit operator string(Url url)
		=> url.With(u => u.ToString())!;

	string? _value;
	public string Value
	{
		get => _value ??= Stringify();
		set
		{
			if (_value != value)
			{
				_value = value;
					
				_match = null;
				_commands = null;
				_hash = null;
				_host = null;
				_login = null;
				_password = null;
				_path = null;
				_port = null;
				_protocol = null;
				_query = null;
			}
		}
	}

	#region Url uncoding/decoding

	public static bool IsSafeChar(char ch)
	{
		if (ch >= 97 && ch <= 122 || ch >= 65 && ch <= 90 || ch >= 48 && ch <= 57 || ch == 33)
			return true;

		switch (ch)
		{
		case '(':
		case ')':
		case '*':
		case '-':
		case '.':
		case '_':
			return true;
		default:
			return false;
		}
	}

	public static int HexToInt(char h)
	{
		if (h >= 48 && h <= 57)
			return h - 48;
		if (h >= 97 && h <= 102)
			return h - 97 + 10;
		if (h < 65 || h > 70)
			return -1;

		return h - 65 + 10;
	}

	public static char IntToHex(int n)
	{
		if (n <= 9)
			return (char) (n + 48);

		return (char) (n - 10 + 97);
	}


	public static string Encode(string str)
	{
		if (string.IsNullOrEmpty(str))
			return str;

		var bytes = Encoding.UTF8.GetBytes(str);

		var count = bytes.Length;
		var safe = true;
		for (var i = 0; i < count; ++i)
		{
			var ch = (char) bytes[i];
			if (ch == 32 || !IsSafeChar(ch))
			{
				safe = false;
				break;
			}
		}

		if (safe)
			return str;

		var res = new StringBuilder(count);

		for (var i = 0; i < count; ++i)
		{
			var num4 = bytes[i];
			var ch = (char) num4;
			if (IsSafeChar(ch))
				res.Append(ch);
			else if (ch == 32)
				res.Append('+');
			else
			{
				res.Append('%');
				res.Append(IntToHex(num4 >> 4 & 15));
				res.Append(IntToHex(num4 & 15));
			}
		}
		return res.ToString();
	}

	public static string Decode(string str)
	{
		if (string.IsNullOrEmpty(str))
			return str;

		var length = str.Length;

		var res = new StringBuilder(length);
		byte[]? bytes = null;
		var cnt = 0;

		for (var i = 0; i < length; ++i)
		{
			var ch1 = str[i];
			switch (ch1)
			{
			case '+':
				ch1 = ' ';
				goto default;
			case '%':
				if (i < length - 2)
				{
					if (str[i + 1] == 117 && i < length - 5)
					{
						var num1 = HexToInt(str[i + 2]);
						var num2 = HexToInt(str[i + 3]);
						var num3 = HexToInt(str[i + 4]);
						var num4 = HexToInt(str[i + 5]);
						if (num1 >= 0 && num2 >= 0 && num3 >= 0 && num4 >= 0)
						{
							if (cnt > 0)
							{
								// ReSharper disable AssignNullToNotNullAttribute
								res.Append(Encoding.UTF8.GetString(bytes, 0, cnt));
								cnt = 0;
							}

							res.Append((char) (num1 << 12 | num2 << 8 | num3 << 4 | num4));
							i += 5;
							break;
						}
					}
					else
					{
						var num1 = HexToInt(str[i + 1]);
						var num2 = HexToInt(str[i + 2]);
						if (num1 >= 0 && num2 >= 0)
						{

							bytes ??= new byte[length - i];

							bytes[cnt++] = (byte) (num1 << 4 | num2);

							i += 2;
							break;
						}
					}
				}
				goto default;
			default:
				if ((ch1 & 65408) == 0)
				{
					bytes ??= new byte[length];

					bytes[cnt++] = (byte) ch1;
					break;
				}
				if (cnt > 0)
				{
					res.Append(Encoding.UTF8.GetString(bytes, 0, cnt));
					cnt = 0;
				}
				res.Append(ch1);
				break;
			}
		}
		if (cnt > 0)
			res.Append(Encoding.UTF8.GetString(bytes, 0, cnt));
		// ReSharper restore AssignNullToNotNullAttribute

		return res.ToString();
	}

	#endregion

}