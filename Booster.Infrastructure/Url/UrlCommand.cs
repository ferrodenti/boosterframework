﻿namespace Booster;

public enum UrlCommand
{
	SiteRoot,
	AppRoot,
	Up,
}