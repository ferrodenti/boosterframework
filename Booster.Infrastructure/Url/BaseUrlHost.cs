using System.Linq;
using Booster.Interfaces;
using Booster.Parsing;

namespace Booster;

public abstract class BaseUrlHost : IStringValue, IConvertiableFrom<string>
{
	public abstract void Refresh();
	public abstract void Reparse();
	public abstract BaseUrlHost Clone(Url owner);
	public abstract string Value { get; set; }

	public bool IsEmpty => Value.IsEmpty();

	public static implicit operator BaseUrlHost(string url)
	{
		var m = UrlDomain.Parser.Match(url);
		if (m.Success)
			return new UrlDomain(m.GetString("d3"), m.GetString("d2"), m.GetString("d1"), null);

		if(url.All(char.IsLetter)) //TODO: включить такой вариант в регикс
			return new UrlDomain(null, url, null, null);

		m = UrlIp4.Parser.Match(url);
		if (m.Success)
			return new UrlIp4(m.GetInt("ip1"), m.GetInt("ip2"), m.GetInt("ip3"), m.GetInt("ip4"), null);
				
		throw new ParsingException($"\"{url}\" seems not a valid domain");
	}

	public static implicit operator string(BaseUrlHost url)
		=> url?.ToString();
}