using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Booster.Interfaces;
using Booster.Parsing;
using static Booster.FastLazyStatic;

#nullable enable

namespace Booster;

public class UrlIp4 : BaseUrlHost, IEnumerable<byte>, IConvertiableFrom<uint>
{
	internal static readonly Regex Parser = Utils.CompileRegex(@"
^
(?<ip1>\d{1,3})\.
(?<ip2>\d{1,3})\.
(?<ip3>\d{1,3})\.
(?<ip4>\d{1,3})
");

	readonly Url? _owner;

	string? _value;
	public override string Value
	{
		get => _value ??= string.Join(".", Octets);
		set
		{
			if (_value == value)
			{
				_value = value;
				_uintValue = null;
				_octets = null;
				_owner?.Changed();
			}
		}
	}

	uint? _uintValue;
	public uint UIntValue
	{
		get => _uintValue ??= BitConverter.ToUInt32(Octets, 0);
		set
		{
			if (value != _uintValue)
			{
				_uintValue = value;
				_value = null;
				_octets = null;
				_owner?.Changed();
			}
		}
	}

	FastLazy<byte[]>? _octets;
	public byte[] Octets
	{
		get => _octets ??= FastLazy(() =>
		{
			if (_uintValue.HasValue)
				return BitConverter.GetBytes(UIntValue);

			if (_value.IsSome())
			{
				var m = Parser.Match(_value);
				if (!m.Success)
					throw new ParsingException($"\"{_value}\" seems not a valid ip4 address");

				return new[] { (byte)m.GetInt("ip1"), (byte)m.GetInt("ip2"), (byte)m.GetInt("ip3"), (byte)m.GetInt("ip4") };
			}

			return Array.Empty<byte>();
		});
		set
		{
			if (_octets?.Value != value)
			{
				_octets = value;
				_value = null;
				_uintValue = null;
				_owner?.Changed();
			}
		}
	}

	uint IConvertiableFrom<uint>.Value
	{
		get => UIntValue;
		set => UIntValue = value;
	}

	protected UrlIp4(Url? owner) 
		=> _owner = owner;

	public UrlIp4(string value, Url? owner)
	{
		_owner = owner;
		_value = value;
	}
	public UrlIp4(uint value, Url? owner)
	{
		_uintValue = value;
		_owner = owner;
	}

	public UrlIp4(int o1, int o2, int o3, int o4, Url owner)
	{
		_owner = owner;

		if (o1 < 0 || o1 > 255) throw new ArgumentException();
		if (o2 < 0 || o2 > 255) throw new ArgumentException();
		if (o3 < 0 || o3 > 255) throw new ArgumentException();
		if (o4 < 0 || o4 > 255) throw new ArgumentException();

		_octets = new[] {(byte) o1, (byte) o2, (byte) o3, (byte) o4};
	}

	public override void Refresh()
	{
		Utils.Nop(UIntValue);
		_value = null;
		_octets = null;
	}

	public override void Reparse()
	{
		_uintValue = null;
		_octets = null;
	}

	public override string  ToString() 
		=> Value;

	public override int  GetHashCode() 
		=> (int) UIntValue;

	public override BaseUrlHost Clone(Url owner)
		=> new UrlIp4(owner)
		{
			_octets = _octets?.Value.ToArray() ?? Array.Empty<byte>(),
			_uintValue = _uintValue,
			_value = _value
		};


	public override bool Equals(object obj) 
		=> obj is UrlIp4 b && b.UIntValue == UIntValue;

	public IEnumerator<byte> GetEnumerator() 
		=> Octets.ToList().GetEnumerator();

	IEnumerator IEnumerable.GetEnumerator() 
		=> GetEnumerator();

	public byte this[int index]
	{
		get => Octets[index];
		set
		{
			Octets[index] = value;

			_value = null;
			_uintValue = null;
			_owner?.Changed();
		}
	}

	public static implicit operator UrlIp4(string url) 
		=> new(url, null);

	public static implicit operator string?(UrlIp4 ip) 
		=> ip.With(s => s.ToString());

	public static implicit operator UrlIp4(uint ip) 
		=> new(ip, null);

	public static implicit operator uint(UrlIp4 ip) 
		=> ip.UIntValue;
}