using System;
using System.Runtime.CompilerServices;
using Booster.Reflection;

namespace Booster.DI;

public class InstanceFactoryEvents
{
	internal TypeEx InstanceType { get; }

	EventHandler _changed;
	public event EventHandler Changed
	{
		[MethodImpl(MethodImplOptions.Synchronized)]
		add
		{
			_changed += value;

			InstanceFactory.AddEvent(this);
		}
			
		[MethodImpl(MethodImplOptions.Synchronized)]
		remove
		{
			// ReSharper disable once DelegateSubtraction
			_changed -= value;

			if (_changed.GetInvocationList().Length == 0)
				InstanceFactory.RemoveEvent(this);
		}
	}

	internal void OnChanged()
        => _changed?.Invoke(this, EventArgs.Empty);

    internal void On(InstanceFactoryActionType action)
        => OnChanged(); //Пока не вижу смысла в большом количестве событий, но можно сделать

    //public event EventHandler InstanceChanged;
	//public event EventHandler InstanceRegistred;
	//public event EventHandler InstanceUnregistred;
	//public event EventHandler CtorRegistred;
	//public event EventHandler CtorUnregistred;
	//public event EventHandler GetterRegistred;
	//public event EventHandler GetterUnregistred;

	public InstanceFactoryEvents(TypeEx instanceType)
        => InstanceType = instanceType;
}