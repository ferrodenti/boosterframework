using System;

namespace Booster.DI;

[AttributeUsage(AttributeTargets.Assembly)]
public class AssemblyInstancesRegistrationAttribute : Attribute
{
}