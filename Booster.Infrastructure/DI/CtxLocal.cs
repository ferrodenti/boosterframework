﻿using System;
using Booster.Interfaces;
using JetBrains.Annotations;

#nullable enable

namespace Booster.DI;

[PublicAPI]
public class CtxLocal<T> : ICtxLocal
{
	readonly Func<T>? _initializer;

	public bool NoDispose { get; }

	public CtxLocal(bool noDispose = false) 
		=> NoDispose = noDispose;

	public CtxLocal(Func<T> initializer, bool noDispose = false) : this(noDispose)
		=> _initializer = initializer;

	public T? Value
	{
		get
		{
			if (!TryGetValue(out var result) && _initializer != null)
			{
				result = _initializer();
				AsyncContext.Current[this] = result;
			}

			return result;
		}
		set => AsyncContext.Current[this] = value;
	}

	public bool TryGetValue(out T? value)
	{
		if (AsyncContext.Current.TryGetAltValue(this, out value))
			return true;

		value = default;
		return false;
	}

	public bool Reset() 
		=> AsyncContext.Current.Remove(this);

	public bool HasValue => TryGetValue(out _);
}