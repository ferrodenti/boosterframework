﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Reflection;
using Booster.Interfaces;
using Booster.Reflection;
using JetBrains.Annotations;

#nullable enable

namespace Booster.DI;

[PublicAPI] 
public static class InstanceFactory
{
	class ContextDicts
	{
		public readonly ConcurrentDictionary<TypeEx, object?> Instances = new();
		public readonly ConcurrentDictionary<TypeEx, Delegate> Getters = new();
		public readonly ConcurrentDictionary<TypeEx, Delegate> Ctors = new();
		public readonly ConcurrentDictionary<TypeEx, InstanceFactoryEvents> Events = new();
	}
	
	static readonly CtxLocal<ContextDicts> _currentContextDicts = new();
	static ContextDicts CurrentContextDicts => _currentContextDicts.Value ?? (_currentContextDicts.Value = _contexts.GetOrAdd(CurrentContext, _ => new ContextDicts()));

	static readonly CtxLocal<Box<string>> _currentContext = new();
	public static string CurrentContext
	{
		get => _currentContext.Value ?? "DEFAULT";
		set
		{
			if (CurrentContext != value)
			{
				_currentContext.Value =  value;
				_currentContextDicts.Value = null;
			}
		}
	}

	static InstanceFactory()
	{
		void EmergencyLog(Exception exception)
		{
			lock (Utils.TraceLock)
				Trace.Fail(exception.Message, exception.ToString());

			Console.WriteLine(exception);
		}

		var assemblies = AppDomain.CurrentDomain.GetAssemblies();
		foreach (var assembly in assemblies)
			try
			{
				var attr = assembly.GetCustomAttribute<AssemblyInstancesRegistrationAttribute>();
				if (attr != null)
					foreach (var type in assembly.GetTypes())
					{
						var iattr = type.GetCustomAttribute<InstanceFactoryAttribute>();
						if (iattr?.AutoRegiser == true)
							try
							{
								var instance = Activator.CreateInstance(type);
								Register(iattr.DefaultInterface, instance, true);
							}
							catch (Exception e)
							{
								EmergencyLog(e);
							}
					}
			}
			catch (Exception e)
			{
				EmergencyLog(e);
			}
	}
	static readonly ConcurrentDictionary<string, ContextDicts> _contexts = new();

	public static T Get<T>(Func<T> creator) 
		=> (T)Get(CurrentContextDicts, typeof(T), creator, true)!;
	
	public static T? Get<T>(Func<T> creator, bool throwIfNotFound) 
		=> (T?)Get(CurrentContextDicts, typeof(T), creator, throwIfNotFound);
		
	public static T Get<T>() 
		=> (T)Get(typeof(T));
	
	public static T? Get<T>(bool throwIfNotFound) 
		=> (T?)Get(typeof(T), throwIfNotFound);

	public static T Get<T>(string context)
		=> (T)Get(context, typeof(T));
	
	public static T? Get<T>(string context, bool throwIfNotFound) 
		=> (T?)Get(context, typeof(T), throwIfNotFound);
	
	public static object Get(TypeEx type) 
		=> Get(CurrentContextDicts, type, null, true)!;
	
	public static object? Get(TypeEx type, bool throwIfNotFound) 
		=> Get(CurrentContextDicts, type, null, throwIfNotFound);

	public static object Get(string context, TypeEx type)
		=> Get(context, type, true)!;
	
	public static object? Get(string context, TypeEx type, bool throwIfNotFound)
	{
		var ctx = CurrentContext == context ? CurrentContextDicts : _contexts.GetOrAdd(context, _ => new ContextDicts());
		return Get(ctx, type, null, throwIfNotFound);
	}
	
	static object? Get(ContextDicts ctx, TypeEx type, Delegate? creator, bool throwIfNotFound)
	{
		if (!TryGetInstance(ctx, type, out var res))
			lock (type)
				if (!TryGetInstance(ctx, type, out res))
				{
					var ctor = creator ?? ctx.Ctors.SafeGet(type);

					if (ctor != null)
						ctx.Instances[type] = res = ctor.DynamicInvoke();
				}

		if (res == null)
		{
			if(throwIfNotFound)
				throw new Exception($"Could not get instance of type {type.FullName}, CTX: {AsyncContext.CurrentName}");
		}
			
		// ReSharper disable once UseMethodIsInstanceOfType
		else if (!type.IsAssignableFrom(res.GetType())) //else if (!type.IsInstanceOfType(res))
			throw new Exception($"{res.GetType()} is not an instance of {type.FullName}");
			
		return res;
	}
	
	static bool TryGetInstance(ContextDicts ctx, TypeEx type, out object? res)
	{
		if (ctx.Getters.TryGetValue(type, out var getter))
		{
			res = getter.DynamicInvoke();
			return true;
		}

		return ctx.Instances.TryGetValue(type, out res);
	}
	
	public static bool TryGet<T>(out T result)
	{
		if (TryGetInstance(CurrentContextDicts, typeof(T), out var res) && res is T res2)
		{
			result = res2;
			return true;
		}

		result = default!;
		return false;
	}

	public static bool TryGet(TypeEx type, out object result) 
		=> TryGetInstance(CurrentContextDicts, type, out result!);
	
	public static bool IsInstanceExists<T>() 
		=> IsInstanceExists(typeof(T));
		
	public static bool IsRegistered<T>() 
		
		=> IsRegistered(typeof(T));
	public static void RemoveInstance<T>() 
		
		=> RemoveInstance(typeof (T));
	public static void RemoveCtor<T>() 
		=> RemoveCtor(typeof (T));
		
	public static void RemoveGetter<T>() 
		=> RemoveGetter(typeof (T));

	
	public static IDisposable? PushContext(string? context)
	{
		context ??= "DEFAULT";

		var old = CurrentContext;
		if (old == context)
			return null;

		CurrentContext = context;
		return new ActionDisposable(() => CurrentContext = old);
	}

	public static InstanceFactoryToken Register<T>(T instance, bool? allowOverride = false) 
		=> Register(typeof(T), instance, allowOverride);

	public static InstanceFactoryToken RegisterCtor<T>(Func<T> ctor, bool? allowOverride = false) 
		=> RegisterCtor(typeof(T), ctor, allowOverride);

	public static InstanceFactoryToken RegisterGetter<T>(Func<T> getter, bool? allowOverride = false) 
		=> RegisterGetter(typeof(T), getter, allowOverride);

	static TypeEx GetInterfaceForInstance(TypeEx? type, object? instance)
	{
		var instanceType = instance.GetTypeEx();
		
		if (type != null && !type.Equals(instanceType))
			return type;

		instanceType ??= type;

		if (instanceType?.TryFindAttribute<InstanceFactoryAttribute>(out var attr) == true && 
			attr.DefaultInterface != null)
			return attr.DefaultInterface;

		return instanceType ?? throw new ArgumentNullException(nameof(instance), "Expected either type or instance.");
	}

	public static bool IsInstanceExists(TypeEx type) 
		=> CurrentContextDicts.Instances.ContainsKey(type);

	public static bool IsRegistered(TypeEx type)
	{
		var ctx = CurrentContextDicts;

		return IsInstanceExists(type) ||
			   ctx.Getters.ContainsKey(type) ||
			   ctx.Ctors.ContainsKey(type);
	}

	public static InstanceFactoryToken Register(TypeEx? type, object? instance, bool? allowOverride = false)
	{
		type = GetInterfaceForInstance(type, instance);

		var ctx = CurrentContextDicts;
		if (ctx.Instances.TryGetValue(type, out var prev))
			switch (allowOverride)
			{
			case false:
				throw new Exception($"Only one instance of {type.FullName} is allowed");
			case null:
				return new InstanceFactoryToken(type, prev, InstanceFactoryTokenType.Instance);
			}

		ctx.Instances[type] = instance;

		On(type, InstanceFactoryActionType.InstanceRegistred);

		return new InstanceFactoryToken(type, instance, InstanceFactoryTokenType.Instance);
	}
	
	public static InstanceFactoryToken RegisterCtor(TypeEx type, Delegate ctor, bool? allowOverride = false)
	{
		type = GetInterfaceForInstance(type, null);

		var ctx = CurrentContextDicts;

		if (ctx.Ctors.TryGetValue(type, out var prev))
			switch (allowOverride)
			{

			case false:
				throw new Exception($"Only one constructor for {type.FullName} is allowed");
			case null:
				return new InstanceFactoryToken(type, prev, InstanceFactoryTokenType.Ctor);
			}

		ctx.Ctors[type] = ctor;
		On(type, InstanceFactoryActionType.CtorRegistred);

		return new InstanceFactoryToken(type, ctor, InstanceFactoryTokenType.Ctor);
	}

	public static InstanceFactoryToken RegisterGetter(TypeEx type, Delegate getter, bool? allowOverride = false)
	{
		type = GetInterfaceForInstance(type, null);

		var ctx = CurrentContextDicts;
		if (ctx.Getters.TryGetValue(type, out var prev))
			switch (allowOverride)
			{
			case false:
				throw new Exception($"Only one getter for {type.FullName} is allowed");
			case null:
				return new InstanceFactoryToken(type, prev, InstanceFactoryTokenType.Getter);
			}

		ctx.Getters[type] = getter;

		On(type, InstanceFactoryActionType.GetterRegistred);

		return new InstanceFactoryToken(type, getter, InstanceFactoryTokenType.Getter);
	}

	public static void RemoveInstance(TypeEx type)
	{
		type = GetInterfaceForInstance(type, null);

		if (CurrentContextDicts.Instances.TryRemove(type, out var o))
		{
			On(type, InstanceFactoryActionType.InstanceRemoved);
				
			(o as IUnregisterInstance)?.UnregisterInstance();
		}
	}

	public static void RemoveCtor(TypeEx type)
	{
		type = GetInterfaceForInstance(type, null);

		if (CurrentContextDicts.Ctors.TryRemove(type, out _))
			On(type, InstanceFactoryActionType.CtorRemoved);
	}

	public static void RemoveGetter(TypeEx type)
	{
		type = GetInterfaceForInstance(type, null);

		if(CurrentContextDicts.Getters.TryRemove(type, out _))
			On(type, InstanceFactoryActionType.GetterRemoved);
	}
	
	public static InstanceFactoryEvents On(TypeEx type) 
		=> CurrentContextDicts.Events.SafeGet(type) ?? new InstanceFactoryEvents(type);
		
	public static InstanceFactoryEvents On<T>() 
		=> On(typeof(T));

	static void On(TypeEx instance, InstanceFactoryActionType action)
	{
		if (CurrentContextDicts.Events.TryGetValue(instance, out var ev))
			ev.On(action);
	}


	internal static void AddEvent(InstanceFactoryEvents ev) 
		=> CurrentContextDicts.Events[ev.InstanceType] = ev;

	internal static void RemoveEvent(InstanceFactoryEvents ev) 
		=> CurrentContextDicts.Events.Remove(ev.InstanceType);

	internal static bool Unregister(InstanceFactoryToken token)
	{
		IDictionary dict = token.Type switch
		{
			InstanceFactoryTokenType.Instance => CurrentContextDicts.Instances,
			InstanceFactoryTokenType.Ctor => CurrentContextDicts.Ctors,
			InstanceFactoryTokenType.Getter => CurrentContextDicts.Getters,
			_ => throw new ArgumentException($"Unknown token type: {token.Type}", nameof(token))
		};

		if (dict.Contains(token.InstanceType) && dict[token.InstanceType] == token.Value)
		{
			dict.Remove(token.InstanceType);
			return true;
		}

		return false;
	}
}