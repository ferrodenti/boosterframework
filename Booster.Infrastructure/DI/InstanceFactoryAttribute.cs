using System;
using Booster.Reflection;
using JetBrains.Annotations;

namespace Booster.DI;

[MeansImplicitUse]
public class InstanceFactoryAttribute : Attribute
{
	public TypeEx DefaultInterface { get; }
	public bool AutoRegiser { get; }

	public InstanceFactoryAttribute(Type defaultInterface, bool autoRegiser = false)
	{
		DefaultInterface = defaultInterface;
		AutoRegiser = autoRegiser;
	}
}