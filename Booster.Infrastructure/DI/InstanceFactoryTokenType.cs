﻿namespace Booster.DI;

enum InstanceFactoryTokenType
{
	Instance,
	Ctor,
	Getter
}