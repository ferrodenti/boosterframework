﻿namespace Booster.DI;

public static class Instance<T> where T : class
{
	static readonly ContextLazy<T> _lazy = new(InstanceFactory.Get<T>);

	static readonly CtxLocal<T> _lastValue = new(true);
	static readonly CtxLocal<Box<string>> _lastContext = new();

	public static T Value
	{
		get
		{
			var ctx = InstanceFactory.CurrentContext;

			if (_lastValue.Value != null && _lastContext.Value == ctx)
				return _lastValue.Value;

			_lastValue.Value = _lazy.Value;
			_lastContext.Value = ctx;

			return _lastValue.Value;
		}
	}

	static Instance()
		=> InstanceFactory.On<T>().Changed += (_, _) =>
		{
			var ctx = InstanceFactory.CurrentContext;
			if (ctx == _lastContext.Value)
				_lastValue.Value = null;

			_lazy.Reset();
		};
}