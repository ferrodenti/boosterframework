﻿#define LOG_ASYNC_CONTEXT1

using System;
using System.Collections.Generic;
using System.Threading;
using Booster.Interfaces;

#nullable enable

namespace Booster.DI;

#if DEBUG_ASYNC_CONTEXT
using System.Diagnostics;
using Log;

public static class AsyncContext
{
	static readonly AsyncLocal<AsyncContextDebug> _context = new AsyncLocal<AsyncContextDebug>();
	static volatile int _counter;
	
	public static IDictionary<object, object> Current
	{
		get
		{
			var result = _context.Value;
			if (result == null)
			{
				_context.Value = result = new AsyncContextDebug(GetNewName());
				Log($"Current get: {CurrentName}");
			}
			return result;
		}
	}

	static string GetNewName()
		=> $"unnamed #{++_counter}";
	
	public static string CurrentName => _context.Value?.Name ?? "(null)";
	static ILog _log;
	
	[Conditional("LOG_ASYNC_CONTEXT")]
	static void Log(string message)
	{
		var log = _log ??= InstanceFactory.Get<ILog>(false);
		log.Debug(message);
	}

	public static IDisposable Push(string name = null)
	{
		var prev = _context.Value;
		InitNew(name);

		Log($"CTX PUSH: {prev?.Name} => {_context.Value.Name}");
			
		return new ActionDisposable(() =>
		{
			Log($"CTX POP: {prev?.Name} => {_context.Value.Name}");
			Dispose();
			//_context.Value = prev;
		});
	}

	public static void InitNew(string name = null)
	{
		_context.Value = new AsyncContextDebug(name ?? GetNewName());
		Log($"InitNew: {CurrentName}");
	}

	public static void Dispose()
	{
		Log($"Dispose: {CurrentName}");
		var ctx = _context.Value;
		_context.Value = null;
		ctx?.Dispose();
	}
}
#else
using Collections;
		
public static class AsyncContext
{
	static readonly AsyncLocal<HybridDictionary<object, object?>?> _context = new();
	public static IDictionary<object, object?> Current => _context.Value ?? (_context.Value = new HybridDictionary<object, object?>());
	public static string CurrentName => _context.Value?.SafeGet("__ctx_name__")?.ToString() ?? "";

	public static IDisposable Push(string? name = null)
	{
		InitNew(name);

		return new ActionDisposable(Dispose);
	}

	public static void InitNew(string? name = null) 
	{
		_context.Value = new HybridDictionary<object, object?>();

		if (name != null)
			_context.Value["__ctx_name__"] = name;
	}

	public static void Dispose()
	{
		var ctx = _context.Value;
		_context.Value = null;
			
		if (ctx != null)
			foreach (var pair in ctx)
				if (pair.Value is IDisposable disp)
				{
					if (pair.Key is ICtxLocal {NoDispose: true})
						continue;
	
					disp.Dispose();
				}
	}
}
#endif