﻿namespace Booster.DI;

public enum InstanceFactoryActionType
{
	InstanceRegistred,
	CtorRegistred,
	GetterRegistred,
	InstanceRemoved,
	CtorRemoved,
	GetterRemoved
}