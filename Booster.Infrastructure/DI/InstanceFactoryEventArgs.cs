﻿using System;
using Booster.Reflection;

namespace Booster.DI;

public class InstanceFactoryEventArgs : EventArgs
{
	public TypeEx IntefaceType { get; set; }
	public InstanceFactoryActionType Event { get; set; }

	public InstanceFactoryEventArgs(TypeEx interfaceType, InstanceFactoryActionType ev)
	{
		IntefaceType = interfaceType;
		Event = ev;
	}
}