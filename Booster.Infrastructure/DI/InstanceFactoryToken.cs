﻿using System;
using Booster.Reflection;
using JetBrains.Annotations;

namespace Booster.DI;

[PublicAPI]
public class InstanceFactoryToken : IDisposable
{
	internal InstanceFactoryToken(TypeEx instanceType, object value, InstanceFactoryTokenType type)
	{
		Type = type;
		Value = value;
		InstanceType = instanceType;
	}

	internal TypeEx InstanceType { get; }
	internal object Value { get; }
	internal InstanceFactoryTokenType Type { get; }

	public bool Unregister()
		=> InstanceFactory.Unregister(this);

	public void Dispose() 
		=> Unregister();
}