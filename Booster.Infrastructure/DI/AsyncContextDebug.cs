#if DEBUG_ASYNC_CONTEXT

using System;
using System.Collections;
using System.Collections.Generic;
using Booster.Collections;
using Booster.Interfaces;

#if TRACE_LOG_ASYNC_CONTEXT
using System.Diagnostics;
#endif

namespace Booster.DI
{	
	public class AsyncContextDebug : IDictionary<object, object>, IDisposable
	{
		volatile bool _disposing;

		readonly string _name;

		public string Name => _disposing ? $"{_name} DISPOSED" : _name;

#if TRACE_LOG_ASYNC_CONTEXT
		readonly string _created;
		string _disposed;
#endif
		
		readonly HybridDictionary<object, object> _inner = new HybridDictionary<object, object>();

		public AsyncContextDebug(string name)
		{
			_name = name;
#if TRACE_LOG_ASYNC_CONTEXT
			_created = new StackTrace(2).ToString();
#endif
		}

		public IEnumerator<KeyValuePair<object, object>> GetEnumerator()
		{
			EnsureNotDisposed();
			return _inner.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			EnsureNotDisposed();
			return ((IEnumerable) _inner).GetEnumerator();
		}

		public void Add(KeyValuePair<object, object> item)
		{
			EnsureNotDisposed();
			_inner.Add(item);
		}

		public void Clear()
		{
			EnsureNotDisposed();
			_inner.Clear();
		}

		public bool Contains(KeyValuePair<object, object> item)
		{
			EnsureNotDisposed();
			return _inner.Contains(item);
		}

		public void CopyTo(KeyValuePair<object, object>[] array, int arrayIndex)
		{
			EnsureNotDisposed();
			_inner.CopyTo(array, arrayIndex);
		}

		public bool Remove(KeyValuePair<object, object> item)
		{
			EnsureNotDisposed();
			return _inner.Remove(item);
		}

		public int Count
		{
			get
			{
				EnsureNotDisposed();
				return _inner.Count;
			}
		}

		public bool IsReadOnly
		{
			get
			{
				EnsureNotDisposed();
				return _inner.IsReadOnly;
			}
		}

		public void Add(object key, object value)
		{
			EnsureNotDisposed();
			_inner.Add(key, value);
		}

		public bool ContainsKey(object key)
		{
			EnsureNotDisposed();
			return _inner.ContainsKey(key);
		}

		public bool Remove(object key)
		{
			EnsureNotDisposed();
			return _inner.Remove(key);
		}

		public bool TryGetValue(object key, out object value)
		{
			EnsureNotDisposed();
			return _inner.TryGetValue(key, out value);
		}

		public object this[object key]
		{
			get
			{
				EnsureNotDisposed();
				return _inner[key];
			}
			set
			{
				EnsureNotDisposed();
				_inner[key] = value;
			}
		}

		public ICollection<object> Keys
		{
			get
			{
				EnsureNotDisposed();
				return _inner.Keys;
			}
		}

		public ICollection<object> Values
		{
			get
			{
				EnsureNotDisposed();
				return _inner.Values;
			}
		}

		protected void EnsureNotDisposed()
		{
			if (_disposing)
				throw new ObjectDisposedException(nameof(AsyncContextDebug));
		}
		
		public void Dispose()
		{
			EnsureNotDisposed();
			_disposing = true;
			
			foreach (var pair in _inner)
				if (pair.Value is IDisposable disp)
				{
					if (pair.Key is ICtxLocal loc && loc.NoDispose)
						continue;

					disp.Dispose();
				}
			
			_inner.Clear();
			
#if TRACE_LOG_ASYNC_CONTEXT
			_disposed = new StackTrace(2).ToString();
#endif
		}

		public override string ToString()
		{
			var result = new EnumBuilder();
			result.Append(Name);
#if TRACE_LOG_ASYNC_CONTEXT
			result.Append($"CREATED: {_created}");
			if (_disposed != null)
				result.Append($"DISPOSED: {_disposed}");
#endif
			return result;
		}
	}	
}

#endif