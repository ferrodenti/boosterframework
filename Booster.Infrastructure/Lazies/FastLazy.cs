﻿using System;
using System.Threading.Tasks;
using Booster.Interfaces;
using JetBrains.Annotations;

#nullable enable

namespace Booster;

[PublicAPI]
public static class FastLazyStatic
{
	public static async Task<FastLazy<T>> FastLazyAsync<T>([InstantHandle]Task<T> value)
	{
		var val = await value.NoCtx();
		return new FastLazy<T>(val);
	}

	public static FastLazy<T> FastLazy<T>([InstantHandle]Func<T> initialization) 
		=> new(initialization());

	//[Obsolete]
	public static FastLazy<T> FastLazy<T>(T value)
		=> new(value);
}

public class FastLazy<T> : IConvertiableFrom<T>
{
	public T Value { get; set; }

	public FastLazy()
	{
		Value = default!;
	}
	public FastLazy(T value) 
		=> Value = value;

	public FastLazy([InstantHandle] Func<T> initialization)
		=> Value = initialization();
	
	public static implicit operator T(FastLazy<T> cache) 
		=> cache.Value;
	
	public static implicit operator FastLazy<T>(T value) 
		=> new(value);

	public static FastLazy<T?> Default => new ((T?)default);
	public bool IsEmpty => false;
}
