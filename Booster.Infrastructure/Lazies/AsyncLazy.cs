﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Booster.Hacks.System;
using Booster.Kitchen;
using Booster.Synchronization;
using JetBrains.Annotations;

#nullable enable

namespace Booster;

[PublicAPI]
[DebuggerDisplay("IsValueCreated={IsValueCreated}, IsValueFaulted={IsValueFaulted}, Value={ValueIfCreated}")]
public class AsyncLazy<T> : BaseLazy<T>
{
	protected readonly AsyncLock Lock = new();
	int _recursionDetector;
	
	[PublicAPI] public Func<Task<T>>? Factory;
	protected Func<object?, Task<T>>? TargetFactory;

	public AsyncLazy(Func<Task<T>> valueFactory) 
		=> Factory = valueFactory ?? throw new ArgumentNullException(nameof(valueFactory));
		
	protected AsyncLazy(Func<object?, Task<T>>? valueFactory) 
		=> TargetFactory = valueFactory ?? throw new ArgumentNullException(nameof(valueFactory));

	public AsyncLazy()
	{
	}

	public AsyncLazy<T> SetFactory<TTarget>(Func<TTarget?, Task<T>> factory)
	{
		TargetFactory = target => factory((TTarget?) target);
		return this;
	}

	public AsyncLazy<T> SetFactory(Func<Task<T>> factory)
	{
		Factory = factory;
		return this;
	}

	protected virtual Task<T> CreateValue(object? target)
	{
		using (CurrentLazy.Push(this))
			return TargetFactory != null ? TargetFactory(target) : Factory?.Invoke()!;
	}

	public virtual async Task<T> GetValue(object? target)
	{
		Throw();

		if (!IsValueCreated)
		{
			using var _ = await Lock.LockAsync().NoCtx();

			if (!IsValueCreated)
			{
				var value = await HandleValueCreation(target).NoCtx();
				SetInternal(value);
			}
		}

		return GetInternal();
	}
		
	public virtual void SetValue(T value)
	{
		using (Lock.Lock())
			SetInternal(value);
	}

	protected async Task<T> HandleValueCreation(object? target)
	{
		try
		{
			if (Interlocked.Increment(ref _recursionDetector) > 1)
				throw new InvalidOperationException(SR.Lazy_Value_RecursiveCallsToValue);

			return await CreateValue(target).NoCtx();
		}
		catch (Exception ex)
		{
			OnError(ex);
			throw;
		}
		finally
		{
			Interlocked.Decrement(ref _recursionDetector);
		}
	}

	public static implicit operator Task<T>(AsyncLazy<T> entity) 
		=> entity.GetValue(null);

	public static AsyncLazy<T> Create<TThis>(Func<TThis, Task<T>> factory) 
		=> new(o => factory((TThis) o!));
}