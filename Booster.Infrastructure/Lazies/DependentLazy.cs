﻿using System;
using Booster.Interfaces;
using Booster.Kitchen;
using JetBrains.Annotations;

namespace Booster;

[PublicAPI]
public class DependentLazy<T> : SyncLazy<T>, IDependentLazy
{
	ILazyDependency _depencencies;
		
	public ILazyDependency GetDependency(bool create = true) // Tут доступ по идее должен быть синхронным
	{
		if (_depencencies == null && create)
			_depencencies = CreateDependecy();

		return _depencencies;
	}

	public DependentLazy()
	{
	}
	public DependentLazy(Func<T> factory) : base(factory)
	{
	}
	protected DependentLazy(Func<object,T> factory) : base(factory)
	{
	}
		
	protected virtual ILazyDependency CreateDependecy() 
		=> new BasicLazyDependency(this);
		
	public override void Reset()
	{
		lock (this)
		{
			GetDependency(false)?.Reset();
			base.Reset();
		}
	}
		
	public static new DependentLazy<T> Create<TThis>(Func<TThis, T> factory) 
		=> new(o => factory((TThis) o));
}