﻿using System;
using System.Threading.Tasks;
using Booster.Interfaces;
using Booster.Kitchen;
using JetBrains.Annotations;

namespace Booster;

[PublicAPI]
public class DependentAsyncLazy<T> : AsyncLazy<T>, IDependentLazy
{
	ILazyDependency _depencencies;
		
	public ILazyDependency GetDependency(bool create = true) // Tут доступ по идее должен быть синхронным
	{
		if (_depencencies == null && create)
			_depencencies = CreateDependecy();

		return _depencencies;
	}

	public DependentAsyncLazy()
	{
	}
	public DependentAsyncLazy(Func<Task<T>> factory) : base(factory)
	{
	}
	protected DependentAsyncLazy(Func<object,Task<T>> factory) : base(factory)
	{
	}
		
	protected virtual ILazyDependency CreateDependecy() 
		=> new BasicLazyDependency(this);
		
	public override void Reset()
	{
		lock (this)
		{
			GetDependency(false)?.Reset();
			base.Reset();
		}
	}
		
	public static new DependentAsyncLazy<T> Create<TThis>(Func<TThis, Task<T>> factory) 
		=> new(o => factory((TThis) o));	
}