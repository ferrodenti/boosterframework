﻿using System;
using System.Diagnostics;
using Booster.Hacks.System;
using Booster.Kitchen;
using JetBrains.Annotations;

#nullable enable

namespace Booster;

[PublicAPI]
[DebuggerDisplay("IsValueCreated={IsValueCreated}, IsValueFaulted={IsValueFaulted}, Value={ValueIfCreated}")]
public class SyncLazy<T> : BaseLazy<T>
{
	volatile bool _recursionDetector;
		
	[PublicAPI] public Func<T>? Factory;
	protected Func<object, T>? TargetFactory;

	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	public T Value
	{
		get => GetValue(null);
		set => SetValue(value);
	}

	public void EnsureCreated() 
		=> GetValue(null);

	public SyncLazy(Func<T> valueFactory) 
		=> Factory = valueFactory ?? throw new ArgumentNullException(nameof(valueFactory));
		
	protected SyncLazy(Func<object, T> valueFactory) 
		=> TargetFactory = valueFactory ?? throw new ArgumentNullException(nameof(valueFactory));
		
	public SyncLazy()
	{
	}

	public SyncLazy<T> SetFactory<TTarget>(Func<TTarget, T> factory)
	{
		TargetFactory = target => factory((TTarget) target);
		return this;
	}
	
	public SyncLazy<T> SetFactory(Func<T> factory)
	{
		Factory = factory;
		return this;
	}

	protected virtual T CreateValue(object? target)
	{
		using (CurrentLazy.Push(this))
			if (TargetFactory != null)
			{
				if (target == null)
					throw new ArgumentNullException(nameof(target), "Target expected");

				return TargetFactory(target);
			}
			else
				return Factory!();
	}

	public virtual T GetValue(object? target)
	{
		Throw();

		if (!IsValueCreated)
			lock (this)
				if (!IsValueCreated)
					SetInternal(HandleValueCreation(target));

		return GetInternal();
	}
		
	public virtual void SetValue(T value)
	{
		lock (this)
			SetInternal(value);
	}
		
	protected T HandleValueCreation(object? target)
	{
		try
		{
			if (_recursionDetector)
				throw new InvalidOperationException(SR.Lazy_Value_RecursiveCallsToValue);

			_recursionDetector = true;

			return CreateValue(target);
		}
		catch (Exception ex)
		{
			OnError(ex);
			throw;
		}
		finally
		{
			_recursionDetector = false;
		}
	}
		
	public static implicit operator T(SyncLazy<T> lazy) 
		=> lazy.Value;

	public static SyncLazy<T> Create<TThis>(Func<TThis, T> factory) 
		=> new(o => factory((TThis) o));
}