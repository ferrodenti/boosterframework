﻿using System;
using System.Diagnostics;
using System.Runtime.ExceptionServices;
using Booster.Hacks.System;
using Booster.Interfaces;
using Booster.Reflection;
using JetBrains.Annotations;

namespace Booster.Kitchen;

public abstract class BaseLazy : ILazy
{
	ExceptionDispatchInfo _exceptionDispatch;
		
	public virtual bool IsValueFaulted => _exceptionDispatch != null;
	public abstract bool IsValueCreated { get; }
	public abstract TypeEx ValueType { get; }
	[PublicAPI] public bool RetryOnError;
		
	public event EventHandler Reseted;

	protected virtual void Throw()
	{
		if (!RetryOnError)
			_exceptionDispatch?.Throw();
	}

	protected virtual void OnError(Exception ex) 
		=> _exceptionDispatch = ExceptionDispatchInfo.Capture(ex);

	protected virtual void ResetError() 
		=> _exceptionDispatch = null;

	protected void OnReseted() 
		=> Reseted?.Invoke(this, EventArgs.Empty);
		
	public abstract void Reset();
}
	
[DebuggerDisplay("IsValueCreated={IsValueCreated}, IsValueFaulted={IsValueFaulted}, Value={ValueIfCreated}")]
public abstract class BaseLazy<T> : BaseLazy
{
	T _value;
	volatile bool _valueCreated;
		
	public virtual T ValueIfCreated => !IsValueCreated ? default : _value;
	public override bool IsValueCreated => _valueCreated;
	public override TypeEx ValueType => typeof(T);

	internal T GetInternal()
		=> _value;

	protected void SetInternal(T value)
	{
		_value = value;
		_valueCreated = true;
		ResetError();
	}

	public override void Reset()
	{
		_value = default;
		_valueCreated = false;
		ResetError();
		OnReseted();
	}

	public override string ToString() 
		=> !IsValueCreated ? SR.Lazy_ToString_ValueNotCreated : $"{_value}";
}

public static class BaseLazyTExpander
{
	public static void DisposeReset<T>(this BaseLazy<T> lazy)
		where T : IDisposable
	{
		if (lazy.IsValueCreated)
		{
			lazy.GetInternal().Dispose();
			lazy.Reset();
		}
	}

}