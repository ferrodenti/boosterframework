﻿using System;
using System.Collections.Generic;
using Booster.DI;
using Booster.Interfaces;
using Booster.Log;
using Booster.Reflection;

#nullable enable

namespace Booster.Kitchen;

public class BasicLazyDependency : ILazyDependency
{
	readonly WeakReference<ILazy> _target;
	readonly List<IWeakEventHandle> _handles = new();

	public event EventHandler? Changed;
		
	public BasicLazyDependency(ILazy target) 
		=> _target = new WeakReference<ILazy>(target);

	protected virtual void OnChanged()
	{
		_target.Get()?.Reset();
		Changed?.Invoke(this, EventArgs.Empty);
	}

	public virtual void Reset()
	{
		foreach (var handle in _handles.ToArray())
			handle?.Unsubscribe();

		_handles.Clear();
	}

	public void Add<T>(Func<T, bool>? condition = null)
	{
		if (condition != null)
			Add(typeof(T), obj => condition((T) obj));
		else
			Add(typeof(T));
	}

	public void Add(object entity)
		=> Add(TypeEx.Of(entity), (_, _) => OnChanged());

	public void Add(TypeEx modelType, Func<object, bool>? condition = null)
		=> Add(modelType, (entity, _) =>
		{
			if (CheckCondition(condition, entity))
				OnChanged();
		});

	protected void Add(TypeEx modelType, Action<object, EventArgs> callback)
	{
		var dependencySource = Instance<IDependencySource>.Value;
		var handle = dependencySource.Add(this, modelType, callback);
		_handles.Add(handle);
	}

	public void Add(ILazy lazy)
		=> _handles.Add(WeakEvents.Subscribe(
			lazy, this,
			(s, eh) => { s.Reseted += eh; },
			(s, eh) => { s.Reseted -= eh; },
			(me, _, _) => me.OnChanged()));

	public void Add(ILazyDependency other)
		=> _handles.Add(WeakEvents.Subscribe(
			other, this,
			(s, eh) => { s.Changed += eh; },
			(s, eh) => { s.Changed -= eh; },
			(me, _, _) => me.OnChanged()));

	static bool CheckCondition<TModel>(Func<TModel, bool>? condition, TModel? entity)
	{
		if (condition == null || entity == null)
			return true;

		try
		{
			return condition(entity);
		}
		catch (Exception ex)
		{
			InstanceFactory.Get<ILog>(false).Error(ex, "CheckCondition");
		}

		return true;
	}
}