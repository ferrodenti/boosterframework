﻿using System;
using System.Collections.Generic;
using Booster.DI;
using Booster.Interfaces;
using JetBrains.Annotations;

#nullable enable

namespace Booster;

[PublicAPI]
public static class CurrentLazy
{
	static readonly CtxLocal<ILazy> _currentLazy = new();
	public static ILazy? Value => _currentLazy.Value;

	public static IDependentLazy GetDependentLazy()
		=> GetDependentLazy(true)!;

	public static IDependentLazy? GetDependentLazy(bool throwException)
	{
		var cur = _currentLazy.Value;

		if (cur is IDependentLazy dependentLazy)
			return dependentLazy;

		if (!throwException)
			return null;

		if (cur == null)
			throw new InvalidOperationException("No lazy is evaluating");


		throw new InvalidOperationException($"Current evaluating lazy has type {cur.GetType()} and it is not impement a IDependentLazy");
	}


	internal static IDisposable Push(ILazy lazy)
	{
		var prev = _currentLazy.Value;
		_currentLazy.Value = lazy;
		return new ActionDisposable(() => _currentLazy.Value = prev);
	}

	public static void AddDependency<T>(bool throwIfNoCurrentLazy = true)
		=> GetDependentLazy(throwIfNoCurrentLazy)?.GetDependency().Add<T>();

	public static void AddDependency<T>(T entity, bool throwIfNoCurrentLazy = true)
		=> GetDependentLazy(throwIfNoCurrentLazy)?.GetDependency().Add(entity);

	public static void AddDependency<T>(IEnumerable<T> entities, bool throwIfNoCurrentLazy = true)
	{
		var dep = GetDependentLazy(throwIfNoCurrentLazy)?.GetDependency();
		if (dep != null)
			foreach (var entity in entities)
				dep.Add(entity);
	}

	public static void AddDependency<T>(Func<T, bool> condition, bool throwIfNoCurrentLazy = true)
		=> GetDependentLazy(throwIfNoCurrentLazy)
			?.GetDependency()
			.Add(condition);
}