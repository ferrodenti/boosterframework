using System;
using System.Collections.Specialized;
using System.Runtime.ExceptionServices;
using Booster.DI;
using Booster.Hacks.System;
using Booster.Reflection;

namespace Booster;

public class ContextLazy<T> : SyncLazy<T>
{
	readonly SyncLazy<HybridDictionary> _values = new(() => new HybridDictionary());
	readonly SyncLazy<HybridDictionary> _errors = new(() => new HybridDictionary());

	public ContextLazy(Func<T> factory) : base(factory)
	{
	}

	protected ContextLazy(Func<object, T> factory) : base(factory)
	{
	}
		
	public override T GetValue(object target)
	{
		Throw();									
		return _values.Value.GetOrAdd(InstanceFactory.CurrentContext, _ => HandleValueCreation(target));
	}
		
	protected override T CreateValue(object target)
	{
		ResetError();
		return base.CreateValue(target);
	}

	protected override void Throw()
	{
		if(_errors.IsValueCreated && 
		   _errors.Value.TryGetValue(InstanceFactory.CurrentContext, out object obj) && 
		   obj is ExceptionDispatchInfo exceptionDispatchInfo)
			exceptionDispatchInfo.Throw();
	}

	protected override void ResetError()
		=> _errors.ValueIfCreated?.Remove(InstanceFactory.CurrentContext);

	protected override void OnError(Exception ex) 
		=> _errors.Value[InstanceFactory.CurrentContext] = ExceptionDispatchInfo.Capture(ex);

	public override void SetValue(T value)
	{
		var dict = _values.Value;
		lock (dict.SyncRoot)
		{
			dict[InstanceFactory.CurrentContext] = value;
			ResetError();
		}
	}

	public override TypeEx ValueType => typeof(T);
	public override bool IsValueFaulted => _errors.ValueIfCreated?.Contains(InstanceFactory.CurrentContext) == true;
	public override bool IsValueCreated => _values.ValueIfCreated?.Contains(InstanceFactory.CurrentContext) == true;
	public override T ValueIfCreated => !IsValueCreated ? default : Value;

	public override void Reset() 
		=> Reset(InstanceFactory.CurrentContext);

	public void Reset(string instanceContext)
	{
		var dict = _values.ValueIfCreated;
		if (dict != null)
			lock (dict.SyncRoot)
			{
				dict.Remove(instanceContext);
				ResetError();
			}
		else
			ResetError();
	}

	public void ResetAll()
	{
		_values.Reset();
		_errors.Reset();
	}
		
	public override string ToString() 
		=> !IsValueCreated ? SR.Lazy_ToString_ValueNotCreated : Value.ToString();
		
	public static new ContextLazy<T> Create<TThis>(Func<TThis, T> factory) 
		=> new(o => factory((TThis) o));
}