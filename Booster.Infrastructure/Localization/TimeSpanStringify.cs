using System;
using Booster.Collections;
using Booster.Localization.Pluralization;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Localization;

[PublicAPI]
public static class TimeSpanStringify //TODO: unit test
{
	delegate int ExtractFunc(ref TimeSpan y);
	delegate int ExtractFunc2(ref DateTime dt, DateTime rel);

	static readonly ListDictionary<TimeSpanPart, ExtractFunc?> _extracts = new()
	{
		[TimeSpanPart.Years] = null,
		[TimeSpanPart.Months] = null,
		[TimeSpanPart.Days] = (ref TimeSpan ts) => Extract(ref ts, ts.TotalDays, v => new TimeSpan(v,0,0,0)),
		[TimeSpanPart.Hours] = (ref TimeSpan ts) => Extract(ref ts, ts.TotalHours, v => new TimeSpan(v,0,0)),
		[TimeSpanPart.Minutes] = (ref TimeSpan ts) => Extract(ref ts, ts.TotalMinutes, v => new TimeSpan(0,v,0)),
		[TimeSpanPart.Seconds] = (ref TimeSpan ts) => Extract(ref ts, ts.TotalSeconds, v => new TimeSpan(0,0,v)),
		[TimeSpanPart.Milliseconds] = (ref TimeSpan ts) => Extract(ref ts, ts.TotalMilliseconds, v => new TimeSpan(0,0,0,0,v)),
		[TimeSpanPart.Ticks] = (ref TimeSpan ts) => Extract(ref ts, ts.Ticks),
	};

	static readonly ListDictionary<TimeSpanPart, ExtractFunc2> _extracts2 = new()
	{
		[TimeSpanPart.Years] = (ref DateTime dt, DateTime rel) =>
		{
			var years = ((rel.Year - dt.Year)*12 + rel.Month - dt.Month) / 12;
			dt = dt.AddYears(years);
			return years;
		},
		[TimeSpanPart.Months] = (ref DateTime dt, DateTime rel) =>
		{
			var months = (rel.Year - dt.Year)*12 + rel.Month - dt.Month;
			dt = dt.AddMonths(months);
			return months;
		},
	};

	static int Extract(ref TimeSpan ts, double value, Func<int,TimeSpan>? change = null)
	{
		var res = (int) value;

		if (change != null)
			ts = ts.Subtract(change(res));
		return res;
	}

	static readonly ListDictionary<TimeSpanPart, Func<int, string>> _enParts = new()
	{
		[TimeSpanPart.Years] = i => Plural.En(i, "year", "years"),
		[TimeSpanPart.Months] = i => Plural.En(i, "month", "months"),
		[TimeSpanPart.Days] = i => Plural.En(i, "day", "days"),
		[TimeSpanPart.Hours] = i => Plural.En(i, "hour", "hours"),
		[TimeSpanPart.Minutes] = i => Plural.En(i, "minute", "minutes"),
		[TimeSpanPart.Seconds] = i => Plural.En(i, "second", "seconds"),
		[TimeSpanPart.Milliseconds] = i => Plural.En(i, "millisecond", "milliseconds"),
		[TimeSpanPart.Ticks] = i => Plural.En(i, "tick", "ticks"),
	};

	static readonly ListDictionary<TimeSpanPart, Func<int, RuCase, string>> _ruParts = new() //TODO: Это бы проверить
	{
		[TimeSpanPart.Years] = (i,c) => PluralRu(i,
			() => SelectCase(c, "год", "года", "году", "год", "годом", "годе"),
			() => SelectCase(c, "года", "лет", "годам", "года", "годами", "годах"),
			() => SelectCase(c, "лет", "лет", "годам", "лет", "годами", "годах")),
		[TimeSpanPart.Months] = (i,c) => PluralRu(i,
			() => SelectCase(c, "месяц", "месяца", "месяцу", "месяц", "месяцем", "месяце"),
			() => SelectCase(c, "месяца", "месяцев", "месяцам", "месяца", "месяцами", "месяцах"),
			() => SelectCase(c, "месяцев", "месяцев", "месяцам", "месяцев", "месяцами", "месяцах")),
		[TimeSpanPart.Days] = (i,c) => PluralRu(i,
			() => SelectCase(c, "день", "дня", "дню", "день", "днем", "дне"),
			() => SelectCase(c, "дня", "дней", "дням", "дня", "днем", "дне"),
			() => SelectCase(c, "дней", "дней", "дням", "дней", "днями", "днях")),
		[TimeSpanPart.Hours] = (i,c) => PluralRu(i,
			() => SelectCase(c, "час", "часа", "часу", "час", "часом", "часе"),
			() => SelectCase(c, "часа", "часов", "часам", "часа", "часами", "часах"),
			() => SelectCase(c, "часов", "часов", "часам", "часов", "часами", "часах")),
		[TimeSpanPart.Minutes] = (i,c) => PluralRu(i,
			() => SelectCase(c, "минута", "минуты", "минуте", "минуту", "минутой", "минуте"),
			() => SelectCase(c, "минуты", "минут", "минутам", "минуты", "минутами", "минутах"),
			() => SelectCase(c, "минут", "минут", "минутам", "минут", "минутами", "минутах")),
		[TimeSpanPart.Seconds] = (i,c) => PluralRu(i,
			() => SelectCase(c, "секунда", "секунды", "секунде", "секунду", "секундой", "секунде"),
			() => SelectCase(c, "секунды", "секунд", "секундам", "секунды", "секундами", "секундах"),
			() => SelectCase(c, "секунд", "секунд", "секундам", "секунд", "секундами", "секундах")),
		[TimeSpanPart.Milliseconds] = (i,c) => PluralRu(i,
			() => SelectCase(c, "миллисекунда", "миллисекунды", "миллисекунде", "миллисекунду", "миллисекундой", "миллисекунде"),
			() => SelectCase(c, "миллисекунды", "миллисекунд", "миллисекундам", "миллисекунды", "миллисекундами", "миллисекундах"),
			() => SelectCase(c, "миллисекунд", "миллисекунд", "миллисекундам", "миллисекунд", "миллисекундами", "миллисекундах")),
		[TimeSpanPart.Ticks] = (i,c) => PluralRu(i,
			() => SelectCase(c, "тик", "тика", "тику", "тик", "тиком", "тике"),
			() => SelectCase(c, "тика", "тиков", "тикам", "тика", "тиками", "тиках"),
			() => SelectCase(c, "тиков", "тиков", "тикам", "тиков", "тиками", "тиках")),
	};

	public static string PluralRu(int count, Func<string> one, Func<string> two, Func<string> five)
	{
		var noun = count%10 == 1 && count%100 != 11 ? one() : count%10 >= 2 && count%10 <= 4 && (count%100 < 10 || count%100 >= 20) ? two() : five();
		return string.Join(" ", count, noun);
	}

	static string SelectCase(RuCase ruCase, string nominative, string genitive, string dative, string accusative, string instrumental, string prepositional)
		=> ruCase switch
		   {
			   RuCase.Nominative    => nominative,
			   RuCase.Genitive      => genitive,
			   RuCase.Dative        => dative,
			   RuCase.Accusative    => accusative,
			   RuCase.Instrumental  => instrumental,
			   RuCase.Prepositional => prepositional,
			   _                    => throw new ArgumentOutOfRangeException(nameof(ruCase))
		   };

	public static string ToString(this TimeSpan ts, TimeSpanPart from = TimeSpanPart.Years, TimeSpanPart to = TimeSpanPart.Milliseconds, string separator = ", ", DateTime rel = default, string now = "now") 
		=> ToStringInt(ts, from, to, separator, rel, now, (i, v) => _enParts[i](v));

	public static string ToRuString(this TimeSpan ts, TimeSpanPart from = TimeSpanPart.Years, TimeSpanPart to = TimeSpanPart.Milliseconds, RuCase ruCase = RuCase.Nominative, string separator = ", ", DateTime rel = default, string now = "сейчас") 
		=> ToStringInt(ts, from, to, separator, rel, now, (i, v) => _ruParts[i](v, ruCase));

	static string ToStringInt(TimeSpan ts, TimeSpanPart from, TimeSpanPart to, string separator, DateTime rel, string now, Func<TimeSpanPart, int, string> format)
	{
		var dt = default(DateTime);
		var initDate = true;
		var str = new EnumBuilder(separator, empty: now);
		var single = from > to;
		if (single) (to, from) = (from, to);

		for (var i = from; i <= to; i++)
		{
			int v;
			var ext = _extracts[i];
			if (ext != null)
				v = ext(ref ts);
			else
			{
				if (initDate)
				{
					if (rel == DateTime.MinValue)
						rel = DateTime.Now;
					dt = rel - ts;
					initDate = false;
				}

				var ext2 = _extracts2[i];
				v = ext2(ref dt, rel);
				ts = rel - dt;
			}
			if (v <= 0)
				continue;

			str.Append(format(i, v));

			if (single)
				break;
		}
		return str;
	}
}