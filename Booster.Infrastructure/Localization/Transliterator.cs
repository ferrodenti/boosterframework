﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Booster.Helpers;
using Booster.Parsing;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Localization;

[PublicAPI]
public class Transliterator
{
	class Node
	{
		int _len;
		string? _to;
		Action<StringParser, StringBuilder>? _proc;

		readonly Dictionary<char, Node> _nodes;
		internal readonly bool IgnoreCase;

		bool IsLeaf => _to != null || _proc != null;
		
		public Node(bool ignoreCase)
		{
			IgnoreCase = ignoreCase;
			_nodes = new Dictionary<char, Node>(new CharComparer(ignoreCase));
		}

		public void Add(string from, string? to, Action<StringParser, StringBuilder>? proc)
		{
			var node = BuildNodes(from);
			node._to = to;
			node._proc = proc;
		}

		Node BuildNodes(string from)
		{
			if (string.IsNullOrEmpty(from))
				throw new ArgumentNullException(nameof(from));

			var node = this;
			foreach (var ch in from)
				node = node._nodes.GetOrAdd(ch, _ => new Node(IgnoreCase));
			
			node._len = from.Length;
			return node;
		}

		public void Process(StringParser parser, StringBuilder builder, bool toUpper)
		{
			var ch = parser.Cursor;
			
			parser.Pos += _len;
			
			_proc?.Invoke(parser, builder);

			if (_to != null)
			{
				var to = _to;
				if (toUpper)
					to = to.ToUpper();
				else
					to = char.IsUpper(ch) ? to.ToUpperFirstLetter() : to.ToLower();

				builder.Append(to);
			}

			parser.Dec();
		}

		public bool TryFind(StringParser str, out Node result)
		{
			if (!str.IsEol)
			{
				if (_nodes.TryGetValue(str.Cursor, out var node))
				{
					using var _ = str.Push();
					{
						str.Inc();

						if (node.TryFind(str, out result))
							return true;
					}

					if (node.IsLeaf)
					{
						result = node;
						return true;
					}
				}

				if (IsLeaf)
				{
					result = this;
					return true;
				}
			}
			result = null!;
			return false;
		}
	}

	readonly HashSet<(string from, string? to)> _rules = new();
	readonly Node _root;

	readonly SyncLazy<Transliterator> _backward = SyncLazy<Transliterator>.Create<Transliterator>(
		self =>
		{
			var rules = self._rules
				.Where(r => r.from.IsSome() && r.to.IsSome() && IsRu(r.from))
				.Reverse()
				.ToDictionarySafe(r => r.to!, r => r.from);

			return new Transliterator(rules, self.IgnoreCase);
		});

	public static bool IsRu(string str)
		=> str.All(IsRu);

	public static bool IsRu(char ch)
		=> ch is >= 'а' and <= 'я' or >= 'А' and <= 'Я' or 'ё' or 'Ё';

	public Transliterator Backward => _backward.GetValue(this);
	public bool IgnoreCase => _root.IgnoreCase;
	bool _readOnly;
	Action<StringParser, StringBuilder>? _unknownProc;

	public Transliterator(bool ignoreCase = true)
		=> _root = new Node(ignoreCase);

	public Transliterator(IEnumerable<KeyValuePair<string, string>> rules, bool ignoreCase = true)
		: this(ignoreCase)
	{
		foreach (var rule in rules)
			AddRule(rule.Key, rule.Value);
	}
	
	public Transliterator(IEnumerable<(string, string)> rules, bool ignoreCase = true)
		: this(ignoreCase)
	{
		foreach (var (a, b) in rules)
			AddRule(a, b);
	}

	public Transliterator(Transliterator proto, bool ignoreCase = true)
		: this(ignoreCase)
	{
		foreach (var (from, to) in proto._rules)
			AddRule(from, to);
	}

	internal Transliterator MakeReadOnly()
	{
		_readOnly = true;
		return this;
	}

	public Transliterator Add(string from, string to)
	{
		AddRule(from, to);
		return this;
	}
	
	public Transliterator Add(string from, Action<StringParser, StringBuilder> proc)
	{
		AddRule(from, null, proc);
		return this;
	}
	
	public Transliterator Unknown(Action<StringParser, StringBuilder> unknown)
	{
		_unknownProc = unknown;
		return this;
	}


	[MethodImpl(MethodImplOptions.Synchronized)]
	public bool AddRule(string from, string? to, Action<StringParser, StringBuilder>? proc = null)
	{
		if (_readOnly)
			throw new NotSupportedException("The transliterator is readonly");

		if (_rules.Add((from, to)))
		{
			_root.Add(from, to, proc);

			if (to.IsSome())
				_root.Add(to!, to, null);

			return true;
		}

		return false;
	}

	public string Convert(string? source, bool toUpper = true, string? unknown = null)
		=> Convert(new StringParser(source), toUpper, unknown);
	
	public string ConvertKeepUnknown(string? source, bool toUpper = true)
		=> Convert(new StringParser(source), toUpper, null);

	public string Convert(StringParser parser, bool toUpper = true, string? unknown = "")
	{
		if (parser.IsEmpty)
			return "";

		var builder = new StringBuilder(parser.Rest * 2);

		do
			if (_root.TryFind(parser, out var node))
				node.Process(parser, builder, toUpper);
			else if (!parser.IsEol)
			{
				_unknownProc?.Invoke(parser, builder);

				if (unknown.IsSome())
					builder.Append(unknown);
				else if(unknown == "")
					builder.Append(parser.Cursor);
			}
		while (parser.Inc());

		return builder.ToString();
	}


	public static Transliterator Gost16876 => _gost16876.Value;
	static readonly Lazy<Transliterator> _gost16876 = new(() => new Transliterator(new Dictionary<string, string>
	{
		{"є", "eh"},
		{"ѓ", "g"},
		{"—", "-"},

		{"а", "a"},
		{"б", "b"},
		{"в", "v"},
		{"г", "g"},
		{"д", "d"},
		{"е", "e"},
		{"ё", "jo"},
		{"ж", "zh"},
		{"з", "z"},
		{"и", "i"},
		{"й", "jj"},
		{"к", "k"},
		{"л", "l"},
		{"м", "m"},
		{"н", "n"},
		{"о", "o"},
		{"п", "p"},
		{"р", "r"},
		{"с", "s"},
		{"т", "t"},
		{"у", "u"},
		{"ф", "f"},
		{"х", "kh"},
		{"ц", "c"},
		{"ч", "ch"},
		{"ш", "sh"},
		{"щ", "shh"},
		{"ъ", ""},
		{"ы", "y"},
		{"ь", ""},
		{"э", "eh"},
		{"ю", "yu"},
		{"я", "ya"}
	}).MakeReadOnly());

	public static Transliterator Iso995 => _iso995.Value;
	static readonly Lazy<Transliterator> _iso995 = new(() => new Transliterator(new Dictionary<string, string>
	{
		{"є", "ye"},
		{"ѓ", "g"},
		{"—", "-"},

		{"а", "a"},
		{"б", "b"},
		{"в", "v"},
		{"г", "g"},
		{"д", "d"},
		{"е", "e"},
		{"ё", "yo"},
		{"ж", "zh"},
		{"з", "z"},
		{"и", "i"},
		{"й", "j"},
		{"к", "k"},
		{"л", "l"},
		{"м", "m"},
		{"н", "n"},
		{"о", "o"},
		{"п", "p"},
		{"р", "r"},
		{"с", "s"},
		{"т", "t"},
		{"у", "u"},
		{"ф", "f"},
		{"х", "x"},
		{"ц", "c"},
		{"ч", "ch"},
		{"ш", "sh"},
		{"щ", "shh"},
		{"ъ", ""},
		{"ы", "y"},
		{"ь", ""},
		{"ю", "yu"},
		{"я", "ya"},

		{"э", "e"}, // Чтобы обратный преобразование для русской "е" давало английскую, а не "э"
	}).MakeReadOnly());

	public static Transliterator Pirate => _pirate.Value;
	static readonly Lazy<Transliterator> _pirate = new(() => new Transliterator(new Dictionary<string, string>
	{
		{"Є", "E"},
		{"І", "I"},
		{"Ѓ", "r"},
		{"і", "i"},
		{"є", "e"},
		{"ѓ", "r"},
		{"—", "-"},

		{"А", "A"},
		{"Б", "6"},
		{"В", "B"},
		{"Г", "r"},
		{"Д", "9"},
		{"Ё", "E"},
		{"Е", "E"},
		{"Ж", "}|{"},
		{"З", "3"},
		{"Й", "U"},
		{"И", "U"},
		{"К", "K"},
		{"Л", "JI"},
		{"М", "M"},
		{"Н", "H"},
		{"О", "O"},
		{"П", "n"},
		{"Р", "P"},
		{"С", "C"},
		{"Т", "T"},
		{"У", "Y"},
		{"Ф", "qp"},
		{"Х", "X"},
		{"Ц", "U"},
		{"Ч", "4"},
		{"Ш", "W"},
		{"Щ", "W,"},
		{"Ъ", "'b"},
		{"Ы", "bI"},
		{"Ь", "b"},
		{"Э", "-)"},
		{"Ю", "|-O"},
		{"Я", "9|"},
		{"а", "a"},
		{"б", "6"},
		{"в", "B"},
		{"г", "r"},
		{"д", "9"},
		{"ё", "e"},
		{"е", "e"},
		{"ж", "}|{"},
		{"з", "3"},
		{"й", "u"},
		{"и", "u"},
		{"к", "k"},
		{"л", "JI"},
		{"м", "m"},
		{"н", "H"},
		{"о", "o"},
		{"п", "n"},
		{"р", "p"},
		{"с", "c"},
		{"т", "T"},
		{"у", "y"},
		{"ф", "qp"},
		{"х", "x"},
		{"ц", "u"},
		{"ч", "4"},
		{"ш", "w"},
		{"щ", "w,"},
		{"ъ", "'b"},
		{"ы", "bI"},
		{"ь", "b"},
		{"э", "-)"},
		{"ю", "I-o"},
		{"я", "9|"},
	}, false).MakeReadOnly());
}