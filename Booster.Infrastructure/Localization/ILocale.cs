namespace Booster.Localization;

public interface ILocale
{
	string Language { get; }
	string Localize(string message);
	string Localize(string message, object[] arguments);
}

