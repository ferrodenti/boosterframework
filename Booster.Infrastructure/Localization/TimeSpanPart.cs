namespace Booster.Localization;

public enum TimeSpanPart
{
	None = 0,
	Years,
	Months,
	Days,
	Hours,
	Minutes,
	Seconds,
	Milliseconds,
	Ticks,
}