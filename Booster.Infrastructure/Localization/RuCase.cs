namespace Booster.Localization;

/// <summary>
/// Падеж
/// </summary>
public enum RuCase
{
	/// <summary>
	/// Именительный Кто? Что?
	/// </summary>
	Nominative = 0,
	/// <summary>
	/// Родительный Кого? Чего?
	/// </summary>
	Genitive,
	/// <summary>
	/// Дательный Кому? Чему?
	/// </summary>
	Dative,
	/// <summary>
	/// Винительный Кого? Что? (вижу)
	/// </summary>
	Accusative,
	/// <summary>
	/// Творительный Кем? Чем?
	/// </summary>
	Instrumental,
	/// <summary>
	/// Предложный О ком? О Чем?
	/// </summary>
	Prepositional	
}