﻿using System;

#nullable enable

namespace Booster.Localization.Pluralization;

public class RuPluralizer : BasePluralizer
{
	public RuPluralizer(params string?[] forms)
		: base(FixArray(forms, 3, 2))
	{
	}
	
	public RuPluralizer(params Func<int, string>?[] forms)
		: base(FixArray(forms, 3, 2))
	{
	}

	protected override int GetFormIndex(int count)
		=> count % 10 == 1 && count % 100 != 11
			? 0
			: count % 10 >= 2 && count % 10 <= 4 && (count % 100 < 10 || count % 100 >= 20)
				? 1
				: 2;

	protected override string? GenerateForm(int idx, int count)
		=> idx == 2 ? GetForm(1, count) : null;
}
