﻿#nullable enable

namespace Booster.Localization.Pluralization;

public interface IPluralizer
{
	bool Join { get; set; }
	bool CacheForms { get; set; }
	string Separator { get; set; }
	string? Zero { get; set; }
	
	string Pluralize(int count);
}