﻿using System;
using Booster.Collections;
using Booster.Reflection;

#nullable enable

namespace Booster.Localization.Pluralization;

public class PluralizerFactory
{
	public static readonly PluralizerFactory Instance = new(true);
	
	readonly HybridDictionary<string, TypeEx> _dictionary = new();

	public PluralizerFactory(bool registerStandard)
	{
		if (!registerStandard)
			return;
		
		Register<EnPluralizer>("en");
		Register<RuPluralizer>("ru");
		//Register<DePluralizer>("de");
	}
	
	public PluralizerFactory Register(string code, TypeEx type)
	{
		_dictionary[code.ToLower()] = type;
		return this;
	}

	public PluralizerFactory Register<T>(string code)
		where T : IPluralizer
		=> Register(code, typeof(T));


	public IPluralizer Create(string code, string?[] forms)
		=> Create(code, forms, true)!;
	
	public IPluralizer? Create(string code, string?[] forms, bool thowIfNotFound)
	{
		if (!_dictionary.TryGetValue(code.ToLower(), out var type))
		{
			if (thowIfNotFound)
				throw new InvalidOperationException($"Pluralizer {code} was not registred");

			return null;
		}

		return (IPluralizer)type.Create(new object[] { forms });
	}
}
