﻿using System;

#nullable enable

namespace Booster.Localization.Pluralization;

public class DePluralizer : BasePluralizer
{
	public DePluralizer(params string?[] forms)
		: base(FixArray(forms, 2, 1))
	{
	}
	public DePluralizer(params Func<int, string>?[] forms)
		: base(FixArray(forms, 2, 1))
	{
	}
}
