﻿using System;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Localization.Pluralization;

[PublicAPI]
public abstract class BasePluralizer : IPluralizer
{
	public bool Join { get; set; } = true;
	public bool CacheForms { get; set; } = true;
	public string Separator { get; set; } = " ";
	public string? Zero { get; set; }

	readonly string?[] _forms;
	readonly Func<int, string>?[]? _formGetters;

	protected BasePluralizer(string?[] forms)
		=> _forms = forms;

	protected BasePluralizer(Func<int, string>?[] formGetters)
	{
		_formGetters = formGetters;
		_forms = new string?[formGetters.Length];
	}

	protected string GetForm(int idx, int count)
	{
		var result = _forms[idx];
		if (result != null)
			return result;

		var getter = _formGetters?[idx];
		if (getter != null)
		{
			result = getter(count);
				
			if (result != null)
				return result;
		}

		result ??= GenerateForm(idx, count);

		if (result != null)
		{
			if (CacheForms)
				_forms[idx] = result;

			return result;
		}

		throw new Exception($"Failed to retrive form, idx={idx}");
	}

	protected virtual string? GenerateForm(int idx, int count)
		=> null;

	protected virtual int GetFormIndex(int count)
		=> count == 1 ? 0 : 1;

	public string Pluralize(int count)
	{
		if (count == 0 && Zero != null)
			return Zero;
		
		var idx = GetFormIndex(count);
		var form = GetForm(idx, count);

		if (Join)
			return string.Join(Separator, count, form);

		return form;
	}

	protected static T?[] FixArray<T>(T[] array, int length, int minimum)
	{
		var len = array.Length;
		if (len == length)
			return array;
		
		if (len < minimum)
			throw new Exception($"Expected at least {minimum} elements");

		var result = new T?[length];
		for (var i = 0; i < len; ++i)
			result[i] = array[i];

		return result;
	}
}
