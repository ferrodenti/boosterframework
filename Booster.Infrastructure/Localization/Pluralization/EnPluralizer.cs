﻿using System;
using System.Collections.Generic;
using System.Linq;
using Booster.Helpers;

#nullable enable

namespace Booster.Localization.Pluralization;

public class EnPluralizer : BasePluralizer 
{
	static readonly HashSet<string> _unpluralizables = new(StringComparer.OrdinalIgnoreCase)
	{
		"equipment",
		"information",
		"rice",
		"money",
		"species",
		"series",
		"fish",
		"sheep",
		"deer",
		"aircraft",
		"trout",
		"swine"
	};
	
	// ReSharper disable ArrangeObjectCreationWhenTypeNotEvident
	static readonly List<ReplaceRegex> _pluralizations = new()
	{
		// Start with the rarest cases, and move to the most common
		new("media$", "mediae"),
		new("person$", "people"),
		new("^ox$", "oxen"),
		new("^criterion$", "criteria"),
		new("child$", "children"),
		new("foot$", "feet"),
		new("tooth$", "teeth"),
		new("^goose$", "geese"),
		// And now the more standard rules.
		new("(.*[^af])fe?$", "$1ves"), // ie, wolf, wife, but not giraffe, gaffe, safe
		new("(hu|talis|otto|Ger|Ro|brah)man$", "$1mans"), // Exceptions for man -> men
		new("(.*)man$", "$1men"),
		new("(.+[^aeiou])y$", "$1ies"),
		new("(.+zz)$", "$1es"), // Buzz -> Buzzes
		new("(.+z)$", "$1zes"), // Quiz -> Quizzes
		new("([m|l])ouse$", "$1ice"),
		new("(append|matr|ind)(e|i)x$", @"$1ices"), // ie, Matrix, Index
		new("(octop|vir|radi|fung)us$", "$1i"),
		new("(phyl|milleni|spectr)um$", "$1a"),
		new("(cris|ax)is$", @"$1es"),
		new("(.+(s|x|sh|ch))$", @"$1es"),
		new("(.+)ies$", @"$1ies"),
		new("(.+)", @"$1s")
	};	
	// ReSharper restore ArrangeObjectCreationWhenTypeNotEvident
	
	public EnPluralizer(params string?[] forms)
		: base(FixArray(forms, 2, 1))
	{
	}

	public EnPluralizer(params Func<int, string>?[] forms)
		: base(FixArray(forms, 2, 1))
	{
	}

	protected override string? GenerateForm(int idx, int count)
	{
		if (idx != 1)
			return null;

		var singular = GetForm(0, count);

		if (_unpluralizables.Contains(singular))
			return singular;

		foreach(var replace in _pluralizations)
			if (replace.TryReplace(singular, out var result))
				return result;

		return $"{singular}s";
	}
	
	public static string PluralizeCamelCase(string str, string separator = "", int count = 2)
	{
		if (count == 1)
			return str;

		var words = str.SplitCamelCase().ToArray();
		var cnt = words.Length - 1;

		var res = new EnumBuilder(separator);
		for (var i = 0; i <= cnt; i++)
		{
			var word = words[i];
			if (i == cnt)
				word = new EnPluralizer(word)
				{
					Join = false
				}.Pluralize(count);
			
			res.Append(word);
		}

		return res;
	}
}
