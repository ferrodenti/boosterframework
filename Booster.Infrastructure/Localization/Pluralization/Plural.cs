using System;

#nullable enable

namespace Booster.Localization.Pluralization;

public static class Plural
{
	public static string Ru(
		int count, 
		string one, 
		string two, 
		string? five = null, 
		bool nounOnly = false, 
		string separator = " ")
		=> new RuPluralizer(one, two, five)
		{
			Join = !nounOnly, 
			Separator = separator,
			CacheForms = false
		}.Pluralize(count);

	public static string Ru(
		int count, 
		Func<int, string> one, 
		Func<int, string> two, 
		Func<int, string>? five = null, 
		bool nounOnly = false,
		string separator = " ")
		=> new RuPluralizer(one, two, five)
		{
			Join = !nounOnly, 
			Separator = separator,
			CacheForms = false
		}.Pluralize(count);

	public static string En(
		int count,
		string one,
		string? many = null,
		bool nounOnly = false,
		string separator = " ")
		=> new EnPluralizer(one, many)
		{
			Join = !nounOnly,
			Separator = separator,
			CacheForms = false
		}.Pluralize(count);

	public static string En(
		int count,
		Func<int, string> one, 
		Func<int, string>? many = null,
		bool nounOnly = false,
		string separator = " ")
		=> new EnPluralizer(one, many)
		{
			Join = !nounOnly,
			Separator = separator,
			CacheForms = false
		}.Pluralize(count);

	public static string De(
		int count,
		string one,
		string? many = null,
		bool nounOnly = false,
		string separator = " ")
		=> new DePluralizer(one, many)
		{
			Join = !nounOnly,
			Separator = separator,
			CacheForms = false
		}.Pluralize(count);
}