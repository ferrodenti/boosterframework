using Booster.DI;

namespace Booster.Localization;

[InstanceFactory(typeof(ILocale))]
public class BasicLocale : ILocale
{
	public string Language { get; }

	public BasicLocale(string language)
		=> Language = language;

	public string Localize(string message)
		=> null;

	public string Localize(string message, object[] arguments)
		=> null;
}