using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

#nullable enable

namespace Booster.Parsing;

[DebuggerDisplay($"{{{nameof(DebuggerDisplay)}()}}")]
public class StringParser : ICloneable
{
	public int Pos { get; set; }
	readonly string _str;

	public bool IgnoreCase { get; set; }
	public int LineNum { get; }

	public bool IsEol => Pos >= _str.Length;

	public char Cursor => IsEol ? (char)0 : _str[Pos];

	public int Length => _str.Length;
	public int Rest => Length - Pos;
	public bool IsEmpty => _str.IsEmpty();
	public bool IsSome => _str.IsSome();

	public string RestString => _str.SubstringSafe(Pos);
	
	public char this[int idx] => idx < Length ? _str[idx] : '\0';

	public StringParser (string? str)
		=> _str = str ?? "";
	
	public StringParser(string? str, int lineNum)
	{
		_str = str ?? "";
		LineNum = lineNum;
	}

	public StringParser (StringParser? proto)
	{
		if (proto != null)
		{
			_str = proto._str;
			Pos = proto.Pos;
			IgnoreCase = proto.IgnoreCase;
			LineNum = proto.LineNum;
		}
		else
			_str = "";
	}

	protected string DebuggerDisplay()
		=> ToString();
	
	public IDisposable Set(int pos)
	{
		var p = Push();
		Pos = pos;
		return p;
	}

	public bool Inc()
	{
		Pos++;
		return !IsEol;
	}

	public bool Dec()
	{
		Pos--;
		return !IsEol;
	}

	public IDisposable Push()
	{
		var prev = Pos;
		return new ActionDisposable(() => Pos = prev);

	}
	public void SkipSpaces()
	{
		while (!IsEol && Cursor == ' ' || Cursor == '\t' || Cursor == '\r' || Cursor == '\n')
			Inc();
	}

	public string GetSpaces()
	{
		var str = new StringBuilder();

		while (!IsEol && Cursor == ' ' || Cursor == '\t' || Cursor == '\r' || Cursor == '\n')
		{
			str.Append(Cursor);
			Inc();
		}
		return str.ToString();
	}


	public string? GetToken()
	{
		var str = new StringBuilder();

		while (!IsEol && (Cursor == '_' || char.IsLetterOrDigit(Cursor)))
		{
			str.Append(Cursor);
			Pos++;
		}
		return str.Length > 0 ? str.ToString() : null;
	}

	public string? GetToken(string additionalChar)
	{
		var str = new StringBuilder();

		while (!IsEol)
			if (Cursor == '_' || char.IsLetterOrDigit(Cursor))
			{
				str.Append(Cursor);
				Pos++;
			}
			else if (Is(additionalChar))
				str.Append(additionalChar);
			else
				break;

		return str.Length > 0 ? str.ToString() : null;
	}
		
	// ReSharper disable once ParameterTypeCanBeEnumerable.Global
	public string? GetToken(string[] additionalChars)
	{
		var str = new StringBuilder();

		while (!IsEol)
		{
			var br = true;
			if (Cursor == '_' || char.IsLetterOrDigit(Cursor))
			{
				str.Append(Cursor);
				Pos++;
				continue;
			}
			foreach (var s in additionalChars)
				if (Is(s))
				{
					str.Append(s);
					br = false;
					break;
				}

			if (br)
				break;
		}
		return str.Length > 0 ? str.ToString() : null;
	}
	// ReSharper disable StringIndexOfIsCultureSpecific.2
	public string? SearchFor(string value, string escape)
	{
		if (IsEol)
			return null;

		var res = new StringBuilder();
		var i = Pos;

		do
		{
			var k = _str.IndexOf(escape, i);
			var l = _str.IndexOf(value, i);

			if (l < 0)
				return null;

			if (k >= 0 && k <= l)
			{
				res.Append(_str.Substring(i, k - i));
				res.Append(value);
				i = k + escape.Length;
				continue;
			}

			res.Append(_str.Substring(i, l - i));
			Pos = l + 1;
			return res.ToString();

		} while (i < _str.Length);

		return null;
	}

	public IEnumerable<Tuple<string, string>> SearchFor(params string[] values)
	{
		var dict = values.ToDictionary(p => p, _ => -1);
		var from = 0;
		while (!IsEol)
		{
			var min = int.MaxValue;
			string? res = null;

			foreach (var pair in dict.ToArray())
				if (pair.Value < 0 || pair.Value < Pos)
				{
					var i = _str.IndexOf(pair.Key, Pos);
					if (i < 0)
						dict.Remove(pair.Key);
					else
					{
						dict[pair.Key] = i;
						if (i < min)
						{
							min = i;
							res = pair.Key;
						}
					}
				}
				else if (pair.Value < min)
				{
					min = pair.Value;
					res = pair.Key;
				}

			if (res != null)
			{
				Pos = min + res.Length;
				yield return Tuple.Create(res, _str.Substring(from, min - from));
				from = Pos;
			}
			else
				yield break;
		}
	}

	public ParsingState SearchFor(params Term[] tokensActions)
	{
		var dict = tokensActions
			.SelectMany(t => t.Values.Select(v => (v, t)))
			.ToDictionary(tup => tup.v, tup => tup.t);

		foreach (var (token, prev) in SearchFor(dict.Keys.ToArray())) //TODO: use SafeAny
			if (!dict[token].ShouldContinueParsing(new ParsingState(this, token, prev)))
				return new ParsingState(this, null, prev) { Stop = true };

		return new ParsingState(this, null, null);
	}

	public bool Is(char value, bool move = true)
	{
		if (IsEol)
			return false;

		bool res;
		if (IgnoreCase)
			res = char.ToLower(Cursor) == char.ToLower(value);
		else
			res = Cursor == value;

		if(res && move)
			Inc();

		return res;
	}
		
	public bool Is(string value, bool whole = false, bool move = true)
	{
		var isLetter = true;

		if (IgnoreCase)
			value = value.ToLower();

		var p = Pos;
		for (var i = 0; i < value.Length; i++, p++)
		{
			if (p >= _str.Length)
				return false;

			if ((IgnoreCase ? char.ToLower(_str[p]) : _str[p]) != value[i])
				return false;

			if (whole && !char.IsLetter(value[i]))
				isLetter = false;
		}

		if (whole && isLetter && p < _str.Length && char.IsLetter(_str[p]))
			return false;

		if (move)
			Pos = p;

		return true;
	}

	public ParsingException CreateException(string message) 
		=> CreateException(Pos, message);

	public ParsingException CreateException(int pos, string message)
	{
		int i = 0, j = 0, line = 0;
		while (i < pos)
		{
			j = _str.IndexOf('\n', i);
			if (j < 0)
			{
				j = _str.Length;
				break;
			}
			if (j > pos)
				break;

			i = j + 1;
			line++;
		}

		//int chr = pos - i;
		//string str = _str.Substring(i, j - i);

		return new ParsingException(message)
		{
			Line = line,
			Char = pos - i,
			ErrorString = _str.Substring(i, j - i).Replace("\r", "")
		};
	}

	internal string Substring(int start)
		=> _str.SubstringSafe(start);
	
	internal string Substring(int start, int count)
		=> _str.SubstringSafe(start, count);
	
	public override string ToString()
		=> IsEol ? $"]{_str}[" : $"{_str.SubstringSafe(0, Pos)}[{Cursor}]{_str.SubstringSafe(Pos + 1)}";

	public StringParser Clone()
		=> new(this);

	object ICloneable.Clone()
		=> Clone();

	public static string operator -(StringParser a, StringParser b)
		=> a.Substring(b.Pos, a.Pos - b.Pos);

	public static string operator +(StringParser a, StringParser b)
		=> a.Substring(a.Pos, b.Pos + a.Pos);

	public static StringParser operator +(StringParser a, int b)
		=> new(a) { Pos = a.Pos + b };
	
	public static StringParser operator -(StringParser a, int b)
		=> new(a) { Pos = a.Pos - b };

	public static StringParser operator ++(StringParser a)
	{
		a.Inc();
		return a;
	}
	
	public static StringParser operator --(StringParser a)
	{
		a.Dec();
		return a;
	}
}