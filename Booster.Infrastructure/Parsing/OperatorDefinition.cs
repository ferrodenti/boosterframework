using System;
using Booster.Parsing.Operators;

namespace Booster.Parsing;

public class OperatorDefinition
{
	public int Prioriry { get; set; }
	public string Token { get; set; }
	public bool IsRightAssociative { get; set; }
	public bool PreOp { get; set; }

	public Func<BaseOperator> OperatorCtor { get; set; }

	public override string ToString()
		=> Token;
}