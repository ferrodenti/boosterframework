using System;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Parsing;

[PublicAPI]
public class ParsingException : Exception
{
	public string? ErrorString { get; set; }
	public int Line { get; set; }
	public int Char { get; set; }

	public ParsingException() { }
	public ParsingException(string errorString, string message) : base(message) => ErrorString = errorString;
	public ParsingException(string message) : base(message) { }
	public ParsingException(string message, Exception innerException) : base(message, innerException) { }
}