using System;
using JetBrains.Annotations;

namespace Booster.Parsing;

public class SyntaxTree
{
	
}

public class Term
{
	public string[] Values { get; }
	public Action<ParsingState> Action { get; private set; }

	public Term(string value, [InstantHandle] Func<string, bool> action)
		: this(value)
		=> Action = state => state.Stop = !action(state.Buffer!);
	
	public Term(string value, [InstantHandle] Action<ParsingState> action)
		: this(value)
		=> Action = action;

    public Term(string value, bool @continue)
        : this(value)
    {
        if (@continue)
            Action = s => s.Skip = true;
        else
            Action = s => s.Stop = true;
    }

	public Term(params string[] values)
		=> Values = values;

	public Term Handle([InstantHandle] Action<ParsingState> action)
	{
		Action = action;
		return this;
	}
	
	public Term HandleQuotes([InstantHandle] Action<ParsingState> action)
	{
		Action = s =>
        {
            var start = s.Parser.Clone();
			
            s.Parser.SearchFor(
				new Term($"\\{s.Token}", true),
				new Term(s.Token, false)
			).HandleRest(_ => throw new ParsingException("Reached EOL while looking for closing quote"));

			var value = s.Parser - 1 - start;

			action(new ParsingState(s.Parser, value, s.Buffer) { Tag = s.Token });
		};
		return this;
	}
    

	public bool ShouldContinueParsing(ParsingState parsingState)
	{
        if (Action != null)
        {
            parsingState.Stop = false;
		
            Action(parsingState);

            if (parsingState.Stop)
                return false;

            parsingState.Stop = false;
        }

        return true;
	}
}