using Booster.Helpers;
using JetBrains.Annotations;

namespace Booster.Parsing;

/// <summary>
/// Шаблон поиска — метод описания поискового запроса с использованием метасимволов (символов-джокеров).
/// </summary>
public static class Wildcards
{
	public static bool IsWildcard([CanBeNull] string mask)
		=> mask?.IndexOfAny(new[] { '*', '?' }) >= 0;

	/// <summary>
	/// Сравнивает строку с маской с учетом регистра
	/// </summary>
	/// <param name="source">Строка для сравнения с маской</param>
	/// <param name="mask">Маска, где "*" - ноль или больше символов, "?" - любой символ(один)</param>
	/// <param name="ignoreCase">Игнорировать регистр</param>
	/// <returns></returns>
	public static bool Compare(string source, string mask, bool ignoreCase = true)
	{
		var comp = new CharComparer(ignoreCase);

		int iSource = 0, iMask = 0;
		int mp = 0, sp = 0;

		for (; iSource < source.Length && iMask < mask.Length && mask[iMask] != '*'; iSource++, iMask++)
			if (!comp.IsEqual(mask[iMask], source[iSource]) && mask[iMask] != '?')
				return false;

		while (iSource < source.Length)
		{
			if (iMask >= mask.Length)
				return false;

			if (mask[iMask] == '*')
			{
				if (++iMask >= mask.Length)
					return true;

				mp = iMask;
				sp = iSource + 1;
			}
			else if (comp.IsEqual(mask[iMask], source[iSource]) || mask[iMask] == '?')
			{
				iMask++;
				iSource++;
			}
			else
			{
				iMask = mp;
				iSource = sp++;
			}
		}

		for (; iMask < mask.Length; iMask++)
			if (mask[iMask] != '*')
				return false;

		return true;
	}
}