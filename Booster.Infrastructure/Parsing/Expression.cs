using Booster.Parsing.Operators;
using JetBrains.Annotations;

namespace Booster.Parsing;

[PublicAPI]
public class Expression
{
	public BaseOperator Operator { get; private set; }

	public Expression(BaseOperator @operator)
		=> Operator = @operator;
}