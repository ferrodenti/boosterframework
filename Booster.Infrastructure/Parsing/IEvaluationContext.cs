namespace Booster.Parsing;

public interface IEvaluationContext
{
	bool CanRetriveParameters { get; }

	object GetVariable(string name);
	object GetParameter(string name);
}