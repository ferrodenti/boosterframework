using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using Booster.Parsing.Operators;
using JetBrains.Annotations;

namespace Booster.Parsing;

[PublicAPI]
public class ExpressionParser
{
	StringComparer _stringComparer;
	protected StringComparer StringComparer => _stringComparer ??= IgnoreCase ? StringComparer.OrdinalIgnoreCase : StringComparer.Ordinal;

	NumberFormatInfo _numberFormatInfo;
	protected NumberFormatInfo NumberFormatInfo => _numberFormatInfo ??= new NumberFormatInfo {NumberDecimalSeparator = DecimalPoint};

	bool _ignoreCase = true;

	public bool IgnoreCase
	{
		get => _ignoreCase;
		set
		{
			if (_ignoreCase != value)
			{
				_ignoreCase = value;
				_stringComparer = null;
			}
		}
	}

	public string ParameterPrefix { get; set; }
	public Func<string, bool> IsParameterProc { get; set; }
		
	public string ObjectAccessor { get; set; }
	public StringEscaperRule VariableEscaping { get; set; }

	public List<StringEscaper> StringEscapings { get; } = new()
	{
		new StringEscaper(new StringEscaperRule("\"", "\\\""))
	};

	string _decimalPoint = ".";
	public string DecimalPoint
	{
		get => _decimalPoint;
		set
		{
			if (_decimalPoint != value)
			{
				_decimalPoint = value;
				_numberFormatInfo = null;
			}
		}
	}

	public ObservableCollection<OperatorDefinition> Operators { get; }

	public Dictionary<string, string> Synonyms => _synonyms ??= new Dictionary<string, string>(StringComparer);
	Dictionary<string, string> _synonyms;

	protected OperatorDefinition[] PostOperators => _postOperators ??= Operators.Where(op => !op.PreOp).OrderByDescending(op => op.Prioriry).ToArray();
	OperatorDefinition[] _postOperators;

	protected OperatorDefinition[] PreOperators => _preOperators ??= Operators.Where(op => op.PreOp).OrderByDescending(op => op.Prioriry).ToArray();
	OperatorDefinition[] _preOperators;

	public HashSet<string> ReservedWords => _reservedWords ??= new HashSet<string>(Operators.Select(op => op.Token), StringComparer);
	HashSet<string> _reservedWords;

	public Dictionary<string, object> SpecialConstants => _specialConstants ??= new Dictionary<string, object>(StringComparer);
	Dictionary<string, object> _specialConstants;

	public ExpressionParser() : this(Enumerable.Empty<OperatorDefinition>())
	{
	}

	public ExpressionParser(IEnumerable<OperatorDefinition> operators)
	{
		Operators = new ObservableCollection<OperatorDefinition>(operators);
		Operators.CollectionChanged += Operators_CollectionChanged;
	}

	public void AddOperator<TOperator>(int priority, string token, bool preOp = false, bool isRightAssociative = false) where TOperator : BaseOperator, new()
		=> AddOperator(new OperatorDefinition
		{
			Prioriry = priority,
			Token = token,
			OperatorCtor = () => new TOperator(),
			IsRightAssociative = isRightAssociative,
			PreOp = preOp
		});

	public void AddOperator(OperatorDefinition definition)
		=> Operators.Add(definition);

	void Operators_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
	{
		_postOperators = null;
		_preOperators = null;
		_reservedWords = null;
	}

	public BaseOperator Parse(string expression)
	{
		var parser = new StringParser(expression) {IgnoreCase = IgnoreCase};
		return Parse(parser);
	}

	protected BaseOperator Parse(StringParser parser, int brackets = 0, int priority = -1)
	{
		BaseOperator tmp = null;
		return Parse(parser, brackets, priority, ref tmp);
	}

	protected BaseOperator Parse(StringParser parser, int brackets, int priority, ref BaseOperator nextOp)
	{
		BaseOperator op = null;

		while (!parser.IsEol)
		{
			parser.SkipSpaces();

			if (parser.IsEol)
				break;

			if (parser.Cursor == ')')
			{
				if (brackets <= 0)
					throw parser.CreateException("Unexpected closing bracket");

				if (op == null)
					throw parser.CreateException("Unexpected expression");

				break;
			}

			if (parser.Is('('))
			{
				if (op != null)
					throw parser.CreateException("Unexpected operator");

				op = Parse(parser, brackets + 1);
				parser.Inc();
				continue;
			}

			if (op == null)
			{
				if (TryParseOperator(parser, PreOperators, out op))
				{
					BaseOperator nxt = null;
					op.AddOperand(parser, Parse(parser, brackets, op.Priority, ref nxt));
					nextOp = nxt;
					//continue;
				}
				else
				{
					op = ParseOperand(parser);
					continue;
				}
			}

			var op2 = nextOp;
			if (op2 == null && !TryParseOperator(parser, PostOperators, out op2))
				throw parser.CreateException("Expected operator");

			if (priority < 0 || op2.Priority > priority || op2.Definition.IsRightAssociative)
			{
				nextOp = null;
				op2.AddOperand(parser, op);
				op2.AddOperand(parser, Parse(parser, brackets, op2.Priority, ref nextOp));
				op = op2;
				continue;
			}

			nextOp = op2;
			break;
		}

		return op;
	}

	protected bool TryParseOperator(StringParser parser, IEnumerable<OperatorDefinition> operators, out BaseOperator op)
	{
		var def = operators.FirstOrDefault(o => parser.Is(o.Token, true));

		if (def != null)
		{
			op = def.OperatorCtor();
			op.Definition = def;

			return true;
		}

		op = null;
		return false;
	}

	protected BaseOperator ParseOperand(StringParser parser)
	{
		if (ParameterPrefix != null && parser.Is(ParameterPrefix))
		{
			var str = parser.GetToken();
			if (str == null)
				throw parser.CreateException("Expected parameter name");

			return new ParameterOperator(str);
		}

		foreach (var esc in StringEscapings)
		{
			var rule = esc.Rules.FirstOrDefault();
			if (rule != null && parser.Is(rule.From))
			{
				var str = parser.SearchFor(rule.From, rule.To);
				if (str == null)
					throw parser.CreateException("Reached end of Line, while looking for a closing quote");

				str = esc.Unescape(str);
				return new ConstantOperator<string>(str);
			}
		}

		if (VariableEscaping != null && parser.Is(VariableEscaping.From))
		{
			var str = parser.SearchFor(VariableEscaping.From, VariableEscaping.To);
			if (str == null)
				throw parser.CreateException($"Reached end of Line, while looking for a '{VariableEscaping.From}'");

			return new VariableOperator(str);
		}

		string token;

		if (ObjectAccessor == null || ObjectAccessor == DecimalPoint)
			token = parser.GetToken(DecimalPoint);
		else
			token = parser.GetToken(new[] {DecimalPoint, ObjectAccessor});

		if (token == null)
			throw parser.CreateException("Expected token");
			
		if (Synonyms.TryGetValue(token, out var tmp))
			token = tmp;

		if (ReservedWords.Contains(token))
			throw parser.CreateException($"Unexpected identifier: {token}");

		if (SpecialConstants.TryGetValue(token, out var @const))
		{
			var type = @const?.GetType() ?? typeof(object);
			type = typeof(ConstantOperator<>).MakeGenericType(type);
			return (BaseOperator) Activator.CreateInstance(type, @const);
		}
			
		if (IsParameterProc?.Invoke(token) == true)
			return new ParameterOperator(token);
			
		if (token[0] == '_' || char.IsLetter(token[0]))
			return new VariableOperator(token);

		if (int.TryParse(token, out var i))
			return new ConstantOperator<int>(i);

		if (double.TryParse(token, NumberStyles.Number, NumberFormatInfo, out var dbl))
			return new ConstantOperator<double>(dbl);

		throw parser.CreateException($"Unexpected identifier: {token}");
	}

	public static ExpressionParser CreateCsParser()
	{
		var parser = new ExpressionParser();

		parser.AddOperator<AndOperator>(0, "&&");
		parser.AddOperator<OrOperator>(0, "||");
		parser.AddOperator<XorOperator>(0, "^");
		parser.AddOperator<NotOperator>(0, "!", true);

		parser.AddOperator<EqualsOperator>(1, "==");
		parser.AddOperator<NotEqualsOperator>(1, "!=");
		parser.AddOperator<LessOrEqualOperator>(1, "<=");
		parser.AddOperator<GreaterOrEqualOperator>(1, ">=");
		parser.AddOperator<LessOperator>(1, "<");
		parser.AddOperator<GreaterOperator>(1, ">");

		parser.AddOperator<NegOperator>(2, "-", true);

		parser.AddOperator<SumOperator>(3, "+");
		parser.AddOperator<SubOperator>(3, "-");

		parser.AddOperator<MulOperator>(4, "*");
		parser.AddOperator<DivOperator>(4, "/");

		parser.SpecialConstants["null"] = null;
		parser.SpecialConstants["true"] = true;
		parser.SpecialConstants["false"] = false;

		return parser;
	}
}