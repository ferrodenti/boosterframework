using System;
using System.Collections.Generic;
using System.Linq;

namespace Booster.Parsing;

public interface IArgumentsCollection
{
	CommandLineArgument AddOptionalArgument(string[] keys, 
		string help = null, 
		string name = null, 
		Type type = null,
		Func<object, string> func = null,
		string[] choices = null,
		bool countKeys = false,
		Box<object> defaultValue = null,
		DefaultValueHandling showDefaultValue = DefaultValueHandling.ShowNonDefault);
}
	
public static class ArgumentsCollectionExtensions
{
	public static CommandLineArgument AddOptionalArgument<T>(this IArgumentsCollection collection, string keys, string help = null, string name = null, 
		Func<T, string> func = null, 
		IEnumerable<T> choices = null, 
		bool countKeys = false,
		Box<object> defaultValue = null,
		DefaultValueHandling showDefaultValue = DefaultValueHandling.ShowNonDefault)
		=> collection.AddOptionalArgument(keys, help, name, typeof(T),
			func != null ? o => func((T)o) : null, 
			choices?.Select(c => c.ToString()).ToArray(), 
			countKeys, defaultValue, showDefaultValue);

	public static CommandLineArgument AddOptionalArgument(this IArgumentsCollection collection, string keys, string help = null, string name = null, Type type = null,
		Func<object, string> func = null, 
		string[] choices = null, 
		bool countKeys = false,
		Box<object> defaultValue = null,
		DefaultValueHandling showDefaultValue = DefaultValueHandling.ShowNonDefault)
		=> collection.AddOptionalArgument(keys.SplitNonEmpty(' '), help, name, type, func, choices, countKeys, defaultValue, showDefaultValue);
		
	public static CommandLineArgument AddOptionalArgument<T>(this IArgumentsCollection collection, string[] keys, string help = null, string name = null, 
		Func<T, string> func = null,
		IEnumerable<T> choices = null, 
		bool countKeys = false,
		Box<object> defaultValue = null,
		DefaultValueHandling showDefaultValue = DefaultValueHandling.ShowNonDefault)
		=> collection.AddOptionalArgument(keys, help, name, typeof(T),
			func != null ? o => func((T)o) : null, 
			choices?.Select(c => c.ToString()).ToArray(), 
			countKeys, defaultValue, showDefaultValue);
}