namespace Booster.Parsing;

public enum DefaultValueHandling
{
	Hide = 0,
	ShowNonDefault,
	Show
}