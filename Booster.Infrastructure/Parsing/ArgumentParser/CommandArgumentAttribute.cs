using System;
using JetBrains.Annotations;

namespace Booster.Parsing;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Method), MeansImplicitUse]
public class CommandArgumentAttribute : Attribute
{
	public string Keys { get; set; }
	public string Name { get; set; }
	public string Help { get; set; }
	public bool IsPositional { get; set; }
	public bool IsOptional { get; set; }
	public object Group { get; set; }

	Box<object> _defaultValue;
	public object DefaultValue
	{
		get => _defaultValue?.Value;
		set => _defaultValue = value;
	}

	public DefaultValueHandling ShowDefaultValue { get; set; } = DefaultValueHandling.ShowNonDefault; 
		
	public CommandArgumentAttribute(bool isPositional, string keys, string help, string name, bool isOptional)
	{
		Keys = keys;
		Name = name;
		Help = help;
		IsPositional = isPositional;
		IsOptional = isOptional;
	}

	public Box<object> GetDefaultValueBox()
		=> _defaultValue;
}