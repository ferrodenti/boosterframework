using System;
using Booster.Reflection;

namespace Booster.Parsing;

public class CommandLineArgument
{
	public string[] Keys { get; }
	public string Name { get; }
	public Type Type { get; set; }
	public bool CountKeys { get; set; }
	public string Help { get; set; }
	public string[] Choices { get; set; }
	public bool IsOptional { get; set; }
		
	public Box<object> DefaultValue { get; set; }
	public DefaultValueHandling ShowDefaultValue { get; set; }

	Box<string> _typeName;
	public string TypeName
	{
		get
		{
			if (_typeName == null)
			{
				if (Type == null)
					_typeName = new Box<string>();
				else
				{

					var type = TypeEx.Get(Type);
					_typeName = type.BuildInName ?? type.Name;
				}
			}

			return _typeName.Value;
		}
	}
		
	Box<string> _parameterName;

	public string ParameterName => (_parameterName ??= new Box<string>(Type != typeof(bool) && !CountKeys
		? Choices?.ToString(new EnumBuilder(",", "{", "}")) ?? Name.ToUpper()
		: null)).Value;

	public Func<object, string> Proc { get; set; }
		
	public BaseMember Member { get; set; }
	public MutuallyExclusiveGroup Group { get; set; }
		
	public int NArgs { get; set; }

	public CommandLineArgument(string[] keys, string name, Type type, string help, bool countKeys, bool isOptional = true, 
		Func<object, string> proc = null, 
		string[] choices = null,
		Box<object> defaultValue = null,
		DefaultValueHandling showDefaultValue = DefaultValueHandling.ShowNonDefault)
	{
		Keys = keys;
		Name = name;
		Type = type;
		Help = help;
		CountKeys = countKeys;
		IsOptional = isOptional;
		Choices = choices;
		Proc = proc;
		DefaultValue = defaultValue;
		ShowDefaultValue = showDefaultValue;
		NArgs = name.SplitNonEmpty(' ').Length;
	}

	public override string ToString()
		=> Keys != null
			? Keys.ToString(k => k, new EnumBuilder("/"))
			: Name;
}