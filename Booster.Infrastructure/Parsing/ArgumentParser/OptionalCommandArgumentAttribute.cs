using JetBrains.Annotations;

namespace Booster.Parsing;

[MeansImplicitUse]
public class OptionalCommandArgumentAttribute : CommandArgumentAttribute
{
	public OptionalCommandArgumentAttribute(string keys, string help = null, string name = null) 
		: base(false, keys, help, name,  true)
	{
	}
}