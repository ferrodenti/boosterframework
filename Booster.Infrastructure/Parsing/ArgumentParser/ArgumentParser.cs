using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Booster.DI;
using Booster.Helpers;
using Booster.Localization;
using Booster.Localization.Pluralization;
using Booster.Reflection;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Parsing;

[PublicAPI]
public class ArgumentParser : IArgumentsCollection
{
	string? _executableName;
	public string ExecutableName
	{
		get
		{
			if (_executableName == null)
			{
				var path = System.Diagnostics.Process.GetCurrentProcess().MainModule?.FileName;
					
				_executableName = path != null 
					? Path.GetFileName(path) 
					: AppDomain.CurrentDomain.FriendlyName;
			}

			return _executableName;
		}
		set
		{
			if (_executableName != null)
			{
				_executableName = value;
				_help = null;
				_usage = null;
			}
		}
	}

	string? _description;
	public string? Description
	{
		get => _description;
		set
		{
			if (_description != value)
			{
				_description = value;
				_help = null;
			}
		}
	}

	bool _ignoreCase = true;
	public bool IgnoreCase
	{
		get => _ignoreCase;
		set
		{
			if (_ignoreCase != value)
			{
				static Dictionary<TKey, TValue>? UpdateDict<TKey, TValue>(Dictionary<TKey, TValue>? src, IEqualityComparer<TKey> comparer)
				{
					if (src == null)
						return null;

					var result = new Dictionary<TKey, TValue>(comparer);
					result.AddRange(src);
					return result;
				}

				_ignoreCase = value;

				_positionalArguments = UpdateDict(_positionalArguments, StringComparer)!;
				_optionalArgumentsByKeys = UpdateDict(_optionalArgumentsByKeys, StringComparer);
				_optionalArgumentsByNames = UpdateDict(_optionalArgumentsByNames, StringComparer);
				_optionalArgumentsByShortKeys = UpdateDict(_optionalArgumentsByShortKeys, new CharComparer(_ignoreCase));
				_defaultValues = UpdateDict(_defaultValues, StringComparer);
			}
		}
	}

	string _newLine = "\n";
	public string NewLine
	{
		get => _newLine;
		set
		{
			if (_newLine != value)
			{
				_newLine = value;
				_help = null;
				_usage = null;
			}
		}
	}

	int _helpDescriptionOffset = 25;
	public int HelpDescriptionOffset
	{
		get => _helpDescriptionOffset;
		set
		{
			if (_helpDescriptionOffset != value)
			{
				_helpDescriptionOffset = value;
				_help = null;
			}
		}
	}
		
	string? _epilog;
	public string? Epilog
	{
		get => _epilog;
		set
		{
			if (_epilog != value)
			{
				_epilog = value;
				_help = null;
			}
		}
	}
		
	public bool IgnoreUnknownArguments { get; set; }

	string? _usage;
	string? _customUsage;
	public string? Usage
	{
		get => _usage ??= _customUsage ?? Help.SplitNonEmpty(NewLine).FirstOrDefault();
		set
		{
			_customUsage = value;

			if (_usage != value)
			{
				_usage = value;
				_help = null;
			}
		}
	}

	string? _help;
	public string Help => _help ??= CreateHelp();

	StringComparer StringComparer => _ignoreCase ? StringComparer.OrdinalIgnoreCase : StringComparer.Ordinal;

	Dictionary<string, CommandLineArgument> _positionalArguments = new();
	readonly List<CommandLineArgument> _optionalArguments = new();
		
	Dictionary<string, CommandLineArgument>? _optionalArgumentsByKeys;
	Dictionary<string, CommandLineArgument> OptionalArgumentsByKeys => _optionalArgumentsByKeys ??= new Dictionary<string, CommandLineArgument>(StringComparer);
		
	Dictionary<string, CommandLineArgument>? _optionalArgumentsByNames;
	Dictionary<string, CommandLineArgument> OptionalArgumentsByNames => _optionalArgumentsByNames ??= new Dictionary<string, CommandLineArgument>(StringComparer);
		
	Dictionary<char, CommandLineArgument>? _optionalArgumentsByShortKeys;
	Dictionary<char, CommandLineArgument> OptionalArgumentsByShortKeys => _optionalArgumentsByShortKeys ??= new Dictionary<char, CommandLineArgument>(new CharComparer(IgnoreCase));
		
	Dictionary<string, object?>? _defaultValues;
	Dictionary<string, object?> DefaultValues => _defaultValues ??= new Dictionary<string, object?>(StringComparer);

	static readonly StringEscaper _stringEscaper = new("\"", "\\");

	Box<ILocale?>? _locale;
	public ILocale? Locale
	{
		get => (_locale ??= new Box<ILocale?>(InstanceFactory.Get<ILocale>(false))).Value;
		set
		{
			if (_locale?.Value != value)
			{
				_locale = new Box<ILocale?>(value);
				_help = null;
				_usage = null;
			}
		}
	}

	public ArgumentParser(string? executableName = null, string? description = null, bool addHelpArgument = true, object? model = null)
	{
		_executableName = executableName;
		Description = description;

		if (addHelpArgument)
			this.AddOptionalArgument<bool>("-h --help", L($"show this help message and exit"), func: _ => Help);

		if (model != null)
		{
			object modelInstance;
			TypeEx typeEx;
			var t = model.GetType();
			if (t == typeof(TypeEx) || t == typeof(Type))
			{
				typeEx = t;
				modelInstance = typeEx.Create();
			}
			else
			{
				modelInstance = model;
				typeEx = modelInstance.GetType();
			}
				
			var groups = new Dictionary<object, MutuallyExclusiveGroup>();
				
			if (typeEx.TryFindAttribute<CommandArgumentsDescriptionAttribute>(out var descAttr))
			{
				IgnoreUnknownArguments = descAttr.IgnoreUnknownArguments;

				_executableName ??= descAttr.ExecutableName;
				_description ??= descAttr.Description;

				_ignoreCase = descAttr.IgnoreCase;
				_epilog = descAttr.Epilog;
				_newLine = descAttr.NewLine;
				_customUsage = descAttr.Usage;

				if (descAttr.HelpDescriptionOffset.HasValue)
					HelpDescriptionOffset = descAttr.HelpDescriptionOffset.Value;
			}
				
			foreach (var mem in typeEx.FindMembers(new ReflectionFilter(
						 MemberTypes.Field | 
						 MemberTypes.Property | 
						 MemberTypes.Method)))
			{
				if (!mem.TryFindAttribute<CommandArgumentAttribute>(out var attr))
					continue;
				
				var target = attr.Group == null || attr.IsPositional 
					? (IArgumentsCollection)this 
					: groups.GetOrAdd(attr.Group, AddMutuallyExclusiveGroup);

				var name = attr.Name ?? mem.Name.ToLower();
				var defaultValue = attr.GetDefaultValueBox();
				if (defaultValue == null && mem is BaseDataMember {CanRead: true} dm)
				{
					var val = dm.GetValue(modelInstance);
					if (!Equals(val, dm.ValueType.DefaultValue) || attr.ShowDefaultValue == DefaultValueHandling.Show)
						defaultValue = val;
				}
				var arg = mem switch
				{
					Method method when attr.IsPositional 
						=> AddPositionalArgument(
							name, 
							attr.Help, 
							method.Parameters.FirstOrDefault()?.ParameterType ?? typeof(bool), 
							attr.IsOptional, 
							defaultValue: defaultValue, 
							showDefaultValue: attr.ShowDefaultValue),
						
					Method method 
						=> target.AddOptionalArgument(
							attr.Keys, 
							attr.Help, 
							name, 
							method.Parameters.FirstOrDefault()?.ParameterType ?? typeof(bool), 
							defaultValue: defaultValue, 
							showDefaultValue: attr.ShowDefaultValue),
						
					BaseDataMember baseDataMember when attr.IsPositional 
						=> AddPositionalArgument(
							name, 
							attr.Help, 
							baseDataMember.ValueType, 
							attr.IsOptional, 
							defaultValue: defaultValue, 
							showDefaultValue: attr.ShowDefaultValue),
						
					BaseDataMember baseDataMember 
						=> target.AddOptionalArgument(
							attr.Keys, 
							attr.Help, 
							name,
							baseDataMember.ValueType, 
							defaultValue: defaultValue, 
							showDefaultValue: attr.ShowDefaultValue),
						
					_ => throw new Exception(L($"Invalid member {mem.GetType()}"))
				};

				arg.Member = mem;
			}
		}
	}
		
	internal string L(FormattableString str)
	{
		var loc = Locale;
		if (loc == null || loc.Language == "EN") 
			return str.ToString();
			
		var arguments = str.GetArguments();
		var res = loc.Localize(str.Format, arguments);
		if (res == null && loc.Language == "RU")
			switch (str.Format)
			{
			case "show this help message and exit":
				return "показывает это сообщение и завершает работу";
			case "Optional positional arguments could be added only to the end of arguments list":
				return "Необязательные позиционные аргументы могут быть добавлены только в конец списка аргументов";
			case "Expected at least one key":
				return "Ожидался хотя бы один ключ";
			case "argument {0}: not allowed with argument {1}":
				return string.Format("аргумент {0}: недопустим одновременно с аргументом {1}", arguments);
			case "argument {0}: expected 1 {1} argument": //TODO: разобраться с множественными
				return string.Format("аргумент {0}: ожидалось значение типа {1}", arguments);
			case "argument {0}: expected 1 argument":
				return string.Format("аргумент {0}: ожидалось значение", arguments);
			case "unrecognized arguments: {0}":
				return string.Format("нераспознанные аргументы {0}", arguments);
			case "the following arguments are required: {0}":
				return string.Format("следующие обязательные аргументы отсутствуют: {0}", arguments);
			case "argument {0}: invalid choice: '{1}' (choise from {2})":
				return string.Format("аргумент {0}: неверное значение: '{1}' (верные значения: {2})", arguments);
			case "argument {0}: invalid {1} value: '{2}'":
				return string.Format("аргумент {0}: неверное значение типа {1}: '{2}'", arguments);
			case "argument {0}: error: {1}":
				return string.Format("аргумент {0}: ошибка: '{1}'", arguments);
			case "argument {0}: invalid member {1}":
				return string.Format("аргумент {0}: неправильный член класса: {1}", arguments);
			case "Invalid member {0}":
				return string.Format("Неправильный член класса: {0}", arguments);
			case "{0}{1}{2}: error: {3}":
				return string.Format("{0}{1}{2}: ошибка: {3}", arguments);
			case "(default: {0})":
				return string.Format("(по умолчанию: {0})", arguments);
			case "Positional arguments:{0}":
				return string.Format("Позиционные аргументы:{0}", arguments);
			case "Optional arguments:{0}":
				return string.Format("Необязательные аргументы:{0}", arguments);
			case "Usage: {0} {1}":
				return string.Format("Использование: {0} {1}", arguments);
			default:
				Utils.Nop();
				break;
			}

		return str.ToString();
	}


	MutuallyExclusiveGroup AddMutuallyExclusiveGroup(object? _)
		=> AddMutuallyExclusiveGroup();

	public MutuallyExclusiveGroup AddMutuallyExclusiveGroup()
		=> new(this);

	public CommandLineArgument AddPositionalArgument<T>(string name, string? help = null, bool isOptional = false,
		Func<T, string>? func = null,
		T[]? choices = null,
		Box<object>? defaultValue = null,
		DefaultValueHandling showDefaultValue = DefaultValueHandling.ShowNonDefault)
		=> AddPositionalArgument(name, help, typeof(T), isOptional,
			func != null ? o => func((T) o) : null,
			choices?.Select(c => c?.ToString()).ToArray(), defaultValue, showDefaultValue);
		
	public CommandLineArgument AddPositionalArgument(string name, string? help = null, Type? type = null, bool isOptional = false, 
		Func<object, string>? func = null, 
		string?[]? choices = null,
		Box<object>? defaultValue = null,
		DefaultValueHandling showDefaultValue = DefaultValueHandling.ShowNonDefault)
	{
		if (name == null)
			throw new ArgumentNullException(nameof(name));

		if (defaultValue != null)
		{
			isOptional = true;
			DefaultValues[name] = defaultValue.Value;
		}

		if (_positionalArguments.LastOrDefault().Value?.IsOptional == true && !isOptional)
			throw new ArgumentException(L($"Optional positional arguments could be added only to the end of arguments list"), nameof(isOptional));
			
		type ??= typeof(string);

		var result = new CommandLineArgument(null, name, type, help, false, isOptional, func, choices, defaultValue, showDefaultValue);

		_positionalArguments[name] = result;

		return result;
	}
		
	public CommandLineArgument AddOptionalArgument(string[] keys, string? help = null, string? name = null, Type? type = null, 
		Func<object, string>? func = null, 
		string[]? choices = null, 
		bool countKeys = false,
		Box<object>? defaultValue = null,
		DefaultValueHandling showDefaultValue = DefaultValueHandling.ShowNonDefault)
	{
		if (keys == null || keys.Length == 0)
			throw new ArgumentException(L($"Expected at least one key"), nameof(keys));

		type ??= countKeys ? typeof(int) : typeof(string);
		name ??= (keys.FirstOrDefault(k => k.StartsWith("--")) ?? keys.First()).TrimStart('-');

		var result = new CommandLineArgument(keys, name, type, help, countKeys, true, func, choices, defaultValue, showDefaultValue);

		_optionalArguments.Add(result);
		OptionalArgumentsByNames[name] = result;

		foreach (var key in keys)
		{
			if (key.Length == 2 && key[0] == '-')
				OptionalArgumentsByShortKeys[key[1]] = result;
				
			OptionalArgumentsByKeys[key] = result;
		}

		if (defaultValue != null)
			DefaultValues[name] = defaultValue.Value;

		return result;
	}

	public bool TryParse(string args, out Dictionary<string, object?> values)
		=> TryParse(SplitArguments(args), out values);

	public bool TryParse(string[] args, out Dictionary<string, object?> values)
	{
		if (TryParse(args, out values, out var message))
			return true;

		Console.WriteLine(message);
		return false;
	}

	public bool TryParse(string args, out Dictionary<string, object?> values, out string message)
		=> TryParse(SplitArguments(args), out values, out message);
		
	public bool TryParse(string[] args, out Dictionary<string, object?> values, out string message)
	{
		message = null!;
		values = new Dictionary<string, object?>(DefaultValues, StringComparer);
		var posArgs = _positionalArguments.Values.ToList();
		var groups = new Dictionary<MutuallyExclusiveGroup, CommandLineArgument>();

		for (var i = 0; i < args.Length; ++i)
		{
			var token = args[i];

			bool SetOptionalArg(CommandLineArgument argument, Dictionary<string, object?> vls, out string msg)
			{
				msg = null!;

				if (argument.Group != null)
				{
					if (groups.TryGetValue(argument.Group, out var other))
					{
						msg = Error($"argument {argument}: not allowed with argument {other}");
						return false;
					}

					groups[argument.Group] = argument;
				}

				if (argument.CountKeys)
				{
					if (vls.TryGetValue(argument.Name, out var o) && o is int cnt)
						vls[argument.Name] = ++cnt;
					else
						vls[argument.Name] = 1;

					return true;
				}
					
				if (argument.Type == typeof(bool))
					return TrySetValue(vls, argument, "true", out msg);

				var argArray = new List<string>();
				for (var j = 0; j < argument.NArgs; ++j)
				{
					if (++i >= args.Length)
					{
						msg = Error(argument.Type != typeof(string)
							? $"argument {argument}: expected {argument.NArgs} {argument.TypeName} {Plural.En(argument.NArgs, "argument","arguments", true)}"
							: (FormattableString) $"argument {argument}: expected {Plural.En(argument.NArgs, "argument","arguments")}");

						return false;
					}
					argArray.Add(args[i]);
				}
						
				return TrySetValue(vls, argument, argArray, out msg);
			}

			if (OptionalArgumentsByKeys.TryGetValue(token, out var arg))
			{
				if (!SetOptionalArg(arg, values, out message)) 
					return false;
			}
			else
			{
				if (token.Length >= 2 && token[0] == '-' && token[1] != '-')
				{
					var unrecognized = new EnumBuilder("", "-");
						
					foreach (var ch in token.Skip(1))
						if (OptionalArgumentsByShortKeys.TryGetValue(ch, out arg))
						{
							if (!SetOptionalArg(arg, values, out message))
								return false;
						}
						else
							unrecognized.Append(ch);

					if (unrecognized.Length > 0)
					{
						message = Error($"unrecognized arguments: {unrecognized}");
						return false;
					}
						
					continue;
				}
						
				arg = posArgs.FirstOrDefault();
				if (arg != null)
				{
					posArgs.Remove(arg);

					if (!TrySetValue(values, arg, token, out message))
						return false;
						
					continue;
				}

				if (!IgnoreUnknownArguments)
				{
					message = Error($"unrecognized arguments: {args.Skip(i).ToString(new EnumBuilder(" "))}");
					return false;
				}
				values[i.ToString()] = token;
			}
		}
			
		posArgs = posArgs.Where(a => !a.IsOptional).ToList();

		if (posArgs.Count > 0)
		{
			message = Error($"the following arguments are required: {posArgs.ToString(a => a.Name)}");
			return false;
		}
			
		return true;
	}

	bool TrySetValue(Dictionary<string, object?> values, CommandLineArgument arg, string value, out string message)
		=> TrySetValue(values, arg, new[] {value}, out message);

	bool TrySetValue(Dictionary<string, object?> values, CommandLineArgument arg, IEnumerable<string> argArray, out string message)
	{
		var oValues = new List<object?>();
			
		foreach (var val in argArray)
		{
			if (arg.Choices?.Contains(val) == false)
			{
				message = Error($"argument {arg}: invalid choice: '{val}' (choise from {arg.Choices.ToString(c => c)})");
				return false;
			}

			if (!StringConverter.Default.TryParse(val, arg.Type, out var value))
			{
				message = Error($"argument {arg}: invalid {arg.TypeName} value: '{val}'");
				return false;
			}

			oValues.Add(value);
		}

		var oValue = oValues.Count == 1 ? oValues[0] : oValues;
		message = null!;

		if (arg.Proc != null)
			try
			{
				message = arg.Proc(oValue);
					
				if (message != null)
					return false;
			}
			catch (Exception e)
			{
				message = Error($"argument {arg}: error: {e.Message}");
				return false;
			}
			
		values[arg.Name] = oValue;
		return true;
	}

	public string? ApplyToModel(object model, Dictionary<string, object?> values)
	{
		foreach (var pair in values)
			if (_positionalArguments.TryGetValue(pair.Key, out var arg) ||
				OptionalArgumentsByNames.TryGetValue(pair.Key, out arg))
				try
				{
					switch (arg.Member)
					{
					case BaseDataMember dm:
						dm.SetValue(model, pair.Value);
						break;
					case Method method:

						var result = pair.Value is List<object> list 
							? method.InvokeAsync(model, list.ToArray()).NoCtxResult() 
							: method.InvokeAsync(model, new {pair.Value}).NoCtxResult();

						switch (result)
						{
						case string str:
							return str;
						case false:
							return "";
						}
						break;
					case null:
						break;
					default:
						return Error($"argument {arg}: invalid member {arg.Member}");
					}
				}
				catch (Exception e)
				{
					return Error($"argument {arg}: error: {e.Message}");
				}

		return null;
	}

	string? SetModelValue(object model, BaseMember member, object value)
	{
		try
		{
			switch (member)
			{
			case BaseDataMember dataMember:
				dataMember.SetValue(model, value);
				return null;
			case Method method:
				return method.Invoke(value)?.ToString();
			default:
				throw new Exception(L($"Invalid member {member.GetType()}"));
			}
		}
		catch (Exception e)
		{
			return e.ToString();
		}
	}
		
	string Error(FormattableString message)
		=> L($"{Usage}{NewLine}{ExecutableName}: error: {L(message)}");

	string CreateHelp()
	{
		void PadHelp(StringBuilder bld2, CommandLineArgument argument)
		{
			if (argument.Help.IsEmpty() && argument.DefaultValue == null)
				return;

			bld2.Append(' ');

			var spaces = HelpDescriptionOffset - bld2.Length - 1;
			if (spaces > 0)
				bld2.Append(new string(' ', spaces));
			else if (spaces < 0)
			{
				bld2.Append(NewLine);
				bld2.Append(new string(' ', HelpDescriptionOffset - 1));
			}

			string? defaultValue = null;
			if (argument.DefaultValue != null)
			{
				var value = argument.DefaultValue.Value;

				switch (argument.ShowDefaultValue)
				{
				case DefaultValueHandling.ShowNonDefault:
					if (value != null && !Equals(value, TypeEx.Of(value).DefaultValue))
						goto case DefaultValueHandling.Show;
					break;
				case DefaultValueHandling.Show:
					defaultValue = L($"(default: {StringConverter.Default.ToString(value) ?? "null"})");
					break;
				}
			}

			bld2.Append(" ".JoinNonEmpty(argument.Help, defaultValue));
		}

		var posArgs = new EnumBuilder(NewLine, L($"Positional arguments:{NewLine}"));
		var optArgs = new EnumBuilder(NewLine, L($"Optional arguments:{NewLine}"));

		var args = new EnumBuilder(" ");

		var processed = new HashSet<CommandLineArgument>();
		foreach (var arg in _optionalArguments)
		{
			if (!processed.Contains(arg))
			{
				var bld3 = new EnumBuilder(" | ");
				bld3.Append(" ".JoinNonEmpty(arg.Keys.FirstOrDefault(), arg.ParameterName));

				if (arg.Group != null)
					foreach (var argAlt in arg.Group.GetOtherOptions(arg))
					{
						bld3.Append(" ".JoinNonEmpty(argAlt.Keys.FirstOrDefault(), argAlt.ParameterName));
						processed.Add(argAlt);
					}

				args.Append($"[{bld3}]");
				processed.Add(arg);
			}

			var bld2 = new StringBuilder("  ");
			bld2.Append(arg.Keys.ToString(k => k));
			if (arg.ParameterName != null)
			{
				bld2.Append(' ');
				bld2.Append(arg.ParameterName);
			}

			PadHelp(bld2, arg);
			optArgs.Append(bld2);
		}

		foreach (var arg in _positionalArguments.Values)
		{
			args.Append(arg.IsOptional ? $"[{arg.Name}]" : arg.Name);
			var bld2 = new StringBuilder("  ");
			bld2.Append(arg.Name);
			PadHelp(bld2, arg);
			posArgs.Append(bld2);
		}

		var bld = new EnumBuilder($"{NewLine}{NewLine}");

		bld.Append(L($"Usage: {ExecutableName} {args}"));

		if (Description.IsSome())
			bld.Append($"{Description}");

		if (posArgs.IsSome)
			bld.Append(posArgs);

		if (optArgs.IsSome)
			bld.Append(optArgs);

		if (Epilog.IsSome())
			bld.Append(Epilog);
			
		return bld;
	}

	public override string ToString()
		=> CreateHelp();

	public static bool TryParse<TModel>(
		string args, 
		out TModel result, 
		string? executableName = null, 
		string? description = null) 
		where TModel : class, new()
		=> TryParse(SplitArguments(args), out result, executableName, description);

	public static bool TryParse<TModel>(
		string[] args, 
		out TModel result, 
		string? executableName = null, 
		string? description = null) 
		where TModel : class, new()
	{
		if (TryParse(args, out result, out var message, executableName, description))
			return true;

		Console.WriteLine(message);
		return false;
	}

	public static bool TryParse<TModel>(
		string args, 
		out TModel result, 
		out string message, 
		string? executableName = null, 
		string? description = null) 
		where TModel : class, new()
		=> TryParse(SplitArguments(args), out result, out message, executableName, description);
		
	public static bool TryParse<TModel>(
		string[] args, 
		out TModel result, 
		out string message, 
		string? executableName = null, 
		string? description = null)
		where TModel : class, new() 
	{
		result = new TModel();
		var parser = new ArgumentParser(executableName, description, model: result);

		if (parser.TryParse(args, out var values, out message))
		{
			message = parser.ApplyToModel(result, values)!;
			return message == null!;
		}

		result = null!;
		return false;
	}

	public static TModel Parse<TModel>(
		string args, 
		string? executableName = null, 
		string? description = null)
		where TModel : class, new()
		=> TryParse(args, out TModel result, executableName, description) 
			? result 
			: throw new ParsingException(args, $"Unable to parse arguments string: {args}");

	public static TModel Parse<TModel>(
		string[] args,
		string? executableName = null,
		string? description = null)
		where TModel : class, new()
	{
		if (TryParse(args, out TModel result, executableName, description))
			return result;
		var argsString = args.ToString(new EnumBuilder(" "));
		throw new ParsingException(argsString, $"Unable to parse arguments string: {argsString}");
	}


	public static string[] SplitArguments(string args)
		=> _stringEscaper.Split(args, " ").Select(a =>
		{
			if (a.Contains(' '))
				a = a.Substring(1, a.Length - 2); //TODO: распарсить нормально

			return a;
		}).ToArray();
}