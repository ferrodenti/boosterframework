namespace Booster.Parsing;

public class PositionalCommandArgumentAttribute : CommandArgumentAttribute
{
	public PositionalCommandArgumentAttribute(string help = null, string name = null, bool isOptional = false) : base(true, null, help, name, isOptional)
	{
	}
}