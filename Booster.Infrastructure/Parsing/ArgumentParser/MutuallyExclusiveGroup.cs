using System;
using System.Collections.Generic;
using System.Linq;

namespace Booster.Parsing;

public class MutuallyExclusiveGroup : IArgumentsCollection
{
	readonly ArgumentParser _owner;
	readonly List<CommandLineArgument> _arguments = new();
		

	public MutuallyExclusiveGroup(ArgumentParser owner)
		=> _owner = owner;

	public IEnumerable<CommandLineArgument> GetOtherOptions(CommandLineArgument argument)
		=> _arguments.Where(a => a != argument);

	public CommandLineArgument AddOptionalArgument(string[] keys, string help = null, string name = null, Type type = null,
		Func<object, string> func = null,
		string[] choices = null,
		bool countKeys = false,
		Box<object> defaultValue = null,
		DefaultValueHandling showDefaultValue = DefaultValueHandling.ShowNonDefault)
	{
		var result =_owner.AddOptionalArgument(keys, help, name, type, func, choices, countKeys, defaultValue, showDefaultValue);
		result.Group = this;
		_arguments.Add(result);

		return result;
	}
}