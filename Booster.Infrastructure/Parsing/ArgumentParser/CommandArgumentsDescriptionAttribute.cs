using System;

namespace Booster.Parsing;

[AttributeUsage(AttributeTargets.Class)]
public class CommandArgumentsDescriptionAttribute : Attribute
{
	public string ExecutableName { get; set; }
	public string Description { get; set; }
	public bool IgnoreUnknownArguments { get; set; }
	public bool IgnoreCase { get; set; }
	public string Locate { get; set; }
	public string NewLine { get; set; } = "\n";
	public int? HelpDescriptionOffset { get; set; }
	public string Epilog { get; set; }
	public string Usage { get; set; }
}