using System;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Parsing;

[PublicAPI]
public class EvaluationException : Exception
{
	public string? Expression { get; set; }

	public EvaluationException() { }
	public EvaluationException(string expression, string message) : base(message) => Expression = expression;
	public EvaluationException(string message) : base(message) { }
	public EvaluationException(string message, Exception innerException) : base(message, innerException) { }
}