﻿using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace Booster.Parsing;

#nullable enable

[PublicAPI]
public class StringAnalizer
{
	public virtual StringFlags Analize(string? str)
		=> AnalyzeChars(str)
			.Select((_, ch) => (StringFlags) ch)
			.Aggregate(StringFlags.Empty, (current, flags) => current | flags);

	protected class State
	{
		public bool Up;
		public bool Low;
		public bool Underscope;
		public bool Digit;
		public StringFlags First;

		public StringFlags Analyze()
		{
			if (Low)
			{
				if (Up)
				{
					if (Underscope)
						return First | StringFlags.SnakeWord;

					return First;
				}

				return Underscope ? StringFlags.SnakeWord : StringFlags.LowerWord;
			}

			if (Up && !Low)
				return StringFlags.UpperWord;

			return Digit ? StringFlags.Number : First;
		}

		public void InitFirst(StringFlags value)
		{
			if (First == StringFlags.Empty)
				First = value;
		}
	}

	public virtual IEnumerable<(char, StringFlags)> AnalyzeChars(string? str) //TODO: Change case
	{
		var state = new State();
		
		if (str != null)
			foreach (var ch in str)
			{
				var flag = Analize(ch);

				if (flag.HasFlag(StringFlags.Upper))
				{
					state.Up = true;
					state.InitFirst(StringFlags.CamelWord | StringFlags.Capital);
				}
				else if (flag.HasFlag(StringFlags.Lower))
				{
					state.Low = true;
					state.InitFirst(StringFlags.CamelWord | StringFlags.Capital);
				}
				else if (ch == '_')
					state.Underscope = true;
				else if (flag.HasFlag(StringFlags.Digits))
					state.Digit = true;
				else
				{
					flag |= state.Analyze();
					state = new State();
				}

				yield return (ch, flag);
			}

		yield return ('\0', state.Analyze());
	}

	public virtual StringFlags Analize(char ch)
	{
		var result = StringFlags.Empty;

		if (char.IsLetter(ch))
		{
			result |= StringFlags.Letters;
			
			if (char.IsUpper(ch))
			{
				result |= StringFlags.Upper;
				ch = char.ToLower(ch);
			}
			else if(char.IsLower(ch)) 
				result |= StringFlags.Lower;

			switch (ch)
			{
			case >= 'a' and <= 'z':
				result |= StringFlags.En;
				break;
			case >= 'а' and <= 'я':
			case 'ё':
				result |= StringFlags.Ru;
				break;
			}
		}
		else if (char.IsDigit(ch))
			result |= StringFlags.Digits;
		else if (char.IsWhiteSpace(ch))
		{
			result |= StringFlags.Whitespaces;
			
			if(char.IsControl(ch))
				result |= StringFlags.Control;
		}
		else if(char.IsPunctuation(ch))
			result |= StringFlags.Punctuation;
		else if (char.IsSymbol(ch))
			result |= StringFlags.Symbol;
		else
			result |= StringFlags.Other;
		
		return result;
	}

	public int IndexOf(string? str, StringFlags flags, bool any = false)
		=> IndexOf(str, 0, flags, any);

	public int IndexOf(string? str, int index, StringFlags flags, bool any = false)
	{
		if (str != null)
		{
			var count = str.Length;
			return IndexOf(str, index, count, flags, any);
		}

		return -1;
	}

	public int IndexOf(string? str, int index, int count, StringFlags flags, bool any = false)
	{
		if(str != null)
			for (int i = index, j = 0, len = str.Length; j < count && i < len; ++i, ++j)
			{
				var ch = str[i];
				var f = Analize(ch) & flags;
				var m = any ? f != StringFlags.Empty : f == flags;
				if (m)
					return i;
			}

		return -1;
	}

	public int LastIndexOf(string? str, StringFlags flags, bool any = false)
	{
		if (str != null)
		{
			var count = str.Length;
			var index = count - 1;
			return LastIndexOf(str, index, count, flags, any);
		}

		return -1;
	}

	public int LastIndexOf(string? str, int index, StringFlags flags, bool any = false)
	{
		if (str != null)
		{
			var count = str.Length;
			return LastIndexOf(str, index, count, flags, any);
		}

		return -1;
	}

	public int LastIndexOf(string? str, int index, int count, StringFlags flags, bool any = false)
	{
		if(str != null)
			for (int i = index, j = 0; j < count && i >= 0; --i, ++j)
			{
				var ch = str[i];
				var f = Analize(ch) & flags;
				var m = any ? f != StringFlags.Empty : f == flags;
				if (m)
					return i;
			}

		return -1;
	}
}