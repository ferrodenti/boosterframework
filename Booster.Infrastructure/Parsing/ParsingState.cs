using System;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Parsing;

public class ParsingState
{
	public StringParser Parser { get; }

	//public string Rest => Owner._str.SubstringSafe(Position);
		
	/// <summary>
	/// Выявленное совпадение. Null при завршении разбора
	/// </summary>
	public readonly string? Token; 
	public readonly string? Buffer;
	public object? Tag { get; set; }

	public int Position => Parser.Pos;

	public bool Stop { get; set; }
	public bool Skip { get; set; }

	public ParsingState(StringParser owner, string? token, string? prev)
	{
		Parser = owner;
		Token = token;
		Buffer = prev;
	}
		
	public ParsingState HandleRest([InstantHandle]Action<ParsingState> handle)
	{
		if (!Stop && Token == null)
			handle(this);

		return this;
	}
}