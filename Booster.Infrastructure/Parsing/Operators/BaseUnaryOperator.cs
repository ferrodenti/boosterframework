using System.Text;

namespace Booster.Parsing.Operators;

public abstract class BaseUnaryOperator : BaseOperator
{
	public override int OperandsCount => 1;

	public override string ToString()
	{
		if (Definition != null)
		{

			var str = new StringBuilder();

			if (Operands.Count > 0 && !Definition.PreOp)
				str.Append(Operands[0]);

			str.Append(Definition.Token);

			if (Operands.Count > 0 && Definition.PreOp)
				str.Append(Operands[0]);

			return str.ToString();
		}
		return base.ToString();
	}
}