using System.Collections.Generic;

namespace Booster.Parsing.Operators;

public abstract class BaseOperator
{
	public OperatorDefinition Definition { get; set; }
	public List<BaseOperator> Operands { get; set; }

	public abstract int OperandsCount { get; }

	public bool IsComplete => OperandsCount == Operands.Count;

	public int Priority => Definition.With(d => d.Prioriry);

	protected BaseOperator()
		=> Operands = new List<BaseOperator>();

	public void AddOperand(StringParser parser, BaseOperator op)
	{
		if( op == null )
			throw parser.CreateException("Expected operand");

		Operands.Add(op);
	}

	public override string ToString()
	{
		if (Definition != null)
			return Definition.Token;

		return base.ToString();
	}

	public virtual BaseOperator TrySimplify()
		=> this;

	public abstract object Evaluate(IEvaluationContext context);
}