using System.Text;

namespace Booster.Parsing.Operators;

public abstract class BaseBinaryOperator : BaseOperator
{
	public override int OperandsCount => 2;

	public override string ToString()
	{
		var str = new StringBuilder();

		if (Operands.Count > 0)
			str.Append(Operands[0]);

		str.Append(Definition.Token);

		if (Operands.Count > 1)
			str.Append(Operands[1]);

		return str.ToString();
	}
}