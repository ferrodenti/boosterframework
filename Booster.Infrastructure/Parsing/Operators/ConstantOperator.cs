namespace Booster.Parsing.Operators;

public class ConstantOperator<T> : BaseConstantOperator
{
	public new T Value { get; }

	public ConstantOperator(T value)
		: base(value, typeof(T))
        => Value = value;
}