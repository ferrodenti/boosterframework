using Booster.Reflection;

namespace Booster.Parsing.Operators;

public abstract class BaseConstantOperator : BaseOperator
{
	public object Value { get; }
	public TypeEx ValueType { get; }

	public override int OperandsCount => 0;

	protected BaseConstantOperator(object value, TypeEx valueType)
	{
		ValueType = valueType;
		Value = value;
	}

	public override string ToString()
		=> Value.ToString();

	public override object Evaluate(IEvaluationContext context)
		=> Value;
}