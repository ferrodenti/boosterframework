namespace Booster.Parsing.Operators;

public class NotEqualsOperator : BaseBinaryOperator
{
	public override object Evaluate(IEvaluationContext context)
		=> !Equals(Operands[0].Evaluate(context), Operands[1].Evaluate(context));
}