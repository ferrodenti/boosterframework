using Booster.Helpers;

namespace Booster.Parsing.Operators;

public class GreaterOperator : BaseBinaryOperator
{
	public override object Evaluate(IEvaluationContext context)
		=> NumericHelper.Greater(Operands[0].Evaluate(context), Operands[1].Evaluate(context));
}