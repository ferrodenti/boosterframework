using Booster.Helpers;

namespace Booster.Parsing.Operators;

public class LessOperator : BaseBinaryOperator
{
	public override object Evaluate(IEvaluationContext context)
		=> NumericHelper.Less(Operands[0].Evaluate(context), Operands[1].Evaluate(context));	}