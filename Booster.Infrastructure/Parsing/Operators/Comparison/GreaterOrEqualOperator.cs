using Booster.Helpers;

namespace Booster.Parsing.Operators;

public class GreaterOrEqualOperator : BaseBinaryOperator
{
	public override object Evaluate(IEvaluationContext context)
		=> NumericHelper.GreaterOrEqual(Operands[0].Evaluate(context), Operands[1].Evaluate(context));
}