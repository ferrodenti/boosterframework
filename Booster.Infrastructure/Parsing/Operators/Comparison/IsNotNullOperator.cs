namespace Booster.Parsing.Operators;

public class IsNotNullOperator : BaseUnaryOperator
{
	public override object Evaluate(IEvaluationContext context)
		=> null != Operands[0].Evaluate(context);
}