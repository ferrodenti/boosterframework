using Booster.Helpers;

namespace Booster.Parsing.Operators;

public class LessOrEqualOperator : BaseBinaryOperator
{
	public override object Evaluate(IEvaluationContext context)
		=> NumericHelper.LessOrEqual(Operands[0].Evaluate(context), Operands[1].Evaluate(context));	}