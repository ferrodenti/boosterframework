namespace Booster.Parsing.Operators;

public class IsNullOperator : BaseUnaryOperator
{
	public override object Evaluate(IEvaluationContext context)
		=> null == Operands[0].Evaluate(context);
}