using Booster.Helpers;

namespace Booster.Parsing.Operators;

public class PowOperator : BaseBinaryOperator
{
	public override object Evaluate(IEvaluationContext context)
		=> NumericHelper.Pow(Operands[0].Evaluate(context), Operands[1].Evaluate(context));
}