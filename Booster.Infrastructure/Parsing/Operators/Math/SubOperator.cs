using Booster.Helpers;

namespace Booster.Parsing.Operators;

public class SubOperator : BaseBinaryOperator
{
	public override object Evaluate(IEvaluationContext context)
		=> NumericHelper.Sub(Operands[0].Evaluate(context), Operands[1].Evaluate(context));
}