using Booster.Helpers;

namespace Booster.Parsing.Operators;

public class SumOperator : BaseBinaryOperator
{
	public override object Evaluate(IEvaluationContext context)
		=> NumericHelper.Sum(Operands[0].Evaluate(context), Operands[1].Evaluate(context));
}