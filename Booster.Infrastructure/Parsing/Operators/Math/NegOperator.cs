using Booster.Helpers;

namespace Booster.Parsing.Operators;

public class NegOperator : BaseUnaryOperator
{
	public override object Evaluate(IEvaluationContext context)
		=> NumericHelper.Neg(Operands[0].Evaluate(context));
}