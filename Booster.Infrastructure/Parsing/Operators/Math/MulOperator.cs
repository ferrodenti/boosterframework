using Booster.Helpers;

namespace Booster.Parsing.Operators;

public class MulOperator : BaseBinaryOperator
{
	public override object Evaluate(IEvaluationContext context)
		=> NumericHelper.Mul(Operands[0].Evaluate(context), Operands[1].Evaluate(context));
}