using Booster.Helpers;

namespace Booster.Parsing.Operators;

public class DivOperator : BaseBinaryOperator
{
	public override object Evaluate(IEvaluationContext context)
		=> NumericHelper.Div(Operands[0].Evaluate(context), Operands[1].Evaluate(context));
}