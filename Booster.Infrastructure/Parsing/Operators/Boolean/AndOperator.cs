namespace Booster.Parsing.Operators;

public class AndOperator : BaseBinaryOperator
{
	public override BaseOperator TrySimplify()
	{
		Operands[0] = Operands[0].TrySimplify();
		Operands[1] = Operands[1].TrySimplify();

		var constOp1 = Operands[1] as ConstantOperator<bool>;
		if (Operands[0] is ConstantOperator<bool> constOp0)
		{
			if (!constOp0.Value)
				return constOp0;

			return constOp1 != null ? new ConstantOperator<bool>(constOp0.Value && constOp1.Value) : Operands[1];
		}

		if (constOp1 != null)
			return !constOp1.Value ? constOp1 : Operands[0];

		return this;
	}

	public override object Evaluate(IEvaluationContext context)
		=> (bool) Operands[0].Evaluate(context) && (bool) Operands[1].Evaluate(context);
}