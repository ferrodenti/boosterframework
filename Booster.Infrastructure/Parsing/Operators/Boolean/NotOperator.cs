using Booster.Reflection;

namespace Booster.Parsing.Operators;

public class NotOperator : BaseUnaryOperator
{
	public override BaseOperator TrySimplify()
	{
		Operands[0] = Operands[0].TrySimplify();

		if (Operands[0] is ConstantOperator<bool> constOp0)
			return new ConstantOperator<bool>(!constOp0.Value);

		return this;
	}

	public override object Evaluate(IEvaluationContext context)
	{
		var value = Operands[0].Evaluate(context);
		if (value == null)
			return true;

		if (!Cast.Try(value, out bool boolVal))
			throw new EvaluationException($"Unable to cast the value '{value}' of type {value.GetType()} to Boolean");

		return !boolVal;
	}
}