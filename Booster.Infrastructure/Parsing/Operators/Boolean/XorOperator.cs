namespace Booster.Parsing.Operators;

public class XorOperator : BaseBinaryOperator
{
	public override BaseOperator TrySimplify()
	{
		Operands[0] = Operands[0].TrySimplify();
		Operands[1] = Operands[1].TrySimplify();

		var constOp1 = Operands[1] as ConstantOperator<bool>;
		if (Operands[0] is ConstantOperator<bool> constOp0)
		{
			if (constOp1 != null)
				return new ConstantOperator<bool>(constOp1.Value ^ constOp0.Value);

			if (!constOp0.Value)
				return Operands[1];
		}
		else if (constOp1 != null && !constOp1.Value)
			return Operands[0];

		return this;
	}

	public override object Evaluate(IEvaluationContext context)
		=> (bool) Operands[0].Evaluate(context) ^ (bool) Operands[1].Evaluate(context);
}