namespace Booster.Parsing.Operators;

public class VariableOperator : BaseOperator
{
	public string Name { get; }
	public override int OperandsCount => 0;

	public VariableOperator(string name)
		=> Name = name;

	public override string ToString()
		=> Name;

	public override object Evaluate(IEvaluationContext context)
		=> context.GetVariable(Name);
}