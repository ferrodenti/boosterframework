namespace Booster.Parsing.Operators;

public class ParameterOperator : BaseOperator
{
	public string Name { get; }

	public override int OperandsCount => 0;

	public ParameterOperator(string name)
		=> Name = name;

	public override string ToString()
		=> Name;

	public override object Evaluate(IEvaluationContext context)
		=> context.GetParameter(Name);
}