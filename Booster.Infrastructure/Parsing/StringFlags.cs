﻿using System;

namespace Booster.Parsing;

[Flags]
public enum StringFlags
{
	Empty /*		*/ = 0,
	Whitespaces /*	*/= 1 << 0,
	Control /*		*/= 1 << 1,
	Digits /*		*/= 1 << 2,
	Symbol /*		*/= 1 << 3,
	Punctuation /*	*/= 1 << 4,

	Letters /*		*/= 1 << 5,
	En /*			*/= 1 << 6,
	Ru /*			*/= 1 << 7,
	Upper /*		*/= 1 << 10,
	Lower /*		*/= 1 << 11,
	
	Capital /*			*/= 1 << 12,
	UpperWord /*		*/= 1 << 13,
	LowerWord /*		*/= 1 << 14,
	SnakeWord /*		*/= 1 << 15,
	CamelWord /*		*/= 1 << 16,
	Number /*			*/= 1 << 17,
	
	Other /*		*/= 1 << 30
}

public static class StringFlagsExpander
{
	static bool Is(this StringFlags flags, StringFlags value, StringFlags from)
		=> value == (flags & from);

	static bool IsAny(this StringFlags flags, StringFlags any)
		=> (flags & any) != 0; 

	public static bool IsEn(this StringFlags flags)
		=> flags.Is(StringFlags.En, StringFlags.En | StringFlags.Ru);

	public static bool IsRu(this StringFlags flags)
		=> flags.Is(StringFlags.Ru, StringFlags.En | StringFlags.Ru);
	
	public static bool IsUpper(this StringFlags flags)
		=> flags.Is(StringFlags.Upper, StringFlags.Upper | StringFlags.Lower);

	public static bool IsLower(this StringFlags flags)
		=> flags.Is(StringFlags.Lower, StringFlags.Upper | StringFlags.Lower);

	public static bool IsLetterOrDigit(this StringFlags flags)
		=> flags.IsAny(StringFlags.Letters | StringFlags.Digits);

}