using System;
using System.Collections.Generic;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Log;

[PublicAPI]
public interface ILogMessage
{
	LogLevel Level { get; set; }
	string? Message { get; set; }
	string? Name { get; set; }
	string? Class { get; set; }
	string? Member { get; set; }
	string? Source { get; set; }
	int Line { get; set; }
	Exception? Exception { get; set; }
	DateTime TimeStamp { get; set; }
	string? ForamattedText { get; set; }
	string? LogFilename { get; set; }

	IDictionary<string, string> Parameters { get; }

	IMessageFormatter? MessageFormatter { get; set; }

	IDisposable PushState();
}