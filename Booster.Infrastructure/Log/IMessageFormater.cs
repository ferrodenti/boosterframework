using System;
using System.Collections.Generic;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Log;

[PublicAPI]
public interface IMessageFormatter
{
	string NewLine { get; set; } 
	string Separator { get; set; } 
	string ParamSeparator { get; set; } 
	string ParamOpener { get; set; } 
	string? ParamCloser { get; set; } 
	string MessagePrefix { get; set; } 
	string DateFormat { get; set; } 
	string TimeFormat { get; set; } 
	int Priority { get; set; } 
	bool PrintLevels { get; set; } 
		
	Func<ILogMessage, string?>? CustomFormat { get; set; }
	Func<ILogMessage, string?>? CustomExceptionFormat { get; set; }

	string? Format(ILogMessage msg, List<string>? parametersOfInterest);
	string? FormatException(ILogMessage msg);
}