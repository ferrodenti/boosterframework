using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Booster.Reflection;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Log;

[PublicAPI]
public static class LogExtensions
{
	#region CreateContext

	public static ILog? Create(this ILog? ctx) 
		=> ctx?.CreateImpl(null, null);

	[ContractAnnotation("ctx:notnull => notnull", true)]
	public static ILog? Create(this ILog? ctx, object? owner) 
		=> ctx?.CreateImpl(owner, null);

	public static ILog? Create(this ILog? ctx, TypeEx type, object? parameters = null) 
		=> ctx?.CreateImpl(type, parameters);

	public static ILog? Create(this ILog? ctx, object? owner, object? parameters) 
		=> ctx?.CreateImpl(owner, parameters);

	public static ILog? Create<T>(this ILog? ctx, object? parameters = null) 
		=> ctx?.CreateImpl(typeof(T), parameters);

	#endregion

	#region Fatal
	public static void Fatal(this ILog? ctx, 
		string? message = null,
		object? parameters = null,
		[CallerLineNumber] int line = 0, 
		[CallerMemberName] string? member = null, 
		[CallerFilePath] string? source = null)
		=> ctx?.Write(LogLevel.Fatal, message, null, parameters, member, source, line);

	public static void Fatal(this ILog? ctx,
		Exception? exception, 
		object? parameters = null,
		[CallerLineNumber] int line = 0, 
		[CallerMemberName] string? member = null,
		[CallerFilePath] string? source = null)
		=> ctx?.Write(LogLevel.Fatal, null, exception, parameters, member, source, line);

	public static void Fatal(this ILog? ctx, 
		Exception? exception, 
		string? message, 
		object? parameters = null,
		[CallerLineNumber] int line = 0,
		[CallerMemberName] string? member = null,
		[CallerFilePath] string? source = null)
		=> ctx?.Write(LogLevel.Fatal, message, exception, parameters, member, source, line);

	#endregion

	#region Error
	
	public static void Error(this ILog? ctx,
		string? message = null, 
		object? parameters = null,
		[CallerLineNumber] int line = 0,
		[CallerMemberName] string? member = null,
		[CallerFilePath] string? source = null)
		=> ctx?.Write(LogLevel.Error, message, null, parameters, member, source, line);

	public static void Error(this ILog? ctx, 
		Exception? exception, 
		object? parameters = null,
		[CallerLineNumber] int line = 0,
		[CallerMemberName] string? member = null,
		[CallerFilePath] string? source = null)
		=> ctx?.Write(LogLevel.Error, null, exception, parameters, member, source, line);

	public static void Error(this ILog? ctx, 
		Exception? exception, 
		string? message, 
		object? parameters = null,
		[CallerLineNumber] int line = 0,
		[CallerMemberName] string? member = null, 
		[CallerFilePath] string? source = null)
		=> ctx?.Write(LogLevel.Error, message, exception, parameters, member, source, line);

	#endregion

	#region Warn
	public static void Warn(this ILog? ctx, 
		string? message = null, 
		object? parameters = null,
		[CallerLineNumber] int line = 0, 
		[CallerMemberName] string? member = null,
		[CallerFilePath] string? source = null)
		=> ctx?.Write(LogLevel.Warn, message, null, parameters, member, source, line);

	public static void Warn(this ILog? ctx,
		Exception? exception, 
		object? parameters = null,
		[CallerLineNumber] int line = 0,
		[CallerMemberName] string? member = null,
		[CallerFilePath] string? source = null)
		=> ctx?.Write(LogLevel.Warn, null, exception, parameters, member, source, line);

	public static void Warn(this ILog? ctx,
		Exception? exception, 
		string? message,
		object? parameters = null,
		[CallerLineNumber] int line = 0,
		[CallerMemberName] string? member = null,
		[CallerFilePath] string? source = null)
		=> ctx?.Write(LogLevel.Warn, message, exception, parameters, member, source, line);

	#endregion

	#region Info
	public static void Info(this ILog? ctx,
		string? message = null, 
		object? parameters = null,
		[CallerLineNumber] int line = 0,
		[CallerMemberName] string? member = null, 
		[CallerFilePath] string? source = null)
		=> ctx?.Write(LogLevel.Info, message, null, parameters, member, source, line);

	public static void Info(this ILog? ctx,
		Exception? exception,
		object? parameters = null,
		[CallerLineNumber] int line = 0, 
		[CallerMemberName] string? member = null, 
		[CallerFilePath] string? source = null)
		=> ctx?.Write(LogLevel.Info, null, exception, parameters, member, source, line);

	public static void Info(this ILog? ctx, 
		Exception? exception, 
		string? message, 
		object? parameters = null,
		[CallerLineNumber] int line = 0, 
		[CallerMemberName] string? member = null, 
		[CallerFilePath] string? source = null)
		=> ctx?.Write(LogLevel.Info, message, exception, parameters, member, source, line);

	#endregion

	#region Trace
	public static void Trace(this ILog? ctx, 
		string? message = null,
		object? parameters = null,
		[CallerLineNumber] int line = 0,
		[CallerMemberName] string? member = null,
		[CallerFilePath] string? source = null)
		=> ctx?.Write(LogLevel.Trace, message, null, parameters, member, source, line);

	public static void Trace(this ILog? ctx,
		Exception? exception, 
		object? parameters = null,
		[CallerLineNumber] int line = 0, 
		[CallerMemberName] string? member = null,
		[CallerFilePath] string? source = null)
		=> ctx?.Write(LogLevel.Trace, null, exception, parameters, member, source, line);

	public static void Trace(this ILog? ctx,
		Exception? exception, 
		string? message, 
		object? parameters = null,
		[CallerLineNumber] int line = 0,
		[CallerMemberName] string? member = null,
		[CallerFilePath] string? source = null)
		=> ctx?.Write(LogLevel.Trace, message, exception, parameters, member, source, line);

	#endregion

	#region Debug
	public static void Debug(this ILog? ctx,
		string? message = null,
		object? parameters = null,
		[CallerLineNumber] int line = 0,
		[CallerMemberName] string? member = null,
		[CallerFilePath] string? source = null)
		=> ctx?.Write(LogLevel.Debug, message, null, parameters, member, source, line);

	public static void Debug(this ILog? ctx,
		Exception? exception, 
		object? parameters = null,
		[CallerLineNumber] int line = 0, 
		[CallerMemberName] string? member = null,
		[CallerFilePath] string? source = null)
		=> ctx?.Write(LogLevel.Debug, null, exception, parameters, member, source, line);

	public static void Debug(this ILog? ctx, 
		Exception? exception, 
		string? message, 
		object? parameters = null,
		[CallerLineNumber] int line = 0,
		[CallerMemberName] string? member = null,
		[CallerFilePath] string? source = null)
		=> ctx?.Write(LogLevel.Debug, message, exception, parameters, member, source, line);

	#endregion
	
	#region Trace
	public static void Write(this ILog? ctx, 
		LogLevel logLevel,
		string? message = null,
		object? parameters = null,
		[CallerLineNumber] int line = 0,
		[CallerMemberName] string? member = null,
		[CallerFilePath] string? source = null)
		=> ctx?.Write(logLevel, message, null, parameters, member, source, line);

	public static void Write(this ILog? ctx,
		LogLevel logLevel,
		Exception? exception, 
		object? parameters = null,
		[CallerLineNumber] int line = 0, 
		[CallerMemberName] string? member = null,
		[CallerFilePath] string? source = null)
		=> ctx?.Write(logLevel, null, exception, parameters, member, source, line);

	public static void Write(this ILog? ctx,
		LogLevel logLevel,
		Exception? exception, 
		string? message, 
		object? parameters = null,
		[CallerLineNumber] int line = 0,
		[CallerMemberName] string? member = null,
		[CallerFilePath] string? source = null)
		=> ctx?.Write(logLevel, message, exception, parameters, member, source, line);

	#endregion
	

	public static bool IsError(this ILogMessage message)
		=> message.Exception != null || message.Level.IsError();

	public static bool IsError(this LogLevel logLevel)
		=> logLevel is LogLevel.Error or LogLevel.Fatal;

	#region TryXXX



	public static bool TryInfo(this ILog? ctx, 
		string? message,
		[InstantHandle] Action action,
		object? parameters = null,
		[CallerLineNumber] int line = 0, 
		[CallerMemberName] string? member = null,
		[CallerFilePath] string? source = null)
	{
		try
		{
			action();
			ctx?.Write(LogLevel.Info, $"{message} success", null, parameters, member, source, line);
			return true;
		}
		catch (Exception exception)
		{
			ctx?.Write(LogLevel.Error, $"{message} fail: ", exception, parameters, member, source, line);
			return false;
		}
	}

	public static async Task<bool> TryInfo(this ILog? ctx,
		string? message, 
		[InstantHandle] Func<Task> action, 
		object? parameters = null,
		[CallerLineNumber] int line = 0, 
		[CallerMemberName] string? member = null,
		[CallerFilePath] string? source = null)
	{
		try
		{
			await action().NoCtx();
			ctx?.Write(LogLevel.Info, $"{message} success", null, parameters, member, source, line);
			return true;
		}
		catch (Exception exception)
		{
			ctx?.Write(LogLevel.Error, $"{message} fail: ", exception, parameters, member, source, line);
			return false;
		}
	}
		
	public static bool TryTrace(this ILog? ctx,
		string? message, 
		[InstantHandle] Action action,
		object? parameters = null,
		[CallerLineNumber] int line = 0, 
		[CallerMemberName] string? member = null, 
		[CallerFilePath] string? source = null)
	{
		try
		{
			action();
			ctx?.Write(LogLevel.Trace, $"{message} success", null, parameters, member, source, line);
			return true;
		}
		catch (Exception exception)
		{
			ctx?.Write(LogLevel.Error, $"{message} fail: ", exception, parameters, member, source, line);
			return false;
		}
	}
		
	public static async Task<bool> TryTrace(this ILog? ctx,
		string? message,
		[InstantHandle] Func<Task> action,
		object? parameters = null,
		[CallerLineNumber] int line = 0,
		[CallerMemberName] string? member = null,
		[CallerFilePath] string? source = null)
	{
		try
		{
			await action().NoCtx();
			ctx?.Write(LogLevel.Trace, $"{message} success", null, parameters, member, source, line);
			return true;
		}
		catch (Exception exception)
		{
			ctx?.Write(LogLevel.Error, $"{message} fail: ", exception, parameters, member, source, line);
			return false;
		}
	}
		
	public static bool TryDebug(this ILog? ctx,
		string? message, 
		[InstantHandle] Action action, 
		object? parameters = null,
		[CallerLineNumber] int line = 0,
		[CallerMemberName] string? member = null, 
		[CallerFilePath] string? source = null)
	{
		try
		{
			action();
			ctx?.Write(LogLevel.Debug, $"{message} success", null, parameters, member, source, line);
			return true;
		}
		catch (Exception exception)
		{
			ctx?.Write(LogLevel.Error, $"{message} fail: ", exception, parameters, member, source, line);
			return false;
		}
	}
		
	public static async Task<bool> TryDebug(this ILog? ctx,
		string? message, 
		[InstantHandle] Func<Task> action,
		object? parameters = null,
		[CallerLineNumber] int line = 0,
		[CallerMemberName] string? member = null,
		[CallerFilePath] string? source = null)
	{
		try
		{
			await action().NoCtx();
			ctx?.Write(LogLevel.Debug, $"{message} success", null, parameters, member, source, line);
			return true;
		}
		catch (Exception exception)
		{
			ctx?.Write(LogLevel.Error, $"{message} fail: ", exception, parameters, member, source, line);
			return false;
		}
	}
	#endregion
}