using System;

namespace Booster.Log;

[Serializable]
public enum LogLevel
{
	Fatal = 1,
	Error = 2,
	Warn = 4,
	Info = 8,
	Trace = 16,
	Debug = 32,
	All = 63
}