using System;
using System.Collections.Generic;
using System.Threading.Tasks;

#if !NETSTANDARD
using Booster.AsyncLinq;
#endif

#nullable enable

namespace Booster.Log;

public interface ILog : IAsyncDisposable, IDisposable
{
	string? Name { get; }
	LogLevel AcceptLevels { get; set; } 

	bool Async { get; set; }
	bool Enabled { get; set; }
	bool DateHeaders { get; set; }
		
	IMessageFormatter MessageFormatter { get; set; }
	Func<ILogMessage, bool>? MessageFilter  { get; set; }

	IDictionary<string, string> Parameters { get; } 
	List<string>? ParametersOfInterest { get; set; } 
		
	ILog CreateImpl(object? name, object? parameters);

	void Write(LogLevel level, string? message, Exception? exception, object? parameters, string? mem, string? src, int line, DateTime timeStamp = default);
	void Write(LogLevel level, string? message, Exception? exception, object? parameters, DateTime timeStamp = default);
	IDisposable? Write(ILogMessage message, IDisposable? disposable);

	Task FlushAsync();
	void Flush();
}