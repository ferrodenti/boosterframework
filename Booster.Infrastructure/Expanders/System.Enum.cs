using System;
using System.Collections.Generic;
using System.Linq;
using Booster.Reflection;

namespace Booster;

/// <summary>
/// Класс расширений для System.Enum
/// </summary>
public static class EnumExpander
{
	public static IEnumerable<T> GetValues<T>()
		=> Enum.GetNames(typeof(T)).Select(str => (T)Enum.Parse(typeof(T), str));

	public static object GetIntValue(object val) //TODO: remove
	{
		TypeEx type = val.GetType();

		if(type.IsEnum)
			switch (TypeEx.Get(Enum.GetUnderlyingType(val.GetType())).TypeCode)
			{
			case TypeCode.SByte:
				return (sbyte) val;
			case TypeCode.Byte:
				return (byte) val;
			case TypeCode.Int16:
				return (short) val;
			case TypeCode.UInt16:
				return (ushort) val;
			case TypeCode.Int32:
				return (int) val;
			case TypeCode.UInt32:
				return (uint) val;
			case TypeCode.Int64:
				return (long) val;
			case TypeCode.UInt64:
				return (ulong) val;
			}

		throw new ArgumentOutOfRangeException();
	}

	public static T Set<T>(this Enum type, T flag, bool value)
	{
		if (value)
			return TypeEx.Get(Enum.GetUnderlyingType(typeof(T))).TypeCode switch
			       {
				       TypeCode.SByte  => (T) (object) ((sbyte) (object) type | (sbyte) (object) flag),
				       TypeCode.Byte   => (T) (object) ((byte) (object) type | (byte) (object) flag),
				       TypeCode.Int16  => (T) (object) ((short) (object) type | (short) (object) flag),
				       TypeCode.UInt16 => (T) (object) ((ushort) (object) type | (ushort) (object) flag),
				       TypeCode.Int32  => (T) (object) ((int) (object) type | (int) (object) flag),
				       TypeCode.UInt32 => (T) (object) ((uint) (object) type | (uint) (object) flag),
				       TypeCode.Int64  => (T) (object) ((long) (object) type | (long) (object) flag),
				       TypeCode.UInt64 => (T) (object) ((ulong) (object) type | (ulong) (object) flag),
				       _               => throw new ArgumentOutOfRangeException()
			       };

		return type.Reset(flag);
	}


	public static T Reset<T>(this Enum type, T flag)
	{
		try
		{
			return TypeEx.Get(Enum.GetUnderlyingType(typeof(T))).TypeCode switch
			       {
				       TypeCode.SByte  => (T) (object) ((sbyte) (object) type & ~(sbyte) (object) flag),
				       TypeCode.Byte   => (T) (object) ((byte) (object) type & ~(byte) (object) flag),
				       TypeCode.Int16  => (T) (object) ((short) (object) type & ~(short) (object) flag),
				       TypeCode.UInt16 => (T) (object) ((ushort) (object) type & ~(ushort) (object) flag),
				       TypeCode.Int32  => (T) (object) ((int) (object) type & ~(int) (object) flag),
				       TypeCode.UInt32 => (T) (object) ((uint) (object) type & ~(uint) (object) flag),
				       TypeCode.Int64  => (T) (object) ((long) (object) type & ~(long) (object) flag),
				       TypeCode.UInt64 => (T) (object) ((ulong) (object) type & ~(ulong) (object) flag),
				       _               => throw new ArgumentOutOfRangeException()
			       };
		}
		catch (Exception ex)
		{
			throw new ArgumentException(
				$"Could not remove value from enumerated type '{typeof (T).Name}'.", ex);
		}
	}

	public static IEnumerable<TEnum> Split<TEnum>(this TEnum value) where TEnum : struct, IComparable, IConvertible, IFormattable
	{
		foreach (Enum v in Enum.GetValues(value.GetType()))
			if (((Enum)(object)value).HasFlag(v))
				yield return (TEnum) (object) v;
	}
}