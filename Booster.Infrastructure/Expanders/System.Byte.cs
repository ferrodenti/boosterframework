using System;
using System.Linq;
using System.Text;
using JetBrains.Annotations;

namespace Booster;

/// <summary>
/// Класс расширений для System.Byte
/// </summary>
[PublicAPI]
public static class ByteExpander
{
	[Obsolete("Use numeric helper")]
	public static bool IsPowerOfTwo(this byte x)
		=> x != 0 && (x & (x - 1)) == 0;

	public static unsafe byte[] Xor(this byte[] arr1, byte[] arr2)
	{
		var result = new byte[arr1.Length];
		var len = arr1.Length; //Remark: if len > 1Mb use Parallel.Invoke		
		var i = 0;
			
		if (len >= sizeof(ulong))
		{
			i = len / sizeof(ulong);
			fixed (byte* pRes = result, pArr1 = arr1, pArr2 = arr2)
			{
				var pr = (ulong*)pRes;
				var p1 = (ulong*)pArr1;
				var p2 = (ulong*)pArr2;
				var pEnd = p1 + i;
				while (p1 < pEnd)
				{
					*pr = *p1 ^ *p2;
					p1++;
					p2++;
					pr++;
				}
			}
			i *= sizeof(ulong);
		}
		for (; i < len; i++)
			result[i] = (byte)(arr1[i] ^ arr2[i]);

		return result;
	}

	public static unsafe void FillWith(this byte[] arr1, byte value)
	{
		var len = arr1.Length; //Remark: if len > 1Mb use Parallel.Invoke
		var i = 0;

		if (len >= sizeof(ulong) + sizeof(ulong))
		{
			for (; i < sizeof (ulong); i++)
				arr1[i] = value;

			i = len / sizeof(ulong);
			fixed (byte* pArr1 = arr1)
			{
				var p1 = (ulong*)pArr1;
				var pEnd = p1 + i;
				var v8 = *p1;
				while (++p1 < pEnd)
					*p1 = v8;
			}
			i = (i + 1)*sizeof (ulong);
		}
		for (; i < len; i++)
			arr1[i] = value;
	}

	public static byte[] Concat(this byte[] first, byte[] second)
	{
		var ret = new byte[first.Length + second.Length];
		Buffer.BlockCopy(first, 0, ret, 0, first.Length);
		Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
		return ret;
	}

	public static byte[] Concat(this byte[] first, byte[] second, byte[] third)
	{
		var ret = new byte[first.Length + second.Length + third.Length];
		Buffer.BlockCopy(first, 0, ret, 0, first.Length);
		Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
		Buffer.BlockCopy(third, 0, ret, first.Length + second.Length,
			third.Length);
		return ret;
	}

	public static byte[] Concat(this byte[] first, params byte[][] arrays)
	{
		var ret = new byte[first.Length + arrays.Sum(x => x.Length)];

		Buffer.BlockCopy(first, 0, ret, 0, first.Length);
		var offset = first.Length;

		foreach (var data in arrays)
		{
			Buffer.BlockCopy(data, 0, ret, offset, data.Length);
			offset += data.Length;
		}
		return ret;
	}

	public static string ToHexString(this byte[] array)
	{
		var hex = new StringBuilder(array.Length*2);
		foreach (var b in array)
			hex.AppendFormat("{0:x2}", b);

		return hex.ToString();
	}
		
	/// <summary>
	/// Performs a comparison of two byte arrays. 
	/// Performs in constant time to prevent timing/side channel attacks
	/// </summary>
	public static int CompareConstantTime(this byte[] a, byte[] b)
	{
		if (a == null)
			throw new ArgumentNullException(nameof(a));
		if (b == null)
			throw new ArgumentNullException(nameof(b));

		var aIsLarger = 0;
		var bIsLarger = 0;
		var lenCmp = a.Length.CompareTo(b.Length);

		unchecked
		{
			for (int i = 0, alen = a.Length, blen = b.Length, len = Math.Max(alen, blen); i < len; i++)
			{
				var byteA = i < alen ? a[i] : 0;
				var byteB = i < blen ? b[i] : 0;

				var byteAIsLarger = ((byteB - byteA) >> 8) & 1;
				var byteBIsLarger = ((byteA - byteB) >> 8) & 1;

				aIsLarger |= byteAIsLarger & ~bIsLarger;
				bIsLarger |= byteBIsLarger & ~aIsLarger;
			}
			var result = aIsLarger - bIsLarger;
			return result == 0 ? lenCmp : result;
		}
	}
}