using System;
using System.Collections.Concurrent;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Booster.Reflection;
using JetBrains.Annotations;

#nullable enable

namespace Booster;

[PublicAPI]
public static class TaskExpander
{
	// [Obsolete("Требуется разобраться: task.IsCompleted=true, если задача ждет")]
	// public static bool IsStopped(this Task task) 
	// 	=> task == null || task.IsCanceled || task.IsCompleted || task.IsFaulted;

	public static ConfiguredTaskAwaitable NoCtx(this Task task)
		=> task.ConfigureAwait(false);

	public static ConfiguredTaskAwaitable<TResult> NoCtx<TResult>(this Task<TResult> task)
		=> task.ConfigureAwait(false);

	public static TResult NoCtxResult<TResult>(this Task<TResult> task)
		=> task.ConfigureAwait(false).GetAwaiter().GetResult();

	public static void NoCtxWait(this Task task)
		=> task.ConfigureAwait(false).GetAwaiter().GetResult();

	//public static async Task<TResult?> WithAsync<TSource, TResult>(this Task<TSource?> source, Func<TSource, Task<TResult?>> selector)
	//{
	//	var src = await source.NoCtx();
	//	if (src == null)
	//		return default;

	//	return await selector(src).NoCtx();
	//}

	//public static async Task<TResult?> WithAsync<TSource, TResult>(this Task<TSource?> source, Func<TSource, TResult?> selector)
	//{
	//	var src = await source.NoCtx();
	//	return src == null ? default : selector(src);
	//}


	public static async Task Cancel(this Task task, CancellationToken token)
	{
		var cancel = new TaskCompletionSource<bool>();
		token.Register(s => ((TaskCompletionSource<bool>) s).SetResult(true), cancel);

		if (await Task.WhenAny(task, cancel.Task).NoCtx() == task)
			cancel.SetCanceled();

		token.ThrowIfCancellationRequested();
	}

	public static async Task<TResult?> Cancel<TResult>(this Task<TResult> task, CancellationToken token)
	{
		var cancel = new TaskCompletionSource<bool>();
		token.Register(s => ((TaskCompletionSource<bool>) s).SetResult(true), cancel);

		var result = default(TResult);
		if (await Task.WhenAny(task, cancel.Task).NoCtx() == task)
		{
			result = task.Result;
			cancel.SetCanceled();
		}

		token.ThrowIfCancellationRequested();
		return result;
	}

	static readonly ConcurrentDictionary<TypeEx, Method?> _taskResultGetters = new();

	static Method? GetResultGetter(Task task)
		=> _taskResultGetters.GetOrAdd(task.GetType(), t =>
		{
			if (!t.IsGenericType)
				return null;

			return t.FindProperty("Result")?.GetMethod;
		});

	public static async Task<object?> GetResultAsync(this Task task)
	{
		await task.NoCtx();
		var getter = GetResultGetter(task);
		if (getter != null)
			return await getter.InvokeAsync(task).NoCtx();

		return default;
	}

	public static async Task<TResult> CastAsync<TResult>(this Task task)
	{
		var result = await task.GetResultAsync().NoCtx();
		return (TResult) result!;
	}

	public static async Task<TResult?> SafeCastAsync<TSource, TResult>(this Task<TSource> task) where TResult : class
	{
		var result = await task.GetResultAsync().NoCtx();
		return result as TResult;
	}

	public static async Task<T> ClaimAsync<T>(this Task<T?> task, bool checkThrow = false)
	{
		var result = await task.NoCtx();

		if (checkThrow && result == null)
			throw new ArgumentNullException(nameof(task));

		return result!;
	}
}