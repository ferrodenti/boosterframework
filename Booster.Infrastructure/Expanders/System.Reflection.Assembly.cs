using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Booster.Reflection;

namespace Booster;

// ReSharper disable once InconsistentNaming
public static class AssemblyExpander
{
	public static TypeEx FindType(this Assembly assembly, string typeName, bool ignoreCase = false)
		=> FindTypes(assembly, typeName, ignoreCase).FirstOrDefault();

	public static IEnumerable<TypeEx> FindTypes( this Assembly assembly, string typeName, bool ignoreCase = false) //TODO: сделать по нормальному как всю другую рефлексию
	{
		IEnumerable<TypeEx> Enumerate()
		{
			var first = assembly.GetType(typeName, false, ignoreCase);
			if (first != null)
				yield return first;
			foreach (var type in assembly.DefinedTypes)
				if (type.Name == typeName || (ignoreCase && type.Name.EqualsIgnoreCase(typeName)))
					yield return type;
		}

		return Enumerate().Distinct(t => t);
	}
}