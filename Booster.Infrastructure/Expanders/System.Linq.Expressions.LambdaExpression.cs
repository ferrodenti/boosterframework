using System.Linq;
using System.Linq.Expressions;
using Booster.Parsing;
using Booster.Reflection;
using JetBrains.Annotations;

namespace Booster;

/// <summary>
/// Класс расширений для System.Linq.Expressions.LambdaExpression
/// </summary>
[PublicAPI]
public static class LambdaExpressionExpander
{
	public static BaseDataMember ParseMemberRef(this LambdaExpression expression, bool throwException = true)
	{
		BaseDataMember Except(string message)
		{
			if (throwException)
				throw new ParsingException(expression.ToString(), message);

			return null;
		}

		var exp = expression.Body;
		
		while (true)
			switch (exp.NodeType)
			{
			case ExpressionType.Convert:
				if (exp is not UnaryExpression uexp)
					return Except("Could not parse expression: UnaryExpression expected");

				exp = uexp.Operand;
				break;

			case ExpressionType.MemberAccess:
				if (exp is not MemberExpression mexp)
					return Except("Could not parse expression: MemberExpression expected");

				var reflectedType = TypeEx.Get(expression.Type).GenericArguments.FirstOrDefault();
				if (reflectedType != null)
				{
					if (reflectedType.Equals(TypeEx.Get(mexp.Member.DeclaringType!)))
						return mexp.Member;

					return reflectedType.FindDataMember(mexp.Member.Name);
				}
				return Except("Could not parse expression: wrong expression arguments");

			default:
				return Except("Could not parse expression: wrong NodeType");
			}

	}
}