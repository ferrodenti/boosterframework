
using System;
using System.Threading;

namespace Booster;

public static class ReaderWriterLockExpander
{
	public static IDisposable ReadLock(this ReaderWriterLock locker)
	{
		locker.AcquireReaderLock(-1);
		return new ActionDisposable(locker.ReleaseReaderLock);
	}
		
	public static IDisposable WriteLock(this ReaderWriterLock locker)
	{
		locker.AcquireWriterLock(-1);
		return new ActionDisposable(locker.ReleaseWriterLock);
	}
		
	public static IDisposable UpgradeableReadLock(this ReaderWriterLock locker)
	{
		locker.UpgradeToWriterLock(-1);
		return new ActionDisposable(locker.ReleaseWriterLock);
	}
}