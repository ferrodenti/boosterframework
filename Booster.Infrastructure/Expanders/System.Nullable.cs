using JetBrains.Annotations;

namespace Booster;

[PublicAPI]
public static class NullableExpander
{
	public static bool TryGetValue<T>(this T? nullable, out T value) where T : struct
	{
		if (!nullable.HasValue)
		{
			value = default;
			return false;
		}

		value = nullable.Value;
		return true;
	}
}
