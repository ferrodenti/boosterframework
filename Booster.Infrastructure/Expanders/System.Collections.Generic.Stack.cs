using System;
using System.Collections.Generic;

namespace Booster;

// ReSharper disable once InconsistentNaming
/// <summary>
/// Класс расширений для System.Collections.Generic.HashSet
/// </summary>
public static class StackExpander
{
	public static bool TryPeek<T>(this Stack<T> stack, out T result)
	{
		try
		{
			if (stack.Count > 0)
			{
				result = stack.Peek();
				return true;
			}
		}
		catch(InvalidOperationException)
		{
			// ignore
		}
		result = default;
		return false;
	}
}