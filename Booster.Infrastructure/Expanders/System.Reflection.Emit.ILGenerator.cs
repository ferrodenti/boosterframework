using System.Reflection.Emit;
using Booster.Reflection;

namespace Booster;

// ReSharper disable once InconsistentNaming
public static class ILGeneratorExpander
{
	public static void BoxIfNeeded(this ILGenerator generator, TypeEx type)
	{
		if (type.IsValueType && type != typeof(void))
			generator.Emit(OpCodes.Box, type);
	}

	public static void UnboxIfNeeded(this ILGenerator generator, TypeEx type)
	{
		if (type.IsValueType && type != typeof(void))
			generator.Emit(OpCodes.Unbox_Any, type);
	}
}