using System.Threading;

namespace Booster;

public static class CancellationTokenExpander
{
	public static CancellationToken Join(this CancellationToken token, CancellationToken other)
	{
		if (token == default)
			return other;
		if (other == default)
			return token;

		return CancellationTokenSource.CreateLinkedTokenSource(token, other).Token;
	}
}