using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Linq;
using System.Reflection;
using Booster.Reflection;
using JetBrains.Annotations;

namespace Booster;

/// <summary>
/// Класс расширений для System.Collections.IEnumerable
/// </summary>
// ReSharper disable once InconsistentNaming
public static class IEnumerableExpander
{
	[UsedImplicitly, Obfuscation(Exclude = true)]
	static T[] ToArray<T>(IEnumerable pks) 
		=> pks.Cast<T>().ToArray();
		
	static readonly Lazy<Method> _toArrayMethod = new(() => TypeEx.Get(typeof(IEnumerableExpander)).FindMethod(nameof(ToArray))); 
	static readonly ConcurrentDictionary<TypeEx, Method> _toArrayCache = new();

	static Method GetToArrayMethod(TypeEx elementType)
		=> _toArrayCache.GetOrAdd(elementType, k => _toArrayMethod.Value.MakeGenericMethod(k));
	
	public static Array ToArray(this IEnumerable source, TypeEx elementType) 
		=> (Array) GetToArrayMethod(elementType).Invoke(null, source);

	public static Array AsArray(this IEnumerable source, TypeEx elementType)
	{
		if (elementType.MakeArrayType().IsAssignableFrom(TypeEx.Of(source)))
			return (Array) source;

		return ToArray(source, elementType);
	}
}