using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Booster.Synchronization;

#nullable enable

namespace Booster;

// ReSharper disable once InconsistentNaming
/// <summary>
/// Класс расширений для System.Collections.Generic.HashSet
/// </summary>
public static class HashSetTExpander
{
	public static T GetOrAdd<T>(this HashSet<T> set, T key, Func<T> creator)
	{
		if (!set.Contains(key))
			lock (set)
				if (!set.Contains(key))
				{
					var res = creator();
					return set.Add(res) ? res : key;
				}

		return key;
	}

	public static async Task<T> GetOrAddAsync<T>(this HashSet<T> set, AsyncLock lck, T key, Func<Task<T>> creator)
	{
		if (!set.Contains(key))
		{
			using var _ = await lck.LockAsync().NoCtx();
			if (!set.Contains(key))
			{
				var res = await creator().NoCtx();
				return set.Add(res) ? res : key;
			}

		}

		return key;
	}

	public static int AddRange<T>(this HashSet<T> set, IEnumerable<T> values)
	{
		var result = 0;
		foreach (var value in values)
			if (set.Add(value))
				++result;

		return result;
	}
}