using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Booster.AsyncLinq;
using Booster.DI;
using Booster.Helpers;
using Booster.Log;
using Booster.Reflection;
using JetBrains.Annotations;

#nullable enable

namespace Booster;

/// <summary>
/// Класс расширений для System.Collections.Generic.IEnumerable&lt;T&gt;
/// </summary>
[PublicAPI]
// ReSharper disable once InconsistentNaming
public static class IEnumerableTExpander
{
	class EnumerableFromEnumerator<T> : IEnumerable<T>
	{
		readonly IEnumerator<T> _enumerator;

		public EnumerableFromEnumerator(IEnumerator<T> enumerator)
			=> _enumerator = enumerator;

		public IEnumerator<T> GetEnumerator() => _enumerator;
		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
	}

	public static IEnumerable<T> WhereSome<T>(
		this IEnumerable<T?>? source)
	{
		if(source != null)
			foreach(var obj in source)
				if (obj != null)
					yield return obj;
	}

	public static IEnumerable<TResult> WhereSome<TSource, TResult>(
		this IEnumerable<TSource?>? source, 
		Func<TSource, TResult?> selector)
	{
		if (source != null)
			foreach (var obj in source)
				if (obj != null)
				{
					var result = selector(obj);
					if (result != null)
						yield return result;
				}
	}

	/// <summary>
	/// Выполняет процедуру action с каждым элементом последовательности
	/// </summary>
	/// <typeparam name="T">Тип элементов последовательности</typeparam>
	/// <param name="source">Последовательность</param>
	/// <param name="action">Процедура</param>
	/// <returns>Ту же самую последовательность source</returns>
	public static IEnumerable<T> WithEvery<T>(this IEnumerable<T> source, Action<T> action) //TODO: => ForEach
		=> source.Select(item =>
		{
			action(item);
			return item;
		});

	/// <summary>
	/// Выполняет асинхронную процедуру action с каждым элементом последовательности
	/// </summary>
	/// <typeparam name="T">Тип элементов последовательности</typeparam>
	/// <param name="source">Последовательность</param>
	/// <param name="action">Асинхронная процедура</param>
	/// <returns>Ту же самую последовательность source</returns>
	public static IAsyncEnumerable<T> WithEvery<T>(this IEnumerable<T> source, Func<T, Task> action)
		=> source.SelectAsync(async item =>
		{
			await action(item).NoCtx();
			return item;
		});

	/// <summary>
	/// Возвращает ту же последовательность повторяющую <paramref name="source"/>, но при этом извлекает первый элемент тем самым исполняя код предшествующий первому yield return.
	/// </summary>
	/// Тип элементов последовательности <paramref name="source"/>.
	/// <param name="source">Последовательность</param>
	/// <returns>Последовательность, повторяющая <paramref name="source"/></returns>
	/// <exception cref="T:System.ArgumentNullException"><paramref name="source"/> равен null.</exception>
	public static IEnumerable<T> Now<T>(this IEnumerable<T> source)
	{
		if (source == null) throw new ArgumentNullException(nameof(source));

		var enumerator = source.GetEnumerator();
		if (enumerator.MoveNext())
		{
			var first = enumerator.Current;
			return new EnumerableFromEnumerator<T>(enumerator).AppendBefore(first);
		}
		return Enumerable.Empty<T>();
	}

	public static IEnumerable<(T, T)> Pairwise<T>(this IEnumerable<T> ie, bool throwOdd = true)
	{
		var item1 = default(T);
		var alt = false;
		foreach (var val in ie)
		{
			if (alt)
				yield return (item1!, val);
			else
				item1 = val;

			alt = !alt;
		}

		if (alt)
		{
			if (throwOdd)
				throw new InvalidOperationException("A enumerable has odd number of items");
				
			yield return (item1!, default!);
		}
	}

	public static IEnumerable<(T1, T2)> Pairwise<T1, T2>(this IEnumerable<T1> first, IEnumerable<T2> second, Func<bool[], bool>? continuation = null)
	{
		using var ie1 = first.GetEnumerator();
		using var ie2 = second.GetEnumerator();
		while (true)
		{
			var nxt1 = ie1.MoveNext();
			var nxt2 = ie2.MoveNext();

			if (!nxt1 && !nxt2)
				yield break;

			if (nxt1 != nxt2)
			{
				if (continuation == null)
					throw new InvalidOperationException("A enumerables sizes missmatch");

				if (!continuation(new[] { nxt1, nxt2 }))
					yield break;
			}

			yield return (
				nxt1 ? ie1.Current : default!,
				nxt2 ? ie2.Current : default!);
		}
	}

	public static IEnumerable<Tuple<T1, T2, T3>> Pairwise<T1, T2, T3>(this IEnumerable<T1> first, IEnumerable<T2> second, IEnumerable<T3> third, Func<bool[], bool>? continuation = null)
	{
		using var ie1 = first.GetEnumerator();
		using var ie2 = second.GetEnumerator();
		using var ie3 = third.GetEnumerator();
		while (true)
		{
			var nxt1 = ie1.MoveNext();
			var nxt2 = ie2.MoveNext();
			var nxt3 = ie3.MoveNext();

			if (!nxt1 && !nxt2 && !nxt3)
				yield break;

			if (nxt1 != nxt2 || nxt1 != nxt3)
			{
				if (continuation == null)
					throw new InvalidOperationException("A enumerables sizes missmatch");

				if(!continuation(new []{nxt1, nxt2, nxt3}))
					yield break;
			}

			yield return Tuple.Create(
				nxt1 ? ie1.Current : default!,
				nxt2 ? ie2.Current : default!,
				nxt3 ? ie3.Current : default!);
		}
	}

	public static IEnumerable<TTuple> Pairwise<TTuple>(this IEnumerable first, IEnumerable[] other, Func<bool[], bool>? continuation = null)
	{
		var resultType = TypeEx.Get<TTuple>();
		var enumerators = other.Select(ie => ie.GetEnumerator()).AppendBefore(first.GetEnumerator()).ToArray();
		var length = enumerators.Length;
		var ctor = resultType.FindConstructor(new ReflectionFilter {Access = AccessModifier.Public, ParametersCount = length});
		if (ctor == null)
			throw new ReflectionException($"{resultType}: cannot find a public constructor with {length} parameters");

		object?[]? defaults = null;
		try
		{
			var next = new bool[length];
			var values = new object?[length];
			while (true)
			{
				var stop = true;
				var any = false;
				for (var i = 0; i < length; ++i)
				{
					var enumerator = enumerators[i];
					var n = enumerator.MoveNext();
					next[i] = n;
					if (n)
					{
						stop = false;
						values[i] = enumerator.Current!;
					}
					else
					{
						any = true;
						if (defaults == null)
						{
							defaults = new object[length];
							for (var j = 0; j < length; ++j)
								defaults[j] = ctor.ParameterTypes[j].DefaultValue;
						}

						values[i] = defaults[i];
					}
				}

				if(stop)
					yield break;

				if (any)
				{
					if (continuation == null)
						throw new InvalidOperationException("A enumerables sizes missmatch");

					if (!continuation(next))
						yield break;
				}
				yield return (TTuple) resultType.Create(values);
			}
		}
		finally
		{
			foreach(var e in enumerators)
				if (e is IDisposable disposable)
					disposable.Dispose();
		}
	}

	public static IEnumerable<T> AppendBefore<T>(this IEnumerable<T>? ie, T first)
	{
		yield return first;

		if(ie != null)
			foreach (var o in ie)
				yield return o;
	}

	public static IEnumerable<T> AppendAfter<T>(this IEnumerable<T>? ie, T last)
	{
		if(ie != null)
			foreach (var o in ie)
				yield return o;

		yield return last;
	}

	public static IEnumerable<T> Concat<T>(this IEnumerable<T> first, IEnumerable<T> second, IEnumerable<T> third, params IEnumerable<T>[] other)
	{
		foreach (var item in first)
			yield return item;
		
		foreach (var item in second)
			yield return item;
		
		foreach (var item in third)
			yield return item;

		foreach (var ie in other)
		foreach (var item in ie)
			yield return item;
	}

	public static T[] AsArray<T>(this IEnumerable<T>? ie) 
		=> ie as T[] ?? ie?.ToArray() ?? Array.Empty<T>();
		
	public static List<T> AsList<T>(this IEnumerable<T>? ie) 
		=> ie as List<T> ?? ie?.ToList() ?? new List<T>();

	public static ICollection<T> AsCollection<T>(this IEnumerable<T>? ie)
		=> ie as ICollection<T> ?? ie?.ToArray() ?? (ICollection<T>) new List<T>();
	
	public static IEnumerable<T> WhereCast<T>(this IEnumerable? ie)
	{
		if (ie != null)
			foreach (var value in ie)
				if (value is T result)
					yield return result;
	}

	public static string ToString<T>(this IEnumerable<T?>? ie, EnumBuilder? enumBuilder = null)
	{
		enumBuilder ??= new EnumBuilder(", ");

		if (ie != null)
			foreach (var obj in ie)
				enumBuilder.Append(obj);

		return enumBuilder;
	}

	public static string ToString<T>(this IEnumerable<T?>? ie, Func<T, object?> selector, EnumBuilder? enumBuilder = null)
	{
		if (enumBuilder == null)
			enumBuilder = new EnumBuilder(", ");
		else if (!enumBuilder.IsEmpty)
			enumBuilder.Clear();

		if (ie != null)
			foreach (var obj in ie!.Select(selector))
				enumBuilder.Append(obj);

		return enumBuilder;
	}
		
	public static IEnumerable<TSource> Range<TSource>(this IEnumerable<TSource> source, int start, int count)
	{
		var i = 0;
		var end = start + count;

		if( source is IList<TSource> list )
		{
			end = Math.Min(end, list.Count);

			for (i = start; i < end; i++)
				yield return list[i];

			yield break;
		}

		foreach(var obj in source)
		{
			if (i >= end)
				yield break;

			if (i >= start)
				yield return obj;

			i++;
		}
	}
	
	public static IEnumerable<Tuple<T, bool>> Alternate<T>(this IEnumerable<T> ie)
	{
		var alt = false;
		foreach (var obj in ie)
		{
			yield return Tuple.Create(obj, alt);
			alt = !alt;
		}
	}

	[Obsolete("Use Shuffle method")]
	public static IEnumerable<TSource> Random<TSource>(this IEnumerable<TSource> source)
		=> Shuffle(source);

	public static IEnumerable<TSource> Shuffle<TSource>(this IEnumerable<TSource> source)
		=> Shuffle(source, new Random((int)DateTime.Now.Ticks));

	public static IEnumerable<TSource> Shuffle<TSource>(this IEnumerable<TSource> source, Random random)
	{
		var list = source.ToArray();

		for (var i = 0; i < list.Length; i++)
		{
			var j = random.Next(i, list.Length);
			yield return list[j];

			list[j] = list[i];
		}
	}

	public static bool TryGetFirst<TSource>(this IEnumerable<TSource>? source, out TSource result)
	{
		if(source != null)
			foreach (var first in source)
			{
				result = first;
				return true;
			}

		result = default!;
		return false;
	}
		
	public static bool TryGetLast<TSource>(this IEnumerable<TSource>? source, out TSource result)
	{
		result = default!;
		if (source != null)
		{
			if (source is IList<TSource> list)
			{
				var count = list.Count;
				if (count > 0)
				{
					result = list[count - 1];
					return true;
				}
			}
			else
			{
				foreach (var first in source)
					result = first;

				return true;
			}
		}
		return false;
	}

	public static IEnumerable<TSource> NonDefault<TSource>(
		this IEnumerable<TSource?> source,
		TSource? @default = default)
		=> source
			.Where(obj => !Equals(obj, @default))
			.Select(s => s!);

	public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
	{
		foreach (var item in source)
			action(item);
	}

	public static IEnumerable<TResult> Distinct<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult> selector) 
		=> source.Select(selector).Distinct();
		
	//public static IEnumerable<TSource> Distinct<TSource>(this IEnumerable<TSource> source, Comparison<TSource> comparison) 
	//	=> source.Distinct(); //TODO


	public static IEnumerable<TSource> OrEmpty<TSource>(this IEnumerable<TSource>? source)
		=> source ?? Enumerable.Empty<TSource>();
		
	public static void Nop<TSource>(this IEnumerable<TSource>? source)
	{
		if (source != null && source.Count() < 0)
			InstanceFactory.Get<ILog>(false)?.Error("Чудо!");
	}

	public static bool None<TSource>(this IEnumerable<TSource>? source, [InstantHandle] Func<TSource, bool> condition)
	{
		if (source != null)
			foreach (var item in source)
				if (condition(item))
					return false;

		return true;
	}

	public static int IndexOf<T>(this IEnumerable<T>? source, Func<T, bool> predicate)
	{
		if (source != null)
		{
			var result = 0;
			foreach (var obj in source)
			{
				if (predicate(obj))
					return result;
					
				result++;
			}
		}
		return -1;
	}

	public static IEnumerable<T?> PadRight<T>(this IEnumerable<T> ie, int count)
		=> PadRight(ie, count, default);

	public static IEnumerable<T> PadRight<T>(this IEnumerable<T> ie, int count, T def)
	{
		foreach (var item in ie)
		{
			yield return item;
			--count;
		}

		for (; count > 0; --count)
			yield return def;
	}

	#region HasSameElements

	public static bool HasSameElements<T>(this IEnumerable<T>? source, IEnumerable<T>? target)
		=> HasSameElements(source, target, EqualityComparer<T>.Default);

	public static bool HasSameElements<T>(this IEnumerable<T>? source, IEnumerable<T>? target, IEqualityComparer<T> comparer)
	{
		if (source == null)
			return target == null;
		if (target == null)
			return false;

		var nulls = 0;
		var counts = new Dictionary<T, int>(comparer);

		foreach (var i in source)
			if (i == null)
				++nulls;
			else if (counts.TryGetValue(i, out var c))
				counts[i] = c + 1;
			else
				counts[i] = 1;

		foreach (var i in target)
		{
			if (i == null)
			{
				if (--nulls < 0)
					return false;

				continue;
			}

			if (!counts.TryGetValue(i, out var c))
				return false;

			if (c == 1)
				counts.Remove(i);
			else
				counts[i] = c - 1;
		}

		return counts.Count == 0 && nulls == 0;
	}

	public static bool HasSameElementsDistinct<T>(this IEnumerable<T?>? source, IEnumerable<T?>? target)
		=> HasSameElementsDistinct(source, target, EqualityComparer<T?>.Default);

	public static bool HasSameElementsDistinct<T>(this IEnumerable<T?>? source, IEnumerable<T?>? target, IEqualityComparer<T?> comparer)
	{
		if (source == null)
			return target == null;
		if (target == null)
			return false;

		var nulls = 0;
		var counts = new Dictionary<T, int>(comparer);

		foreach (var i in source)
			if (i == null)
				nulls = 1;
			else
				counts[i] = 1;

		foreach (var i in target)
		{
			if (i == null)
			{
				if (nulls == 0)
					return false;

				nulls = 2;
				continue;
			}

			if (!counts.ContainsKey(i))
				return false;

			counts[i] = 2;
		}

		return counts.Values.All(v => v == 2) && nulls != 1;
	}

	#endregion

	#region Partition

	class PartitionEnumerator<T> : IEnumerator<T>
	{
		readonly IEnumerator<T> _inner;
		int _count;
			
		public bool Eoc { get; private set; }

		public PartitionEnumerator(IEnumerator<T> inner, int count)
		{
			_inner = inner;
			_count = count;
		}
			
		public bool MoveNext()
		{
			if (_count-- <= 0)
				return false;
				
			if (!_inner.MoveNext())
			{
				Eoc = true;
				return false;
			}
				
			return true;
		}

		public void Reset()
		{
		}

		public T Current => _inner.Current;

		object? IEnumerator.Current => Current;

		public void Dispose()
		{
		}
	}

	class PartitionEnumerable<T> : IEnumerable<T>
	{
		readonly IEnumerator<T> _inner;
		readonly int _count;
		PartitionEnumerator<T>? _partitionEnumerator;

		public bool Eoc => _partitionEnumerator?.Eoc ?? false;

		public PartitionEnumerable(IEnumerator<T> inner, int count)
		{
			_inner = inner;
			_count = count;
		}

		public IEnumerator<T> GetEnumerator()
			=> _partitionEnumerator ??= new PartitionEnumerator<T>(_inner, _count);

		IEnumerator IEnumerable.GetEnumerator()
			=> GetEnumerator();
	}

	public static IEnumerable<IEnumerable<T>> Partition<T>(this IEnumerable<T> source, int size)
	{
		IEnumerable<IEnumerable<T>> IeProc()
		{
			using var ie = source.GetEnumerator();
			while (true)
			{
				var enumerable =  new PartitionEnumerable<T>(ie, size);
				yield return enumerable;

				if (enumerable.Eoc)
					break;
			}
		}

		return size <= 0 ? new[] {source} : IeProc();
	}

	#endregion

	public static bool SafeAny<T>(this IEnumerable<T>? source)
		=> source?.Any() == true;
	public static bool SafeAny<T>(this IEnumerable<T>? source, Func<T, bool> predicate)
		=> source?.Where(predicate).Any() == true;

	#region ToDictionarySafe

	public static Dictionary<TKey, TSource> ToDictionarySafe<TSource, TKey>(
		this IEnumerable<TSource>? source,
		Func<TSource, TKey> keySelector,
		IEqualityComparer<TKey>? comparer = null)
		=> ToDictionarySafe(source, keySelector, e => e, comparer);

	public static Dictionary<TKey, TElement> ToDictionarySafe<TSource, TKey, TElement>(
		this IEnumerable<TSource>? source,
		Func<TSource, TKey>? keySelector,
		Func<TSource, TElement>? elementSelector,
		IEqualityComparer<TKey>? comparer = null)
	{
		if (keySelector == null)
			throw new ArgumentNullException(nameof(keySelector));
		if (elementSelector == null)
			throw new ArgumentNullException(nameof(elementSelector));

		var dictionary = new Dictionary<TKey, TElement>(comparer);

		if (source != null)
			foreach (var obj in source)
				Try.IgnoreErrors(() =>
				{
					var key = keySelector(obj);
					if (key != null)
						dictionary[key] = elementSelector(obj);
					else
						Utils.Nop();
				});

		return dictionary;
	}

	public static TDict ToDictionarySafe<TSource, TKey, TElement, TDict>(
		this IEnumerable<TSource>? source,
		Func<TSource, TKey>? keySelector = null,
		Func<TSource, TElement>? elementSelector = null,
		IEqualityComparer<TKey>? comparer = null,
		Func<TDict>? creator = null)
		where TDict : IDictionary<TKey, TElement>
	{
		if (keySelector == null)
		{
			if (typeof(TKey) != typeof(TElement))
				throw new ArgumentNullException(nameof(keySelector));

			keySelector = item => (TKey)(object)item!;
		}

		if (elementSelector == null)
		{
			if (typeof(TSource) != typeof(TElement))
				throw new ArgumentNullException(nameof(elementSelector));

			elementSelector = item => (TElement)(object)item!;
		}
		comparer ??= EqualityComparer<TKey>.Default;

		TDict result;
		if (creator != null)
			result = creator();
		else
			result = (TDict)TypeEx.Get<TDict>().Create(comparer);

		if (source != null)
			foreach (var item in source)
				Try.IgnoreErrors(() =>
				{
					var key = keySelector(item);
					if (key != null)
						result[key] = elementSelector(item);
					else
						Utils.Nop();
				});

		return result;
	}

	#endregion

	#region MinOrDefault

	public static int MinOrDefault(this IEnumerable<int>? source, int defValue = 0)
	{
		if (source == null) 
			return defValue;
		
		var value = 0;
		var hasValue = false;

		foreach (var x in source)
			if (hasValue)
			{
				if (x < value) value = x;
			}
			else
			{
				value = x;
				hasValue = true;
			}

		return hasValue ? value : defValue;
	}

	public static long MinOrDefault(this IEnumerable<long>? source, long defValue = 0)
	{
		if (source == null) 
			return defValue;

		long value = 0;
		var hasValue = false;
		foreach (var x in source)
			if (hasValue)
			{
				if (x < value) value = x;
			}
			else
			{
				value = x;
				hasValue = true;
			}

		return hasValue ? value : defValue;
	}

	public static float MinOrDefault(this IEnumerable<float>? source, float defValue = 0)
	{
		if (source == null) 
			return defValue;

		float value = 0;
		var hasValue = false;
		foreach (var x in source)
			if (hasValue)
			{
				// Normally NaN < anything is false, as is anything < NaN 
				// However, this leads to some irksome outcomes in MinOrDefault and MaxOrDefault.
				// If we use those semantics then MinOrDefault(NaN, 5.0) is NaN, but 
				// MinOrDefault(5.0, NaN) is 5.0!  To fix this, we impose a total 
				// ordering where NaN is smaller than every value, including
				// negative infinity. 
				if (x < value || float.IsNaN(x)) value = x;
			}
			else
			{
				value = x;
				hasValue = true;
			}

		return hasValue ? value : defValue;
	}

	public static double MinOrDefault(this IEnumerable<double>? source, double defValue = 0)
	{
		if (source == null) 
			return defValue;

		double value = 0;
		var hasValue = false;
		foreach (var x in source)
			if (hasValue)
			{
				if (x < value || double.IsNaN(x)) value = x;
			}
			else
			{
				value = x;
				hasValue = true;
			}

		return hasValue ? value : defValue;
	}

	public static decimal MinOrDefault(this IEnumerable<decimal>? source, decimal defValue = 0)
	{
		if (source == null) 
			return defValue;

		decimal value = 0;
		var hasValue = false;
		foreach (var x in source)
			if (hasValue)
			{
				if (x < value) value = x;
			}
			else
			{
				value = x;
				hasValue = true;
			}

		return hasValue ? value : defValue;
	}

	public static TSource MinOrDefault<TSource>(this IEnumerable<TSource>? source, TSource defValue)
	{
		if (source == null)
			return defValue;
		
		var comparer = Comparer<TSource>.Default;
		var value = default(TSource);
		// ReSharper disable CompareNonConstrainedGenericWithNull
		if (value == null)
		{
			foreach (var x in source)
				if (x != null && (value == null || comparer.Compare(x, value) < 0))
					value = x;

			return value ?? defValue;
		}
		// ReSharper restore CompareNonConstrainedGenericWithNull
		var hasValue = false;
		foreach (var x in source)
			if (hasValue)
			{
				if (comparer.Compare(x, value) < 0)
					value = x;
			}
			else
			{
				value = x;
				hasValue = true;
			}

		return hasValue ? value : defValue;
	}

	public static int MinOrDefault<TSource>(this IEnumerable<TSource>? source, Func<TSource, int> selector, int defValue = 0)
		=> (source?.Select(selector)).MinOrDefault(defValue);
		
	public static long MinOrDefault<TSource>(this IEnumerable<TSource>? source, Func<TSource, long> selector, long defValue = 0)
		=> (source?.Select(selector)).MinOrDefault(defValue);

	public static float MinOrDefault<TSource>(this IEnumerable<TSource>? source, Func<TSource, float> selector, float defValue = 0)
		=> (source?.Select(selector)).MinOrDefault(defValue);

	public static double MinOrDefault<TSource>(this IEnumerable<TSource>? source, Func<TSource, double> selector, double defValue = 0)
		=> (source?.Select(selector)).MinOrDefault(defValue);

	public static decimal MinOrDefault<TSource>(this IEnumerable<TSource>? source, Func<TSource, decimal> selector, decimal defValue = 0)
		=> (source?.Select(selector)).MinOrDefault(defValue);

	public static TResult MinOrDefault<TSource, TResult>(this IEnumerable<TSource>? source, Func<TSource, TResult> selector, TResult defValue = default!)
		=> (source?.Select(selector)).MinOrDefault(defValue);

	#endregion

	#region MaxOrDefault
	public static int MaxOrDefault(this IEnumerable<int>? source, int defValue = 0)
	{
		if (source == null) 
			return defValue;
		
		var value = 0;
		var hasValue = false;
		foreach (var x in source)
			if (hasValue)
			{
				if (x > value) value = x;
			}
			else
			{
				value = x;
				hasValue = true;
			}

		return hasValue ? value : defValue;
	}

	public static long MaxOrDefault(this IEnumerable<long>? source, long defValue = 0)
	{
		if (source == null) 
			return defValue;
		
		long value = 0;
		var hasValue = false;
		foreach (var x in source)
			if (hasValue)
			{
				if (x > value) value = x;
			}
			else
			{
				value = x;
				hasValue = true;
			}

		return hasValue ? value : defValue;
	}

	public static double MaxOrDefault(this IEnumerable<double>? source, double defValue = 0)
	{
		if (source == null) 
			return defValue;

		double value = 0;
		var hasValue = false;
		foreach (var x in source)
			if (hasValue)
			{
				if (x > value || double.IsNaN(value)) value = x;
			}
			else
			{
				value = x;
				hasValue = true;
			}
		return hasValue ? value : defValue;
	}

	public static float MaxOrDefault(this IEnumerable<float>? source, float defValue = 0)
	{
		if (source == null) 
			return defValue;

		float value = 0;
		var hasValue = false;
		foreach (var x in source)
			if (hasValue)
			{
				if (x > value || double.IsNaN(value)) value = x;
			}
			else
			{
				value = x;
				hasValue = true;
			}

		return hasValue ? value : defValue;
	}

	public static decimal MaxOrDefault(this IEnumerable<decimal>? source, decimal defValue = 0)
	{
		if (source == null) 
			return defValue;

		decimal value = 0;
		var hasValue = false;
		foreach (var x in source)
			if (hasValue)
			{
				if (x > value) value = x;
			}
			else
			{
				value = x;
				hasValue = true;
			}

		return hasValue ? value : defValue;
	}

		

	public static TSource MaxOrDefault<TSource>(this IEnumerable<TSource>? source, TSource defValue = default!)
	{
		if (source == null) 
			return defValue;

		var comparer = Comparer<TSource>.Default;
		var value = default(TSource);
		// ReSharper disable CompareNonConstrainedGenericWithNull
		if (value == null)
		{
			foreach (var x in source)
				if (x != null && (value == null || comparer.Compare(x, value) > 0))
					value = x;
			
			return value ?? defValue;
		}
		// ReSharper restore CompareNonConstrainedGenericWithNull
		var hasValue = false;
		foreach (var x in source)
			if (hasValue)
			{
				if (comparer.Compare(x, value) > 0)
					value = x;
			}
			else
			{
				value = x;
				hasValue = true;
			}

		return hasValue ? value : defValue;
	}

	public static int MaxOrDefault<TSource>(this IEnumerable<TSource>? source, Func<TSource, int> selector, int defValue = 0)
		=> (source?.Select(selector)).MaxOrDefault(defValue);

	public static long MaxOrDefault<TSource>(this IEnumerable<TSource>? source, Func<TSource, long> selector, long defValue = 0)
		=> (source?.Select(selector)).MaxOrDefault(defValue);

	public static float MaxOrDefault<TSource>(this IEnumerable<TSource>? source, Func<TSource, float> selector, float defValue = 0)
		=> (source?.Select(selector)).MaxOrDefault(defValue);

	public static double MaxOrDefault<TSource>(this IEnumerable<TSource>? source, Func<TSource, double> selector, double defValue = 0)
		=> (source?.Select(selector)).MaxOrDefault(defValue);

	public static decimal MaxOrDefault<TSource>(this IEnumerable<TSource>? source, Func<TSource, decimal> selector, decimal defValue = 0)
		=> (source?.Select(selector)).MaxOrDefault(defValue);

	public static TResult MaxOrDefault<TSource, TResult>(this IEnumerable<TSource>? source, Func<TSource, TResult> selector, TResult defValue = default!)
		=> (source?.Select(selector)).MaxOrDefault(defValue);

	#endregion

	#region SumOrDefault

	public static int SumOrDefault(this IEnumerable<int>? source, int defValue = 0)
	{
		if (source == null) 
			return defValue;
		
		var value = 0;
		var hasValue = false;
		foreach (var x in source)
		{
			value += x;
			hasValue = true;
		}

		return hasValue ? value : defValue;
	}

	public static long SumOrDefault(this IEnumerable<long>? source, long defValue = 0)
	{
		if (source == null) 
			return defValue;

		long value = 0;
		var hasValue = false;
		foreach (var x in source)
		{
			value += x;
			hasValue = true;
		}

		return hasValue ? value : defValue;
	}

	public static double SumOrDefault(this IEnumerable<double>? source, double defValue = 0)
	{
		if (source == null) 
			return defValue;

		double value = 0;
		var hasValue = false;
		foreach (var x in source)
		{
			value += x;
			hasValue = true;
		}
		return hasValue ? value : defValue;
	}

	public static float SumOrDefault(this IEnumerable<float>? source, float defValue = 0)
	{
		if (source == null) 
			return defValue;

		float value = 0;
		var hasValue = false;
		foreach (var x in source)
		{
			value += x;
			hasValue = true;
		}

		return hasValue ? value : defValue;
	}

	public static decimal SumOrDefault(this IEnumerable<decimal>? source, decimal defValue = 0)
	{
		if (source == null) 
			return defValue;

		decimal value = 0;
		var hasValue = false;
		foreach (var x in source)
		{
			value += x;
			hasValue = true;
		}

		return hasValue ? value : defValue;
	}
	public static TSource SumOrDefault<TSource>(this IEnumerable<TSource>? source, TSource defValue = default!)
	{
		if (source == null) 
			return defValue;

		object res = default(TSource)!;
		var hasValue = false;

		foreach (var item in source)
		{
			res = NumericHelper.Sum(res, item);
			hasValue = true;
		}

		return hasValue ? (TSource) res : defValue;
	}

	public static int SumOrDefault<TSource>(this IEnumerable<TSource>? source, Func<TSource, int> selector, int defValue = 0)
		=> (source?.Select(selector)).SumOrDefault(defValue);

	public static long SumOrDefault<TSource>(this IEnumerable<TSource>? source, Func<TSource, long> selector, long defValue = 0)
		=> (source?.Select(selector)).SumOrDefault(defValue);

	public static float SumOrDefault<TSource>(this IEnumerable<TSource>? source, Func<TSource, float> selector, float defValue = 0)
		=> (source?.Select(selector)).SumOrDefault(defValue);

	public static double SumOrDefault<TSource>(this IEnumerable<TSource>? source, Func<TSource, double> selector, double defValue = 0)
		=> (source?.Select(selector)).SumOrDefault(defValue);

	public static decimal SumOrDefault<TSource>(this IEnumerable<TSource>? source, Func<TSource, decimal> selector, decimal defValue = 0)
		=> (source?.Select(selector)).SumOrDefault(defValue);

	public static TResult SumOrDefault<TSource, TResult>(this IEnumerable<TSource>? source, Func<TSource, TResult> selector, TResult defValue = default!)
		=> (source?.Select(selector)).SumOrDefault(defValue);

	#endregion
}