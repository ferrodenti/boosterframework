using Booster.Parsing;
using JetBrains.Annotations;

namespace Booster;

/// <summary>
/// Класс расширений для System.Char
/// </summary>
[PublicAPI]
public static class CharExpander
{
	static StringAnalizer _stringAnalizer = new();
	
	public static bool IsDigit(this char x)
		=> Analize(x).HasFlag(StringFlags.Digits);

	public static bool IsLatin(this char x)
		=> Analize(x).HasFlag(StringFlags.En);

	public static bool IsRussian(this char x)
		=> Analize(x).HasFlag(StringFlags.Ru);

	public static bool IsLatinOrRussian(this char x)
		=> (Analize(x) & (StringFlags.En | StringFlags.Ru)) != StringFlags.Empty;
	
	public static StringFlags Analize(this char ch)
		=> _stringAnalizer.Analize(ch);
}