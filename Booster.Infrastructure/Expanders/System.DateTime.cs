using System;
using JetBrains.Annotations;

#nullable enable

namespace Booster;

/// <summary>
/// Класс расширений для System.DateTime
/// </summary>
[PublicAPI]
public static class DateTimeExpander
{
	public static bool IsEmpty(this DateTime dt) 
		=> dt == DateTime.MinValue;
		
	public static bool IsSome(this DateTime dt) 
		=> dt > DateTime.MinValue;

	public static DateTime Or(this DateTime dt, DateTime second)
		=> dt.IsSome() ? dt : second;
	public static DateTime Or(this DateTime dt, Func<DateTime> second)
		=> dt.IsSome() ? dt : second();
		
	public static bool IsSignificant(this DateTime dt) 
		=> dt > DateTime.MinValue && dt < DateTime.MaxValue;
		
	public static bool IsMarginal(this DateTime dt) 
		=> dt == DateTime.MinValue || dt == DateTime.MaxValue;

	public static DateTime Min(this DateTime first, DateTime second) 
		=> first > second ? second : first;
		
	public static DateTime Max(this DateTime first, DateTime second) 
		=> first < second ? second : first;

	static readonly long _jsDatetimeMinTimeTicks = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).Ticks;

	public static long ToJsMilliseconds(this DateTime dt) 
		=> (dt.ToUniversalTime().Ticks - _jsDatetimeMinTimeTicks)/10000;

	// ReSharper disable InconsistentNaming
	public static string? ddMMyyyy(this DateTime dt, string separator = ".")
		=> dt.IsSome().Then(() => dt.ToString($"dd{separator}MM{separator}yyyy"));

	public static string? ddMMyyyyHHmm(this DateTime dt, string dateSeparator = ".", string timeSeparator = ":")
		=> dt.IsSome().Then(() => dt.ToString($"dd{dateSeparator}MM{dateSeparator}yyyy HH{timeSeparator}mm"));

	public static string? ddMMyyyyHHmmss(this DateTime dt, string dateSeparator = ".", string timeSeparator = ":")
		=> dt.IsSome().Then(() => dt.ToString($"dd{dateSeparator}MM{dateSeparator}yyyy HH{timeSeparator}mm{timeSeparator}ss"));

	public static string? ddMM(this DateTime dt, string separator = ".")
		=> dt.IsSome().Then(() => dt.ToString($"dd{separator}MM"));

	public static string? HHmmss(this DateTime dt, string separator = ":")
		=> dt.IsSome().Then(() => dt.ToString($"HH{separator}mm{separator}ss"));

	public static string? yyyyMMdd(this DateTime dt, string separator = ".")
		=> dt.IsSome().Then(() => dt.ToString($"yyyy{separator}MM{separator}dd"));

	// ReSharper restore InconsistentNaming


}