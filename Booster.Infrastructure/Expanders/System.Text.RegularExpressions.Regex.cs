using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Booster;

/// <summary>
/// Класс расширений для System.Text.RegularExpressions.Regex
/// </summary>
public static class RegexExpander
{
	public static IEnumerable<string> GetGroupNamesOnly(this Regex re)
	{
		var groupNames = re.GetGroupNames();
		var groupNumbers = re.GetGroupNumbers();

		for (var i=0; i < groupNumbers.Length; i++)
		{
			var name = groupNames[i];
			if ( name != groupNumbers[i].ToString())
				yield return name;
		}

	}

	public static int GetInt(this GroupCollection collection, string group, int def = default)
	{
		var g = collection[group];
		if (g.Success && int.TryParse(g.Value, out var i))
			return i;

		return def;
	}
	public static string GetString(this GroupCollection collection, string group, string def = default)
	{
		var g = collection[group];
		return g.Success ? g.Value : def;
	}

	static readonly NumberFormatInfo _nfi = new() { NumberDecimalSeparator = "." };
	public static decimal GetDecimal(this GroupCollection collection, string group, decimal def = default)
	{
		var g = collection[group];
		if (!g.Success)
		{
			if (decimal.TryParse(g.Value, NumberStyles.Any, _nfi, out var d))
				return d;
			if (decimal.TryParse(g.Value, out d))
				return d;
		}
		return def;
	}

	public static int GetInt(this Match match, string group, int def = default)
		=> match.Groups.GetInt(group, def);

	public static decimal GetDecimal(this Match match, string group, decimal def = default)
		=> match.Groups.GetDecimal(group, def);

	public static string GetString(this Match match, string group, string def = default)
		=> match.Groups.GetString(group, def);
}