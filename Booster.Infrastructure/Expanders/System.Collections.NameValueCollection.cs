using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace Booster;

/// <summary>
/// Класс расширений для System.Collections.Specialized.NameValueCollection
/// </summary>
public static class NameValueCollectionExpander
{
	public static Dictionary<string, object> ToDictionary(this NameValueCollection collection, bool ignoreCase = true)
	{
		var res = ignoreCase ? new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase) : new Dictionary<string, object>();

		foreach (string key in collection)
			res[key] = collection[key];

		return res;
	}
}