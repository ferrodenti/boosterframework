using System;

#nullable enable

namespace Booster;

public static class WeakReferenceExpander
{
	public static bool IsAlive<T>(this WeakReference<T>? reference) where T : class?
		=> reference?.TryGetTarget(out _) == true;

	public static T? Get<T>(this WeakReference<T>? reference) where T : class?
	{
		T? result = null;
		reference?.TryGetTarget(out result);
		return result;
	}
}