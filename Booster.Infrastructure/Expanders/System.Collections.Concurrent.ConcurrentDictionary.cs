using System.Collections.Concurrent;

namespace Booster;

/// <summary>
/// Класс расширений для System.Collections.Concurrent.ConcurrentDictionary
/// </summary>
public static class ConcurrentDictionaryTExpander
{
	public static bool Remove<TKey, TValue>(this ConcurrentDictionary<TKey, TValue> dict, TKey key)
		=> dict.TryRemove(key, out var val);
}