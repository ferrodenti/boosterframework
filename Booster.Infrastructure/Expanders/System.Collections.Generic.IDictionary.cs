using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Booster.Collections;
using Booster.Synchronization;
using JetBrains.Annotations;

#nullable enable

namespace Booster;

// ReSharper disable once InconsistentNaming
/// <summary>
/// Класс расширений для System.Collections.Generic.IDictionary&lt;T&gt;
/// </summary>
[PublicAPI]
public static class IDictionaryTExpander
{
	public static bool TryGetValue(
		this IDictionary? dictionary,
		object? key,
		out object? result)
	{
		if (dictionary != null && key != null && dictionary.Contains(key))
		{
			result = dictionary[key];
			return true;
		}

		result = default;
		return false;
	}


	public static bool TryGetAltValue<TKey, TValue>(
		this IDictionary? dictionary,
		TKey? key,
		out TValue? result)
	{
		if (dictionary != null &&
			key != null &&
			dictionary.TryGetValue(key, out var obj) &&
			(obj.Is(out result) || result == null))
			return true;

		result = default;
		return false;
	}

	public static bool TryGetAltValue<TKey, TValue, TValue2>(
		this IDictionary<TKey, TValue?>? dictionary,
		TKey? key,
		out TValue2? result)
	{
		if (dictionary != null &&
			key != null &&
			dictionary.TryGetValue(key, out var obj) &&
			(obj.Is(out result) || result == null))
			return true;

		result = default;
		return false;
	}

	public static IEnumerable<(object? key, object? value)> EnumerateSafe(this IDictionary dictionary)
	{
		var helper = KeyValueHelper.Instance;
		
		foreach (var kv in dictionary)
			if (helper.TryRead(kv, out var key, out var value))
				yield return (key, value);
			else
			{
				foreach (var k in dictionary.Keys)
				{
					key = k;
					value = dictionary[key];
					yield return (key, value);
				}

				yield break;
			}
	}
		
	public static TValue GetOrAdd<TKey, TValue>(
		this IDictionary<TKey, TValue> dict,
		TKey key, 
		[InstantHandle] Func<TKey, TValue> creator)
	{
		if (!dict.TryGetValue(key, out var res))
			dict[key] = res = creator(key);

		return res;
	}

	public static TValue GetOrAdd<TKey, TValue>(
		this IDictionary<TKey, TValue> dict, 
		AsyncLock? lck, 
		TKey key, 
		[InstantHandle] Func<TKey, TValue> creator)
	{
		lck ??= AsyncLock.GetFor(dict);

		if (!dict.TryGetValue(key, out var res))
			using(lck.Lock())
				if (!dict.TryGetValue(key, out res))
					dict[key] = res = creator(key);

		return res;
	}

	public static async Task<TValue> GetOrAddAsync<TKey, TValue>(
		this IDictionary<TKey, TValue> dict, 
		TKey key,
		[InstantHandle] Func<TKey, Task<TValue>> creator)
	{
		if (!dict.TryGetValue(key, out var res))
			dict[key] = res = await creator(key).NoCtx();

		return res;
	}

	public static async Task<TValue> GetOrAddAsync<TKey, TValue>(
		this IDictionary<TKey, TValue> dict,
		AsyncLock? lck, 
		TKey key, 
		[InstantHandle] Func<TKey, Task<TValue>> creator)
	{
		lck ??= AsyncLock.GetFor(dict);

		if(!dict.TryGetValue(key, out var result))
			using(await lck.LockAsync().NoCtx())
				if (!dict.TryGetValue(key, out result))
					dict[key] = result = await creator(key).NoCtx();

		return result;
	}

	public static TValue? SafeGet<TKey, TValue>(
		this IDictionary<TKey, TValue> dict,
		TKey key)
	{
		if (dict.TryGetValue(key, out var res) != true)
			return default!;

		return res;
	}
	public static TValue SafeGet<TKey, TValue>(
		this IDictionary<TKey, TValue> dict, 
		TKey key,
		TValue @default)
	{
		if (dict.TryGetValue(key, out var res) != true)
			return @default;

		return res ?? @default;
	}

	public static bool TryRemove<TKey, TValue>(
		this IDictionary<TKey, TValue>  dict,
		TKey key, 
		out TValue value)
		=> dict.TryGetValue(key, out value) && dict.Remove(key);


#if !NETSTANDARD
	public static bool TryAdd<TKey, TValue>(
		this IDictionary<TKey, TValue> dict, 
		TKey key, 
		TValue value)
	{
		var lck = AsyncLock.GetFor(dict);
		using (lck.Lock())
		{
			if (dict.ContainsKey(key))
				return false;

			dict.Add(key, value);
			return true;
		}
	}

#endif
	public static int AddRange<TKey, TValue>(
		this IDictionary<TKey, TValue> dict, 
		IEnumerable<KeyValuePair<TKey, TValue>>? pairs)
	{
		var result = 0;

		if (pairs != null)
			foreach (var pair in pairs)
				if (dict.TryAdd(pair.Key, pair.Value))
					++result;

		return result;
	}

	public static int RemoveRange<TKey, TValue>(
		this IDictionary<TKey, TValue> dict,
		IEnumerable<TKey>? keys)
	{
		var result = 0;

		if(keys != null)
			foreach (var key in keys)
				if(dict.Remove(key))
					++result;

		return result;
	}
}