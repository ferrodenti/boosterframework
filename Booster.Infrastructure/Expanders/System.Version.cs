using System;
using System.Threading.Tasks;
using JetBrains.Annotations;

#nullable enable

namespace Booster;

/// <summary>
/// Класс расширений для System.Version
/// </summary>
[PublicAPI]
public static class VersionExpander
{
	public static Version Duplicate(this Version? version)
		=> version.With(v => new Version(v.Major, v.Minor, v.Build, v.Revision)) ?? new Version(1, 0, 0, 0);

	public static int[] Parts(this Version version)
		=> new[] {version.Major, version.Minor, version.Build, version.Revision};

	public static Version FromParts(int[] parts)
		=> new Version(parts[0], parts[1], parts[2], parts[3]);

	public static Version MakePositive(this Version version)
	{
		var parts = version.Parts();

		for (var i = 0; i < 4; i++)
			if (parts[i] < 0)
				parts[i] = 0;

		return FromParts(parts);
	}

	public static Version Increment(this Version version, int i)
	{
		var parts = version.Parts();

		++parts[i];
		for (++i; i < 4; ++i)
			parts[i] = 0;

		return FromParts(parts);
	}

	public static Version Decrement(this Version version, int i)
	{
		var parts = version.Parts();

		if (parts[i] > 0)
			--parts[i];

		return FromParts(parts);
	}
}