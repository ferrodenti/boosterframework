using System;
using System.Threading.Tasks;
using JetBrains.Annotations;

#nullable enable

namespace Booster;

/// <summary>
/// Класс расширений для System.Boolean
/// </summary>
[PublicAPI]
public static class BoolExpander
{
	public static string ToJsString(this bool x)
		=> x ? "true" : "false";

	public static T? Then<T>(this bool condition, T? value)
		=> condition ? value : default;

	public static T? Then<T>(this bool condition, Func<T> getValue)
		=> condition ? getValue() : default;

	public static async Task<T?> Then<T>(this bool condition, Func<Task<T>> getValue)
		=> condition ? await getValue().NoCtx() : default;

	public static T? Otherwise<T>(this bool condition, T? value)
		=> !condition ? value : default;

	public static T? Otherwise<T>(this bool condition, Func<T> getValue)
		=> !condition ? getValue() : default;

	public static async Task<T?> Otherwise<T>(this bool condition, Func<Task<T>> getValue)
		=> !condition ? await getValue().NoCtx() : default;
}