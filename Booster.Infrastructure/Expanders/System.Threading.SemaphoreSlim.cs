using System;
using System.Threading;
using System.Threading.Tasks;

namespace Booster;

public static class SemaphoreSlimExpander
{
	public static void ReleaseSafe(this SemaphoreSlim  semaphore, int releaseCount = 1, int maxCount = 1)
		=> Try.IgnoreErrors<SemaphoreFullException>(() =>
		{
			if (semaphore.CurrentCount < maxCount)
				semaphore.Release(Math.Max(releaseCount, maxCount - semaphore.CurrentCount));
		});


	public static async Task<bool> TryEnterAsync(this SemaphoreSlim semaphore, Func<Task> onEnterSuccess)
	{
		if (!await semaphore.WaitAsync(1).NoCtx())
			return false;

		try
		{
			await onEnterSuccess().NoCtx();
			return true;
		}
		finally
		{
			semaphore.Release();
		}
	}

	public static Task<bool> TryEnterAsync(this SemaphoreSlim semaphore, Action onEnterSuccess)
		=> semaphore.TryEnterAsync(() =>
		{
			onEnterSuccess();
			return Task.CompletedTask;
		});
}