using System;
using System.Text;
using JetBrains.Annotations;

namespace Booster;

/// <summary>
/// Класс расширений для System.Text.StringBuilder
/// </summary>
public static class StringBuilderExpander
{
	[StringFormatMethod("format")]
	[Obsolete("Use string interpolation")]
	public static void AppendLineFormat(this StringBuilder sb, string format, params object[] args)
		=> sb.AppendLine(string.Format(format, args));
}