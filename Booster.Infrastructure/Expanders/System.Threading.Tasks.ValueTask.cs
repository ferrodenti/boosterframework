#if NETSTANDARD2_1
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Booster
{
	public static class ValueTaskExpander
	{
		public static ConfiguredValueTaskAwaitable NoCtx(this ValueTask task) 
			=> task.ConfigureAwait(false);

		public static ConfiguredValueTaskAwaitable<TResult> NoCtx<TResult>(this ValueTask<TResult> task) 
			=> task.ConfigureAwait(false);	

		public static TResult NoCtxResult<TResult>(this ValueTask<TResult> task)
			=> task.ConfigureAwait(false).GetAwaiter().GetResult();
		
		public static void NoCtxWait(this ValueTask task)
			=> task.ConfigureAwait(false).GetAwaiter().GetResult();
	}
}
#endif