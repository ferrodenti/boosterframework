using System;

namespace Booster;

/// <summary>
/// Класс расширений для System.Int32
/// </summary>
public static class IntExpander
{
	[Obsolete("Use numeric helper")]
	public static bool IsPowerOfTwo(this int x)
		=> x != 0 && (x & (x - 1)) == 0;
}