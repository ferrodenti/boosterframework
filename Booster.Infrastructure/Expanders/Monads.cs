using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using JetBrains.Annotations;

#nullable enable

namespace Booster;

[PublicAPI]
public static class Monads
{
	[ContractAnnotation("o:null => null")]
	public static TResult? With<TInput, TResult>(
		[NotNullIfNotNull("o")] this TInput? o, 
		Func<TInput, TResult?> evaluator)
		where TInput : class
		=> o == null ? default : evaluator(o);

	public static TResult With<TInput, TResult>(
		this TInput? o, 
		Func<TInput, TResult?> evaluator,
		TResult defaultValue)
		where TInput : class
		=> o == null ? defaultValue : evaluator(o) ?? defaultValue;

	public static TResult? With<TInput, TResult>(
		this TInput? o, 
		Func<TResult?> evaluator)
		where TInput : class
		=> o == null ? default : evaluator();

	public static TResult With<TInput, TResult>(
		this TInput? o, 
		Func<TResult?> evaluator, 
		TResult defaultValue)
		where TInput : class
		=> o == null ? defaultValue : evaluator() ?? defaultValue;


	#region WithAsync

	public static Task<TResult?> WithAsync<TInput, TResult>(
		this TInput? o, 
		Func<TInput, Task<TResult?>> evaluator)
		where TInput : class
		=> o == null ? Task.FromResult(default(TResult?)) : evaluator(o);

	public static async Task<TResult> WithAsync<TInput, TResult>(
		this TInput? o,
		Func<TInput, Task<TResult?>> evaluator, 
		TResult defaultValue)
		where TInput : class
		=> o == null ? defaultValue : await evaluator(o).NoCtx() ?? defaultValue;

	public static async Task<TResult?> WithAsync<TInput, TResult>(
		this Task<TInput?> o,
		Func<TInput, Task<TResult?>> evaluator)
		where TInput : class
	{
		var obj = await o.NoCtx();
		if (obj != null) 
			return await evaluator(obj).NoCtx();

		return default;
	}

	public static async Task<TResult> WithAsync<TInput, TResult>(
		this Task<TInput?> o,
		Func<TInput, Task<TResult?>> evaluator,
		TResult defaultValue)
		where TInput : class
	{
		var obj = await o.NoCtx();
		if (obj != null)
			return await evaluator(obj).NoCtx() ?? defaultValue;

		return defaultValue;
	}

	public static async Task<TResult?> WithAsync<TInput, TResult>(
		this Task<TInput?> o,
		Func<TInput, TResult?> evaluator)
		where TInput : class
	{
		var obj = await o.NoCtx();
		if (obj != null)
			return evaluator(obj);

		return default;
	}

	public static async Task<TResult> WithAsync<TInput, TResult>(
		this Task<TInput?> o,
		Func<TInput, TResult?> evaluator,
		TResult defaultValue)
		where TInput : class
	{
		var obj = await o.NoCtx();
		if (obj != null)
			return evaluator(obj) ?? defaultValue;

		return defaultValue;
	}

	#endregion

	public static async Task<TResult> OrAsync<TResult>(
		this Task<TResult?>? task, 
		TResult defaultValue)
	{
		if (task != null)
		{
			var result = await task.NoCtx();
			return result ?? defaultValue;
		}

		return defaultValue;
	}
	
	public static async Task<TResult> OrAsync<TResult>(
		this Task<TResult?>? task, 
		[InstantHandle]Func<TResult> defaultValue)
	{
		if (task != null)
		{
			var result = await task.NoCtx();
			return result ?? defaultValue();
		}

		return defaultValue();
	}
	
	public static Task WithAsync<TInput>(
		this TInput? o, 
		Func<TInput, Task> evaluator)
		where TInput : class
		=> o == null ? Task.CompletedTask : evaluator(o);

	public static TInput? If<TInput>(
		this TInput? o, 
		Func<TInput, bool> evaluator) 
		where TInput : class
		=> o != null && evaluator(o) ? o : null;

	public static TInput? Do<TInput>(
		this TInput? o, 
		Action<TInput> action) 
		where TInput : class
	{
		if (o != null)
		{
			action(o);
			return o;
		}

		return null;
	}

	public static bool Is<TInput, TResult>(
		this TInput? input, 
		out TResult result)
	{
		if (input is TResult output)
		{
			result = output;
			return true;
		}

		result = default!;
		return false;
	}

	[Obsolete] // Зачем? Где?
	public delegate bool OutActionDelegate<in TInput, TResult>(TInput? input, out TResult result);

	[Obsolete] // Зачем? Где?
	public static TResult OutAction<TInput, TResult>(this TInput? o, OutActionDelegate<TInput, TResult> action)
	{
		action(o, out var res); 
		return res;
	}
}