using System;

namespace Booster;

/// <summary>
/// Класс расширений для System.DateTime
/// </summary>
public static class TimeSpanExpander
{
	/// <summary>
	/// Сравнивает интервал с TimeSpan.MinValue
	/// </summary>
	public static bool IsEmpty(this TimeSpan ts) 
		=> ts == TimeSpan.MinValue;
		
	/// <summary>
	/// Сравнивает интервал с TimeSpan.MinValue
	/// </summary>
	public static bool IsSome(this TimeSpan ts) 
		=> ts > TimeSpan.MinValue;

	/// <summary>
	/// Сравнивает интервал с TimeSpan.Zero
	/// </summary>
	public static bool IsZero(this TimeSpan ts) 
		=> ts == TimeSpan.Zero;
		
	/// <summary>
	/// Сравнивает интервал с TimeSpan.Zero
	/// </summary>
	public static bool IsNonZero(this TimeSpan ts) 
		=> ts != TimeSpan.Zero;
		
	public static bool IsSignificant(this TimeSpan ts) 
		=> ts > TimeSpan.MinValue && ts < TimeSpan.MaxValue;
		
	public static bool IsBetweenZeroAndMax(this TimeSpan ts) 
		=> ts > TimeSpan.Zero && ts < TimeSpan.MaxValue;
		
	public static bool IsMarginal(this TimeSpan ts) 
		=> ts == TimeSpan.MinValue || ts == TimeSpan.MaxValue;

	public static TimeSpan Min(this TimeSpan first, TimeSpan second) 
		=> first > second ? second : first;
		
	public static TimeSpan Max(this TimeSpan first, TimeSpan second) 
		=> first < second ? second : first;
}