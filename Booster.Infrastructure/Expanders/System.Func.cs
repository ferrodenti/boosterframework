﻿using System;
using System.Threading.Tasks;
using JetBrains.Annotations;

#nullable enable

namespace Booster;

public static class FuncExpander
{
	public static TResult? InvokeOrDefault<TSource, TResult>(this Func<TSource, TResult>? func, TSource source)
		=> func != null ? func(source) : default;

	public static async Task<TResult?> InvokeOrDefaultAsync<TSource, TResult>(this Func<TSource, Task<TResult>>? func, TSource source)
		=> func != null ? await func(source).NoCtx() : default;
}