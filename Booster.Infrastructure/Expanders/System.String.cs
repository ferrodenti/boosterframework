#nullable enable

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using Booster.Helpers;
using Booster.Localization.Pluralization;
using Booster.Parsing;
using JetBrains.Annotations;

namespace Booster;

/// <summary>
/// Класс расширений для System.String
/// </summary>
[PublicAPI]
public static class StringExpander
{
	[ContractAnnotation("null => true")]
	public static bool IsEmpty([NotNullWhen(false)] this string? str)
		=> string.IsNullOrWhiteSpace(str);

	[ContractAnnotation("null => false")]
	public static bool IsSome([NotNullWhen(true)] this string? str)
		=> !string.IsNullOrWhiteSpace(str);

	#region MyRegion

	static readonly StringAnalizer _stringAnalizer = new();

	public static StringFlags Analize(this string? str)
		=> _stringAnalizer.Analize(str);

	public static  int IndexOf(this string? str, StringFlags flags, bool any = false)
		=> _stringAnalizer.IndexOf(str, flags, any);

	public static  int IndexOf(this string? str, int index, StringFlags flags, bool any = false)
		=> _stringAnalizer.IndexOf(str, index, flags, any);

	public static  int IndexOf(this string? str, int index, int count, StringFlags flags, bool any = false)
		=> _stringAnalizer.IndexOf(str, index, count, flags, any);

	public static  int LastIndexOf(this string? str, StringFlags flags, bool any = false)
		=> _stringAnalizer.LastIndexOf(str, flags, any);

	public static  int LastIndexOf(this string? str, int index, StringFlags flags, bool any = false)
		=> _stringAnalizer.LastIndexOf(str, index, flags, any);

	public static  int LastIndexOf(this string? str, int index, int count, StringFlags flags, bool any = false)
		=> _stringAnalizer.LastIndexOf(str, index, count, flags, any);

	#endregion

	public static string? Or(this string? str, object? second)
		=> str.IsSome() ? str : second?.ToString();

	public static string Or(this string? str, string second)
		=> str.IsSome() ? str : second;

	public static string? Or(this string? str, [InstantHandle] Func<object?> second)
		=> str.IsSome() ? str : second()?.ToString();

	public static string Or(this string? str, [InstantHandle] Func<string> second)
		=> str.IsSome() ? str : second();

	public static string? Or(this string? str, params object?[] other)
	{
		if (str.IsSome())
			return str;

		foreach (var obj in other)
		{
			str = obj?.ToString();
			if (str.IsSome())
				return str;
		}

		return null;
	}

	public static string? Or(this string? str, params Func<object?>[] other)
	{
		if (str.IsSome())
			return str;

		foreach (var func in other)
		{
			str = func()?.ToString();
			if (str.IsSome())
				return str;
		}

		return null;
	}

	public static bool Skip(this string str, string token, out string result, bool ignoreCase = true)
	{
		if (str.StartsWith(token, ignoreCase))
		{
			result = str.SubstringSafe(token.Length);
			return true;
		}

		result = default!;
		return false;
	}


	public static string SubstringSafe(this string? str, int start, int count)
	{
		if (str.IsEmpty())
			return "";

		if (start < 0)
		{
			count += start;
			start = 0;
		}

		var len = str.Length;

		if (len <= start)
			return "";

		return len <= start + count
			? str.Substring(start)
			: str.Substring(start, count);
	}

	public static string SubstringSafe(this string? str, int start)
	{
		if (str.IsEmpty())
			return "";

		return str.Length <= start ? "" : str.Substring(start);
	}

	public static TResult? WithSome<TResult>(this string? str, Func<string, TResult> evaluator)
		=> str.IsSome() ? evaluator(str) : default;

	public static TResult WithSome<TResult>(this string? str, Func<string, TResult> evaluator, TResult defaultValue)
		=> str.IsSome() ? evaluator(str) : defaultValue;

	public static void WithSome(this string? str, [InstantHandle] Action<string> evaluator)
	{
		if (str.IsSome())
			evaluator(str);
	}

	public static string Limit(this string? str, int length)
	{
		if (str.IsEmpty())
			return "";

		return str.Length < length ? str : str.Substring(0, length);
	}

	public static string Truncate(this string? str, int length, string? suffix = "...")
	{
		if (str.IsEmpty())
			return "";

		return str.Length <= length
			? str
			: $"{str.Substring(0, length - (suffix?.Length ?? 0))}{suffix}";
	}

	public static string[] SplitNonEmpty(this string? str, params char[] separators)
		=> str?.Split(separators, StringSplitOptions.RemoveEmptyEntries) ?? Array.Empty<string>();

	public static string[] SplitNonEmpty(this string? str, params string[] separators)
		=> str?.Split(separators, StringSplitOptions.RemoveEmptyEntries) ?? Array.Empty<string>();

	public static string JoinNonEmpty(this string? separator, params object?[] values)
		=> JoinNonEmpty(separator, values.Select(o => o.With(v => v as string ?? v?.ToString())));

	public static string JoinNonEmpty(this string? separator, IEnumerable<string?> values)
	{
		var res = new EnumBuilder(separator);

		foreach (var str in values)
			if (str.IsSome())
				res.Append(str);

		return res;
	}

	[ContractAnnotation("str:notnull => notnull")]
	[ContractAnnotation("str:null => null")]
	public static string? ToUpperFirstLetter(this string? str)
	{
		if (string.IsNullOrWhiteSpace(str))
			return str;

		return char.ToUpper(str[0]) + str.SubstringSafe(1);
	}

	public static string? ToLowerFirstLetter(this string? str, bool preserveAbbs = true)
	{
		if (string.IsNullOrWhiteSpace(str))
			return str;

		if (preserveAbbs && str.Length > 1 && char.IsUpper(str[1]))
			return str;

		return char.ToLower(str[0]) + str.SubstringSafe(1);
	}

	public static bool StartsWith(this string? str, string value, bool ignoreCase)
		=> ignoreCase ? str.StartsWithIgnoreCase(value) : !string.IsNullOrEmpty(str) && str.StartsWith(value);

	public static bool StartsWith(this string? str, char value)
		=> !string.IsNullOrEmpty(str) && str[0] == value;

	public static bool StartsWithIgnoreCase(this string? str, string value)
	{
		if (string.IsNullOrEmpty(str) || string.IsNullOrEmpty(value) || value.Length > str.Length)
			return false;

		return string.Compare(str, 0, value, 0, value.Length, StringComparison.OrdinalIgnoreCase) == 0;
	}

	[ContractAnnotation("str:notnull => notnull", true)]
	[ContractAnnotation("str:null => null", true)]
	public static string? TrimStart(this string? str, string value, bool ignoreCase = false)
		=> str.StartsWith(value, ignoreCase) ? str!.Substring(value.Length) : str;

	[ContractAnnotation("str:notnull => notnull", true)]
	[ContractAnnotation("str:null => null", true)]
	public static string? TrimEnd(this string? str, string? value, bool ignoreCase = false)
		=> str.EndsWith(value, ignoreCase) ? str.CutRight(value!.Length) : str;

	public static string CutRight(this string? str, int charectersToCut)
		=> charectersToCut >= str?.Length ? "" : str?.Substring(0, str.Length - charectersToCut) ?? "";


	public static bool ContainsIgnoreCase(this string? str, string? value)
	{
		if (string.IsNullOrEmpty(str) || string.IsNullOrEmpty(value) || value.Length > str.Length)
			return false;

		return str.IndexOf(value, StringComparison.OrdinalIgnoreCase) >= 0;
	}

	[ContractAnnotation("str:null => false", true)]
	[ContractAnnotation("value:null => false", true)]
	public static bool EndsWith(this string? str, string? value, bool ignoreCase)
	{
		if (str == null || value == null)
			return false;

		if (ignoreCase)
			return str.EndsWithIgnoreCase(value);

		return str.EndsWith(value);
	}

	public static bool EndsWith(this string? str, char value)
		=> !string.IsNullOrEmpty(str) && str[str.Length - 1] == value;

	public static bool EndsWithIgnoreCase(this string? str, string? value)
	{
		if (str == null || value == null)
			return false;

		var indexA = str.Length - value.Length;
		if (indexA < 0)
			return false;

		return string.Compare(str, indexA, value, 0, value.Length, StringComparison.OrdinalIgnoreCase) == 0;
	}

	public static bool EqualsIgnoreCase(this string? str, string? second)
		=> string.Equals(str, second, StringComparison.OrdinalIgnoreCase);

	public static bool Equals(this string? str, string? second, bool ignoreCase)
		=> string.Equals(str, second, 
			ignoreCase 
				? StringComparison.OrdinalIgnoreCase 
				: StringComparison.Ordinal);

	public static int CompareConstantTime(this string? str, string? second)
	{
		if (str == null)
			return second == null ? 0 : -1;

		if (second == null)
			return 1;

		var a = Encoding.UTF8.GetBytes(str);
		var b = Encoding.UTF8.GetBytes(second);

		return a.CompareConstantTime(b);
	}

	public static bool EqualsConstantTime(this string? str, string? second)
		=> str.CompareConstantTime(second) == 0;

	public static IEnumerable<string> SplitCamelCase(this string? str, bool preserveAbbrs = true)
	{
		if (string.IsNullOrWhiteSpace(str))
			yield break;

		var res = new StringBuilder();
		var abbr = false;
		var len = str.Length;

		for (var i = 0; i < len; i++)
		{
			var ch = str[i];

			//if (!char.IsLetterOrDigit(ch))
			//{
			//	if (res.Length > 0)
			//	{
			//		yield return res.ToString();
			//		res.Clear();
			//	}
			//	abbr = false;
			//	continue;
			//}
			if (char.IsUpper(ch))
			{
				if (preserveAbbrs)
				{
					var nextUp = i < len - 1 && char.IsUpper(str[i + 1]) && (i == len - 2 || char.IsUpper(str[i + 2]));
					//bool nextUp = i < len - 1 && char.IsUpper(str[i + 1]) && (i == len - 2 || !char.IsLetter(str[i + 2]) || char.IsUpper(str[i + 2]));

					if (!abbr && nextUp)
					{
						if (res.Length > 0)
						{
							yield return res.ToString();
							res.Clear();
						}

						abbr = true;
					}

					if (abbr)
					{
						res.Append(ch);

						if (!nextUp)
						{
							yield return res.ToString();
							res.Clear();
							abbr = false;
						}

						continue;
					}
				}

				if (res.Length > 0)
				{
					yield return res.ToString();
					res.Clear();
				}
			}

			res.Append(ch);
		}

		if (res.Length > 0)
			yield return res.ToString();
	}

	public static string SeparateCamelCase(this string? str, bool upFirstLetter = true, string? separator = " ")
	{
		var res = new EnumBuilder(separator);

		foreach (var w in str.SplitCamelCase())
			if (upFirstLetter)
			{
				upFirstLetter = false;
				res.Append(w.ToUpperFirstLetter());
			}
			else
				res.Append(w);

		return res;
	}

	

	public static int GetDeterministicHashCode(this string? str)
	{
		unchecked
		{
			var hash1 = (5381 << 16) + 5381;
			var hash2 = hash1;

			if (str != null)
				for (var i = 0; i < str.Length; i += 2)
				{
					hash1 = ((hash1 << 5) + hash1) ^ str[i];
					if (i == str.Length - 1)
						break;
					hash2 = ((hash2 << 5) + hash2) ^ str[i + 1];
				}

			return hash1 + hash2 * 1566083941;
		}
	}

	#region Case modification

	public static StringCase GetCase(this string? str)
	{
		if (str.IsEmpty())
			return StringCase.Unknown;

		var up = false;
		var low = false;
		var first = StringCase.Unknown;
		
		foreach (var ch in str)
		{
			if (char.IsUpper(ch))
			{
				up = true;

				if (first == StringCase.Unknown)
					first = StringCase.Camel ;
			}
			else if (char.IsLower(ch))
			{
				low = true;

				if (first == StringCase.Unknown)
					first = StringCase.Snake;
			}

			if (up && low)
				return first;
		}

		if (up && !low)
			return StringCase.Upper;

		if (low && !up)
			return StringCase.Lower;
		
		return first;
	}

	public static string ChangeCase(this string str, StringCase newCase)
	{
		if (str.IsEmpty())
			return str;
		
		var old = GetCase(str);
		if (old == newCase)
			return str;

		switch (newCase)
		{
		case StringCase.Upper: return str.ToUpper();
		case StringCase.Lower: return str.ToLower();
		case StringCase.Camel:
			if (old != StringCase.Snake && old != StringCase.Camel)
				str = str.ToLower();
			
			return str.ToUpperFirstLetter()!;
		
		case StringCase.Snake: 
			if (old != StringCase.Snake && old != StringCase.Camel)
				str = str.ToLower();
			
			return str.ToUpperFirstLetter()!;
		default:
			return str;
		}
	}

	#endregion
	
	#region Pluralize
	/// <summary>
	/// Pluralize a word based on standard English Pluralization Rules (ha ha).
	/// </summary>
	/// <param name="count"></param>
	/// <param name="singular"></param>
	/// <returns></returns>
	[Obsolete("Use EnPluralizer")]
	public static string Pluralize(this string singular, int count = 2)
		=> new EnPluralizer(singular).Pluralize(count);

	[Obsolete("Use EnPluralizer.PluralizeCamelCase")]
	public static string PluralizeCamelCase(this string str, string separator = "", int count = 2)
		=> EnPluralizer.PluralizeCamelCase(str, separator, count);

	#endregion
}