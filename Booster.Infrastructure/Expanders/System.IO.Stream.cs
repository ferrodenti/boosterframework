using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Booster;

public static class StreamExpander
{
	public static void SetEof(this Stream stream)
		=> stream.SetLength(stream.Position);
	
	public static async Task<byte[]> ReadPascal(this Stream stream, int maxSize = 1024*1024*100 /*100Mb*/, CancellationToken token = default)
	{
		var size = await ReadInt(stream, token).NoCtx();
		if (maxSize > 0 && size > maxSize)
			throw new IOException("Message is too long");

		return await ReadWhole(stream, size, token).NoCtx();
	}

	public static async Task WritePascal(this Stream stream, byte[] data, CancellationToken token = default)
	{
		var buff = BitConverter.GetBytes(data.Length);
		await stream.WriteAsync(buff, 0, buff.Length, token).NoCtx();
		await stream.WriteAsync(data, 0, data.Length, token).NoCtx();
	}

	static async Task<int> ReadInt(Stream stream, CancellationToken token)
	{
		var bytes = await ReadWhole(stream, sizeof(int), token).NoCtx();
		return BitConverter.ToInt32(bytes, 0);
	}

	public static async Task<byte[]> ReadWhole(this Stream stream, int size, CancellationToken token = default)
	{
		var buff = new byte[size];
		var cnt = 0;
		while (cnt < size)
		{
			var read = await stream.ReadAsync(buff, cnt, size - cnt, token).NoCtx();
			if (read == 0)
				throw new IOException("A stream was closed");

			cnt += read;
		}

		return buff;
	}
}