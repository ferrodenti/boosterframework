using System.Collections.Generic;

#nullable enable

namespace Booster;

/// <summary>
/// Класс расширений для System.Collections.Generic.ICollection&lt;T&gt;
/// </summary>
public static class CollectionTExpander
{
	public static void AddRange<T>(this ICollection<T> collection, IEnumerable<T>? items )
	{
		if(items != null)
			foreach (var item in items)
				collection.Add(item);
	}
		
	public static void RemoveRange<T>(this ICollection<T> collection, IEnumerable<T>? items )
	{
		if(items != null)
			foreach (var item in items)
				collection.Remove(item);
	}

}