using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Booster.Reflection;
using JetBrains.Annotations;

namespace Booster;

#nullable enable

/// <summary>
/// Класс расширений для System.Object
/// </summary>
[PublicAPI]
public static class ObjectExpander
{
	public static T Claim<T>(this T? obj, bool checkThrow = false)
	{
		if (checkThrow && obj == null)
			throw new ArgumentNullException(nameof(obj));
		
		return obj!;
	}

	public static async Task<T> Claim<T>(this Task<T?> objTask, bool checkThrow = false)
	{
		var obj = await objTask.NoCtx();

		if (checkThrow && obj == null)
			throw new ArgumentNullException(nameof(objTask));

		return obj!;
	}

	[ContractAnnotation("obj:null => null; obj:notnull=>notnull")]
	[return: NotNullIfNotNull("obj")]
	public static TypeEx? GetTypeEx(this object? obj)
		=> obj.With(TypeEx.Of);
}