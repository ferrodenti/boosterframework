
using System;
using System.Threading;

namespace Booster;

public static class ReaderWriterLockSlimExpander
{
	public static IDisposable ReadLock(this ReaderWriterLockSlim locker)
	{
		locker.EnterReadLock();
		return new ActionDisposable(locker.ExitReadLock);
	}
		
	public static IDisposable WriteLock(this ReaderWriterLockSlim locker)
	{
		locker.EnterWriteLock();
		return new ActionDisposable(locker.ExitWriteLock);
	}
		
	public static IDisposable UpgradeableReadLock(this ReaderWriterLockSlim locker)
	{
		locker.EnterUpgradeableReadLock();
		return new ActionDisposable(locker.ExitUpgradeableReadLock);
	}
}