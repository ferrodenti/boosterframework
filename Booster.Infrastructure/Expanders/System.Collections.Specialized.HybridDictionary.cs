using System;
using System.Collections.Specialized;

namespace Booster;

// ReSharper disable once InconsistentNaming
/// <summary>
/// Класс расширений для System.Collections.Specialized.HybridDictionary
/// </summary>
public static class HybridDictionaryExpander
{
	public static bool TryGetValue<TValue>(this HybridDictionary dict, object key, out TValue value)
	{
		var o = dict[key];
		if (o != null)
		{
			value = (TValue) o;
			return true;
		}
		value = default;
		return dict.Contains(key);
	}

	static TValue GetOrAddNoLock<TValue>(HybridDictionary dict, object key, Func<object, TValue> factory)
	{
		if (!dict.TryGetValue(key, out TValue res))
				dict[key] = res = factory(key);

		return res;
	}

	public static TValue GetOrAdd<TValue>(this HybridDictionary dict, object key, Func<object, TValue> factory)
	{
		var syncRoot = dict.SyncRoot;

		if (!Utils.IsNull(syncRoot))
		{
			if (dict.TryGetValue(key, out TValue res))
				return res;

			
			lock (syncRoot)
				return GetOrAddNoLock(dict, key, factory);

		}
		return GetOrAddNoLock(dict, key, factory);
	}
}