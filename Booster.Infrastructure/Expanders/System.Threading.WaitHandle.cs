using System;
using System.Threading;
using System.Threading.Tasks;

namespace Booster;

public static class WaitHandleExpander
{
	public static Task AsTask(this WaitHandle handle)
		=> AsTask(handle, Timeout.InfiniteTimeSpan);

	public static Task AsTask(this WaitHandle handle, TimeSpan timeout)
	{
		var tcs = new TaskCompletionSource<object>();
		var registration = ThreadPool.RegisterWaitForSingleObject(handle, (state, timedOut) =>
		{
			var localTcs = (TaskCompletionSource<object>)state;
			if (timedOut)
				localTcs.TrySetCanceled();
			else
				localTcs.TrySetResult(null);
		}, tcs, timeout, true);
		tcs.Task.ContinueWith((_, state) => ((RegisteredWaitHandle)state).Unregister(null), registration, TaskScheduler.Default);
		return tcs.Task;
	}
}