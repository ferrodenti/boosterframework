using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Booster;

// ReSharper disable once InconsistentNaming
/// <summary>
/// Класс расширений для System.Collections.Generic.IList&lt;T&gt;
/// </summary>
public static class IListTExpander
{
	public static bool Remove<T>(this IList<T> list, T item, IEqualityComparer comparer)
	{
		if (comparer == null || ReferenceEquals(comparer, EqualityComparer<T>.Default))
			return list.Remove(item);

		var i = 0;
		foreach (var v in list)
		{
			if (comparer.Equals(v, item))
			{
				list.RemoveAt(i);
				return true;
			}
			i++;
		}
		return false;
	}

	public static int IndexOf<T>(this IList<T> list, T item, IEqualityComparer comparer = null)
	{
		if (comparer == null || ReferenceEquals(comparer, EqualityComparer<T>.Default))
			return list.IndexOf(item);

		var i = 0;
		foreach (var v in list)
		{
			if (comparer.Equals(v, item))
				return i;
			i++;
		}
		return -1;
	}

	public static bool Contains<T>(this IList<T> list, T item, IEqualityComparer comparer = null)
	{
		if (comparer == null || ReferenceEquals(comparer, EqualityComparer<T>.Default))
			return list.Contains(item);

		return list.Any(v => comparer.Equals(v, item));
	}
}