using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Booster;

/// <summary>
/// Класс расширений для System.Runtime.Serialization.Formatters.BinaryFormatter
/// </summary>
public static class BinaryFormatterExpander
{
	public static byte[] SerializeToByteArray(this BinaryFormatter formatter, object obj)
	{
		using var stream = new MemoryStream();
		formatter.Serialize(stream, obj);
		return stream.ToArray();
	}

	public static object DeserializeFromByteArray(this BinaryFormatter formatter, byte[] data)
	{
		using var stream = new MemoryStream(data);
		return formatter.Deserialize(stream);
	}
}