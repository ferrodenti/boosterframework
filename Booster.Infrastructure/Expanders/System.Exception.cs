﻿using System;
using System.Collections.Generic;
using System.Linq;

#nullable enable

namespace Booster;

/// <summary>
/// Класс расширений для System.Exception
/// </summary>
public static class ExceptionExpander
{
	[Obsolete("Use Expand")]
	public static List<Exception> GetInnerExceptions(this Exception? exception)
	{
		var exceptions = new List<Exception>();

		while (exception != null)
		{
			exceptions.Add(exception);
			exception = exception.InnerException;
		}

		return exceptions;
	}

	public static IEnumerable<Exception> Expand(this Exception? exception, bool innerToOuter = true)
	{
		if (exception == null)
			return Array.Empty<Exception>();

		IEnumerable<Exception> ExpandAggregate()
		{
			if (exception is AggregateException agg)
				foreach (var ex in agg.InnerExceptions)
				foreach (var ex2 in Expand(ex, innerToOuter))
					yield return ex2;
		}
		IEnumerable<Exception> ExpandInner()
		{
			if(exception.InnerException != null)
				foreach (var ex in exception.InnerException.Expand(innerToOuter))
					yield return ex;
		}

		return innerToOuter
			? ExpandAggregate().Concat(ExpandInner()).AppendAfter(exception)
			: ExpandInner().Concat(ExpandAggregate()).AppendBefore(exception);
	}

	public static Exception First(this Exception? exception) => exception.Expand().First();
}