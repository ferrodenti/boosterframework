using System;
using System.Linq;
using System.Reflection;
using Booster.Helpers;
using Booster.Reflection;
using JetBrains.Annotations;

namespace Booster.Hacks.System;

[PublicAPI]
public static class SR
{
#if NETSTANDARD
	static readonly Assembly _assy = AssemblyHelper.GetAssembly("System.Private.CoreLib");
#else
	static readonly Assembly _assy = AssemblyHelper.GetAssembly("System");
#endif
	public static readonly TypeEx SrType = _assy?.GetType("System.SR")!;

	static readonly Method[] _methods = new[]
	{
		TypeEx.Get(typeof(Environment)).FindMethod("GetResourceString", typeof(string)),
		SrType?.FindMethod("GetResourceString", typeof(string)),
		SrType?.FindMethod("GetString", typeof(string))
	}.NonDefault().ToArray();

	public static string GetString(string name)
		=> Try.OrDefault(() =>
		{
			foreach (var method in _methods)
				if (method.Invoke(null, name) is string value)
					return value;

			return name;
		}, name);

	// ReSharper disable InconsistentNaming
	public static string Argument_EmptyPath => GetString(nameof(Argument_EmptyPath));
	public static string ArgumentNull_Path => GetString(nameof(ArgumentNull_Path));

	public static string IO_EOF_ReadBeyondEOF => GetString(nameof(IO_EOF_ReadBeyondEOF));
	public static string IO_FileTooLong2GB => GetString(nameof(IO_FileTooLong2GB));

	public static string Lazy_Value_RecursiveCallsToValue => GetString(nameof(Lazy_Value_RecursiveCallsToValue));
	public static string Lazy_ToString_ValueNotCreated => GetString(nameof(Lazy_ToString_ValueNotCreated));
	public static string Lazy_ctor_ModeInvalid => GetString(nameof(Lazy_ctor_ModeInvalid));
	public static string Lazy_CreateValue_NoParameterlessCtorForT => GetString(nameof(Lazy_CreateValue_NoParameterlessCtorForT));
	// ReSharper restore InconsistentNaming
};