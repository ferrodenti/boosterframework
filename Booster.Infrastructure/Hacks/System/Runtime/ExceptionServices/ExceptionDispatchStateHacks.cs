namespace Booster.Hacks.System.Runtime.ExceptionServices;

public class ExceptionDispatchStateHacks
{
	public object Inner;

	public ExceptionDispatchStateHacks(object inner)
		=> Inner = inner;
}