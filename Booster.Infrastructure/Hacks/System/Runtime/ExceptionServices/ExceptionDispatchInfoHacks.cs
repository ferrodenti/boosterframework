using System;
using System.Runtime.ExceptionServices;
using Booster.Reflection;

#nullable enable

namespace Booster.Hacks.System.Runtime.ExceptionServices;

public class ExceptionDispatchInfoHacks
{
	public readonly ExceptionDispatchInfo Inner;

	FastLazy<ExceptionHacks>? _exception;
	public ExceptionHacks Exception => _exception ??= new ExceptionHacks(Inner.SourceException);

	public ExceptionDispatchInfoHacks(ExceptionDispatchInfo inner)
		=> Inner = inner;

	public static ExceptionDispatchInfoHacks Capture(Exception exception) 
		=> new(ExceptionDispatchInfo.Capture(exception));

	static readonly TypeEx _type = TypeEx.Get<ExceptionDispatchInfo>();

	static FastLazy<Field>? _dispatchStateField;
	public static Field DispatchStateField => _dispatchStateField ??= _type.FindField("_dispatchState")!;

	public ExceptionDispatchStateHacks DispatchState
	{
		get
		{
			var obj = DispatchStateField.GetValue(Inner);
			return new ExceptionDispatchStateHacks(obj);
		}
		set => DispatchStateField.SetValue(Inner, value.Inner);
	}

}