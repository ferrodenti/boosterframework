using System;
using Booster.Hacks.System.Runtime.ExceptionServices;
using Booster.Reflection;

#nullable enable

namespace Booster.Hacks.System;

public class ExceptionHacks
{
	public readonly Exception Inner;

	public ExceptionHacks(Exception inner)
		=> Inner = inner;

	static readonly TypeEx _type = typeof(Exception);

	static FastLazy<Method>? _captureDispatchStateMethod;

	protected static Method CaptureDispatchStateMethod => _captureDispatchStateMethod ??= _type.FindMethod("CaptureDispatchState")!;

	static FastLazy<Method>? _restoreDispatchStateMethod;

	protected static Method RestoreDispatchStateMethod => _restoreDispatchStateMethod ??= _type.FindMethod("RestoreDispatchState")!;

	static FastLazy<Method>? _setCurrentStackTraceMethod;

	protected static Method SetCurrentStackTraceMethod => _setCurrentStackTraceMethod ??= _type.FindMethod("SetCurrentStackTrace")!;

	public ExceptionDispatchStateHacks CaptureDispatchState()
	{
		var obj = CaptureDispatchStateMethod.Invoke(Inner);
		return new ExceptionDispatchStateHacks(obj);
	}

	public void RestoreDispatchState(ExceptionDispatchStateHacks state)
		=> RestoreDispatchStateMethod.Invoke(Inner, state.Inner);

	public void SetCurrentStackTrace()
		=> SetCurrentStackTraceMethod.Invoke(Inner);
}
