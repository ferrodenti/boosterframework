using System;
using System.ComponentModel;
using System.Threading;

namespace Booster;

public class DelayedEventHandler : BaseDelayedEventHandler<EventArgs>
{
	public event EventHandler Handler;

	public DelayedEventHandler(int delay = 300)
		: base(delay)
	{
	}

	protected override void Fire(object sender, EventArgs args)
		=> Handler?.Invoke(sender, args);

	public void Subscribe(EventHandler handler)
		=> Handler += handler;

	public void Unsubscribe(EventHandler handler)
		=> Handler += handler;
}

public class DelayedEventHandler<TArgs> : BaseDelayedEventHandler<TArgs> where TArgs : EventArgs
{
	public event EventHandler<TArgs> Handler;

	public DelayedEventHandler(int delay = 300)
		: base(delay)
	{
	}

	protected override void Fire(object sender, TArgs args)
		=> Handler?.Invoke(sender, args);
}

[Browsable(false)]
[EditorBrowsable(EditorBrowsableState.Never)]
public abstract class BaseDelayedEventHandler<TArgs>  where TArgs : EventArgs
{
	readonly int _delay;
	Timer _timer;
	readonly object _lock= new();


	protected BaseDelayedEventHandler(int delay)
        => _delay = delay;

    public void Invoke(object sender, TArgs ea)
	{
		lock (_lock)
			if (_timer != null)
			{
				_timer.Dispose();
				_timer = null;
			}

		// ReSharper disable once InconsistentlySynchronizedField
		_timer = new Timer(TimerProc, Tuple.Create(sender, ea), _delay, 0);
	}

	void TimerProc(object state)
	{
		lock (_lock)
			_timer = null;

		var args = (Tuple<object, TArgs>)state;

		Fire(args.Item1, args.Item2);
	}

	protected abstract void Fire(object sender, TArgs args);
}