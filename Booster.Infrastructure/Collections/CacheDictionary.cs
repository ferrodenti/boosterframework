﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using JetBrains.Annotations;
using Booster.Interfaces;
using Booster.Synchronization;
using Booster.Kitchen;


#nullable enable

namespace Booster.Collections;

[PublicAPI]
public class CacheDictionary<TKey, TValue> : 
	IAsyncLockableDictionary<TKey, TValue>, IDisposable 
	where TValue : class
{
	[PublicAPI]
	public class ItemCollectingEventArgs : EventArgs
	{
		public TKey Key { get; }
		public TValue Value { get; }
		public bool Cancel { get; set; }

		public ItemCollectingEventArgs(TKey key, TValue value)
		{
			Key = key;
			Value = value;
		}
	}

	class Entry
	{
		TValue? _value;
		readonly WeakReference<TValue> _weak;

		readonly bool _isNull;
		readonly bool _noWeakCheckExpiration;

		public DateTime Expiration;
		readonly TimeSpan _slidingExpiration;
		readonly DateTime _absoluteExpiration;

		public bool TryGetValue(out TValue result)
		{
			if (_value != null || _isNull)
			{
				OnAccess();
				result = _value!;
				return true;
			}

			if (_weak.TryGetTarget(out result))
			{
				_value = result;
				OnAccess();
				return true;
			}

			return false;
		}

		public bool TryGetWeakValue(out TValue result)
		{
			if (_value != null || _isNull)
			{
				result = _value!;
				return true;
			}

			return _weak.TryGetTarget(out result);
		}

		public bool Expire()
		{
			if (_value != null)
			{
				_value = null;
				return false;
			}

			if (_noWeakCheckExpiration)
				return true;

			return _weak.IsAlive();
		}

		public readonly TKey Key;

		public bool IsExpired => Expiration <= DateTime.Now;

		public Entry(TKey key, TValue? value, CacheSettings settings, bool noWeakCheckExpiration)
			: this(key, value, settings.SlidingExpiration, settings.GetAbsoluteExpirationFromNow(), noWeakCheckExpiration)
		{
		}

		public Entry(TKey key, TValue? value, TimeSpan slidingExpiration, DateTime absoluteExpiration, bool noWeakCheckExpiration)
		{
			Key = key;

			// ReSharper disable once ConditionIsAlwaysTrueOrFalse
			_isNull = Equals(value, null);
			_value = value;
			_weak = new WeakReference<TValue>(value!);

			_slidingExpiration = slidingExpiration;
			_absoluteExpiration = absoluteExpiration;
			_noWeakCheckExpiration = noWeakCheckExpiration;

			OnAccess();
		}

		public void OnAccess()
		{
			if (_slidingExpiration > default(TimeSpan))
			{
				Expiration = DateTime.Now + _slidingExpiration;

				if (_absoluteExpiration.IsSignificant())
					Expiration = Expiration.Min(_absoluteExpiration);
			}
			else if (_absoluteExpiration.IsSignificant())
				Expiration = _absoluteExpiration;
			else
				Expiration = DateTime.MaxValue;
		}

		public override string ToString()
		{
			if (_value != null)
				return _value.ToString();

			if (_isNull)
				return "(null)";

			if (_weak.IsAlive())
				return "(weak)";

			return "(dead)";
		}
	}

	readonly ConcurrentDictionary<TKey, Entry> _inner;

	readonly SyncLazy<TimeSchedule.Subscription> _gcSubscription = SyncLazy<TimeSchedule.Subscription>.Create<CacheDictionary<TKey, TValue>>(self =>
	{
		var interval = default(TimeSpan);

		void Update(TimeSpan timeSpan)
		{
			if (timeSpan.IsBetweenZeroAndMax() && (interval == TimeSpan.Zero || timeSpan < interval))
				interval = timeSpan;
		}

		Update(self.Settings.SlidingExpiration);
		Update(self.Settings.AbsoluteExpiration);
		Update(self.Settings.GCInterval);

		return TimeSchedule.GC.Subscribe(self, interval, self.GCProc);
	});
	protected TimeSchedule.Subscription GCSubscription => _gcSubscription.GetValue(this);

	public CacheSettings Settings { get; }
	public bool NoWeakCheckExpiration;

	public AsyncLock AsyncLock  { get; } = new();

	public event EventHandler<ItemCollectingEventArgs>? ItemCollecting;
	public event EventHandler<ItemCollectingEventArgs>? ItemCollected;

	CacheDictionary(ConcurrentDictionary<TKey, Entry> dictionary, CacheSettings? settings)
	{
		Settings = settings ?? new CacheSettings();
		_inner = dictionary;
	}

	void GCProc()
	{
		var next = DateTime.MaxValue;

		if(Settings.SlidingExpiration.IsBetweenZeroAndMax() || Settings.AbsoluteExpiration.IsBetweenZeroAndMax())
			foreach (var entry in _inner.Values)
			{
				if (entry.IsExpired)
				{
					TryCollectItem(entry);
					continue;
				}

				if (next > entry.Expiration)
					next = entry.Expiration;
			}

		if (Settings.AggressiveGCStart > 0 && _inner.Count >= Settings.AggressiveGCStart)
		{
			var queue = new PriorityQueue<Entry, DateTime>();
			foreach (var value in _inner.Values)
				queue.Enqueue(value, value.Expiration);

			var toCollect = _inner.Count - Settings.AggressiveGCStart + 1;
			for (var i = queue.Count; i >= 0 && toCollect > 0; --i)
			{
				var item = queue[i];
				if (item != null && TryCollectItem(item))
					toCollect--;
			}
		}

		if (next < DateTime.MaxValue)
			GCSubscription.Next = GCSubscription.Next.Min(next);
	}

	bool TryCollectItem(Entry entry)
	{
		using var lck = AsyncLock.Lock(timeout: 0);

		if(lck != null)
			if (ItemCollecting != null)
			{
				entry.TryGetWeakValue(out var val);
				var ea = new ItemCollectingEventArgs(entry.Key, val);
				ItemCollecting(this, ea);
				if (!ea.Cancel)
				{
					if (entry.Expire() && Remove(entry.Key))
						ItemCollected?.Invoke(this, ea);

					return true;
				}

				entry.OnAccess();
			}
			else
			{
				if (entry.Expire())
				{
					var handler = ItemCollected;
					if (handler == null)
						return Remove(entry.Key);

					if (TryRemove(entry.Key, out var val))
					{
						handler.Invoke(this, new ItemCollectingEventArgs(entry.Key, val));
						return true;
					}
				}
			}

		return false;
	}

	public CacheDictionary(CacheSettings? settings = null)
		: this(new ConcurrentDictionary<TKey, Entry>(), settings)
	{
	}

	public CacheDictionary(IEqualityComparer<TKey> comparer, CacheSettings? settings = null)
		: this(new ConcurrentDictionary<TKey, Entry>(comparer), settings)
	{
	}

	public CacheDictionary(IEnumerable<KeyValuePair<TKey, TValue>> collection, CacheSettings? settings = null)
		: this(collection, null, settings)
	{
	}
	
	public CacheDictionary(IEnumerable<KeyValuePair<TKey, TValue>> collection, IEqualityComparer<TKey>? comparer, CacheSettings? settings = null)
	{
		Settings = settings ?? new CacheSettings();
		comparer ??= EqualityComparer<TKey>.Default;
		
		_inner = new ConcurrentDictionary<TKey, Entry>(
			collection.Select(
				p => new KeyValuePair<TKey, Entry>(p.Key, CreateEntry(p)))
			, comparer);
	}

	Entry CreateEntry(KeyValuePair<TKey, TValue> pair) 
		=> CreateEntry(pair.Key, pair.Value);

	Entry CreateEntry(TKey key, TValue? value) 
		=> new(key, value, Settings, NoWeakCheckExpiration);

	public void Add(KeyValuePair<TKey, TValue> item) 
		=> Set(item.Key, CreateEntry(item));

	public void Add(TKey key, TValue? value) 
		=> Set(key, CreateEntry(key, value));

	public void Add(TKey key, TValue? value, TimeSpan slidingExpiration, DateTime absoluteExpiration) 
		=> Set(key, new Entry(key, value, slidingExpiration, absoluteExpiration, NoWeakCheckExpiration));

	void Set(TKey key, Entry entry)
	{
		_inner[key] = entry ?? throw new ArgumentNullException(nameof(entry));

		if (_inner.Count >= Settings.GCStart && GCSubscription.Next > entry.Expiration)
			GCSubscription.Next = entry.Expiration;
	}

	public bool Remove(KeyValuePair<TKey, TValue> item) => Remove(item.Key);
	public bool Remove(TKey key) => _inner.TryRemove(key, out _);

	public bool TryRemove(TKey key, out TValue value)
	{
		if (_inner.TryRemove(key, out var val) && val.TryGetWeakValue(out value))
			return true;

		value = default!;
		return false;
	}

	public bool TryGetValue(TKey key, out TValue value)
	{
		if (_inner.TryGetValue(key, out var entry))
		{
			if (entry.TryGetValue(out value))
			{
				if (value == null! && !Settings.CacheAbsent)
				{
					_inner.Remove(key);
					return false;
				}

				return true;
			}

			if (!Settings.CacheAbsent)
				_inner.Remove(key);
		}
		else
		{
			if (Settings.CacheAbsent)
				Add(key, null);

			value = default!;
		}

		return false;
	}

	public bool TryGetWeakValue(TKey key, out TValue value)
	{
		if (_inner.TryGetValue(key, out var entry))
		{
			if (entry.TryGetWeakValue(out value))
				return true;

			_inner.Remove(key);
		}
		else
			value = default!;

		return false;
	}

	public bool TrySetExpiration(TKey key, DateTime expiration)
	{
		if (!_inner.TryGetValue(key, out var entry) || entry.IsExpired) 
			return false;

		entry.Expiration = expiration;
		return true;
	}

	public TValue this[TKey key]
	{
		get
		{
			TryGetValue(key, out var value);
			return value;
		}
		set => Set(key, CreateEntry(key, value));
	}

	public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
	{
		foreach (var pair in _inner)
			if (pair.Value.TryGetValue(out var val))
				yield return new KeyValuePair<TKey, TValue>(pair.Key, val);
	}

	IEnumerator IEnumerable.GetEnumerator()
		=> GetEnumerator();

	public void Clear()
		=> _inner.Clear();

	public bool Contains(KeyValuePair<TKey, TValue> item)
		=> _inner.Contains(new KeyValuePair<TKey, Entry>(item.Key, CreateEntry(item)));

	public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
		=> this.Select(kv => kv).ToList().CopyTo(array, arrayIndex);

	public int Count => _inner.Count;
	public bool IsReadOnly => false;

	public bool ContainsKey(TKey key)
		=> _inner.ContainsKey(key);

	public ICollection<TKey> Keys => _inner.Keys;

	public ICollection<TValue> Values
	{
		get
		{
			var result = new List<TValue>();
			foreach (var pair in _inner)
				if (pair.Value.TryGetValue(out var val))
					result.Add(val);

			return result;
		}
	}

	public void Dispose()
	{
		_inner.Clear();
		_gcSubscription.DisposeReset();
	}
}