using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Booster.Interfaces;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Collections;

[PublicAPI]
public abstract class BaseLazyIndexedCollection<TItem, TKey> : ICollection<TItem>, IEmptyState 
	where TItem : class 
{
	protected class Record
	{
		public readonly TKey Key;
		public bool IsLoaded;
			
		TItem? _item;
		public TItem? Item
		{
			get => _item;
			set
			{
				_item = value;
				IsLoaded = value != null;
			} 
		}

		public Record(TKey key, TItem? item = default)
		{
			Item = item;
			Key = key;
		}

		public override string ToString()
			=> $"{Key}: {(IsLoaded ? Item.With(i => i.ToString(), "Missing") : "NotLoaded")}";
	}
		
	protected class KeyEqualityComparer : IEqualityComparer<TKey>
	{
		readonly IComparer<TKey> _comparer;

		public KeyEqualityComparer(IComparer<TKey> comparer)
			=> _comparer = comparer;

		public bool Equals(TKey x, TKey y) 
			=> _comparer.Compare(x, y) == 0;

		public int GetHashCode(TKey obj) 
			=> obj?.GetHashCode() ?? 0;
	}

	public IComparer<TKey> KeyComparer { get; set; } = Comparer<TKey>.Default;
		
	bool _unique = true;
	public bool Unique
	{
		get => _unique;
		set
		{
			if (_unique != value)
			{
				_unique = value;
				UpdateList();
			}
		}
	}

	bool _removeMissing = true;
	public bool RemoveMissing
	{
		get => _removeMissing;
		set
		{
			if (_removeMissing != value)
			{
				_removeMissing = value;
				UpdateList();
			}
		}
	}

	bool _sorted;
	public bool Sorted
	{
		get => _sorted;
		set
		{
			if (_sorted != value)
			{
				_sorted = value;
				UpdateList();
			}
		}
	}
		
	ReaderWriterLockSlim _lock = new(); // избегаем рекурсивных блокировок!!!
		
		
	//[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	protected List<Record> Records => _records.GetValue(this);
	readonly SyncLazy<List<Record>> _records = SyncLazy<List<Record>>.Create<BaseLazyIndexedCollection<TItem, TKey>>(self =>
	{
		self._lookup = new Dictionary<TKey, Record>(); // тоже чтобы в рекурсию не попасть
		self.PrepareRecords(self.InitRecords(), out var records, out self._lookup);
		return records.ToList();
	});
		
	//[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	protected Dictionary<TKey,Record> Lookup
	{
		get
		{
			if(_lookup == null)
				Utils.Nop(Records);

			return _lookup!;
		}
	}
	Dictionary<TKey, Record>? _lookup;
		
	readonly SyncLazy<IEqualityComparer<TKey>> _keyEqComparer = SyncLazy<IEqualityComparer<TKey>>
		.Create<BaseLazyIndexedCollection<TItem, TKey>>(self
			=> ReferenceEquals(self.KeyComparer, Comparer<TKey>.Default)
				? EqualityComparer<TKey>.Default
				: new KeyEqualityComparer(self.KeyComparer));

	protected IEqualityComparer<TKey> KeyEqComparer => _keyEqComparer.GetValue(this);
	
	//[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	public TKey[] Keys
	{
		get
		{
			using (ReadLock())
				return Records.Select(r => r.Key).ToArray();
		}
		set => Set(value);
	}

	//[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	public TItem?[] Items
	{
		get
		{
			using (UpgradeableReadLock())
			{
				EnsureLoaded();
				return Records.Select(r => r.Item).ToArray();
			}
		}
		set => Set(value.WhereSome());
	}

	protected BaseLazyIndexedCollection()
	{			
	}
		
	protected BaseLazyIndexedCollection(IEnumerable<TKey> keys)
	{
		Unique = false;
		Set(keys);
	}

	protected BaseLazyIndexedCollection(IEnumerable<TItem> items)
	{
		Unique = false;
		Set(items);
	}

	void UpdateList()
	{
		using (UpgradeableReadLock())
			if (_records.IsValueCreated)
				SetItems(Records);
	}
		
	public void Set(IEnumerable<TItem> items)
		=> SetItems(items.OrEmpty().Select(i => new Record(GetItemKey(i), i)));
		
	public void Set(IEnumerable<TKey> keys)
		=> SetItems(keys.OrEmpty().Select(k => new Record(k)));

	protected IDisposable ReadLock()
		=> _lock.ReadLock();
		
	protected IDisposable WriteLock()
		=> _lock.WriteLock();
		
	protected IDisposable UpgradeableReadLock()
		=> _lock.UpgradeableReadLock();
		
	protected void SetItems(IEnumerable<Record> newRecords)
	{
		if (Unique)
			newRecords = newRecords.Distinct(new LambdaComparison<Record>((x, y) => KeyEqComparer.Equals(x.Key, y.Key)));

		if (Sorted)
			newRecords = newRecords.OrderBy(r => r.Key, KeyComparer);

		using (WriteLock())
		{
			PrepareRecords(newRecords, out var records, out var lookup);
			_records.Value = records.ToList();
			_lookup = lookup;
				
			OnItemsChanged();
		}
	}

	protected void PrepareRecords(IEnumerable<Record> newRecords, out IEnumerable<Record> records, out Dictionary<TKey, Record> lookup)
	{
		if (Unique)
			newRecords = newRecords.Distinct(new LambdaComparison<Record>((x, y) => KeyEqComparer.Equals(x.Key, y.Key)));

		if (Sorted)
			newRecords = newRecords.OrderBy(r => r.Key, KeyComparer);
			
		var prevLookup = Lookup;
		var newLookup = new Dictionary<TKey, Record>(KeyEqComparer);

		records = newRecords.Select(record => newLookup.GetOrAdd(record.Key,
			key =>
			{
				if (prevLookup.TryGetValue(key, out var prevRec) && prevRec.Item != null)
					record.Item = prevRec.Item;
				else
					_isLoaded = false;

				return record;
			}));

		lookup = newLookup;
	}

	object _loadedLock = new();
	volatile bool _isLoaded;
			
	protected void EnsureLoaded()
	{
		if(!_isLoaded)
			lock(_loadedLock)
				if (!_isLoaded)
				{
					var records = Records;
					var lookup = Lookup;
					var notLoaded = lookup
						.Where(p => !p.Value.IsLoaded)
						.ToDictionary(p => p.Key, p => p.Value, KeyEqComparer);

					foreach (var item in LoadItems(notLoaded.Keys))
					{
						var key = GetItemKey(item);
						notLoaded[key].Item = item;
						notLoaded.Remove(key);
					}
												
					if (RemoveMissing && notLoaded.Count > 0)
						using (WriteLock())
						{
							foreach (var pair in notLoaded)
							{
								lookup.Remove(pair.Key);
								records.RemoveAll(i => KeyEqComparer.Equals(i.Key, pair.Key));
							}
							OnItemsChanged();
						}
						
					_isLoaded = true;
				}		
	}

	protected virtual List<Record> InitRecords() 
		=> new();

	protected void ReloadRecords()
	{
		_records.Reset();
		_lookup = null;
		_isLoaded = false;
	}

	protected virtual void OnItemsChanged()
	{
	}

	protected abstract TKey GetItemKey(TItem value);
	protected abstract IEnumerable<TItem> LoadItems(IEnumerable<TKey> keys);

	public IEnumerator<TItem> GetEnumerator()
	{
		using (UpgradeableReadLock())
		{
			EnsureLoaded();
			return Records.Select(r => r.Item).WhereSome().GetEnumerator();
		}
	}

	IEnumerator IEnumerable.GetEnumerator() 
		=> GetEnumerator();
		
	int FindIndexFor(TKey key)
	{
		var len = Records.Count;
		if (!Sorted)
			return len;
			
		var beg = 0;

		while(true)
		{
			if (len == 0)
				return beg;

			var mid = len / 2;
			var midv = Records[beg + mid].Key;

			if(KeyComparer.Compare(midv, key) > 0)
				len = mid;
			else
			{
				mid++;
				beg += mid;
				len -= mid;	
			}
		}
	}

	public bool Remove(TKey key, bool all = true)
	{		
		using (UpgradeableReadLock())
		{
			if (!Lookup.TryGetValue(key, out var record))
				return false;		

			using (WriteLock())
			{
				bool result;

				if (all)
				{
					result = Records.RemoveAll(r => KeyEqComparer.Equals(r.Key, record.Key)) > 0;
					Lookup.Remove(key);
				}
				else
				{
					result = Records.Remove(record);
						
					if (Records.All(r => !KeyEqComparer.Equals(r.Key, record.Key)))
						Lookup.Remove(key);
				}

				OnItemsChanged();

				return result;
			}
		}
	}
		
	// ReSharper disable once MethodOverloadWithOptionalParameter
	public bool Remove(TItem item, bool all = true) 
		=> Remove(GetItemKey(item), all);
		
	bool ICollection<TItem>.Remove(TItem item) 
		=> Remove(GetItemKey(item));

	public bool Add(TItem item)
		=> Add(new Record(GetItemKey(item), item));
		
	public bool Add(TKey key) 
		=> Add(new Record(key));

	public bool Set(TItem item, bool value)
	{
		if (value)
			return Add(item);

		return Remove(item);
	}

	void ICollection<TItem>.Add(TItem item) 
		=> Add(item);
		
	protected bool Add(Record record)
	{
		using (WriteLock())
		{
			if (Lookup.TryGetValue(record.Key, out var prevRec))
			{
				if (Unique)
					return false;

				record = prevRec;
			}
			else
			{
				Lookup.Add(record.Key, record);
					
				if (!record.IsLoaded)
					_isLoaded = false;
			}

			var idx = FindIndexFor(record.Key);
			Records.Insert(idx, record);

			OnItemsChanged();
				
			return true;
		}
	}

	public virtual void Clear()
	{
		using (WriteLock())
		{
			_records.Reset();
			_lookup = null;
			_isLoaded = true;
		}
	}

	public bool Contains(TKey key)
	{
		using (ReadLock())
			return Lookup.ContainsKey(key);
	}

	public bool Contains(TItem item)
		=> Contains(GetItemKey(item));	
		
	public void CopyTo(TItem[] array, int arrayIndex) 
		=> Items.CopyTo(array, arrayIndex);

	public int Count => Records.Count;
	public bool IsReadOnly => false;

	public bool IsEmpty => Records.Count == 0;
	public bool IsSome => Records.Count > 0;
			
	public static implicit operator TKey[](BaseLazyIndexedCollection<TItem, TKey>? items) 
		=> items?.Keys ?? Array.Empty<TKey>();

	public static implicit operator List<TKey>(BaseLazyIndexedCollection<TItem, TKey>? items) 
		=> items?.Keys.ToList() ?? new List<TKey>();	
}