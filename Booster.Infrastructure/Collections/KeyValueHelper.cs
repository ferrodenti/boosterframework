using System.Collections;
using System.Collections.Generic;
using Booster.Reflection;

#nullable enable

namespace Booster.Collections;

public class KeyValueHelper
{
	public static KeyValueHelper Instance = new();
	
	class Reflect
	{
		readonly BaseDataMember _keyProperty;
		readonly BaseDataMember _valueProperty;

		public Reflect(BaseDataMember keyProperty, BaseDataMember valueProperty)
		{
			_keyProperty = keyProperty;
			_valueProperty = valueProperty;
		}

		public (object? key, object? value) Read(object keyValueObj)
		{
			var key = _keyProperty.GetValue(keyValueObj);
			var value = _valueProperty.GetValue(keyValueObj);
			return (key, value);
		}
	}

	readonly Dictionary<TypeEx, Reflect?> _cache = new();

	bool TryReflect(TypeEx type, out Reflect result)
	{
		if (_cache.GetOrAdd(type, _ =>
		    {
			    var key = type.FindDataMember("Key");
			    var val = type.FindDataMember("Value");

			    if (key is { CanRead: true } && val is { CanRead: true })
				    return new Reflect(key, val);

			    return null;
		    }).Is(out result))
			return true;

		return false;
	}

	public bool TryRead(object? keyValueObj, out object? key, out object? value)
	{
		if (keyValueObj is DictionaryEntry de)
		{
			(key, value) = (de.Key, de.Value);
			return true;
		}
		if (keyValueObj == null ||
		    !TryReflect(keyValueObj.GetType(), out var reflect))
		{
			(key, value) = (null, null);
			return false;
		}
		
		(key, value) = reflect.Read(keyValueObj);
		return true;
	}
}
