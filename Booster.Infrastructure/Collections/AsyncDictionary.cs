﻿using System.Collections.Concurrent;
using Booster.Synchronization;
using JetBrains.Annotations;

namespace Booster.Collections;

[PublicAPI]
public class AsyncDictionary<TKey, TValue> 
	: ConcurrentDictionary<TKey, TValue>
{
	readonly AsyncLock _lock = new();


}