﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using Booster.Comparers;
using JetBrains.Annotations;

namespace Booster.Collections;

[PublicAPI]
public class OrderedDictionary<TKey, TValue> : IDictionary<TKey, TValue> //TODO: возможно надо все-таки скопировать код из OrderedDictionary
{
	readonly OrderedDictionary _inner;

	static IEqualityComparer CreateComparer(IEqualityComparer<TKey> equalityComparer)
		=> new LambdaEqComparer<TKey>(equalityComparer.Equals, equalityComparer.GetHashCode);

	public OrderedDictionary(IEqualityComparer<TKey> equalityComparer) 
		=> _inner = new OrderedDictionary(CreateComparer(equalityComparer));

	public OrderedDictionary(int capacity, IEqualityComparer<TKey> equalityComparer) 
		=> _inner = new OrderedDictionary(capacity, CreateComparer(equalityComparer));

	public OrderedDictionary(int capacity) 
		=> _inner = new OrderedDictionary(capacity);

	public OrderedDictionary(IEnumerable<KeyValuePair<TKey, TValue>> init)
	{
		if (init is ICollection<KeyValuePair<TKey, TValue>> coll)
			_inner = new OrderedDictionary(coll.Count);
		else
			_inner = new OrderedDictionary();

		foreach (var pair in init)
			_inner[pair.Key] = pair.Value;
	}
		
	public OrderedDictionary(IEnumerable<KeyValuePair<TKey, TValue>> init, IEqualityComparer<TKey> equalityComparer)
	{
		if (init is ICollection<KeyValuePair<TKey, TValue>> coll)
			_inner = new OrderedDictionary(coll.Count, CreateComparer(equalityComparer));
		else
			_inner = new OrderedDictionary(CreateComparer(equalityComparer));

		foreach (var pair in init)
			_inner[pair.Key] = pair.Value;
	}

	public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
	{
		IEnumerable<KeyValuePair<TKey, TValue>> AsIe()
		{
			foreach (DictionaryEntry kv in _inner)
				yield return new KeyValuePair<TKey, TValue>((TKey) kv.Key, (TValue) kv.Value);
		}

		return AsIe().GetEnumerator();
	}

	IEnumerator IEnumerable.GetEnumerator() 
		=> _inner.GetEnumerator();

	public void Add(KeyValuePair<TKey, TValue> item) 
		=> _inner.Add(item.Key, item.Value);

	public void Clear() 
		=> _inner.Clear();

	public bool Contains(KeyValuePair<TKey, TValue> item) 
		=> _inner.Contains(item);

	public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
	{
		foreach (var pair in this)
			array[arrayIndex++] = pair;
	}

	public bool Remove(KeyValuePair<TKey, TValue> item) 
		=> Remove(item.Key);

	public int Count => _inner.Count;

	public bool IsReadOnly => _inner.IsReadOnly;

	public void Add(TKey key, TValue value) 
		=> _inner.Add(key, value);

	public bool ContainsKey(TKey key) 
		=> _inner.Contains(key);

	public bool Remove(TKey key)
	{
		if (_inner.Contains(key))
		{
			_inner.Remove(key);
			return true;
		}
		return false;
	}

	public bool TryGetValue(TKey key, out TValue value)
	{
		if (_inner.Contains(key)) //TODO: попытаться как-то оптимизировать
		{
			value = (TValue)_inner[key];
			return true;
		}

		value = default;
		return false;
	}

	public TValue this[TKey key]
	{
		get => (TValue)_inner[key];
		set => _inner[key] = value;
	}

	public ICollection<TKey> Keys => _inner.Keys.Cast<TKey>().ToArray();

	public ICollection<TValue> Values => _inner.Values.Cast<TValue>().ToArray();
}