using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Booster.Reflection;
using JetBrains.Annotations;

namespace Booster.Collections;

[PublicAPI]
public class PriorityQueue<TValue, TPriority> : IReadOnlyList<TValue> where TPriority : IComparable
{
	readonly List<TValue> _items = new();
	public readonly List<TPriority> Priorities = new();

	public int Count => _items.Count;
	public bool IsEmpty => _items.Count == 0;

	readonly object _lock = new();

	public void Enqueue(TValue item, TPriority priority)
	{
		lock (_lock)
		{
			var i = FindIndexFor(priority);
			_items.Insert(i, item);
			Priorities.Insert(i, priority);
		}			
	}

	int FindIndexFor(TPriority priority)
	{
		var len = Priorities.Count;
		var beg = 0;

		while(true)
		{
			if (len == 0)
				return beg;

			var mid = len / 2;
			var midv = Priorities[beg + mid];

			if(midv.CompareTo(priority) > 0)
				len = mid;
			else
			{
				mid++;
				beg += mid;
				len -= mid;	
			}
		}
	}

	public int IndexOf(TValue value) 
		=> _items.IndexOf(value);

	public bool TryRemove(TValue value)
	{
		lock (_lock)
		{
			var i = IndexOf(value);
			if (i >= 0)
			{
				_items.RemoveAt(i);
				Priorities.RemoveAt(i);
				return true;
			}

			return false;
		}
	}
		
	public void Change(TValue value, TPriority newPriority)
	{
		lock (_lock)
		{
			var i = IndexOf(value);
			if (i >= 0)
			{
				_items.RemoveAt(i);
				Priorities.RemoveAt(i);
			}

			Enqueue(value, newPriority);
		}
	}

	public bool TryDequeue(out TValue value, out TPriority priority)
	{
		lock (_lock)
		{
			if (_items.Count == 0)
			{
				priority = (TPriority)TypeEx.Get<TPriority>().MaxValue;
				value = default;
				return false;
			}

			value = _items[0];
			priority = Priorities[0];
			_items.RemoveAt(0);
			Priorities.RemoveAt(0);
			return true;
		}
	}

	public void Clear()
	{
		lock (_lock)
		{
			_items.Clear();
			Priorities.Clear();
		}
	}

	// ReSharper disable once InconsistentlySynchronizedField
	public TPriority MinPriority => Priorities.FirstOrDefault();

	public IEnumerator<TValue> GetEnumerator() 
		=> _items.GetEnumerator();

	IEnumerator IEnumerable.GetEnumerator() 
		=> _items.GetEnumerator();

	public TValue this[int index] => _items[index];
}