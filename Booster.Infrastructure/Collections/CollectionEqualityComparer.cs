﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Booster.Collections;

[PublicAPI]
public class CollectionEqualityComparer<T> : IEqualityComparer<T> where  T : ICollection
{
	readonly IEqualityComparer _itemComparer;

	public CollectionEqualityComparer(IEqualityComparer itemComparer)
		=> _itemComparer = itemComparer;

	public CollectionEqualityComparer()
	{
	}

	public bool Equals(T x, T y)
	{
		if (ReferenceEquals(x, null))
			return ReferenceEquals(y, null);

		if (ReferenceEquals(y, null))
			return false;

		var cnt1 = x.Count;
		var cnt2 = y.Count;
		if (cnt1 != cnt2)
			return false;

		var ie1 = x.GetEnumerator();
		var ie2 = y.GetEnumerator();

		if (_itemComparer != null)
			while (ie1.MoveNext())
			{
				if (!ie2.MoveNext())
					return false;

				if (!_itemComparer.Equals(ie1.Current, ie2.Current))
					return false;
			}
		else
			while (ie1.MoveNext())
			{
				if (!ie2.MoveNext())
					return false;

				if (!Equals(ie1.Current, ie2.Current))
					return false;
			}
		return true;
	}


	public int GetHashCode(T obj)
	{
		unchecked
		{
			int len = obj.Count, i = len;

			// ReSharper disable LoopCanBeConvertedToQuery
			if (_itemComparer != null)
			{
				foreach (var o in obj)
					if (o != null)
						i += _itemComparer.GetHashCode(o);
			}
			else 
				foreach (var o in obj)
					if (o != null)
						i += o.GetHashCode();
			// ReSharper restore LoopCanBeConvertedToQuery

			return i;
		}
	}
}