﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Booster.Debug;
using Booster.Hacks.System;

namespace Booster.Collections;

[DebuggerDisplay("{DebuggerDisplay,nq}")]
[DebuggerTypeProxy(typeof(DictionaryDebugView))] //TODO: DebugView не работает, но это проблема студии
public class ListDictionary<TKey, TValue> : IDictionary<TKey, TValue>, ICollection
{
	DictionaryNode _head;
	int _version;
	readonly IEqualityComparer<TKey> _comparer;
	[NonSerialized] object _syncRoot;

	string DebuggerDisplay => $"Count = {Count}";

	public bool TryGetValue(TKey key, out TValue value)
	{
		if (key == null)
			throw new ArgumentNullException(nameof(key), SR.GetString("ArgumentNull_Key"));

		var dictionaryNode = _head;
		if (_comparer == null)
			for (; dictionaryNode != null; dictionaryNode = dictionaryNode.Next)
			{
				object obj = dictionaryNode.Key;
				if (obj != null && obj.Equals(key))
				{
					value = dictionaryNode.Value;
					return true;
				}
			}
		else
			for (; dictionaryNode != null; dictionaryNode = dictionaryNode.Next)
			{
				var x = dictionaryNode.Key;
				if (x != null && _comparer.Equals(x, key))
				{
					value = dictionaryNode.Value;
					return true;
				}
			}

		value = default;
		return false;
	}

	/// <summary>
	/// Gets or sets the Value associated with the specified Key.
	/// </summary>
	/// 
	/// <returns>
	/// The Value associated with the specified Key. If the specified Key is not found, attempting to get it returns null, and attempting to set it creates a new entry using the specified Key.
	/// </returns>
	/// <param name="key">The Key whose Value to get or set. </param><exception cref="T:System.ArgumentNullException"><paramref name="key"/> is null. </exception>
	public TValue this[TKey key]
	{
		get
		{
			TryGetValue(key, out var val);
			return val;
		}
		set
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key), SR.GetString("ArgumentNull_Key"));

			_version++;

			DictionaryNode dictionaryNode1 = null;
			DictionaryNode dictionaryNode2;
			for (dictionaryNode2 = _head; dictionaryNode2 != null; dictionaryNode2 = dictionaryNode2.Next)
			{
				var x = dictionaryNode2.Key;
				if (_comparer?.Equals(x, key) ?? x.Equals(key))
					break;

				dictionaryNode1 = dictionaryNode2;
			}

			if (dictionaryNode2 != null)
				dictionaryNode2.Value = value;
			else
			{
				var dictionaryNode3 = new DictionaryNode
				{
					Key = key,
					Value = value
				};

				if (dictionaryNode1 != null)
					dictionaryNode1.Next = dictionaryNode3;
				else
					_head = dictionaryNode3;

				Count++;
			}
		}
	}

	/// <summary>
	/// Gets the number of Key/Value pairs contained in the ListDictionary.
	/// </summary>
	/// <returns>
	/// The number of Key/Value pairs contained in the ListDictionary.
	/// </returns>
	public int Count { get; private set; }

	/// <summary>
	/// Gets an <see cref="T:System.Collections.ICollection"/> containing the keys in the ListDictionary.
	/// </summary>
	/// 
	/// <returns>
	/// An <see cref="T:System.Collections.ICollection"/> containing the keys in the ListDictionary.
	/// </returns>
	public ICollection<TKey> Keys => new KeyCollection(this);

	/// <summary>
	/// Gets a Value indicating whether the ListDictionary is read-only.
	/// </summary>
	/// 
	/// <returns>
	/// This property always returns false.
	/// </returns>
	public bool IsReadOnly => false;

	/// <summary>
	/// Gets a Value indicating whether the ListDictionary is synchronized (thread safe).
	/// </summary>
	/// 
	/// <returns>
	/// This property always returns false.
	/// </returns>
	public bool IsSynchronized => false;

	/// <summary>
	/// Gets an object that can be used to synchronize access to the ListDictionary.
	/// </summary>
	/// 
	/// <returns>
	/// An object that can be used to synchronize access to the ListDictionary.
	/// </returns>
	public object SyncRoot
	{
		get
		{
			if (_syncRoot == null)
				Interlocked.CompareExchange(ref _syncRoot, new object(), null);

			return _syncRoot;
		}
	}

	/// <summary>
	/// Gets an <see cref="T:System.Collections.ICollection"/> containing the values in the ListDictionary.
	/// </summary>
	/// 
	/// <returns>
	/// An <see cref="T:System.Collections.ICollection"/> containing the values in the ListDictionary.
	/// </returns>
	public ICollection<TValue> Values => new ValueCollection(this);

	/// <summary>
	/// Creates an empty ListDictionary using the default _comparer.
	/// </summary>
	public ListDictionary()
	{
	}
	public ListDictionary(IDictionary<TKey,TValue> initial)
	{
		if(initial != null)
			foreach (var pair in initial)
				this[pair.Key] = pair.Value;
	}

	/// <summary>
	/// Creates an empty ListDictionary using the specified _comparer.
	/// </summary>
	/// <param name="comparer">The <see cref="T:System.Collections.IComparer&lt;&gt;"/> to use to determine whether two keys are equal.-or- null to use the default _comparer, which is each Key's implementation of <see cref="M:System.Object.Equals(System.Object)"/>. </param>
	public ListDictionary(IEqualityComparer<TKey> comparer)
		=> _comparer = comparer ?? EqualityComparer<TKey>.Default;

	public ListDictionary(IDictionary<TKey,TValue> initial, IEqualityComparer<TKey> comparer) : this(initial)
		=> _comparer = comparer ?? EqualityComparer<TKey>.Default;


	/// <summary>
	/// Adds an entry with the specified Key and Value into the ListDictionary.
	/// </summary>
	/// <param name="key">The Key of the entry to add. </param><param name="value">The Value of the entry to add. The Value can be null. </param><exception cref="T:System.ArgumentNullException"><paramref name="key"/> is null. </exception><exception cref="T:System.ArgumentException">An entry with the same Key already exists in the ListDictionary. </exception>
	public void Add(TKey key, TValue value)
	{
		if (key == null)
			throw new ArgumentNullException(nameof(key), SR.GetString("ArgumentNull_Key"));

		_version++;
		DictionaryNode dictionaryNode1 = null;
		for (var dictionaryNode2 = _head; dictionaryNode2 != null; dictionaryNode2 = dictionaryNode2.Next)
		{
			var x = dictionaryNode2.Key;
			if (_comparer?.Equals(x, key) ?? x.Equals(key))
				throw new ArgumentException(SR.GetString("Argument_AddingDuplicate"));
			dictionaryNode1 = dictionaryNode2;
		}
		var dictionaryNode3 = new DictionaryNode
		{
			Key = key,
			Value = value
		};
		if (dictionaryNode1 != null)
			dictionaryNode1.Next = dictionaryNode3;
		else
			_head = dictionaryNode3;

		Count ++;
	}

	public void Add(KeyValuePair<TKey, TValue> item)
		=> Add(item.Key, item.Value);

	/// <summary>
	/// Removes all entries from the ListDictionary.
	/// </summary>
	public void Clear()
	{
		Count = 0;
		_head = null;
		_version ++;
	}

	public bool Contains(KeyValuePair<TKey, TValue> item)
		=> Equals(this[item.Key], item.Value);


	/// <summary>
	/// Determines whether the ListDictionary contains a specific Key.
	/// </summary>
	/// 
	/// <returns>
	/// true if the ListDictionary contains an entry with the specified Key; otherwise, false.
	/// </returns>
	/// <param name="key">The Key to locate in the ListDictionary. </param><exception cref="T:System.ArgumentNullException"><paramref name="key"/> is null. </exception>
	public bool ContainsKey(TKey key)
	{
		if (key == null)
			throw new ArgumentNullException(nameof(key), SR.GetString("ArgumentNull_Key"));

		for (var dictionaryNode = _head; dictionaryNode != null; dictionaryNode = dictionaryNode.Next)
		{
			var x = dictionaryNode.Key;
			if (_comparer?.Equals(x, key) ?? x.Equals(key))
				return true;
		}
		return false;
	}

	public void CopyTo(KeyValuePair<TKey, TValue>[] array, int index)
	{
		if (array == null)
			throw new ArgumentNullException(nameof(array));
		if (index < 0)
			throw new ArgumentOutOfRangeException(nameof(index), SR.GetString("ArgumentOutOfRange_NeedNonNegNum"));
		if (array.Length - index < Count)
			throw new ArgumentException(SR.GetString("Arg_InsufficientSpace"));

		for (var dictionaryNode = _head; dictionaryNode != null; dictionaryNode = dictionaryNode.Next)
		{
			array.SetValue(new DictionaryEntry(dictionaryNode.Key, dictionaryNode.Value), index);
			++index;
		}
	}

	/// <summary>
	/// Copies the ListDictionary entries to a one-dimensional <see cref="T:System.Array"/> instance at the specified index.
	/// </summary>
	/// <param name="array">The one-dimensional <see cref="T:System.Array"/> that is the destination of the <see cref="T:System.Collections.DictionaryEntry"/> objects copied from ListDictionary. The <see cref="T:System.Array"/> must have zero-based indexing. </param><param name="index">The zero-based index in <paramref name="array"/> at which copying begins. </param><exception cref="T:System.ArgumentNullException"><paramref name="array"/> is null. </exception><exception cref="T:System.ArgumentOutOfRangeException"><paramref name="index"/> is less than zero. </exception><exception cref="T:System.ArgumentException"><paramref name="array"/> is multidimensional.-or- The number of elements in the source ListDictionary is greater than the available space from <paramref name="index"/> to the end of the destination <paramref name="array"/>. </exception><exception cref="T:System.InvalidCastException">The type of the source ListDictionary cannot be cast automatically to the type of the destination <paramref name="array"/>. </exception>
	public void CopyTo(Array array, int index)
	{
		if (array == null)
			throw new ArgumentNullException(nameof(array));
		if (index < 0)
			throw new ArgumentOutOfRangeException(nameof(index), SR.GetString("ArgumentOutOfRange_NeedNonNegNum"));
		if (array.Length - index < Count)
			throw new ArgumentException(SR.GetString("Arg_InsufficientSpace"));
		for (var dictionaryNode = _head; dictionaryNode != null; dictionaryNode = dictionaryNode.Next)
		{
			array.SetValue(new DictionaryEntry(dictionaryNode.Key, dictionaryNode.Value), index);
			++index;
		}
	}

	/// <summary>
	/// Returns an <see cref="T:System.Collections.IDictionaryEnumerator"/> that iterates through the ListDictionary.
	/// </summary>
	/// 
	/// <returns>
	/// An <see cref="T:System.Collections.IDictionaryEnumerator"/> for the ListDictionary.
	/// </returns>
	public IEnumerator<KeyValuePair<TKey,TValue>> GetEnumerator()
		=> new NodeEnumerator(this);

	IEnumerator IEnumerable.GetEnumerator()
		=> new NodeEnumerator(this);

	public bool Remove(KeyValuePair<TKey, TValue> item)
		=> Remove(item.Key);

	/// <summary>
	/// Removes the entry with the specified Key from the ListDictionary.
	/// </summary>
	/// <param name="key">The Key of the entry to remove. </param><exception cref="T:System.ArgumentNullException"><paramref name="key"/> is null. </exception>
	public bool Remove(TKey key)
	{
		if (key == null)
			throw new ArgumentNullException(nameof(key), SR.GetString("ArgumentNull_Key"));
		_version ++;
		DictionaryNode dictionaryNode1 = null;
		DictionaryNode dictionaryNode2;

		for (dictionaryNode2 = _head; dictionaryNode2 != null; dictionaryNode2 = dictionaryNode2.Next)
		{
			var x = dictionaryNode2.Key;
			if (_comparer?.Equals(x, key) ?? x.Equals(key))
				break;

			dictionaryNode1 = dictionaryNode2;
		}
		if (dictionaryNode2 == null)
			return false;

		if (dictionaryNode2 == _head)
			_head = dictionaryNode2.Next;
		else if (dictionaryNode1 != null) 
			dictionaryNode1.Next = dictionaryNode2.Next;

		Count--;

		return true;
	}

	class NodeEnumerator : IEnumerator<KeyValuePair<TKey,TValue>>
	{
		readonly ListDictionary<TKey,TValue> _list;
		DictionaryNode _current;
		readonly int _version;
		bool _start;

		public KeyValuePair<TKey,TValue> Current 
		{
			get
			{
				if (_current == null)
					throw new InvalidOperationException(SR.GetString("InvalidOperation_EnumOpCantHappen"));

				return new KeyValuePair<TKey,TValue>(_current.Key, _current.Value);
			}
		}

		public NodeEnumerator(ListDictionary<TKey,TValue> list)
		{
			_list = list;
			_version = list._version;
			_start = true;
			_current = null;
		}

		public bool MoveNext()
		{
			if (_version != _list._version)
				throw new InvalidOperationException(SR.GetString("InvalidOperation_EnumFailedVersion"));

			if (_start)
			{
				_current = _list._head;
				_start = false;
			}
			else
				_current = _current?.Next;

			return _current != null;
		}

		public void Reset()
		{
			if (_version != _list._version)
				throw new InvalidOperationException(SR.GetString("InvalidOperation_EnumFailedVersion"));

			_start = true;
			_current = null;
		}

		object IEnumerator.Current => Current;

		public void Dispose()
		{
		}
	}

	class KeyCollection : ICollection<TKey>
	{
		readonly ListDictionary<TKey,TValue> _list;

		void ICollection<TKey>.Add(TKey item) 
			=> throw new NotSupportedException();

		void ICollection<TKey>.Clear()
		{
		}

		bool ICollection<TKey>.Remove(TKey item) 
			=> throw new NotSupportedException();

		public bool Contains(TKey key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key), SR.GetString("ArgumentNull_Key"));

			return _list.ContainsKey(key);
		}

		public int Count
		{
			get
			{
				var num = 0;

				for (var dictionaryNode = _list._head; dictionaryNode != null; dictionaryNode = dictionaryNode.Next)
					++num;

				return num;
			}
		}

		public bool IsReadOnly => true;

		public KeyCollection(ListDictionary<TKey,TValue> list) 
			=> _list = list;

		public void CopyTo(TKey[] array, int index)
		{
			if (array == null)
				throw new ArgumentNullException(nameof(array));

			if (index < 0)
				throw new ArgumentOutOfRangeException(nameof(index), SR.GetString("ArgumentOutOfRange_NeedNonNegNum"));

			for (var dictionaryNode = _list._head; dictionaryNode != null; dictionaryNode = dictionaryNode.Next)
			{
				array.SetValue(dictionaryNode.Key, index);
				++index;
			}
		}

		public IEnumerator<TKey> GetEnumerator() 
			=> new KeysEnumerator(_list);

		IEnumerator IEnumerable.GetEnumerator() 
			=> GetEnumerator();

		class KeysEnumerator : IEnumerator<TKey>
		{
			readonly ListDictionary<TKey,TValue> _list;
			DictionaryNode _current;
			readonly int _version;
			bool _start;

			public TKey Current
			{
				get
				{
					if (_current == null)
						throw new InvalidOperationException(SR.GetString("InvalidOperation_EnumOpCantHappen"));

					return _current.Key;
				}
			}

			public KeysEnumerator(ListDictionary<TKey,TValue> list)
			{
				_list = list;
				_version = list._version;
				_start = true;
				_current = null;
			}

			public bool MoveNext()
			{
				if (_version != _list._version)
					throw new InvalidOperationException(SR.GetString("InvalidOperation_EnumFailedVersion"));
				if (_start)
				{
					_current = _list._head;
					_start = false;
				}
				else
					_current = _current?.Next;

				return _current != null;
			}

			public void Reset()
			{
				if (_version != _list._version)
					throw new InvalidOperationException(SR.GetString("InvalidOperation_EnumFailedVersion"));
				_start = true;
				_current = null;
			}

			object IEnumerator.Current => Current;

			public void Dispose()
			{
			}
		}
	}

	class ValueCollection : ICollection<TValue>
	{
		readonly ListDictionary<TKey,TValue> _list;

		void ICollection<TValue>.Add(TValue item) 
			=> throw new NotSupportedException();

		void ICollection<TValue>.Clear()
		{
		}

		bool ICollection<TValue>.Remove(TValue item) 
			=> throw new NotSupportedException();

		public bool Contains(TValue item)
		{
			for (var dictionaryNode = _list._head; dictionaryNode != null; dictionaryNode = dictionaryNode.Next)
				if (Equals(dictionaryNode.Value, item))
					return true;
			return false;
		}

		public int Count
		{
			get
			{
				var num = 0;

				for (var dictionaryNode = _list._head; dictionaryNode != null; dictionaryNode = dictionaryNode.Next)
					++num;

				return num;
			}
		}

		public bool IsReadOnly => true;

		public ValueCollection(ListDictionary<TKey,TValue> list) 
			=> _list = list;

		public void CopyTo(TValue[] array, int index)
		{
			if (array == null)
				throw new ArgumentNullException(nameof(array));

			if (index < 0)
				throw new ArgumentOutOfRangeException(nameof(index), SR.GetString("ArgumentOutOfRange_NeedNonNegNum"));

			for (var dictionaryNode = _list._head; dictionaryNode != null; dictionaryNode = dictionaryNode.Next)
			{
				array.SetValue(dictionaryNode.Value, index);
				++index;
			}
		}

		public IEnumerator<TValue> GetEnumerator() 
			=> new ValuesEnumerator(_list);

		IEnumerator IEnumerable.GetEnumerator() 
			=> GetEnumerator();

		class ValuesEnumerator : IEnumerator<TValue>
		{
			readonly ListDictionary<TKey,TValue> _list;
			DictionaryNode _current;
			readonly int _version;
			bool _start;

			public TValue Current
			{
				get
				{
					if (_current == null)
						throw new InvalidOperationException(SR.GetString("InvalidOperation_EnumOpCantHappen"));

					return _current.Value;
				}
			}

			public ValuesEnumerator(ListDictionary<TKey,TValue> list)
			{
				_list = list;
				_version = list._version;
				_start = true;
				_current = null;
			}

			public bool MoveNext()
			{
				if (_version != _list._version)
					throw new InvalidOperationException(SR.GetString("InvalidOperation_EnumFailedVersion"));
				if (_start)
				{
					_current = _list._head;
					_start = false;
				}
				else
					_current = _current?.Next;

				return _current != null;
			}

			public void Reset()
			{
				if (_version != _list._version)
					throw new InvalidOperationException(SR.GetString("InvalidOperation_EnumFailedVersion"));
				_start = true;
				_current = null;
			}

			object IEnumerator.Current => Current;

			public void Dispose()
			{
			}
		}
	}

	[Serializable]
	class DictionaryNode
	{
		public TKey Key;
		public TValue Value;
		public DictionaryNode Next;
	}
}