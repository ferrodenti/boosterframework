using System;
using System.Collections;
using static Booster.FastLazyStatic;

#nullable enable
namespace Booster.Collections;

public class DictionaryEnumerator : IDictionaryEnumerator, IDisposable
{
	readonly IEnumerator _enumerator;

	public DictionaryEnumerator(IEnumerator enumerator)
		=> _enumerator = enumerator;

	void Setup()
	{
		_entry = null;
		Key = null;
		Value = null;
	}

	public bool MoveNext()
	{
		Setup();
		return _enumerator.MoveNext();
	}

	public void Reset()
	{
		Setup();
		_enumerator.Reset();
	}

	public object? Current => _enumerator.Current;

	FastLazy<DictionaryEntry>? _entry;
	public DictionaryEntry Entry => (_entry ??= FastLazy(() =>
	{
		if (KeyValueHelper.Instance.TryRead(Current, out var key, out var value))
		{
			Key = key;
			Value = value;

			return new DictionaryEntry(Key!, Value);
		}
		return new DictionaryEntry();
	}));

	public object? Key { get; private set; }
	public object? Value { get; private set; }

	public void Dispose()
	{
		Setup();
		(_enumerator as IDisposable)?.Dispose();
	}
}
