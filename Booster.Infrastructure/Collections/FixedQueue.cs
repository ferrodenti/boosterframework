using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace Booster.Collections;

[PublicAPI] 
public class FixedQueue<T> : IList<T>
{
	readonly T[] _inner;
	readonly int _capacity;
	int _tail;
	int _head;

	public event EventHandler<EventArgs<T>> Removed;

	public FixedQueue(int capacity)
	{
		_capacity = capacity;
		_inner = new T[capacity];
	}

	public void Add(T item)
	{
		_inner[_head] = item;
		_head = ++_head%_capacity;

		if (_head == _tail)
		{
			Removed?.Invoke(this, new EventArgs<T>(_inner[_tail]));
			_tail = ++_tail%_capacity;
		}
		else
			Count ++;
	}


	public IEnumerator<T> GetEnumerator()
	{
		for (var i = _tail; i != _head; i = ++i%_capacity)
			yield return _inner[i];
	}

	IEnumerator IEnumerable.GetEnumerator()
		=> GetEnumerator();


	public void Clear()
		=> _head = _tail = Count = 0;

	public bool Contains(T item)
		=> this.Any(i => i.Equals(item));

	public void CopyTo(T[] array, int arrayIndex)
	{
		foreach (var item in this)
			array[arrayIndex++] = item;
	}

	public bool Remove(T item)
	{
		var i = IndexOf(item);
		if (i >= 0)
		{
			RemoveAt(i);
			return true;
		}
		return false;
	}

	public int Count { get; private set; }
	public bool IsReadOnly => false;

	public int IndexOf(T item)
	{
		var res = 0;
		foreach (var i in this)
			if (i.Equals(item))
				return res;
			else
				res++;

		return -1;
	}

	public void Insert(int index, T item)
	{
		if (index >= Count)
			throw new ArgumentOutOfRangeException(nameof(index));

		_inner[(_tail + index)%_capacity] = item;
	}

	public void RemoveAt(int index)
	{
		if (index >= Count)
			throw new ArgumentOutOfRangeException(nameof(index));

		var i = (_tail + index)%_capacity;
		Removed?.Invoke(this, new EventArgs<T>(_inner[i]));
		for (; i != _head - 1; i = ++i%_capacity)
			_inner[i] = _inner[(i + 1)%_capacity];

		_head = --_head%_capacity;
		Count--;
	}

	public T this[int index]
	{
		get
		{
			if (index >= Count)
				throw new ArgumentOutOfRangeException(nameof(index));

			return _inner[index%_capacity];
		}
		set
		{
			if (index >= Count)
				throw new ArgumentOutOfRangeException(nameof(index));

			_inner[index%_capacity] = value;
		}
	}
}