﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Booster.Debug;

#nullable enable

namespace Booster.Collections;

[Serializable]
[DebuggerDisplay("{DebuggerDisplay,nq}")]
[DebuggerTypeProxy(typeof(DictionaryDebugView))] //TODO: DebugView не работает, но это проблема студии
public class HybridDictionary<TKey,TValue> : IDictionary<TKey, TValue>, IDictionary
{
	const int _cutoverPoint = 9;
	const int _initialHashtableSize = 13;
	const int _fixedSizeCutoverPoint = 6;

	readonly IEqualityComparer<TKey> _equalityComparer;
	ListDictionary<TKey,TValue>? _list;
	Dictionary<TKey,TValue>? _hashtable;
	IDictionary<TKey, TValue>? _inner;

	string DebuggerDisplay => $"Count = {Count}";
	
	public event EventHandler? Changed;
	
	protected virtual void OnChanged()
		=> Changed?.Invoke(this, EventArgs.Empty);
	
	/// <summary>Creates an empty<see cref="T:Booster.Collections. HybridDictionary" />.</summary>
	public HybridDictionary()
		=> _equalityComparer = EqualityComparer<TKey>.Default;

	/// <summary>Creates an empty <see cref="T:Booster.Collections. HybridDictionary" /> with the specified equality сomparer.</summary>
	public HybridDictionary(IEqualityComparer<TKey> equalityComparer)
		=> _equalityComparer = equalityComparer;

	/// <summary>Creates a <see cref="T:Booster.Collections. HybridDictionary" /> with the specified initial size and equality сomparer.</summary>
	/// <param name="initialSize">The approximate number of entries that the <see cref="T:Booster.Collections. HybridDictionary" /> can initially contain. </param>
	/// <param name="equalityComparer"></param>
	public HybridDictionary(int initialSize, IEqualityComparer<TKey>? equalityComparer = null)
	{
		_equalityComparer = equalityComparer ?? EqualityComparer<TKey>.Default;

		if (initialSize < _fixedSizeCutoverPoint)
			return;

		_hashtable = new Dictionary<TKey, TValue>(initialSize, _equalityComparer);
	}
		
	public HybridDictionary(IDictionary<TKey,TValue> initial, IEqualityComparer<TKey>?equalityComparer = null)
	{
		_equalityComparer = equalityComparer ?? EqualityComparer<TKey>.Default;

		if (initial.Count >= _fixedSizeCutoverPoint)
			_hashtable = new Dictionary<TKey, TValue>(initial.Count, _equalityComparer);

		foreach (var pair in initial)
			Add(pair.Key, pair.Value);
	}

	public HybridDictionary(IReadOnlyDictionary<TKey,TValue> initial, IEqualityComparer<TKey>? equalityComparer = null)
	{
		_equalityComparer = equalityComparer ?? EqualityComparer<TKey>.Default;

		if (initial.Count >= _fixedSizeCutoverPoint)
			_hashtable = new Dictionary<TKey, TValue>(initial.Count, _equalityComparer);

		foreach (var pair in initial)
			Add(pair.Key, pair.Value);
	}

	public bool TryGetValue(TKey key, out TValue value)
	{
		var dict = _inner;
		if (dict != null)
			return dict.TryGetValue(key, out value);

		value = default!;
		return false;
	}

	/// <summary>Gets or sets the value associated with the specified key.</summary>
	/// <param name="key">The key whose value to get or set. </param>
	/// <returns>The value associated with the specified key. If the specified key is not found, attempting to get it returns <see langword="null" />, and attempting to set it creates a new entry using the specified key.</returns>
	/// <exception cref="T:System.ArgumentNullException">
	/// <paramref name="key" /> is <see langword="null" />. </exception>
	public TValue this[TKey key]
	{
		get
		{
			TryGetValue(key, out var res);
			return res;
		}
		set
		{
			if (_hashtable != null)
				_hashtable[key] = value;
			else if (_list != null)
			{
				if (_list.Count + 1 >= _cutoverPoint)
				{
					ChangeOver();
					_hashtable![key] = value;
				}
				else
					_list[key] = value;
			}
			else
				_inner = _list = new ListDictionary<TKey, TValue>(_equalityComparer) {[key] = value};
			
			OnChanged();
		}
	}

	void ChangeOver()
	{
		var hashtable = new Dictionary<TKey, TValue>(_initialHashtableSize, _equalityComparer);
		
		if(_list != null)
			foreach (var pair in _list)
				hashtable[pair.Key] = pair.Value;
			
		_inner = _hashtable = hashtable;
		_list = null;
	}

	public bool Remove(KeyValuePair<TKey, TValue> item) => Remove(item.Key);

	public void CopyTo(Array array, int index) => throw new NotSupportedException();

	/// <summary>Gets the number of key/value pairs contained in the <see cref="T:Booster.Collections. HybridDictionary" />.</summary>
	/// <returns>The number of key/value pairs contained in the <see cref="T:Booster.Collections. HybridDictionary" />.Retrieving the value of this property is an O(1) operation.</returns>
	public int Count => _inner?.Count ?? 0;

	/// <summary>Gets an <see cref="T:System.Collections.ICollection" /> containing the keys in the <see cref="T:Booster.Collections. HybridDictionary" />.</summary>
	/// <returns>An <see cref="T:System.Collections.ICollection" /> containing the keys in the <see cref="T:Booster.Collections. HybridDictionary" />.</returns>
	public ICollection<TKey> Keys => _inner?.Keys ?? Array.Empty<TKey>();

	

	/// <summary>Gets an <see cref="T:System.Collections.ICollection" /> containing the values in the <see cref="T:Booster.Collections. HybridDictionary" />.</summary>
	/// <returns>An <see cref="T:System.Collections.ICollection" /> containing the values in the <see cref="T:Booster.Collections. HybridDictionary" />.</returns>
	public ICollection<TValue> Values => _inner?.Values ?? Array.Empty<TValue>();

	/// <summary>Gets a value indicating whether the <see cref="T:Booster.Collections. HybridDictionary" /> is read-only.</summary>
	/// <returns>This property always returns <see langword="false" />.</returns>
	public bool IsReadOnly => false;

	

	/// <summary>Gets a value indicating whether the <see cref="T:Booster.Collections. HybridDictionary" /> is synchronized (thread safe).</summary>
	/// <returns>This property always returns <see langword="false" />.</returns>
	public bool IsSynchronized => false;

	/// <summary>Gets an object that can be used to synchronize access to the <see cref="T:Booster.Collections. HybridDictionary" />.</summary>
	/// <returns>An object that can be used to synchronize access to the <see cref="T:Booster.Collections. HybridDictionary" />.</returns>
	public object SyncRoot => this;


	/// <summary>Adds an entry with the specified key and value into the <see cref="T:Booster.Collections. HybridDictionary" />.</summary>
	/// <param name="key">The key of the entry to add. </param>
	/// <param name="value">The value of the entry to add. The value can be <see langword="null" />. </param>
	/// <exception cref="T:System.ArgumentNullException">
	/// <paramref name="key" /> is <see langword="null" />. </exception>
	/// <exception cref="T:System.ArgumentException">An entry with the same key already exists in the <see cref="T:Booster.Collections. HybridDictionary" />. </exception>
	public void Add(TKey key, TValue value)
	{
		if (_hashtable != null)
			_hashtable.Add(key, value);
		else if (_list == null)
			_inner = _list = new ListDictionary<TKey,TValue>(_equalityComparer) {{key, value}};
		else if (_list.Count + 1 >= _cutoverPoint)
		{
			ChangeOver();
			_hashtable?.Add(key, value);
		}
		else
			_list.Add(key, value);
		
		OnChanged();
	}

	public void Add(KeyValuePair<TKey, TValue> item) 
		=> Add(item.Key, item.Value);


	#region IDictionary

	TKey GetKey(object? key)
	{
		if (key is TKey result)
			return result;

		var display = key != null ? $"{key} ({key.GetType().Name})" : "(null)";
		throw new ArgumentException($"Expected {display}", nameof(key));
	}
	
	TValue GetValue(object? value)
	{
		if (value is TValue result)
			return result;

		return default!;
	}

	object IDictionary.this[object key]
	{
		get => this[GetKey(key)]!;
		set => this[GetKey(key)] = GetValue(value);
	}
	
	void IDictionary.Add(object key, object value)
	{
		if (key is TKey tk &&
		    value is TValue tv)
			Add(tk, tv);
		else
			throw new ArgumentException();
	}
	
	bool IDictionary.Contains(object key)
		=> ContainsKey(GetKey(key)!);
	
	IDictionaryEnumerator IDictionary.GetEnumerator()
		=> new DictionaryEnumerator(GetEnumerator());

	public void Remove(object key)
		=> Remove(GetKey(key));

	public bool IsFixedSize => false;
	
	ICollection IDictionary.Values => Values.ToArray();

	ICollection IDictionary.Keys => Keys.ToArray();
	
	#endregion
	
	

	

	/// <summary>Removes all entries from the <see cref="T:Booster.Collections. HybridDictionary" />.</summary>
	public void Clear()
	{
		_inner = null;

		if (_hashtable != null)
		{
			var hashtable = _hashtable;
			_hashtable = null;
			hashtable.Clear();
		}

		if (_list != null)
		{
			var list = _list;
			_list = null;
			list.Clear();
		}
		
		OnChanged();
	}


	public bool Contains(KeyValuePair<TKey, TValue> item) 
		=> _inner?.ContainsKey(item.Key) ?? false;

	public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex) 
		=> _inner?.CopyTo(array, arrayIndex);

	/// <summary>Determines whether the <see cref="T:Booster.Collections. HybridDictionary" /> contains a specific key.</summary>
	/// <param name="key">The key to locate in the <see cref="T:Booster.Collections. HybridDictionary" />. </param>
	/// <returns>
	/// <see langword="true" /> if the <see cref="T:Booster.Collections. HybridDictionary" /> contains an entry with the specified key; otherwise, <see langword="false" />.</returns>
	/// <exception cref="T:System.ArgumentNullException">
	/// <paramref name="key" /> is <see langword="null" />. </exception>
	public bool ContainsKey(TKey key) 
		=> _inner?.ContainsKey(key) ?? false;

	/// <inheritdoc />
	/// <summary>Returns an <see cref="T:System.Collections.IDictionaryEnumerator" /> that iterates through the <see cref="T:Booster.Collections. HybridDictionary" />.</summary>
	/// <returns>An <see cref="T:System.Collections.IDictionaryEnumerator" /> for the <see cref="T:Booster.Collections. HybridDictionary" />.</returns>
	public IEnumerator<KeyValuePair<TKey,TValue>> GetEnumerator() 
		=> _inner?.GetEnumerator() ?? Enumerable.Empty<KeyValuePair<TKey,TValue>>().GetEnumerator();

	IEnumerator IEnumerable.GetEnumerator() 
		=> GetEnumerator();

	/// <summary>Removes the entry with the specified key from the <see cref="T:Booster.Collections. HybridDictionary" />.</summary>
	/// <param name="key">The key of the entry to remove. </param>
	/// <exception cref="T:System.ArgumentNullException">
	/// <paramref name="key" /> is <see langword="null" />. </exception>
	public bool Remove(TKey key)
	{
		if (_inner?.Remove(key) == true)
		{
			OnChanged();
			return true;
		}

		return false;
	}
}

