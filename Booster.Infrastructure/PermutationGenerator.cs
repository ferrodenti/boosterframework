
using System.Linq;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Numerics;

#nullable enable

namespace Booster;

public class PermutationGenerator
{
	public static readonly PermutationGenerator Instance = new();

	const int _maxVariants = ushort.MaxValue;

	static readonly int _defaultCachingOffset = 1;

	static PermutationGenerator()
	{
		while (Factorial(_defaultCachingOffset) <= _maxVariants)
			++_defaultCachingOffset;
	}

	public int CachingOffset { get; set; } = _defaultCachingOffset;

	readonly ConcurrentDictionary<int, int[][]> _cache = new();

	static bool NextSet(IList<int> array)
	{
		var last = array.Count - 1;
		var j = array.Count - 2;

		while (j >= 0 && array[j] >= array[j + 1])
			--j;

		if (j < 0)
			return false; // больше перестановок нет

		var k = last;
		while (array[j] >= array[k])
			--k;

		(array[j], array[k]) = (array[k], array[j]);

		int l = j + 1, r = last; // сортируем оставшуюся часть последовательности
		while (l < r)
		{
			var x = l++;
			var y = r--;
			(array[x], array[y]) = (array[y], array[x]);
		}

		return true;
	}

	static IEnumerable<int[]> GeneratePermutations(int count)
	{
		var array = new int[count];

		for (var i = 0; i < count; ++i)
			array[i] = i;

		int[] Result()
			=> (int[]) array.Clone();

		yield return Result();

		while (NextSet(array))
			yield return Result();
	}

	public IEnumerable<int[]> GetPermutations(int count)
	{
		if (count <= CachingOffset)
			return _cache.GetOrAdd(count, n => GeneratePermutations(n).ToArray());

		return GeneratePermutations(count);
	}

	public IEnumerable<T[]> Permutatate<T>(IEnumerable<T> values)
	{
		var arr = values.ToArray();

		foreach (var permutation in GetPermutations(arr.Length))
			yield return permutation.Select(i => arr[i]).ToArray();
	}

	public static BigInteger Factorial(int n)
	{
		var factorial = new BigInteger(1);
		for (var i = 1; i <= n; ++i)
			factorial *= i;

		return factorial;
	}
}