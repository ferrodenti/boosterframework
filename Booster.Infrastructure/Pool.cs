using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Booster.DI;
using Booster.Log;
using JetBrains.Annotations;

namespace Booster;

#nullable enable

public class Pool<T> : IDisposable where T : class
{
	readonly Func<Task<T>> _factory;
		
	readonly HashSet<T> _items = new();
	readonly ConcurrentStack<T> _stack = new();
	readonly ConcurrentStack<Exception> _exceptions = new();
		
	readonly SemaphoreSlim _semaphore = new(0);
	readonly SemaphoreSlim _reallocLock = new(1, 1);
		
	volatile int _minFree;
	volatile int _desiredCapacity = 3;
	int _maxCapacity = 20;
	int _minCapacity = 5;

	public int MaxCapacity
	{
		get => _maxCapacity;
		set
		{
			if (_maxCapacity != value)
			{
				_maxCapacity = value;
				Realloc(Capacity);
			}
		}
	}

	public int MinCapacity
	{
		get => _minCapacity;
		set
		{
			if (_minCapacity != value)
			{
				_minCapacity = value;
				Realloc(Capacity);
			}
		}
	}

	public int Capacity => _items.Count;
	public bool IsFull => Capacity >= MaxCapacity;
		
	public int AutoGrowStep { get; set; } = 2;
	public int MaxItemsReleasedPerGCIter { get; set; } = 10;

	[PublicAPI] public TimeSpan GCInterval
	{
		get => _gcSubscription.Interval;
		set => _gcSubscription.Interval = value;
	}

	readonly TimeSchedule.Subscription _gcSubscription;
	readonly ILog? _log;

	public Pool(Func<Task<T>> factory, ILog? log)
	{
		_log = log.Create(this);
		_factory = factory;
		_minFree = MaxCapacity;
		_gcSubscription = TimeSchedule.GC.Subscribe(this, TimeSpan.FromMinutes(1), () =>
		{
			var maxToRelese = Math.Min(_minFree, MaxItemsReleasedPerGCIter);
			var cnt = 0;
			for (var i = 0; i < maxToRelese && Capacity > MinCapacity; ++i)
				if (_stack.TryPop(out var toRelease))
				{
					_items.Remove(toRelease);
					(toRelease as IDisposable)?.Dispose();
					cnt++;
				}

			if (cnt > 0)
				_log.Debug($"{cnt} pool items disposed, current={Capacity}");
			
			_minFree = MaxCapacity;
		});
	}

	void Realloc(int newCapacity)
	{
		if (newCapacity < MinCapacity)
			newCapacity = MinCapacity;
		if (newCapacity > MaxCapacity)
			newCapacity = MaxCapacity;

		if (newCapacity == Capacity /*|| newCapacity == _desiredCapacity*/)
			return;

		_desiredCapacity = Math.Max(newCapacity, _desiredCapacity);
		var ctx = InstanceFactory.CurrentContext;

		Utils.StartTaskNoFlow(async () =>
		{
			using (InstanceFactory.PushContext(ctx))
				try
				{
					await _reallocLock.WaitAsync().NoCtx();
					var cap = Capacity;
					var cnt = 0;
					while (cap != _desiredCapacity)
					{
						if (cap > _desiredCapacity)
						{
							if (_stack.TryPeek(out var item))
							{
								_items.Remove(item);
								(item as IDisposable)?.Dispose();
								cnt--;
							}
							else
								return;
						}
						else if (cap < _desiredCapacity)
						{
							var item = await _factory().NoCtx();
							_stack.Push(item);
							_items.Add(item);
							_semaphore.Release(1);
							cnt++;
						}

						cap = Capacity;
					}

					if (cnt != 0)
						_log.Debug($"Realloc: {cnt} new pool items, current={Capacity}");
				}
				catch (Exception ex)
				{
					_exceptions.Push(ex);
					_semaphore.Release(1);
				}
				finally
				{
					_reallocLock.Release(1);
				}
		});
	}

	public async Task<T> TakeAsync(CancellationToken token)
	{
		T res;
		while (!_stack.TryPop(out res))
		{
			Realloc(Capacity + AutoGrowStep);
				
			await _semaphore.WaitAsync(token).NoCtx();

			if (_exceptions.TryPop(out var ex))
				throw ex;
		}

		if (_minFree > _stack.Count)
			_minFree = _stack.Count;
			
		return res;
	}

	public T Take()
	{
		T res;
		while (!_stack.TryPop(out res))
		{
			Realloc(Capacity + AutoGrowStep);

			_semaphore.Wait();// TODO: Pool.TakeTimeout, throw exception
			//if (!_semaphore.Wait(5000))
			//{
			//	Utils.Nop();
			//}

			if (_exceptions.TryPop(out var ex))
				throw ex;
		}

		if (_minFree > _stack.Count)
			_minFree = _stack.Count;
			
		return res;
	}

	public void Release(T item)
	{
		_stack.Push(item);
		_semaphore.Release(1);
	}
		
	public void Change(T? oldItem, T newItem)
	{
		if (oldItem != null)
			_items.Remove(oldItem);

		_items.Add(newItem);
	}

	public void Dispose()
	{
		_gcSubscription.Dispose();
		_desiredCapacity = 0;

		while (_semaphore.Wait(0))
			Utils.Nop();

		var items = _items.ToArray();

		_stack.Clear();
		_items.Clear();

		foreach (var item in items)
			Try.IgnoreErrors(() =>
				(item as IDisposable)?.Dispose());
	}
}