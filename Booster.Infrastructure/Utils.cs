using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net.Mail;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Booster.DI;
using Booster.Log;
using Booster.Reflection;
using JetBrains.Annotations;

#nullable enable

namespace Booster;

[PublicAPI]
public static class Utils
{
	public static readonly object TraceLock = new();

	/// <summary>
	/// Мы не хотим, чтобы в запущенные задания передавалось наше соединение с бд и другие CtxLocal
	/// </summary>
	/// <param name="proc"></param>
	/// <param name="token"></param>
	/// <returns></returns>
	public static Task StartTaskNoFlow(Action proc, CancellationToken token = default)
	{
		using (ExecutionContext.SuppressFlow())
			return Task.Run(() =>
			{
				using (AsyncContext.Push())
					proc();
			}, token);
	}

	public static Task StartTaskNoFlow(Func<Task> proc, CancellationToken token = default)
	{
		using (ExecutionContext.SuppressFlow())
			return Task.Run(async () =>
			{
				using (AsyncContext.Push())
					await proc().NoCtx();

			}, token);
	}

	public static Task StartTaskNoFlow(Func<Task> proc, [InstantHandle]Action<Exception>? errorHandler, CancellationToken token = default)
	{
		using (ExecutionContext.SuppressFlow())
			return Task.Run(async () =>
			{
				try
				{
					using (AsyncContext.Push())
						await proc().NoCtx();

				}
				catch (TaskCanceledException) { }
				catch (Exception ex)
				{
					errorHandler?.Invoke(ex);
				}
			}, token);
	}


	public static Task<TResult> StartTaskNoFlow<TResult>(Func<Task<TResult>> proc, CancellationToken token = default)
	{
		using (ExecutionContext.SuppressFlow())
			return Task.Run(async () =>
			{
				using (AsyncContext.Push())
					return await proc().NoCtx();
			}, token);
	}

	/// <summary>
	/// Мы не хотим, чтобы в запущенные задания передавалось наше соединение с бд и другие CtxLocal
	/// </summary>
	/// <param name="proc"></param>
	/// <param name="token"></param>
	/// <returns></returns>
	public static Task<TResult> StartTaskNoFlow<TResult>(Func<TResult> proc, CancellationToken token = default)
	{
		using (ExecutionContext.SuppressFlow())
			return Task.Run(() =>
			{
				using (AsyncContext.Push())
					return proc();
			}, token);
	}

	public static CancellationToken TimeoutToken(TimeSpan delay)
		=> delay < TimeSpan.Zero ? default : new CancellationTokenSource(delay).Token;

	public static CancellationToken TimeoutToken(int millisecondsDelay)
		=> millisecondsDelay < 0 ? default : new CancellationTokenSource(millisecondsDelay).Token;

	/// <summary>
	/// Мы не хотим, чтобы в запущенные задания передавалось наше соединение с бд и другие CtxLocal
	/// </summary>
	/// <param name="proc"></param>
	/// <param name="token"></param>
	/// <param name="errorHandler"></param>
	/// <returns></returns>
	public static void ForgetTaskNoFlow([InstantHandle]Action proc, [InstantHandle]Action<Exception>? errorHandler = null, CancellationToken token = default)
	{
		using (ExecutionContext.SuppressFlow())
			Task.Run(() =>
			{
				using (AsyncContext.Push())
					try
					{
						proc();
					}
					catch (TaskCanceledException) { }
					catch (Exception ex)
					{
						errorHandler?.Invoke(ex);
					}
			}, token);
	}

	public static void ForgetTaskNoFlow([InstantHandle]Func<Task> proc, [InstantHandle]Action<Exception>? errorHandler = null, CancellationToken token = default)
	{
		using (ExecutionContext.SuppressFlow())
			Task.Run(async () =>
			{
				using (AsyncContext.Push())
					try
					{
						await proc().NoCtx();
					}
					catch (TaskCanceledException) { }
					catch (Exception ex)
					{
						errorHandler?.Invoke(ex);
					}
			}, token);
	}

	[MethodImpl(MethodImplOptions.NoInlining)]
	public static object? Nop(params object?[]? arr)
		=> arr?.FirstOrDefault();

	[MethodImpl(MethodImplOptions.NoInlining)]
	public static void Nop<T>(params object?[]? _)
		=> Nop(typeof(T));

	[MethodImpl(MethodImplOptions.NoInlining)]
	public static Task<object?> AsyncNop(params object?[]? arr)
		=> Task.FromResult(arr?.FirstOrDefault());

	[MethodImpl(MethodImplOptions.NoInlining)]
	public static Task AsyncNop<T>(params object?[]? _)
		=> AsyncNop(typeof(T));
	
	public static T Default<T>()
		=> default!;

	// ReSharper disable once InconsistentNaming
	public static bool IsDBNull(object? o) 
		=> Convert.IsDBNull(o);

	[ContractAnnotation("null => true")]
	public static bool IsNull([NotNullWhen(true)]object? o)
		=> o == null;
		
	#region Error handling

	

	[Obsolete("Use Try.OrDefault")]
	public static T? IgnoreErrors<T>([InstantHandle] Func<T?> evaluator)
		=> Try.OrDefault(evaluator);

	[Obsolete("Use Try.OrDefaultAsync")]
	public static Task<TResult?> IgnoreErrorsAsync<TResult>([InstantHandle] Func<Task<TResult?>> evaluator)
		=> Try.OrDefaultAsync(evaluator);

	[Obsolete("Use Try.OrDefaultAsync")]
	public static Task<TResult?> IgnoreErrorsAsync<TException, TResult>([InstantHandle] Func<Task<TResult?>> evaluator)
		where TException : Exception
		=> Try.OrDefaultAsync<TException, TResult>(evaluator);
		
	
		
	#endregion
		
	public static Regex CompileRegex(string pattern, bool unformat = true, bool ignoreCase = true)
	{
		if (unformat)
			pattern = pattern.Replace("\n", "").Replace("\r", "").Replace("	", "");
		
		var options = RegexOptions.CultureInvariant | RegexOptions.Multiline | RegexOptions.Compiled;

		if (ignoreCase)
			options |= RegexOptions.IgnoreCase;
		
		return new Regex(pattern, options);
	}

	static readonly Regex _emailRegex = CompileRegex(@"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
													 + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
													 + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$");

	public static bool IsValidEmail(string? emailAddress)
	{
		if (emailAddress.IsEmpty())
			return false;

		if (!_emailRegex.IsMatch(emailAddress))
			return false;

		try
		{
			Nop(new MailAddress(emailAddress));
			return true;
		}
		catch
		{
			return false;
		}
	}

	public static bool TryNormalizeEmail(ref string? emailAddress)
	{
		var str = emailAddress?.Trim().ToLower();
		if (str != null && IsValidEmail(str))
		{
			emailAddress = str;
			return true;
		}

		return false;
	}

	const int _minPhoneLength = 12;

	//8777666554433

	public static bool IsValidPhone(string? phone)
		=> TryNormalizePhone(ref phone);

	public static bool TryNormalizePhone(ref string? phone)
	{

		var trimmed = phone?.Trim() ?? "";

		if (trimmed.IsEmpty())
			return false;

		var bld = new StringBuilder("+");

		var i = 0;

		if (trimmed.StartsWith('8'))
		{
			bld.Append('7');
			++i;
		}
		else if (trimmed.StartsWith("+")) 
			++i;

		for (var len = trimmed.Length; i < len; ++i)
		{
			var chr = trimmed[i];

			if (chr.IsDigit())
			{
				bld.Append(chr);
				continue;
			}

			if(char.IsWhiteSpace(chr) || chr is '(' or ')' or '-')
				continue;

			break;
		}

		if (bld.Length < _minPhoneLength)
			return false;

		phone = bld.ToString();
		return true;
	}

	public static string? FormatPhone(string phone, string separator = " ", bool operatorBrackets = true)
	{
		if (phone.Length < _minPhoneLength)
			return null;

		var bld = new EnumBuilder(separator);
		bld.Append(phone.SubstringSafe(0, 2));

		bld.Append(operatorBrackets
			? $"({phone.SubstringSafe(2, 3)})" 
			: phone.SubstringSafe(2, 3));

		bld.Append(phone.SubstringSafe(5, 3));
		bld.Append(phone.SubstringSafe(8, 2));
		bld.Append(phone.SubstringSafe(10, 2));
		bld.AppendIfSome(phone.SubstringSafe(12));

		return bld;
	}


	public static int StringIp2Int(string ip)
	{
		var res = 0;
		var bytes = ip.Split('.');
		if( bytes.Length == 4 )
			foreach (var str in bytes)
			{
				res <<= 8;
					
				if (byte.TryParse(str, out var b))
					res += b & 0xFF;
			}
		return res;
	}

	public static string IntIp2String(int ip)
		=> $"{(ip >> 24) & 0xFF}.{(ip >> 16) & 0xFF}.{(ip >> 8) & 0xFF}.{ip & 0xFF}";

	public static double JulianDate2Double(DateTime dt)
		=> dt.ToOADate() + 2415018.5;

	public static DateTime Double2JulianDate(double d)
		=> DateTime.FromOADate(d - 2415018.5);

	public static async Task<TResult?> RepeatTillSuccessAsync<TResult>([InstantHandle] Func<TaskRepeatOptions, Task<TResult>> proc, TaskRepeatOptions? repeatOptions = null)
	{
		repeatOptions ??= TaskRepeatOptions.Default;
		int delay = -1, i=0;

		while (!repeatOptions.CancellationToken.IsCancellationRequested)
			try
			{
				return await proc(repeatOptions).NoCtx();
			}
			catch (Exception e)
			{
				if (repeatOptions.ExceptionType != null && !TypeEx.Of(e).CanCastTo(repeatOptions.ExceptionType))
					throw;

				if (++i >= repeatOptions.Atempts)
				{
					if (repeatOptions.Throw)
						throw;

					break;
				}

				if (delay < 0)
				{
					var n = Math.Max(1, repeatOptions.ExpDelay
						? Enumerable.Range(1, repeatOptions.Atempts + 1).SumOrDefault(v => v)
						: repeatOptions.TotalDelay);

					delay = repeatOptions.TotalDelay / n;
				}

				if (delay > 0)
				{
					await Task.Delay(delay, repeatOptions.CancellationToken).NoCtx();

					if (repeatOptions.ExpDelay)
						delay *= 2;
				}
			}

		return default;
	}

	public static TResult? RepeatTillSuccess<TResult>([InstantHandle] Func<TaskRepeatOptions,TResult> proc, TaskRepeatOptions? repeatOptions = null)
	{
		repeatOptions ??= TaskRepeatOptions.Default;
		int delay = -1, i=0;

		while (!repeatOptions.CancellationToken.IsCancellationRequested)
			try
			{
				return proc(repeatOptions);
			}
			catch (Exception e)
			{
				if (repeatOptions.ExceptionType != null && !TypeEx.Of(e).CanCastTo(repeatOptions.ExceptionType))
					throw;

				if (++i >= repeatOptions.Atempts)
				{
					if (repeatOptions.Throw)
						throw;

					repeatOptions.Log?.Error(e);
					break;
				}

				if (delay < 0)
				{
					var n = Math.Max(1, repeatOptions.ExpDelay
						? Enumerable.Range(1, repeatOptions.Atempts + 1).SumOrDefault(v => v)
						: repeatOptions.TotalDelay);

					delay = repeatOptions.TotalDelay / n;
				}

				if (delay > 0)
				{
					Thread.Sleep(delay);

					if (repeatOptions.ExpDelay)
						delay *= 2;
				}
			}

		return default;
	}

	public static async Task RunWithMaxDegreeOfConcurrency<T>(int maxDegreeOfConcurrency, IEnumerable<T> collection, Func<T, Task> taskFactory)
	{
		var activeTasks = new List<Task>(maxDegreeOfConcurrency);
		foreach (var task in collection.Select(taskFactory))
		{
			activeTasks.Add(task);
			if (activeTasks.Count == maxDegreeOfConcurrency)
			{
				await Task.WhenAny(activeTasks.ToArray()).NoCtx();
				activeTasks.RemoveAll(t => t.IsCompleted); 
			}
		}
		await Task.WhenAll(activeTasks.ToArray()).NoCtx();
	}
}