using System.Collections.Generic;

#nullable enable

namespace Booster.Comparers;

public class ArrayEqualityComparer : IEqualityComparer<int[]>
{
	public static ArrayEqualityComparer Instance = new();

	public bool Equals(int[]? x, int[]? y)
	{
		if (x == null)
			return y == null;

		if (y == null)
			return false;

		if (x.Length != y.Length)
			return false;

		for (var i = 0; i < x.Length; i++)
			if (x[i] != y[i])
				return false;

		return true;
	}

	public int GetHashCode(int[] obj)
	{
		var result = new HashCode(obj.Length);
		result.AppendEnumerable(obj);
		return result;
	}
}