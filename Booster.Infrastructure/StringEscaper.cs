﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JetBrains.Annotations;

// ReSharper disable StringIndexOfIsCultureSpecific.2

#nullable enable

namespace Booster;

public class StringEscaperRule
{
	public StringEscaperRule(string from, string to)
	{
		To = to;
		From = from;
	}

	public string From { get; }
	public string To { get; }
}

public class StringEscaper
{
	protected class Hash
	{
		public Dictionary<char, List<string>> Patterns { get; }
		public char[] StartChars { get; }

		public Hash(IReadOnlyList<string> patterns, bool ignoreCase)
		{
			Patterns = new Dictionary<char, List<string>>();

// ReSharper disable once ForCanBeConvertedToForeach
			for (var i = 0; i < patterns.Count; i++)
				Patterns.GetOrAdd(patterns[i][0], _ => new List<string>())
					.Add(ignoreCase ? patterns[i].ToLower() : patterns[i]);

			StartChars = Patterns.Keys.ToArray();
		}

		public Hash(Hash hash, IReadOnlyList<string> patterns, bool ignoreCase)
		{
			Patterns = new Dictionary<char, List<string>>(hash.Patterns);

// ReSharper disable once ForCanBeConvertedToForeach
			for (var i = 0; i < patterns.Count; i++)
				Patterns.GetOrAdd(patterns[i][0], _ => new List<string>()).Add(ignoreCase ? patterns[i].ToLower() : patterns[i]);

			StartChars = Patterns.Keys.ToArray();
		}
	}

	public List<StringEscaperRule> Rules { get; }

	readonly string[] _froms;
	readonly string[] _tos;

	public bool IgnoreCase { get; set; } = true;

	public StringEscaper(params StringEscaperRule[] rules)
	{
		Rules = rules.ToList();

		_froms = new string[rules.Length];
		_tos = new string[rules.Length];

		var i = 0;
		foreach (var r in rules)
		{
			_froms[i] = r.From;
			_tos[i++] = r.To;
		}
	}

	Hash? _fromsHash;
	protected Hash FromsHash => _fromsHash ??= new Hash(_froms, IgnoreCase);

	Hash? _tosHash;
	protected Hash TosHash => _tosHash ??= new Hash(_tos, IgnoreCase);

	public StringEscaper(string escapeChar, params string[] protection)
		: this(CreateRules(escapeChar, protection))
	{
	}

	protected static StringEscaperRule[] CreateRules(string escapeChar, string[] protection)
	{
		var rules = new StringEscaperRule[protection.Length + 1];
		rules[0] = new StringEscaperRule(escapeChar, escapeChar + escapeChar);

		for (var i = 0; i < protection.Length; i++)
			rules[i + 1] = new StringEscaperRule(protection[i], escapeChar + protection[i]);

		return rules;
	}

	public string? Escape(string str)
		=> Replace(str, _froms, _tos);

	public string? Unescape(string str)
		=> Replace(str, _tos, _froms);

	string? Replace(string? str, string[] froms, string[] tos)
	{
		if (str == null)
			return null;

		var pos = 0;
		var searchStr = IgnoreCase ? str.ToLower() : str;
		var srcLen = str.Length;
		var res = new StringBuilder(srcLen);
		do
		{
			var id = Search(searchStr, pos, froms, FromsHash, out var tokenPos);
			if (id < 0)
				break;

			var len = tokenPos - pos;
			if (len > 0)
				res.Append(str.Substring(pos, len));

			res.Append(tos[id]);
			pos += len + froms[id].Length;

		} while (pos < srcLen);

		if (pos < srcLen)
			res.Append(str.Substring(pos, srcLen - pos));

		return res.ToString();
	}

	static int Search(string str, int index, IReadOnlyList<string> patterns, Hash hash, out int tokenPos)
	{
		tokenPos = str.Length;

		var j = str.IndexOfAny(hash.StartChars, index);
		if (j >= 0)
			for (var i = 0; i < patterns.Count; i++)
			{
				var pat = patterns[i];
				var len = pat.Length;
				if (j + len > str.Length)
					continue;

				if (str.IndexOf(pat, j, len, StringComparison.Ordinal) == j)
				{
					tokenPos = j;
					return i;
				}
			}

		return -1;
	}

	readonly ConcurrentDictionary<string, Hash> _sepaCache = new();

	public IEnumerable<string> Split(string? source, string separator, bool keepEmpty = false, bool unescape = false)
	{
		if (source.IsEmpty())
			return Enumerable.Empty<string>();

		if (source.IndexOfAny(TosHash.StartChars) < 0)
			return source.SplitNonEmpty(separator);

		var patterns = _tos.Concat(new[] {IgnoreCase ? separator : separator.ToLower()}).ToArray();
		var hash = _sepaCache.GetOrAdd(separator, _ => new Hash(patterns, IgnoreCase));

		return SplitInt(source, keepEmpty, unescape, patterns, hash);
	}

	[PublicAPI]
	public IEnumerable<string> Split(string? source, string[] separators, bool keepEmpty = false, bool unescape = false)
	{
		if (source == null)
			return Enumerable.Empty<string>();

		var patterns = _tos.Concat(IgnoreCase ? separators : separators.Select(s => s.ToLower())).ToArray();

		return SplitInt(source, keepEmpty, unescape, patterns, new Hash(patterns, IgnoreCase));
	}

	IEnumerable<string> SplitInt(string source, bool keepEmpty, bool unescape, string[] patterns, Hash hash)
	{
		int pos = 0, searchFrom = 0, len;

		var searchStr = IgnoreCase ? source.ToLower() : source;
		var srcLen = source.Length;
		do
		{
			var id = Search(searchStr, searchFrom, patterns, hash, out var tokenPos);
			if (id < 0)
				break;

			if (id < _tos.Length)
			{
				searchFrom = tokenPos + patterns[id].Length;
				continue;
			}

			len = tokenPos - pos;
			if (len > 0)
			{
				var str = source.Substring(pos, len);
				yield return unescape ? Replace(str, _tos, _froms)! : str;
			}
			else if (keepEmpty)
				yield return string.Empty;

			pos = tokenPos + patterns[id].Length;
			searchFrom = pos;
		} while (pos < srcLen);

		len = srcLen - pos;
		if (len > 0)
		{
			var str = source.Substring(pos, len);
			yield return unescape ? Replace(str, _tos, _froms)! : str;
		}
		else if (keepEmpty)
			yield return string.Empty;
	}

	public string Join(IEnumerable<string> values, string separator)
	{
		var str = new EnumBuilder(separator);
		foreach (var val in values)
			str.Append(Escape(val));

		return str;
	}

	//int IndexOf(string str, string value, int index)
	//{
	//	return IgnoreCase ? str.IndexOf(value, index, StringComparison.OrdinalIgnoreCase) : str.IndexOf(value, index);
	//}

	//HybridDictionary CreateCache()
	//{
	//	return new HybridDictionary(!IgnoreCase);
	//}
}