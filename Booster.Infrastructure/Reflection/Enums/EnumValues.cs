using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Booster.Helpers;
using JetBrains.Annotations;
using static Booster.FastLazyStatic;

#nullable enable

namespace Booster.Reflection;

[PublicAPI]
public class EnumValues : IEnumerable<EnumValue> //TODO: ToString
{
	readonly EnumHelper _owner;

	IEnumerable<EnumValue> _descriptors;
	FastLazy<EnumValue[]>? _descriptorsArray;

	public EnumValue[] Descriptors => _descriptorsArray ??= _descriptors.AsArray();

	public int Count => Descriptors.Length;

	public EnumValues Visible => new(_owner, Descriptors.Where(v => v.Visible));
	public EnumValues Default => new(_owner, Descriptors.Where(v => v.IsDefault));
	public EnumValues NonDefault => new(_owner, Descriptors.Where(v => !v.IsDefault));
	public EnumValues PowerOfTwo => new(_owner, Descriptors.Where(v => v.IsPowerOfTwo));
	public EnumValues UniqueFlags => new(_owner, GetUniqueFlags());
	public IEnumerable Values => Descriptors.Select(d => d.Value);

	public TValue[] GetValues<TValue>()
		=> Values.Cast<TValue>().ToArray();
	
	IEnumerable<EnumValue> GetUniqueFlags()
	{
		var values = Descriptors.OrderBy(v => v.IntValue).ToArray();
		var sum = _owner.UnderlyingType.DefaultValue;
		
		foreach (var v in values)
		{
			var n = NumericHelper.Or(sum, v.IntValue);
			if (Equals(n, sum))
				continue;

			sum = n;
			yield return v;
		}
	}

	FastLazy<object>? _mergedValue;

	public object MergedValue => (_mergedValue ??= FastLazy(() =>
	{
		var sum = _owner.UnderlyingType.DefaultValue ?? 0;
		
		foreach (var v in Descriptors)
			sum = NumericHelper.Or(sum, v.IntValue);

		return sum;
	})).Value; //Тут необходимо использовать Value, чтобы не получить FastLazy<object> на выходе

	public IDictionary<object, string> ToDictionary(bool enumValuesInsteadOfInt = true, bool displayNameInsteadOfEnumValues = true)
		=> Descriptors.OrderBy(v => v.Order).ToDictionary(
			v => enumValuesInsteadOfInt ? v.Value : v.IntValue,
			v => displayNameInsteadOfEnumValues ? v.DisplayName : v.Name);
		
	public EnumValues(EnumHelper owner, IEnumerable<EnumValue> inner)
	{
		_owner = owner;
		_descriptors = inner;
	}

	public IEnumerator<EnumValue> GetEnumerator()
		=> Descriptors.OrderBy(v => v.Order).GetEnumerator();

	IEnumerator IEnumerable.GetEnumerator()
		=> GetEnumerator();
}