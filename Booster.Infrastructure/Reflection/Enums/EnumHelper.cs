using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Booster.Helpers;
using JetBrains.Annotations;

namespace Booster.Reflection;

[PublicAPI]
public class EnumHelper
{
	public TypeEx EnumType { get; }
	public TypeEx UnderlyingType { get; }
	public bool IsFlags { get; }

	EnumValues _values;
	public EnumValues Values => _values ??= new EnumValues(this, Enum.GetValues(EnumType).Cast<object>().Select(v => new EnumValue(v)));

	public EnumValue this[string enumValue] => Values.FirstOrDefault(v => v.Name.EqualsIgnoreCase(enumValue));
	public EnumValue this[int intValue] => Values.FirstOrDefault(v => Equals(v.IntValue, intValue));
	public EnumValue this[object enumValue] => Values.FirstOrDefault(v => Equals(v.Value, enumValue));

	readonly SyncLazy<EnumValue> _default = SyncLazy<EnumValue>.Create<EnumHelper>(self => self.Values.Default.FirstOrDefault());
	public EnumValue Default => _default.GetValue(this);
		
	protected EnumHelper(TypeEx enumType)
	{
		EnumType = enumType;
		UnderlyingType = enumType.TypeInfo.GetEnumUnderlyingType();
		IsFlags = enumType.HasAttribute<FlagsAttribute>();
	}

	#region statics
	static readonly ConcurrentDictionary<TypeEx, EnumHelper> _cache = new();

	public static EnumHelper Get<TEnum>() 
		=> Get(typeof (TEnum));

	public static EnumHelper Get(Type type) 
		=> Get(TypeEx.Get(type));

	public static EnumHelper Get(TypeEx type)
	{
		if (!type.IsEnum)
			throw new ArgumentException($"{type} is not a enum type", nameof(type));

		return _cache.GetOrAdd(type, k => new EnumHelper(k));
	}

	public static EnumValue Get<TEnum>(TEnum value) 
		=> Get<TEnum>()[value];

	public static EnumValue Get(object value) 
		=> Get(value.GetType())[value];

	public static EnumValues GetValues<TEnum>(bool visible = true) 
		=> visible ? Get<TEnum>().Values.Visible: Get<TEnum>().Values;

	public static EnumValues GetValues(TypeEx type, bool visible = true) 
		=> visible ? Get(type).Values.Visible: Get(type).Values;

	public static EnumValues GetValues(object value, bool visible = true, bool @default = false)
	{
		TypeEx type = value.GetType();
		var intVal = GetIntValue(value);
		var helper = Get(type);

		IEnumerable<EnumValue> ie;

		if (!helper.IsFlags)
			ie = new[] {helper[value]};
		else if (Equals(intVal, 0))
			ie = helper.Default.With(d => new[] {d}, Array.Empty<EnumValue>());
		else if (@default)
			ie = helper.Values.Where(v => Equals(v.IntValue, NumericHelper.And(intVal, v.IntValue)));
		else
			ie = helper.Values.NonDefault.Where(v => Equals(v.IntValue, NumericHelper.And(intVal, v.IntValue)));

		if (visible)
			ie = ie.Where(v => v.Visible);

		return new EnumValues(helper, ie);
	}

	public static TEnum GetMergedValue<TEnum>(bool visible = true)
		=> (TEnum)GetValues<TEnum>(visible).MergedValue;

	public static T GetSetting<T>(object value, string key, T @default = default) 
		=> Get(TypeEx.Of(value))[value].With(d => d.GetSetting(key, @default), @default);

	public static string ToString(object value, string separator = ", ", string empty = "", bool onlyVisible = false)
	{
		if (value == null)
			return empty;

		var desc = Get(value.GetType());
		if (desc.IsFlags)
		{
			var str = new EnumBuilder(separator, empty: empty);

			var en = (Enum) value;
			foreach (var val in desc.Values)
			{
				var en2 = (Enum) val.Value;
				if (!val.IsDefault && (!onlyVisible || val.Visible) && en2 != null && en.HasFlag(en2))
					str.Append(val.DisplayName);
			}

			if (str.IsEmpty && empty == null)
				desc.Default.Do(v => str.Append(v.DisplayName));

			return str;
		}

		return desc[value].With(v => v.DisplayName, value.ToString());
	}

	public static object GetIntValue(object val)
	{
		var type = val.GetType();
		if (!type.GetTypeInfo().IsEnum)
			throw new ArgumentException($"{type} is not a enum type", nameof(val));

		return TypeEx.Get(Enum.GetUnderlyingType(val.GetType())).TypeCode switch
		{
			TypeCode.SByte => (sbyte)val,
			TypeCode.Byte => (byte)val,
			TypeCode.Int16 => (short)val,
			TypeCode.UInt16 => (ushort)val,
			TypeCode.Int32 => (int)val,
			TypeCode.UInt32 => (uint)val,
			TypeCode.Int64 => (long)val,
			TypeCode.UInt64 => (ulong)val,
			_ => throw new ArgumentOutOfRangeException()
		};
	}
	#endregion
}