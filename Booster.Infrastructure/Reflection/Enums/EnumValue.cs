using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using Booster.Evaluation;
using Booster.Helpers;
using Booster.Interfaces;
using JetBrains.Annotations;

namespace Booster.Reflection;

[PublicAPI]
public class EnumValue
{
	public TypeEx EnumType { get; }
	public object Value { get; }

	string _displayName;
	public string DisplayName
	{
		get
		{
			if (_displayName == null)
			{
				var res = Settings.Get<string>(Value, "DisplayName", out var isConstant);

				if (res == null)
					foreach (var attr in FindAttributes<DisplayNameAttribute>())
						res = attr.DisplayName;

				res ??= Name;

				if (isConstant)
					_displayName = res;

				return res;
			}
			return _displayName;
		}
	}

	string _name;
	public string Name => _name ??= Value.ToString();

	string _description;
	public string Description
	{
		get
		{
			if (_description == null)
			{
				var res = Settings.Get<string>(Value, "Description", out var isConstant);

				if (res == null)
					foreach (var attr in FindAttributes<DescriptionAttribute>())
						res = attr.Description;
					
				res ??= Name;

				if (isConstant)
					_description = res;

				return res;
			}
			return _description;
		}
	}

	bool? _disabled;
	public bool Disabled
	{
		get
		{
			if (!_disabled.HasValue)
			{
				var res = Settings.Get<bool>(Value, "Disabled", out var isConstant);
					
				if (isConstant)
					_disabled = res;

				return res;
			}
			return _disabled.Value;
		}
	}

	bool? _selected;
	public bool Selected
	{
		get
		{
			if (!_selected.HasValue)
			{
				var res = Settings.Get<bool>(Value, "Selected", out var isConstant);
					
				if (isConstant)
					_selected = res;

				return res;
			}
			return _selected.Value;
		}
	}

	bool? _visible;
	public bool Visible
	{
		get
		{
			if (!_visible.HasValue)
			{
				var res = Settings.ContainsKey("Visible") ?
					Settings.Get(Value, "Visible", out var isConstant, true) :
					Settings.Get(Value, "visibility", out isConstant, true);

				if (isConstant)
					_visible = res;

				return res;
			}
			return _visible.Value;
		}
	}

	object _order;
	public object Order
	{
		get
		{
			if (_order == null)
			{
				var res = Settings.Get<object>(Value, "Order", out var isConstant) ?? IntValue;

				if (isConstant)
					_order = res;

				return res;
			}
			return _order;
		}
	}

	object _intValue;
	public object IntValue => _intValue ??= EnumHelper.GetIntValue(Value);

	Field _field;
	public Field Field => _field ??= EnumType.FindField(Name);

	bool? _isDefault;
	public bool IsDefault => _isDefault ??= NumericHelper.IsZero(IntValue);

	bool? _isPowerOfTwo;
	public bool IsPowerOfTwo => _isPowerOfTwo ??= NumericHelper.IsPowerOfTwo(IntValue);

	EnumHelper _enumHelper;
	protected EnumHelper EnumHelper => _enumHelper ??= EnumHelper.Get(EnumType);

	DynamicSettings _settings;
	public DynamicSettings Settings
	{
		get
		{
			if (_settings == null)
			{
				var res = new DynamicSettings(EnumType);

				foreach (var attr in EnumType.FindAttributes<ISchemaAttribute>())
					attr.ApplySettings(res);

				foreach (var attr in FindAttributes<ISchemaAttribute>())
					attr.ApplySettings(res);

				_settings = res;
			}
			return _settings;
		}
	}

	public object GetSetting(string key)
		=> Settings[Value, key];

	public T GetSetting<T>(string key, T @default = default)
		=> Settings.Get(Value, key, @default);

	public T GetSetting<T>(string key, out bool isConstant, T @default = default)
		=> Settings.Get(Value, key, out isConstant, @default);

	/// <summary>
	/// Получает все аттрибуты значения
	/// </summary>
	/// <param name="attributeType">Тип атрибута (можно указать базовый, чтобы получить всех наследников).</param>
	/// <returns>Перечисление атрибутов удовлетворяющих условиям</returns>
	public IEnumerable<object> FindAttributes(TypeEx attributeType)
		=> Field.FindAttributes(attributeType, false);

	/// <summary>
	/// Получает все аттрибуты заданного типа. Может быть указан базовый тип атрибута.
	/// </summary>
	/// <typeparam name="TAttribute">Тип атрибута (можно указать базовый, чтобы получить всех наследников).</typeparam>
	/// <returns>Перечисление атрибутов удовлетворяющих условиям</returns>
	public IEnumerable<TAttribute> FindAttributes<TAttribute>()
		=> FindAttributes(typeof (TAttribute)).Cast<TAttribute>();

	/// <summary>
	/// Находит аттрибут заданного типа. Может быть указан базовый тип атрибута.
	/// </summary>
	/// <typeparam name="TAttribute">Тип атрибута (можно указать базовый, чтобы получить всех наследников).</typeparam>
	/// <returns>Первый атрибут, удовлетворяющий условиям</returns>
	public TAttribute FindAttribute<TAttribute>()
		=> FindAttributes(typeof (TAttribute)).Cast<TAttribute>().FirstOrDefault();

	public bool HasAttribute<TAttribute>()
		=> FindAttributes(typeof (TAttribute)).Any();

	internal EnumValue(object enumValue)
	{
		Value = enumValue;
		EnumType = enumValue.GetType();
	}
}