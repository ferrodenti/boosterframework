﻿using System;
using Booster.Interfaces;

namespace Booster.Reflection;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Enum)]
public class EnumAttribute : Attribute, ISchemaAttribute
{
	public string DisplayName { get; set; }
	public object Visible { get; set;}
	public object Disabled { get; set; }
	public object Selected { get; set; }
	public object Order { get; set; }

	public EnumAttribute()
	{
	}

	public EnumAttribute(string displayName, object visible = null, object disabled = null, object selected = null)
	{
		DisplayName = displayName;
		Visible = visible;
		Disabled = disabled;
		Selected = selected;
	}
		
	public void ApplySettings(IDynamicSettings settings)
	{
		if (DisplayName.IsSome()) settings.Add("DisplayName", DisplayName);
		if (Visible != null) settings.Add("Visible", Visible);
		if (Disabled != null) settings.Add("Disabled", Disabled);
		if (Selected != null) settings.Add("Selected", Selected);
		if (Order != null) settings.Add("Order", Order);
	}
}