using System.Collections.Generic;
using System.Threading;

#nullable enable

namespace Booster.Reflection;

static class ReflectionRepository
{
	static readonly Dictionary<long, object> _dictionary = new();
	static readonly object _lock = new();
	static long _newId;

	public static long GetNewId(object obj)
	{
		lock (_lock)
		{
			var res = Interlocked.Increment(ref _newId);
			_dictionary[res] = obj;
			return res;
		}
	}

	public static T? GetById<T>(long id) where T : class 
	{
		lock (_lock)
			return _dictionary.SafeGet(id) as T;
	}
}