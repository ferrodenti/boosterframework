using System.Reflection;

namespace Booster.Reflection;

interface IInvokeWithParameters
{
	ParameterInfo[] Parameters { get; }
}