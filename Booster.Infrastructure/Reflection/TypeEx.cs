using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using Booster.Helpers;
using JetBrains.Annotations;
using static Booster.FastLazyStatic;

#nullable enable

namespace Booster.Reflection;


/// <summary>
/// Описатель типов .net
/// Обертка над классами System.Type и System.Reflection.TypeInfo осуществляющая более удобную рефлексию.
/// Все экземпляры создаются приведеним от типа System.Type, либо вызовом TypeEx.Get(...) и TypeEx.Of(object).
/// </summary>
[PublicAPI]
public sealed class TypeEx
{
	public override bool Equals(object? obj)
		=> obj is TypeEx b && Inner == b.Inner;

	public bool Equals(TypeEx? other)
		=> Inner == other?.Inner;
	
	public override int GetHashCode()
		=> Inner.GetHashCode();

	/// <summary>
	/// Уникальный идентификатор объекта рефлексии. Назначается при создании нового экземпляра описателя типа.
	/// </summary>
	public long Id { get; }

	/// <summary>
	/// Внутренний описатель System.Type
	/// </summary>
	public Type Inner { get; }

	/// <summary>
	/// Внутренний описатель System.Reflection.TypeInfo
	/// </summary>
	public TypeInfo TypeInfo => _typeInfo ??= Inner.GetTypeInfo();
	FastLazy<TypeInfo>? _typeInfo;

	public Module Module => _module ??= TypeInfo.Module;
	FastLazy<Module>? _module;

	/// <summary>
	/// Сборка, в которой определен данный тип
	/// </summary>
	public Assembly Assembly => _assembly ??= TypeInfo.Assembly;
	FastLazy<Assembly>? _assembly;

	/// <summary>
	/// Базовый тип
	/// </summary>
	public TypeEx? BaseType => _baseType ??= TypeInfo.BaseType.With(Get);
	FastLazy<TypeEx?>? _baseType;

	public string Name => _name ??= TypeInfo.Name;
	FastLazy<string>? _name;

	public string FullName => _fullName ??= TypeInfo.FullName;
	FastLazy<string>? _fullName;

	public string Namespace => _namespace ??= TypeInfo.Namespace;
	FastLazy<string>? _namespace;

	/// <summary>
	/// Имя пригодное для кодогенерации
	/// </summary>
	public string CompliantName => _compliantName ??= FastLazy(() =>
	{
		if (IsNullable)
			return $"{GenericArguments[0].CompliantName}?";
		
		if (IsArray)
			return $"{ArrayItemType!.CompliantName}[]";

		var name = IsNested 
			? ".".JoinNonEmpty(DeclaringType?.CompliantName, Name) 
			: FullName;
		
		if (IsGenericTypeDefinition || IsGenericType)
		{
			var cut = name.IndexOf('`');
			var res = cut > 0 ? name.Substring(0, cut) : name;
			var arguments = GenericArguments.ToString(tp => tp.IsGenericParameter ? tp.Name : tp.CompliantName, new EnumBuilder(", ", "<", ">"));
			return res + arguments;
		}
		return name;
	});
	FastLazy<string>? _compliantName;

	/// <summary>
	///  Модификатор доступа типа
	/// </summary>
	public AccessModifier Access
	{
		get
		{
			return _access ??= FastLazy(() =>
			{
				if (IsProtected)
					return AccessModifier.Protected;
				//if(IsProtectedOrInternal)
				//	return AccessModifier.ProtectedOrInternal;
				//if(IsProtectedAndInternal)
				//	return AccessModifier.ProtectedAndInternal;
				if (IsPublic)
					return AccessModifier.Public;
				
				return AccessModifier.Private;

				//if (IsInternal)
				//	_access |= AccessModifier.Internal;

				//throw new ReflectionException($"Cannot resolve access modifier of {this}");
			});
			
		}
	}
	FastLazy<AccessModifier>? _access;

	public bool IsPublic => _isPublic ??= TypeInfo.IsPublic;
	FastLazy<bool>? _isPublic;
	
	public bool IsProtected => false; //TODO: получить модификаторы доступа

	//public bool IsInternal => _isInternal ?? (_isInternal = Inner.IsAs()).Value;
	//bool? _isInternal;
	//public bool IsPrivate  => _isPrivate ?? (_isPrivate = GetIsPrivate()).Value;
	//bool? _isPrivate;
	//public bool IsProtectedAndInternal => _isProtectedAndInternal ?? (_isProtectedAndInternal = GetIsProtectedAndInternal()).Value;
	//bool? _isProtectedAndInternal;
	//public bool IsProtectedOrInternal => _isProtectedOrInternal ?? (_isProtectedOrInternal = GetIsProtectedOrInternal()).Value;
	//bool? _isProtectedOrInternal;


	public bool IsNested => _isNested ??= TypeInfo.IsNested;
	FastLazy<bool>? _isNested;

	public bool IsGenericType => _isGenericType ??= TypeInfo.IsGenericType;
	FastLazy<bool>? _isGenericType;

	public bool IsGenericTypeDefinition => _isGenericTypeDefinition ??= TypeInfo.IsGenericTypeDefinition;
	FastLazy<bool>? _isGenericTypeDefinition;

	public bool IsGenericParameter => _isGenericParameter ??= TypeInfo.IsGenericParameter;
	FastLazy<bool>? _isGenericParameter;

	public bool IsInterface => _isInterface ??= TypeInfo.IsInterface;
	FastLazy<bool>? _isInterface;

	public bool IsArray => _isArray ??= TypeInfo.IsArray;
	FastLazy<bool>? _isArray;

	public TypeEx? ArrayItemType => _arrayItemType ??= TypeInfo.GetElementType().With(Get);
	FastLazy<TypeEx?>? _arrayItemType;

	public bool IsAbstract => _isAbstract ??= TypeInfo.IsAbstract;
	FastLazy<bool>? _isAbstract;

	public bool IsSealed => _isSealed ??= TypeInfo.IsSealed;
	FastLazy<bool>? _isSealed;

	public bool IsStatic => _isStatic ??= IsAbstract && IsSealed;
	FastLazy<bool>? _isStatic;

	public bool IsVisible => _isVisible ??= TypeInfo.IsVisible;
	FastLazy<bool>? _isVisible;

	public bool IsByRef => _isByRef ??= TypeInfo.IsByRef;
	FastLazy<bool>? _isByRef;

	public bool IsNullable => _isNullable ??= (GenericTypeDefinition?.Inner == typeof(Nullable<>));
	FastLazy<bool>? _isNullable;

	public string? BuildInName => _buildInName ??= FastLazy(() =>
	{
		if (Inner == typeof(object))
			return "object";

		if (!IsEnum)
			switch (TypeCode)
			{
			case TypeCode.Boolean: return "bool";
			case TypeCode.Char: return "char";
			case TypeCode.SByte: return"sbyte";
			case TypeCode.Byte: return "byte";
			case TypeCode.Int16: return "short";
			case TypeCode.UInt16: return "ushort";
			case TypeCode.Int32: return "int";
			case TypeCode.UInt32: return "uint";
			case TypeCode.Int64: return "long";
			case TypeCode.UInt64: return "ulong";
			case TypeCode.Single: return "float";
			case TypeCode.Double: return "double";
			case TypeCode.Decimal: return "decimal";
			case TypeCode.String: return "string";
			}

		return null;
	});

	FastLazy<string?>? _buildInName;
	
	/// <summary>
	/// Возвращает обобщенный тип без аргументов, являющийся базовым по отношению к текущему
	/// </summary>
	public TypeEx? GenericTypeDefinition => _genericTypeDefinition ??= (IsGenericType ? (TypeEx)TypeInfo.GetGenericTypeDefinition() : null);
	FastLazy<TypeEx?>? _genericTypeDefinition;
	

	/// <summary>
	/// Возвращает код встроенного типа
	/// </summary>
	public TypeCode TypeCode => _typeCode ??= Type.GetTypeCode(Inner);
	FastLazy<TypeCode>? _typeCode;

	/// <summary>
	/// Является ли тип типом-значением
	/// </summary>
	public bool IsValueType => _isValueType ??= TypeInfo.IsValueType;
	FastLazy<bool>? _isValueType;

	/// <summary>
	/// Является ли тип типом-значением
	/// </summary>
	public bool IsEnum => _isEnum ??= TypeInfo.IsEnum;
	FastLazy<bool>? _isEnum;

	/// <summary>
	/// Является ли тип числовым. Nullable&lt;&gt; числовых типов так же считается числовым
	/// </summary>
	public bool IsNumeric => _isNumeric ??= NumericHelper.IsNumeric(this);
	FastLazy<bool>? _isNumeric;

	/// <summary>
	/// Является ли тип целочисленным. Nullable&lt;&gt; целочисленных типов так же считается целочисленным
	/// </summary>
	public bool IsInteger => _isInteger ??= NumericHelper.IsInteger(this);
	FastLazy<bool>? _isInteger;

	/// <summary>
	/// Является ли тип вещественным числом. Nullable&lt;&gt; вещественных типов так же считается вещественным
	/// </summary>
	public bool IsReal => _isReal ??= NumericHelper.IsReal(this);
	FastLazy<bool>? _isReal;
	
	public object? MaxValue => (_maxValue ??= FastLazy(() =>
	{
		if (IsValueType && TryFindField(new ReflectionFilter("MaxValue")
			{
				IsStatic = true,
				Access = AccessModifier.Public
			}, out var fld))
			return fld.GetValue(null);

		return DefaultValue;
	})).Value; // Для FastLazy<object?> обязательное использование .Value, чтобы не получить FastLazy<object?> на выходе

	FastLazy<object?>? _maxValue;

	public object? MinValue => (_minValue ??= FastLazy(() =>
	{
		if (IsValueType && TryFindField(new ReflectionFilter("MinValue")
			{
				IsStatic = true,
				Access = AccessModifier.Public
			}, out var fld))
			return fld.GetValue(null);
		
		return DefaultValue;
	})).Value; // Для FastLazy<object?> обязательное использование .Value, чтобы не получить FastLazy<object?> на выходе

	FastLazy<object?>? _minValue;
	/// <summary>
	/// Тип, в котором был объявлен этот тип. 
	/// </summary>
	public TypeEx? DeclaringType => _declaringType ??= TypeInfo.DeclaringType.With(Get);
	FastLazy<TypeEx?>? _declaringType;


	public TypeEx? ElementType => _elementType ??= TypeInfo.GetElementType().With(Get);
	FastLazy<TypeEx?>? _elementType;

	/// <summary>
	/// Все базовые классы от наследников к предкам (к System.Object)
	/// </summary>
	public TypeEx[] BaseTypes => _baseTypes ??= FastLazy(() =>
	{
		var res = new List<TypeEx>();
		var type = BaseType;
				
		while (type is not null)
		{
			res.Add(type);
			type = type.BaseType;
		}

		return res.ToArray();
	});

	FastLazy<TypeEx[]>? _baseTypes;

	/// <summary>
	/// Значение заданного типа по умолчанию. null - для ссылочных типов
	/// </summary>
	// ReSharper disable once ReturnTypeCanBeNotNullable
	public object? DefaultValue => (_defaultValue ??= FastLazy(() =>
	{
		if (IsValueType)
			return Try.OrDefault(() => Create());

		return null;
	})).Value; //Для FastLazy<object?> Обязательно .Value
	FastLazy<object?>? _defaultValue; // Для FastLazy<object?> обязательное использование .Value, чтобы не получить FastLazy<object?> на выходе
	
	/// <summary>
	/// Все интерфейсы, которые реализует тип включая те, которые реализованы базовыми классами
	/// </summary>
	public TypeEx[] Interfaces => _interfaces ??= TypeInfo.GetInterfaces().Select(Get).ToArray();
	FastLazy<TypeEx[]>? _interfaces;
	
	/// <summary>
	/// Все поля типа включая унаследованные и защищенные
	/// </summary>
	public Field[] Fields => _fields ??= FastLazy(() =>
	{
		var res = new HashSet<Field>();

		foreach (Field fld in TypeInfo.DeclaredFields)
			res.Add(fld);

		if (BaseType is not null)
			foreach (var fld in BaseType.Fields)
				if (!res.Contains(fld))
					res.Add(fld);

		return res.ToArray();
	});

	FastLazy<Field[]>? _fields;

	/// <summary>
	/// Все свойства типа включая унаследованные и защищенные
	/// </summary>
	public Property[] Properties => _properties ??= FastLazy(() =>
	{
		var res = new List<Property>();
		var skip = new HashSet<Property>();

		foreach (Property prop in TypeInfo.DeclaredProperties)
		{
			res.Add(prop);
			skip.Add(prop);
			skip.Add(prop.BaseDefinition);
		}

		if (BaseType is not null)
			res.AddRange(BaseType.Properties
				.Where(prop => !skip.Contains(prop) && (prop.BaseDefinition == prop || !skip.Contains(prop.BaseDefinition))));

		return res.ToArray();
	});

	FastLazy<Property[]>? _properties;

	/// <summary>
	/// Все конструкторы типа включая защищенные
	/// </summary>
	public Constructor[] Constructors => _constructors ??= FastLazy(() =>
	{
		var res = new List<Constructor>();

		foreach (Constructor ctor in TypeInfo.DeclaredConstructors)
			res.Add(ctor);

		return res.ToArray();
	});

	FastLazy<Constructor[]>? _constructors;

	/// <summary>
	/// Все методы типа включая унаследованные и защищенные
	/// </summary>
	public Method[] Methods => _methods ??= FastLazy(() =>
	{
		var res = new List<Method>();
		var skip = new HashSet<Method?>();

		foreach (Method mtd in TypeInfo.DeclaredMethods)
		{
			res.Add(mtd);
			skip.Add(mtd);
			skip.Add(mtd.BaseDefinition);
		}

		if (BaseType.IsSome())
			foreach (var mtd in BaseType.Methods)
				if (!skip.Contains(mtd) && (mtd.BaseDefinition == mtd || !skip.Contains(mtd.BaseDefinition)))
					res.Add(mtd);

		return res.ToArray();
	});

	FastLazy<Method[]>? _methods;

	/// <summary>
	/// Все события типа включая унаследованные и защищенные
	/// </summary>
	public Event[] Events => _events ??= FastLazy(() =>
	{
		var res = new List<Event>();
		var skip = new HashSet<Event>();

		foreach (Event ev in TypeInfo.DeclaredEvents)
		{
			res.Add(ev);
			skip.Add(ev);
			skip.Add(ev.BaseDefinition);
		}

		if (BaseType is not null)
			foreach (var ev in BaseType.Events)
				if (!skip.Contains(ev) && (ev.BaseDefinition == ev || !skip.Contains(ev.BaseDefinition)))
					res.Add(ev);

		return res.ToArray();
	});

	FastLazy<Event[]>? _events;

	/// <summary>
	/// Все поля и свойства типа включая унаследованные и защищенные
	/// </summary>
	public BaseDataMember[] DataMembers => _dataMembers ??= Fields.Cast<BaseDataMember>().Concat(Properties).ToArray();
	FastLazy<BaseDataMember[]>? _dataMembers;

	/// <summary>
	/// Все методы и конструкторы и свойства типа включая унаследованные и защищенные
	/// </summary>
	public BaseMethodMember[] CodeMembers => _codeMembers ??= Constructors.Cast<BaseMethodMember>().Concat(Methods).ToArray();
	FastLazy<BaseMethodMember[]>? _codeMembers;

	/// <summary>
	/// Все члены включая унаследованные и защищенные
	/// </summary>
	public BaseMember[] Members => _members ??= DataMembers.Cast<BaseMember>().Concat(CodeMembers).Concat(Events).ToArray();
	FastLazy<BaseMember[]>? _members;

	/// <summary>
	/// Все обобщенные параметры типа
	/// </summary>
	public TypeEx[] GenericArguments => _genericArguments ??= TypeInfo.GenericTypeArguments.Select(Get).ToArray();
	FastLazy<TypeEx[]>? _genericArguments;

	public Method[] ImplicidCasts => _implicidCasts ??= FindMethods(
		new ReflectionFilter("op_Implicit")
		{
			Access = AccessModifier.Public | AccessModifier.Internal | AccessModifier.Internal,
			IsStatic = true,
			ParametersCount = 1
		}).ToArray();
	
	FastLazy<Method[]>? _implicidCasts;

	public Method[] ExplicitCasts => _explicitCasts ??= FindMethods(new ReflectionFilter("op_Explicit")
	{
		Access = AccessModifier.Public | AccessModifier.Internal | AccessModifier.Internal,
		IsStatic = true,
		ParametersCount = 1
	}).ToArray();

	FastLazy<Method[]>? _explicitCasts;

	TypeEx(Type type)
	{
		Inner = type;
		Id = ReflectionRepository.GetNewId(this);
	}

	

	/// <summary>
	/// Описатель конструктора без параметров, если таковой имеется у типа
	/// </summary>
	public Constructor? ParameterlessConstructor => _parameterLessCtor ??= FindConstructor(new ReflectionFilter { ParametersCount = 0 });
	FastLazy<Constructor?>? _parameterLessCtor;

	void CheckTypeCast<T>()
	{
		var target = Get<T>();

		if (!IsInstanceOfType(target))
			throw new ArgumentOutOfRangeException(nameof(T), $"Invalid generic argument <{target}>: could not cast from <{this}>");
	}

	public T CreateAs<T>(params object?[] parameters)
	{
		CheckTypeCast<T>();
		return (T) Create(parameters);
	}

	public T CreateAs<T>(InvocationOptions invocationOptions, params object?[] parameters)
	{
		CheckTypeCast<T>();
		return (T) Create(invocationOptions, parameters);
	}

	public T CreateAs<T>(InvocationOptions invocationOptions, ReflectionFilter reflectionFilter, params object?[] parameters)
	{
		CheckTypeCast<T>();
		return (T) Create(invocationOptions, reflectionFilter, parameters);
	}

	public static T Create<T>(params object?[] parameters)
		=> (T) Get<T>().Create(parameters);

	public static T Create<T>(InvocationOptions invocationOptions, params object?[] parameters)
		=> (T) Get<T>().Create(invocationOptions, parameters);

	public static T Create<T>(InvocationOptions invocationOptions, ReflectionFilter reflectionFilter, params object?[] parameters)
		=> (T) Get<T>().Create(invocationOptions, reflectionFilter, parameters);

	public object Create(params object?[] parameters)
		=> parameters.Length == 0
			? Activator.CreateInstance(
				this, 
				BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance, 
				null, 
				Array.Empty<object>(), 
				null)
			: Create(new InvocationOptions(), parameters);

	public object Create(InvocationOptions invocationOptions, params object?[] parameters)
		=> Create(invocationOptions,
			new ReflectionFilter
			{
				ParametersEx = parameters.Select(p => p.With(Of, AnyRef)).ToArray()
			}, parameters);

	public object Create(InvocationOptions invocationOptions, ReflectionFilter reflectionFilter, params object?[] parameters)
	{
		Constructor? ctor;

		if (reflectionFilter.ParametersEx?.Length == 0)
		{
			if (parameters.Length == 0)
			{
				ctor = ParameterlessConstructor;
				if (ctor == null || 
					reflectionFilter.Access != AccessModifier.Any && 
					(reflectionFilter.Access & ctor.Access) == AccessModifier.Any)
					throw new ReflectionException($"Не найден подходящий конструктор типа {this} без параметров.");

				return Activator.CreateInstance(
					this, 
					BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance, 
					null, 
					Array.Empty<object>(), 
					null);
			}

			reflectionFilter.ParametersEx ??= parameters.Select(p => p.With(Of, AnyRef)).ToArray();
		}

		ctor = FindConstructor(reflectionFilter);
		if (ctor == null)
		{
			var paramStr = reflectionFilter.ParametersEx.ToString(new EnumBuilder(","));
			throw new ReflectionException($"Не найден подходящий конструктор типа {this} для вызова с аргументами {paramStr}.");
		}

		return ctor.Invoke(invocationOptions, parameters);
	}

	#region FindXXX methods

	#region Members
	
	public IEnumerable<BaseMember> FindMembers(ReflectionFilter filter)
	{
		var res = FindMembers2(filter);

		switch (filter.Order)
		{
		case ReflectionFilterOrder.None:
			return res;
		case ReflectionFilterOrder.ByNameAsc:
			return res.OrderBy(m => m.Name);
		case ReflectionFilterOrder.ByNameDesc:
			return res.OrderByDescending(m => m.Name);
		}

		var baseTypes = BaseTypes.AppendBefore(this).ToList();

		return filter.Order switch
		{
			ReflectionFilterOrder.AncestorsToDescendants => res.OrderByDescending(m => baseTypes.IndexOf(m.DeclaringType ?? AnyRef)),
			ReflectionFilterOrder.DescendantsToAncestors => res.OrderBy(m => baseTypes.IndexOf(m.DeclaringType ?? AnyRef)),
			ReflectionFilterOrder.DelaringAncestorsToDescendants => res.OrderByDescending(m => baseTypes.IndexOf(m.BaseDefinition?.DeclaringType ?? m.DeclaringType ?? AnyRef)),
			ReflectionFilterOrder.DelaringDescendantsToAncestors => res.OrderBy(m => baseTypes.IndexOf(m.BaseDefinition?.DeclaringType ?? m.DeclaringType ?? AnyRef)),
			_ => res
		};
	}

	IEnumerable<BaseMember> FindMembers2(ReflectionFilter filter)
	{
		var members = EnumerateMembers(filter.MemberType);

		foreach (var member in members)
			if (filter.Check(member))
				yield return member;
	}

	IEnumerable<BaseMember> EnumerateMembers(MemberTypes memberTypes)
	{
		if (memberTypes == 0)
			memberTypes = MemberTypes.All;

		if (memberTypes.HasFlag(MemberTypes.Field))
			foreach (var field in Fields)
				yield return field;

		if (memberTypes.HasFlag(MemberTypes.Property))
			foreach (var property in Properties)
				yield return property;

		if (memberTypes.HasFlag(MemberTypes.Constructor))
			foreach (var constructor in Constructors)
				yield return constructor;

		if (memberTypes.HasFlag(MemberTypes.Method))
			foreach (var method in Methods)
				yield return method;

		if (memberTypes.HasFlag(MemberTypes.Event))
			foreach (var @event in Events)
				yield return @event;
	}

	static readonly Dictionary<Type, MemberTypes> _memberTypesMap = new()
	{
		{ typeof(Method), MemberTypes.Method },
		{ typeof(Property), MemberTypes.Property },
		{ typeof(Field), MemberTypes.Field },
		{ typeof(BaseDataMember), MemberTypes.Field | MemberTypes.Property},
		{ typeof(Constructor), MemberTypes.Constructor },
		{ typeof(Event), MemberTypes.Event },
	};
		
	static class MemberStaticCache<TMember>
	{
		public static readonly MemberTypes Type = _memberTypesMap[typeof(TMember)];
	}
	
	public IEnumerable<TMember> FindMembers<TMember>(ReflectionFilter filter)
		where TMember : BaseMember
	{
		filter.MemberType = MemberStaticCache<TMember>.Type;
		return FindMembers(filter).Cast<TMember>();
	}
	
	public IEnumerable<TMember> FindMembers<TMember>(string name)
		where TMember : BaseMember
		=> FindMembers<TMember>(new ReflectionFilter(name));
	
	public TMember? FindMember<TMember>(ReflectionFilter filter)
		where TMember : BaseMember
		=> FindMembers<TMember>(filter).FirstOrDefault();
	
	public TMember? FindMember<TMember>(string name)
		where TMember : BaseMember
		=> FindMembers<TMember>(new ReflectionFilter(name)).FirstOrDefault();

	public bool TryFindMember<TMember>(ReflectionFilter filter, out TMember result)
		where TMember : BaseMember
		=> FindMember<TMember>(filter).Is(out result);
	
	public bool TryFindMember<TMember>(string name, out TMember result)
		where TMember : BaseMember
		=> FindMember<TMember>(name).Is(out result);
	
	public BaseMember? FindMember(ReflectionFilter filter)
		=> FindMembers(filter).FirstOrDefault();

	public BaseMember? FindMember(string name)
		=> FindMember(new ReflectionFilter(name));

	public bool TryFindMember(ReflectionFilter filter, out BaseMember member)
		=> FindMember(filter).Is(out member);

	public bool TryFindMember(string name, out BaseMember member)
		=> TryFindMember(new ReflectionFilter(name), out member);

	#endregion

	#region DataMember

	public IEnumerable<BaseDataMember> FindDataMembers(ReflectionFilter filter)
		=> FindMembers<BaseDataMember>(filter);

	public BaseDataMember? FindDataMember(ReflectionFilter filter)
		=> FindMember<BaseDataMember>(filter);
	
	public BaseDataMember? FindDataMember(string name)
		=> FindMember<BaseDataMember>(name);

	public BaseDataMember? FindDataMember(string name, TypeEx valueType)
		=> FindMember<BaseDataMember>(new ReflectionFilter(name) { ReturnType = valueType });

	public bool TryFindDataMember(ReflectionFilter filter, out BaseDataMember result)
		=> TryFindMember(filter, out result);

	public bool TryFindDataMember(string name, out BaseDataMember result)
		=> TryFindMember(name, out result);
	
	#endregion

	#region Property
	
	public IEnumerable<Property> FindProperties(ReflectionFilter filter)
		=> FindMembers<Property>(filter);
	
	public IEnumerable<Property> FindProperties(string name)
		=> FindMembers<Property>(name);

	public Property? FindProperty(ReflectionFilter filter)
		=> FindMember<Property>(filter);

	public Property? FindProperty(string name)
		=> FindMember<Property>(name);

	public Property? FindProperty(string name, TypeEx valueType)
		=> FindMember<Property>(new ReflectionFilter(name){ ReturnType = valueType });
	
	public bool TryFindProperty(ReflectionFilter filter, out Property result)
		=> TryFindMember(filter, out result);
	
	public bool TryFindProperty(string name, out Property result)
		=> TryFindMember(name, out result);
	
	#endregion
	
	#region Fields
	
	public IEnumerable<Field> FindFields(ReflectionFilter filter)
		=> FindMembers<Field>(filter);
	
	/// <summary>
	/// Поиск поля с использованием рефлекторного фильтра
	/// </summary>
	/// <param name="filter">Рефлекторный фильтр</param>
	/// <returns>Описатель поля Booster.Reflection.Field</returns>
	public Field? FindField(ReflectionFilter filter)
		=> FindMember<Field>(filter);

	/// <summary>
	/// Поиск поля по имени
	/// </summary>
	/// <param name="name">Имя поля</param>
	/// <returns>Описатель поля Booster.Reflection.Field</returns>
	public Field? FindField(string name)
		=> FindMember<Field>(name);
	
	/// <summary>
	/// Поиск поля по имени и типу значения поля
	/// </summary>
	/// <param name="name">Имя поля</param>
	/// <param name="valueType">Тип значения поля</param>
	/// <returns>Описатель поля Booster.Reflection.Field</returns>
	public Field? FindField(string name, TypeEx valueType)
		=> FindMember<Field>(new ReflectionFilter(MemberTypes.Field, name) { ReturnType = valueType });

	public bool TryFindField(ReflectionFilter filter, out Field result)
		=> TryFindMember(filter, out result);

	public bool TryFindField(string name, out Field result)
		=> TryFindMember(name, out result);
	
	#endregion

	#region Method

	public IEnumerable<Method> FindMethods(ReflectionFilter filter)
		=> FindMembers<Method>(filter);
	
	public IEnumerable<Method> FindMethods(string name)
		=> FindMembers<Method>(name);

	public Method? FindMethod(ReflectionFilter filter)
		=> FindMember<Method>(filter);
	
	public Method? FindMethod(string name, TypeEx[]? parameters, [InstantHandle] Action<ReflectionFilter> setup)
		=> FindMember<Method>(new ReflectionFilter(MemberTypes.Method, name, parameters).Setup(setup));

	public Method? FindMethod(string name, Type[]? parameters, [InstantHandle] Action<ReflectionFilter> setup)
		=> FindMember<Method>(new ReflectionFilter(MemberTypes.Method, name, parameters).Setup(setup));
	
	public Method? FindMethod(string name)
		=> FindMember<Method>(new ReflectionFilter(name));
	
	public Method? FindMethod(string name, Action<ReflectionFilter> setup)
		=> FindMethod(name, (TypeEx[]?)null, setup);
	
	public Method? FindMethod(string name, params TypeEx[] parameters)
		=> FindMember<Method>(new ReflectionFilter(MemberTypes.Method, name, parameters));

	public Method? FindMethod(string name, IEnumerable<Type> parameters)
		=> FindMember<Method>(new ReflectionFilter(MemberTypes.Method, name, parameters));
	
	public bool TryFindMethod(ReflectionFilter filter, out Method result)
		=> TryFindMember(filter, out result);

	public bool TryFindMethod(string name, out Method result)
		=> TryFindMember(name, out result);
	
	#endregion
	
	#region Constructor

	public IEnumerable<Constructor> FindConstructors(ReflectionFilter filter)
		=> FindMembers<Constructor>(filter);
	
	public Constructor? FindConstructor(ReflectionFilter filter)
		=> FindMember<Constructor>(filter);

	public Constructor? FindConstructor(AccessModifier access = AccessModifier.Any)
		=> FindMember<Constructor>(new ReflectionFilter
		{
			ParametersCount = 0,
			Access = access,
			IsStatic = false
		});

	public Constructor? FindConstructor(params TypeEx[] parameters)
		=> FindMember<Constructor>(new ReflectionFilter(MemberTypes.Constructor, null, parameters));

	public Constructor? FindConstructor(IEnumerable<Type> parameters)
		=> FindMember<Constructor>(new ReflectionFilter(MemberTypes.Constructor, null, parameters));

	public bool TryFindConstructor(ReflectionFilter filter, out Constructor result)
		=> TryFindMember(filter, out result);
	
	#endregion

	#region Event
	
	public IEnumerable<Event> FindEvents(ReflectionFilter filter)
		=> FindMembers<Event>(filter);
	
	public IEnumerable<Event> FindEvents(string name)
		=> FindMembers<Event>(name);
	
	public Event? FindEvent(ReflectionFilter filter)
		=> FindMember<Event>(filter);

	public Event? FindEvent(string name)
		=> FindMember<Event>(name);

	public bool TryFindEvent(ReflectionFilter filter, out Event result)
		=> TryFindMember(filter, out result);

	public bool TryFindEvent(string name, out Event result)
		=> TryFindMember(name, out result);
	
	#endregion

	#region FindAttributes
	
	/// <summary>
	/// Получает все аттрибуты заданного типа.
	/// </summary>
	/// <param name="attributeType">Тип атрибута (можно указать базовый, чтобы получить всех наследников).</param>
	/// <param name="inherit">Искать атрибуты базовых классов</param>
	/// <param name="baseTypesFirst">Вначале искать в базовых типах</param>
	/// <returns>Перечисление атрибутов удовлетворяющих условиям</returns>
	public IEnumerable<object> FindAttributes(TypeEx attributeType, bool inherit = true, bool baseTypesFirst = true)
	{
		if (inherit)
			foreach (var t in baseTypesFirst ? BaseTypes.Reverse().AppendAfter(this) : BaseTypes.AppendBefore(this))
			foreach (var attr in t.FindAttributes(attributeType, false))
				yield return attr;
		else
			foreach (var attr in TypeInfo.GetCustomAttributes(attributeType, false))
				yield return attr;
	}

	/// <summary>
	/// Получает все аттрибуты заданного типа. Может быть указан базовый тип атрибута.
	/// </summary>
	/// <typeparam name="TAttribute">Тип атрибута (можно указать базовый, чтобы получить всех наследников).</typeparam>
	/// <param name="inherit">Искать атрибуты базовых классов</param>
	/// <param name="baseTypesFirst">Вначале искать в базовых типах</param>
	/// <returns>Перечисление атрибутов удовлетворяющих условиям</returns>
	public IEnumerable<TAttribute> FindAttributes<TAttribute>(bool inherit = true, bool baseTypesFirst = true)
		=> FindAttributes(typeof(TAttribute), inherit, baseTypesFirst).Cast<TAttribute>();

	public object? FindAttribute(TypeEx attributeType, bool inherit = true, bool baseTypesFirst = false)
		=> FindAttributes(attributeType, inherit, baseTypesFirst).FirstOrDefault();
		
	/// <summary>
	/// Находит аттрибут заданного типа. Может быть указан базовый тип атрибута.
	/// </summary>
	/// <typeparam name="TAttribute">Тип атрибута (можно указать базовый, чтобы получить всех наследников).</typeparam>
	/// <param name="inherit">Искать атрибуты базовых классов</param>
	/// <param name="baseTypesFirst">Вначале искать в базовых типах</param>
	/// <returns>Первый атрибут, удовлетворяющий условиям</returns>
	public TAttribute? FindAttribute<TAttribute>(bool inherit = true, bool baseTypesFirst = false)
		=> FindAttributes<TAttribute>(inherit, baseTypesFirst).FirstOrDefault();
	
	/// <summary>
	/// Находит аттрибут заданного типа. Может быть указан базовый тип атрибута.
	/// </summary>
	/// <typeparam name="TAttribute">Тип атрибута (можно указать базовый, чтобы получить всех наследников).</typeparam>
	/// <param name="result">Возвращает первый атрибут, удовлетворяющий условиям</param>
	/// <param name="inherit">Искать атрибуты базовых классов</param>
	/// <param name="baseTypesFirst">Вначале искать в базовых типах</param>
	/// <returns>true, если атрибут был найден</returns>
	public bool TryFindAttribute<TAttribute>(out TAttribute result, bool inherit = true, bool baseTypesFirst = false)
		=> FindAttribute<TAttribute>(inherit, baseTypesFirst).Is(out result);
	
	public bool TryFindAttribute(TypeEx attributeType, out object result, bool inherit = true, bool baseTypesFirst = false)
		=> FindAttribute(attributeType, inherit, baseTypesFirst).Is(out result);

	public bool HasAttribute(TypeEx type, bool inherit = true)
		=> TryFindAttribute(type, out _, inherit);
	
	public bool HasAttribute<TAttribute>(bool inherit = true)
		=> TryFindAttribute<TAttribute>(out _, inherit);
	
	#endregion

	#region Interfaces
	
	public TypeEx[] FindGenericInterfaces(TypeEx genericTypeDefinition)
	{
		if (!genericTypeDefinition.IsGenericTypeDefinition)
			throw new ArgumentException("Expected generic type definition interface");

		return Interfaces
			.Where(i => i.IsGenericType &&
						Equals(i.GenericTypeDefinition, genericTypeDefinition))
			.ToArray();
	}

	public bool TryFindGenericInterface(TypeEx genericTypeDefinition, out TypeEx result)
		=> FindGenericInterfaces(genericTypeDefinition).FirstOrDefault().Is(out result);

	public TypeEx? FindInterface(TypeEx interfaceType)
		=> interfaceType.IsGenericTypeDefinition
			? FindGenericInterfaces(interfaceType).FirstOrDefault()
			: Interfaces.FirstOrDefault(t => t.IsAssignableFrom(interfaceType));
	//=> TypeInfo.GetInterface(type.Name).With(Get);

	public bool HasInterface(TypeEx interfaceType)
		=> FindInterface(interfaceType) is not null;
	
	public bool HasInterface<TInterface>()
		=> HasInterface(typeof(TInterface));
	
	public TypeEx? FindInterface<TInterface>()
		=> FindInterface(typeof(TInterface));

	public bool TryFindInterface(TypeEx type, out TypeEx result)
		=> FindInterface(type).Is(out result);
	
	public bool HasInterface(string interfaceName)
		=> TypeInfo.GetInterface(interfaceName) != null;
	
	#endregion
	
	#endregion
	
	public bool IsAssignableFrom(TypeEx type)
		=> TypeInfo.IsAssignableFrom(type);

	public bool IsInstanceOf<T>()
		=> IsInstanceOfType(Get<T>());

	public bool IsInstanceOfType(TypeEx type)
		=> Equals(type) || type.IsAssignableFrom(this);

	public bool IsInstanceOfType(object obj)
		=> TypeInfo.IsInstanceOfType(obj);

	public bool HasGenericTypeDefinition(TypeEx genericTypeDefinition) // Нет обобщенной перегрузки, потому что нельзя написать что-то типа HasGenericTypeDefinition<Type<>>()
	{
		if (IsInterface && IsGenericType)
		{
			if (Equals(GenericTypeDefinition, genericTypeDefinition))
				return true;

			if (BaseType is null || Equals(BaseType, Get<object>()))
				return false;

			return BaseType.HasGenericTypeDefinition(genericTypeDefinition);
		}

		if (IsInterface)
			return Interfaces.Any(i => Equals(i.GenericTypeDefinition, genericTypeDefinition));

		var type = this;
		while (type is not null && Equals(type, Get<object>()))
		{
			var cur = IsGenericType ? GenericTypeDefinition : type;
			if (cur is not null && genericTypeDefinition.Equals(cur))
				return true;

			type = type.BaseType;
		}

		return false;
	}

	public TypeEx MakeGenericType(params TypeEx[] typeArguments)
		=> MakeGenericType(typeArguments.Select(t => t.Inner).ToArray());

	public TypeEx MakeGenericType(params Type[] typeArguments)
		=> Inner.MakeGenericType(typeArguments);

	public TypeEx MakeArrayType()
		=> Inner.MakeArrayType();

	public static TypeEx MakeGenericType(Type definition, params Type[] arguments)
		=> Get(definition).MakeGenericType(arguments);

	public static TypeEx MakeGenericType(Type definition, params TypeEx[] arguments)
		=> Get(definition).MakeGenericType(arguments);

	public static TypeEx MakeGenericType(TypeEx definition, params TypeEx[] arguments)
		=> definition.MakeGenericType(arguments);

	public static TypeEx MakeGenericType(TypeEx definition, params Type[] arguments)
		=> definition.MakeGenericType(arguments);

	#region Implicit / Explicit casts

	static readonly ConcurrentDictionary<Tuple<TypeEx, TypeEx>, Method[]> _castMethods = new();

	public Method? FindCastMethod(TypeEx to, CastType castType = CastType.Any)
	{
		if (to.IsAssignableFrom(this))
			return null;

		var methods = _castMethods.GetOrAdd(Tuple.Create(this, to), _ =>
		{
			var res = new List<Method>(); //Вначале все неявные, потом явные

			foreach (var from in BaseTypes.AppendBefore(this))
			foreach (var mtd in from.ImplicidCasts.Concat(to.ImplicidCasts))
				if (to.IsAssignableFrom(mtd.ReturnType!) &&
					Equals(Get(mtd.Parameters[0].ParameterType), from))
					res.Add(mtd);

			foreach (var from in BaseTypes.AppendBefore(this))
			foreach (var mtd in from.ExplicitCasts.Concat(to.ExplicitCasts))
				if (to.IsAssignableFrom(mtd.ReturnType!) &&
					Equals(Get(mtd.Parameters[0].ParameterType), from))
					res.Add(mtd);

			return res.ToArray();
		});

		if (castType.HasFlag(CastType.Implicit))
		{
			var res = methods.FirstOrDefault(m => m.Name == "op_Implicit");
			if (res != null)
				return res;
		}

		if (castType.HasFlag(CastType.Explicit))
		{
			var res = methods.FirstOrDefault(m => m.Name == "op_Explicit");
			if (res != null)
				return res;
		}

		return null;
	}

	public bool CanCastTo(TypeEx to, CastType castType = CastType.Any)
	{
		while (true)
		{
			if (Equals(to, this))
				return true;

			if (castType.HasFlag(CastType.ToBase) && to.IsAssignableFrom(this))
				return true;

			if (Equals(this, AnyRef))
			{
				if (!to.IsValueType)
					return true;

				if (castType.HasFlag(CastType.Nulls2Defaults))
					return true;
			}

			if (castType.HasFlag(CastType.Nullables))
			{
				if (IsNullable)
					return GenericArguments[0].CanCastTo(to);

				if (to.IsNullable)
				{
					to = to.GenericArguments[0];
					castType = CastType.Any;
					continue;
				}
			}

			if ((castType & (CastType.Implicit | CastType.Explicit)) != 0 && FindCastMethod(to, castType) != null)
				return true;

			if (IsNumeric)
				return to.IsNumeric && castType.HasFlag(CastType.Numerics) || to.IsEnum && castType.HasFlag(CastType.Enums);

			return to.IsNumeric && IsEnum && castType.HasFlag(CastType.Enums);
		}
	}

	public CastType GetCastType(TypeEx to)
	{
		while (true)
		{
			if (Equals(to, this) || to.IsAssignableFrom(this))
				return CastType.Implicit;

			if (Equals(this, AnyRef))
				if (!to.IsValueType)
					return CastType.Explicit;

			if (IsNullable)
				return GenericArguments[0].CanCastTo(to) ? CastType.Explicit : CastType.Deny;

			if (to.IsNullable)
			{
				to = to.GenericArguments[0];
				continue;
			}

			var castMethod = FindCastMethod(to);
			if (castMethod != null)
				switch (castMethod.Name)
				{
				case "op_Implicit":
					return CastType.Implicit;
				case "op_Explicit":
					return CastType.Explicit;
				}

			if (IsNumeric && (to.IsNumeric || to.IsEnum))
				return CastType.Explicit;

			if (to.IsNumeric && IsEnum)
				return CastType.Explicit;

			return CastType.Deny;
		}
	}


	public bool TryCast(object? value, TypeEx to, out object result, CastType castType = CastType.Any)
	{
		result = null!;

		if (to.IsByRef)
			to = to.ElementType!;

		if (to.IsAssignableFrom(this))
		{
			result = value!;
			return true;
		}

		if ((IsNumeric || IsEnum) && (to.IsNumeric || to.IsEnum))
		{
			if (castType.HasFlag(CastType.Nullables))
			{
				if (IsNullable && value == null)
					value = GenericArguments[0].DefaultValue;

				if (to.IsNullable)
					to = to.GenericArguments[0];
			}

			if ((IsEnum || to.IsEnum) && !castType.HasFlag(CastType.Enums) || !castType.HasFlag(CastType.Numerics))
				return false;

			try
			{
				result = Convert.ChangeType(value, to);
				return true;
			}
			catch (Exception e)
			{
				Utils.Nop(e);
			}
			
		}

		var mi = FindCastMethod(to, castType);
		if (mi != null)
		{
			result = mi.Invoke(null, value)!;
			return true;
		}

		if (TypeCode == TypeCode.String && castType.HasFlag(CastType.StringParsing))
			return StringConverter.Default.TryParse(value?.ToString(), to, out result!);

		if (to.TypeCode == TypeCode.String && castType.HasFlag(CastType.Stringify))
		{
			result = StringConverter.Default.ToString(value)!;
			return true;
		}

		return false;
	}

	#endregion

	/// <summary>
	/// Название типа, пригодное для кодогенерации
	/// </summary>
	public override string ToString()
		=> CompliantName;

	static readonly ConcurrentDictionary<Type, TypeEx> _cache = new();

	static TypeEx Cached(Type type)
		=> _cache.GetOrAdd(type, _ => new TypeEx(type));

	public static TypeEx Get<T>()
		=> Cached(typeof(T));

	public static TypeEx? Get(string typeName, bool throwOnError = false, bool ignoreCase = false)
	{
		var type = Type.GetType(typeName, throwOnError, ignoreCase);
		if (type == null)
			return null;
		
		return Cached(type);
	}

	public static TypeEx Get(Type type)
		=> Cached(type);

	public static TypeEx Of(object obj)
		=> obj.GetType();

	static FastLazy<TypeEx>? _void;
	public static TypeEx Void => _void ??= Get(typeof(void));

	// ReSharper disable once ClassNeverInstantiated.Local
	class AnyRefInt
	{
	}

	public static TypeEx AnyRef => _anyRef.Value;
	static readonly Lazy<TypeEx> _anyRef = new(Get<AnyRefInt>);

	/// <summary>
	/// Получает объект рефлексии по заданному идентификатору. Объект должен быть предварительно инициализирован.
	/// Метод преднозначен для кодогенерации, когда генератор заранее находит объект и может передать в сгенерированный код просто ссылку.
	/// </summary>
	/// <param name="id">Уникальный идентификатор объекта рефлексии.</param>
	/// <returns>Ранее инициализированный объект с этим идентификатором либо null, если объект отсутствует, либо тип объекта неправильный</returns>
	public static TypeEx? GetById(long id)
		=> ReflectionRepository.GetById<TypeEx>(id);

	/// <summary>
	/// Неявная перегрузка оператора преобразования System.Type -> Booster.Reflection.Type
	/// </summary>
	/// <param name="type"></param>
	/// <returns></returns>
	public static implicit operator TypeEx(Type type)
		=> type.With(Cached)!;

	/// <summary>
	/// Неявная перегрузка оператора преобразования Booster.Reflection.Type -> System.Type
	/// </summary>
	/// <param name="type"></param>
	/// <returns></returns>
	public static implicit operator Type(TypeEx type)
		=> type.With(t => t.Inner)!;
}

[PublicAPI]
public static class TypeExExpander
{
	public static TypeEx? Ex(this Type? type)
	{
		if (type == null)
			return null;

		return TypeEx.Get(type);
	}

	public static bool Is<T>(this TypeEx? a)
	{
		if (a == null)
			return false;

		return a.Inner == typeof(T);
	}

	public static bool Is(this TypeEx? a, Type? b)
	{
		if (a == null)
			return b == null;

		return a.Inner == b;
	}

	public static bool IsSome([NotNullWhen(true)]this TypeEx? v)
		=> v != null && !ReferenceEquals(v, TypeEx.AnyRef);

	public static bool IsEmpty([NotNullWhen(false)]this TypeEx? v)
		=> v == null || ReferenceEquals(v, TypeEx.AnyRef);
}