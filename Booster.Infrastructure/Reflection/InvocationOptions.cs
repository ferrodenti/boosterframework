namespace Booster.Reflection;

/// <summary>
/// Настройки вызова метода. Определяет правила преобразования набора аргументов к параметрам метода
/// </summary>
public class InvocationOptions
{
	/// <summary>
	/// Определяет, преобразовывать ли заданные аргументы так, чтобы они удовлетворяли сигнатуре метода.
	/// </summary>
	/// <remarks>
	/// Если значение установлено в false, аргументы передаются в инвокатор без обработки, при этом
	/// для методов не являющихся статичными либо конструкторами значение target игнорируется 
	/// и читается из первого аргумента.
	/// </remarks>
	public bool DoPrepare { get; set; }

	/// <summary>
	/// Определяет допустимые приведения типов аргументов к параметрам заданным в сигнатуре метода
	/// </summary>
	/// <remarks>Значение по умолчанию: <seealso cref="F:Booster.Reflection.CastType.Any"/> – допустимы любые возможные приведения</remarks>
	public CastType CastType { get; set; } = CastType.Any;

	/// <summary>
	/// Определяет, следует ли заполнять необязательные параметры для которых заданны значения по умолчанию.
	/// </summary>
	/// <remarks>
	/// Если установлено в true, то для необязательных параметров (а так же для параметров с ключевым словом param),
	/// для которых отсутсвует соответствующий аргумент будет создан аргумент со значением по умолчанию
	/// </remarks>
	/// <remarks>Значение по умолчанию: <code>true</code></remarks>
	public bool FillOptionals { get; set; } = true;

	/// <summary>
	/// Определяет, следует ли заполнять параметры с ключевым словом param массивом собраным из последующих аргументов, либо пустым массивом.
	/// </summary>
	/// <remarks>Значение по умолчанию: <code>true</code></remarks>
	public bool ComposeParamsArray { get; set; } = true;

	/// <summary>
	/// Определяет, следует ли заполнять значениями типа по умолчанию аргументы тех параметров, для которых преведение типов не удалось.
	/// </summary>
	/// <remarks>Значение по умолчанию: <code>false</code></remarks>
	public bool DefaultsOnCastFail { get; set; } = false;

	/// <summary>
	/// Определяет, следует ли заполнять значениями типа по умолчанию аргументы тех параметров, для аргумент отсутсвует.
	/// </summary>
	/// <remarks>Значение по умолчанию: <code>false</code></remarks>
	public bool DefaultsOnMissing { get; set; } = false;

	/// <summary>
	/// Создает новый экземпляр настроек вызова
	/// </summary>
	/// <param name="doPrepare">Осуществлять ли преобразование списка аргументов</param>
	public InvocationOptions(bool doPrepare = true)
        => DoPrepare = doPrepare;
}