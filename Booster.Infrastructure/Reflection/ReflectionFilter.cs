using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using Booster.Parsing;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Reflection;

/// <summary>
/// Фильтр для использования в методах поиска членов класса Booster.Reflection.TypeEx
/// </summary>
[PublicAPI]
public class ReflectionFilter //TODO: MemberFilter?
{
	/// <summary>
	/// Фильтрация имен (точное соответствие). Если null (по умолчанию), то фильтрация по точному соответствию имени не осуществляется 
	/// </summary>
	public string? Name { get; set; }
	/// <summary>
	/// Фильтрация имен (регулярное выражение). Если null (по умолчанию), то фильтрация по имени регулярным выражением не осуществляется 
	/// </summary>
	public string? NameRegex { get; set; }

	protected Regex? NameRegexExpression => _nameRegex ??= NameRegex.With(v => Utils.CompileRegex(v, ignoreCase: IgnoreCase));
	FastLazy<Regex?>? _nameRegex;
	
	/// <summary>
	/// Фильтрация имен (шаблон wildcard). Если null (по умолчанию), то фильтрация по имени шаблоном не осуществляется 
	/// </summary>
	public string? NameWildcards { get; set; }
		
	/// <summary>
	/// Игнорировать регистр при сравнении имен. По умолчанию false.
	/// </summary>
	public bool IgnoreCase { get; set; }

	/// <summary>
	///  Модификатор доступа члена. Может быть указанно несколько значений, в этом случае будут выбраны члены с любым модификатором из указанных; Значение по умолчанию <see cref="F:Booster.Reflection.AccessModifier.Any"/> - то фильтрация по модификатору доступа не осуществляется
	/// </summary>
	public AccessModifier Access { get; set; }
		
	/// <summary>
	/// Если true - будут выбраны только виртуальные члены.
	/// Если false - только не виртуальные.
	/// Если null (по умолчанию) - любые члены независимо от виртуальности.
	/// </summary>
	public bool? IsVirtual { get; set; }

	/// <summary>
	/// Если true - будут выбраны только статичные члены.
	/// Если false - только не стачиные.
	/// Если null (по умолчанию) - любые члены независимо от статичности.
	/// </summary>
	public bool? IsStatic { get; set; }
	
	/// <summary>
	/// Если true - будут выбраны только абстрактные члены.
	/// Если false - только не абстрактные.
	/// Если null (по умолчанию) - любые члены независимо от абстрактности.
	/// </summary>
	public bool? IsAbstract { get; set; }

	/// <summary>
	/// Фильтрация типов членов. По умолчанию - искать любые члены
	/// </summary>
	public MemberTypes MemberType { get; set; }

	/// <summary>
	/// Фильтрация по типу возвращаемого значения метода, либо типу значения свойства или метода. По умолчанию null - не фильтровать по типу возвращаемого значения. Возможные приведения типов задаются с помощью <see cref="P:Booster.Reflection.ReflectionFilter.ReturnCastType"/>
	/// </summary>
	public TypeEx? ReturnType
	{
		get => ReturnTypes?.FirstOrDefault().With(TypeEx.Get);
		set => ReturnTypes = value.With(v => new[] {v.Inner});
	}

	public Type[]? ReturnTypes { get; set; }

	public TypeEx? DeclaringType { get; set; }
	
	/// <summary>
	/// Фильтрация по количеству параметров метода, либо индексов свойства. По умолчанию null - не фильтровать по количеству параметров.
	/// </summary>
	public int? ParametersCount { get; set; }
		
	/// <summary>
	/// Фильтрация по типам аргументов, так чтобы метод можно было вызвать с этими аргументами с параметрами вызова <see cref="P:Booster.Reflection.ReflectionFilter.InvocationOptions"/>
	/// </summary>
	public TypeEx[]? ParametersEx { get; set; } //TODO: ArgumentTypes

	public Type[]? Parameters
	{
		get => ParametersEx?.Select(p => p.Inner).ToArray();
		set => ParametersEx = value?.Select(TypeEx.Get).ToArray();
	}
		
	/// <summary>
	/// Если задан <see cref="P:Booster.Reflection.ReflectionFilter.ReturnType"/>, то определяет допустимые виды прведения типов. Значение по умолчанию <see cref="F:Booster.Reflection.CastType.Any"/> - допустимы любые возможные приведения типов.
	/// </summary>
	public CastType ReturnCastType { get; set; } = CastType.Any;

	/// <summary>
	/// Если заданы <see cref="P:Booster.Reflection.ReflectionFilter.ArgumentTypes"/>, то определяет допустимые виды прведения типов. Значение по умолчанию <see cref="F:Booster.Reflection.CastType.Any"/> - допустимы любые возможные приведения типов.
	/// </summary>
	public CastType CastType
	{
		get => InvocationOptions.CastType;
		set => InvocationOptions.CastType = value;
	}

	public InvocationOptions InvocationOptions
	{
		get => _invocationOptions ??= new InvocationOptions();
		set => _invocationOptions = value;
	}

	InvocationOptions? _invocationOptions;

	public List<TypeEx> RequiredAttributes { get; set; } = new(); 

	public Predicate<BaseMember>? Predicate { get; set; }

	public ReflectionFilterOrder Order { get; set; }

	public ReflectionFilter()
	{
	}

	public ReflectionFilter(string name)
		=> Name = name;

	public ReflectionFilter(string? name, params TypeEx[] parameters)
	{
		Name = name;
		ParametersEx = parameters;
	}
	public ReflectionFilter(string? name, params Type[] parameters)
	{
		Name = name;
		ParametersEx = parameters.Select(TypeEx.Get).ToArray();
	}
	public ReflectionFilter(MemberTypes memberType, string? name = null)
	{
		MemberType = memberType;
		Name = name;
	}

	public ReflectionFilter(MemberTypes memberType, string? name, params TypeEx[]? parameters)
	{
		MemberType = memberType;
		Name = name;
		ParametersEx = parameters;
	}
	public ReflectionFilter(MemberTypes memberType, string? name, IEnumerable<Type>? parameters)
	{
		MemberType = memberType;
		Name = name;
		ParametersEx = parameters?.Select(TypeEx.Get).ToArray();
	}

	public ReflectionFilter(ReflectionFilter proto)
	{
		Name = proto.Name;
		NameWildcards = proto.NameWildcards;
		NameRegex = proto.NameRegex;
		MemberType = proto.MemberType;
		ParametersCount = proto.ParametersCount;
		ParametersEx = proto.ParametersEx;
		Access = proto.Access;
		IgnoreCase = proto.IgnoreCase;
		IsStatic = proto.IsStatic;
		RequiredAttributes = proto.RequiredAttributes;
		IsAbstract = proto.IsAbstract;
		ReturnTypes = proto.ReturnTypes;
		DeclaringType = proto.DeclaringType;
		Predicate = proto.Predicate;
	}

	internal ReflectionFilter Setup([InstantHandle] Action<ReflectionFilter>? setup)
	{
		setup?.Invoke(this);
		return this;
	}
	
	public ReflectionFilter Where(Predicate<BaseMember> condition)
	{
		Predicate = Predicate == null
			? condition
			: member => Predicate(member) &&
			            condition(member);

		return this;
	}

	public ReflectionFilter RequireAttribute(TypeEx attributeType)
	{
		if (!typeof(Attribute).IsAssignableFrom(attributeType))
			throw new ArgumentException("Attribute type expected", nameof(attributeType));
		
		RequiredAttributes.Add(attributeType);
		return this;
	}

	public ReflectionFilter RequireAttribute<TAttribute>()
		where TAttribute : Attribute
		=> RequireAttribute(typeof(TAttribute));

	public bool Check(BaseMember member)
	{
		if (MemberType != 0 && (MemberType & member.MemberType) == 0)
			return false;

		if (DeclaringType != null && !Equals(DeclaringType, member.DeclaringType))
			return false;

		if (Access != AccessModifier.Any && (Access & member.Access) == AccessModifier.Any)
			return false;

		if (ReturnTypes != null && ReturnTypes.All(r => member.ReturnType?.CanCastTo(r, ReturnCastType) == false))
			return false;

		if (IsVirtual.HasValue && IsVirtual.Value != member.IsVirtual)
			return false;

		if (IsStatic.HasValue && IsStatic.Value != member.IsStatic)
			return false;

		if (IsAbstract.HasValue && IsAbstract.Value != member.IsAbstract)
			return false;

		if (Name != null && !Name.Equals(member.Name, IgnoreCase ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal))
			return false;
		
		if (NameWildcards != null && !Wildcards.Compare(member.Name, NameWildcards, IgnoreCase))
			return false;

		if (NameRegexExpression != null && !NameRegexExpression.IsMatch(member.Name))
			return false;
		
		if (RequiredAttributes.Any(attr => !member.HasAttribute(attr)))
			return false;

		var iwp = member as IInvokeWithParameters;

		if (ParametersCount.HasValue && iwp?.Parameters.Length != ParametersCount.Value)
			return false;

		if (ParametersEx != null && !iwp.With(i => ParametersProcessor.CanBeInvokedWith(i.Parameters, ParametersEx, InvocationOptions)))
			return false;

		// ReSharper disable once ConvertIfStatementToReturnStatement
		if (Predicate != null && !Predicate(member))
			return false;

		return true;
	}

}