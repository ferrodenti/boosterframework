using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using JetBrains.Annotations;
using static Booster.FastLazyStatic;

#nullable enable

namespace Booster.Reflection;

public abstract class BaseMember
{
	/// <summary>
	/// Уникальный идентификатор объекта рефлексии. Назначается при создании нового экземпляра описателя члена типа.
	/// </summary>
	public long Id { get; }
	/// <summary>
	/// Внутренний описатель MemberInfo
	/// </summary>
	public MemberInfo Inner { get; }

	public MemberTypes MemberType => _memberType ??= Inner.MemberType;
	FastLazy<MemberTypes>? _memberType;

	public Module Module => _module ??= Inner.Module;
	FastLazy<Module>? _module;

	public string Name => _name ??= Inner.Name;
	FastLazy<string>? _name;

	[PublicAPI]
	public int MetadataToken => Inner.MetadataToken;

	FastLazy<TypeEx?>? _declaringType;
	public TypeEx? DeclaringType => _declaringType ??= Inner.DeclaringType.With(TypeEx.Get);

	/// <summary>
	/// Если текущий член был переопределен, то указывает на член базового типа
	/// </summary>
	public BaseMember? BaseDefinition => _baseDefinition ??= GetBaseDefinition();
	FastLazy<BaseMember?>? _baseDefinition;

	/// <summary>
	///  Модификатор доступа члена
	/// </summary>
	public virtual AccessModifier Access => _access ??= FastLazy(() =>
	{
		var intern = IsInternal ? AccessModifier.Internal : AccessModifier.Any;

		if (IsPrivate)
			return AccessModifier.Private | intern;
		if (IsProtected)
			return AccessModifier.Protected | intern;
		if (IsProtectedOrInternal)
			return AccessModifier.ProtectedOrInternal | intern;
		if (IsProtectedAndInternal)
			return AccessModifier.ProtectedAndInternal | intern;
		if (IsPublic)
			return AccessModifier.Public | intern;
				
		return AccessModifier.Private | intern;
	});

	FastLazy<AccessModifier>? _access;

	public bool IsPublic => _isPublic ??= GetIsPublic();
	FastLazy<bool>? _isPublic;
	
	public bool IsProtected => _isProtected ??= GetIsProtected();
	FastLazy<bool>? _isProtected;
	
	public bool IsInternal => _isInternal ??= GetIsInternal();
	FastLazy<bool>? _isInternal;
	
	public bool IsPrivate  => _isPrivate ??= GetIsPrivate();
	FastLazy<bool>? _isPrivate;
	
	public bool IsProtectedAndInternal => _isProtectedAndInternal ??= GetIsProtectedAndInternal();
	FastLazy<bool>? _isProtectedAndInternal;
	
	public bool IsProtectedOrInternal => _isProtectedOrInternal ??= GetIsProtectedOrInternal();
	FastLazy<bool>? _isProtectedOrInternal;
	
	public bool IsStatic => _isStatic ??= GetIsStatic();
	FastLazy<bool>? _isStatic;
	
	public bool IsAbstract => _isAbstract ??= GetIsAbstract();
	FastLazy<bool>? _isAbstract;
	
	public bool IsVirtual => _isVirtual ??= GetIsVirtual();
	FastLazy<bool>? _isVirtual;
	
	public TypeEx? ReturnType => _returnType ??= GetReturnType();
	FastLazy<TypeEx?>? _returnType;

#pragma warning disable 1591
	protected abstract bool GetIsPublic ();
	protected abstract bool GetIsProtected ();
	protected abstract bool GetIsInternal();
	protected abstract bool GetIsPrivate();
	protected abstract bool GetIsStatic();
		
	protected virtual bool GetIsProtectedAndInternal() 
		=> false;
		
	protected virtual bool GetIsProtectedOrInternal() 
		=> false;
		
	protected virtual bool GetIsVirtual() 
		=> false;
		
	protected virtual bool GetIsAbstract() 
		=> false;
		
	protected virtual TypeEx? GetReturnType() 
		=> null;
		
	protected virtual BaseMember? GetBaseDefinition() 
		=> null;

	protected BaseMember(MemberInfo inner)
	{
		Inner = inner;
		Id = ReflectionRepository.GetNewId(this);
	}
#pragma warning restore 1591

	/// <summary>
	/// Получает все аттрибуты члена
	/// </summary>
	/// <param name="attributeType">Тип атрибута (можно указать базовый, чтобы получить всех наследников).</param>
	/// <param name="inherit">Искать атрибуты базовых классов</param>
	/// <param name="baseTypesFirst">Вначале искать в базовых типах</param>
	/// <returns>Перечисление атрибутов удовлетворяющих условиям</returns>
	public IEnumerable<object> FindAttributes(TypeEx attributeType, bool inherit = true, bool baseTypesFirst = true) 
		=> Inner.GetCustomAttributes(attributeType, inherit); //TODO: разобраться тут с наследованием
		
	/// <summary>
	/// Получает все аттрибуты заданного типа. Может быть указан базовый тип атрибута.
	/// </summary>
	/// <typeparam name="TAttribute">Тип атрибута (можно указать базовый, чтобы получить всех наследников).</typeparam>
	/// <param name="inherit">Искать атрибуты базовых классов</param>
	/// <param name="baseTypesFirst">Вначале искать в базовых типах</param>
	/// <returns>Перечисление атрибутов удовлетворяющих условиям</returns>
	public IEnumerable<TAttribute> FindAttributes<TAttribute>(bool inherit = true, bool baseTypesFirst = true) 
		=> FindAttributes(typeof (TAttribute), inherit, baseTypesFirst).Cast<TAttribute>();

	/// <summary>
	/// Находит аттрибут заданного типа. Может быть указан базовый тип атрибута.
	/// </summary>
	/// <typeparam name="TAttribute">Тип атрибута (можно указать базовый, чтобы получить всех наследников).</typeparam>
	/// <param name="inherit">Искать атрибуты базовых классов</param>
	/// <param name="baseTypesFirst">Вначале искать в базовых типах</param>
	/// <returns>Первый атрибут, удовлетворяющий условиям</returns>
	public TAttribute? FindAttribute<TAttribute>(bool inherit = true, bool baseTypesFirst = false) 
		=> FindAttributes(typeof (TAttribute), inherit, baseTypesFirst).Cast<TAttribute>().FirstOrDefault();

	public bool TryFindAttribute<TAttribute>(out TAttribute result)
	{
		var res = FindAttribute<TAttribute>();
		if (res != null)
		{
			result = res;
			return true;
		}

		result = default!;
		return false;
	}

	public bool HasAttribute<TAttribute>(bool inherit = true) 
		=> FindAttributes(typeof (TAttribute), inherit).Any();

	public bool HasAttribute(TypeEx attributeType, bool inherit = true) 
		=> FindAttributes(attributeType, inherit).Any();

	public override string ToString() => Inner.ToString();

	static readonly MemberInfoEqualityComparer _comparer = new();
	static readonly ConcurrentDictionary<MemberInfo, BaseMember> _cache1 = new();
	static readonly ConcurrentDictionary<MemberInfo, BaseMember> _cache2 = new(_comparer);

	protected static TMember Cache<TMember>(MemberInfo obj, Func<TMember> creator) where TMember : BaseMember 
		=> (TMember) _cache1.GetOrAdd(obj, _ => _cache2.GetOrAdd(obj, _ => creator()));

	/// <summary>
	/// Получает объект рефлексии по заданному идентификатору. Объект должен быть предварительно инициализирован.
	/// Метод преднозначен для кодогенерации, когда генератор заранее находит объект и может передать в сгенерированный код просто ссылку.
	/// </summary>
	/// <param name="id">Уникальный идентификатор объекта рефлексии.</param>
	/// <returns>Ранее инициализированный объект с этим идентификатором либо null, если объект отсутствует, либо тип объекта неправильный</returns>
	public static BaseMember GetById(long id) 
		=> ReflectionRepository.GetById<BaseMember>(id)!;
}