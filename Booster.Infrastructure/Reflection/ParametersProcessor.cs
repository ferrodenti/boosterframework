using System;
using System.Collections;
using System.Linq;
using System.Reflection;

namespace Booster.Reflection;

/// <summary>
/// Вспомогательный класс позволяющий определить подходит ли заданный набор типов аргументов для вызова метода и преобразовать аргументы перед вызовом
/// </summary>
public static class ParametersProcessor
{
	/// <summary>
	/// Проверка набора типов аргументов на пригодность к вызову метода с указанным набором параметров
	/// </summary>
	/// <param name="parameters">Набор параметров, относительно которого будет осуществлена проверка</param>
	/// <param name="argumentTypes">Типы аргументов, которые будут проверяться</param>
	/// <param name="invocationOptions">Настройки вызова определяющие возможные преобразования типов и заполнения необязательных параметров</param>
	/// <returns>true если метод может быть вызван с аргументами указанных типов</returns>
	public static bool CanBeInvokedWith(ParameterInfo[] parameters, TypeEx[] argumentTypes, InvocationOptions invocationOptions)
	{
		var castType = invocationOptions.CastType;
		var fillOptionals = invocationOptions.FillOptionals;
		var noDefaultsOnCastFail = !invocationOptions.DefaultsOnCastFail;

		for (var i = 0; i < parameters.Length; i++)
		{
			if (i == parameters.Length - 1)
			{
				var param = parameters[i];
				if (param.GetCustomAttributes<ParamArrayAttribute>()?.Any() == true)
				{
					if (i == argumentTypes.Length - 1 && argumentTypes[i].Inner == param.ParameterType)
						return true;

					if (!invocationOptions.ComposeParamsArray)
						return i == argumentTypes.Length && fillOptionals;

					if (noDefaultsOnCastFail)
					{
						var type = param.ParameterType.GetElementType();
						for (; i < argumentTypes.Length; i++)
							if (!(argumentTypes[i] ?? TypeEx.AnyRef).CanCastTo(type, castType))
								return false;
					}
					return true;
				}
			}

			if (i >= argumentTypes.Length)
			{
				if (!invocationOptions.DefaultsOnMissing)
					for (; i < parameters.Length; i++)
						if (!parameters[i].HasDefaultValue || !fillOptionals)
							return false;

				return true;
			}

			if (noDefaultsOnCastFail && !(argumentTypes[i] ?? TypeEx.AnyRef).CanCastTo(parameters[i].ParameterType, castType))
				return false;
		}
		return parameters.Length >= argumentTypes.Length;
	}

	/// <summary>
	/// Подготавливает набор аргументов к безопасному вызову метода
	/// </summary>
	/// <param name="targetType">Тип объекта-цели у которого будет вызван метод. Может быть null если вызывается конструктор, либо статичный метод</param>
	/// <param name="target">Объект-цель у которого будет вызван метод. Может быть null если вызывается конструктор, либо статичный метод</param>
	/// <param name="parameters">Набор параметров метода</param>
	/// <param name="arguments">Набор аргументов с которым будет вызываться метод</param>
	/// <param name="invocationOptions">Настройки вызова определяющие возможные преобразования типов и заполнения необязательных параметров</param>
	/// <returns>Если преобразование типов прошло успешно, то возвращается набор аргументов пригодный для безопасного вызова инвокатора</returns>
	/// <exception cref="T:System.ArgumentNullException">Не задан объект-цель при существующем типе объекта-цели</exception>  
	/// <exception cref="T:System.InvalidCastException">Невозможно (либо запрещено настройками) привести значение к типу параметра</exception>  
	/// <exception cref="T:System.ArgumentException">Не указанн аргумент для обязательного параметра, либо заполенение необязательных параметров запрещено настройками</exception>  
	public static object[] PrepareForInvoke(TypeEx targetType, object target, ParameterInfo[] parameters, object[] arguments, InvocationOptions invocationOptions)
	{
		var castType = invocationOptions.CastType;
		var fillOptionals = invocationOptions.FillOptionals;
		var defaultsOnCastFail = invocationOptions.DefaultsOnCastFail;
		var defaultsOnMissing = invocationOptions.DefaultsOnMissing;

		object[] result;
		object o;
		var i = 0;
		if (targetType != null)
		{
			result = new object[arguments.Length + 1];
			if (target == null)
				throw new ArgumentNullException(nameof(target), $"Ожидался экземпляр типа {targetType} у которого будет вызван метод");

			var tp = TypeEx.Of(target);

			if (!Cast.Try(target, targetType, out o, castType))
				throw new InvalidCastException($"Объект-цель: невозможно (либо запрещено настройками) привести значение типа {tp} к {targetType}.");

			result[0] = o;
			i++;
		}
		else
			result = new object[arguments.Length];

		for (var j = 0; j < parameters.Length; j++, i++)
		{
			var param = parameters[j];
			TypeEx paramType = param.ParameterType;

			if (j == parameters.Length - 1)
				if (param.GetCustomAttributes<ParamArrayAttribute>().SafeAny()) // Может возвращать null!
				{
					if (j == arguments.Length - 1 && TypeEx.Of(arguments[j]) == paramType)
					{
						result[i] = arguments[j];
						return result;
					}

					if (!invocationOptions.ComposeParamsArray)
					{
						if (j == arguments.Length && fillOptionals)
						{
							var n = new object[i + 1];
							Buffer.BlockCopy(result, 0, n, 0, i);
							n[i] = paramType.Create(0);
							return n;
						}
						return result;
					}

					var itemType = paramType.ArrayItemType;
					var arr = (IList)paramType.Create(arguments.Length - j);

					Array.Resize(ref result, i + 1);
					result[i] = arr;


					for (var k = 0; j < arguments.Length; k++, j++)
						if (Cast.Try(arguments[j], itemType, out o, castType))
							arr[k] = o;
						else if (defaultsOnCastFail)
							arr[k] = itemType.DefaultValue;
						else
							throw new InvalidCastException($"Параметр \"{param.Name}\": Невозможно (либо запрещено настройками) привести значение типа {TypeEx.Of(arguments[j])} к {itemType}");

					return result;
				}

			if (j >= arguments.Length)
			{
				Array.Resize(ref result, parameters.Length + targetType.With(() => 1));
				do
				{
					if (param.GetCustomAttributes<ParamArrayAttribute>().Any())
					{
						if (!fillOptionals)
							throw new ArgumentException($"Параметр \"{param.Name}\": Опциями вызова не разрешается заполнение необязательных параметров.");

						result[i] = paramType.Create(0);
					}
					else
					{
						if (param.HasDefaultValue)
						{
							if (!fillOptionals)
								throw new ArgumentException($"Параметр \"{param.Name}\": Опциями вызова не разрешается заполнение необязательных параметров.");

							result[i] = param.DefaultValue;
						}
						else if (defaultsOnMissing)
							result[i] = param.HasDefaultValue ? param.DefaultValue : paramType.DefaultValue;
						else
							throw new ArgumentException($"Параметр \"{param.Name}\": Не задано значения для обязательного параметра #{j + 1} типа {paramType}.", nameof(arguments));
					}
					if (++j >= parameters.Length)
						break;
					i++;
					param = parameters[j];
					paramType = param.ParameterType;
				} while (true);

				return result;
			}

			if(paramType.IsByRef && !param.IsOut && paramType.ElementType == TypeEx.Of(arguments[j]))
				result[i] = arguments[j];
			else if (Cast.Try(arguments[j], paramType, out o, castType))
				result[i] = o;
			else if (defaultsOnCastFail)
				result[i] = param.HasDefaultValue ? param.DefaultValue : paramType.DefaultValue;
			else
				throw new InvalidCastException($"Параметр \"{param.Name}\": Невозможно (либо запрещено настройками) привести значение типа {TypeEx.Of(arguments[j])} к {paramType}");
		}

		return result;
	}
}