using System;
using System.Reflection;
using System.Reflection.Emit;

namespace Booster.Reflection;

public sealed class Field : BaseDataMember
{
	public new readonly FieldInfo Inner; //TODO: property here?
	public new Field BaseDefinition => (Field)base.BaseDefinition;
	public TypeEx FieldType => ReturnType;

	public bool IsInitOnly => _isInitOnly ?? (_isInitOnly = Inner.IsInitOnly).Value;
	bool? _isInitOnly;

	public bool IsConstant => _isConstant ?? (_isConstant = Inner.IsLiteral).Value;
	bool? _isConstant;

	Field(FieldInfo inner) : base(inner)
		=> Inner = inner;

	static Field CreateCached(FieldInfo fi)
		=> Cache(fi, () => new Field(fi));
		
#pragma warning disable 1591
	protected override bool GetIsPublic() 
		=> Inner.IsPublic;
		
	protected override bool GetIsProtected() 
		=> Inner.IsFamily;
		
	protected override bool GetIsInternal() 
		=> Inner.IsAssembly;
		
	protected override bool GetIsPrivate() 
		=> Inner.IsPrivate;
		
	protected override bool GetIsStatic() 
		=> Inner.IsStatic;
		
	protected override TypeEx GetReturnType() 
		=> Inner.FieldType;

	protected override bool GetCanRead() 
		=> true;
		
	protected override bool GetCanWrite() 
		=> !IsInitOnly && !IsConstant;
		
	protected override bool GetHasPublicGetter() 
		=> IsPublic;
		
	protected override bool GetHasPublicSetter() 
		=> IsPublic && CanWrite;

	protected override Func<object,object> GetGetter()
	{
		var method = new DynamicMethod("FieldGetter", typeof (object), new[] {typeof (object)}, DeclaringType, true);
		var il = method.GetILGenerator();

		if (IsConstant)
		{
			var val = Inner.GetValue(null);
			return _ => val;
		}

		if (IsStatic)
			il.Emit(OpCodes.Ldsfld, Inner);
		else
		{
			il.Emit(OpCodes.Ldarg_0);
			il.UnboxIfNeeded(DeclaringType);
			il.Emit(OpCodes.Ldfld, Inner);
		}
		il.BoxIfNeeded(Inner.FieldType);
		il.Emit(OpCodes.Ret);

		return (Func<object,object>) method.CreateDelegate(typeof (Func<object,object>));
	}

	protected override Action<object,object> GetSetter()
	{
		if (IsInitOnly)
			return null;

		var method = new DynamicMethod("Fieldsetter", typeof(void), new[] { typeof(object), typeof(object) }, DeclaringType, true);
		var il = method.GetILGenerator();

		if (IsStatic)
		{
			il.Emit(OpCodes.Ldarg_1);
			il.UnboxIfNeeded(Inner.FieldType);
			il.Emit(OpCodes.Stsfld, Inner);
		}
		else
		{
			il.Emit(OpCodes.Ldarg_0);
			il.UnboxIfNeeded(DeclaringType);
			il.Emit(OpCodes.Ldarg_1);
			il.UnboxIfNeeded(Inner.FieldType);
			il.Emit(OpCodes.Stfld, Inner);
		}
		il.Emit(OpCodes.Ret);

		return (Action<object,object>)method.CreateDelegate(typeof(Action<object,object>));	
	}

		
	public static implicit operator Field(FieldInfo fi) 
		=> fi.With(CreateCached);
	public static implicit operator FieldInfo(Field field) 
		=> field?.Inner;
#pragma warning restore 1591
	/// <inheritdoc/>
	public static new Field GetById(long id) 
		=> ReflectionRepository.GetById<Field>(id);
}