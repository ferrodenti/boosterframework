using System.Reflection;
using System.Reflection.Emit;

namespace Booster.Reflection;

public sealed class Constructor : BaseMethodMember
{
	public new readonly ConstructorInfo Inner;
		
	public new Constructor BaseDefinition => (Constructor)base.BaseDefinition;

	Constructor(ConstructorInfo inner) : base(inner)
		=> Inner = inner;

	static Constructor CreateCached(ConstructorInfo ci)
		=> Cache(ci, () => new Constructor(ci));
		
#pragma warning disable 1591
	public static implicit operator Constructor(ConstructorInfo ci)
		=> ci.With(CreateCached);

	public static implicit operator ConstructorInfo(Constructor constructor)
		=> constructor?.Inner;

	protected override void EmitCall(ILGenerator il)
	{
		il.Emit(OpCodes.Newobj, this); // Зовем конструктор
		il.BoxIfNeeded(DeclaringType); // Делаем боксинг вэлью-типов
	}

	public object Invoke(InvocationOptions invocationOptions, params object[] arguments)
	{
		if (invocationOptions.DoPrepare)
			arguments = ParametersProcessor.PrepareForInvoke(null, null, Parameters, arguments, invocationOptions);

		return InvokeProc(arguments);
	}

	public object Invoke(params object[] arguments)
		=> Invoke(new InvocationOptions(), arguments);

#pragma warning restore 1591

	/// <inheritdoc/>
	public static new Constructor GetById(long id)
		=> ReflectionRepository.GetById<Constructor>(id);
}