using System;
using System.Linq;
using System.Reflection;

namespace Booster.Reflection;

public sealed class Event : BaseMember
{
	public new readonly EventInfo Inner;

	Box<Method> _addMethod;
	public Method AddMethod => _addMethod ??= new Box<Method>(Inner.AddMethod);

	Box<Method> _removeMethod;
	public Method RemoveMethod => _removeMethod ??= new Box<Method>(Inner.RemoveMethod);

	Box<Method> _raiseMethod;
	public Method RaiseMethod => _raiseMethod ??= new Box<Method>(Inner.RaiseMethod);

	Method _anyAccessMethod;
	public Method AnyAccessMethod => _anyAccessMethod ??= AddMethod ?? RemoveMethod ?? RaiseMethod;

	public new Event BaseDefinition => (Event) base.BaseDefinition;

	/// <inheritdoc/>
	public override AccessModifier Access
	{
		get
		{
			if (_access == AccessModifier.Any)
				_access = (AccessModifier) Math.Max(Math.Max(
						(int) (AddMethod?.Access ?? AccessModifier.Any),
						(int) (RemoveMethod?.Access ?? AccessModifier.Any)),
					(int) (RaiseMethod?.Access ?? AccessModifier.Any));

			return _access;
		}
	}
	AccessModifier _access;

	Event(EventInfo inner) : base(inner)
		=> Inner = inner;

	static Event CreateCached(EventInfo ci)
		=> Cache(ci, () => new Event(ci));

#pragma warning disable 1591
	protected override bool GetIsPublic() 
		=> Access.HasFlag(AccessModifier.Public);
		
	protected override bool GetIsProtected() 
		=> Access.HasFlag(AccessModifier.Protected);
		
	protected override bool GetIsInternal() 
		=> Access.HasFlag(AccessModifier.Internal);
		
	protected override bool GetIsPrivate() 
		=> Access.HasFlag(AccessModifier.Private);
		
	protected override bool GetIsProtectedAndInternal() 
		=> Access.HasFlag(AccessModifier.ProtectedAndInternal);
		
	protected override bool GetIsProtectedOrInternal() 
		=> Access.HasFlag(AccessModifier.ProtectedOrInternal);
		
	protected override bool GetIsStatic() 
		=> (AddMethod?.IsStatic | RemoveMethod?.IsStatic | RaiseMethod?.IsStatic) == true;
		
	protected override bool GetIsAbstract() 
		=> (AddMethod?.IsAbstract | RemoveMethod?.IsAbstract | RaiseMethod?.IsAbstract) == true;
		
	protected override bool GetIsVirtual() 
		=> (AddMethod?.IsVirtual | RemoveMethod?.IsVirtual | RaiseMethod?.IsVirtual) == true;

	protected override BaseMember GetBaseDefinition()
	{
		BaseMember bd = AnyAccessMethod.BaseDefinition;

		if (bd.DeclaringType != DeclaringType)
			foreach(var ev in bd.DeclaringType.Events)
				if (ev.AddMethod == bd || ev.RemoveMethod == bd || ev.RaiseMethod == bd)
					return ev;

		return this;
	}

	public static implicit operator Event(EventInfo ci)
		=> ci.With(CreateCached);
		
	public static implicit operator EventInfo(Event constructor)
		=> constructor?.Inner;
		
#pragma warning restore 1591

	public Event FindImplementation(TypeEx type)
	{
		if (!DeclaringType.IsInterface && !DeclaringType.IsAbstract)
			throw new InvalidOperationException("Not an interface member");

		if (AddMethod != null)
		{
			foreach (var impl in AddMethod.FindPossibleImplementations(type))
			{
				var result = type.Events.FirstOrDefault(property => property.AddMethod == impl);
				if (result != null)
					return result;
			}
			throw new InvalidOperationException("Add method not implemented!");
		}

		if (RemoveMethod != null)
		{
			foreach (var impl in RemoveMethod.FindPossibleImplementations(type))
			{
				var result = type.Events.FirstOrDefault(property => property.RemoveMethod == impl);
				if (result != null)
					return result;
			}
			throw new InvalidOperationException("Remove method not implemented!");
		}
		if (RaiseMethod != null)
		{
			foreach (var impl in RaiseMethod.FindPossibleImplementations(type))
			{
				var result = type.Events.FirstOrDefault(property => property.RaiseMethod == impl);
				if (result != null)
					return result;
			}
			throw new InvalidOperationException("Raise method not implemented!");
		}
		return null;
	}

	/// <inheritdoc/>
	public static new Event GetById(long id)
		=> ReflectionRepository.GetById<Event>(id);
}