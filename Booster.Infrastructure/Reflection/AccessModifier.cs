using System;

namespace Booster.Reflection;

/// <summary>
/// Модификатор доступа объекта рефлексии
/// </summary>
[Flags]
public enum AccessModifier
{
	/// <summary>
	/// Любой. Выбирает объекты независимо от их модификатора доступа
	/// </summary>
	Any = 0,
	/// <summary>
	/// Доступ только из данного типа
	/// </summary>
	Private = 1,
	/// <summary>
	/// Доступ только из данной иерархии типов
	/// </summary>
	Protected = 4,
	/// <summary>
	/// Доступ только из данной иерархии типов и данной сборки. В C# на данный момент не реализован. В IL называется FamilyAndAssembly
	/// </summary>
	ProtectedAndInternal = 2,
	/// <summary>
	/// Доступ только из данной иерархии типов или данной сборки. В C# задается как "<code>protected internal</code>". В IL называется FamilyOrAssembly
	/// </summary>
	ProtectedOrInternal = 8,
	/// <summary>
	/// Доступ только из данной сборки
	/// </summary>
	Internal = 16,
	/// <summary>
	/// Доступ любому коду
	/// </summary>
	Public = 32,
	/// <summary>
	/// Все, что не доступно абсолютно любому коду: совокупность <code>Internal | Protected | Private | ProtectedAndInternal | ProtectedOrInternal</code>
	/// </summary>
	NonPublic = Internal | Protected | Private | ProtectedAndInternal | ProtectedOrInternal
}