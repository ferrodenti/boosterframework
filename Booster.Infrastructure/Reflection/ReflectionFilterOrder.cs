namespace Booster.Reflection;

public enum ReflectionFilterOrder
{
	None,
	ByNameAsc,
	ByNameDesc,
	AncestorsToDescendants,
	DescendantsToAncestors,
	DelaringAncestorsToDescendants,
	DelaringDescendantsToAncestors
}