using System;

namespace Booster.Reflection;

/// <summary>
/// Настройки приведения типов
/// </summary>
[Flags]
public enum CastType
{
	/// <summary>
	/// Любые приведения запрещены
	/// </summary>
	Deny = 0,
	/// <summary>
	/// Разрешено приведение к базовому типу или интерфейсу
	/// </summary>
	ToBase = 1,
	/// <summary>
	/// Разрешено использование неявных операторов приведения 
	/// </summary>
	Implicit = 2,
	/// <summary>
	/// Разрешено использование явных операторов приведения 
	/// </summary>
	Explicit = 4,
	/// <summary>
	/// Разрешено приведение любых числовых типов
	/// </summary>
	Numerics = 8, //TODO: Minor2Major, Major2Minor
	/// <summary>
	/// Разрешено приведение числовых типов к перечислениями и наоборот
	/// </summary>
	Enums = 16, //TODO: Numerics2Enums, Enums2Numerics, Enums2Enums
	/// <summary>
	/// Разрешено приведение Nullable-типов к типам-значениям с использованием значений по умолчанию
	/// </summary>
	Nullables = 32,
	/// <summary>
	/// Разрешено приведение null к значениям по умолчанию типов-значений
	/// </summary>
	Nulls2Defaults = 64,
	/// <summary>
	/// Разрешен парсинг строковых значений с применением <see cref="T:Booster.StringConverter"/>
	/// </summary>
	StringParsing = 128,
	/// <summary>
	/// Разрешено преобразование в строку с применением <see cref="T:Booster.StringConverter"/>
	/// </summary>
	Stringify = 256,
	/// <summary>
	/// Разрешены любые доступные приведения типов
	/// </summary>
	Any = 65535,
}