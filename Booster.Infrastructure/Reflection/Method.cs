using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading.Tasks;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Reflection;

[PublicAPI]
public sealed class Method : BaseMethodMember
{
	public new readonly MethodInfo Inner;

	public new Method? BaseDefinition 
		=> (Method?) base.BaseDefinition;
	
	public bool IsGeneric 
		=> Inner.IsGenericMethod;
		
	/// <summary>
	/// Все обобщенные параметры типа
	/// </summary>
	public TypeEx[] GenericArguments => _genericArguments ??= Inner.GetGenericArguments().Select(TypeEx.Get).ToArray();
	TypeEx[]? _genericArguments;
		
	Method(MethodInfo inner) : base(inner) 
		=> Inner = inner;

	static Method CreateCached(MethodInfo mi) 
		=> Cache(mi, () => new Method(mi));

#pragma warning disable 1591
	protected override bool GetIsAbstract() 
		=> Inner.IsAbstract;
	protected override TypeEx GetReturnType() 
		=> Inner.ReturnType;
	protected override BaseMember GetBaseDefinition() 
		=> (Method)Inner.GetBaseDefinition();
	protected override bool GetIsVirtual() 
		=> Inner.IsVirtual;

	protected override int EmitThisExtract(ILGenerator il)
	{
		if (IsStatic)
			return 0;

		il.Emit(OpCodes.Ldarg_0); // берем массив из стека
		il.Emit(OpCodes.Ldc_I4_0);
		il.Emit(OpCodes.Ldelem_Ref); // Извлекаем элемент

		if (DeclaringType?.IsValueType == true)
		{
			il.DeclareLocal(DeclaringType);
			il.Emit(OpCodes.Unbox_Any, DeclaringType);
			il.Emit(OpCodes.Stloc_0);
			il.Emit(OpCodes.Ldloca_S, 0);
		}
		return 1;
	}

	protected override void EmitCall(ILGenerator il)
	{
		il.Emit(OpCodes.Call, this); // Зовем метод

		if (ReturnType?.Inner == typeof (void))
			il.Emit(OpCodes.Ldnull);
		else
			il.BoxIfNeeded(ReturnType); // Делаем боксинг вэлью-типов
	}

	public static implicit operator Method(MethodInfo mi) 
		=> mi.With(CreateCached)!;
	
	public static implicit operator MethodInfo(Method method) 
		=> method.With(m => m.Inner)!;
	
#pragma warning restore 1591

	public object? Invoke(object? target, params object?[] parameters) 
		=> Invoke(target, new InvocationOptions(), parameters);

	public object? Invoke(object? target, InvocationOptions invocationOptions, params object?[] parameters)
	{
		if (invocationOptions.DoPrepare)
			parameters = ParametersProcessor.PrepareForInvoke(IsStatic ? null : DeclaringType, target, Parameters, parameters, invocationOptions);

		return InvokeProc(parameters);
	}

	public Task<object?> InvokeAsync(object? target, params object?[] arguments) 
		=> TryAwait(Invoke(target, new InvocationOptions(), arguments));

	public Task<object?> InvokeAsync(object target, InvocationOptions invocationOptions, params object?[] arguments)
		=> TryAwait(Invoke(target, new InvocationOptions(), arguments));

	static async Task<object?> TryAwait(object? result)
	{
		if (result is Task task)
		{
			await task.NoCtx();

			var taskType = TypeEx.Of(result);
#if NETSTANDARD
				if (taskType.IsGenericType)
#else
			if (taskType.HasGenericTypeDefinition(typeof(Task<>)))
#endif
			{
				var resProp = taskType.FindProperty("Result");
				if (resProp != null)
					result = resProp.GetValue(result);
			}
		}
		return result;
	}

	internal bool DeclaringTypeAbstract => DeclaringType != null && (DeclaringType.IsInterface || DeclaringType.IsAbstract);
	
	/// <summary>
	/// Находит реализацию объявленного в абстрактном классе или интерфейсе метода в указанном типе
	/// </summary>
	public Method? FindImplementation(TypeEx targetType)
	{
		if (!DeclaringTypeAbstract)
			throw new InvalidOperationException("Not an interface or abstract class member");

		return FindPossibleImplementations(targetType).FirstOrDefault();
	}

	/// <summary>
	/// Находит все методы с подходящей для переопределения или реализации сигнатурой. Сортировка от типа к предкам.
	/// </summary>
	/// <param name="targetType">Тип в котором искать методы</param>
	/// <returns>Перечисление подходящих методов реализаций</returns>
	internal IEnumerable<Method> FindPossibleImplementations(TypeEx? targetType)
	{
		while (targetType != null)
		{
			if (DeclaringType?.IsInterface == true)
				foreach (var mtd in targetType.FindMethods(new ReflectionFilter
						 {
							 Access = AccessModifier.Private,
							 IsStatic = false,
							 ReturnType = ReturnType,
							 NameWildcards = $"*.{Name}",
							 ParametersEx = ParameterTypes
						 }))
					yield return mtd;

			foreach (var mtd in targetType.FindMethods(new ReflectionFilter(Name, ParameterTypes)
					 {
						 Access = Access,
						 IsVirtual = DeclaringTypeAbstract ? null : true,
						 IsStatic = false,
						 ReturnType = ReturnType
					 }))
				yield return mtd;

			targetType = targetType.BaseType;
		}
	}

	public Method MakeGenericMethod(params TypeEx[] typeArguments) 
		=> MakeGenericMethod(typeArguments.Select(t => t.Inner).ToArray());
	public Method MakeGenericMethod(params Type[] typeArguments) 
		=> Inner.MakeGenericMethod(typeArguments);

	/// <inheritdoc cref="BaseMethodMember.GetById" />
	public static new Method? GetById(long id) 
		=> ReflectionRepository.GetById<Method>(id);
}