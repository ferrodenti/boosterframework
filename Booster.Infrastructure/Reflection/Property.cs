using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace Booster.Reflection;

public sealed class Property : BaseDataMember, IInvokeWithParameters
{
	/// <inheritdoc/>
	public new readonly PropertyInfo Inner;

	public Method GetMethod => _getMethod ??= new Box<Method>(Inner.GetMethod);
	Box<Method> _getMethod;

	public Method SetMethod => _setMethod ??= new Box<Method>(Inner.SetMethod);
	Box<Method> _setMethod;

	public Method AnyAccessMethod => _anyAccessMethod ??= GetMethod ?? SetMethod;
	Method _anyAccessMethod;

	public new Property BaseDefinition => (Property) base.BaseDefinition;

	public TypeEx PropertyType => ReturnType;

	public ParameterInfo[] Parameters => _parameters ??= Inner.GetIndexParameters();
	ParameterInfo[] _parameters;

	/// <inheritdoc/>
	public override AccessModifier Access
	{
		get
		{
				
			if (_access == AccessModifier.Any)
				_access = (AccessModifier) Math.Max(
					(int) (GetMethod?.Access ?? AccessModifier.Any),
					(int) (SetMethod?.Access ?? AccessModifier.Any));

			return _access;
		}
	}
	AccessModifier _access;

	Property(PropertyInfo inner) : base(inner)
		=> Inner = inner;

	static Property CreateCached(PropertyInfo pi)
		=> Cache(pi, () => new Property(pi));
		
#pragma warning disable 1591
	protected override bool GetIsPublic() 
		=> Access.HasFlag(AccessModifier.Public);
		
	protected override bool GetIsProtected() 
		=> Access.HasFlag(AccessModifier.Protected);
		
	protected override bool GetIsInternal() 
		=> Access.HasFlag(AccessModifier.Internal);
		
	protected override bool GetIsPrivate() 
		=> Access.HasFlag(AccessModifier.Private);
		
	protected override bool GetIsProtectedAndInternal() 
		=> Access.HasFlag(AccessModifier.ProtectedAndInternal);
		
	protected override bool GetIsProtectedOrInternal() 
		=> Access.HasFlag(AccessModifier.ProtectedOrInternal);
		
	protected override bool GetIsStatic() 
		=> (GetMethod?.IsStatic | SetMethod?.IsStatic) == true;
		
	protected override bool GetIsAbstract() 
		=> (GetMethod?.IsAbstract | SetMethod?.IsAbstract) == true;
		
	protected override bool GetIsVirtual() 
		=> (GetMethod?.IsVirtual | SetMethod?.IsVirtual) == true;
		
	protected override TypeEx GetReturnType() 
		=> Inner.PropertyType;

	protected override bool GetCanRead() 
		=> Inner.CanRead;
		
	protected override bool GetCanWrite() 
		=> Inner.CanWrite;
		
	protected override bool GetHasPublicGetter() 
		=> GetMethod?.IsPublic == true;
		
	protected override bool GetHasPublicSetter() 
		=> SetMethod?.IsPublic == true;


	protected override Func<object,object> GetGetter()
	{
		if (!CanRead || Parameters.Length > 0)
			return null;

		if (DeclaringType.IsInterface)
			return target =>
			{
				var type = target.GetType();
				var implementation = FindImplementation(type);
				if (implementation == null)
					throw new ReflectionException($"Could not find implementation of property {type.Name}.{Name}");

				return implementation.GetValue(target);
			};


		var method = new DynamicMethod("PropertyGetter", typeof (object), new[] {typeof (object)}, DeclaringType, true);
		var il = method.GetILGenerator();

		if (!IsStatic)
		{
			il.Emit(OpCodes.Ldarg_0);
			if (DeclaringType.IsValueType)
			{
				il.DeclareLocal(DeclaringType);
				il.Emit(OpCodes.Unbox_Any, DeclaringType);
				il.Emit(OpCodes.Stloc_0);
				il.Emit(OpCodes.Ldloca_S, 0);
			}
		}
		il.Emit(OpCodes.Call, GetMethod);
		il.BoxIfNeeded(GetMethod.ReturnType);
		il.Emit(OpCodes.Ret);

		return (Func<object,object>) method.CreateDelegate(typeof (Func<object,object>));
	}

	protected override Action<object,object> GetSetter()
	{
		if (!CanWrite || Parameters.Length > 0)
			return null;
			
		if (DeclaringType.IsInterface)
			return (target, value) =>
			{
				var type = target.GetType();
				var implementation = FindImplementation(type);
				if (implementation == null)
					throw new ReflectionException($"Could not find implementation of property {type.Name}.{Name}");

				implementation.SetValue(target, value);
			};

		var method = new DynamicMethod("PropertySetter", typeof(void), new[] { typeof(object), typeof(object) }, DeclaringType, true);
		var il = method.GetILGenerator();

		if (!IsStatic)
		{
			il.Emit(OpCodes.Ldarg_0);
			if (DeclaringType.IsValueType)
			{
				il.DeclareLocal(DeclaringType);
				il.Emit(OpCodes.Unbox_Any, DeclaringType);
				il.Emit(OpCodes.Stloc_0);
				il.Emit(OpCodes.Ldloca_S, 0);
			}
		}
		il.Emit(OpCodes.Ldarg_1);
		il.UnboxIfNeeded(SetMethod.Parameters[0].ParameterType);
		il.Emit(OpCodes.Call, SetMethod);
		il.Emit(OpCodes.Ret);

		return (Action<object,object>)method.CreateDelegate(typeof(Action<object,object>));
	}

	protected override BaseMember GetBaseDefinition()
	{
		BaseMember bd = AnyAccessMethod.BaseDefinition;

		if (bd.DeclaringType != DeclaringType)
			foreach(var prop in bd.DeclaringType.Properties)
				if (prop.GetMethod == bd || prop.SetMethod == bd)
					return prop;

		return this;
	}

		
	public static implicit operator Property(PropertyInfo pi)
		=> pi.With(CreateCached);
		
	public static implicit operator PropertyInfo(Property property)
		=> property?.Inner;
		
#pragma warning restore 1591

	public bool CanBeInvokedWith(params TypeEx[] indexerArgumentTypes)
		=> ParametersProcessor.CanBeInvokedWith(Parameters, indexerArgumentTypes, new InvocationOptions());

	public bool CanBeInvokedWith(InvocationOptions invocationOptions, params TypeEx[] indexerArgumentTypes)
		=> ParametersProcessor.CanBeInvokedWith(Parameters, indexerArgumentTypes, invocationOptions);

	/// <summary>
	/// Находит реализацию объявленного в абстрактном классе или интерфейсе свойства в указанном типе
	/// </summary>
	public Property FindImplementation(TypeEx type)
	{
		if (!DeclaringType.IsInterface && !DeclaringType.IsAbstract)
			throw new InvalidOperationException("Not an interface member");

		if (GetMethod != null)
		{
			foreach (var impl in GetMethod.FindPossibleImplementations(type))
			{
				var result = type.Properties.FirstOrDefault(property => property.GetMethod == impl);
				if (result != null)
					return result;
			}
			throw new InvalidOperationException("Get method not implemented!");
		}

		if (SetMethod != null)
		{
			foreach (var impl in SetMethod.FindPossibleImplementations(type))
			{
				var result = type.Properties.FirstOrDefault(property => property.SetMethod == impl);
				if (result != null)
					return result;
			}
			throw new InvalidOperationException("Set method not implemented!");
		}
		return null;
	}
		
	/// <inheritdoc/>
	public static new Property GetById(long id)
		=> ReflectionRepository.GetById<Property>(id);
}