using System;
using System.Collections.Generic;
using System.Reflection;
#if DNX
using System.Linq;
#endif

namespace Booster.Reflection;

/// <summary>
/// Осуществляет сравнения System.Reflection.MemberInfo таким образом, чтобы экземпляры, описывающие на один и тот же член были равны.
/// По умолчанию фрейворк сравнивает MemberInfo по ссылкам, что приводит к тому, что дескрипторы полученные из 
/// разных классов иерархии, но указывающие на один член будут различаться
/// </summary>
public class MemberInfoEqualityComparer : IEqualityComparer<MemberInfo>
{
	/// <summary>
	/// Сравнивает 2 объекта System.Reflection.MemberInfo
	/// </summary>
	/// <returns>true если x и y описывают один и тот же член класса</returns>
	public bool Equals(MemberInfo x, MemberInfo y)
	{
		if (ReferenceEquals(x, y))
			return true;

		if (x == null ||
			x.MemberType != y?.MemberType ||
			x.Name != y.Name ||
			x.DeclaringType != y.DeclaringType ||
			!ReferenceEquals(x.Module, y.Module))
			return false;

		var fi = x as FieldInfo;
		if (fi != null)
			return Equals(fi, (FieldInfo) y);

		var ci = x as MethodBase;
		if (ci != null)
			return Equals(ci, (MethodBase) y);

		var pi = x as PropertyInfo;
		if (pi != null)
			return Equals(pi, (PropertyInfo) y);

		var ei = x as EventInfo;
		if (ei != null)
			return Equals(ei, (EventInfo) y);

		if (x is Type type)
			return type == (Type) y;

		throw new ArgumentException($"Unexpected value type: {x.GetType()}", nameof(x));
	}

	static bool Equals(FieldInfo x, FieldInfo y)
		=> x.Attributes == y.Attributes &&
		   x.FieldType == y.FieldType;

	static bool Equals(MethodBase x, MethodBase y)
		=> x.Attributes == y.Attributes &&
		   x.MethodHandle == y.MethodHandle;

	bool Equals(PropertyInfo x, PropertyInfo y)
		=> x.Attributes == y.Attributes &&
		   x.PropertyType == y.PropertyType &&
		   Equals((MemberInfo) x.GetMethod, y.GetMethod) &&
		   Equals((MemberInfo) x.SetMethod, y.SetMethod);

	bool Equals(EventInfo x, EventInfo y)
		=> x.Attributes == y.Attributes &&
		   x.EventHandlerType == y.EventHandlerType &&
		   Equals((MemberInfo) x.AddMethod, y.AddMethod) &&
		   Equals((MemberInfo) x.RemoveMethod, y.RemoveMethod) &&
		   Equals((MemberInfo) x.RaiseMethod, y.RaiseMethod);

	/// <summary>
	/// Возвращает хеш-значение указанного объекта
	/// </summary>
	public int GetHashCode(MemberInfo obj)
	{
		unchecked
		{
			var pre = obj.MemberType.GetHashCode() +
					  obj.Name.GetHashCode() +
					  obj.DeclaringType.With(d => d.GetHashCode()) +
					  obj.Module.GetHashCode();


			var fi = obj as FieldInfo;
			if (fi != null)
				return pre + fi.Attributes.GetHashCode() + fi.FieldType.GetHashCode();

			var mi = obj as MethodBase;
			if (mi != null)
				return pre + mi.Attributes.GetHashCode() + mi.MethodHandle.GetHashCode();

			var pi = obj as PropertyInfo;
			if (pi != null)
				return pre + pi.Attributes.GetHashCode() + pi.GetMethod.With(m => GetHashCode(m)) + pi.SetMethod.With(m => GetHashCode(m));

			var ei = obj as EventInfo;
			if (ei != null)
				return pre + ei.Attributes.GetHashCode() + ei.AddMethod.With(m => GetHashCode(m)) + ei.RemoveMethod.With(m => GetHashCode(m)) + ei.RaiseMethod.With(m => GetHashCode(m));

			if (obj is Type type)
				return pre + type.GetHashCode();

			throw new ArgumentException($"Unexpected value type: {obj.GetType()}", nameof(obj));
		}
	}
}
