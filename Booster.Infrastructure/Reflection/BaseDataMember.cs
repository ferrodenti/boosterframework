using System;
using System.Reflection;
using Booster.Interfaces;

#nullable enable

namespace Booster.Reflection;

/// <summary>
/// Базовый класс для полей и свойств
/// </summary>
public abstract class BaseDataMember : BaseMember, IDataAccessor
{
	/// <summary>
	/// Указывает, поддерживает ли член извлечение значения
	/// </summary>
	public bool CanRead => _canRead ??= GetCanRead();
	FastLazy<bool>? _canRead;

	/// <summary>
	/// Указывает, поддерживает ли член изменение значения после инициализации типа
	/// </summary>
	public bool CanWrite => _canWrite ??= GetCanWrite();
	FastLazy<bool>? _canWrite;

	/// <summary>
	/// Тип значения поля или свойства (тоже самое, что и <see cref="P:Booster.Reflection.BaseMember.ReturnType"/>)
	/// </summary>
	public TypeEx ValueType => ReturnType!;

	/// <summary>
	/// Указывает, является ли извлечение значения публичным (публичный get метод свойства или публичное поле)
	/// </summary>
	public bool HasPublicGetter => _hasPublicGetter ??= GetHasPublicGetter();
	FastLazy<bool>? _hasPublicGetter;
		
	/// <summary>
	/// Указывает, является ли изменение значения публичным (публичный set метод свойства или публичное не readonly поле)
	/// </summary>
	public bool HasPublicSetter => _hasPublicSetter ??= GetHasPublicSetter();
	FastLazy<bool>? _hasPublicSetter;
	
	protected Func<object?, object?> Getter => _getter ??= GetGetter();
	FastLazy<Func<object?,object?>>? _getter;

	protected Action<object?, object?> Setter => _setter ??= GetSetter();
	FastLazy<Action<object?,object?>>? _setter; 

	protected abstract bool GetCanRead();
	protected abstract bool GetCanWrite();
	protected abstract bool GetHasPublicGetter();
	protected abstract bool GetHasPublicSetter();
	protected abstract Func<object?,object?> GetGetter();
	protected abstract Action<object?,object?> GetSetter();

	protected BaseDataMember(MemberInfo inner) : base(inner)
	{
	}

	public object? GetValue(object? target)
	{
		if (Getter == null)
			throw new Exception("Член не предусматривает извлечения значений");

		return Getter(target);
	}

	public bool SetValue(object? target, object? value)
	{
		if (Setter == null)
			throw new Exception("Член не предусматривает установку значений");

		Setter(target, value);

		return true;
	}


	public static implicit operator BaseDataMember(MemberInfo mi)
	{
		var pi = mi as PropertyInfo;
		if (pi != null)
			return (Property) pi;
		
		var fi = mi as FieldInfo;
		if (fi != null)
			return (Field) fi;

		throw new InvalidCastException($"Cannot cast {mi.GetType().Name} to a {nameof(BaseDataMember)} instance");
	}

	public new static BaseDataMember GetById(long id)
		=> ReflectionRepository.GetById<BaseDataMember>(id)!;
}