using System;

#nullable enable

namespace Booster.Reflection;

/// <summary>
/// Вспомогательный класс для приведения типов
/// </summary>
public static class Cast
{
	public static bool Try<TValue>(object value, out TValue result, CastType castType = CastType.Any)
	{
		result = default!;
		return Try(value, typeof(TValue), out var obj, castType) && obj.Is(out result);
	}

	/// <summary>
	/// Приводит указанные значение к типу если это возможно и разрешено настроками приведения 
	/// </summary>
	/// <param name="value">Исходное значение. Может быть null, который успешно приводится к любым ссылочным типам в отличии от похожего метода <seealso cref="M:Booster.Reflection.TypeEx.TryCast"/></param>
	/// <param name="to">Желаемый тип</param>
	/// <param name="result">Результат приведения</param>
	/// <param name="castType">Настройки приведения</param>
	/// <returns>true если приведение произошло успешно</returns>
	public static bool Try(object? value, TypeEx to, out object? result, CastType castType = CastType.Any)
	{		
		result = null;
		if (value == null)
		{
			if (!to.IsValueType)
				return true;

			if (castType.HasFlag(CastType.Nulls2Defaults))
			{
				result = to.DefaultValue;
				return true;
			}
			return false;
		}

		return TypeEx.Of(value).TryCast(value, to, out result, castType);
	}


	/// <summary>
	/// Приводит указанные значение к типу 
	/// </summary>
	/// <param name="value">Исходное значение. Может быть null, который успешно приводится к любым ссылочным типам</param>
	/// <param name="to">Желаемый тип</param>
	/// <param name="castType">Настройки приведения</param>
	/// <returns>Результат приведения</returns>
	/// <exception cref="T:System.InvalidCastException">Невозможно (либо запрещено настройками) привести значение к типу</exception>  
	public static object Do(object value, TypeEx to, CastType castType = CastType.Any)
	{
		if (!Try(value, to, out var res, castType))
			throw new InvalidCastException($"Невозможно привести значение типа {TypeEx.Of(value)} к {to}.");

		return res!;
	}
}