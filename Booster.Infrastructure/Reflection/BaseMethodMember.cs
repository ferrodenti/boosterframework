using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace Booster.Reflection;

public abstract class BaseMethodMember : BaseMember, IInvokeWithParameters
{	 
	public new readonly MethodBase Inner;

	public RuntimeMethodHandle MethodHandle => _methodHandle ?? (_methodHandle = Inner.MethodHandle).Value;
	RuntimeMethodHandle? _methodHandle;

	public ParameterInfo[] Parameters => _parameters ??= Inner.GetParameters();
	ParameterInfo[] _parameters;

	public TypeEx[] ParameterTypes => _parameterTypes ??= Parameters.Select(p => TypeEx.Get(p.ParameterType)).ToArray();
	TypeEx[] _parameterTypes;

	public bool IsSpecialName => _isSpecialName ?? (_isSpecialName = Inner.IsSpecialName).Value;
	bool? _isSpecialName;

#pragma warning disable 1591
	protected Func<object[], object> InvokeProc => _invokeProc ??= GetInvokeProc();
	Func<object[], object> _invokeProc; 

	protected override bool GetIsPublic() 
		=> Inner.IsPublic;
		
	protected override bool GetIsProtected() 
		=> Inner.IsFamily;
		
	protected override bool GetIsInternal() 
		=> Inner.IsAssembly;
		
	protected override bool GetIsPrivate() 
		=> Inner.IsPrivate;
		
	protected override bool GetIsStatic() 
		=> Inner.IsStatic;
		
	protected virtual int EmitThisExtract(ILGenerator il) 
		=> 0;
		
	protected abstract void EmitCall(ILGenerator il);

	protected BaseMethodMember(MethodBase inner) : base(inner)
		=> Inner = inner;

	protected virtual Func<object[], object> GetInvokeProc()
	{
		var method = new DynamicMethod("Invocator",
			typeof (object),
			new[] {typeof (object[])},
			Module, DeclaringType?.IsVisible == false || !IsPublic || IsInternal);

		var il = method.GetILGenerator();

		var k = EmitThisExtract(il);

		for (var i = 0; i < Parameters.Length; i++, k++)
		{
			var param = Parameters[i];
			il.Emit(OpCodes.Ldarg_0); // берем массив из стека


			switch (k) // пишем смещение
			{
			case 0:
				il.Emit(OpCodes.Ldc_I4_0);
				break;
			case 1:
				il.Emit(OpCodes.Ldc_I4_1);
				break;
			case 2:
				il.Emit(OpCodes.Ldc_I4_2);
				break;
			case 3:
				il.Emit(OpCodes.Ldc_I4_3);
				break;
			default:
				il.Emit(OpCodes.Ldc_I4, k);
				break;
			}
			if (param.ParameterType.IsByRef && !param.IsOut)
				il.Emit(OpCodes.Ldelem_Ref); // TODO: не работают ref параметры
			else
			{
				il.Emit(OpCodes.Ldelem_Ref); // Извлекаем элемент
				il.UnboxIfNeeded(param.ParameterType); // Делаем анбоксинг вэлью-типов
			}
		}

		EmitCall(il);

		il.Emit(OpCodes.Ret); // Все

		//byte[] arr = il.GetType().GetMethod("BakeByteArray", BindingFlags.NonPublic | BindingFlags.Instance).Invoke(il, null) as byte[];

		return (Func<object[], object>)method.CreateDelegate(typeof (Func<object[], object>));	
	}
#pragma warning restore 1591

	public bool CanBeInvokedWith(params TypeEx[] argumentTypes)
		=> ParametersProcessor.CanBeInvokedWith(Parameters, argumentTypes, new InvocationOptions());

	public bool CanBeInvokedWith(InvocationOptions invocationOptions, params TypeEx[] argumentTypes)
		=> ParametersProcessor.CanBeInvokedWith(Parameters, argumentTypes, invocationOptions);

	/// <inheritdoc cref="BaseMember.GetById" />
	public static new BaseMethodMember GetById(long id)
		=> ReflectionRepository.GetById<BaseMethodMember>(id);
}