using System;

namespace Booster.Reflection;

public class ReflectionException : Exception
{
	public ReflectionException()
	{
	}

	public ReflectionException(string message)
		: base(message)
	{
	}

	public ReflectionException(TypeEx type, string name)
		: base($"Cannot reflect {type.Name}.{name}")
	{
	}

	public ReflectionException(BaseMember member, string name)
		: base($"Cannot reflect {member.Name}.{name}")
	{
	}
}