﻿using System;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace Booster;

[PublicAPI]
public static class UniqueGenerator
{
	/// <summary>
	/// Создает уникальное значение, последовательно проверяя уникальность и изменяя его
	/// </summary>
	/// <param name="check">Метод проверки уникальности</param>
	/// <param name="mutate">Метод изменения значения, где на вход подается значение счетчика цикла</param>
	/// <param name="from">Начало цикла подбора</param>
	/// <param name="to">Конец цикла подбора. При достижении этого значения будет возвращено default(TValue)</param>
	/// <returns>Первое значение прошедшее проверку на уникальность или default(TValue)</returns>
	public static TValue Generate<TValue>(
		[InstantHandle] Func<TValue, bool> check,
		[InstantHandle] Func<int, TValue> mutate,
		int from = 1, int to = int.MaxValue)
		=> Generate(mutate(from), check, mutate, from + 1, to);

	/// <summary>
	/// Создает уникальное значение, последовательно проверяя уникальность и изменяя его
	/// </summary>
	/// <param name="initialValue">Начальное значение</param>
	/// <param name="check">Метод проверки уникальности</param>
	/// <param name="mutate">Метод изменения значения, где на вход подается значение счетчика цикла</param>
	/// <param name="from">Начало цикла подбора</param>
	/// <param name="to">Конец цикла подбора. При достижении этого значения будет возвращено default(TValue)</param>
	/// <returns>Первое значение прошедшее проверку на уникальность или default(TValue)</returns>
	public static TValue Generate<TValue>(
		TValue initialValue, 
		[InstantHandle] Func<TValue, bool> check, 
		[InstantHandle] Func<int, TValue> mutate,
		int from = 2, int to = int.MaxValue)
	{
		bool Check(TValue value)
			=> check == null || check(value);
			
		if (Check(initialValue))
			return initialValue;
			
		for (var i = from; i < to; i++)
		{
			var newValue = mutate(i);
			if (Check(newValue))
				return newValue;
		}
		return default;
	}
	/// <summary>
	/// Асинхронно создает уникальное значение, последовательно проверяя уникальность и изменяя его
	/// </summary>
	/// <param name="initialValue">Начальное значение</param>
	/// <param name="check">Асинхронный метод проверки уникальности</param>
	/// <param name="mutate">Асинхронный метод изменения значения, где на вход подается значение счетчика цикла</param>
	/// <param name="from">Начало цикла подбора</param>
	/// <param name="to">Конец цикла подбора. При достижении этого значения будет возвращено default(TValue)</param>
	/// <returns>Первое значение прошедшее проверку на уникальность или default(TValue)</returns>
	public static async Task<TValue> GenerateAsync<TValue>(
		TValue initialValue, 
		[InstantHandle] Func<TValue, Task<bool>> check, 
		[InstantHandle] Func<int, Task<TValue>> mutate,
		int from = 2, int to = int.MaxValue)
	{
		async Task<bool> Check(TValue value)
			=> check == null || await check(value).NoCtx();
			
		if (await Check(initialValue).NoCtx())
			return initialValue;
			
		for (var i = from; i < to; i++)
		{
			var newValue = await mutate(i).NoCtx();
			if (await Check(newValue).NoCtx())
				return newValue;
		}
		return default;
	}
}
