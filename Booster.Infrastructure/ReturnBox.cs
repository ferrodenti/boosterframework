using JetBrains.Annotations;

#nullable disable

namespace Booster;

[PublicAPI]
public class ReturnBox<T, TError>
{
	public readonly T Result;
	public readonly TError Error;
	public readonly bool Success = true;

	public ReturnBox(T result)
		=> Result = result;

	public ReturnBox(TError error)
	{
		Error = error;
		Success = false;
	}

	public bool IsSuccess(out T result, out TError error)
	{
		error = Error;
		result = Result;
		return Success;
	}

	public bool IsError(out TError error, out T result)
	{
		error = Error;
		result = Result;
		return !Success;
	}

	public bool IsSuccess(out T result)
	{
		result = Result;
		return Success;
	}

	public bool IsError(out TError error)
	{
		error = Error;
		return !Success;
	}

	public static implicit operator bool(ReturnBox<T, TError> box)
		=> box.Success;

	public static implicit operator TError(ReturnBox<T, TError> box)
		=> box.Error;

	public static implicit operator ReturnBox<T, TError>(TError value)
		=> new(value);

	public static implicit operator T(ReturnBox<T, TError> box)
		=> box.Result;

	public static implicit operator ReturnBox<T, TError>(T value)
		=> new(value);
}