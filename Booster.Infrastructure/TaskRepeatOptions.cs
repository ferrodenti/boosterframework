using System.Threading;
using Booster.Log;
using Booster.Reflection;
using JetBrains.Annotations;

namespace Booster;
#nullable enable

[PublicAPI]
public class TaskRepeatOptions
{
	public TypeEx? ExceptionType;
	public bool Throw;
	public int Atempts = 5;
	public int TotalDelay = 1000;
	public bool ExpDelay = true;
	public ILog? Log;
	public CancellationToken CancellationToken = CancellationToken.None;
	public static readonly TaskRepeatOptions Default = new();
	public static readonly TaskRepeatOptions DefaultThrow = new() {Throw = true};

	public static readonly TaskRepeatOptions Fast = new()
	{
		Atempts = 1,
		TotalDelay = 1,
		ExpDelay = false
	};
	public static readonly TaskRepeatOptions FastThrow = new()
	{
		Atempts = 1,
		TotalDelay = 1,
		ExpDelay = false,
		Throw = true
	};
}