using System;
using System.Runtime.CompilerServices;
using Booster.Collections;
using Booster.Helpers;
using JetBrains.Annotations;

#nullable enable

namespace Booster;

[PublicAPI]
public struct KnownFolder
{
	readonly int _value;

	public string Name => _names.TryGetValue(_value, out var name) ? name ?? "" : $"{Unknown}({_value})";
	
	KnownFolder(int value, [CallerMemberName] string? name = null)
	{
		_value = value;
		_names.Add(value, name);
	}
	
	public override bool Equals(object obj)
		=> obj is KnownFolder b && b._value == _value;

	public override int GetHashCode()
		=> _value;

	public override string ToString()
		=> Name;

	public bool IsUnknown => !_names.ContainsKey(_value);
	
	static readonly ListDictionary<int, string?> _names = new();

	public static KnownFolder Unknown = new(0);
	public static KnownFolder Home = new(1);
	public static KnownFolder Documents = new(2);
	public static KnownFolder Downloads = new(3);
	public static KnownFolder Music = new(4);
	public static KnownFolder Pictures = new(5);
	public static KnownFolder SavedGames = new(6);

	static readonly Lazy<ListDictionary<KnownFolder, Guid>> _windowsGuids = new(
		() => new ListDictionary<KnownFolder, Guid>
		{
			{ Documents, new Guid("FDD39AD0-238F-46AF-ADB4-6C85480369C7") },
			{ Downloads, new Guid("374DE290-123F-4565-9164-39C4925E467B") },
			{ Music, new Guid("4BD8D571-6D19-48D3-BE97-422220080E43") },
			{ Pictures, new Guid("33E28130-4E1E-4676-835A-98395C3BC3BB") },
			{ SavedGames, new Guid("4C5C32FF-BB9D-43B0-B5B4-2D72E54EAAA4") }
		});


	bool TryFindWindowsFolder(out string result)
	{
		if (_windowsGuids.Value.TryGetValue(this, out var guid))
		{
			result = WinApi.SHGetKnownFolderPath(guid, 0);
			return result.IsSome();
		}

		result = null!;
		return false;
	}

	bool TryFindUnixFolder(OSType osType, out string result)
	{
		if (_value == Home._value)
		{
			var res = osType.IsLinux
				? Environment.GetEnvironmentVariable("HOME")
				: Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%");

			return res.Is(out result);
		}

		if (!IsUnknown)
		{
			result = null!;
			return false;
		}

		result = System.IO.Path.Combine(Home.GetPath(osType)!, Name);

		return true;
	}


	
	public string? GetPath(OSType osType)
	{
		if (osType.IsWindows)
		{
			if (TryFindWindowsFolder(out var result))
				return result;

			return null;
		}

		if (TryFindUnixFolder(osType, out var result2))
			return result2;

		return null;
	}

	SyncLazy<string?> _path = SyncLazy<string?>.Create<KnownFolder>(self => self.GetPath(OSType.CurrentOS));

	public string? Path => _path.GetValue(this);
}
