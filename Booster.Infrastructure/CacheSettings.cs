﻿using System;

namespace Booster;

public class CacheSettings
{
	public bool EnableCaching = true;
	public bool CacheAbsent { get; set; }
	public bool Prefetch = false;
	public bool IgnoreCase = true;
	
	public TimeSpan SlidingExpiration = TimeSpan.FromMinutes(10);
	public TimeSpan AbsoluteExpiration;
	public TimeSpan GCInterval;
	public int GCStart = 1000;
	public int AggressiveGCStart = 10000;

	public DateTime GetAbsoluteExpirationFromNow() 
		=> AbsoluteExpiration.IsBetweenZeroAndMax() ? DateTime.Now + AbsoluteExpiration : DateTime.MaxValue;


	public CacheSettings Clone()
		=> (CacheSettings) MemberwiseClone();
}