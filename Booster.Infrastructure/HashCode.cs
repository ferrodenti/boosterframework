using System;
using System.Collections;
using System.Text;
using Booster.Interfaces;
using JetBrains.Annotations;

#nullable enable

namespace Booster;

[PublicAPI]
public struct HashCode : IIntValue
{
	uint _value;
	int _step;

	public HashCode(int seed)
	{
		_value = (uint)seed;
		_step = 0;
	}
	public HashCode(string str)
	{
		_value = 0;
		_step = 0;

		Append(str);
	}
	public HashCode(IEnumerable ie)
	{
		_value = 0;
		_step = 0;

		if (ie is string)
			Append(ie);
		else
			AppendEnumerable(ie);
	}
		
	public HashCode(params object?[] values)
	{
		_value = 0;
		_step = 0;

		foreach (var o in values)
			Append(o);
	}


	public HashCode Append(object? obj)
		=> Append(obj?.GetHashCode() ?? 0);

	public HashCode Append(int hash)
	{
		unchecked
		{
			_value += (uint)((hash << (_step * 8)) + (hash >> (sizeof(int) - _step * 8)));
			_step = (_step + 1) % sizeof(int);
		}

		return this;
	}

	public HashCode AppendEnumerable(IEnumerable obj)
	{
		foreach (var o in obj)
			Append(o?.GetHashCode());

		return this;
	}
	
	public HashCode AppendEnumerable(IEnumerable obj, IEqualityComparer equalityComparer)
	{
		foreach (var o in obj)
		{
			var hash = o != null ? equalityComparer.GetHashCode(o) : 0;
			Append(hash);
		}

		return this;
	}
	public int Value => (int)_value;

	public override string ToString()
		=> _value.ToString("X");

	public override int GetHashCode()
		// ReSharper disable once NonReadonlyMemberInGetHashCode
		=> (int)_value;

	public string ToHexString()
	{
		var sb = new StringBuilder();

		foreach (var b in BitConverter.GetBytes(_value))
			sb.Append($"{b:X2}");

		return sb.ToString();
	}

	public override bool Equals(object obj)
	{
		if (obj is HashCode code)
			return code.Value == Value;

		return true;
	}

	public static implicit operator HashCode(int id)
		=> new(id);
		
	public static implicit operator int(HashCode one)
		=> one.Value;
		
	public static bool operator ==(HashCode a, HashCode b)
		=> a.Value == b.Value;
		
	public static bool operator !=(HashCode a, HashCode b)
		=> a.Value != b.Value;

	public static HashCode operator +(HashCode a, object b)
	{
		a.Append(b); 
		return a;
	}
}