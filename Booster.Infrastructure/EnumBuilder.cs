﻿using System.Collections;
using System.Text;
using JetBrains.Annotations;

#nullable enable

namespace Booster;

[PublicAPI]
public class EnumBuilder
{
	readonly StringBuilder _builder = new();
	bool _resetComma;

	// ReSharper disable FieldCanBeMadeReadOnly.Global
	public string? Opener;
	public string? Closer;
	public string? Comma;
	public string? Empty;
	// ReSharper restore FieldCanBeMadeReadOnly.Global

	public bool IsEmpty { get; private set; } = true;
	public bool IsSome => !IsEmpty;

	public int Length => _builder.Length;

	public EnumBuilder(string? comma = null, string? opener = null, string? closer = null, string? empty = null)
	{
		Comma = comma;
		Opener = opener;
		Closer = closer;
		Empty = empty;
	}

	public void Clear()
	{
		_builder.Clear();
		IsEmpty = true;
		_resetComma = false;
	}

	public void AppendRange(IEnumerable? ie)
	{
		if(ie != null)
			foreach (var o in ie)
				Append(o);
	}

	public void Append(object? obj)
	{
		if (!IsEmpty)
		{
			if (_resetComma)
				_resetComma = false;
			else
				_builder.Append(Comma);
		}

		if (obj != null)
			_builder.Append(obj);

		IsEmpty = false;
	}
		
	public void Append(string? str)
		=> Append((object?)str);

	public void AppendIfSome(string? str)
	{
		if (!string.IsNullOrWhiteSpace(str))
			Append(str);
	}


	public void AppendNoComma(object? obj)
	{
		_builder.Append(obj);
		IsEmpty = false;
	}

	public void ResetComma()
		=> _resetComma = true;

	public void AppendNoComma(string? str)
		=> AppendNoComma((object?)str);

	public override string ToString()
	{
		if (IsEmpty)
			return Empty ?? "";

		return $"{Opener}{_builder}{Closer}";
	}

	public static implicit operator string(EnumBuilder enumBuilder)
		=> enumBuilder.ToString();

	public static string Join(string? comma, params string?[] args)
	{
		var str = new EnumBuilder(comma);
		foreach(var s in args)
			if (s.IsSome())
				str.Append(s);

		return str;
	}
}