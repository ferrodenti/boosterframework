namespace Booster.Interfaces;

public interface ICtxLocal
{
	bool NoDispose { get; }
}