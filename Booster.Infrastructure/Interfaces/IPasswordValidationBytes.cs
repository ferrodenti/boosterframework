using System;
using System.Security.Cryptography;
using System.Text;
using JetBrains.Annotations;

namespace Booster.Interfaces;

public interface IPasswordValidationBytes
{
	byte[] PasswordHash { get; set; }
	byte[] PasswordSalt { get; set; }
}

// ReSharper disable once InconsistentNaming
public static class IPasswordValidationBytesExpander
{
	[PublicAPI]
	public static bool IsPasswordCorrect(this IPasswordValidationBytes passwordValidation, string password)
	{
		if (password.IsSome() &&
		    passwordValidation.PasswordSalt != null &&
		    passwordValidation.PasswordHash != null)
		{
			var hash = HashPassword(password, passwordValidation.PasswordSalt);
			var orig = passwordValidation.PasswordHash;

			return hash.CompareConstantTime(orig) == 0;
		}

		return false;
	}

	[PublicAPI]
	public static void SetPassword(this IPasswordValidationBytes passwordValidation, string password)
	{
		var salt = Rnd.Bytes(16);
		var hash = HashPassword(password, salt);

		passwordValidation.PasswordHash = hash;
		passwordValidation.PasswordSalt = salt;
	}

	static byte[] HashPassword(string password, byte[] salt)
	{
		var utf8 = Encoding.UTF8;
		var clearText = password.ToCharArray();
		var data = new byte[salt.Length + utf8.GetMaxByteCount(clearText.Length)];
		byte[] hash;

		try
		{
			Array.Copy(salt, 0, data, 0, salt.Length);

			var byteCount = utf8.GetBytes(clearText, 0, clearText.Length, data, salt.Length);

			using var alg = SHA256.Create();
			hash = alg.ComputeHash(data, 0, salt.Length + byteCount);
		}
		finally
		{
			Array.Clear(clearText, 0, clearText.Length);
			Array.Clear(data, 0, data.Length);
		}

		return hash;
	}
}