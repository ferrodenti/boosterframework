using System.Collections;
using System.Threading.Tasks;
#if !NETSTANDARD2_1
using Booster.AsyncLinq;
#else
using System.Collections.Generic;
#endif

namespace Booster.Interfaces
{
	public interface IReorderer
	{
		Task ReorderAfter(IAsyncEnumerable<IReorderable> collection, IReorderable target, IReorderable after);
		void ReorderAfter(IEnumerable collection, IReorderable target, IReorderable after);
	}
}