using System;

namespace Booster.Interfaces;

/// <summary>
/// Интерфейс компонента, работу которого можно запускать и останавливать
/// </summary>
public interface IStartStop : IDisposable
{
	bool Start();
	bool Stop();

	bool IsWorking { get; set; }
}