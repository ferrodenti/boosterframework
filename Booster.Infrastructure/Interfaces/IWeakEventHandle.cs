namespace Booster.Interfaces;

public interface IWeakEventHandle
{
	public void Unsubscribe();
}