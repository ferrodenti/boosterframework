namespace Booster.Interfaces;

public interface IUnregisterInstance
{
	void UnregisterInstance();
}