namespace Booster.Interfaces;

public interface IIntValue
{
	int Value { get; }
}