using Booster.Synchronization;

#nullable enable

namespace Booster.Interfaces;

public interface IAsyncLockable
{
	AsyncLock AsyncLock { get; }
}