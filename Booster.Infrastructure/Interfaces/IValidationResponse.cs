using System;
using System.Linq.Expressions;

#nullable enable

namespace Booster.Interfaces;

public interface IValidationResponse
{
	bool IsOk { get; }
	void Error(string parameterName, string message);
	void Error<T>(T obj, Expression<Func<T, object?>> accessor, string message); //TODO: Theese methods belongs to some "validation context" with GetValue(Func) method as well
}