namespace Booster.Interfaces;

public interface IReorderable
{
	int Order { get; set; }
}