using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Booster.Interfaces;

public interface IAsyncLockableDictionary<TKey, TValue> : IDictionary<TKey, TValue>, IAsyncLockable
{
}

public static class AsyncLockableDictionaryExpander
{
	public static TValue GetOrAdd<TKey, TValue>(this IAsyncLockableDictionary<TKey, TValue> dict, TKey key, [InstantHandle] Func<TKey, TValue> creator)
	{
		if (!dict.TryGetValue(key, out var res))
			using (dict.Lock())
				if (!dict.TryGetValue(key, out res))
					dict[key] = res = creator(key);

		return res;
	}

	public static async Task<TValue> GetOrAddAsync<TKey, TValue>(this IAsyncLockableDictionary<TKey, TValue> dict, TKey key, [InstantHandle] Func<TKey, Task<TValue>> creator)
	{
		if (dict.TryGetValue(key, out var res)) return res;
		
		using var _ = await dict.LockAsync().NoCtx();

		if (!dict.TryGetValue(key, out res))
			dict[key] = res = await creator(key).NoCtx();

		return res;
	}
}