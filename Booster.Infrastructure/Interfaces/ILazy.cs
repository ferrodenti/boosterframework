﻿using System;
using Booster.Reflection;

namespace Booster.Interfaces;

public interface ILazy
{
	TypeEx ValueType { get; }
	bool IsValueFaulted { get; }
	bool IsValueCreated { get; }
		
	event EventHandler Reseted;
		
	void Reset();
}