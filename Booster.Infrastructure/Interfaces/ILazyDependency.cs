﻿using System;
using Booster.Reflection;

namespace Booster.Interfaces;

public interface ILazyDependency
{
	void Add<T>(Func<T, bool> condition = null);
	void Add(object entity);
	void Add(TypeEx entityType, Func<object, bool> condition = null);
	void Add(ILazy lazy);
	void Add(ILazyDependency other);

	event EventHandler Changed;
		
	void Reset();
}