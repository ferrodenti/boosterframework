﻿using System;
using Booster.Reflection;

namespace Booster.Interfaces;

public interface IDependencySource
{
	IWeakEventHandle Add(object target, object entity, Action<object, EventArgs> callback);
	IWeakEventHandle Add(object target, TypeEx entityType, Action<object, EventArgs> callback);
}