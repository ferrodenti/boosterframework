namespace Booster.Interfaces;

public interface ISchemaAttribute
{
	void ApplySettings(IDynamicSettings settings);
}