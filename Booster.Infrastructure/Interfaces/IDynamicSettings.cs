using System.Collections.Generic;

namespace Booster.Interfaces;

public interface IDynamicSettings : IEnumerable<KeyValuePair<string, IDynamicParameter>>
{
	T Get<T>(object target, string key, T @default = default);
	T Get<T>(object target, string key, out bool isConstant, T @default = default);

	void Add(string key, string value);
	void Add(string key, IDynamicParameter value);
	void Add(string key, object value);

	bool ContainsKey(string key);

	object this[object target, string key] { get; }
	IDynamicParameter this[string key] { get; set; }

	IDictionary<string, object> GetValues(object target);
	IDictionary<string, object> GetChangedValues(object target, IDictionary<string, object> old, bool skipStatics = false);
}