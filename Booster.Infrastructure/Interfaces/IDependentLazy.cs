﻿namespace Booster.Interfaces;

public interface IDependentLazy : ILazy
{
	ILazyDependency GetDependency(bool create = true);
}