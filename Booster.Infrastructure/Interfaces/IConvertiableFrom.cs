namespace Booster.Interfaces;

public interface IConvertiableFrom<T> : IEmptyState
{
	T Value { get; set; }
}