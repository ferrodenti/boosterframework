using System;
using System.Threading;
using System.Threading.Tasks;

#nullable enable

namespace Booster.Interfaces;

public static class AsyncLockableExpander
{
	public static IDisposable Lock(this IAsyncLockable lockable)
		=> lockable.AsyncLock.Lock();

	public static Task<IDisposable> LockAsync(this IAsyncLockable lockable)
		=> lockable.AsyncLock.LockAsync();

	public static IDisposable? Lock(this IAsyncLockable lockable, int timeout, CancellationToken token = default)
		=> lockable.AsyncLock.Lock(timeout, token);

	public static Task<IDisposable?> LockAsync(this IAsyncLockable lockable, int timeout, CancellationToken token = default)
		=> lockable.AsyncLock.LockAsync(timeout, token);

}