using System;

namespace Booster.Interfaces;

public interface IScheduleProvider
{
	DateTime GetNext(DateTime previous);
}