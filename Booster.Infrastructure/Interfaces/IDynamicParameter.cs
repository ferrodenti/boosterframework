﻿using Booster.Reflection;

namespace Booster.Interfaces;

public interface IDynamicParameter
{
	bool IsConstant { get; }
	bool IsStatic { get; }
	TypeEx ValueType { get; }

	//object Evaluate(object target, Func<object,string, object> dataAccessor = null);
	object Evaluate(object target);
}