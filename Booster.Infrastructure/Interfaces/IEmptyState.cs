namespace Booster.Interfaces;

public interface IEmptyState
{
	bool IsEmpty { get; }
}