using Booster.Reflection;

namespace Booster.Interfaces;

public interface IDataAccessor
{
	string Name { get; }
	TypeEx ValueType { get; }
	bool CanRead { get; }
	bool CanWrite { get; }
	object GetValue(object target);
	bool SetValue(object target, object value);
}