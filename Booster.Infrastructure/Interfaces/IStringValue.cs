namespace Booster.Interfaces;

public interface IStringValue
{
	string Value { get; }
}