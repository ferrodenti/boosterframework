using System;
using System.Collections.Generic;
using System.Globalization;
using Booster.Reflection;

namespace Booster.Ranges;

public class NumericHelper<T> : INumericHelper<T>
{
	public static NumericHelper<T> Instance = new();
	static EqualityComparer<T> _equalityComparer = EqualityComparer<T>.Default;
		
	public T MaxValue { get; }
	public T MinValue { get; }
	public T DefaultValue { get; }

	Method _tryParseMethod;
	Method _tryParseExactMethod;
		
	public NumericHelper()
	{
		TypeEx type = typeof(T);

		var prop = type.FindProperty(new ReflectionFilter("MinValue")
		{
			IsStatic = true, 
			ReturnType = typeof(T), 
			Access = AccessModifier.Public
		});
		if (prop != null)
			MinValue = (T) prop.GetValue(null);
			
		prop = type.FindProperty(new ReflectionFilter("MaxValue")
		{
			IsStatic = true, 
			ReturnType = typeof(T), 
			Access = AccessModifier.Public
		});
		if (prop != null)
			MaxValue = (T) prop.GetValue(null);

		_tryParseExactMethod = type.FindMethod(new ReflectionFilter("TryParseExact") //TODO: будет ли здесь искаться метод с out?
		{
			Access = AccessModifier.Public,
			ReturnType = typeof(bool),
			IsStatic = true,
			Parameters = new []
			{
				typeof(string),
				typeof(string),
				typeof(IFormatProvider),
				typeof(DateTimeStyles),
				typeof(T)
			}
		});

		_tryParseMethod = type.FindMethod(new ReflectionFilter("TryParse")
		{
			Access = AccessModifier.Public,
			ReturnType = typeof(bool),
			IsStatic = true,
			Parameters = new []
			{
				typeof(string),
				typeof(NumberStyles),
				typeof(IFormatProvider),
				typeof(T)
			}
		});
	}

	public bool Equals(T x, T y)
		=> _equalityComparer.Equals(x, y);

	public int GetHashCode(T obj)
		=> _equalityComparer.GetHashCode(obj);
		
	public bool Greater(T a, T b)
	{
		if (a is IComparable comp)
			return comp.CompareTo(b) > 0;

		throw new NotImplementedException();
	}

	public bool Less(T a, T b)
	{
		if (a is IComparable comp)
			return comp.CompareTo(b) < 0;
			
		throw new NotImplementedException();
	}

	public bool GreaterOrEqual(T a, T b)
	{
		if (a is IComparable comp)
			return comp.CompareTo(b) >= 0;
			
		throw new NotImplementedException();
	}

	public bool LessOrEqual(T a, T b)
	{
		if (a is IComparable comp)
			return comp.CompareTo(b) <= 0;
			
		throw new NotImplementedException();
	}

	public bool TryParse(
		string value, 
		out T result, 
		string format = null,
		IFormatProvider formatProvider = null, 
		int styles = 0)
	{
		formatProvider ??= CultureInfo.CurrentCulture;
			
		var box = new Box<T>();
		if (_tryParseExactMethod != null)
		{
			if (Equals(_tryParseExactMethod.Invoke(null, value, //TODO: будет ли тут так вызываться метод с out?
				    format ?? "dd.MM.yyyy", 
				    formatProvider,
				    (DateTimeStyles)styles, 
				    box), true))
			{
				result = box.Value;
				return true;
			}
		}
		else if (_tryParseMethod != null)
            if (Equals(_tryParseMethod.Invoke(null, value, 
                    (NumberStyles)styles,
                    formatProvider,
                    box), true))
            {
                result = box.Value;
                return true;
            }

        result = default;
		return false;
	}
}