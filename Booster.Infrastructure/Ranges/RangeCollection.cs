using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace Booster.Ranges;

[PublicAPI]
public class RangeCollection<T> : ICollection<Range<T>>
{
	readonly List<Range<T>> _inner = new();

	public int Count => _inner.Count;
	public bool IsReadOnly => false;
		
	public RangeCollection()
	{
	}

	public RangeCollection(IEnumerable<Range<T>> ranges)
		=> AddRange(ranges);

	public void AddRange(IEnumerable<Range<T>> ranges)
	{
		foreach (var range in ranges)
			Add(range);
	}
		
	public IEnumerator<Range<T>> GetEnumerator()
		=> _inner.GetEnumerator();

	IEnumerator IEnumerable.GetEnumerator()
		=> GetEnumerator();

	public void Add(Range<T> item)
	{
		if (item.IsEmpty)
			return;
			
		var current = item;
		var progress = true;

		while (progress)
		{
			progress = false;
					
			foreach (var range2 in _inner.ToArray())
				if (current.TryCombine(range2, out var combination))
				{
					_inner.Remove(range2);
					current = combination;
					progress = true;
					break;
				}
		}
		_inner.Add(current);
	}

	public void Clear()
		=> _inner.Clear();

	public bool Contains(Range<T> item)
		=> _inner.Contains(item);

	public bool Wraps(Range<T> item)
		=> _inner.Any(i => i.Contains(item));

	public IEnumerable<Range<T>> Intersect(Range<T> range)
	{
		foreach(var item in _inner)
			if (item.TryIntersect(range, out var i))
				yield return i;
	}

	public void CopyTo(Range<T>[] array, int arrayIndex)
		=> _inner.CopyTo(array, arrayIndex);

	public bool Remove(Range<T> item)
		=> _inner.Remove(item);
}