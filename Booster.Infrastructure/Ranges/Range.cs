using System;
using System.Linq;
using JetBrains.Annotations;

namespace Booster.Ranges;

[PublicAPI]
public readonly struct Range<T>
{
	static readonly INumericHelper<T> _numericHelper = NumericHelper<T>.Instance;

	public T From { get; }
	public T To { get; }

	public bool IsSome => _numericHelper.LessOrEqual(From, To);
	public bool IsEmpty => _numericHelper.Greater(From, To);
	public bool IsPoint => _numericHelper.Equals(From, To);

	public bool IsContinuum => _numericHelper.Equals(From, _numericHelper.MinValue) &&
							   _numericHelper.Equals(To, _numericHelper.MaxValue);

	public Range(T from, T to)
	{
		From = from;
		To = to;
	}

	public override bool Equals(object obj)
		=> obj is Range<T> b && Equals(b);

	public bool Equals(Range<T> other)
		=> _numericHelper.Equals(From, other.From) &&
		   _numericHelper.Equals(To, other.To);

	public override int GetHashCode()
	{
		unchecked
		{
			return (_numericHelper.GetHashCode(From) * 397) ^ _numericHelper.GetHashCode(To);
		}
	}

	public bool Contains(T point)
		=> _numericHelper.LessOrEqual(From, point) && _numericHelper.GreaterOrEqual(To, point);

	public bool Contains(Range<T> other)
		=> Contains(other.From) && Contains(other.To);

	public bool TryCombine(Range<T> other, out Range<T> result)
	{
		result = default;

		if (!IsEmpty || other.IsEmpty)
			return false;

		if (Contains(other.From))
		{
			result = Contains(other.To) ? this : new Range<T>(From, other.To);
			return true;
		}

		if (other.Contains(From))
		{
			result = other.Contains(To) ? other : new Range<T>(other.From, To);
			return true;
		}

		return false;
	}

	public bool TryIntersect(Range<T> other, out Range<T> result)
	{
		result = default;

		if (IsSome && other.IsSome)
		{
			var a = _numericHelper.Min(other.From, other.To);
			var b = _numericHelper.Max(other.From, other.To);
			var r = new Range<T>(a, b);
			if (r.IsSome)
			{
				result = r;
				return true;
			}
		}

		return false;
	}

	public bool TryParse(string value, out Range<T> result, string format = null, string separator = "-", IFormatProvider formatProvider = null, int styles = 0)
	{
		result = default;

		if (value.IsEmpty())
			return false;

		var segments = value.SplitNonEmpty(separator).Select(s => s.Trim()).ToArray();
		switch (segments.Length)
		{
		case 1:
			if (_numericHelper.TryParse(segments[0], out var val, format, formatProvider, styles))
			{
				result = new Range<T>(val, val);
				return true;
			}

			break;
		case 2:

			bool TryParseT(string s, T def, out T res)
			{
				if (s.IsEmpty())
				{
					res = def;
					return true;
				}

				return _numericHelper.TryParse(s, out res, format, formatProvider, styles);
			}

			if (TryParseT(segments[0], _numericHelper.MinValue, out val) &&
				TryParseT(segments[1], _numericHelper.MaxValue, out var val2))
			{
				result = new Range<T>(val, val2);
				return true;
			}

			break;
		}

		return false;
	}
}