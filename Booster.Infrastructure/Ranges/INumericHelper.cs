using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Booster.Ranges;

[PublicAPI]
public interface INumericHelper<T> : IEqualityComparer<T>
{
	T MaxValue { get; }
	T MinValue { get; }
	T DefaultValue { get; }
		
	bool Greater(T a, T b);
	bool Less(T a, T b);
	bool GreaterOrEqual(T a, T b);
	bool LessOrEqual(T a, T b);

	bool TryParse(string value, out T result, 
		string format = null, 
		IFormatProvider formatProvider = null,
		int styles = 0);
}

public static class NumericHelperExpander
{
	public static T Min<T>(this INumericHelper<T> helper, T a, T b)
		=> helper.Less(a, b) ? a : b;
		
	public static T Max<T>(this INumericHelper<T> helper, T a, T b)
		=> helper.Greater(a, b) ? a : b;
}