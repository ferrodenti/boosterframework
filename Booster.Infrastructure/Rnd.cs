﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JetBrains.Annotations;
#if !DNX
using System.Security.Cryptography;
#endif

#nullable enable

namespace Booster;

public static class Rnd
{
	static readonly RNGCryptoServiceProvider _global = new();
	[ThreadStatic] static Random? _local;

	public static Random Local
	{
		get
		{
			var inst = _local;
			if (inst == null)
			{
				var buffer = new byte[4];
				_global.GetBytes(buffer);
				_local = inst = new Random(BitConverter.ToInt32(buffer, 0));
			}

			return inst;
		}
	}

	public static bool Bool(int fromChance = 2)
		=> Local.Next() % fromChance == 0;

	public static byte Byte()
		=> (byte) Local.Next();

	public static byte Byte(byte max)
		=> (byte) Local.Next(max);

	public static byte Byte(byte min, byte max)
		=> (byte) Local.Next(min, max);

	public static byte[] Bytes(int count)
	{
		var res = new byte[count];
		Local.NextBytes(res);
		return res;
	}

	public static int Int()
		=> Local.Next();

	public static int Int(int max)
		=> Local.Next(max);

	public static int Int(int min, int max)
		=> Local.Next(min, max);

	public static long Long()
	{
		var buf = new byte[8];
		Local.NextBytes(buf);
		return BitConverter.ToInt64(buf, 0);
	}

	public static long Long(long max)
	{
		var buf = new byte[8];
		Local.NextBytes(buf);
		return Math.Abs(BitConverter.ToInt64(buf, 0) % max);
	}

	public static long Long(long min, long max)
	{
		var buf = new byte[8];
		Local.NextBytes(buf);
		var longRand = BitConverter.ToUInt64(buf, 0);
		var d = (ulong) max - (ulong) min;
		return (long) (longRand % d) + min;
	}

	[PublicAPI]
	public static double Double()
		=> Local.NextDouble();

	public static double Double(double max)
		=> Local.NextDouble() * max;

	public static double Double(double min, double max)
	{
		var sample = Local.NextDouble();
		return min * sample + max * (1 - sample);
	}

	public static float Float()
		=> (float) Local.NextDouble();

	public static float Float(float max)
		=> (float) Local.NextDouble() * max;

	public static float Float(float min, float max)
	{
		// ReSharper disable once CompareOfFloatsByEqualityOperator
		if (min == max)
			return min;

		float res;
		do
		{
			var sample = (float) Local.NextDouble();
			res = min * sample + max * (1 - sample);
		} while (res < min || res >= max);

		return res;
	}

	public static decimal Decimal()
	{
		var scale = (byte) Local.Next(29);
		var sign = Local.Next(2) == 1;
		return new decimal(Local.Next(int.MinValue, int.MaxValue),
			Local.Next(int.MinValue, int.MaxValue),
			Local.Next(int.MinValue, int.MaxValue),
			sign,
			scale);
	}

	public static decimal Decimal(decimal max)
		=> Decimal(decimal.Zero, max);

	public static decimal Decimal(decimal min, decimal max)
	{
		const int maxScale = 28;
		var sample = 1m;
		//After ~200 million tries this never took more than one attempt but it is possible to generate combinations of a, b, and c with the approach below resulting in a sample >= 1.
		while (sample >= 1)
		{
			var a = Local.Next(int.MinValue, int.MaxValue);
			var b = Local.Next(int.MinValue, int.MaxValue);
			//The high bits of 0.9999999999999999999999999999m are 542101086.
			var c = Local.Next(542101087);
			sample = new decimal(a, b, c, false, maxScale);
		}

		return min * sample + max * (1 - sample);
	}

	static readonly ConcurrentDictionary<string, char[]> _stringRangesCache = new();

	/// <summary>
	/// Гененирует случайную строку заданного размера из символов в заданных диапазонах
	/// </summary>
	/// <param name="len">Длина строки</param>
	/// <param name="ranges">Строка содержащая диапазоны символов, пример: azAZ09</param>
	/// <returns>Случайная строка</returns>
	/// <exception cref="ArgumentException">Возникает в случае если строка диапазонов имеет неверный формат</exception>
	public static string String(int len, string ranges)
	{
		if (len < 0)
			return null!;

		if (string.IsNullOrEmpty(ranges))
			throw new ArgumentException("Ожидается непустая строка", nameof(ranges));

		var symbols = _stringRangesCache.GetOrAdd(ranges, _ =>
		{
			var result = new HashSet<char>();


			if (ranges.Length % 2 == 1)
				throw new ArgumentException("Ожидается строка длиной кратной 2", nameof(ranges));

			for (var i = 0; i < ranges.Length; i += 2)
			{
				var from = ranges[i];
				var to = ranges[i + 1];
				if (from > to)
					throw new ArgumentException($"Неверный интервал: cимвол '{from}' стоит после символа '{to}'", nameof(ranges));

				for (var ch = from; ch <= to; ch++)
					result.Add(ch);
			}

			return result.ToArray();
		});

		var str = new StringBuilder(len);
		for (var j = 0; j < len; j++)
			str.Append(Element(symbols));

		return str.ToString();
	}

	[Obsolete("Используй перегрузку метода с параметрами (int len, string ranges)")]
	public static string String(int len, (char, char)[] ranges)
	{
		var key = new StringBuilder(ranges.Length * 2);
		foreach (var (from, to) in ranges)
		{
			key.Append(from);
			key.Append(to);
		}

		return String(len, key.ToString());
	}

	public static string String(int len)
		=> String(len, "azAZаяАЯ09");

	public static T Element<T>(T[] arr)
		=> arr[Int(arr.Length)];

	public static T Element<T>(List<T> arr)
		=> arr[Int(arr.Count)];
}