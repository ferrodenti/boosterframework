
namespace Booster;

public class Box<T>
{
	public T Value { get; set; }

	public Box() { }
	public Box(T value)
		=> Value = value;

	public override int GetHashCode()
	{
		unchecked
		{
			var i = typeof (T).GetHashCode();
			// ReSharper disable once NonReadonlyMemberInGetHashCode
			var v = Value;
			if (v != null)
				i += v.GetHashCode();

			return i;
		}
	}

	public override bool Equals(object obj)
	{
		if (obj is Box<T> b)
			return Value != null ? Equals(Value, b.Value) : b.Value == null;

		return false;
	}

	public override string ToString()
		=> Value?.ToString() ?? base.ToString();

	public static implicit operator T(Box<T> box)
		=> box.With(b => b.Value);

	public static implicit operator Box<T>(T value)
		=> new(value);
}