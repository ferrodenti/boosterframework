using System;
using System.Text;

namespace Booster.Helpers;

public static class Base36
{
	static char MakeFunky(char c)
		=> char.IsLetter(c) && Rnd.Bool() ? char.ToUpper(c) : c;

	static readonly char[] _base36Chars = "0123456789abcdefghijklmnopqrstuvwxyz".ToCharArray();
	public static string Convert(int value, int size = int.MaxValue, bool funky = false)
	{
		var res = new StringBuilder();
		var hash = (uint)value;
		for (var i = 0; hash != 0 && i < size; i++)
		{
			var c = _base36Chars[hash%_base36Chars.Length];

			if (funky)
				c = MakeFunky(c);

			res.Append(c);

			hash /= (uint)_base36Chars.Length;
		}
		return res.ToString();
	}

	public static string GenerateUnique(int value, int size, bool funky, Func<string, bool> isUnique)
	{
		var res = Convert(value, size, funky);

		while (!isUnique(res))
			res = res.Insert(Rnd.Int(0, res.Length), Convert(Rnd.Int(36), 1, true));

		return res;
	}
}