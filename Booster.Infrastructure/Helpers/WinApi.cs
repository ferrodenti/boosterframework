﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using JetBrains.Annotations;
using Microsoft.Win32.SafeHandles;

namespace Booster.Helpers;

[PublicAPI]
public static class WinApi
{
	[DllImport("kernel32.dll", SetLastError = true)]
	static extern uint SetErrorMode(uint mode);
	const uint _semNogpfaulterrorbox = 0x8007;

	public static int DisableErrorBox()
	{
		var prev = (int)SetErrorMode(_semNogpfaulterrorbox);
		var now = (int)SetErrorMode(_semNogpfaulterrorbox);
		return now - prev;
	}

	[DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
	public static extern SafeFileHandle CreateFile(string lpFileName, int dwDesiredAccess, int dwShareMode,
		IntPtr securityAttrs, int dwCreationDisposition, int dwFlagsAndAttributes, IntPtr hTemplateFile);

	[DllImport("Kernel32.dll", SetLastError = true)]
	public static extern uint GetShortPathName(string lpszLongPath, [Out] StringBuilder lpszShortPath, uint cchBuffer);

	[DllImport("user32.dll", SetLastError = true)]
	[return: MarshalAs(UnmanagedType.Bool)]
	public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

	#region Console

	[DllImport("Kernel32.dll", SetLastError = true)]
	public static extern IntPtr GetStdHandle(int nStdHandle);

	[DllImport("kernel32.dll", SetLastError = true)]
	public static extern bool GetConsoleMode(IntPtr hConsoleHandle, out int lpMode);

	[DllImport("kernel32.dll", SetLastError = true)]
	public static extern bool SetConsoleMode(IntPtr hConsoleHandle, int ioMode);

	[DllImport("kernel32.dll", SetLastError = true)]
	[return: MarshalAs(UnmanagedType.Bool)]
	public static extern bool AllocConsole();

	[DllImport("kernel32.dll", SetLastError = true)]
	public static extern IntPtr GetConsoleWindow();

	[DllImport("Kernel32", SetLastError = true)]
	public static extern bool SetConsoleCtrlHandler(HandlerRoutine handler, bool add);
	public delegate bool HandlerRoutine(int ctrlType);

	#endregion
		
	[DllImport("shell32", CharSet = CharSet.Unicode, ExactSpelling = true, PreserveSig = false)]
	public static extern string SHGetKnownFolderPath([MarshalAs(UnmanagedType.LPStruct)] Guid rfid, uint dwFlags, nint hToken = default);
}