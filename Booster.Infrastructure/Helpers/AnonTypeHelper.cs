﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Runtime.CompilerServices;
using Booster.Collections;
using Booster.Reflection;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Helpers;

public class AnonTypeHelper
{
	readonly object _obj;

	Dictionary<string, object?>? _properties;

	public Dictionary<string, object?> Properties => _properties ??= ReadProperties(_obj);

	public object? this[string propertyName]
	{
		get
		{
			Properties.TryGetValue(propertyName, out var value);
			return value;
		}
	}

	public bool TryGetValue<T>(string propertyName, out T value)
	{
		if (Properties.TryGetValue(propertyName, out var obj) && obj is T tobj)
		{
			value = tobj;
			return true;
		}

		value = default!;
		return false;
	}

	public T? Get<T>(string propertyName, T? @default = default)
		=> TryGetValue<T>(propertyName, out var value) ? value : @default;

	public AnonTypeHelper(object @object)
		=> _obj = @object ?? throw new ArgumentNullException(nameof(@object));

	static readonly HybridDictionary<TypeEx, (Method keyGetter, Method valueGetter)> _pairReflection = new();

	[MethodImpl(MethodImplOptions.Synchronized)]
	static (Method keyGetter, Method valueGetter) ReflectPair(TypeEx pairType)
		=> _pairReflection.GetOrAdd(pairType, _ =>
		{
			var keyProp = pairType.FindProperty("Key");
			var valProp = pairType.FindProperty("Value");
			var keyGet = keyProp?.GetMethod;
			var valGet = valProp?.GetMethod;

			if (keyGet == null || valGet == null)
				throw new ReflectionException(pairType, "Unable to reflect Key and Value properties");

			return (keyGet, valGet);
		});

	static IEnumerable<(object? key, object? value)> ReadAnonDictionary(object? obj)
	{
		if (obj == null)
			yield break;

		var dict = TypeEx.Of(obj).FindInterface(typeof (IDictionary<,>));
		if (dict != null || obj is IDictionary)
		{
			Type? pairType = null;
			Method? keyGet = null, valGet = null;

			foreach (var pair in (IEnumerable)obj)
			{
				var type = pair.GetType();
				if (type != pairType) 
					(keyGet, valGet) = ReflectPair(type);

				var key = keyGet!.Invoke(pair);
				var value = valGet!.Invoke(pair);
				yield return (key, value);
			}
			yield break;
		}

		var props = TypeDescriptor.GetProperties(obj);
		foreach (PropertyDescriptor prop in props)
		{
			var value = prop.GetValue(obj);
			yield return (prop.Name, value);
		}
	}

	public static Dictionary<string, object?> ReadProperties(object? obj, IEqualityComparer<string>? comparer = null)
	{
		var dict = comparer != null
			? new Dictionary<string, object?>(comparer)
			: new Dictionary<string, object?>();

		foreach (var (key, value) in ReadAnonDictionary(obj))
		{
			var skey = key?.ToString() ?? "";
			dict[skey] = value;
		}

		return dict;
	}
	
	// ReSharper disable once InconsistentNaming
	public static IEnumerable<KeyValuePair<TKey, TValue>> ToKVPairs<TKey, TValue>(object? obj, bool allowNullKeys = false)
	{
		if (obj is ICollection<KeyValuePair<TKey, TValue>> col)
		{
			foreach (var pair in col)
				if (pair.Key != null || allowNullKeys)
					yield return pair;

			yield break;
		}

		foreach (var (key, value) in ReadAnonDictionary(obj))
			if (Cast.Try(key, typeof(TKey), out var tkey) && (tkey != null || allowNullKeys))
			{
				if (!Cast.Try(value, typeof(TValue), out var tval))
					tval = default(TValue);

				yield return new KeyValuePair<TKey, TValue>((TKey) tkey!, (TValue?) tval!);
			}
	}

	[PublicAPI]
	public static ExpandoObject ToExpandoObject(object o)
	{
		IDictionary<string, object?> expando = new ExpandoObject();

		foreach (var pair in ReadProperties(o))
			expando[pair.Key] = pair.Value;

		return (ExpandoObject)expando;
	}

	public static T? Get<T>(object o, string key)
	{
		var res = ReadProperties(o).SafeGet(key);

		if (res is T variable)
			return variable;

		return default;
	}

}