﻿using System;
using System.Linq;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Helpers;

[PublicAPI]
public class DateTimeHelper
{
	public static readonly DateTimeHelper Instance = new();

	public int ShortYearThreshold = DateTime.Now.Year % 100 + 15;

	public int CurrentCentury = DateTime.Now.Year / 100 * 100;
	public int PreviousCentury = (DateTime.Now.Year / 100 - 1) * 100;

	public int ToShortYear(int year)
	{
		if (year <= PreviousCentury + ShortYearThreshold ||
			year > CurrentCentury + ShortYearThreshold)
			return year;

		return year % 100;
	}

	public int FromShortYear(int year)
	{
		if (year < 0 || year > 99)
			return year;

		if (year <= DateTime.Now.Year % 100 + 15)
			return CurrentCentury + year;

		return PreviousCentury + year;
	}

	string[] _prefferedFormats =
	{
		"dmy",
		"mdy",
		"ymd",
	};

	public string[] PrefferedFormats
	{
		get => _prefferedFormats;
		set
		{
			_prefferedFormats = value;
			_dateFormats.Reset();
		}
	}

	readonly SyncLazy<string[]> _dateFormats = SyncLazy<string[]>.Create<DateTimeHelper>(self => 
		PermutationGenerator.Instance.Permutatate("dmy")
		.Select(chars => new string(chars))
		.OrderBy(str =>
		{
			var i = self._prefferedFormats.IndexOf(str);
			if (i < 0)
				return self._prefferedFormats.Length;

			return i;
		}).ToArray());

	static string CreateFormat(char key, string value)
		=> new(key, value.Length);

	bool TryCreateDate(string[] values, string format, char delim, out string resultFormat, out DateTime result)
	{
		int day = 0, month = 0, year = 0;
		resultFormat = null!;
		result = default;
		var bld = new EnumBuilder(delim.ToString());

		foreach (var (key, str) in format.ToLower().Pairwise(values))
		{
			if (!int.TryParse(str, out var value))
				return false;

			switch (key)
			{
			case 'd':
				if (!TryParseTimePart(str, 31, out day))
					return false;

				break;
			case 'm':
				if (!TryParseTimePart(str, 12, out month))
					return false;

				break;
			case 'y':
				year = FromShortYear(value);
				break;
			default:
				return false;
			}

			bld.Append(CreateFormat(key == 'm' ? 'M' : key, str));
		}

		if (TryCreateDate(year, month, day, out result))
		{
			resultFormat = bld;
			return true;
		}
		return false;
	}

	bool TryCreateDate(int year, int month, int day, out DateTime result)
	{
		if (day > DateTime.DaysInMonth(year, month))
		{
			result = default;
			return false;
		}

		result = new DateTime(year, month, day);
		return true;
	}

	public bool TryParseDateFormat(string src, out DateTime result, out string format)
	{
		foreach (var delim in new[] {'-', '/', '.'})
		{
			var strings = src.SplitNonEmpty(delim);

			if (strings.Length != 3)
				continue;

			foreach (var fmt in _dateFormats.GetValue(this))
				if (TryCreateDate(strings, fmt, delim, out var resultFormat, out result))
				{
					format = resultFormat;
					return true;
				}
		}

		format = null!;
		result = default;
		return false;
	}


	public bool TryParseTimeFormat(string src, out TimeSpan result, out string format)
	{
		format = null!;
		result = default!;

		var hmsAndMs = src.SplitNonEmpty('.');
		if (hmsAndMs.Length is 0 or > 2)
			return false;

		var parts = hmsAndMs[0].SplitNonEmpty(":");
		int hour, minute, second = 0, ms = 0;
		switch (parts.Length)
		{
			case 2:
				if (!TryParseTimePart(parts[0], 23, out hour) ||
					!TryParseTimePart(parts[1], 59, out minute))
					return false;

				format = $"{CreateFormat('H', parts[0])}:{CreateFormat('m', parts[1])}";
				break;
			case 3:
				if (!TryParseTimePart(parts[0], 23, out hour) ||
					!TryParseTimePart(parts[1], 59, out minute) ||
					!TryParseTimePart(parts[2], 59, out second))
					return false;

				format = $"{CreateFormat('H', parts[0])}:{CreateFormat('m', parts[1])}:{CreateFormat('s', parts[2])}";
				break;

			default: 
				return false;
		}

		if (hmsAndMs.Length == 2)
		{
			if (!TryParseTimePart(hmsAndMs[1], 1000, out ms))
				return false;

			format = $"{format}.fff";
		}

		result = new TimeSpan(0, hour, minute, second, ms);
		return true;
	}

	public bool TryParseDateTimeFormat(string src, out DateTime result, out string format)
	{
		src = src.Trim();
		var parts = src.SplitNonEmpty(' ', '\t', '\n', '\r');

		switch (parts.Length)
		{
		case 1:
			return TryParseDateFormat(parts[0], out result, out format);
		case 2:
			if (!TryParseDateFormat(parts[0], out var date, out var dtFormat) ||
				!TryParseTimeFormat(parts[1], out var time, out var tmFormat))
				break;

			var delim = src[parts[0].Length];
			result = date.Date + time;
			format = $"{dtFormat}{delim}{tmFormat}";
			return true;
		}

		result = default;
		format = default!;
		return false;
	}

	public static bool TryParseTimePart(string str, int max, out int result)
		=> int.TryParse(str, out result) && result >= 0 && result <= max;
}