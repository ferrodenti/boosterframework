using System.Buffers;

namespace Booster.Helpers;

/// <summary>
/// Класс расширений для System.Buffers.ArrayPool
/// </summary>
public static class ArrayPools
{
	public static ArrayPoolRent<T> Rent<T>(int length)
	{
		var pool = ArrayPool<T>.Shared;
		var array = pool.Rent(length);
		return new ArrayPoolRent<T>(pool, array);
	}
}