using System;
using System.Buffers;

namespace Booster.Helpers;

public class ArrayPoolRent<T> : IDisposable
{
	readonly ArrayPool<T> _pool;

	public T[] Array { get; }

	public ArrayPoolRent(ArrayPool<T> pool, T[] array)
	{
		_pool = pool;
		Array = array;
	}

	public void Dispose()
		=> _pool.Return(Array);
}