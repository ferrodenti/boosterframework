﻿using System.Text.RegularExpressions;

namespace Booster.Helpers;

public class ReplaceRegex
{
	public Regex FindPattern { get; }
	public string ReplacePattern { get; }
	public bool KeepCase { get; }

	public ReplaceRegex(string findPattern, string replacePattern, bool keepCase = true)
	{
		FindPattern = new Regex(findPattern, RegexOptions.IgnoreCase);
		ReplacePattern = replacePattern;
		KeepCase = keepCase;
	}

	public bool TryReplace(string source, out string result)
	{
		var replace = ReplacePattern;

		if (KeepCase)
		{
			var scase = source.GetCase();
			replace = replace.ChangeCase(scase);
		}

		result = FindPattern.Replace(source, replace);
		return result != source;
	}
}
