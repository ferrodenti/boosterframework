namespace Booster.Helpers;

public enum StringCase
{
	Unknown,
	Lower,
	Upper,
	Snake,
	Camel
}