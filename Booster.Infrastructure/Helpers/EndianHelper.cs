﻿using System;

namespace Booster.Helpers;

public static class EndianHelper
{
	public static uint Swap32(uint val)
	{
		val = ((val << 8) & 0xFF00FF00) | ((val >> 8) & 0xFF00FF);
		return (val << 16) | (val >> 16);
	}

	public static int Swap32(int val)
	{
		val = (int) ((val << 8) & 0xFF00FF00) | ((val >> 8) & 0xFF00FF);
		return (val << 16) | (val >> 16);
	}

	public static ulong Swap64(ulong val)
	{
		val = ((val << 8) & 0xFF00FF00FF00FF00) | ((val >> 8) & 0x00FF00FF00FF00FF);
		val = ((val << 16) & 0xFFFF0000FFFF0000) | ((val >> 16) & 0x0000FFFF0000FFFF);
		return (val << 32) | (val >> 32);
	}

	public static byte[] InvariantToByteArray(int number)
	{
		if (!BitConverter.IsLittleEndian)
			return BitConverter.GetBytes(number);

		var temp = number;
		var result = new byte[4];
		for (var i = 3; i >= 0; i--)
		{
			result[i] = (byte)(0x000000ff & temp);
			temp >>= 8;
		}
		return result;
	}

	public static byte[] InvariantToByteArray(ulong number)
	{
		if (!BitConverter.IsLittleEndian)
			return BitConverter.GetBytes(number);

		var temp = number;
		var result = new byte[4];
		for (var i = 3; i >= 0; i--)
		{
			result[i] = (byte)(0x000000ff & temp);
			temp >>= 8;
		}
		return result;
	}
}