using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using Booster.Collections;
using Booster.Reflection;
using JetBrains.Annotations;

using NotNull = System.Diagnostics.CodeAnalysis.NotNullAttribute;


// ReSharper disable SwitchExpressionHandlesSomeKnownEnumValuesWithExceptionInDefault
// ReSharper disable CompareOfFloatsByEqualityOperator

#nullable enable

namespace Booster.Helpers;

[PublicAPI]
public class NumericHelper
{
	public static TypeCode MakeSame([NotNull] ref object? a, [NotNull] ref object? b)
		=> PrepareValues(ref a, ref b, true);
	
	public static object? ExtractContainer(object? a) //TODO: move to Utils, extract IConvertiable<>
	{
		var type =  a.GetTypeEx();

		if (_nullableTType.Equals(type?.GenericTypeDefinition))
		{
			var getter = ReflectNullableGetter(type!);
			var result = getter.Invoke(a);
			return result;
		}

		return a;
	}
	
	#region math operations

	public static object Sum(object? a, object? b, bool castToASutableType = false)
		=> PrepareValues(ref a, ref b, castToASutableType) switch
		{
			TypeCode.SByte   => (sbyte) a + (sbyte) b,
			TypeCode.Byte    => (byte) a + (byte) b,
			TypeCode.Int16   => (short) a + (short) b,
			TypeCode.UInt16  => (ushort) a + (ushort) b,
			TypeCode.Int32   => (int) a + (int) b,
			TypeCode.UInt32  => (uint) a + (uint) b,
			TypeCode.Int64   => (long) a + (long) b,
			TypeCode.UInt64  => (ulong) a + (ulong) b,
			TypeCode.Double  => (double) a + (double) b,
			TypeCode.Single  => (float) a + (float) b,
			TypeCode.Decimal => (decimal) a + (decimal) b,
			_                => throw new ArgumentException($"Expected values of numeric type, got {a.GetType()}, {b.GetType()}", nameof(a))
		};

	public static object Sub(object? a, object? b, bool castToASutableType = false)
		=> PrepareValues(ref a, ref b, castToASutableType) switch
		{
			TypeCode.SByte   => (sbyte) a - (sbyte) b,
			TypeCode.Byte    => (byte) a - (byte) b,
			TypeCode.Int16   => (short) a - (short) b,
			TypeCode.UInt16  => (ushort) a - (ushort) b,
			TypeCode.Int32   => (int) a - (int) b,
			TypeCode.UInt32  => (uint) a - (uint) b,
			TypeCode.Int64   => (long) a - (long) b,
			TypeCode.UInt64  => (ulong) a - (ulong) b,
			TypeCode.Double  => (double) a - (double) b,
			TypeCode.Single  => (float) a - (float) b,
			TypeCode.Decimal => (decimal) a - (decimal) b,
			_                => throw new ArgumentException($"Expected values of numeric type, got {a.GetType()}, {b.GetType()}", nameof(a))
		};
	
	public static object Neg(object? a)
		=> PrepareValue(ref a) switch
		{
			TypeCode.SByte   => -(sbyte) a,
			TypeCode.Byte    => -(byte) a,
			TypeCode.Int16   => -(short) a,
			TypeCode.UInt16  => -(ushort) a,
			TypeCode.Int32   => -(int) a,
			TypeCode.UInt32  => -(uint) a,
			TypeCode.Int64   => -(long) a,
			TypeCode.UInt64  => -(long) (ulong) a,
			TypeCode.Double  => -(double) a,
			TypeCode.Single  => -(float) a,
			TypeCode.Decimal => -(decimal) a,
			TypeCode.Boolean => !(bool) a,
			_                => throw new ArgumentException($"Expected values of numeric type, got {a.GetType()}", nameof(a))
		};

	public static object Mul(object? a, object? b, bool castToASutableType = false)
		=> PrepareValues(ref a, ref b, castToASutableType) switch
		{
			TypeCode.SByte   => (sbyte) a * (sbyte) b,
			TypeCode.Byte    => (byte) a * (byte) b,
			TypeCode.Int16   => (short) a * (short) b,
			TypeCode.UInt16  => (ushort) a * (ushort) b,
			TypeCode.Int32   => (int) a * (int) b,
			TypeCode.UInt32  => (uint) a * (uint) b,
			TypeCode.Int64   => (long) a * (long) b,
			TypeCode.UInt64  => (ulong) a * (ulong) b,
			TypeCode.Double  => (double) a * (double) b,
			TypeCode.Single  => (float) a * (float) b,
			TypeCode.Decimal => (decimal) a * (decimal) b,
			_                => throw new ArgumentException($"Expected values of numeric type, got {a.GetType()}, {b.GetType()}", nameof(a))
		};

	public static object Pow(object? a, object? b, bool castToASutableType = false)
		=> PrepareValues(ref a, ref b, castToASutableType) switch
		{
			TypeCode.SByte   => Math.Pow((sbyte) a, (sbyte) b),
			TypeCode.Byte    => Math.Pow((byte) a, (byte) b),
			TypeCode.Int16   => Math.Pow((short) a, (short) b),
			TypeCode.UInt16  => Math.Pow((ushort) a, (ushort) b),
			TypeCode.Int32   => Math.Pow((int) a, (int) b),
			TypeCode.UInt32  => Math.Pow((uint) a, (uint) b),
			TypeCode.Int64   => Math.Pow((long) a, (long) b),
			TypeCode.UInt64  => Math.Pow((ulong) a, (ulong) b),
			TypeCode.Double  => Math.Pow((double) a, (double) b),
			TypeCode.Single  => Math.Pow((float) a, (float) b),
			TypeCode.Decimal => Math.Pow((double) (decimal) a, (double) (decimal) b),
			_                => throw new ArgumentException($"Expected values of numeric type, got {a.GetType()}, {b.GetType()}", nameof(a))
		};

	public static object Div(object? a, object? b, bool castToASutableType = false)
		=> PrepareValues(ref a, ref b, castToASutableType) switch
		{
			TypeCode.SByte   => (sbyte) a / (sbyte) b,
			TypeCode.Byte    => (byte) a / (byte) b,
			TypeCode.Int16   => (short) a / (short) b,
			TypeCode.UInt16  => (ushort) a / (ushort) b,
			TypeCode.Int32   => (int) a / (int) b,
			TypeCode.UInt32  => (uint) a / (uint) b,
			TypeCode.Int64   => (long) a / (long) b,
			TypeCode.UInt64  => (ulong) a / (ulong) b,
			TypeCode.Double  => (double) a / (double) b,
			TypeCode.Single  => (float) a / (float) b,
			TypeCode.Decimal => (decimal) a / (decimal) b,
			_                => throw new ArgumentException($"Expected values of numeric type, got {a.GetType()}, {b.GetType()}", nameof(a))
		};

	public static object Inc(object? a)
		=> PrepareValue(ref a) switch
		{
			TypeCode.SByte   => (sbyte) a + 1,
			TypeCode.Byte    => (byte) a + 1,
			TypeCode.Int16   => (short) a + 1,
			TypeCode.UInt16  => (ushort) a + 1,
			TypeCode.Int32   => (int) a + 1,
			TypeCode.UInt32  => (uint) a + 1,
			TypeCode.Int64   => (long) a + 1,
			TypeCode.UInt64  => (ulong) a + 1,
			TypeCode.Double  => (double) a + 1,
			TypeCode.Single  => (float) a + 1,
			TypeCode.Decimal => (decimal) a + 1,
			_                => throw new ArgumentException($"Expected values of numeric type, got {a.GetType()}", nameof(a))
		};

	public static object Dec(object? a)
		=> PrepareValue(ref a) switch
		{
			TypeCode.SByte   => (sbyte) a - 1,
			TypeCode.Byte    => (byte) a - 1,
			TypeCode.Int16   => (short) a - 1,
			TypeCode.UInt16  => (ushort) a - 1,
			TypeCode.Int32   => (int) a - 1,
			TypeCode.UInt32  => (uint) a - 1,
			TypeCode.Int64   => (long) a - 1,
			TypeCode.UInt64  => (ulong) a - 1,
			TypeCode.Double  => (double) a - 1,
			TypeCode.Single  => (float) a - 1,
			TypeCode.Decimal => (decimal) a - 1,
			_                => throw new ArgumentException($"Expected values of numeric type, got {a.GetType()}", nameof(a))
		};

	#endregion

	#region Comparison operations

	public static bool IsZero(object? a)
		=> PrepareValue(ref a) switch
		{
			TypeCode.SByte   => (sbyte) a == 0,
			TypeCode.Byte    => (byte) a == 0,
			TypeCode.Int16   => (short) a == 0,
			TypeCode.UInt16  => (ushort) a == 0,
			TypeCode.Int32   => (int) a == 0,
			TypeCode.UInt32  => (uint) a == 0,
			TypeCode.Int64   => (long) a == 0,
			TypeCode.UInt64  => (ulong) a == 0,
			TypeCode.Double  => (double) a == 0, //-V3024
			TypeCode.Single  => (float) a == 0, //-V3024
			TypeCode.Decimal => (decimal) a == 0,
			TypeCode.Boolean => (bool) a == false,
			_                => throw new ArgumentException($"Expected values of numeric type, got {a.GetType()}", nameof(a))
		};

	public static object IsNegative(object? a)
		=> PrepareValue(ref a) switch
		{
			TypeCode.SByte   => (sbyte) a < 0,
			TypeCode.Byte    => false,
			TypeCode.Int16   => (short) a < 0,
			TypeCode.UInt16  => false,
			TypeCode.Int32   => (int) a < 0,
			TypeCode.UInt32  => false,
			TypeCode.Int64   => (long) a < 0,
			TypeCode.UInt64  => false,
			TypeCode.Single  => (float) a < 0,
			TypeCode.Double  => (double) a < 0,
			TypeCode.Decimal => (decimal) a < 0,
			TypeCode.Boolean => false,
			_                => throw new ArgumentException($"Expected a value of integer or boolean type, got {a.GetType()}", nameof(a))
		};

	public static object IsPositive(object? a)
		=> PrepareValue(ref a) switch
		{
			TypeCode.SByte   => (sbyte) a >= 0,
			TypeCode.Byte    => true,
			TypeCode.Int16   => (short) a >= 0,
			TypeCode.UInt16  => true,
			TypeCode.Int32   => (int) a >= 0,
			TypeCode.UInt32  => true,
			TypeCode.Int64   => (long) a >= 0,
			TypeCode.UInt64  => true,
			TypeCode.Single  => (float) a >= 0,
			TypeCode.Double  => (double) a >= 0,
			TypeCode.Decimal => (decimal) a >= 0,
			TypeCode.Boolean => true,
			_                => throw new ArgumentException($"Expected a value of integer or boolean type, got {a.GetType()}", nameof(a))
		};

	public static bool IsPowerOfTwo(object? a)
		=> PrepareValue(ref a) switch
		{
			TypeCode.SByte  => ((sbyte) a & ((sbyte) a - 1)) == 0,
			TypeCode.Byte   => ((byte) a & ((byte) a - 1)) == 0,
			TypeCode.Int16  => ((short) a & ((short) a - 1)) == 0,
			TypeCode.UInt16 => ((ushort) a & ((ushort) a - 1)) == 0,
			TypeCode.Int32  => ((int) a & ((int) a - 1)) == 0,
			TypeCode.UInt32 => ((uint) a & ((uint) a - 1)) == 0,
			TypeCode.Int64  => ((long) a & ((long) a - 1)) == 0,
			TypeCode.UInt64 => ((ulong) a & ((ulong) a - 1)) == 0,
			_               => throw new ArgumentException($"Expected a value of integer or boolean type, got {a.GetType()}", nameof(a))
		};

	public static bool Greater(object? a, object? b, bool castToASutableType = false)
		=> PrepareValues(ref a, ref b, castToASutableType) switch
		{
			TypeCode.SByte   => (sbyte) a > (sbyte) b,
			TypeCode.Byte    => (byte) a > (byte) b,
			TypeCode.Int16   => (short) a > (short) b,
			TypeCode.UInt16  => (ushort) a > (ushort) b,
			TypeCode.Int32   => (int) a > (int) b,
			TypeCode.UInt32  => (uint) a > (uint) b,
			TypeCode.Int64   => (long) a > (long) b,
			TypeCode.UInt64  => (ulong) a > (ulong) b,
			TypeCode.Double  => (double) a > (double) b,
			TypeCode.Single  => (float) a > (float) b,
			TypeCode.Decimal => (decimal) a > (decimal) b,
			_                => throw new ArgumentException($"Expected values of numeric type, got {a.GetType()}, {b.GetType()}", nameof(a))
		};

	public static bool Less(object? a, object? b, bool castToASutableType = false)
		=> PrepareValues(ref a, ref b, castToASutableType) switch
		{
			TypeCode.SByte   => (sbyte) a < (sbyte) b,
			TypeCode.Byte    => (byte) a < (byte) b,
			TypeCode.Int16   => (short) a < (short) b,
			TypeCode.UInt16  => (ushort) a < (ushort) b,
			TypeCode.Int32   => (int) a < (int) b,
			TypeCode.UInt32  => (uint) a < (uint) b,
			TypeCode.Int64   => (long) a < (long) b,
			TypeCode.UInt64  => (ulong) a < (ulong) b,
			TypeCode.Double  => (double) a < (double) b,
			TypeCode.Single  => (float) a < (float) b,
			TypeCode.Decimal => (decimal) a < (decimal) b,
			_                => throw new ArgumentException($"Expected values of numeric type, got {a.GetType()}, {b.GetType()}", nameof(a))
		};

	public static bool GreaterOrEqual(object? a, object? b, bool castToASutableType = false)
		=> PrepareValues(ref a, ref b, castToASutableType) switch
		{
			TypeCode.SByte   => (sbyte) a >= (sbyte) b,
			TypeCode.Byte    => (byte) a >= (byte) b,
			TypeCode.Int16   => (short) a >= (short) b,
			TypeCode.UInt16  => (ushort) a >= (ushort) b,
			TypeCode.Int32   => (int) a >= (int) b,
			TypeCode.UInt32  => (uint) a >= (uint) b,
			TypeCode.Int64   => (long) a >= (long) b,
			TypeCode.UInt64  => (ulong) a >= (ulong) b,
			TypeCode.Double  => (double) a >= (double) b,
			TypeCode.Single  => (float) a >= (float) b,
			TypeCode.Decimal => (decimal) a >= (decimal) b,
			_                => throw new ArgumentException($"Expected values of numeric type, got {a.GetType()}, {b.GetType()}", nameof(a))
		};

	public static bool LessOrEqual(object? a, object? b, bool castToASutableType = false)
		=> PrepareValues(ref a, ref b, castToASutableType) switch
		{
			TypeCode.SByte   => (sbyte) a <= (sbyte) b,
			TypeCode.Byte    => (byte) a <= (byte) b,
			TypeCode.Int16   => (short) a <= (short) b,
			TypeCode.UInt16  => (ushort) a <= (ushort) b,
			TypeCode.Int32   => (int) a <= (int) b,
			TypeCode.UInt32  => (uint) a <= (uint) b,
			TypeCode.Int64   => (long) a <= (long) b,
			TypeCode.UInt64  => (ulong) a <= (ulong) b,
			TypeCode.Double  => (double) a <= (double) b,
			TypeCode.Single  => (float) a <= (float) b,
			TypeCode.Decimal => (decimal) a <= (decimal) b,
			_                => throw new ArgumentException($"Expected values of numeric type, got {a.GetType()}, {b.GetType()}", nameof(a))
		};

	public static bool Equal(object? a, object? b, bool castToASutableType = false)
		=> PrepareValues(ref a, ref b, castToASutableType) switch
		{
			TypeCode.SByte   => (sbyte) a == (sbyte) b,
			TypeCode.Byte    => (byte) a == (byte) b,
			TypeCode.Int16   => (short) a == (short) b,
			TypeCode.UInt16  => (ushort) a == (ushort) b,
			TypeCode.Int32   => (int) a == (int) b,
			TypeCode.UInt32  => (uint) a == (uint) b,
			TypeCode.Int64   => (long) a == (long) b,
			TypeCode.UInt64  => (ulong) a == (ulong) b,
			TypeCode.Single  => (float) a == (float) b, //-V3024
			TypeCode.Double  => (double) a == (double) b, //-V3024
			TypeCode.Decimal => (decimal) a == (decimal) b,
			TypeCode.Boolean => (bool) a == (bool) b,
			_                => throw new ArgumentException($"Expected values of numeric type, got {a.GetType()}, {b.GetType()}", nameof(a))
		};

	#endregion

	#region logic operations

	public static object And(object? a, object? b, bool castToASutableType = false)
		=> PrepareValues(ref a, ref b, castToASutableType) switch
		{
			TypeCode.SByte   => (sbyte) a & (sbyte) b,
			TypeCode.Byte    => (byte) a & (byte) b,
			TypeCode.Int16   => (short) a & (short) b,
			TypeCode.UInt16  => (ushort) a & (ushort) b,
			TypeCode.Int32   => (int) a & (int) b,
			TypeCode.UInt32  => (uint) a & (uint) b,
			TypeCode.Int64   => (long) a & (long) b,
			TypeCode.UInt64  => (ulong) a & (ulong) b,
			TypeCode.Boolean => (bool) a && (bool) b,
			_                => throw new ArgumentException($"Expected values of integer or boolean type, got {a.GetType()}, {b.GetType()}", nameof(a))
		};

	public static object Or(object? a, object? b, bool castToASutableType = false)
		=> PrepareValues(ref a, ref b, castToASutableType) switch
		{
			TypeCode.SByte   => (sbyte) a | (sbyte) b,
			TypeCode.Byte    => (byte) a | (byte) b,
			TypeCode.Int16   => (short) a | (short) b,
			TypeCode.UInt16  => (ushort) a | (ushort) b,
			TypeCode.Int32   => (int) a | (int) b,
			TypeCode.UInt32  => (uint) a | (uint) b,
			TypeCode.Int64   => (long) a | (long) b,
			TypeCode.UInt64  => (ulong) a | (ulong) b,
			TypeCode.Boolean => (bool) a || (bool) b,
			_                => throw new ArgumentException($"Expected values of integer or boolean type, got {a.GetType()}, {b.GetType()}", nameof(a))
		};

	public static object Xor(object? a, object? b, bool castToASutableType = false)
		=> PrepareValues(ref a, ref b, castToASutableType) switch
		{
			TypeCode.SByte   => (sbyte) a ^ (sbyte) b,
			TypeCode.Byte    => (byte) a ^ (byte) b,
			TypeCode.Int16   => (short) a ^ (short) b,
			TypeCode.UInt16  => (ushort) a ^ (ushort) b,
			TypeCode.Int32   => (int) a ^ (int) b,
			TypeCode.UInt32  => (uint) a ^ (uint) b,
			TypeCode.Int64   => (long) a ^ (long) b,
			TypeCode.UInt64  => (ulong) a ^ (ulong) b,
			TypeCode.Boolean => (bool) a ^ (bool) b,
			_                => throw new ArgumentException($"Expected values of integer or boolean type, got {a.GetType()}, {b.GetType()}", nameof(a))
		};

	public static object Not(object? a)
		=> PrepareValue(ref a) switch
		{
			TypeCode.SByte   => ~(sbyte) a,
			TypeCode.Byte    => ~(byte) a,
			TypeCode.Int16   => ~(short) a,
			TypeCode.UInt16  => ~(ushort) a,
			TypeCode.Int32   => ~(int) a,
			TypeCode.UInt32  => ~(uint) a,
			TypeCode.Int64   => ~(long) a,
			TypeCode.UInt64  => ~(ulong) a,
			TypeCode.Boolean => !(bool) a,
			_                => throw new ArgumentException($"Expected a value of integer or boolean type, got {a.GetType()}", nameof(a))
		};

	#endregion

	#region Other operations

	public static object Cast(object? a, TypeCode typeCode)
	{
		var from = PrepareValue(ref a);
		if (from == typeCode)
			return a;

		return CastInt(a, from, typeCode);
	}

	public static bool IsNumeric(TypeEx type)
		=> ExtractInnerType(type).TypeCode switch
		{
			TypeCode.Byte    => true,
			TypeCode.SByte   => true,
			TypeCode.UInt16  => true,
			TypeCode.UInt32  => true,
			TypeCode.UInt64  => true,
			TypeCode.Int16   => true,
			TypeCode.Int32   => true,
			TypeCode.Int64   => true,
			TypeCode.Decimal => true,
			TypeCode.Double  => true,
			TypeCode.Single  => true,
			_                => false
		};

	public static bool IsInteger(TypeEx type)
		=> ExtractInnerType(type).TypeCode switch
		{
			TypeCode.Byte   => true,
			TypeCode.SByte  => true,
			TypeCode.UInt16 => true,
			TypeCode.UInt32 => true,
			TypeCode.UInt64 => true,
			TypeCode.Int16  => true,
			TypeCode.Int32  => true,
			TypeCode.Int64  => true,
			_               => false
		};

	public static bool IsReal(TypeEx type)
		=> ExtractInnerType(type).TypeCode switch
		{
			TypeCode.Decimal => true,
			TypeCode.Double  => true,
			TypeCode.Single  => true,
			_                => false
		};


	public static string ToString(object? a, int radix)
		=> PrepareValue(ref a) switch
		{
			TypeCode.SByte   => Convert.ToString((sbyte) a, radix),
			TypeCode.Byte    => Convert.ToString((byte) a, radix),
			TypeCode.Int16   => Convert.ToString((short) a, radix),
			TypeCode.UInt16  => Convert.ToString((ushort) a, radix),
			TypeCode.Int32   => Convert.ToString((int) a, radix),
			TypeCode.UInt32  => Convert.ToString((uint) a, radix),
			TypeCode.Int64   => Convert.ToString((long) a, radix),
			TypeCode.UInt64  => Convert.ToString(unchecked((long) (ulong) a), radix),
			TypeCode.Single  => Convert.ToString((float) a),
			TypeCode.Double  => Convert.ToString((double) a),
			TypeCode.Decimal => Convert.ToString((decimal) a),
			TypeCode.Boolean => Convert.ToString((bool) a),
			_                => throw new ArgumentException($"Expected a value of integer or boolean type, got {a.GetType()}", nameof(a))
		};
	
	public static IEnumerable<T> Counter<T>(T start = default) where T : struct
	{
		for (var i = start;; i = (T)Inc(i))
			yield return i;

		// ReSharper disable once IteratorNeverReturns
	}
	
	#endregion
	
	#region Non public methods
	
	static TypeCode PrepareValue([NotNull] ref object? a)
	{
		a = ExtractContainer(a) ?? 0;
		var type = a.GetTypeEx();
		return type.TypeCode;
	}
	
	static TypeCode PrepareValues([NotNull] ref object? a, [NotNull] ref object? b, bool castToASutableType)
	{
		a = ExtractContainer(a) ?? 0;
		b = ExtractContainer(b) ?? 0;

		var tca = a.GetTypeEx().TypeCode;
		var tcb = b.GetTypeEx().TypeCode;

		if (tca == tcb)
			return tca;

		var ntia = NumericTypeInfo.Get(tca) ?? throw new ArgumentOutOfRangeException(nameof(a), "Expected numeric value");
		var ntib = NumericTypeInfo.Get(tcb) ?? throw new ArgumentOutOfRangeException(nameof(b), "Expected numeric value");

		if (!castToASutableType)
			throw new InvalidOperationException($"The given parameters have different types: {ntia.TypeCode}, {ntib.TypeCode}");
				
		var max = NumericTypeInfo.SelectMax(ntia, ntib, out var overflow);
		var result = max.TypeCode;
		
		if (overflow)
		{
			if (!ntia.IsSigned)
				result = CheckOverflow(a, ntia.TypeCode) ? max.TypeCode : ntib.TypeCode;
			else if (!ntib.IsSigned)
				result = CheckOverflow(b, ntib.TypeCode) ? max.TypeCode : ntia.TypeCode;
			else
				throw new Exception("Unexpected signed overflow");
		}
		
		if(result != ntia.TypeCode)
			a = CastInt(a, ntia.TypeCode, result);
		
		if(result != ntib.TypeCode)
			b = CastInt(b, ntib.TypeCode, result);
		
		return result;
	}
	
	static readonly TypeEx _nullableTType = TypeEx.Get(typeof(Nullable<>));
	static readonly HybridDictionary<TypeEx, Method> _nullableGetters = new();

	[MethodImpl(MethodImplOptions.Synchronized)]
	static Method ReflectNullableGetter(TypeEx type)
		=> _nullableGetters.GetOrAdd(type, _ =>
		{
			var m = _nullableTType.FindProperty("Value")!;
			var mi = m.FindImplementation(type);
			return mi.GetMethod;
		});

	static TypeEx ExtractInnerType(TypeEx type)
	{
		if (type.IsNullable)
			return type.GenericArguments[0];

		return type;
	}

	[SuppressMessage("ReSharper", "RedundantOverflowCheckingContext")]
	static bool CheckOverflow(object a, TypeCode cast)
		=> cast switch
		{
			TypeCode.Byte   => unchecked((sbyte) (byte) a) < 0,
			TypeCode.UInt16 => unchecked((short) (ushort) a) < 0,
			TypeCode.UInt32 => unchecked((int) (uint) a) < 0,
			TypeCode.UInt64 => unchecked((long) (ulong) a) < 0,
			_               => throw new ArgumentOutOfRangeException()
		};
	
	static object CastInt(object a, TypeCode from, TypeCode to)
		=> from switch
		{
			TypeCode.Byte => to switch
			{
				TypeCode.Byte    => (byte) a,
				TypeCode.SByte   => (sbyte) (byte) a,
				TypeCode.UInt16  => (ushort) (byte) a,
				TypeCode.Int16   => (short) (byte) a,
				TypeCode.UInt32  => (uint) (byte) a,
				TypeCode.Int32   => (int) (byte) a,
				TypeCode.UInt64  => (ulong) (byte) a,
				TypeCode.Int64   => (long) (byte) a,
				TypeCode.Single  => (float) (byte) a,
				TypeCode.Double  => (double) (byte) a,
				TypeCode.Decimal => (decimal) (byte) a,
				_                => throw new ArgumentOutOfRangeException(nameof(to), to, null)
			},
			TypeCode.SByte => to switch
			{
				TypeCode.Byte    => (byte) (sbyte) a,
				TypeCode.SByte   => (sbyte) a,
				TypeCode.UInt16  => (ushort) (sbyte) a,
				TypeCode.Int16   => (short) (sbyte) a,
				TypeCode.UInt32  => (uint) (sbyte) a,
				TypeCode.Int32   => (int) (sbyte) a,
				TypeCode.UInt64  => (ulong) (sbyte) a,
				TypeCode.Int64   => (long) (sbyte) a,
				TypeCode.Single  => (float) (sbyte) a,
				TypeCode.Double  => (double) (sbyte) a,
				TypeCode.Decimal => (decimal) (sbyte) a,
				_                => throw new ArgumentOutOfRangeException(nameof(to), to, null)
			},
			TypeCode.UInt16 => to switch
			{
				TypeCode.Byte    => (byte) (ushort) a,
				TypeCode.SByte   => (sbyte) (ushort) a,
				TypeCode.UInt16  => (ushort) a,
				TypeCode.Int16   => (short) (ushort) a,
				TypeCode.UInt32  => (uint) (ushort) a,
				TypeCode.Int32   => (int) (ushort) a,
				TypeCode.UInt64  => (ulong) (ushort) a,
				TypeCode.Int64   => (long) (ushort) a,
				TypeCode.Single  => (float) (ushort) a,
				TypeCode.Double  => (double) (ushort) a,
				TypeCode.Decimal => (decimal) (ushort) a,
				_                => throw new ArgumentOutOfRangeException(nameof(to), to, null)
			},
			TypeCode.Int16 => to switch
			{
				TypeCode.Byte    => (byte) (short) a,
				TypeCode.SByte   => (sbyte) (short) a,
				TypeCode.UInt16  => (ushort) (short) a,
				TypeCode.Int16   => (short) a,
				TypeCode.UInt32  => (uint) (short) a,
				TypeCode.Int32   => (int) (short) a,
				TypeCode.UInt64  => (ulong) (short) a,
				TypeCode.Int64   => (long) (short) a,
				TypeCode.Single  => (float) (short) a,
				TypeCode.Double  => (double) (short) a,
				TypeCode.Decimal => (decimal) (short) a,
				_                => throw new ArgumentOutOfRangeException(nameof(to), to, null)
			},
			TypeCode.UInt32 => to switch
			{
				TypeCode.Byte    => (byte) (uint) a,
				TypeCode.SByte   => (sbyte) (uint) a,
				TypeCode.UInt16  => (ushort) (uint) a,
				TypeCode.Int16   => (short) (uint) a,
				TypeCode.UInt32  => (uint) a,
				TypeCode.Int32   => (int) (uint) a,
				TypeCode.UInt64  => (ulong) (uint) a,
				TypeCode.Int64   => (long) (uint) a,
				TypeCode.Single  => (float) (uint) a,
				TypeCode.Double  => (double) (uint) a,
				TypeCode.Decimal => (decimal) (uint) a,
				_                => throw new ArgumentOutOfRangeException(nameof(to), to, null)
			},
			TypeCode.Int32 => to switch
			{
				TypeCode.Byte    => (byte) (int) a,
				TypeCode.SByte   => (sbyte) (int) a,
				TypeCode.UInt16  => (ushort) (int) a,
				TypeCode.Int16   => (short) (int) a,
				TypeCode.UInt32  => (uint) (int) a,
				TypeCode.Int32   => (int) a,
				TypeCode.UInt64  => (ulong) (int) a,
				TypeCode.Int64   => (long) (int) a,
				TypeCode.Single  => (float) (int) a,
				TypeCode.Double  => (double) (int) a,
				TypeCode.Decimal => (decimal) (int) a,
				_                => throw new ArgumentOutOfRangeException(nameof(to), to, null)
			},
			TypeCode.UInt64 => to switch
			{
				TypeCode.Byte    => (byte) (ulong) a,
				TypeCode.SByte   => (sbyte) (ulong) a,
				TypeCode.UInt16  => (ushort) (ulong) a,
				TypeCode.Int16   => (short) (ulong) a,
				TypeCode.UInt32  => (uint) (ulong) a,
				TypeCode.Int32   => (int) (ulong) a,
				TypeCode.UInt64  => (ulong) a,
				TypeCode.Int64   => (long) (ulong) a,
				TypeCode.Single  => (float) (ulong) a,
				TypeCode.Double  => (double) (ulong) a,
				TypeCode.Decimal => (decimal) (ulong) a,
				_                => throw new ArgumentOutOfRangeException(nameof(to), to, null)
			},
			TypeCode.Int64 => to switch
			{
				TypeCode.Byte    => (byte) (long) a,
				TypeCode.SByte   => (sbyte) (long) a,
				TypeCode.UInt16  => (ushort) (long) a,
				TypeCode.Int16   => (short) (long) a,
				TypeCode.UInt32  => (uint) (long) a,
				TypeCode.Int32   => (int) (long) a,
				TypeCode.UInt64  => (ulong) (long) a,
				TypeCode.Int64   => (long) a,
				TypeCode.Single  => (float) (long) a,
				TypeCode.Double  => (double) (long) a,
				TypeCode.Decimal => (decimal) (long) a,
				_                => throw new ArgumentOutOfRangeException(nameof(to), to, null)
			},
			TypeCode.Single => to switch
			{
				TypeCode.Byte    => (byte) (float) a,
				TypeCode.SByte   => (sbyte) (float) a,
				TypeCode.UInt16  => (ushort) (float) a,
				TypeCode.Int16   => (short) (float) a,
				TypeCode.UInt32  => (uint) (float) a,
				TypeCode.Int32   => (int) (float) a,
				TypeCode.UInt64  => (ulong) (float) a,
				TypeCode.Int64   => (long) (float) a,
				TypeCode.Single  => (float) a,
				TypeCode.Double  => (double) (float) a,
				TypeCode.Decimal => (decimal) (float) a,
				_                => throw new ArgumentOutOfRangeException(nameof(to), to, null)
			},
			TypeCode.Double => to switch
			{
				TypeCode.Byte    => (byte) (double) a,
				TypeCode.SByte   => (sbyte) (double) a,
				TypeCode.UInt16  => (ushort) (double) a,
				TypeCode.Int16   => (short) (double) a,
				TypeCode.UInt32  => (uint) (double) a,
				TypeCode.Int32   => (int) (double) a,
				TypeCode.UInt64  => (ulong) (double) a,
				TypeCode.Int64   => (long) (double) a,
				TypeCode.Single  => (float) (double) a,
				TypeCode.Double  => (double) a,
				TypeCode.Decimal => (decimal) (double) a,
				_                => throw new ArgumentOutOfRangeException(nameof(to), to, null)
			},
			TypeCode.Decimal => to switch
			{
				TypeCode.Byte    => (byte) (decimal) a,
				TypeCode.SByte   => (sbyte) (decimal) a,
				TypeCode.UInt16  => (ushort) (decimal) a,
				TypeCode.Int16   => (short) (decimal) a,
				TypeCode.UInt32  => (uint) (decimal) a,
				TypeCode.Int32   => (int) (decimal) a,
				TypeCode.UInt64  => (ulong) (decimal) a,
				TypeCode.Int64   => (long) (decimal) a,
				TypeCode.Single  => (float) (decimal) a,
				TypeCode.Double  => (double) (decimal) a,
				TypeCode.Decimal => (decimal) a,
				_                => throw new ArgumentOutOfRangeException(nameof(to), to, null)
			},
			_ => throw new ArgumentOutOfRangeException(nameof(from), from, null)
		};

	#endregion
}