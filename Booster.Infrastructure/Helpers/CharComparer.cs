using System.Collections.Generic;
using System.Globalization;

namespace Booster.Helpers;

public class CharComparer : IComparer<char>, IEqualityComparer<char>
{
	public static readonly CharComparer IgnoreCase = new(true);
	
	readonly bool _ignoreCase;
	readonly CultureInfo _ci;
		
	public CharComparer(CultureInfo ci, bool ignoreCase = false)
	{
		_ci = ci;
		_ignoreCase = ignoreCase;
	}
	
	public CharComparer(bool ignoreCase = false)
		: this(CultureInfo.CurrentCulture, ignoreCase)
	{
	}

	public int Compare(char x, char y)
	{
		if (_ignoreCase)
			return char.ToUpper(x, _ci) - char.ToUpper(y, _ci);

		return x - y;
	}

	public bool IsEqual(char x, char y)
		=> Compare(x, y) == 0;

	public bool Equals(char x, char y)
		=> Compare(x, y) == 0;

	public int GetHashCode(char obj)
		=> _ignoreCase 
			? char.ToUpper(obj, _ci).GetHashCode() 
			: obj.GetHashCode();
}