using System;
using System.Collections.Generic;

#nullable enable

namespace Booster.Helpers;

public class NumericTypeInfo
{
	public readonly bool IsFloat;
	public readonly bool IsSigned;
	public readonly int Bytes;
	public readonly TypeCode TypeCode;
	public readonly TypeCode NextSigned;

	protected NumericTypeInfo(bool isFloat, bool isSigned, int bytes, TypeCode typeCode, TypeCode nextSigned = TypeCode.Empty)
	{
		IsFloat = isFloat;
		IsSigned = isSigned;
		Bytes = bytes;
		TypeCode = typeCode;
		NextSigned = nextSigned;
	}

	public static NumericTypeInfo? Get(TypeCode typeCode)
		=> _types.SafeGet(typeCode);

	static readonly Dictionary<TypeCode, NumericTypeInfo> _types = new()
	{
		{TypeCode.Byte, new NumericTypeInfo(false, false, 1, TypeCode.Byte, TypeCode.Int16)},
		{TypeCode.SByte, new NumericTypeInfo(false, true, 1, TypeCode.SByte)},
		{TypeCode.UInt16, new NumericTypeInfo(false, false, 2, TypeCode.UInt16, TypeCode.Int32)},
		{TypeCode.Int16, new NumericTypeInfo(false, true, 2, TypeCode.Int16)},
		{TypeCode.UInt32, new NumericTypeInfo(false, false, 4, TypeCode.UInt32, TypeCode.Int64)},
		{TypeCode.Int32, new NumericTypeInfo(false, true, 4, TypeCode.Int32)},
		{TypeCode.UInt64, new NumericTypeInfo(false, false, 8, TypeCode.UInt64, TypeCode.Decimal)},
		{TypeCode.Int64, new NumericTypeInfo(false, true, 8, TypeCode.Int64)},
		{TypeCode.Single, new NumericTypeInfo(true, true, 4, TypeCode.Single)},
		{TypeCode.Double, new NumericTypeInfo(true, true, 8, TypeCode.Double)},
		{TypeCode.Decimal, new NumericTypeInfo(true, true, 16, TypeCode.Decimal)}
	};

	public static NumericTypeInfo SelectMax(NumericTypeInfo a, NumericTypeInfo b, out bool overflow)
	{
		NumericTypeInfo MaxBytes()
			=> a.Bytes > b.Bytes ? a : b;

		overflow = false;

		if (a.IsFloat)
		{
			if (b.IsFloat) return MaxBytes();

			return a;
		}

		if (b.IsFloat)
			return b;

		if (a.IsSigned == b.IsSigned)
			return MaxBytes();

		if (a.Bytes == b.Bytes)
		{
			overflow = true;

			if (!a.IsSigned)
				return Get(a.NextSigned) ?? b;

			return Get(b.NextSigned) ?? a;
		}

		return MaxBytes();
	}
}