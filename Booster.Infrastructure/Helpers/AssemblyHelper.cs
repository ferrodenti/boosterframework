using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Booster.Collections;
using Booster.FileSystem;
using Booster.Reflection;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Helpers;

[PublicAPI]
public static class AssemblyHelper
{
	static FastLazy<FilePath>? _binPath;

	public static FilePath BinPath => _binPath ??=
		(FilePath)AppDomain.CurrentDomain.RelativeSearchPath
			.Or(AppDomain.CurrentDomain.BaseDirectory);

	static readonly ListDictionary<StringComparer, Dictionary<string, Assembly>> _assemblyComparerCache = new();

	internal static Dictionary<string, Assembly> GetCache(StringComparer comparer)
		=> _assemblyComparerCache.GetOrAdd(comparer, cmp => GetAllAssemblies().ToDictionarySafe(a => a.GetName().Name, cmp));

	static Dictionary<string, Assembly> GetAssemblyCache(bool ignoreCase)
		=> GetCache(ignoreCase ? StringComparer.OrdinalIgnoreCase : StringComparer.Ordinal);

	public static Assembly? GetAssembly(string name, bool ignoreCase = true)
		=> GetAssemblyCache(ignoreCase)!.GetOrAdd(name, _ => FindAssembly(name, ignoreCase));

	static Assembly? FindAssembly(string name, bool ignoreCase)
		=> GetAllAssemblies()
			.FirstOrDefault(a => a.GetName().Name.Equals(name, ignoreCase));


	static IEnumerable<Assembly> GetAllAssemblies()
		=> AppDomain.CurrentDomain.GetAssemblies();

	// ReSharper disable once ReturnTypeCanBeNotNullable
	public static TypeEx? FindType(string name, bool ignoreCase = true)
		=> GetAllAssemblies()
			.Select(assy => assy.GetType(name, false, ignoreCase))
			.FirstOrDefault(res => (TypeEx?)res != null);


	static readonly Lazy<int> _reflectionOnlyLoadFromSubscribe = new(() =>
	{
		AppDomain.CurrentDomain.ReflectionOnlyAssemblyResolve += (_, e)
			=> Assembly.ReflectionOnlyLoad(e.Name);

		return 1;
	});

	public static Assembly? ReflectionOnlyLoadFrom(string assemblyFile)
	{
		Utils.Nop(_reflectionOnlyLoadFromSubscribe.Value);
		return Try.OrDefault(() => Assembly.ReflectionOnlyLoadFrom(assemblyFile));
	}

	static FastLazy<Assembly>? _boosterCallingAssembly;
	public static Assembly BoosterCallingAssembly
	{
		get => _boosterCallingAssembly ??= GetBoosterCallingAssembly();
		set => _boosterCallingAssembly = value;
	}

	static Assembly GetBoosterCallingAssembly()
	{
		var frames = new StackTrace().GetFrames();
		if (frames != null)
			foreach (var frame in frames)
			{
				var assy = frame.GetMethod().ReflectedType?.Assembly;
				if (assy == null)
					continue;

				var assyName = assy.FullName;
				if (!assyName.StartsWith("Booster") && !assyName.StartsWith("mscorlib"))
					return assy;
			}

		return Assembly.GetCallingAssembly();
	}
}