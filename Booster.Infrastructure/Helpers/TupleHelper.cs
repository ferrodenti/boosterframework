using System;
using System.Collections.Generic;
using System.Linq;
using Booster.Collections;
using Booster.Reflection;

namespace Booster.Helpers;

public abstract class TupleHelper
{
	public readonly TypeEx Type;
	public readonly TypeEx[] Arguments;
	public readonly TypeEx[] SubTypes;

	public static bool IsTuple(TypeEx type)
		=> type.FullName.StartsWith("System.ValueTuple'") ||
		   type.HasInterface("System.ITuple") ||
		   type.HasInterface("System.Runtime.CompilerServices.ITuple");

	protected TupleHelper(TypeEx type1)
	{
		Type = type1;

		if (!IsTuple(Type))
			throw new ArgumentException("Expected Tuple<...> parameter");

		var arguments = new List<TypeEx>();
		var subTypes = new List<TypeEx>();
		var sub = Type;
		while (true)
		{
			for (var i = 0; i < sub.GenericArguments.Length - 1; ++i) 
				arguments.Add(sub.GenericArguments[i]);

			var last = sub.GenericArguments.Last();
			if (IsTuple(last))
			{
				subTypes.Add(sub);
				sub = last;
				continue;
			}

			subTypes.Add(sub);
			arguments.Add(last);
			break;
		}
		Arguments = arguments.ToArray();
		SubTypes = ((IEnumerable<TypeEx>)subTypes).Reverse().ToArray();
	}

	public static object Create(TypeEx type, params object[] arguments)
		=> GetInstance(type).Create(arguments);

	public object Create(params object[] arguments)
	{
		if (SubTypes.Length == 1)
			return Type.Create(arguments);
			
		var k = arguments.Length;
		var args = SubTypes[0].GenericArguments.Length;
		k -= args;
		var result = SubTypes[0].Create(arguments.Range(k, args).ToArray());
		foreach (var type in SubTypes.Skip(1))
		{
			args = type.GenericArguments.Length - 1;
			k -= args;
			result = type.Create(arguments.Range(k, args).AppendAfter(result).ToArray());
		}

		return result;
	}

	static readonly HybridDictionary<TypeEx, TupleHelper> _cache = new();

	public static TupleHelper GetInstance(TypeEx tupleType)
		=> _cache.GetOrAdd(tupleType,
			t => (TupleHelper) TypeEx.Get(typeof(TupleHelper<>).MakeGenericType(t))
				.FindField("Instance")
				.GetValue(null));
}
	
public class TupleHelper<T> : TupleHelper
{
	public static readonly TupleHelper<T> Instance = new();
		
	protected TupleHelper() : base(typeof(T))
	{
	}

	public new T Create(params object[] arguments)
		=> (T) base.Create(arguments);
}