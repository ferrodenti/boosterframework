using System;
using System.Threading.Tasks;
using JetBrains.Annotations;

#nullable enable

namespace Booster;

[PublicAPI]
public static class Try
{
	public static T? OrDefault<T>([InstantHandle] Func<T?> evaluator)
	{
		try
		{
			return evaluator();
		}
		catch
		{
			return default;
		}
	}

	public static TResult OrDefault<TResult>(
		[InstantHandle] Func<TResult?> evaluator,
		TResult defaultValue)
	{
		try
		{
			return evaluator() ?? defaultValue;
		}
		catch
		{
			return defaultValue;
		}
	}

	public static async Task<TResult?> OrDefaultAsync<TResult>(
		[InstantHandle] Func<Task<TResult?>> evaluator)
	{
		try
		{
			return await evaluator().NoCtx();
		}
		catch
		{
			return default;
		}

	}

	public static async Task<TResult> OrDefaultAsync<TResult>(
		[InstantHandle] Func<Task<TResult?>> evaluator, 
		TResult defaultValue)
	{
		try
		{
			return await evaluator().NoCtx() ?? defaultValue;
		}
		catch
		{
			return defaultValue;
		}
	}

	public static async Task<TResult?> OrDefaultAsync<TException, TResult>(
		[InstantHandle] Func<Task<TResult?>> evaluator)
		where TException : Exception
	{
		try
		{
			return await evaluator().NoCtx();
		}
		catch (TException)
		{
			return default;
		}

	}

	public static async Task<TResult> OrDefaultAsync<TException, TResult>(
		[InstantHandle] Func<Task<TResult?>> evaluator,
		TResult defaultValue)
		where TException : Exception
	{
		try
		{
			return await evaluator().NoCtx() ?? defaultValue;
		}
		catch (TException)
		{
			return defaultValue;
		}

	}

	public static void IgnoreErrors([InstantHandle] Action action)
	{
		try
		{
			action();
		}
		catch { /* ignored */ }
	}

	public static async Task IgnoreErrors(Func<Task> func)
	{
		try
		{
			await func().NoCtx();
		}
		catch { /* ignored */ }
	}

	public static void IgnoreErrors<TException>([InstantHandle] Action action)
		where TException : Exception
	{
		try
		{
			action();
		}
		catch (TException) { /* ignored */ }
	}

	public static async Task IgnoreErrorsAsync([InstantHandle] Func<Task> action)
	{
		try
		{
			await action().NoCtx();
		}
		catch { /* ignored */ }
	}

	public static async Task IgnoreErrorsAsync<TException>([InstantHandle] Func<Task> action)
		where TException : Exception
	{
		try
		{
			await action().NoCtx();
		}
		catch (TException) { /* ignored */ }
	}

	public static async Task ConditionalTry(
		bool useTry,
		Func<Task> tryBody,
		[InstantHandle] Action<Exception>? catchBody = null,
		[InstantHandle] Action? finallyBody = null)
	{
		if (useTry)
			try
			{
				await tryBody().NoCtx();

			}
			catch (Exception e)
			{
				catchBody?.Invoke(e);
			}
			finally
			{
				finallyBody?.Invoke();
			}
		else
			await tryBody().NoCtx();
	}
}