using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using Booster.Interfaces;
using JetBrains.Annotations;

#nullable enable

namespace Booster;

[PublicAPI]
public static class WeakEvents
{
	class WeakEventHandle : IWeakEventHandle
	{
		readonly Bucket _bucket;

		internal WeakEventHandle(Bucket bucket)
			=> _bucket = bucket;

		public void Unsubscribe() 
			=> _bucket.Unsubscribe(this);
	}

	class Bucket : IDisposable
	{
		readonly WeakReference _source;
		public Action<object,Bucket>? UnsubscribeProc;

		public bool IsAlive => _source.IsAlive && _invocationList.Any(pair => pair.Value.Item1.IsAlive);

		public Bucket(WeakReference source) 
			=> _source = source;

		readonly ConcurrentDictionary<WeakEventHandle, Tuple<WeakReference, Action<object, object, EventArgs>>> _invocationList 
			= new();

		public WeakEventHandle Subscribe(object handler, Action<object,object, EventArgs> call)
		{
			var handle = new WeakEventHandle(this);

			if (_source.IsAlive)
				_invocationList[handle] = Tuple.Create(new WeakReference(handler), call);

			return handle;
		}

		public void Unsubscribe(WeakEventHandle handle) 
			=> _invocationList.Remove(handle);

		public void Invoke(object sender, EventArgs ea)
		{
			foreach (var pair in _invocationList)
			{
				var handler = pair.Value.Item1.Target;
				if (handler != null)
					pair.Value.Item2(handler, sender, ea);
				else
					_invocationList.Remove(pair.Key);
			}
		}

		public void Dispose()
		{
			var source = _source.Target;
			if (source != null)
				UnsubscribeProc?.Invoke(source, this);
		}
	}

	static readonly ReaderWriterLockSlim _gcLock = new();

	static readonly Lazy<TimeSchedule.Subscription> _gcSubscribtion = new(
		() => TimeSchedule.GC.Subscribe(null, TimeSpan.FromMinutes(10),
			() =>
			{
				using (_gcLock.WriteLock())
					foreach (var pair in _buckets.Where(pair => !pair.Value.IsAlive).ToArray())
					{
						_buckets.Remove(pair.Key);
						pair.Value.Dispose();
					}
			}));

	static readonly ConcurrentDictionary<object, Bucket> _buckets = new();
		
	public static IWeakEventHandle Subscribe<TSource, THandler>(
		TSource source, THandler handler,
		Action<TSource, EventHandler> subscribe,
		Action<TSource, EventHandler> unsubscribe,
		Action<THandler, object, EventArgs> call)
		=> Subscribe(new object(), source, handler, subscribe, unsubscribe, call);
		
	public static IWeakEventHandle Subscribe<TSource, THandler, TEventArgs>(
		TSource source, THandler handler,
		Action<TSource, EventHandler<TEventArgs>> subscribe,
		Action<TSource, EventHandler<TEventArgs>> unsubscribe,
		Action<THandler, object, TEventArgs> call) where TEventArgs : EventArgs 
		=> Subscribe(new object(), source, handler, subscribe, unsubscribe, call);

	public static IWeakEventHandle Subscribe<TSource, THandler, TEventArgs>(
		object key, TSource source, THandler handler, 
		Action<TSource, EventHandler<TEventArgs>> subscribe, 
		Action<TSource, EventHandler<TEventArgs>> unsubscribe, 
		Action<THandler, object, TEventArgs> call) where  TEventArgs : EventArgs
	{
		_gcLock.EnterReadLock();
		try
		{
			var sourceRef = new WeakReference(source);
			var bucket = _buckets.GetOrAdd(key, _ =>
			{
				var res = new Bucket(sourceRef) {UnsubscribeProc = (s, b) => unsubscribe((TSource) s, b.Invoke)};

				subscribe(source, res.Invoke);

				return res;
			});
			return bucket.Subscribe(handler!, (me, s, ea) => call((THandler) me, s, (TEventArgs) ea));
		}
		finally
		{
			_gcLock.ExitReadLock();
			Utils.Nop(_gcSubscribtion.Value);
		}
	}

	public static IWeakEventHandle Subscribe<TSource, THandler>(
		object key, TSource source, THandler handler,
		Action<TSource, EventHandler> subscribe,
		Action<TSource, EventHandler> unsubscribe,
		Action<THandler, object, EventArgs> call)
	{
		_gcLock.EnterReadLock();
		try
		{
			var sourceRef = new WeakReference(source);
			var bucket = _buckets.GetOrAdd(key, _ =>
			{
				var res = new Bucket(sourceRef) { UnsubscribeProc = (s, b) => unsubscribe((TSource)s, b.Invoke) };

				subscribe(source, res.Invoke);

				return res;
			});
			return bucket.Subscribe(handler!, (me, s, ea) => call((THandler)me, s, ea));
		}
		finally
		{
			_gcLock.ExitReadLock();
			Utils.Nop(_gcSubscribtion.Value);
		}
	}
}