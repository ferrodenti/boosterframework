using System;
using Booster.Interfaces;
using Booster.Reflection;
using JetBrains.Annotations;

#nullable enable

namespace Booster;

public class ConvertiableDataAccessor : IDataAccessor
{
	readonly IDataAccessor _innerAccessor;
	readonly TypeEx _interface;

	[PublicAPI]
	public ConvertiableDataAccessor(IDataAccessor innerAccessor) :
		this(innerAccessor, innerAccessor.ValueType.FindInterface(typeof(IConvertiableFrom<>)))
	{
	}

	public ConvertiableDataAccessor(IDataAccessor innerAccessor, TypeEx? interfaceType)
	{
		if (interfaceType == null)
			throw new ArgumentNullException(nameof(interfaceType));

		if (!interfaceType.IsGenericType)
			throw new ArgumentException("Expected generic interface", nameof(interfaceType));

		_innerAccessor = innerAccessor;
		_interface = interfaceType;

		ValueType = _interface.GenericArguments[0];
		RealValueType = _innerAccessor.ValueType;
	}


	public TypeEx RealValueType { get; }
	public string Name => _innerAccessor.Name;
	public TypeEx ValueType { get; }
	public bool CanRead => true;
	public bool CanWrite => true;

	IDataAccessor? _valueDataAccessor;
	protected IDataAccessor? ValueDataAccessor => _valueDataAccessor ??= _interface.FindProperty("Value")?.FindImplementation(RealValueType);

	public object? GetValue(object obj)
	{
		var conv = _innerAccessor.GetValue(obj);
		return conv.With(o => ValueDataAccessor?.GetValue(o));
	}

	public bool SetValue(object obj, object? value)
	{
		var typeFrom = value?.GetType();
		if (typeFrom != null)
			if (TypeEx.Of(typeFrom).TryCast(value, _innerAccessor.ValueType, out var c) && _innerAccessor.CanWrite)
			{
				_innerAccessor.SetValue(obj, c);
				return true;
			}

		var conv = _innerAccessor.GetValue(obj);
		return conv == null && value == null || (ValueDataAccessor?.SetValue(conv, value) ?? false);
	}
}