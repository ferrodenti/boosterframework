using System;
using System.Collections.Generic;
using System.Linq;
using Booster.Hacks.System.Runtime.ExceptionServices;

#nullable enable

namespace Booster;

public class ExceptionKeeper
{
	List<ExceptionDispatchInfoHacks>? _exceptionDispatchInfos;

	public void Add(Exception exception)
	{
		var edi = ExceptionDispatchInfoHacks.Capture(exception);

		lock (this)
		{
			_exceptionDispatchInfos ??= new();
			_exceptionDispatchInfos.Add(edi);
		}
	}

	public void Throw()
	{
		if (_exceptionDispatchInfos != null)
			switch (_exceptionDispatchInfos.Count)
			{
				case 1:
					_exceptionDispatchInfos[0].Inner.Throw();
					break;
				case > 1:
					var exx = _exceptionDispatchInfos.Select(i =>
					{
						i.Exception.RestoreDispatchState(i.DispatchState);
						return i.Exception.Inner;
					});
					throw new AggregateException(exx);
			}
	}
}