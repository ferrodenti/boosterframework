using System;
using System.Collections;
using System.Collections.Generic;

namespace Booster;

public class LambdaEqComparer<T> : IEqualityComparer<T>, IEqualityComparer
{
	readonly Func<T, T, bool> _compareProc;
	readonly Func<T, int> _hashProc;

	public LambdaEqComparer(Func<T, T, bool> compareProc, Func<T, int> hashProc = null)
	{
		_compareProc = compareProc;
		_hashProc = hashProc;
	}

	public bool Equals(T x, T y)
		=> _compareProc(x, y);

	public int GetHashCode(T obj)
		=> _hashProc?.Invoke(obj) ?? obj.GetHashCode();

	public new bool Equals(object x, object y)
		=> x is T variable && y is T variable1 && Equals(variable, variable1);

	public int GetHashCode(object obj)
		=> obj.GetHashCode();
}