using System;
using System.Threading.Tasks;
using Booster.AsyncLinq;

#nullable enable

namespace Booster;

public class AsyncActionDisposable : IHybridDisposable
{
	readonly Func<Task>? _action;

	public AsyncActionDisposable(Func<Task>? action)
		=> _action = action;

#if NETSTANDARD2_1
	public async ValueTask DisposeAsync()
	{
		if (_action != null)
			await _action.Invoke().NoCtx();
	}
#else
	public Task DisposeAsync()
		=> _action?.Invoke() ?? Task.CompletedTask;
#endif
		
	public void Dispose()
		=> DisposeAsync().NoCtxWait();

	public static AsyncActionDisposable Empty => new(null);
}