using System;
using System.Runtime.CompilerServices;
using Booster.Collections;
using JetBrains.Annotations;

namespace Booster;

[PublicAPI]
public struct OSType
{
	readonly int _value;

	public string Name => _names.TryGetValue(_value, out var name) ? name : $"{Unknown}({_value})";
	
	OSType(int value, [CallerMemberName] string name = null)
	{
		_value = value;
		_names.Add(_value, name);
	}

	public override bool Equals(object obj)
		=> obj is OSType b && b._value == _value;

	public override int GetHashCode()
		=> _value;

	public override string ToString()
		=> Name;

	static Lazy<OSType> _currentOS = new(() => Environment.OSVersion.Platform switch
	{
		PlatformID.MacOSX => Osx,
		PlatformID.Unix => Linux,
		PlatformID.Win32NT => Windows,
		PlatformID.Win32S => Windows,
		PlatformID.Win32Windows => Windows,
		PlatformID.WinCE => Windows,
		PlatformID.Xbox => Windows,
		_ => Unknown
	});

	static ListDictionary<int, string> _names = new();
	public static OSType CurrentOS => _currentOS.Value;

	public static OSType Unknown = new(0);
	public static OSType Windows = new(1);
	public static OSType Linux = new(2);
	public static OSType Osx = new(4);

	public bool IsUnknown => _value == Unknown._value || !_names.ContainsKey(_value);
	public bool IsWindows => _value == Windows._value;
	public bool IsLinux => _value == Linux._value;
	public bool IsOsx => _value == Osx._value;

	public static explicit operator OSType(int value)
		=> new(value);

	public static explicit operator int(OSType osType)
		=> osType._value;

	public static bool operator ==(OSType a, OSType b)
		=> a._value == b._value;

	public static bool operator !=(OSType a, OSType b)
		=> a._value != b._value;
}