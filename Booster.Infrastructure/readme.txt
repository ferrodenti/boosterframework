﻿Базовая сборка фреймворка. Аналог MBC.Infrastructure
Заполнять будем вдумчиво, по мере необходимости

Вообще нужно обсудить, хотим ли мы видеть сборку большой и всеобъемлющей типа mscorlib или по минимуму(и куча сборок в солюшине фреймворка, каждая ссылается на .Infrastructure и выполняет строго отдельный тип задач).

Что планируется сюда добавить:
* EnumBuilder, Url, Cast, AnonTypeHelper, InstanceFactory, ThreadContext, Duplet-Triplet, HashList, HashCode, TypeSchema, MemSize, DatePeriod, IniPeriod, Period<>, Schedule из MBC
* SafeThread, CacheDictionary, ScheduleTimer из Карсов и MagService
* Объекты CodeSecurity для антиотладки. Должны выпиливаться макросами или чем-то ещё для проектов, которые надо сдавать с исходниками
* StreamCopy из FileTransfer (если тот будет показывать приемущество в скорости и гибкости перед Stream.Copy)
* Расширения основных классов mscorlib
* Getter, Setter, Creator, Cast (может быть и не сюда, а в отдельную сборку .DynamicAccess)
* IniOrm  (может быть и не сюда, а в отдельную сборку .IniOrm)
* Собственные сериализаторы (может быть и не сюда, а в отдельную сборку .Serialization)

Какие сборки-бустеры ещё возможны: (пишу пока сюда, ибо они пока не существуют)
* Web.Mvc -- расширения mvc соответственно
* Orm, Orm.SqlServer, Orm.Oracle, Orm.Postgresql
* CommonBusinessLogic -- для классов типа AgedPrice, если будут