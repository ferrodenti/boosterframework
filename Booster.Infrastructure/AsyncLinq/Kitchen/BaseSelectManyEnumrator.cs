﻿#if !NETSTANDARD2_1
using System.Threading;
using System.Threading.Tasks;

namespace Booster.AsyncLinq.Kitchen
{
	abstract class BaseSelectManyEnumerator<T1, T2, TEnumerator> : IAsyncEnumerator<T2> where TEnumerator : class 
	{
		protected readonly IAsyncEnumerator<T1> Enumerator1;
		protected TEnumerator Enumerator2;
		readonly CancellationToken _token;

		protected BaseSelectManyEnumerator(IAsyncEnumerator<T1> inner, CancellationToken token)
		{
			Enumerator1 = inner;
			_token = token;
		}

		public async Task<bool> MoveNextAsync()
		{
			while (true)
			{
				if (Enumerator2 != null)
				{
					if (await MoveNextAsync2().NoCtx())
						return true;

					await Dispose(Enumerator2).NoCtx();
					Enumerator2 = null;
				}

				if (Enumerator2 == null)
				{
					if (!await Enumerator1.MoveNextAsync().NoCtx())
						return false;

					Enumerator2 = await NextEnumerator(_token).NoCtx();
				}
			}
		}

		public abstract Task<bool> MoveNextAsync2();

		protected abstract Task<TEnumerator> NextEnumerator(CancellationToken token);

		public abstract T2 Current { get; }

		object IAsyncEnumerator.Current => Current;

		protected abstract Task Dispose(TEnumerator enumerator);

		public virtual async Task DisposeAsync()
		{
			await Dispose(Enumerator2).NoCtx();
			await Enumerator1.DisposeAsync().NoCtx();
		}
	}
}
#endif