﻿#if !NETSTANDARD2_1
using System.Threading;
using System.Threading.Tasks;

namespace Booster.AsyncLinq.Kitchen
{
	abstract class BaseSelectManyAsyncEnumerator<T1, T2> : BaseSelectManyEnumerator<T1, T2, IAsyncEnumerator<T2>>
	{
		protected BaseSelectManyAsyncEnumerator(IAsyncEnumerator<T1> inner, CancellationToken token) : base(inner, token)
		{
		}

		public override Task<bool> MoveNextAsync2() => Enumerator2.MoveNextAsync();

		public override T2 Current => Enumerator2.Current;

		protected sealed override Task Dispose(IAsyncEnumerator<T2> enumerator) 
			=> enumerator?.DisposeAsync();
	}
}
#endif