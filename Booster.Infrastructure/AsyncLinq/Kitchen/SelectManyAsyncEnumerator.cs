﻿#if !NETSTANDARD2_1
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Booster.AsyncLinq.Kitchen
{
	sealed class SelectManyAsyncEnumerator<T1, T2> : BaseSelectManyAsyncEnumerator<T1,T2>
	{
		readonly Func<T1, IAsyncEnumerable<T2>> _selector;

		public SelectManyAsyncEnumerator(IAsyncEnumerator<T1> inner,Func<T1, IAsyncEnumerable<T2>> selector, CancellationToken token) 
			: base(inner, token) 
			=> _selector = selector;

		protected override Task<IAsyncEnumerator<T2>> NextEnumerator(CancellationToken token) 
			=> Task.FromResult(_selector(Enumerator1.Current).GetAsyncEnumerator(token));
	}
}
#endif