﻿#if !NETSTANDARD2_1
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Booster.AsyncLinq.Kitchen
{
	sealed class SelectManySyncEnumerator<T1, T2> : BaseSelectManySyncEnumerator<T1,T2>
	{
		readonly Func<T1, IEnumerable<T2>> _selector;

		public SelectManySyncEnumerator(IAsyncEnumerator<T1> inner, Func<T1, IEnumerable<T2>> selector, CancellationToken token) 
			: base(inner, token) 
			=> _selector = selector;

		protected override Task<IEnumerator<T2>> NextEnumerator(CancellationToken token) 
			=> Task.FromResult(_selector(Enumerator1.Current).GetEnumerator());
	}
}
#endif