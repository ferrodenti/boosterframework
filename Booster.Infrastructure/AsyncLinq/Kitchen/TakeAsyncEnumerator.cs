#if !NETSTANDARD2_1
using System.Threading.Tasks;

namespace Booster.AsyncLinq.Kitchen
{
	sealed class TakeAsyncEnumerator<T> : IAsyncEnumerator<T>
	{
		readonly IAsyncEnumerator<T> _enumerator;
		int _count;

		public TakeAsyncEnumerator(IAsyncEnumerator<T> enumerator, int count)
		{
			_enumerator = enumerator;
			_count = count;
		}

		public Task DisposeAsync()
		{
			return _enumerator.DisposeAsync();
		}

		public Task<bool> MoveNextAsync()
			=> _count-- <= 0 
				? Task.FromResult(false) 
				: _enumerator.MoveNextAsync();

		public T Current => _enumerator.Current;

		object IAsyncEnumerator.Current => ((IAsyncEnumerator) _enumerator).Current;
	}
}
#endif
