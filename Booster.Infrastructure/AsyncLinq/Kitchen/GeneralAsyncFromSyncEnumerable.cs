﻿#if !NETSTANDARD2_1
using System;
using System.Collections.Generic;
using System.Threading;

namespace Booster.AsyncLinq.Kitchen
{
	public class GeneralAsyncFromSyncEnumerable<T1,T2> : IAsyncEnumerable<T2>
	{
		readonly IEnumerable<T1> _inital;
		readonly Func<CancellationToken, IEnumerable<T1>, IAsyncEnumerator<T2>> _enumeratorConstructor;

		public GeneralAsyncFromSyncEnumerable(IEnumerable<T1> inital, Func<CancellationToken, IEnumerable<T1>, IAsyncEnumerator<T2>> enumeratorConstructor)
		{
			_inital = inital;
			_enumeratorConstructor = enumeratorConstructor;
		}

		public IAsyncEnumerator<T2> GetAsyncEnumerator(CancellationToken token = default) 
			=> _enumeratorConstructor(token, _inital);

		// IAsyncEnumerator IAsyncEnumerable.GetAsyncEnumerator(CancellationToken token) 
		// 	=> GetAsyncEnumerator(token);
	}
}
#endif