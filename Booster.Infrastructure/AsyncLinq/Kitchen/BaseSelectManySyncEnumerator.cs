﻿#if !NETSTANDARD2_1
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Booster.AsyncLinq.Kitchen
{
	abstract class BaseSelectManySyncEnumerator<T1, T2> : BaseSelectManyEnumerator<T1, T2, IEnumerator<T2>>
	{
		protected BaseSelectManySyncEnumerator(IAsyncEnumerator<T1> inner, CancellationToken token) : base(inner, token)
		{
		}

		public override Task<bool> MoveNextAsync2() => Task.FromResult(Enumerator2.MoveNext());

		public override T2 Current => Enumerator2.Current;

		protected sealed override Task Dispose(IEnumerator<T2> enumerator)
		{
			enumerator?.Dispose();
			return Task.CompletedTask;
		}
	}
}
#endif