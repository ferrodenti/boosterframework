﻿#if !NETSTANDARD2_1
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Booster.AsyncLinq.Kitchen
{
	sealed class SelectManyTaskAsyncEnumerator<T1, T2> : BaseSelectManyAsyncEnumerator<T1,T2>
	{
		readonly Func<T1, Task<IAsyncEnumerable<T2>>> _selector2;

		public SelectManyTaskAsyncEnumerator(IAsyncEnumerator<T1> inner,Func<T1, Task<IAsyncEnumerable<T2>>> selector2, CancellationToken token) 
			: base(inner, token) 
			=> _selector2 = selector2;

		protected override async Task<IAsyncEnumerator<T2>> NextEnumerator(CancellationToken token) 
			=> (await _selector2(Enumerator1.Current).NoCtx()).GetAsyncEnumerator(token);
	}
}
#endif