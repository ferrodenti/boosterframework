﻿#if !NETSTANDARD2_1
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Booster.AsyncLinq.Kitchen
{
	sealed class SelectManyTaskSyncEnumerator<T1, T2> : BaseSelectManySyncEnumerator<T1,T2>
	{
		readonly Func<T1, Task<IEnumerable<T2>>> _selector2;

		public SelectManyTaskSyncEnumerator(IAsyncEnumerator<T1> inner,Func<T1, Task<IEnumerable<T2>>> selector2, CancellationToken token) 
			: base(inner, token) 
			=> _selector2 = selector2;

		protected override async Task<IEnumerator<T2>> NextEnumerator(CancellationToken token) 
			=> (await _selector2(Enumerator1.Current).NoCtx()).GetEnumerator();
	}
}
#endif