﻿#if !NETSTANDARD2_1
using System;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace Booster.AsyncLinq.Kitchen
{
	[PublicAPI]
	public class LambdaEnumerator<T, TState> : IAsyncEnumerator<T>
	{
		Box<TState> _state;

		public Func<Task<TState>> Init;
		public Func<TState, Task<Tuple<bool, T>>> MoveNext;
		public Func<TState, Task> Dispose;

		public async Task<bool> MoveNextAsync()
		{
			TState state;
			if (_state == null)
			{
				if (Init != null)
					state = await Init().NoCtx();

				else
					state = default;

				_state = new Box<TState>(state);
			}
			else
				state = _state.Value;

			var (moved, current) = await MoveNext(state).NoCtx();
			if (moved)
			{
				Current = current;
				return true;
			}

			return false;
		}
		
		public T Current { get; private set; }
		object IAsyncEnumerator.Current => Current;

		public Task DisposeAsync()
		{
			if (Dispose != null)
			{
				var state = _state != null ? _state.Value : default;
				return Dispose(state);
			}
			
			return Task.CompletedTask;
		}
	}
}
#endif