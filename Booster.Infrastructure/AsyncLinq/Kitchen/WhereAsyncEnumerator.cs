#if !NETSTANDARD2_1
using System;
using System.Threading.Tasks;

namespace Booster.AsyncLinq.Kitchen
{
	sealed class WhereAsyncEnumerator<T> : IAsyncEnumerator<T>
	{
		readonly Predicate<T> _predicate;
		readonly IAsyncEnumerator<T> _inner;
	
		public T Current => _inner.Current;
		object IAsyncEnumerator.Current => _inner.Current;
		
		public WhereAsyncEnumerator(IAsyncEnumerator<T> inner, Predicate<T> predicate)
		{
			_predicate = predicate;
			_inner = inner;
		}
		
		public Task DisposeAsync()
			=> _inner.DisposeAsync();

		public async Task<bool> MoveNextAsync()
		{
			while (await _inner.MoveNextAsync().NoCtx())
				if (_predicate(_inner.Current))
					return true;
			
			return false;
		}
	}
}
#endif