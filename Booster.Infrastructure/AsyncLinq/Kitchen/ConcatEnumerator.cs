﻿#if !NETSTANDARD2_1
using System.Threading;
using System.Threading.Tasks;

namespace Booster.AsyncLinq.Kitchen;
//	public class ConcatEnumerator : IAsyncEnumerator<object>
//	{
//		IAsyncEnumerator _enumerator;
//		readonly IAsyncEnumerable[] _enumerables;
//		int _count;
//		readonly CancellationToken _token;

//		public ConcatEnumerator(IAsyncEnumerator firstEnumerator, CancellationToken token, params IAsyncEnumerable[] enumerables)
//		{
//			_token = token;
//			_enumerables = enumerables;
//			_enumerator = firstEnumerator;
//		}

//		public async Task DisposeAsync()
//		{
//			if (_enumerator != null)
//				await _enumerator.DisposeAsync().NoCtx();
//		}

//		public async Task<bool> MoveNextAsync()
//		{
//			if (_enumerator == null)
//				return false;

//			while (!await _enumerator.MoveNextAsync().NoCtx())
//			{
//				await _enumerator.DisposeAsync().NoCtx();

//				if (_count >= _enumerables.Length)
//				{
//					_enumerator = null;
//					return false;
//				}

//				_enumerator = _enumerables[_count++].GetAsyncEnumerator(_token);
//			}

//			return true;
//		}

//		public object Current => _enumerator?.Current;
//	}

public class ConcatEnumerator<T> : IAsyncEnumerator<T>
{
	IAsyncEnumerator<T> _enumerator;
	readonly IAsyncEnumerable<T>[] _enumerables;
	int _count;
	readonly CancellationToken _token;

	public ConcatEnumerator(IAsyncEnumerator<T> firstEnumerator, CancellationToken token, params IAsyncEnumerable<T>[] enumerables)
	{
		_token = token;
		_enumerables = enumerables;
		_enumerator = firstEnumerator;
	}

	public async Task DisposeAsync()
	{
		if (_enumerator != null)
			await _enumerator.DisposeAsync().NoCtx();
	}

	public async Task<bool> MoveNextAsync()
	{
		if (_enumerator == null)
			return false;

		while (!await _enumerator.MoveNextAsync().NoCtx())
		{
			await _enumerator.DisposeAsync().NoCtx();

			if (_count >= _enumerables.Length)
			{
				_enumerator = null;
				return false;
			}

			_enumerator = _enumerables[_count++].GetAsyncEnumerator(_token);
		}

		return true;
	}

	public T Current => _enumerator == null ? default : _enumerator.Current;

	object IAsyncEnumerator.Current => Current;
}
#endif