﻿#if !NETSTANDARD2_1
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Booster.AsyncLinq.Kitchen
{
	sealed class SelectManySyncToAsyncEnumerator<T2> : IAsyncEnumerator<T2>
	{
		readonly IEnumerator<Task<IEnumerable<T2>>> _tasksEnumerator;
		IEnumerator<T2> _current;

		public SelectManySyncToAsyncEnumerator(IEnumerable<Task<IEnumerable<T2>>> enumerator) 
			=> _tasksEnumerator = enumerator.GetEnumerator();

		public Task DisposeAsync()
		{
			_tasksEnumerator.Dispose();
			_current?.Dispose();
			return Task.CompletedTask;
		}

		public async Task<bool> MoveNextAsync()
		{
			while (true)
			{
				if (_current == null)
				{
					if (!_tasksEnumerator.MoveNext())
						return false;

					_current = (await _tasksEnumerator.Current.NoCtx()).GetEnumerator();
				}

				if (_current.MoveNext())
					return true;

				_current.Dispose();
				_current = null;
			}
		}

		public T2 Current => _current.Current;
		object IAsyncEnumerator.Current => Current;
	}
}
#endif