#if !NETSTANDARD2_1
using System;
using System.Threading.Tasks;

namespace Booster.AsyncLinq.Kitchen
{
	sealed class WhereAsyncEnumerator2<T> : IAsyncEnumerator<T>
	{
		readonly Func<T,Task<bool>> _predicate;
		readonly IAsyncEnumerator<T> _inner;
	
		public T Current => _inner.Current;
		object IAsyncEnumerator.Current => _inner.Current;
		
		public WhereAsyncEnumerator2(IAsyncEnumerator<T> inner, Func<T,Task<bool>> predicate)
		{
			_predicate = predicate;
			_inner = inner;
		}
		
		public Task DisposeAsync()
			=> _inner.DisposeAsync();

		public async Task<bool> MoveNextAsync()
		{
			while (await _inner.MoveNextAsync().NoCtx())
				if (await _predicate(_inner.Current).NoCtx())
					return true;
			
			return false;
		}
	}
}
#endif