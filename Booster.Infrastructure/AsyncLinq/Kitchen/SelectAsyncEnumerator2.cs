#if !NETSTANDARD2_1
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Booster.AsyncLinq.Kitchen
{
	sealed class SelectAsyncEnumerator2<T1, T2> : IAsyncEnumerator<T2>
	{
		readonly Func<T1, Task<T2>> _selector;
		readonly IAsyncEnumerator<T1> _inner;

		public T2 Current { get; private set; } = default!;
		object IAsyncEnumerator.Current => Current!;
		
		public SelectAsyncEnumerator2(IAsyncEnumerator<T1> inner, 
			Func<T1, Task<T2>> selector, 
			CancellationToken cancellationToken = default)
		{
			_selector = selector;
			_inner = inner;
		}
		
		public Task DisposeAsync()
			=> _inner.DisposeAsync();

		public async Task<bool> MoveNextAsync()
		{
			var result = await _inner.MoveNextAsync().NoCtx();
			
			if (result) 
				Current = await _selector(_inner.Current).NoCtx();
			
			return result;
		}
	}
}
#endif