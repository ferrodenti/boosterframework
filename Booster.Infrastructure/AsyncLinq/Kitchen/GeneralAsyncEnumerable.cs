﻿#if !NETSTANDARD2_1
using System;
using System.Threading;

#nullable enable

namespace Booster.AsyncLinq.Kitchen;

/*public class GeneralAsyncEnumerable<T> : IAsyncEnumerable<T>
{
	readonly IAsyncEnumerable _inital;
	readonly Func<CancellationToken, IAsyncEnumerator, IAsyncEnumerator<T>> _enumeratorConstructor;
	public GeneralAsyncEnumerable(IAsyncEnumerable inital, Func<CancellationToken, IAsyncEnumerator, IAsyncEnumerator<T>> enumeratorConstructor)
	{
		_inital = inital;
		_enumeratorConstructor = enumeratorConstructor;
	}

	public IAsyncEnumerator<T> GetAsyncEnumerator(CancellationToken token = default) 
		=> _enumeratorConstructor(token, _inital?.GetAsyncEnumerator(token));

	IAsyncEnumerator IAsyncEnumerable.GetAsyncEnumerator(CancellationToken token) 
		=> GetAsyncEnumerator(token);
}*/

public class GeneralAsyncEnumerable<T1,T2> : IAsyncEnumerable<T2>
{
	readonly IAsyncEnumerable<T1> _inital;
	readonly Func<CancellationToken, IAsyncEnumerator<T1>, IAsyncEnumerator<T2>> _enumeratorConstructor;
	public GeneralAsyncEnumerable(IAsyncEnumerable<T1> inital, Func<CancellationToken, IAsyncEnumerator<T1>, IAsyncEnumerator<T2>> enumeratorConstructor)
	{
		_inital = inital;
		_enumeratorConstructor = enumeratorConstructor;
	}

	public IAsyncEnumerator<T2> GetAsyncEnumerator(CancellationToken token = default) 
		=> _enumeratorConstructor(token, _inital?.GetAsyncEnumerator(token));

	// IAsyncEnumerator IAsyncEnumerable.GetAsyncEnumerator(CancellationToken token) 
	// 	=> GetAsyncEnumerator(token);
}
#endif