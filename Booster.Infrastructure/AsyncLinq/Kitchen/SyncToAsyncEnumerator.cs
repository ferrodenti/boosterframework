#if !NETSTANDARD2_1
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Booster.AsyncLinq.Kitchen
{
	sealed class SyncToAsyncEnumerator<T> : IAsyncEnumerator<T>
	{
		readonly IEnumerator<T> _enumerator;

		public SyncToAsyncEnumerator(IEnumerator<T> enumerator)
			=> _enumerator = enumerator;

		public Task DisposeAsync()
		{
			_enumerator.Dispose();
			return Task.CompletedTask;
		}

		public Task<bool> MoveNextAsync()
		{
			return Task.FromResult(_enumerator.MoveNext());
		}

		public T Current => _enumerator.Current;

		object IAsyncEnumerator.Current => Current;
	}
}
#endif