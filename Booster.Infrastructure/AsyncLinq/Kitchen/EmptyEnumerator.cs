﻿#if !NETSTANDARD2_1
using System.Threading.Tasks;

namespace Booster.AsyncLinq.Kitchen
{
	class EmptyEnumerator<T> : IAsyncEnumerator<T>
	{
		public Task<bool> MoveNextAsync() => Task.FromResult(false);
		public T Current => default;
		object IAsyncEnumerator.Current => Current;

		public Task DisposeAsync() => Task.CompletedTask;
	}
}
#endif