﻿#if !NETSTANDARD2_1
using System.Threading.Tasks;

namespace Booster.AsyncLinq.Kitchen
{
	class CastEnumerator<T> : IAsyncEnumerator<T>
	{
		readonly IAsyncEnumerator _initial;
		public CastEnumerator(IAsyncEnumerator initial) => _initial = initial;
		public Task<bool> MoveNextAsync() => _initial.MoveNextAsync();
		public T Current => (T) _initial.Current;
		object IAsyncEnumerator.Current => Current;

		public Task DisposeAsync() => _initial.DisposeAsync();
	}
}
#endif