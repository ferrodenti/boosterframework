#if !NETSTANDARD2_1
using System;
using System.Threading.Tasks;

namespace Booster.AsyncLinq.Kitchen
{
	sealed class SelectAsyncEnumerator<T1, T2> : IAsyncEnumerator<T2>
	{
		readonly Func<T1, T2> _selector;
		readonly IAsyncEnumerator<T1> _inner;
	
		public T2 Current => _selector(_inner.Current);
		object IAsyncEnumerator.Current => Current;
		
		public SelectAsyncEnumerator(IAsyncEnumerator<T1> inner, Func<T1, T2> selector)
		{
			_selector = selector;
			_inner = inner;
		}
		
		public Task DisposeAsync()
			=> _inner.DisposeAsync();

		public Task<bool> MoveNextAsync()
			=> _inner.MoveNextAsync();
	}
}
#endif