#if NETSTANDARD2_1
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Booster.Reflection;
using JetBrains.Annotations;

#nullable enable

namespace Booster.AsyncLinq;

public class AsyncEnumerable<T> : IAsyncEnumerable<T>
{
	public static IAsyncEnumerable<T> Empty()
		=> AsyncEnumerable.Empty<T>();

	readonly Func<Task<(bool,T)>> _getter;

	public AsyncEnumerable(Func<Task<(bool,T)>> getter) 
		=> _getter = getter;

	class Enumerator : IAsyncEnumerator<T>
	{
		Func<Task<(bool,T)>>? _getter;

		public Enumerator(Func<Task<(bool,T)>> getter) 
			=> _getter = getter;

		public ValueTask DisposeAsync()
		{
			_getter = null;
			return default;
		}

		public async ValueTask<bool> MoveNextAsync()
		{
			if (_getter == null) 
				return false;

			var value = await _getter().NoCtx();
			if (value.Item1)
				Current = value.Item2;

			return value.Item1;

		}

		public T Current { get; private set; } = default!;
	}

	public IAsyncEnumerator<T> GetAsyncEnumerator(CancellationToken cancellationToken = new()) 
		=> new Enumerator(_getter);
}


[PublicAPI]
public static class AsyncEnumerable
{
	public static async IAsyncEnumerable<T> Empty<T>()
	{
		await Utils.AsyncNop().NoCtx(); 
		yield break;
	}

	#region ForEach

	[Obsolete("Используй await foreach")]
	public static async Task ForEach<T>(
		this IAsyncEnumerable<T> source,
		[InstantHandle] Func<T, ValueTask<bool>> body,
		CancellationToken token = default, 
		bool ctx = false)
	{
		await foreach (var item in source.NoCtx(token))
			if (!await body(item).ConfigureAwait(ctx))
				break;
	}

	#region ForEach<ValueTuple<...>>

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2>(
		this IAsyncEnumerable<(T1, T2)> source, 
		[InstantHandle] Action<T1, T2> body,
		CancellationToken token = default, 
		bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2), token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2>(
		this IAsyncEnumerable<(T1, T2)> source,
		[InstantHandle] Func<T1, T2, Task> body, 
		CancellationToken token = default,
		bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2), token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2>(
		this IAsyncEnumerable<(T1, T2)> source,
		[InstantHandle] Func<T1, T2, Task<bool>> body, 
		CancellationToken token = default,
		bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2), token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2, T3>(
		this IAsyncEnumerable<(T1, T2, T3)> source,
		[InstantHandle] Action<T1, T2, T3> body, 
		CancellationToken token = default,
		bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3), token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2, T3>(
		this IAsyncEnumerable<(T1, T2, T3)> source,
		[InstantHandle] Func<T1, T2, T3, Task> body,
		CancellationToken token = default, 
		bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3), token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2, T3>(
		this IAsyncEnumerable<(T1, T2, T3)> source,
		[InstantHandle] Func<T1, T2, T3, Task<bool>> body, 
		CancellationToken token = default, 
		bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3), token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2, T3, T4>(
		this IAsyncEnumerable<(T1, T2, T3, T4)> source,
		[InstantHandle] Action<T1, T2, T3, T4> body, 
		CancellationToken token = default, 
		bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4), token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2, T3, T4>(
		this IAsyncEnumerable<(T1, T2, T3, T4)> source,
		[InstantHandle] Func<T1, T2, T3, T4, Task> body, 
		CancellationToken token = default, 
		bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4), token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2, T3, T4>(
		this IAsyncEnumerable<(T1, T2, T3, T4)> source,
		[InstantHandle] Func<T1, T2, T3, T4, Task<bool>> body, 
		CancellationToken token = default, 
		bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4), token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2, T3, T4, T5>(
		this IAsyncEnumerable<(T1, T2, T3, T4, T5)> source,
		[InstantHandle] Action<T1, T2, T3, T4, T5> body, 
		CancellationToken token = default, 
		bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5), token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2, T3, T4, T5>(
		this IAsyncEnumerable<(T1, T2, T3, T4, T5)> source,
		[InstantHandle] Func<T1, T2, T3, T4, T5, Task> body,
		CancellationToken token = default,
		bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5), token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2, T3, T4, T5>(
		this IAsyncEnumerable<(T1, T2, T3, T4, T5)> source,
		[InstantHandle] Func<T1, T2, T3, T4, T5, Task<bool>> body,
		CancellationToken token = default,
		bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5), token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2, T3, T4, T5, T6>(
		this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6)> source,
		[InstantHandle] Action<T1, T2, T3, T4, T5, T6> body, 
		CancellationToken token = default,
		bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6), token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2, T3, T4, T5, T6>(
		this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6)> source,
		[InstantHandle] Func<T1, T2, T3, T4, T5, T6, Task> body,
		CancellationToken token = default,
		bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6), token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2, T3, T4, T5, T6>(
		this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6)> source,
		[InstantHandle] Func<T1, T2, T3, T4, T5, T6, Task<bool>> body,
		CancellationToken token = default,
		bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6), token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7>(
		this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7)> source,
		[InstantHandle] Action<T1, T2, T3, T4, T5, T6, T7> body, 
		CancellationToken token = default,
		bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7),
			token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7>(
		this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7)> source,
		[InstantHandle] Func<T1, T2, T3, T4, T5, T6, T7, Task> body,
		CancellationToken token = default,
		bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7),
			token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7>(
		this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7)> source,
		[InstantHandle] Func<T1, T2, T3, T4, T5, T6, T7, Task<bool>> body,
		CancellationToken token = default,
		bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7),
			token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7, T8>(
		this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7, T8)> source,
		[InstantHandle] Action<T1, T2, T3, T4, T5, T6, T7, T8> body,
		CancellationToken token = default,
		bool ctx = false)
		=> source.ForEach(
			tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7, tup.Item8),
			token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7, T8>(
		this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7, T8)> source,
		[InstantHandle] Func<T1, T2, T3, T4, T5, T6, T7, T8, Task> body,
		CancellationToken token = default,
		bool ctx = false)
		=> source.ForEach(
			tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7, tup.Item8),
			token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7, T8>(
		this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7, T8)> source,
		[InstantHandle] Func<T1, T2, T3, T4, T5, T6, T7, T8, Task<bool>> body,
		CancellationToken token = default,
		bool ctx = false)
		=> source.ForEach(
			tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7, tup.Item8),
			token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7, T8, T9>(
		this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7, T8, T9)> source,
		[InstantHandle] Action<T1, T2, T3, T4, T5, T6, T7, T8, T9> body,
		CancellationToken token = default,
		bool ctx = false)
		=> source.ForEach(
			tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7, tup.Item8,
				tup.Item9), token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7, T8, T9>(
		this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7, T8, T9)> source,
		[InstantHandle] Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, Task> body,
		CancellationToken token = default,
		bool ctx = false)
		=> source.ForEach(
			tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7, tup.Item8,
				tup.Item9), token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7, T8, T9>(
		this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7, T8, T9)> source,
		[InstantHandle] Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, Task<bool>> body,
		CancellationToken token = default, 
		bool ctx = false)
		=> source.ForEach(
			tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7, tup.Item8,
				tup.Item9), token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(
		this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10)> source,
		[InstantHandle] Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> body,
		CancellationToken token = default,
		bool ctx = false)
		=> source.ForEach(
			tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7, tup.Item8,
				tup.Item9, tup.Item10), token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(
		this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10)> source,
		[InstantHandle] Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, Task> body, 
		CancellationToken token = default,
		bool ctx = false)
		=> source.ForEach(
			tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7, tup.Item8,
				tup.Item9, tup.Item10), token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(
		this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10)> source,
		[InstantHandle] Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, Task<bool>> body,
		CancellationToken token = default,
		bool ctx = false)
		=> source.ForEach(
			tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7, tup.Item8,
				tup.Item9, tup.Item10), token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(
		this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11)> source,
		[InstantHandle] Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> body,
		CancellationToken token = default, 
		bool ctx = false)
		=> source.ForEach(
			tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7, tup.Item8,
				tup.Item9, tup.Item10, tup.Item11), token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(
		this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11)> source,
		[InstantHandle] Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, Task> body,
		CancellationToken token = default,
		bool ctx = false)
		=> source.ForEach(
			tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7, tup.Item8,
				tup.Item9, tup.Item10, tup.Item11), token);

	[Obsolete("Используй await foreach")]
	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(
		this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11)> source,
		[InstantHandle] Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, Task<bool>> body,
		CancellationToken token = default, 
		bool ctx = false)
		=> source.ForEach(
			tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7, tup.Item8,
				tup.Item9, tup.Item10, tup.Item11), token);

	#endregion

	[Obsolete("Используй await foreach")]
	public static async Task ForEach<T>(
		this IAsyncEnumerable<T> source,
		[InstantHandle] Func<T, Task> body,
		CancellationToken token = default, 
		bool ctx = false)
	{
		await foreach (var item in source.NoCtx(token))
			await body(item).ConfigureAwait(ctx);
	}

	[Obsolete("Используй await foreach")]
	public static async Task ForEach<T>(
		this IAsyncEnumerable<T> source,
		[InstantHandle] Func<T, bool> body,
		CancellationToken token = default, 
		bool ctx = false)
	{
		await foreach (var item in source.NoCtx(token))
			if(!body(item))
				break;
	}

	[Obsolete("Используй await foreach")]
	public static async Task ForEach<T>(this IAsyncEnumerable<T> source, [InstantHandle] Action<T> body,
		CancellationToken token = default,
		bool ctx = false)
	{
		await foreach (var item in source.NoCtx(token))
			body(item);
	}

	#endregion

	#region AsIEnumerable

	public static IEnumerable<T> AsIEnumerable<T>(
		this IAsyncEnumerable<T>? source,
		CancellationToken token = default)
	{
		if (source == null)
			yield break;

		var enumerator = source.GetAsyncEnumerator(token);
		try
		{
			while (enumerator.MoveNextAsync().NoCtxResult())
				yield return enumerator.Current;
		}
		finally
		{
			enumerator.DisposeAsync().NoCtxWait();
		}
	}

	#endregion

	#region AsIAsyncEnumerable

	public static async IAsyncEnumerable<T> AsIAsyncEnumerable<T>(this IEnumerable<T>? source)
	{
		if (source == null)
			yield break;
		
		var enumerator = source.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
				yield return enumerator.Current;
		}
		finally
		{
			await Task.Delay(0).NoCtx();
			enumerator.Dispose();
		}
	}

	#endregion

	#region ToList

	public static async Task<List<T>> ToList<T>(
		this IAsyncEnumerable<T> source, 
		CancellationToken token = default)
	{
		var result = new List<T>();
		await foreach (var item in source.NoCtx(token)) 
			result.Add(item);

		return result;
	}

	#endregion

	#region ToDictionary

	public static Task<Dictionary<TKey, TSource>> ToDictionary<TSource, TKey>(
		this IAsyncEnumerable<TSource> source,
		Func<TSource, TKey>? keySelector,
		IEqualityComparer<TKey>? comparer = null,
		CancellationToken token = default)
		=> source.ToDictionary(keySelector, i => i, comparer, token);

	public static Task<Dictionary<TKey, TElement>> ToDictionary<TSource, TKey, TElement>(
		this IAsyncEnumerable<TSource> source,
		Func<TSource, TKey>? keySelector,
		Func<TSource, TElement>? elementSelector,
		IEqualityComparer<TKey>? comparer = null,
		CancellationToken token = default)
		=> source.ToDictionary(keySelector, elementSelector, comparer, () => new Dictionary<TKey, TElement>(comparer), token);
	
	public static async Task<TDict> ToDictionary<TSource, TKey, TElement, TDict>(
		this IAsyncEnumerable<TSource> source,
		Func<TSource, TKey>? keySelector = null,
		Func<TSource, TElement>? elementSelector = null,
		IEqualityComparer<TKey>? comparer = null,
		Func<TDict>? creator = null,
		CancellationToken token = default)
			where TDict: IDictionary<TKey, TElement>
	{
		if (source == null)
			throw new ArgumentNullException(nameof(source));

		if (keySelector == null)
		{
			if (typeof(TKey) != typeof(TElement))
				throw new ArgumentNullException(nameof(keySelector));

			keySelector = item => (TKey)(object)item!;
		}

		if (elementSelector == null)
		{
			if (typeof(TSource) != typeof(TElement))
				throw new ArgumentNullException(nameof(elementSelector));

			elementSelector = item => (TElement)(object)item!;
		}

		TDict result;
		if (creator != null)
			result = creator();
		else
			result = (TDict)TypeEx.Get<TDict>().Create(comparer);
		
		await foreach (var item in source.NoCtx(token).ConfigureAwait(false))
			result[keySelector(item)] = elementSelector(item);

		return result;
	}

	#endregion

	#region ToHashSet

	public static async Task<HashSet<T>> ToHashSet<T>(
		this IAsyncEnumerable<T> source,
		IEqualityComparer<T>? comparer = null, 
		CancellationToken token = default)
	{
		var result = new HashSet<T>(comparer);

		await foreach (var item in source.NoCtx(token))
			result.Add(item);

		return result;
	}

	#endregion


	public static async Task<T[]> ToArray<T>(
		this IAsyncEnumerable<T> source, 
		CancellationToken token = default)
		=> (await source.ToList(token).NoCtx()).ToArray();

	#region FirstOrDefault

	public static async Task<T?> FirstOrDefaultAsync<T>(
		this IAsyncEnumerable<T> source, 
		CancellationToken token = default)
	{
		await foreach (var item in source.NoCtx(token))
			return item;

		return default;
	}

	public static async Task<T?> FirstOrDefaultAsync<T>(
		this IAsyncEnumerable<T> source, 
		[InstantHandle] Func<T, bool> predicate,
		CancellationToken token = default)
	{
		await foreach (var item in source.NoCtx(token))
			if (predicate(item))
				return item;

		return default;
	}

	public static async Task<T?> FirstOrDefaultAsync<T>(
		this IAsyncEnumerable<T> source, 
		[InstantHandle] Func<T, Task<bool>> predicate,
		CancellationToken token = default)
	{
		await foreach (var item in source.NoCtx(token))
			if (await predicate(item).NoCtx())
				return item;

		return default;
	}

	#endregion

	#region Any

	public static async Task<bool> Any<T>(
		this IAsyncEnumerable<T> source,
		CancellationToken token = default)
	{
		await foreach (var _ in source.NoCtx(token))
			return true;

		return false;
	}

	public static async Task<bool> Any<T>(
		this IAsyncEnumerable<T> source,
		[InstantHandle] Func<T, bool> predicate,
		CancellationToken token = default)
	{
		await foreach (var item in source.NoCtx(token))
			if (predicate(item))
				return true;

		return false;
	}

	public static async Task<bool> Any<T>(
		this IAsyncEnumerable<T> source,
		[InstantHandle] Func<T, Task<bool>> predicate,
		CancellationToken token = default)
	{
		await foreach (var item in source.NoCtx(token))
			if (await predicate(item).NoCtx())
				return true;

		return false;
	}
	#endregion

	#region All

	public static async Task<bool> All<T>(
		this IAsyncEnumerable<T> source,
		[InstantHandle] Func<T, bool> predicate,
		CancellationToken token = default)
	{
		await foreach (var item in source.NoCtx(token))
			if (!predicate(item))
				return false;

		return true;
	}

	public static async Task<bool> All<T>(
		this IAsyncEnumerable<T> source,
		[InstantHandle] Func<T, Task<bool>> predicate,
		CancellationToken token = default)
	{
		await foreach (var item in source.NoCtx(token))
			if (!await predicate(item).NoCtx())
				return false;

		return true;
	}
	#endregion

	public static async IAsyncEnumerable<T> WhereSome<T>(
		this IAsyncEnumerable<T?> source, 
		[EnumeratorCancellation] CancellationToken token = default)
	{
		await foreach (var item in source.NoCtx(token))
			if (item != null)
				yield return item;
	}

	public static async IAsyncEnumerable<T> WhereSome<T>(
		this IAsyncEnumerable<T> source,
		[InstantHandle] Predicate<T> predicate, 
		[EnumeratorCancellation] CancellationToken token = default)
	{
		await foreach (var item in source.NoCtx(token))
			if (predicate(item))
				yield return item;
	}

	public static async IAsyncEnumerable<T> Where<T>(
		this IAsyncEnumerable<T> source,
		[InstantHandle] Func<T, Task<bool>> predicate,
		[EnumeratorCancellation] CancellationToken token = default)
	{
		await foreach (var item in source.NoCtx(token))
			if (await predicate(item).NoCtx())
				yield return item;
	}

	public static async IAsyncEnumerable<T> Where<T>(
		this IAsyncEnumerable<T> source,
		[InstantHandle] Predicate<T> predicate, 
		[EnumeratorCancellation] CancellationToken token = default)
	{
		await foreach (var item in source.NoCtx(token))
			if (predicate(item))
				yield return item;
	}

	

	public static async IAsyncEnumerable<TResult> Select<TSource, TResult>(
		this IAsyncEnumerable<TSource> source,
		[InstantHandle] Func<TSource, Task<TResult>> selector,
		[EnumeratorCancellation] CancellationToken token = default)
	{
		await foreach (var item in source.NoCtx(token))
			yield return await selector(item).NoCtx();
	}

	public static async IAsyncEnumerable<TResult> Select<TSource, TResult>(
		this IAsyncEnumerable<TSource> source,
		[InstantHandle] Func<TSource, TResult> selector, 
		[EnumeratorCancellation] CancellationToken token = default)
	{
		await foreach (var item in source.NoCtx(token))
			yield return selector(item);
	}

	public static async IAsyncEnumerable<TResult> SelectNonDefault<TSource, TResult>(
		this IAsyncEnumerable<TSource> source,
		[InstantHandle] Func<TSource, Task<TResult?>> selector,
		[EnumeratorCancellation] CancellationToken token = default)
	{
		await foreach (var item in source.NoCtx(token))
		{
			var result = await selector(item).NoCtx();
			if (result != null)
				yield return result;
		}
	}

	public static async IAsyncEnumerable<TResult> SelectNonDefault<TSource, TResult>(
		this IAsyncEnumerable<TSource> source,
		[InstantHandle] Func<TSource, TResult?> selector, 
		[EnumeratorCancellation] CancellationToken token = default)
	{
		await foreach (var item in source.NoCtx(token))
		{
			var result = selector(item);
			if (result != null)
				yield return result;
		}
	}

	public static async IAsyncEnumerable<T> Distinct<T>(
		this IAsyncEnumerable<T> source,
		IEqualityComparer<T> comparer,
		[EnumeratorCancellation] CancellationToken token = default)
	{
		var hashset = new HashSet<T>(comparer);
		await foreach (var item in source.NoCtx(token))
			if (hashset.Add(item))
				yield return item;
	}

	public static IAsyncEnumerable<T> Distinct<T>(
		this IAsyncEnumerable<T> source,
		CancellationToken token = default)
		=> Distinct(source, EqualityComparer<T>.Default, token);
	
	public static async IAsyncEnumerable<T> Distinct<T,TKey>(
		this IAsyncEnumerable<T> source,
		Func<T, TKey> selector,
		IEqualityComparer<TKey> comparer,
		[EnumeratorCancellation] CancellationToken token = default)
	{
		var hashset = new HashSet<TKey>(comparer);
		await foreach (var item in source.NoCtx(token))
		{
			var key = selector(item);
			if (hashset.Add(key))
				yield return item;
		}
	}

	public static IAsyncEnumerable<T> Distinct<T, TKey>(
		this IAsyncEnumerable<T> source,
		Func<T, TKey> selector,
		CancellationToken token = default)
		=> Distinct(source, selector, EqualityComparer<TKey>.Default, token);

	// public static async IAsyncEnumerable<TResult> Cast<TResult>(this IAsyncEnumerable source, 
	// 	[EnumeratorCancellation] CancellationToken token = default) 
	// {
	// 	await foreach (var item in source.NoCtx(token))
	// 		yield return (TResult)item;
	// }
	public static async IAsyncEnumerable<TResult> Cast<TResult>(
		this IAsyncEnumerable<object> source, 
		[EnumeratorCancellation] CancellationToken token = default) 
	{
		await foreach (var item in source.NoCtx(token))
			yield return (TResult)item;
	}
	
	public static async IAsyncEnumerable<TResult> Cast<TSource, TResult>(
		this IAsyncEnumerable<TSource> source, 
		[EnumeratorCancellation] CancellationToken token = default) 
	{
		await foreach (var item in source.NoCtx(token))
			yield return (TResult)(object)item!;
	}
	
	#region SelectMany

	public static async IAsyncEnumerable<TResult> SelectMany<TSource, TResult>(
		this IAsyncEnumerable<TSource> source,
		[InstantHandle] Func<TSource, Task<IAsyncEnumerable<TResult>>> selector, 
		[EnumeratorCancellation] CancellationToken token = default)
	{
		await foreach (var item in source.NoCtx(token))
		await foreach (var item2 in (await selector(item).NoCtx()).WithCancellation(token))
			yield return item2;
	}

	public static async IAsyncEnumerable<TResult> SelectMany<TSource, TResult>(
		this IAsyncEnumerable<TSource> source,
		[InstantHandle] Func<TSource, IAsyncEnumerable<TResult>> selector,
		[EnumeratorCancellation] CancellationToken token = default)
	{
		await foreach (var item in source.NoCtx(token))
		await foreach (var item2 in selector(item).WithCancellation(token))
			yield return item2;
	}

	public static async IAsyncEnumerable<TResult> SelectMany<TSource, TResult>(
		this IAsyncEnumerable<TSource> source,
		[InstantHandle] Func<TSource, Task<IEnumerable<TResult>>> selector,
		[EnumeratorCancellation] CancellationToken token = default)
	{
		await foreach (var item in source.NoCtx(token))
		foreach (var item2 in await selector(item).NoCtx())
			yield return item2;
	}

	public static async IAsyncEnumerable<TResult> SelectMany<TSource, TResult>(
		this IAsyncEnumerable<TSource> source,
		[InstantHandle] Func<TSource, IEnumerable<TResult>> selector,
		[EnumeratorCancellation] CancellationToken token = default)
	{
		await foreach (var item in source.NoCtx(token))
		foreach (var item2 in selector(item))
			yield return item2;
	}

	

	#endregion

	public static async IAsyncEnumerable<T> Concat<T>(
		this IAsyncEnumerable<T> source,
		params IAsyncEnumerable<T>[] other)
	{
		await foreach (var item in source)
			yield return item;

		foreach (var ie in other)
		await foreach (var item in ie)
			yield return item;
	}
	
	public static async IAsyncEnumerable<T> Concat<T>(
		this IAsyncEnumerable<T> source, 
		[EnumeratorCancellation] CancellationToken token,
		params IAsyncEnumerable<T>[] other)
	{
		await foreach (var item in source.NoCtx(token))
			yield return item;

		foreach (var ie in other)
		await foreach (var item in ie.WithCancellation(token))
			yield return item;
	}

	public static async IAsyncEnumerable<T> AppendBefore<T>(
		this IAsyncEnumerable<T> source, 
		Func<T> first, 
		[EnumeratorCancellation] CancellationToken token = default)
	{
		yield return first();

		await foreach (var item in source.NoCtx(token))
			yield return item;
	}

	public static async IAsyncEnumerable<T> AppendAfter<T>(
		this IAsyncEnumerable<T> source, 
		Func<T> last, 
		[EnumeratorCancellation] CancellationToken token = default)
	{
		await foreach (var item in source.NoCtx(token))
			yield return item;

		yield return last();
	}

	#region OrderBy

	public static async Task<IEnumerable<TSource>> OrderByDescending<TSource, TKey>(
		this IAsyncEnumerable<TSource> source, 
		Func<TSource, TKey> keySelector,
		IComparer<TKey>? comparer = null, 
		CancellationToken token = default)
		=> (await source.ToList(token).NoCtx()).OrderByDescending(keySelector, comparer);

	public static async Task<IEnumerable<TSource>> OrderBy<TSource, TKey>(
		this IAsyncEnumerable<TSource> source,
		Func<TSource, TKey> keySelector, 
		IComparer<TKey>? comparer = null, 
		CancellationToken token = default)
		=> (await source.ToList(token).NoCtx()).OrderBy(keySelector, comparer);

	public static async Task<IEnumerable<TSource>> OrderByDescending<TSource, TKey>(
		this IAsyncEnumerable<TSource> source, 
		Func<TSource, Task<TKey>> keySelector,
		IComparer<TKey>? comparer = null, 
		CancellationToken token = default)
		=> await (await source.ToList(token).NoCtx()).OrderByDescending(keySelector, comparer).NoCtx();

	public static async Task<IEnumerable<TSource>> OrderBy<TSource, TKey>(
		this IAsyncEnumerable<TSource> source,
		Func<TSource, Task<TKey>> keySelector, 
		IComparer<TKey>? comparer = null,
		CancellationToken token = default)
		=> await (await source.ToList(token).NoCtx()).OrderBy(keySelector, comparer).NoCtx();

	#endregion


	public static async IAsyncEnumerable<TSource> Skip<TSource>(
		this IAsyncEnumerable<TSource> source, 
		int count, 
		[EnumeratorCancellation] CancellationToken token = default)
	{
		await foreach (var item in source.NoCtx(token))
			if (--count < 0)
				yield return item;
	}

	public static async IAsyncEnumerable<TSource> Take<TSource>(
		this IAsyncEnumerable<TSource> source, 
		int count, 
		[EnumeratorCancellation] CancellationToken token = default)
	{
		await foreach (var item in source.NoCtx(token))
			if (--count >= 0)
				yield return item;
	}

	public static ConfiguredCancelableAsyncEnumerable<TSource> NoCtx<TSource>(this IAsyncEnumerable<TSource> source)
		=> source.ConfigureAwait(false);

	public static ConfiguredCancelableAsyncEnumerable<TSource> NoCtx<TSource>(
		this IAsyncEnumerable<TSource> source,
		CancellationToken token)
		=> source.ConfigureAwait(false).WithCancellation(token);

	#region Partition

	#endregion
}

#endif