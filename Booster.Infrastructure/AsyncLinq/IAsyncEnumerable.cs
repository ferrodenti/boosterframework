#if !NETSTANDARD2_1
using System.Threading;

namespace Booster.AsyncLinq
{
	// public interface IAsyncEnumerable
	// {
	// 	IAsyncEnumerator GetAsyncEnumerator(CancellationToken token = default);
	// }

	public interface IAsyncEnumerable<out T> //: IAsyncEnumerable
	{
		IAsyncEnumerator<T> GetAsyncEnumerator(CancellationToken token = default);
	}
}
#endif