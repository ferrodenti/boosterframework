#if !NETSTANDARD2_1
using System;
//using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Booster.AsyncLinq.Kitchen;
//using Booster.Reflection;
using JetBrains.Annotations;

#nullable enable

namespace Booster.AsyncLinq;

[PublicAPI]
public static class AsyncEnumerable
{
	public static IAsyncEnumerable<T> Empty<T>()
		=> new GeneralAsyncEnumerable<object, T>(null, (_, _) => new EmptyEnumerator<T>());

	/*static readonly Lazy<Method> _emptyMethod = new Lazy<Method>(() => TypeEx.Get(typeof(AsyncEnumerable)).FindMethod(
		new ReflectionFilter("Empty")
		{
			IsStatic = true,
			ParametersCount = 0
		}));

	public static IAsyncEnumerable Empty(TypeEx type) 
		=> (IAsyncEnumerable) _emptyMethod.Value.MakeGenericMethod(type).Invoke(null);

	public static IAsyncEnumerable FromEnumerable(IEnumerable source) 
		=> new GeneralAsyncEnumerable<object>(null, (_, __) => new SyncroniousEnumerator<object>(source.Cast<object>().GetEnumerator()));*/

	public static IAsyncEnumerable<T> FromEnumerable<T>(IEnumerable<T> source)
		=> new GeneralAsyncEnumerable<object, T>(null, (_, _) => new SyncroniousEnumerator<T>(source.GetEnumerator()));


	#region ForEach


	/*public static async Task ForEach(this IAsyncEnumerable source, [InstantHandle] Func<object, bool> body, CancellationToken token = default, bool ctx = false)
	{
		var enumerator = source.GetAsyncEnumerator(token);
		try
		{
			while (await enumerator.MoveNextAsync().ConfigureAwait(ctx))
				if (!body(enumerator.Current))
					break;
		}
		finally
		{
			await enumerator.DisposeAsync().NoCtx();
		}
	}

	public static async Task ForEach(this IAsyncEnumerable source, [InstantHandle] Action<object> body, CancellationToken token = default, bool ctx = false)
	{
		var enumerator = source.GetAsyncEnumerator(token);
		try
		{
			while (await enumerator.MoveNextAsync().ConfigureAwait(ctx))
				body(enumerator.Current);
		}
		finally
		{
			await enumerator.DisposeAsync().NoCtx();
		}
	}*/

	public static async Task ForEach<T>(this IAsyncEnumerable<T> source, [InstantHandle] Func<T, Task<bool>> body, CancellationToken token = default, bool ctx = false)
	{
		var enumerator = source.GetAsyncEnumerator(token);
		try
		{
			while (await enumerator.MoveNextAsync().ConfigureAwait(ctx))
				if (!await body(enumerator.Current).ConfigureAwait(ctx))
					break;
		}
		finally
		{
			await enumerator.DisposeAsync().NoCtx();
		}
	}

	#region ForEach<ValueTuple<...>>

	public static Task ForEach<T1, T2>(this IAsyncEnumerable<(T1, T2)> source, [InstantHandle] Action<T1, T2> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2), token);

	public static Task ForEach<T1, T2>(this IAsyncEnumerable<(T1, T2)> source, [InstantHandle] Func<T1, T2, Task> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2), token);

	public static Task ForEach<T1, T2>(this IAsyncEnumerable<(T1, T2)> source, [InstantHandle] Func<T1, T2, Task<bool>> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2), token);

	public static Task ForEach<T1, T2, T3>(this IAsyncEnumerable<(T1, T2, T3)> source, [InstantHandle] Action<T1, T2, T3> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3), token);

	public static Task ForEach<T1, T2, T3>(this IAsyncEnumerable<(T1, T2, T3)> source, [InstantHandle] Func<T1, T2, T3, Task> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3), token);

	public static Task ForEach<T1, T2, T3>(this IAsyncEnumerable<(T1, T2, T3)> source, [InstantHandle] Func<T1, T2, T3, Task<bool>> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3), token);

	public static Task ForEach<T1, T2, T3, T4>(this IAsyncEnumerable<(T1, T2, T3, T4)> source, [InstantHandle] Action<T1, T2, T3, T4> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4), token);

	public static Task ForEach<T1, T2, T3, T4>(this IAsyncEnumerable<(T1, T2, T3, T4)> source, [InstantHandle] Func<T1, T2, T3, T4, Task> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4), token);

	public static Task ForEach<T1, T2, T3, T4>(this IAsyncEnumerable<(T1, T2, T3, T4)> source, [InstantHandle] Func<T1, T2, T3, T4, Task<bool>> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4), token);

	public static Task ForEach<T1, T2, T3, T4, T5>(this IAsyncEnumerable<(T1, T2, T3, T4, T5)> source, [InstantHandle] Action<T1, T2, T3, T4, T5> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5), token);

	public static Task ForEach<T1, T2, T3, T4, T5>(this IAsyncEnumerable<(T1, T2, T3, T4, T5)> source, [InstantHandle] Func<T1, T2, T3, T4, T5, Task> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5), token);

	public static Task ForEach<T1, T2, T3, T4, T5>(this IAsyncEnumerable<(T1, T2, T3, T4, T5)> source, [InstantHandle] Func<T1, T2, T3, T4, T5, Task<bool>> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5), token);

	public static Task ForEach<T1, T2, T3, T4, T5, T6>(this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6)> source, [InstantHandle] Action<T1, T2, T3, T4, T5, T6> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6), token);

	public static Task ForEach<T1, T2, T3, T4, T5, T6>(this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6)> source, [InstantHandle] Func<T1, T2, T3, T4, T5, T6, Task> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6), token);

	public static Task ForEach<T1, T2, T3, T4, T5, T6>(this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6)> source, [InstantHandle] Func<T1, T2, T3, T4, T5, T6, Task<bool>> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6), token);

	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7>(this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7)> source, [InstantHandle] Action<T1, T2, T3, T4, T5, T6, T7> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7), token);

	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7>(this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7)> source, [InstantHandle] Func<T1, T2, T3, T4, T5, T6, T7, Task> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7), token);

	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7>(this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7)> source, [InstantHandle] Func<T1, T2, T3, T4, T5, T6, T7, Task<bool>> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7), token);

	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7, T8>(this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7, T8)> source, [InstantHandle] Action<T1, T2, T3, T4, T5, T6, T7, T8> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7, tup.Item8), token);

	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7, T8>(this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7, T8)> source, [InstantHandle] Func<T1, T2, T3, T4, T5, T6, T7, T8, Task> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7, tup.Item8), token);

	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7, T8>(this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7, T8)> source, [InstantHandle] Func<T1, T2, T3, T4, T5, T6, T7, T8, Task<bool>> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7, tup.Item8), token);

	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7, T8, T9>(this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7, T8, T9)> source, [InstantHandle] Action<T1, T2, T3, T4, T5, T6, T7, T8, T9> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7, tup.Item8, tup.Item9), token);

	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7, T8, T9>(this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7, T8, T9)> source, [InstantHandle] Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, Task> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7, tup.Item8, tup.Item9), token);

	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7, T8, T9>(this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7, T8, T9)> source, [InstantHandle] Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, Task<bool>> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7, tup.Item8, tup.Item9), token);

	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10)> source, [InstantHandle] Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7, tup.Item8, tup.Item9, tup.Item10), token);

	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10)> source, [InstantHandle] Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, Task> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7, tup.Item8, tup.Item9, tup.Item10), token);

	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10)> source, [InstantHandle] Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, Task<bool>> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7, tup.Item8, tup.Item9, tup.Item10), token);

	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11)> source, [InstantHandle] Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7, tup.Item8, tup.Item9, tup.Item10, tup.Item11), token);

	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11)> source, [InstantHandle] Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, Task> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7, tup.Item8, tup.Item9, tup.Item10, tup.Item11), token);

	public static Task ForEach<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(this IAsyncEnumerable<(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11)> source, [InstantHandle] Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, Task<bool>> body, CancellationToken token = default, bool ctx = false)
		=> source.ForEach(tup => body(tup.Item1, tup.Item2, tup.Item3, tup.Item4, tup.Item5, tup.Item6, tup.Item7, tup.Item8, tup.Item9, tup.Item10, tup.Item11), token);

	#endregion

	public static async Task ForEach<T>(this IAsyncEnumerable<T> source, [InstantHandle] Func<T, Task> body, CancellationToken token = default, bool ctx = false)
	{
		var enumerator = source.GetAsyncEnumerator(token);
		try
		{
			while (await enumerator.MoveNextAsync().ConfigureAwait(ctx))
				await body(enumerator.Current).ConfigureAwait(ctx);
		}
		finally
		{
			await enumerator.DisposeAsync().NoCtx();
		}
	}

	public static async Task ForEach<T>(this IAsyncEnumerable<T> source, [InstantHandle] Func<T, bool> body, CancellationToken token = default, bool ctx = false)
	{
		var enumerator = source.GetAsyncEnumerator(token);
		try
		{
			while (await enumerator.MoveNextAsync().ConfigureAwait(ctx))
				if (!body(enumerator.Current))
					break;
		}
		finally
		{
			await enumerator.DisposeAsync().NoCtx();
		}
	}

	public static async Task ForEach<T>(this IAsyncEnumerable<T> source, [InstantHandle] Action<T> body, CancellationToken token = default, bool ctx = false)
	{
		var enumerator = source.GetAsyncEnumerator(token);
		try
		{
			while (await enumerator.MoveNextAsync().ConfigureAwait(ctx))
				body(enumerator.Current);
		}
		finally
		{
			await enumerator.DisposeAsync().NoCtx();
		}
	}

	#endregion

	#region AsIEnumerable

	/*public static IEnumerable AsIEnumerable(this IAsyncEnumerable source, CancellationToken token = default)
	{
		var enumerator = source.GetAsyncEnumerator(token);
		try
		{
			while (enumerator.MoveNextAsync().NoCtxResult())
				yield return enumerator.Current;
		}
		finally
		{
			enumerator.DisposeAsync().NoCtxWait();
		}
	}*/

	public static IEnumerable<T> AsIEnumerable<T>(this IAsyncEnumerable<T> source, CancellationToken token = default)
	{
		var enumerator = source.GetAsyncEnumerator(token);
		try
		{
			while (enumerator.MoveNextAsync().NoCtxResult())
				yield return enumerator.Current;
		}
		finally
		{
			enumerator.DisposeAsync().NoCtxWait();
		}
	}

	#endregion

	#region ToList


	/*public static async Task<List<object>> ToList(this IAsyncEnumerable source, CancellationToken token = default)
	{
		var result = new List<object>();

		var enumerator = source.GetAsyncEnumerator(token);
		try
		{
			while (await enumerator.MoveNextAsync().NoCtx())
				result.Add( enumerator.Current);
		}
		finally
		{
			await enumerator.DisposeAsync().NoCtx();
		}

		return result;
	}*/

	public static async Task<List<T>> ToList<T>(this IAsyncEnumerable<T> source, CancellationToken token = default)
	{
		var result = new List<T>();

		var enumerator = source.GetAsyncEnumerator(token);
		try
		{
			while (await enumerator.MoveNextAsync().NoCtx())
				result.Add(enumerator.Current);
		}
		finally
		{
			await enumerator.DisposeAsync().NoCtx();
		}

		return result;
	}

	#endregion

	#region ToDictionary

	public static Task<Dictionary<TKey, TSource>> ToDictionary<TSource, TKey>(this IAsyncEnumerable<TSource> source,
		Func<TSource, TKey> keySelector,
		IEqualityComparer<TKey>? comparer = null,
		CancellationToken token = default)
		=> source.ToDictionary(keySelector, i => i, comparer, token);

	public static async Task<Dictionary<TKey, TElement>> ToDictionary<TSource, TKey, TElement>(this IAsyncEnumerable<TSource> source,
		Func<TSource, TKey> keySelector,
		Func<TSource, TElement> elementSelector,
		IEqualityComparer<TKey>? comparer = null,
		CancellationToken token = default)
	{
		if (source == null)
			throw new ArgumentNullException(nameof(source));
		if (keySelector == null)
			throw new ArgumentNullException(nameof(keySelector));
		if (elementSelector == null)
			throw new ArgumentNullException(nameof(elementSelector));

		var result = new Dictionary<TKey, TElement>(comparer);

		var enumerator = source.GetAsyncEnumerator(token);
		try
		{
			while (await enumerator.MoveNextAsync().NoCtx())
			{
				var item = enumerator.Current;
				result[keySelector(item)] = elementSelector(item);
			}
		}
		finally
		{
			await enumerator.DisposeAsync().NoCtx();
		}

		return result;
	}

	#endregion

	#region ToHashSet


	/*public static async Task<HashSet<object>> ToHashSet(this IAsyncEnumerable source, IEqualityComparer<object> comparer = null, CancellationToken token = default)
	{
		var result = new HashSet<object>(comparer);

		var enumerator = source.GetAsyncEnumerator(token);
		try
		{
			while (await enumerator.MoveNextAsync().NoCtx())
				result.Add( enumerator.Current);
		}
		finally
		{
			await enumerator.DisposeAsync().NoCtx();
		}

		return result;
	}*/

	public static async Task<HashSet<T>> ToHashSet<T>(
		this IAsyncEnumerable<T> source, 
		IEqualityComparer<T>? comparer = null, 
		CancellationToken token = default)
	{
		var result = new HashSet<T>(comparer);

		var enumerator = source.GetAsyncEnumerator(token);
		try
		{
			while (await enumerator.MoveNextAsync().NoCtx())
				result.Add(enumerator.Current);
		}
		finally
		{
			await enumerator.DisposeAsync().NoCtx();
		}

		return result;
	}

	#endregion


	//public static async Task<object[]> ToArray(this IAsyncEnumerable source) 
	//    => (await source.ToList().NoCtx()).ToArray();

	public static async Task<T[]> ToArray<T>(this IAsyncEnumerable<T> source)
		=> (await source.ToList().NoCtx()).ToArray();


	//public static IAsyncEnumerable<T> Cast<T>(this IAsyncEnumerable source) 
	//    => new GeneralAsyncEnumerable<T>(source, (token, enu) => new CastEnumerator<T>(enu));

	public static IAsyncEnumerable<TResult> Cast<TSource, TResult>(this IAsyncEnumerable<TSource> source)
		=> new GeneralAsyncEnumerable<TSource, TResult>(source, (_, enu) => new CastEnumerator<TResult>(enu));

	public static IAsyncEnumerable<TResult> Cast<TResult>(this IAsyncEnumerable<object> source)
		=> new GeneralAsyncEnumerable<object, TResult>(source, (_, enu) => new CastEnumerator<TResult>(enu));

	#region FirstOrDefault

	public static async Task<T?> FirstOrDefault<T>(this IAsyncEnumerable<T> source, CancellationToken token)
	{
		var enumerator = source.GetAsyncEnumerator(token);
		try
		{
			return await enumerator.MoveNextAsync().NoCtx()
				? enumerator.Current
				: default;
		}
		finally
		{
			await enumerator.DisposeAsync().NoCtx();
		}
	}


	/*public static async Task<object> FirstOrDefault(this IAsyncEnumerable source, CancellationToken token)
	{
		var enumerator = source.GetAsyncEnumerator(token);
		try
		{
			return await enumerator.MoveNextAsync().NoCtx()
				? enumerator.Current
				: null;
		}
		finally
		{
			await enumerator.DisposeAsync().NoCtx();
		}
	}*/

	#endregion

	public static IAsyncEnumerable<T> Where<T>(this IAsyncEnumerable<T> source, [InstantHandle] Func<T, Task<bool>> predicate)
		=> new GeneralAsyncEnumerable<T, T>(source, (_, enumerator) => new WhereAsyncEnumerator2<T>(enumerator, predicate));

	public static IAsyncEnumerable<T> Where<T>(this IAsyncEnumerable<T> source, [InstantHandle] Predicate<T> predicate)
		=> new GeneralAsyncEnumerable<T, T>(source, (_, enumerator) => new WhereAsyncEnumerator<T>(enumerator, predicate));

	public static IAsyncEnumerable<TResult> Select<TSource, TResult>(
		this IAsyncEnumerable<TSource> source, 
		[InstantHandle] Func<TSource, Task<TResult>> selector)
		=> new GeneralAsyncEnumerable<TSource, TResult>(source, 
			(_, enumerator) => new SelectAsyncEnumerator2<TSource, TResult>(enumerator, selector));

	public static IAsyncEnumerable<TResult> Select<TSource, TResult>(
		this IAsyncEnumerable<TSource> source,
		[InstantHandle] Func<TSource, TResult> selector)
		=> new GeneralAsyncEnumerable<TSource, TResult>(source, 
			(_, enumerator) => new SelectAsyncEnumerator<TSource, TResult>(enumerator, selector));

	public static async Task<TSource?> FirstOrDefaultAsync<TSource>(this IAsyncEnumerable<TSource> source, CancellationToken token = default)
	{
		IAsyncEnumerator<TSource>? ie = null;
		try
		{
			ie = source.GetAsyncEnumerator(token);

			if (await ie.MoveNextAsync().NoCtx()) 
				return ie.Current;

			return default;
		}
		finally
		{
			await ie.WithAsync(i => i.DisposeAsync()).NoCtx();
		}
	}

	#region SelectMany

	public static IAsyncEnumerable<TResult> SelectMany<TSource, TResult>(this IAsyncEnumerable<TSource> source, [InstantHandle] Func<TSource, Task<IAsyncEnumerable<TResult>>> selector)
		=> new GeneralAsyncEnumerable<TSource, TResult>(source, (token, enumerator) => new SelectManyTaskAsyncEnumerator<TSource, TResult>(enumerator, selector, token));

	public static IAsyncEnumerable<TResult> SelectMany<TSource, TResult>(this IAsyncEnumerable<TSource> source, [InstantHandle] Func<TSource, IAsyncEnumerable<TResult>> selector)
		=> new GeneralAsyncEnumerable<TSource, TResult>(source, (token, enumerator) => new SelectManyAsyncEnumerator<TSource, TResult>(enumerator, selector, token));

	public static IAsyncEnumerable<TResult> SelectMany<TSource, TResult>(this IAsyncEnumerable<TSource> source, [InstantHandle] Func<TSource, Task<IEnumerable<TResult>>> selector)
		=> new GeneralAsyncEnumerable<TSource, TResult>(source, (token, enumerator) => new SelectManyTaskSyncEnumerator<TSource, TResult>(enumerator, selector, token));

	public static IAsyncEnumerable<TResult> SelectMany<TSource, TResult>(this IAsyncEnumerable<TSource> source, [InstantHandle] Func<TSource, IEnumerable<TResult>> selector)
		=> new GeneralAsyncEnumerable<TSource, TResult>(source, (token, enumerator) => new SelectManySyncEnumerator<TSource, TResult>(enumerator, selector, token));



	#endregion

	public static IAsyncEnumerable<T> Concat<T>(this IAsyncEnumerable<T> source, params IAsyncEnumerable<T>[] other)
		=> new GeneralAsyncEnumerable<T, T>(source, (token, enumerator) => new ConcatEnumerator<T>(enumerator, token, other));

	public static IAsyncEnumerable<T> Concat<T>(this IAsyncEnumerable<T> source, CancellationToken token, params IAsyncEnumerable<T>[] other)
		=> new GeneralAsyncEnumerable<T, T>(source, (_, enumerator) => new ConcatEnumerator<T>(enumerator, token, other));

	// public static IAsyncEnumerable Concat(this IAsyncEnumerable source, params IAsyncEnumerable[] other)
	// 	=> new GeneralAsyncEnumerable<object>(source, (token, enumerator) => new ConcatEnumerator(enumerator, token, other));

	public static IAsyncEnumerable<T> AppendAfter<T>(this IAsyncEnumerable<T> source, Func<T> last)
	{
		var first = true;
		var other = new AsyncYield<T>((_, stop) =>
		{
			if (first)
			{
				first = false;
				var item = last();
				return Task.FromResult(item);
			}

			stop.Value = true;
			return Task.FromResult(default(T))!;
		});
		return source.Concat(other);
	}

	#region OrderBy

	public static async Task<IEnumerable<TSource>> OrderByDescending<TSource, TKey>(
		this IAsyncEnumerable<TSource> source, 
		Func<TSource, TKey> keySelector, 
		IComparer<TKey>? comparer = null)
		=> (await source.ToList().NoCtx()).OrderByDescending(keySelector, comparer);

	public static async Task<IEnumerable<TSource>> OrderBy<TSource, TKey>(
		this IAsyncEnumerable<TSource> source, 
		Func<TSource, TKey> keySelector,
		IComparer<TKey>? comparer = null)
		=> (await source.ToList().NoCtx()).OrderBy(keySelector, comparer);

	public static async Task<IEnumerable<TSource>> OrderByDescending<TSource, TKey>(
		this IAsyncEnumerable<TSource> source, 
		Func<TSource, Task<TKey>> keySelector, 
		IComparer<TKey>? comparer = null)
		=> await (await source.ToList().NoCtx()).OrderByDescending(keySelector, comparer ?? Comparer<TKey>.Default).NoCtx();

	public static async Task<IEnumerable<TSource>> OrderBy<TSource, TKey>(
		this IAsyncEnumerable<TSource> source, 
		Func<TSource, Task<TKey>> keySelector, 
		IComparer<TKey>? comparer = null)
		=> await (await source.ToList().NoCtx()).OrderBy(keySelector, comparer ?? Comparer<TKey>.Default).NoCtx();

	#endregion


	public static IAsyncEnumerable<TSource> Skip<TSource>(this IAsyncEnumerable<TSource> source, int count)
		=> source.Where(_ => count-- <= 0);

	public static IAsyncEnumerable<TSource> Take<TSource>(this IAsyncEnumerable<TSource> source, int count)
		=> new GeneralAsyncEnumerable<TSource, TSource>(source, (t, _)
			=> new TakeAsyncEnumerator<TSource>(source.GetAsyncEnumerator(t), count));


	#region Partition

	#endregion
}
#endif