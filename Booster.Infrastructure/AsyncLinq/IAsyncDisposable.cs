#if !NETSTANDARD2_1
using System.Threading.Tasks;

namespace Booster.AsyncLinq
{
	public interface IAsyncDisposable
	{
		Task DisposeAsync();
	}
}
#endif