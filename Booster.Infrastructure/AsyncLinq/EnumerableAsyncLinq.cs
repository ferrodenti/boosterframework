﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;

#nullable enable

#if !NETSTANDARD2_1
using System.Threading;
using Booster.AsyncLinq.Kitchen;
#endif

namespace Booster.AsyncLinq
{
	[PublicAPI]
	public static class EnumerableAsyncLinq
	{
		public static async Task<TSource?> FirstOrDefaultAsync<TSource>(
			this IEnumerable<TSource> source, 
			Func<TSource, Task<bool>> predicate)
		{
			foreach (var el in source)
				if (await predicate(el).NoCtx())
					return el;

			return default;
		}

		public static async Task<IEnumerable<TSource>> OrderByDescending<TSource, TKey>(
			this IEnumerable<TSource> source, 
			Func<TSource, Task<TKey>> keySelector,
			IComparer<TKey>? comparer = null)
		{
			var list = new List<Tuple<TSource, TKey>>();
			foreach (var el in source)
				list.Add(Tuple.Create(el, await keySelector(el).NoCtx()));

			return list.OrderByDescending(t => t.Item2, comparer).Select(t => t.Item1);
		}

		public static async Task<IEnumerable<TSource>> OrderBy<TSource, TKey>(
			this IEnumerable<TSource> source,
			Func<TSource, Task<TKey>> keySelector, 
			IComparer<TKey>? comparer = null)
		{
			var list = new List<Tuple<TSource, TKey>>();
			foreach (var el in source)
				list.Add(Tuple.Create(el, await keySelector(el).NoCtx()));

			return list.OrderBy(t => t.Item2, comparer).Select(t => t.Item1);
		}

		public static IAsyncEnumerable<TResult> SelectAsync<TSource, TResult>(
			this IEnumerable<TSource> source,
			Func<TSource, Task<TResult>> selector)
			=> source.AsAsyncEnumerable().Select(selector);

		public static IAsyncEnumerable<TSource> WhereAsync<TSource>(
			this IEnumerable<TSource> source,
			Func<TSource, Task<bool>> predicate)
			=> source.AsAsyncEnumerable().Where(predicate);

		public static Task<T[]> WhenAll<T>(this IEnumerable<Task<T>> source)
			=> Task.WhenAll(source);
#if NETSTANDARD2_1
#pragma warning disable 1998
		public static async IAsyncEnumerable<TSource> AsAsyncEnumerable<TSource>(this IEnumerable<TSource> source)
#pragma warning restore 1998
		{
			foreach (var item in source)
				yield return item;

		}

		public static async IAsyncEnumerable<TResult> SelectManyAsync<TSource, TResult>(this IEnumerable<TSource> source,
			[InstantHandle] Func<TSource, Task<IEnumerable<TResult>>> selector)
		{
			foreach (var item in source)
			foreach (var item2 in await selector(item).NoCtx())
				yield return item2;
		}
#else
		public static IAsyncEnumerable<TSource> AsAsyncEnumerable<TSource>(this IEnumerable<TSource> source)
			=> new GeneralAsyncFromSyncEnumerable<TSource, TSource>(source, (_, src) => new SyncToAsyncEnumerator<TSource>(src.GetEnumerator()));

		public static IAsyncEnumerable<TResult> SelectManyAsync<TSource, TResult>(this IEnumerable<TSource> source, [InstantHandle] Func<TSource, Task<IEnumerable<TResult>>> selector)
			=> new GeneralAsyncFromSyncEnumerable<TSource, TResult>(source, (_, enumerable)
				=> new SelectManySyncToAsyncEnumerator<TResult>(enumerable.Select(selector)));

		public static IAsyncEnumerable<TResult> SelectManyAsync<TSource, TResult>(this IEnumerable<TSource> source, [InstantHandle] Func<TSource, CancellationToken, Task<IEnumerable<TResult>>> selector)
			=> new GeneralAsyncFromSyncEnumerable<TSource, TResult>(source, (token, enumerable)
				=> new SelectManySyncToAsyncEnumerator<TResult>(enumerable.Select(o => selector(o, token))));


#endif
	}
}
