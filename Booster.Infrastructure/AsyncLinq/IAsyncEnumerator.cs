#if !NETSTANDARD2_1
using System.Threading.Tasks;

#nullable enable

namespace Booster.AsyncLinq;

public interface IAsyncEnumerator : IAsyncDisposable
{
	Task<bool> MoveNextAsync();
	object Current { get; }
}

public interface IAsyncEnumerator<out T> : IAsyncEnumerator
{
	new T Current { get; }
}
#endif