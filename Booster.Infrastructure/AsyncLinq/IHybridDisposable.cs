using System;

namespace Booster.AsyncLinq;

public interface IHybridDisposable : IAsyncDisposable, IDisposable
{
}