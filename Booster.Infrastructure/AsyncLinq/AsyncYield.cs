#if NETSTANDARD2_1
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Booster.AsyncLinq
{
	public class AsyncYield<T> : IAsyncEnumerable<T>
	{
		class Enumerator : IAsyncEnumerator<T>
		{
			readonly Func<CancellationToken, Box<bool>, Task<T>> _proc;
			readonly CancellationToken _token;

			public Enumerator(Func<CancellationToken, Box<bool>, Task<T>> proc, CancellationToken token)
			{
				_proc = proc;
				_token = token;
			}

			public async ValueTask<bool> MoveNextAsync()
			{
				var cancel = new Box<bool>(false);
				var task = _proc(_token, cancel);
				if (task == null)
					return false;

				Current = await task.NoCtx();
				return !cancel.Value;
			}

			public ValueTask DisposeAsync()
				=> default;

			public T Current { get; private set; }
		}

		readonly Func<CancellationToken, Box<bool>, Task<T>> _proc;

		public AsyncYield(Func<CancellationToken, Box<bool>, Task<T>> proc) 
			=> _proc = proc;

		public IAsyncEnumerator<T> GetAsyncEnumerator(CancellationToken token = default) 
			=> new Enumerator(_proc, token);

		// IAsyncEnumerator IAsyncEnumerable.GetAsyncEnumerator(CancellationToken token) 
		// 	=> GetAsyncEnumerator(token);
	}
}
#else
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Booster.AsyncLinq
{
	public class AsyncYield<T> : IAsyncEnumerable<T>
	{
		class Enumerator : IAsyncEnumerator<T>
		{
			readonly Func<CancellationToken, Box<bool>, Task<T>> _proc;
			readonly CancellationToken _token;

			public Enumerator(Func<CancellationToken, Box<bool>, Task<T>> proc, CancellationToken token)
			{
				_proc = proc;
				_token = token;
			}

			public Task DisposeAsync() 
				=> Task.CompletedTask;

			public async Task<bool> MoveNextAsync()
			{
				var cancel = new Box<bool>(false);
				var task = _proc(_token, cancel);
				if (task == null)
					return false;

				Current = await task.NoCtx();
				return !cancel.Value;
			}

			public T Current { get; private set; }

			object IAsyncEnumerator.Current => Current;
		}

		readonly Func<CancellationToken, Box<bool>, Task<T>> _proc;

		public AsyncYield(Func<CancellationToken, Box<bool>, Task<T>> proc) 
			=> _proc = proc;

		public IAsyncEnumerator<T> GetAsyncEnumerator(CancellationToken token = default) 
			=> new Enumerator(_proc, token);

		// IAsyncEnumerator IAsyncEnumerable.GetAsyncEnumerator(CancellationToken token) 
		// 	=> GetAsyncEnumerator(token);
	}
}
#endif