using System.Runtime.CompilerServices;
using JetBrains.Annotations;

namespace Booster;

[PublicAPI]
public class FlyingAverage
{
	readonly int _capacity;
	int _count;

	public decimal Value { get; private set; }

	public FlyingAverage(int capacity)
		=> _capacity = capacity;

	[MethodImpl(MethodImplOptions.Synchronized)]
	public decimal Push(decimal value)
	{
		if (_count < _capacity)
			++_count;

		return Value = (Value * (_count - 1) + value) / _count;
	}
}