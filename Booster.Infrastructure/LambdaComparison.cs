using System;
using System.Collections.Generic;

namespace Booster;

public sealed class LambdaComparison<T> : IEqualityComparer<T>
{
	readonly Func<T, T, bool> _comparison;
	readonly IEqualityComparer<T> _defComparer = EqualityComparer<T>.Default;

	public LambdaComparison(Func<T, T, bool> del)
		=> _comparison = del;

	public bool Equals(T left, T right)
		=> _comparison(left, right);

	public int GetHashCode(T value)
		=> _defComparer.GetHashCode(value);
}