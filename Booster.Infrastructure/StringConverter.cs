using System;
using System.Globalization;
using Booster.Interfaces;
using Booster.Reflection;
using JetBrains.Annotations;

#nullable enable

namespace Booster;

[PublicAPI]
public class StringConverter
{
	public static StringConverter Default => new();

	readonly NumberFormatInfo _nfi = new() {NumberDecimalSeparator = "."};

	public bool LowerCaseBoolean { get; set; } = true;

	public string? DefaultDateTimeFormat { get; set; }

	public Func<TypeEx, object, string>? EnumFormatter { get; set; } //TODO: use EnumHelper by default
	public Func<TypeEx, string, object>? EnumParser { get; set; } 
	public Func<TypeEx, object, string>? ArrayFormatter { get; set; }
	public Func<TypeEx, string, object>? ArrayParser { get; set; }

	public string? ToString(object? src, string? format = null)
	{
		if (src == null)
			return null;

		TypeEx type = src.GetType();

		if (type.IsArray && ArrayFormatter != null)
			return ArrayFormatter(type, src);

		if (type.IsEnum && EnumFormatter != null)
			return EnumFormatter(type, src);

		switch (type.TypeCode)
		{
		case TypeCode.String:
			return (string) src;

		case TypeCode.Empty:
		case TypeCode.DBNull:
			return null;
		case TypeCode.Boolean:
			if (LowerCaseBoolean)
				return (bool) src ? "true" : "false";

			return (bool) src ? "True" : "False";
		case TypeCode.DateTime:
			var dt = (DateTime) src;
			if (dt == DateTime.MinValue)
				return null;

			return dt.ToString(format ?? DefaultDateTimeFormat ?? "dd.MM.yyyy");

		case TypeCode.Single:
			return ((float) src).ToString("G", _nfi);
		case TypeCode.Double:
			return ((double)src).ToString("G", _nfi);
		case TypeCode.Decimal:
			return ((decimal)src).ToString("G", _nfi);
		}

		return src.ToString();
	}

	public bool TryParse<TObject>(string src, out TObject? value, string? format = null)
	{
		if (TryParse(src, typeof(TObject), out var obj, format))
		{
			value = (TObject?) obj;
			return true;
		}
		value = default;
		return false;
	}

	public bool TryParse(string? src, TypeEx type, out object? value, string? format = null)
	{
		value = type.DefaultValue;

		if (src == null)
			return true;

		if (type.IsEnum)
			try
			{
				value = EnumParser != null ? EnumParser(type, src) : Enum.Parse(type, src, true);
				return true;
			}
			catch { return false; }

		if (type.IsArray && ArrayParser != null)
			try
			{
				value = ArrayParser(type, src);
				return true;
			}
			catch { return false; }

		switch (type.TypeCode)
		{
		case TypeCode.Empty:
		case TypeCode.DBNull:
			return false;
		case TypeCode.Boolean:
			value = src.ToLower() == "true" || src.ToLower() == "on";
			return true;

		case TypeCode.Char:
			if (src.Length == 1)
			{
				value = src[0];
				return true;
			}
			return false;

		case TypeCode.SByte:
			if (src.Length == 1)
			{
				value = (sbyte)src[0];
				return true;
			}
			return false;
		case TypeCode.Byte:  //TODO: прав ли я последних 2х случаях??
			if (src.Length == 1)
			{
				value = (byte)src[0];
				return true;
			}
			return false;

		case TypeCode.Int16:
			if (short.TryParse(src, out var s))
			{
				value = s;
				return true;
			}
			return false;
		case TypeCode.UInt16:
			if (ushort.TryParse(src, out var us))
			{
				value = us;
				return true;
			}
			return false;
		case TypeCode.Int32:
			if (int.TryParse(src, out var i))
			{
				value = i;
				return true;
			}
			return false;
		case TypeCode.UInt32:
			if (uint.TryParse(src, out var ui))
			{
				value = ui;
				return true;
			}
			return false;
		case TypeCode.Int64:
			if (long.TryParse(src, out var l))
			{
				value = l;
				return true;
			}
			return false;
		case TypeCode.UInt64:
			if (long.TryParse(src, out var ul))
			{
				value = ul;
				return true;
			}
			return false;
		case TypeCode.Single:
			if (float.TryParse(src.Replace(',', '.'), NumberStyles.Any, _nfi, out var sgl))
			{
				value = sgl;
				return true;
			}
			return false;
		case TypeCode.Double:
			if (double.TryParse(src.Replace(',', '.'), NumberStyles.Any, _nfi, out var dbl))
			{
				value = dbl;
				return true;
			}
			return false;
		case TypeCode.Decimal:
			if (decimal.TryParse(src.Replace(',', '.'), NumberStyles.Any, _nfi, out var dec))
			{
				value = dec;
				return true;
			}
			return false;
		case TypeCode.DateTime:
			if (DateTime.TryParseExact(src, format ?? DefaultDateTimeFormat ?? "dd.MM.yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out var dt))
			{
				value = dt;
				return true;
			}
			return false;
		case TypeCode.String:
			value = src;
			return true;
		}
		return false;
	}

	public T? ToObject<T>(string? src, string? format = null)
		=> (T?) ToObject(src, typeof (T), format);
		
	public T? ToObject<T>(object? src, string? format = null) //TODO: орм почему-то вначале конвертит ips-transport.UserRole.Id в int переменную, а потом повторно присваевает Id = pk через этот метод  
	{
		if (src is T result )
			return result;
			
		return (T?) ToObject(src?.ToString(), typeof(T), format);
	}

	public object? ToObject(string? src, TypeEx type, string? format = null)
	{
		if (src == null)
			return type.DefaultValue;

		if (type.Inner == typeof(string))
			return src;

		if (type.IsEnum)
		{
			if (EnumParser != null)
				return EnumParser(type, src);
				
			return src == "" ? type.DefaultValue : Enum.Parse(type, src, true);
		}

		if (type.IsArray && ArrayParser != null)
			return ArrayParser(type, src);
			
		switch (type.TypeCode)
		{
		case TypeCode.Empty:
		case TypeCode.DBNull:
			throw new ArgumentOutOfRangeException();
		case TypeCode.Boolean:
			var ls = src.ToLower();

			switch (ls)
			{
			case "true":
			case "on":
			case "yes":
			case "y":
			case "1":
			case "-1":
				return true;
			case "0":
				return false;
			}

			if (int.TryParse(src, out var i))
				return i != 0;

			return false;
		case TypeCode.Char:
			return src.Length > 0 ? src[0] : (char)0;
		case TypeCode.SByte:
			return src.Length > 0 ? (sbyte)src[0] : 0;
		case TypeCode.Byte:
			return src.Length > 0 ? (byte)src[0] : 0;
			
		case TypeCode.Int16:
			short.TryParse(src, out var s);
			return s;
		case TypeCode.UInt16:
			ushort.TryParse(src, out var us);
			return us;
		case TypeCode.Int32:
			int.TryParse(src, out i);
			return i;
		case TypeCode.UInt32:
			uint.TryParse(src, out var ui);
			return ui;
		case TypeCode.Int64:
			long.TryParse(src, out var l);
			return l;
		case TypeCode.UInt64:
			long.TryParse(src, out var ul);
			return ul;
		case TypeCode.Single:
			float.TryParse(src, NumberStyles.Any, _nfi, out var sgl);
			return sgl;
		case TypeCode.Double:
			double.TryParse(src, NumberStyles.Any, _nfi, out var dbl);
			return dbl;
		case TypeCode.Decimal:
			decimal.TryParse(src, NumberStyles.Any, _nfi, out var dec);
			return dec;
		case TypeCode.DateTime:
			DateTime.TryParseExact(src, format ?? DefaultDateTimeFormat ?? "dd.MM.yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out var dt);
			return dt;
		case TypeCode.String:
			return src;
		}

		if (TypeEx.Get<string>().TryCast(src, type, out var result))
			return result;
			
		var convIfc = type.FindInterface(typeof(IConvertiableFrom<>));
		if (convIfc != null)
		{
			result = ToObject(src, convIfc.GenericArguments[0], format);
				
			if (convIfc.GenericArguments[0].TryCast(result, type, out result))
				return result;
		}

		throw new ArgumentOutOfRangeException();
	}
		

}