﻿using System;

#nullable enable

namespace Booster;

/// <summary>
/// Обобщенный аргумент события.
/// </summary>
/// <typeparam name="T">Тип аргумента события</typeparam>
public class EventArgs<T> : EventArgs
{
	/// <summary>
	/// Данные события
	/// </summary>
	public T Data { get; set; }

	///// <summary>
	///// Создает аргумент события без данных
	///// </summary>
	//public EventArgs() {}

	/// <summary>
	/// Создает аргумент события с данными
	/// </summary>
	/// <param name="data">данные</param>
	public EventArgs(T data)
		=> Data = data;
}