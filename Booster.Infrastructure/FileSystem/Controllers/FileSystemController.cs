using Booster.Log;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

#nullable enable

namespace Booster.FileSystem.Controllers;

public class FileSystemController : IFileSystemController
{
	public static readonly FileSystemController Instance = new();

	readonly IFileSystemController _fileController;
	readonly IFileSystemController _directoryController;

	public bool AutoDetect { get; set; } = true;
	
	public FileSystemController()
		: this(FileController.Instance, DirectoryController.Instance)
	{
	}
	
	public FileSystemController(IFileSystemController fileController, IFileSystemController directoryController)
	{
		_fileController = fileController;
		_directoryController = directoryController;
	}

	protected IFileSystemController Get(ref FilePath source)
	{
		if (AutoDetect)
		{
			var src = source.AsDirectory();
			
			if (Directory.Exists(src))
			{
				source = src;
				return _directoryController;
			}
			
			src = source.AsFile();
			if (File.Exists(src))
			{
				source = src;
				return _fileController;
			}
		}
		
		return source.DirectoryEnding
			? _directoryController
			: _fileController;
	}

	public bool Ensure(FilePath source, ILog? log = null)
		=>  _directoryController.Ensure(source.Directory, log);

	public bool Copy(FilePath source, FilePath destination, bool overwrite, IFileSystemController? fileController = null)
		=> Get(ref source).Copy(source, destination, overwrite, fileController ?? _fileController);

	public bool Move(FilePath source, FilePath destination, bool overwrite, IFileSystemController? fileController = null)
		=> Get(ref source).Move(source, destination, overwrite, fileController ?? _fileController);

	public bool Delete(FilePath source)
		=> Get(ref source).Delete(source);

	public Task<bool> CopyAsync(FilePath source, FilePath destination, bool overwrite, CancellationToken cancellationToken = default, IFileSystemController? fileController = null)
		=> Get(ref source).CopyAsync(source, destination, overwrite, cancellationToken, fileController ?? _fileController);

	public Task<bool> MoveAsync(FilePath source, FilePath destination, bool overwrite, CancellationToken cancellationToken = default, IFileSystemController? fileController = null)
		=> Get(ref source).MoveAsync(source, destination, overwrite, cancellationToken, fileController ?? _fileController);

	public Task<bool> DeleteAsync(FilePath source, CancellationToken cancellationToken = default)
		=> Get(ref source).DeleteAsync(source, cancellationToken);
}
