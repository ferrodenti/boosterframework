﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Booster.Log;

#nullable enable

namespace Booster.FileSystem.Controllers;

public class FileController : BaseFileSystemController
{
	public static readonly FileController Instance = new();

	public override bool Ensure(FilePath source, ILog? log = null)
		=> DirectoryController.Instance.Ensure(source.Directory, log);
	
	public override bool Delete(FilePath source)
	{
		if (Exists(source))
		{
			File.Delete(source);
			return true;
		}

		return false;
	}
	public override bool Copy(FilePath source, FilePath destination, bool overwrite, IFileSystemController? fileController = null)
	{
		if (!PrepeareDestination(source, ref destination, overwrite)) 
			return false;
		
		File.Copy(source, destination, overwrite);
		return true;
	}
	
	
	public override Task<bool> CopyAsync(FilePath source, FilePath destination, bool overwrite, CancellationToken cancellationToken = default, IFileSystemController? fileController = null)
	{
		return Task.FromResult(Copy(source, destination, overwrite, fileController));
		/*
		const FileOptions fileOptions = FileOptions.Asynchronous | FileOptions.SequentialScan;
		const int bufferSize = 4096;
			
		if (!PrepeareDestination(source, ref destination, overwrite)) 
			return false;
		
#if NETSTANDARD2_1
		await using var sourceStream = new FileStream(source, FileMode.Open, FileAccess.Read, FileShare.Read, bufferSize, fileOptions);
		await using var destinationStream = new FileStream(destination, FileMode.CreateNew, FileAccess.Write, FileShare.None, bufferSize, fileOptions);
#else
		using var sourceStream = new FileStream(source, FileMode.Open, FileAccess.Read, FileShare.Read, bufferSize, fileOptions);
		using var destinationStream = new FileStream(destination, FileMode.CreateNew, FileAccess.Write, FileShare.None, bufferSize, fileOptions);
#endif
		await sourceStream.CopyToAsync(destinationStream, bufferSize, cancellationToken).NoCtx();

		return true;*/
	}

	public override bool MoveInt(FilePath source, FilePath destination, bool overwrite)
	{
		if (!overwrite && Exists(destination))
			return false;

		File.Move(source, destination);
		
		return true;
	}

	protected override bool Exists(FilePath path)
		=> File.Exists(path);

	public override Task<bool> MoveAsyncInt(FilePath source, FilePath destination, bool overwrite, CancellationToken cancellationToken = default)
	{
		if (!overwrite && Exists(destination))
			return Task.FromResult(false);

		File.Move(source, destination);
		
		return Task.FromResult(true);
	}

	public bool PrepeareDestination(FilePath source, ref FilePath destination, bool overwrite)
	{
		if (destination.DirectoryEnding)
			destination += source.Filename!;
		
		if (!overwrite && Exists(destination))
			return false;
		 
		Delete(destination);
		return true;
	}
}
