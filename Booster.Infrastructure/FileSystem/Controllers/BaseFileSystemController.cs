using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Booster.Log;

#nullable enable

namespace Booster.FileSystem.Controllers;

public abstract class BaseFileSystemController : IFileSystemController
{
	public abstract bool Ensure(FilePath source, ILog? log = null);

	public abstract bool Delete(FilePath source);

	public virtual Task<bool> DeleteAsync(FilePath source, CancellationToken cancellationToken = default)
	{
		Utils.Nop(cancellationToken);
		return Task.FromResult(Delete(source));
	}

	public abstract bool Copy(FilePath source, FilePath destination, bool overwrite, IFileSystemController? fileController = null);

	public abstract Task<bool> CopyAsync(FilePath source, FilePath destination, bool overwrite, CancellationToken cancellationToken = default, IFileSystemController? fileController = null);

	public virtual bool Move(FilePath source, FilePath destination, bool overwrite, IFileSystemController? fileController = null)
	{
		if (SameDrive(source, destination) && !Exists(destination))
			return MoveInt(source, destination, overwrite);

		return Copy(source, destination, overwrite, fileController) && Delete(source);
	}

	public abstract bool MoveInt(FilePath source, FilePath destination, bool overwrite);

	public virtual async Task<bool> MoveAsync(FilePath source, FilePath destination, bool overwrite, CancellationToken cancellationToken = default, IFileSystemController? fileController = null)
	{
		if (SameDrive(source, destination) && !Exists(destination))
			return await MoveAsyncInt(source, destination, overwrite, cancellationToken);

		return await CopyAsync(source, destination, overwrite, cancellationToken, fileController).NoCtx() &&
			   await DeleteAsync(source, cancellationToken).NoCtx();
	}

	protected abstract bool Exists(FilePath path);

	static bool SameDrive(FilePath a, FilePath b)
	{
		a = a.MakeAbsolute();
		b = b.MakeAbsolute();
		return a.Segments.FirstOrDefault()?.ToLower() == b.Segments.FirstOrDefault()?.ToLower();
	}

	public abstract Task<bool> MoveAsyncInt(FilePath source, FilePath destination, bool overwrite, CancellationToken cancellationToken = default);

}
