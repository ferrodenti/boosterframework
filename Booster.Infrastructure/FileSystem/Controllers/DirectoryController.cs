using Booster.Log;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

#nullable enable

namespace Booster.FileSystem.Controllers;

public class DirectoryController : BaseFileSystemController
{
	public static readonly DirectoryController Instance = new();

	public override bool Ensure(FilePath source, ILog? log = null)
	{
		if (!Exists(source))
		{
			log.Debug($"������ ����� {source}");
			Directory.CreateDirectory(source);
		}

		return true;
	}
	
	public override bool Delete(FilePath source)
	{
		if (Exists(source))
		{
			Directory.Delete(source, true);
			return true;
		}

		return false;
	}
	public override bool Copy(FilePath source, FilePath destination, bool overwrite, IFileSystemController? fileController = null)
	{
		fileController ??= FileController.Instance;
		destination = destination.AsDirectory();

		Ensure(destination);

		foreach (FilePath file in Directory.EnumerateFiles(source))
		{
			var dst = destination + file.Filename!;
			if (!fileController.Copy(file, dst, overwrite))
				return false;
		}

		foreach (FilePath file in Directory.EnumerateDirectories(source))
		{
			var dst = destination + file.Directory;
			if (!Copy(file, dst, overwrite, fileController))
				return false;
		}

		return true;
	}
	
	public override async Task<bool> CopyAsync(FilePath source, FilePath destination, bool overwrite, CancellationToken cancellationToken = default, IFileSystemController? fileController = null)
	{
		fileController ??= FileController.Instance;
		destination = destination.AsDirectory();

		Ensure(destination);

		foreach (FilePath file in Directory.EnumerateFiles(source))
		{
			var dst = destination + file.Filename!;
			if (!await fileController.CopyAsync(file, dst, overwrite, cancellationToken, fileController).NoCtx())
				return false;
		}

		foreach (FilePath file in Directory.EnumerateDirectories(source))
		{
			var dst = (destination + file.Name!).AsDirectory();
			if (!await CopyAsync(file, dst, overwrite, cancellationToken, fileController).NoCtx())
				return false;
		}

		return true;
	}

	public override bool MoveInt(FilePath source, FilePath destination, bool overwrite)
	{
		if (!overwrite && Exists(destination))
			return false;

		Directory.Move(source, destination);
		
		return true;
	}

	protected override bool Exists(FilePath path)
		=> Directory.Exists(path);

	public override Task<bool> MoveAsyncInt(FilePath source, FilePath destination, bool overwrite, CancellationToken cancellationToken = default)
	{
		if (!overwrite && Exists(destination))
			return Task.FromResult(false);

		Directory.Move(source, destination);
		
		return Task.FromResult(true);
	}
}
