﻿using System.Threading;
using System.Threading.Tasks;
using Booster.Log;

#nullable enable

namespace Booster.FileSystem.Controllers;

public interface IFileSystemController
{
	public bool Ensure(FilePath source, ILog? log = null);
	
	bool Copy(FilePath source, FilePath destination, bool overwrite, IFileSystemController? fileController = null);
	bool Move(FilePath source, FilePath destination, bool overwrite, IFileSystemController? fileController = null);
	bool Delete(FilePath source);
	
	Task<bool> CopyAsync(FilePath source, FilePath destination, bool overwrite, CancellationToken cancellationToken = default, IFileSystemController? fileController = null);
	Task<bool> MoveAsync(FilePath source, FilePath destination, bool overwrite, CancellationToken cancellationToken = default, IFileSystemController? fileController = null);
	Task<bool> DeleteAsync(FilePath source, CancellationToken cancellationToken = default);
}