#define USE_SCHEDULE1 // TODO: произвольно зависают тесты под NETCORE 

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Booster.DI;
using Booster.Parsing;
using JetBrains.Annotations;

namespace Booster.FileSystem;

/// <summary>
/// Класс для отслеживания изменений в файлах. Является оберткой над System.IO.FileSystemWatcher, а так же реализует отследживание изменений в файлах по таймеру.
/// В отличии от System.IO.FileSystemWatcher позволяет следить за файлами на сетевых дисках.
/// Имеет временной порог срабатывания событий, позволяющий отсеить промежуточные изменения в файлах (медленная запись в файл).
/// </summary>
[PublicAPI]
public class FileWatcher : IDisposable //TODO: use IStartStop
{
	class Item : IDisposable
	{
		readonly string _directory;
		readonly string _fileName;
		WatcherChangeTypes _changeType;
		FileSystemEventArgs _fileSystemEventArgs;
			
#if USE_SCHEDULE
			TimeSchedule.Subscription _eventDelay;
#else
		Timer _eventDelay;
#endif
		readonly FileWatcher _owner;
		readonly object _lock = new();
		readonly string _path;
		public long Size;
		public DateTime LastWrite;

		public FileSystemEventArgs FileSystemEventArgs => _fileSystemEventArgs ?? new FileSystemEventArgs(_changeType, _directory, _fileName);

		public Item(FileWatcher owner, string path)
		{
			_owner = owner;
			_path = path.ToLower();
			_fileName = System.IO.Path.GetFileName(path);
			_directory = System.IO.Path.GetDirectoryName(path);
			var fi = new FileInfo(path);
			if (fi.Exists)
			{
				Size = fi.Length;
				LastWrite = fi.LastWriteTimeUtc;
			}
		}

		public void SetChanged(WatcherChangeTypes changeType, FileSystemEventArgs fileSystemEventArgs = null)
		{
			_changeType = changeType;
			_fileSystemEventArgs = fileSystemEventArgs;

			if (_owner.JoinEvents)
				_owner.AddChangedFile(this);
			else
				lock (_lock)
				{
#if USE_SCHEDULE
						if (_eventDelay == null)
							_eventDelay = _schedule.Subscribe(this, () => DelayedEventCallback());
						
						_eventDelay.Next = DateTime.Now + _owner.EventDelay;
#else
					_eventDelay ??= new Timer(_ => DelayedEventCallback());
					_eventDelay.Change((int)_owner.EventDelay.TotalMilliseconds, -1);
#endif
				}
		}

		void DelayedEventCallback()
		{
#if !USE_SCHEDULE
			lock (_lock)
			{
				_eventDelay?.Dispose();
				_eventDelay = null;
			}
#endif

			var ea = FileSystemEventArgs;
			_owner.Event?.Invoke(this,  new FileWatcherEventArgs(ea));
			switch (ea.ChangeType)
			{
			case WatcherChangeTypes.Created:
				_owner.Created?.Invoke(this, new FileWatcherEventArgs(ea));
				break;
			case WatcherChangeTypes.Changed:
				_owner.Changed?.Invoke(this, new FileWatcherEventArgs(ea));
				break;
			case WatcherChangeTypes.Deleted:
				_owner.Deleted?.Invoke(this, new FileWatcherEventArgs(ea));
				break;
			case WatcherChangeTypes.Renamed:
				_owner.Renamed?.Invoke(this, new FileWatcherEventArgs(ea));
				break;
			}
		}

		public void Dispose()
		{
			_eventDelay?.Dispose();
			_eventDelay = null;
		}

		public override bool Equals(object obj) 
			=> ( obj as Item)?._path == _path;

		public override string ToString()
			=> _path;

		public override int GetHashCode() 
			=> _path.GetHashCode();
	}

#if USE_SCHEDULE
		static readonly TimeSchedule _schedule = new TimeSchedule("FileWatcherTimers");
		TimeSchedule.Subscription _timer;
		TimeSchedule.Subscription _eventDelay;
#else
	Timer _timer;
	Timer _eventDelay;
#endif
	ConcurrentDictionary<string, Item> _knownFiles;
	HashSet<Item> _changedFiles = new();
	FileSystemWatcher _fileSystemWatcher;
	FilePath _directory = "";
	string _filter;
		
	readonly object _lock = new();
	readonly SemaphoreSlim _semaphore = new(1, 1);
	/// <summary>
	/// Маска мониторинга файлов. формат: \dir\wildcards
	/// </summary>
	public FilePath Path
	{
		get => _path;
		set
		{
			if (_path != value)
			{
				_path = value ?? "";
				_directory = _path.Directory;
				_filter = _path.Filename;

				if (_working)
				{
					if (_useFileSystemWatcher)
					{
						DisposeFileSystemWatcher();
						SetupFileSystemWatcher();
					}
					ReadDirectory();
				}
			}
		}
	}

	FilePath _path;

	/// <summary>
	/// Отслеживать или нет вложенные папки
	/// </summary>
	public bool IncludeSubdirectories
	{
		get => _includeSubdirectories;
		set
		{
			if (_includeSubdirectories != value)
			{
				_includeSubdirectories = value;

				var fw = _fileSystemWatcher;
				if (fw != null) fw.IncludeSubdirectories = value;
				if (_working)
					ReadDirectory();
			}
		}
	}
	bool _includeSubdirectories;

	/// <summary>
	/// Включенно ли слежение
	/// </summary>
	public bool Working
	{
		get => _working;
		set
		{
			if (_working != value)
			{
				if (value)
					Start();
				else
					Stop();
			}
		}
	}
	bool _working;

	/// <summary>
	/// Использовать или нет System.IO.FileSystemWatcher для ослуживания изменений в файлах 
	/// </summary>
	public bool UseFileSystemWatcher
	{
		get => _useFileSystemWatcher;
		set
		{
			if (_useFileSystemWatcher != value)
			{
				_useFileSystemWatcher = value;

				if (_working)
				{
					if (value)
						SetupFileSystemWatcher();
					else
						DisposeFileSystemWatcher();
				}
			}
		}
	}

	bool _useFileSystemWatcher = true;

	/// <summary>
	/// Интервал отслеживания изменений по таймеру. Значения 0 и меньше отключают таймер.
	/// </summary>
	public TimeSpan WatchInterval
	{
		get => _watchInterval;
		set
		{
			if (_watchInterval != value)
			{
				_watchInterval = value;

				if (_working)
				{
					if (value.Ticks > 0)
						SetupTimer();
					else
						DisposeTimer();
				}
			}
		}
	}

	TimeSpan _watchInterval = TimeSpan.FromSeconds(1);

	/// <summary>
	/// Задержка отсылки события изменения файла
	/// </summary>
	public TimeSpan EventDelay { get; set; } = TimeSpan.FromSeconds(2);

	/// <summary>
	/// Объединить изменения нескольких файлов в одно событие
	/// </summary>
	public bool JoinEvents { get; set; } = true;

	/// <summary>
	/// Свойства файла которые следует мониторить. По умолчанию - время записи/создания, размер, имя
	/// </summary>
	public NotifyFilters Monitors { get; set; } = NotifyFilters.LastWrite | NotifyFilters.Size | NotifyFilters.FileName | NotifyFilters.DirectoryName | NotifyFilters.CreationTime;

	/// <summary>
	/// Типы событий файловой системы на которые следует реагировать. По умолчанию - любые события
	/// </summary>
	public WatcherChangeTypes EventTypes
	{
		get => _eventTypes;
		set
		{
			var fw = _fileSystemWatcher;
			if (fw != null && _eventSubscriptions != value)
			{
				var val = value.HasFlag(WatcherChangeTypes.Created);
				if (val != _eventSubscriptions.HasFlag(WatcherChangeTypes.Created))
				{
					if (val)
						fw.Created += FileSystemWatcherEvent;
					else
						fw.Created -= FileSystemWatcherEvent;
				}

				val = value.HasFlag(WatcherChangeTypes.Changed);
				if (val != _eventSubscriptions.HasFlag(WatcherChangeTypes.Changed))
				{
					if (val)
						fw.Changed += FileSystemWatcherEvent;
					else
						fw.Changed -= FileSystemWatcherEvent;
				}

				val = value.HasFlag(WatcherChangeTypes.Deleted);
				if (val != _eventSubscriptions.HasFlag(WatcherChangeTypes.Deleted))
				{
					if (val)
						fw.Deleted += FileSystemWatcherEvent;
					else
						fw.Deleted -= FileSystemWatcherEvent;
				}

				val = value.HasFlag(WatcherChangeTypes.Renamed);
				if (val != _eventSubscriptions.HasFlag(WatcherChangeTypes.Renamed))
				{
					if (val)
						fw.Renamed += FileSystemWatcherEvent;
					else
						fw.Renamed -= FileSystemWatcherEvent;
				}

				_eventSubscriptions = value;
			}
			_eventTypes = value;
		}
	}

	WatcherChangeTypes _eventSubscriptions = 0;
	WatcherChangeTypes _eventTypes = WatcherChangeTypes.All;

	/// <summary>
	/// Событие возникающее при любых изменениях файлов соответствующих параметрам мониторинга.
	/// </summary>
	public event EventHandler<FileWatcherEventArgs> Event;
	/// <summary>
	/// Событие возникающее при изменении файлов соответствующих параметрам мониторинга.
	/// </summary>
	public event EventHandler<FileWatcherEventArgs> Changed;
	/// <summary>
	/// Событие возникающее при cоздании файлов соответствующих параметрам мониторинга.
	/// </summary>
	public event EventHandler<FileWatcherEventArgs> Created;
	/// <summary>
	/// Событие возникающее при удалении файлов соответствующих параметрам мониторинга.
	/// </summary>
	public event EventHandler<FileWatcherEventArgs> Deleted;
	/// <summary>
	/// Событие возникающее при переименовании файлов соответствующих параметрам мониторинга.
	/// </summary>
	public event EventHandler<FileWatcherEventArgs> Renamed;

	/// <summary>
	/// Событие возникающее при ошибке мониторинга
	/// </summary>
	public event EventHandler<ErrorEventArgs> Error;
	/// <summary>
	/// Создает FileWatcher с заданной маской мониторинга файлов
	/// </summary>
	/// <param name="path">Маска мониторинга файлов. формат: \dir\wildcards</param>
	/// <param name="start">Запустить ли мониторинг немедленно</param>
	public FileWatcher(FilePath path, bool start = true)
	{
		Path = path;

		if (start)
			Start();
	}

	/// <summary>
	/// Создает не инициализированный экземпляр FileWatcher
	/// </summary>
	public FileWatcher()
	{
	}

	void AddChangedFile(Item item)
	{
		lock (_lock)
			if (!_changedFiles.Contains(item))
				_changedFiles.Add(item);
	}

	void SetupFileSystemWatcher()
	{
		if (Directory.Exists(_directory))
		{
			_fileSystemWatcher = new FileSystemWatcher(_directory, _filter.Or("*"))
			{
				IncludeSubdirectories = _includeSubdirectories,
				EnableRaisingEvents = true,
				NotifyFilter = Monitors
			};

			_eventSubscriptions = 0;

			EventTypes = EventTypes;

			_knownFiles ??= new ConcurrentDictionary<string, Item>(StringComparer.OrdinalIgnoreCase);
		}
	}

	void SetupTimer()
	{
#if USE_SCHEDULE
			if (_timer == null)
				_timer = _schedule.Subscribe(this, WatchInterval, () => TimerCallback());
			else
				_timer.Interval = WatchInterval;
#else
		_timer ??= new Timer(_ => TimerCallback());
		_timer?.Change(WatchInterval, WatchInterval);
#endif
		if (_knownFiles == null)
			ReadDirectory();
	}

	void ReadDirectory()
	{
		var res = new ConcurrentDictionary<string, Item>(StringComparer.OrdinalIgnoreCase);

		foreach (var fn in EnumerateFiles())
			res[fn] = new Item(this, fn);

		_knownFiles = res;
	}

	IEnumerable<string> EnumerateFiles()
	{
		if (!Directory.Exists(_directory))
			return Enumerable.Empty<string>();

		var ie = Directory.EnumerateFiles(_directory, _filter.Or("*"), IncludeSubdirectories ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);

		if (_filter.IsSome())
			ie = ie.Where(fn => Wildcards.Compare(System.IO.Path.GetFileName(fn), _filter));

		return ie;
	}

	void FileSystemWatcherEvent(object sender, FileSystemEventArgs e)
	{
		try
		{
			if (EventTypes.HasFlag(e.ChangeType))
			{
				var item = _knownFiles.GetOrAdd(e.FullPath, path => new Item(this, path));
				item.SetChanged(e.ChangeType, e);

				if (JoinEvents)
					ScheduleJoinedEvent();
			}
		}
		catch (Exception ex)
		{
			Error?.Invoke(this, new ErrorEventArgs(ex));
		}
	}

	void TimerCallback()
	{
		lock (_lock)
			if (_working && _knownFiles != null && _semaphore.Wait(0) )
				try
				{
					var dict = new Dictionary<string, Item>(_knownFiles, StringComparer.OrdinalIgnoreCase);

					var changed = false;
					foreach (var fn in EnumerateFiles())
						if (_knownFiles.TryGetValue(fn, out var item))
						{
							var fi = new FileInfo(fn);
							var cha = false;
							if (Monitors.HasFlag(NotifyFilters.LastWrite) && item.LastWrite != fi.LastWriteTimeUtc)
							{
								item.LastWrite = fi.LastWriteTimeUtc;
								cha = true;
							}
							if (Monitors.HasFlag(NotifyFilters.Size) && item.Size != fi.Length)
							{
								item.Size = fi.Length;
								cha = true;
							}
							if (cha)
							{
								item.SetChanged(WatcherChangeTypes.Changed);
								changed = true;
							}

							dict.Remove(fn);
						}
						else
						{
							_knownFiles[fn] = item = new Item(this, fn);
							item.SetChanged(WatcherChangeTypes.Created);
							changed = true;
						}

					foreach (var pair in dict)
					{
						_knownFiles.Remove(pair.Key);
						pair.Value.SetChanged(WatcherChangeTypes.Deleted);
						changed = true;
					}

					if (JoinEvents && changed)
						ScheduleJoinedEvent();
				}
				catch (Exception ex)
				{
					Error?.Invoke(this, new ErrorEventArgs(ex));
				}
				finally
				{
					_semaphore.Release();
				}
	}

	void ScheduleJoinedEvent()
	{
		lock (_lock)
		{
#if USE_SCHEDULE
				if (_eventDelay == null)
					_eventDelay = _schedule.Subscribe(this, () => DelayedEventCallback());

				_eventDelay.Next = DateTime.Now + EventDelay;
#else
			_eventDelay ??= new Timer(_ => DelayedEventCallback());
			_eventDelay.Change((int)EventDelay.TotalMilliseconds, -1);
#endif
		}
	}

	void DelayedEventCallback()
	{
		lock (_lock)
			using (AsyncContext.Push())
				Try.IgnoreErrors(() =>
				{
#if !USE_SCHEDULE
					_eventDelay?.Dispose();
					_eventDelay = null;
#endif
					if (!_working)
						return;

					if (_changedFiles.Count > 0)
					{
						var handler = Event;
						if (handler != null)
						{
							var ea = new FileWatcherEventArgs();
							foreach (var f in _changedFiles)
								ea.Files.Add(f.FileSystemEventArgs);

							_changedFiles.Clear();
							handler(this, ea);
						}

						FireOn(WatcherChangeTypes.Created, Created);
						FireOn(WatcherChangeTypes.Changed, Changed);
						FireOn(WatcherChangeTypes.Deleted, Deleted);
						FireOn(WatcherChangeTypes.Renamed, Renamed);

						_changedFiles.Clear();
					}
				});
	}

	void FireOn(WatcherChangeTypes changeType, EventHandler<FileWatcherEventArgs> handler)
	{
		if (handler != null)
		{
			var ea = new FileWatcherEventArgs();
			foreach (var f in _changedFiles.Where(f => f.FileSystemEventArgs.ChangeType == changeType))
				ea.Files.Add(f.FileSystemEventArgs);

			handler(this, ea);
		}
			
		if (UseFileSystemWatcher && _fileSystemWatcher == null && Directory.Exists(_directory))
			SetupFileSystemWatcher();
	}

	/// <summary>
	/// Запуск мониторинга файлов
	/// </summary>
	/// <returns>Объект this для вызова в цепочке</returns>
	public FileWatcher Start()
	{
		if (!_working)
		{
			_working = true;

			if (_watchInterval.Ticks > 0)
				SetupTimer();

			if (_useFileSystemWatcher)
				SetupFileSystemWatcher();
		}
		return this;
	}

	/// <summary>
	/// Останавливает мониторинг файлов и освобождает все ресурсы. Синоним метода Dispose().
	/// </summary>
	public void Stop()
	{
		if (!_working)
			return;

		_working = false;

		DisposeFileSystemWatcher();
		DisposeTimer();

		if(_knownFiles != null)
			foreach (var pair in _knownFiles)
				pair.Value.Dispose();

		_changedFiles = null;
		_knownFiles = null;
	}

	void DisposeFileSystemWatcher()
	{
		if (_fileSystemWatcher != null)
		{
			_fileSystemWatcher.EnableRaisingEvents = false;
			_fileSystemWatcher.Dispose();
			_fileSystemWatcher = null;
		}
	}

	void DisposeTimer()
	{
		_timer?.Dispose();
		_timer = null;
			
		_eventDelay?.Dispose();
		_eventDelay = null;
	}

	/// <summary>
	/// Останавливает мониторинг файлов и освобождает все ресурсы. Синоним метода Stop().
	/// </summary>
	public void Dispose() 
		=> Stop();
}