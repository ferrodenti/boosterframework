using System.Collections.Generic;
using System.IO;
using System.Linq;
using Booster.Log;
using JetBrains.Annotations;

#nullable enable

namespace Booster.FileSystem;

[PublicAPI]
public class DirView //TODO: Объектно ориентированный доступ к фвйловой системе
{
	protected ILog? Log;
	public FilePath Path { get; }

	FastLazy<DirectoryInfo>? _directoryInfo;
	protected DirectoryInfo DirectoryInfo => _directoryInfo ??= new DirectoryInfo(Path);
	
	public DirView(FilePath path, ILog? log)
	{
		Path = path;
		Log = log.Create(this);
	}
	
	protected DirView(DirectoryInfo directoryInfo, ILog? log)
	{
		_directoryInfo = new FastLazy<DirectoryInfo>(directoryInfo);
		Path = directoryInfo.FullName;
		Log = log.Create(this);
	}
	
	protected IEnumerable<DirView> GetDirectories(string searchPatern, SearchOption searchOption = SearchOption.TopDirectoryOnly)
		=> DirectoryInfo.EnumerateDirectories(searchPatern, searchOption)
			.Select(di => new DirView(di, Log));

	public IEnumerable<DirView> Directories => GetDirectories("*");
	public IEnumerable<DirView> AllDirectories => GetDirectories("*", SearchOption.AllDirectories);
	
}
