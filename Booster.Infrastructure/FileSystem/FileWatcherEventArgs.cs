using System;
using System.Collections.Generic;
using System.IO;

namespace Booster.FileSystem;

/// <summary>
/// Аргументы события Booster.FileSystem.FileWatcher. Содержат список измененных файлов
/// </summary>
public class FileWatcherEventArgs : EventArgs
{
	/// <summary>
	/// Список измененных файлов
	/// </summary>
	public List<FileSystemEventArgs> Files { get; }

	/// <summary>
	/// Создает новый экземпляр Booster.FileSystem.FileWatcherEventArgs с пустым инициализированным списком файлов
	/// </summary>
	public FileWatcherEventArgs()
		=> Files = new List<FileSystemEventArgs>();

	/// <summary>
	/// Создает новый экземпляр Booster.FileSystem.FileWatcherEventArgs для одного файла
	/// </summary>
	public FileWatcherEventArgs(FileSystemEventArgs fileSystemEventArgs)
		=> Files = new List<FileSystemEventArgs> {fileSystemEventArgs};
}