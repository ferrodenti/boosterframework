using System.IO;
using System.Threading.Tasks;
using Booster.Helpers;
using Booster.Log;
using JetBrains.Annotations;

#nullable enable

namespace Booster.FileSystem;

[PublicAPI]
public static class FileUtils
{
	#region Directory operations

	public static bool CheckFolderWriteAccess(FilePath path)
	{
		try
		{
			do
			{
				if (path.IsSome() && !Directory.Exists(path))
					return false;

				var rndName = path + Path.GetRandomFileName();
				
				if (File.Exists(rndName))
					continue;
				
				const string txt = "WRITE ACCESS";
				
				File.WriteAllText(rndName, txt);
				var act = File.ReadAllText(rndName);
				File.Delete(rndName);
				return txt == act;
			} while (true);
		}
		catch
		{
			return false;
		}
	}

	#region Ensure

	public static bool EnsureDirectory(FilePath? path, TaskRepeatOptions? options = null)
		=> Utils.RepeatTillSuccess(o =>
		{
			if (path.IsSome() && !Directory.Exists(path))
			{
				o.Log.Trace($"Создаю директорию: {path}");
				Directory.CreateDirectory(path);
			}

			return true;
		}, options);

	public static Task<bool> EnsureDirectoryAsync(FilePath? path, TaskRepeatOptions? options = null)
		=> Utils.RepeatTillSuccessAsync(o =>
		{
			if (path.IsSome() && !Directory.Exists(path))
			{
				o.Log.Trace($"Создаю директорию: {path}");
				Directory.CreateDirectory(path);
			}

			return Task.FromResult(true);
		}, options);

	#endregion

	#region Delete

	public static bool DeleteDirectory(FilePath path, bool recursive = true, TaskRepeatOptions? options = null)
		=> Utils.RepeatTillSuccess(o =>
		{
			if (!Directory.Exists(path))
				return false;
			
			o.Log.Trace($"Удаляю директорию: {path}");
			Directory.Delete(path, recursive);
			return true;

		}, options);

	public static Task<bool> DeleteDirectoryAsync(FilePath path, bool recursive = true, TaskRepeatOptions? options = null)
		=> Utils.RepeatTillSuccessAsync(o =>
		{
			if (!Directory.Exists(path))
				return Task.FromResult(false);

			o.Log.Trace($"Удаляю директорию: {path}");
			Directory.Delete(path, recursive);
			return Task.FromResult(true);

		}, options);

	#endregion

	#endregion

	#region File operations

	public static bool DeleteFile(FilePath filename, TaskRepeatOptions? options = null)
		=> Utils.RepeatTillSuccess(o =>
		{
			if (!File.Exists(filename))
			{
				o.Log.Trace($"Файл не найден: {filename}");
				return false;
			}

			o.Log.Trace($"Удаляю файл: {filename}");
			File.Delete(filename);
			return true;

		}, options);

	public static Task<bool> DeleteFileAsync(FilePath filename, TaskRepeatOptions? options = null)
		=> Utils.RepeatTillSuccessAsync(o =>
		{
			if (!File.Exists(filename))
			{
				o.Log.Trace($"Файл не найден: {filename}");
				return Task.FromResult(false);
			}

			o.Log.Trace($"Удаляю файл: {filename}");
			File.Delete(filename);
			return Task.FromResult(true);

		}, options);

	#region Move

	static bool MoveFileInt(FilePath source, FilePath destination, bool overwrite, TaskRepeatOptions o)
	{
		if (!File.Exists(source))
		{
			o.Log.Trace($"Файл не найден: {source}");
			return false;
		}

		if (File.Exists(destination))
		{
			o.Log.Trace($"Файл уже существует: {destination}");
			
			if (overwrite)
				DeleteFile(destination, o);
			else
				return false;
		}
		o.Log.Trace($"Перемещаю файл: {source} => {destination}");
		File.Move(source, destination);
		return true;
	}
	
	static async Task<bool> MoveFileIntAsync(FilePath source, FilePath destination, bool overwrite, TaskRepeatOptions o)
	{
		if (!File.Exists(source))
		{
			o.Log.Trace($"Файл не найден: {source}");
			return false;
		}

		if (File.Exists(destination))
		{
			o.Log.Trace($"Файл уже существует: {destination}");
			
			if (overwrite)
				await DeleteFileAsync(destination, o).NoCtx();
			else
				return false;
		}
		o.Log.Trace($"Перемещаю файл: {source} => {destination}");
		File.Move(source, destination);
		return true;
	}


	public static bool MoveFile(FilePath source, FilePath destination, bool overwrite = true, TaskRepeatOptions? options = null)
	{
		if (IsCaseChange(source, destination, out var tmp))
			return Utils.RepeatTillSuccess(o => MoveFileInt(source, tmp, overwrite, o), options) &&
				   Utils.RepeatTillSuccess(o => MoveFileInt(tmp, destination, overwrite, o), options);

		return Utils.RepeatTillSuccess(o => MoveFileInt(source, destination, overwrite, o), options);
	}

	public static async Task<bool> MoveFileAsync(FilePath source, FilePath destination, bool overwrite = true, TaskRepeatOptions? options = null)
	{
		if (IsCaseChange(source, destination, out var tmp))
			return await Utils.RepeatTillSuccessAsync(o => MoveFileIntAsync(source, tmp, overwrite, o), options).NoCtx() &&
				   await Utils.RepeatTillSuccessAsync(o => MoveFileIntAsync(tmp, destination, overwrite, o), options).NoCtx();

		return await Utils.RepeatTillSuccessAsync(o => MoveFileIntAsync(source, destination, overwrite, o), options).NoCtx();
	}

	#endregion

	#region Copy

	static bool CopyFileInt(FilePath source, FilePath destination, bool overwrite, TaskRepeatOptions o)
	{
		if (!File.Exists(source))
		{
			o.Log.Trace($"Файл не найден: {source}");
			return false;
		}

		if (destination.DirectoryEnding && source.Filename.IsSome())
			destination += source.Filename;

		if (File.Exists(destination))
		{
			o.Log.Trace($"Файл уже существует: {destination}");

			if (overwrite)
				DeleteFile(destination, o);
			else
				return false;
		}
		o.Log.Trace($"Перемещаю файл: {source} => {destination}");
		File.Copy(source, destination);
		return true;
	}

	static async Task<bool> CopyFileIntAsync(FilePath source, FilePath destination, bool overwrite, TaskRepeatOptions o)
	{
		if (!File.Exists(source))
		{
			o.Log.Trace($"Файл не найден: {source}");
			return false;
		}

		if (File.Exists(destination))
		{
			o.Log.Trace($"Файл уже существует: {destination}");

			if (overwrite)
				await DeleteFileAsync(destination, o).NoCtx();
			else
				return false;
		}
		o.Log.Trace($"Перемещаю файл: {source} => {destination}");
		File.Copy(source, destination);
		return true;
	}


	public static bool CopyFile(FilePath source, FilePath destination, bool overwrite = true, TaskRepeatOptions? options = null)
		=> Utils.RepeatTillSuccess(o => CopyFileInt(source, destination, overwrite, o), options);

	public static async Task<bool> CopyFileAsync(FilePath source, FilePath destination, bool overwrite = true, TaskRepeatOptions? options = null)
		=> await Utils.RepeatTillSuccessAsync(o => CopyFileIntAsync(source, destination, overwrite, o), options).NoCtx();

	#endregion

	#endregion


	/*
		public static bool SafeMove(FilePath source, FilePath destination, bool overwrite, TaskRepeatOptions? options = null)
		{
			if (IsCaseChange(source, destination, out var tmp))
				return Utils.RepeatTillSuccess(() => _controller.Move(source, tmp, overwrite), options) &&
					   Utils.RepeatTillSuccess(() => _controller.Move(tmp, destination, overwrite), options);

			return Utils.RepeatTillSuccess(() => _controller.Move(source, destination, overwrite), options);
		}

		public static Task<bool> SafeMoveAsync(string source, string destination, bool overwrite, TaskRepeatOptions? options = null)
		{
			options ??= TaskRepeatOptions.Default;
			return Utils.RepeatTillSuccessAsync(() => FileAsync.MoveAsync(source, destination, overwrite, options.CancellationToken), options);
		}

		public static bool SafeCopy(string source, string destination, bool overwrite, TaskRepeatOptions? options = null)
			=> Utils.RepeatTillSuccess(() =>
			{
				File.Copy(source, destination, overwrite);
				return true;
			}, options);

		public static Task<bool> SafeCopyAsync(string source, string destination, bool overwrite, TaskRepeatOptions? options = null)
		{
			options ??= TaskRepeatOptions.Default;
			return Utils.RepeatTillSuccessAsync(() => FileAsync.CopyAsync(source, destination, overwrite, options.CancellationToken), options);
		}



		public static bool SafeDelete(FilePath filename, TaskRepeatOptions? options = null)
			=> Utils.RepeatTillSuccess(() => _controller.Delete(filename), options);



		public static bool SafeCopy(FilePath source, FilePath destination, bool overwrite, TaskRepeatOptions? options = null)
			=> Utils.RepeatTillSuccess(() => _controller.Copy(source, destination, overwrite), options);

		#endregion

		#region Async operations


		public static Task<bool> SafeDeleteAsync(FilePath filename, TaskRepeatOptions? options = null)
			=> Utils.RepeatTillSuccessAsync(() =>  _controller.DeleteAsync(filename), options);

		public static async Task<bool> SafeMoveAsync(FilePath source, FilePath destination, bool overwrite, TaskRepeatOptions? options = null)
		{
			options ??= TaskRepeatOptions.Default;

			if (IsCaseChange(source, destination, out var tmp))
				return await Utils.RepeatTillSuccessAsync(() => _controller.MoveAsync(source, tmp, overwrite, options.CancellationToken), options).NoCtx() &&
					   await Utils.RepeatTillSuccessAsync(() => _controller.MoveAsync(tmp, destination, overwrite, options.CancellationToken), options).NoCtx();

			return await Utils.RepeatTillSuccessAsync(() => _controller.MoveAsync(source, destination, overwrite, options.CancellationToken), options).NoCtx();
		}

		public static Task<bool> SafeCopyAsync(FilePath source, FilePath destination, bool overwrite, TaskRepeatOptions? options = null)
		{
			options ??= TaskRepeatOptions.Default;
			return Utils.RepeatTillSuccessAsync(() => _controller.CopyAsync(source, destination, overwrite, options.CancellationToken), options);
		}

		#endregion
		*/


	static bool IsCaseChange(string source, string destination, out string tempName)
	{
		tempName = default!;
		var len1 = source.Length;
		if (len1 != destination.Length)
			return false;

		var result = false;
		for (var i = 0; i < len1; ++i)
		{
			var src = source[i];
			var dst = destination[i];
			if (src == dst)
				continue;
			
			if (CharComparer.IgnoreCase.IsEqual(src, dst))
			{
				result = true;
				continue;
			}

			return false;
		}

		if (result)
		{
			tempName = UniqueGenerator.Generate(
				d => !Directory.Exists(d),
				_ => $"{source.TrimEnd('\\', '/')}_{Rnd.Int(0, int.MaxValue)}");
			
			return true;
		}
		return false;
	}
}