using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using JetBrains.Annotations;

#nullable enable

using static Booster.FastLazyStatic;

namespace Booster.FileSystem;

[PublicAPI]
public class FilePath
{
	public ReadOnlyCollection<string> Segments { get; }
	public bool DirectoryEnding { get; }
	public bool DirectoryStarting { get; }

	public int Length => ToString().Length;
	public bool IsSome => Segments.Count > 0;

	FastLazy<string?>? _name;
	public string? Name => _name ??= (string?)Segments.LastOrDefault();
	
	FastLazy<FilePath?>? _fileName;

	public FilePath? Filename => _fileName ??= DirectoryEnding ? null : (FilePath?) Segments.LastOrDefault(); //TODO: set

	FastLazy<FilePath?>? _fileNameWithoutExtension;
	public FilePath? FilenameWithoutExtension => ParseFilename(() => _fileNameWithoutExtension?.Value); //TODO: set

	FastLazy<FilePath?>? _extension;
	public FilePath? Extension => ParseFilename(() => _extension?.Value); //TODO: set

	T ParseFilename<T>(Func<T> getter)
	{
		var filename = Filename?.ToString();
		if (filename == null)
		{
			_fileNameWithoutExtension = new FastLazy<FilePath?>();
			_extension = new FastLazy<FilePath?>();
		}
		else
		{
			var i = filename.IndexOf('.');
			if (i < 0)
			{
				_fileNameWithoutExtension = new FastLazy<FilePath?>(filename);
				_extension = new FastLazy<FilePath?>();
			}
			else
			{
				_fileNameWithoutExtension = new FastLazy<FilePath?>(filename.Substring(0, i));
				_extension = new FastLazy<FilePath?>(filename.Substring(i));
			}
		}

		return getter();
	}

	FastLazy<FilePath>? _directory;
	public FilePath Directory => _directory ??= Filename == null ? this : GetParentDirectory();
	
	FastLazy<FilePath>? _parentDirectory;
	public FilePath ParentDirectory => _parentDirectory ??= !DirectoryEnding ? Directory : GetParentDirectory();

	FilePath GetParentDirectory()
		=> this + "\\..\\";

	FastLazy<bool>? _isAbsolute;
	public bool IsAbsolute => _isAbsolute ??= FastLazy(() =>
	{
		//TODO: network folders
		if (!DirectoryStarting)
			return Segments.FirstOrDefault()?.LastOrDefault() == ':';

		return false;
	});

	public static FilePath CurrectDirectory
	{
		get => System.IO.Directory.GetCurrentDirectory();
		set => System.IO.Directory.SetCurrentDirectory(value);
	}

	protected FilePath(IList<string> segments, bool directoryStarting, bool directoryEnding)
	{
		Segments = new ReadOnlyCollection<string>(segments);
		DirectoryStarting = directoryStarting;
		DirectoryEnding = directoryEnding;
	}

	public FilePath(string? value)
	{
		value = value?.Trim();
		if (value?.Length > 0)
		{
			var start = value[0];
			// ReSharper disable once UseIndexFromEndExpression
			var end = value[value.Length - 1];

			if (start == '\\' || start == '/')
				DirectoryStarting = value.Length > 1;
			if (end == '\\' || end == '/')
				DirectoryEnding = true;

			Segments = new ReadOnlyCollection<string>(value.SplitNonEmpty('\\', '/'));
		}
		else
			Segments = new ReadOnlyCollection<string>(new List<string>());
	}

	static FilePath Join(IEnumerable<string?> values) 
		=> new("\\".JoinNonEmpty(values));

	public static FilePath AsDirectory(params string?[] values)
		=> Join(values).AsDirectory();

	public static FilePath AsFile(params string?[] values)
		=> Join(values).AsFile();
	
	public FilePath AsFile()
		=> DirectoryEnding ? new FilePath(Segments, DirectoryStarting, false) : this;
	
	public FilePath AsDirectory() 
		=> DirectoryEnding ? this : new FilePath(Segments, DirectoryStarting, true);

	public FilePath ChangeFilename(string filename)
		=> Directory + filename;

	public FilePath ChangeExtension(string extension)
	{
		var filename = Path.ChangeExtension(Filename ?? "", extension);
		return ChangeFilename(filename);
	}

	public FilePath Clone()
		=> new(Segments, DirectoryStarting, DirectoryEnding);

	public bool Contains(FilePath path)
	{
		if (IsAbsolute == !path.IsAbsolute)
			return IsAbsolute;

		if (path.Segments.Count < Segments.Count)
			return false;

		return !Segments.Where((t, i) => !t.EqualsIgnoreCase(path.Segments[i])).Any();
	}

	public FilePath Combine(params FilePath[] parts)
	{
		try
		{
			var segments = new List<string>(Segments);
			var dirEnding = false;
			
			foreach (var part in parts)
			{
				if (part.IsAbsolute)
					throw new Exception("Exected relative path: " + part);

				segments.AddRange(part.Segments);
				dirEnding = part.DirectoryEnding;
			}
			ApplyCommands(segments);

			return new FilePath(segments, DirectoryStarting, dirEnding);
		}
		catch (Exception e)
		{
			Utils.Nop(e);
			throw;
		}
		
	}

	public FilePath RemoveDirectoryEnding() 
		=> new(Segments, DirectoryStarting, false);

	public FilePath MakeRooted()
		=> !Path.IsPathRooted(this) 
			? CurrectDirectory.Combine(this) 
			: this;

	public FilePath MakeRelative(FilePath root)
	{
		if (!IsAbsolute)
			throw new Exception("Exected absolute path: " + this);

		if (!root.IsAbsolute || root.Segments[0] != Segments[0])
			return this;

		var segments = new List<string>();
		var i = 0;
		var len = Math.Min(Segments.Count, root.Segments.Count);

		for (; i < len; i++)
			if (!string.Equals(Segments[i], root.Segments[i], StringComparison.OrdinalIgnoreCase))
				break;

		for (var j = i; j < root.Segments.Count; j++)
			segments.Add("..");

		for (; i < Segments.Count; i++)
			segments.Add(Segments[i]);

		return new FilePath(segments, false, DirectoryEnding);
	}

	public FilePath MakeAbsolute(FilePath? root = null)
	{
		if (IsAbsolute)
			return this;

		if (root == null)
			root = new FilePath(System.IO.Directory.GetCurrentDirectory());

		return root.Combine(this);
	}

	public FilePath MakeDir(bool directoryEnding = true)
		=> DirectoryEnding == directoryEnding 
			? this 
			: new FilePath(Segments, DirectoryStarting, directoryEnding);

	protected void ApplyCommands(List<string> segments)
	{
		for (var i = 0; i < segments.Count; i++)
			switch (segments[i])
			{
			case "~":
				segments.RemoveAt(i);
				i--;
				break;
			case "..":
				if (i > 1 && segments[i - 1] != "..")
				{
					segments.RemoveAt(i);
					segments.RemoveAt(i - 1);
					i -= 2;
				}
				break;
			}
	}

	FastLazy<string>? _toString;
	public override string ToString()
		=> _toString ??= FastLazy(() =>
		{
			var sepa = Path.DirectorySeparatorChar.ToString();
			return $"{(DirectoryStarting ? sepa : "")}{string.Join(sepa, Segments)}{(DirectoryEnding ? sepa : "")}";
		});

	public override bool Equals(object? obj)
	{
		if (ReferenceEquals(this, obj))
			return true;

		var b = obj as FilePath;

		return b != null &&
			   Segments.SequenceEqual(b.Segments, StringComparer.OrdinalIgnoreCase) &&
			   DirectoryStarting == b.DirectoryStarting &&
			   DirectoryEnding == b.DirectoryEnding;
	}

	FastLazy<int>? _hashCode;

	public override int GetHashCode() // ReSharper disable once NonReadonlyMemberInGetHashCode
		=> _hashCode ??=
			new HashCode(DirectoryStarting, DirectoryEnding)
				.AppendEnumerable(Segments.Select(s => s.ToLower())).Value;

	public static FilePath operator+(FilePath a, FilePath b)
		=> a.Combine(b);

	public static bool operator==(FilePath? a, FilePath? b)
	{
		if (Equals(a, null))
			return Equals(b, null);
			
		return !Equals(b, null) && a.Equals(b);
	}
	public static bool operator!=(FilePath? a, FilePath? b)
	{
		if (Equals(a, null))
			return !Equals(b, null);
			
		return Equals(b, null) || !a.Equals(b);
	}

	public static implicit operator string(FilePath path)
		=> path.With(p => p.ToString(), "");

	public static implicit operator FilePath(string path)
		=> path.With(p => new FilePath(p))!;
}

public static class FilePathExpander
{
	[ContractAnnotation("null => true")]
	public static bool IsEmpty([NotNullWhen(false)] this FilePath? path)
		=> path?.IsSome != true;

	[ContractAnnotation("null => false")]
	public static bool IsSome([NotNullWhen(true)] this FilePath? path)
		=> path?.IsSome == true;
}