#if !NET_STANDARD

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Booster.Hacks.System;
using JetBrains.Annotations;

namespace Booster.FileSystem
{
	[PublicAPI]
	public static class FileAsync
	{
		static readonly Encoding _utf8NoBom = new UTF8Encoding(false, true);
		const int _bufferSize = 4096;

		public static async Task<bool> MoveAsync(string source, string destination, bool overwrite, CancellationToken cancellationToken = default)
		{
			if (!await CopyAsync(source, destination, overwrite, cancellationToken).NoCtx())
				return false;
			
			cancellationToken.ThrowIfCancellationRequested();

			File.Delete(source);
			
			return true;
		}
		
		public static async Task<bool> CopyAsync(string source, string destination, bool overwrite, CancellationToken cancellationToken = default)
		{
			const FileOptions fileOptions = FileOptions.Asynchronous | FileOptions.SequentialScan;
			const int bufferSize = 4096;
			
			if (File.Exists(destination))
			{
				if (overwrite)
					File.Delete(destination);
				else
					return false;
			}
#if NETSTANDARD2_1
			await using var sourceStream = new FileStream(source, FileMode.Open, FileAccess.Read, FileShare.Read, bufferSize, fileOptions);
			await using var destinationStream = new FileStream(destination, FileMode.CreateNew, FileAccess.Write, FileShare.None, bufferSize, fileOptions);
#else
			using var sourceStream = new FileStream(source, FileMode.Open, FileAccess.Read, FileShare.Read, bufferSize, fileOptions);
			using var destinationStream = new FileStream(destination, FileMode.CreateNew, FileAccess.Write, FileShare.None, bufferSize, fileOptions);
#endif
			await sourceStream.CopyToAsync(destinationStream, bufferSize, cancellationToken).NoCtx();

			return true;
		}

		public static Task<string> ReadAllTextAsync(string path, CancellationToken cancellationToken = default) 
			=> ReadAllTextAsync(path, Encoding.UTF8, cancellationToken);

		public static Task<string> ReadAllTextAsync(string path, Encoding encoding, CancellationToken cancellationToken = default)
		{
			if (path == null)
				throw new ArgumentNullException(nameof (path));
			if (encoding == null)
				throw new ArgumentNullException(nameof (encoding));
			if (path.Length == 0)
				throw new ArgumentException(SR.Argument_EmptyPath, nameof(path));

			return !cancellationToken.IsCancellationRequested 
				? InternalReadAllTextAsync(path, encoding, cancellationToken) 
				: Task.FromCanceled<string>(cancellationToken);
		}

		public static Task WriteAllTextAsync(string path, string contents, CancellationToken cancellationToken = default) 
			=> WriteAllTextAsync(path, contents, _utf8NoBom, cancellationToken);

		public static Task WriteAllTextAsync(string path, string contents, Encoding encoding, CancellationToken cancellationToken = default)
		{
			if (path == null)
				throw new ArgumentNullException(nameof (path));
			if (encoding == null)
				throw new ArgumentNullException(nameof (encoding));
			if (path.Length == 0)
				throw new ArgumentException(SR.Argument_EmptyPath, nameof (path));
			if (cancellationToken.IsCancellationRequested)
				return Task.FromCanceled(cancellationToken);
			if (!string.IsNullOrEmpty(contents))
				return InternalWriteAllTextAsync(AsyncStreamWriter(path, encoding, false), contents, cancellationToken);

			new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.Read).Dispose();

			return Task.CompletedTask;
		}

		public static Task<byte[]> ReadAllBytesAsync(string path, CancellationToken cancellationToken = default)
		{
			if (cancellationToken.IsCancellationRequested)
				return Task.FromCanceled<byte[]>(cancellationToken);

			var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read, 1, FileOptions.Asynchronous | FileOptions.SequentialScan);
			var shouldDisposeStream = false;
			try
			{
				var length = fs.Length;
				if (length > int.MaxValue)
					return Task.FromException<byte[]>(new IOException(SR.IO_FileTooLong2GB));

				shouldDisposeStream = true;
				return length > 0L 
					? InternalReadAllBytesAsync(fs, (int) length, cancellationToken) 
					: InternalReadAllBytesUnknownLengthAsync(fs, cancellationToken);
			}
			finally
			{
				if (!shouldDisposeStream)
					fs.Dispose();
			}
		}

		public static Task WriteAllBytesAsync(string path, byte[] bytes, CancellationToken cancellationToken = default)
		{
			if (path == null)
				throw new ArgumentNullException(nameof (path), SR.ArgumentNull_Path);
			if (path.Length == 0)
				throw new ArgumentException(SR.Argument_EmptyPath, nameof (path));
			if (bytes == null)
				throw new ArgumentNullException(nameof (bytes));

			return !cancellationToken.IsCancellationRequested 
				? InternalWriteAllBytesAsync(path, bytes, cancellationToken) 
				: Task.FromCanceled(cancellationToken);
		}

		static async Task InternalWriteAllBytesAsync(string path, byte[] bytes, CancellationToken cancellationToken)
		{
#if NETSTANDARD2_1
			await using var fs = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.Read, _bufferSize, FileOptions.Asynchronous | FileOptions.SequentialScan);
#else
			using var fs = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.Read, _bufferSize, FileOptions.Asynchronous | FileOptions.SequentialScan);
#endif
			await fs.WriteAsync(bytes, 0, bytes.Length, cancellationToken).NoCtx();
			await fs.FlushAsync(cancellationToken).NoCtx();
		}

		public static Task<string[]> ReadAllLinesAsync(string path, CancellationToken cancellationToken = default) 
			=> ReadAllLinesAsync(path, Encoding.UTF8, cancellationToken);

		public static Task<string[]> ReadAllLinesAsync(string path, Encoding encoding, CancellationToken cancellationToken = default)
		{
			if (path == null)
				throw new ArgumentNullException(nameof (path));
			if (encoding == null)
				throw new ArgumentNullException(nameof (encoding));
			if (path.Length == 0)
				throw new ArgumentException(SR.Argument_EmptyPath, nameof (path));

			return !cancellationToken.IsCancellationRequested 
				? InternalReadAllLinesAsync(path, encoding, cancellationToken) 
				: Task.FromCanceled<string[]>(cancellationToken);
		}


		public static Task WriteAllLinesAsync(string path, IEnumerable<string> contents, CancellationToken cancellationToken = default) 
			=> WriteAllLinesAsync(path, contents, _utf8NoBom, cancellationToken);

		public static Task WriteAllLinesAsync(string path, IEnumerable<string> contents, Encoding encoding, CancellationToken cancellationToken = default)
		{
			if (path == null)
				throw new ArgumentNullException(nameof (path));
			if (contents == null)
				throw new ArgumentNullException(nameof (contents));
			if (encoding == null)
				throw new ArgumentNullException(nameof (encoding));
			if (path.Length == 0)
				throw new ArgumentException(SR.Argument_EmptyPath, nameof (path));

			return !cancellationToken.IsCancellationRequested 
				? InternalWriteAllLinesAsync(AsyncStreamWriter(path, encoding, false), contents, cancellationToken) 
				: Task.FromCanceled(cancellationToken);
		}

		public static Task AppendAllTextAsync(string path, string contents, CancellationToken cancellationToken = default) 
			=> AppendAllTextAsync(path, contents, _utf8NoBom, cancellationToken);

		public static Task AppendAllTextAsync(string path, string contents, Encoding encoding, CancellationToken cancellationToken = default)
		{
			if (path == null)
				throw new ArgumentNullException(nameof (path));
			if (encoding == null)
				throw new ArgumentNullException(nameof (encoding));
			if (path.Length == 0)
				throw new ArgumentException(SR.Argument_EmptyPath, nameof (path));
			if (cancellationToken.IsCancellationRequested)
				return Task.FromCanceled(cancellationToken);

			if (!string.IsNullOrEmpty(contents))
				return InternalWriteAllTextAsync(AsyncStreamWriter(path, encoding, true), contents, cancellationToken);

			new FileStream(path, FileMode.Append, FileAccess.Write, FileShare.Read).Dispose();

			return Task.CompletedTask;
		}

		public static Task AppendAllLinesAsync(string path, IEnumerable<string> contents, CancellationToken cancellationToken = default) 
			=> AppendAllLinesAsync(path, contents, _utf8NoBom, cancellationToken);

		public static Task AppendAllLinesAsync(string path, IEnumerable<string> contents, Encoding encoding, CancellationToken cancellationToken = default)
		{
			if (path == null)
				throw new ArgumentNullException(nameof (path));
			if (contents == null)
				throw new ArgumentNullException(nameof (contents));
			if (encoding == null)
				throw new ArgumentNullException(nameof (encoding));
			if (path.Length == 0)
				throw new ArgumentException(SR.Argument_EmptyPath, nameof (path));

			return !cancellationToken.IsCancellationRequested 
				? InternalWriteAllLinesAsync(AsyncStreamWriter(path, encoding, true), contents, cancellationToken) 
				: Task.FromCanceled(cancellationToken);
		}

		static StreamReader AsyncStreamReader(string path, Encoding encoding) 
			=> new(new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read, _bufferSize, FileOptions.Asynchronous | FileOptions.SequentialScan), encoding, true);

		static StreamWriter AsyncStreamWriter(string path, Encoding encoding, bool append) 
			=> new(new FileStream(path, append ? FileMode.Append : FileMode.Create, FileAccess.Write, FileShare.Read, _bufferSize, FileOptions.Asynchronous | FileOptions.SequentialScan), encoding);

		static async Task<string> InternalReadAllTextAsync(string path, Encoding encoding, CancellationToken cancellationToken)
		{
			var sr = AsyncStreamReader(path, encoding);
			try
			{
				cancellationToken.ThrowIfCancellationRequested();
				var buffer = new char[sr.CurrentEncoding.GetMaxCharCount(_bufferSize)];
				var sb = new StringBuilder();
				while (true)
				{
					var charCount = await sr.ReadAsync(buffer, 0, buffer.Length).Cancel(cancellationToken).NoCtx();
					if (charCount != 0)
						sb.Append(buffer, 0, charCount);
					else
						break;
				}
				return sb.ToString();
			}
			finally
			{
				sr.Dispose();
			}
		}

		static async Task<byte[]> InternalReadAllBytesAsync(FileStream fs, int count, CancellationToken cancellationToken)
		{
#if NETSTANDARD2_1
			await using (fs)
#else
			using (fs)
#endif
			{
				var index = 0;
				var bytes = new byte[count];
				do
				{
					var num = await fs.ReadAsync(bytes, index, count - index, cancellationToken).NoCtx();
					if (num == 0)
						throw new EndOfStreamException(SR.IO_EOF_ReadBeyondEOF);
					index += num;
				}
				while (index < count);
				return bytes;
			}
		}

		static async Task<byte[]> InternalReadAllBytesUnknownLengthAsync(FileStream fs, CancellationToken cancellationToken)
		{
			try
			{
				var result = new byte[512];
				var bytesRead = 0;
				while (true)
				{
					if (bytesRead == result.Length)
					{
						var num = (uint) (result.Length * 2);
						if (num > 2147483591U)
							num = (uint) Math.Max(2147483591, result.Length + 1);
						var numArray = new byte[num];
						Buffer.BlockCopy(result, 0, numArray, 0, bytesRead);
						result = numArray;
					}
					var num1 = await fs.ReadAsync(result, 0, bytesRead, cancellationToken).NoCtx();
					if (num1 != 0)
						bytesRead += num1;
					else
						break;
				}

				Array.Resize(ref result, bytesRead);
				return result;
			}
			finally
			{
#if NETSTANDARD2_1
				await fs.DisposeAsync().NoCtx();
#else
				fs.Dispose();
#endif
			}
		}

		static async Task<string[]> InternalReadAllLinesAsync(string path, Encoding encoding, CancellationToken cancellationToken)
		{
			using var sr = AsyncStreamReader(path, encoding);
			cancellationToken.ThrowIfCancellationRequested();
			string line;
			var lines = new List<string>();
			while ((line = await sr.ReadLineAsync().NoCtx()) != null)
			{
				lines.Add(line);
				cancellationToken.ThrowIfCancellationRequested();
			}

			return lines.ToArray();
		}

		static async Task InternalWriteAllLinesAsync(TextWriter writer, IEnumerable<string> contents, CancellationToken cancellationToken)
		{
#if NETSTANDARD2_1
			await using (writer)
#else
			using (writer)
#endif
			{
				foreach (var content in contents)
				{
					cancellationToken.ThrowIfCancellationRequested();
					await writer.WriteLineAsync(content).NoCtx();
				}
				cancellationToken.ThrowIfCancellationRequested();
				await writer.FlushAsync().NoCtx();
			}
		}

		static async Task InternalWriteAllTextAsync(StreamWriter sw, string contents, CancellationToken cancellationToken)
		{
			try
			{
				var buffer = new char[_bufferSize];
				var count = contents.Length;
				var index = 0;
				while (index < count)
				{
					var batchSize = Math.Min(_bufferSize, count - index);
					contents.CopyTo(index, buffer, 0, batchSize);
					await sw.WriteAsync(buffer, 0, batchSize).Cancel(cancellationToken).NoCtx();
					index += batchSize;
				}
				cancellationToken.ThrowIfCancellationRequested();
				await sw.FlushAsync().NoCtx();
			}
			finally
			{
#if NETSTANDARD2_1
				await sw.DisposeAsync().NoCtx();
#else
				sw.Dispose();
#endif			
			}
		}
	}
}
#endif