using System.IO;
using JetBrains.Annotations;

#nullable enable

namespace Booster.FileSystem;

[PublicAPI]
public static class UniqueFilenameGenerator
{
	class Cache
	{
		public readonly string? Directory;
		public readonly string Filename;
		public readonly string Extension;

		public Cache(string filename)
		{
			Directory = Path.GetDirectoryName(filename);
			Filename = Path.GetFileNameWithoutExtension(filename);
			Extension = Path.GetExtension(filename);
		}

	}
	public static string Generate(
		string initialPath, 
		string pattern = "{0}({1})", 
		int from = 2, 
		int to = int.MaxValue)
	{
		Cache? cache = null;
		
		return UniqueGenerator.Generate(initialPath,
			filename => !File.Exists(filename),
			i =>
			{
				cache ??= new Cache(initialPath);
				var filename = string.Format(pattern, cache.Filename, i) + cache.Extension;
				return cache.Directory.IsEmpty() ? filename : Path.Combine(cache.Directory!, filename);
			}, from, to);
	}
}
