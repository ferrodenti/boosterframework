using System;
using JetBrains.Annotations;

#nullable enable

namespace Booster;

[PublicAPI]
public class ExceptionEventArgs : EventArgs
{
	public Exception? Exception { get; set; }

	public ExceptionEventArgs()
	{
	}

	public ExceptionEventArgs(Exception exception)
		=> Exception = exception;
}