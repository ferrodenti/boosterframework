using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Booster.Interfaces;
using Booster.Reflection;

namespace Booster.Evaluation;

public class DynamicSettings : IDynamicSettings
{
	readonly TypeEx _entityType;
	readonly Dictionary<string, IDynamicParameter> _inner = new(StringComparer.OrdinalIgnoreCase);

	public DynamicSettings(TypeEx entityType) 
		=> _entityType = entityType;

	public T Get<T>(object target, string key, T @default = default)
		=> Get(target, key, out _, @default);

	public T Get<T>(object target, string key, out bool isConstant, T @default = default)
	{
		isConstant = true;

		if (_inner.TryGetValue(key, out var param))
			try
			{
				isConstant = param.IsConstant;
				var res = param.Evaluate(target);
				if (res != null)
				{
					if (res is T variable)
						return variable;

					if (StringConverter.Default.TryParse(res.ToString(), out T res2))
						return res2;
				}
			}
			catch (Exception e)
			{
				if (param is DynamicParameter dp)
					throw new Exception($"Error evaluating parameter: Key={key}, Expression={dp.Expression}:", e);

				throw;
			}

		return @default;
	}


	public void Add(string key, string value) 
		=> _inner[key] = new DynamicParameter(_entityType, value);

	public void Add(string key, IDynamicParameter value) 
		=> _inner[key] = value;

	public void Add(string key, object value)
	{
		switch (value)
		{
		case string str:
			Add(key, str);
			break;
		case IDynamicParameter da:
			Add(key, da);
			break;
		default:
			_inner[key] = new DynamicParameter(value);
			break;
		}
	}

	public bool ContainsKey(string key) 
		=> _inner.ContainsKey(key);

	public object this[object target, string key] 
		=> _inner.SafeGet(key)?.Evaluate(target);

	public IDynamicParameter this[string key]
	{
		get => _inner.SafeGet(key);
		set => Add(key, value);
	}

	public IDictionary<string, object> GetValues(object target) 
		=> _inner.ToDictionary(p => p.Key, p => p.Value.Evaluate(target), StringComparer.OrdinalIgnoreCase);

	public IDictionary<string, object> GetChangedValues(object target, IDictionary<string, object> old, bool skipStatics = false)
	{
		var res = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
		foreach (var pair in _inner)
		{
			object value;
			if (old.TryGetValue(pair.Key, out var oldValue))
			{
				if (skipStatics && pair.Value.IsStatic)
					continue;

				if (pair.Value.IsConstant)
					continue;

				value = pair.Value.Evaluate(target);
				if (Equals(value, oldValue))
					continue;
			}
			else
				value = pair.Value.Evaluate(target);

			res[pair.Key] = value;
		}
		return res;
	}

	public IEnumerator<KeyValuePair<string, IDynamicParameter>> GetEnumerator() 
		=> _inner.GetEnumerator();

	IEnumerator IEnumerable.GetEnumerator() 
		=> GetEnumerator();
}