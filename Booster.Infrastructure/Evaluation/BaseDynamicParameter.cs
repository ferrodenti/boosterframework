﻿using System;
using System.Collections.Generic;
using System.Text;
using Booster.Interfaces;
using Booster.Parsing;
using Booster.Reflection;
using JetBrains.Annotations;

namespace Booster.Evaluation;

public abstract class BaseDynamicParameter : IDynamicParameter
{
	protected class ParsingContext
	{
		public readonly TypeEx TargetType;
		public readonly Dictionary<string, IDataAccessor> DataAccessors;

		public ParsingContext(TypeEx targetType, Dictionary<string, IDataAccessor> dataAccessors)
		{
			TargetType = targetType;
			DataAccessors = dataAccessors;
		}
	}
	protected readonly TypeEx TargetType;
	public string Expression { get; }
	public bool CacheDataAccessors { get; set; } = true;
	public bool IgnoreCase { get; set; }

	protected virtual string[] BracketEscaping => new [] {"{{", "}}"};

	protected readonly Dictionary<string, Func<object, string>> Formatters = new(StringComparer.OrdinalIgnoreCase);

	bool _staticValueEvaluated;
	object _staticValue;
	public object StaticValue
	{
		get
		{
			if (!_staticValueEvaluated)
			{
				var seq = Sequence;

				if (IsStatic && seq?.Length > 0)
				{
					if (seq.Length == 1)
						return seq[0];

					var str = new StringBuilder();
					foreach (string s in seq)
						str.Append(s);

					_staticValue = str.ToString();

				}
				_staticValueEvaluated = true;
			}
			return _staticValue;
		}
	}

	object[] _sequence;
	protected object[] Sequence
	{
		get
		{
			if (_sequence == null)
				Parse();
			return _sequence;
		}
	}

	bool? _isConstant;
	public bool IsConstant
	{
		get
		{
			_isConstant ??= IsStatic || TargetType == null;

			return _isConstant.Value;
		}	
	}

	bool? _isStatic;
	public bool IsStatic
	{
		get
		{
			if (!_isStatic.HasValue)
				Parse();

			// ReSharper disable once PossibleInvalidOperationException
			return _isStatic.Value;
		}
	}

	TypeEx _valueType;
	public TypeEx ValueType
	{
		get
		{
			if (_valueType == null)
			{
				if (IsStatic)
					_valueType = StaticValue.With(v => v.GetType());
				else if (Sequence.Length == 1)
					_valueType = ((IDataAccessor) Sequence[0]).ValueType;
				else
					_valueType = typeof (string);
			}
			return _valueType;
		}
	}
		
	protected BaseDynamicParameter(TypeEx entityType, string expression)
	{
		TargetType = entityType;
		Expression = expression;
	}

	protected BaseDynamicParameter(object constValue)
	{
		_isStatic = true;
		_staticValue = constValue;
		_staticValueEvaluated = true;
		_sequence = Array.Empty<object>();
	}

	#region Parsing

	protected abstract IDataAccessor CreateDataAccessor(ParsingContext context, string expression);
		
	protected virtual void Parse()
	{
		try
		{
			var dataAccessors = new Dictionary<string, IDataAccessor>();
			var sequence = new List<object>();
			_isStatic = true;

			if (Expression == "")
			{
				_staticValue = "";
				_sequence = Array.Empty<object>();
				_staticValueEvaluated = true;
				return;
			}

			if (Expression != null)
			{
				var parser = new StringParser(Expression);
				var str = new StringBuilder();
				int start = -1, nesting = 0;

				parser.SearchFor(
					new Term(BracketEscaping[0], s =>
					{
						str.Append(s.Buffer);
						str.Append('{');
					}),
					new Term(BracketEscaping[1], s =>
					{
						str.Append(s.Buffer);
						str.Append('}');
					}),
					new Term("\"", s =>
					{
						str.Append(s.Buffer);
						str.Append('\"');
						
						parser.SearchFor(
							new Term("\\\"", true),
							new Term("\"", false)
						).HandleRest(_ => throw new ParsingException("Unable to parse string constant"));
						
						str.Append(Expression.Substring(s.Position, parser.Pos - s.Position));
					}),
					new Term("{", s =>
					{
						if (nesting++ == 0)
						{
							str.Append(s);
							if (str.Length > 0)
							{
								sequence.Add(str.ToString());
								str.Clear();
							}

							start = parser.Pos;
						}

						return true;
					}),
					new Term("}", _ =>
					{
						if (--nesting == 0)
						{
							var subexp = Expression.Substring(start, parser.Pos - 1 - start);
							if (subexp != "")
							{
								if (CacheDataAccessors)
									sequence.Add(GetOrAddDataAccessor(new ParsingContext(TargetType, dataAccessors), subexp));
								else
									sequence.Add(Tuple.Create(subexp));

								_isStatic = false;
								str.Clear();
								start = -1;
							}
						}

						return true;
					}));

				if (str.Length == 0 && sequence.Count == 0)
					sequence.Add(Expression);
				else
				{
					if (start >= 0)
						str.Append(Expression.Substring(Math.Max(0, start - 1)));

					if (str.Length > 0)
						sequence.Add(str.ToString());

					if (!parser.IsEol)
						sequence.Add(Expression.Substring(parser.Pos));
				}
			}

			_sequence = sequence.ToArray();
		}
		catch (ParsingException e)
		{
			throw new ParsingException($"Unable to parse expression: \"{Expression}\"", e);
		}

	}

	protected IDataAccessor GetOrAddDataAccessor(ParsingContext context, string expression)
	{
		IDataAccessor Creator(string exp)
		{
			var da = CreateDataAccessor(context, exp);
			if (da?.CanRead != true) 
				throw new Exception($"{context.TargetType.Name}.{exp} is inaccessible");

			return da;
		}
			
		return context.DataAccessors?.GetOrAdd(expression, Creator) ?? Creator(expression);
	}

	#endregion

	[PublicAPI]
	public void AddFormatter(string name, Func<object, string> formatter) 
		=> Formatters[name] = formatter;

	public object Evaluate(object target)
	{
		if (IsStatic)
			return StaticValue;

		if (Sequence.Length == 1)
			return Evaluate(Sequence[0], target);

		var str = new StringBuilder();
		foreach (var obj in Sequence)
			str.Append(Evaluate(obj, target));

		return str.ToString();
	}

	protected object Evaluate(object sequenceObj, object target)
		=> sequenceObj is string 
			? sequenceObj 
			: (sequenceObj as IDataAccessor ?? CreateDataAccessor(new ParsingContext(target?.GetType(), null), ((Tuple<string>) sequenceObj).Item1)).GetValue(target);
		
	public override string ToString()
		=> $"{{{TargetType}}}: {Expression}";
}