﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using Booster.Interfaces;
using Booster.Parsing;
using Booster.Reflection;

namespace Booster.Evaluation;

public class DynamicParameter : BaseDynamicParameter
{
	#region DataAccessors

	abstract class BaseReadOnlyDataAccessor : IDataAccessor
	{
		public string Name { get; protected set; }
		public TypeEx ValueType { get; protected set; }
		public virtual bool CanRead => true;
		public bool CanWrite => false;

		public abstract object GetValue(object obj);

		public bool SetValue(object obj, object value) 
			=> throw new NotSupportedException();
	}

	class StaticTypeAccessor : BaseReadOnlyDataAccessor
	{
		public override bool CanRead => false;
			
		public StaticTypeAccessor(TypeEx valueType)
		{
			ValueType = valueType;
			Name = valueType.Name;
		}

		public override object GetValue(object obj) 
			=> throw new NotSupportedException();
	}

	class ThisDataAccessor : BaseReadOnlyDataAccessor
	{
		public ThisDataAccessor(TypeEx valueType)
		{
			Name = "this";
			ValueType = valueType;
		}

		public override object GetValue(object obj) => obj;
	}

	class ConstDataAccessor : BaseReadOnlyDataAccessor
	{
		readonly object _value;
			
		public ConstDataAccessor(object value)
		{
			_value = value;
			ValueType = value?.GetType();
			Name = $"Const ({ValueType?.Name})";
		}

		public override object GetValue(object obj)
			=> _value;
	}

	#endregion

	public TypeEx[] AccessAttributes { get; set; }
	public Assembly[] AdditionalAssemblies { get; set; }
	public Func<TypeEx, string, IDataAccessor> DataAccessorProvider { get; set; }

	public DynamicParameter(TypeEx entityType, string expression) : base(entityType, expression)
	{
	}

	public DynamicParameter(object constValue) : base(constValue)
	{
	}

	protected bool CheckAccess(BaseMember mi)
		=> AccessAttributes == null || AccessAttributes.Any(type => mi.FindAttributes(type).Any());

	protected IDataAccessor ParseMemberOrType(HashSet<Assembly> assemblies, TypeEx targetType, StringParser parser, bool isInArgs)
	{
		parser.SkipSpaces();
		string token;
		IDataAccessor res;

		if (parser.Is('"'))
		{
			var cnst = parser.SearchFor("\"", "\\\"");
			return ParseNext(assemblies, cnst, targetType, parser, new ConstDataAccessor(cnst), isInArgs);
		}

		if (parser.Is('\''))
		{
			var cnst = parser.SearchFor("'", "\\'");
			return ParseNext(assemblies, cnst, targetType, parser, new ConstDataAccessor(cnst), isInArgs);
		}

		if (char.IsDigit(parser.Cursor))
		{
			var ch = parser.Cursor;
			var cnst = new StringBuilder();
			do
			{
				cnst.Append(ch);
				parser.Inc();
				ch = parser.Cursor;
			} while (char.IsDigit(ch) || ch == '.');

			var str = cnst.ToString();

			if (long.TryParse(str, out var l))
				res = new ConstDataAccessor(l);
			else
			{
				var d = StringConverter.Default.ToObject<double>(str);
				res = new ConstDataAccessor(d);
			}
			return ParseNext(assemblies, str, targetType, parser, res, isInArgs);
		}

		if (!parser.Is("::"))
		{
			token = parser.GetToken();
			parser.SkipSpaces();

			res = DataAccessorProvider?.Invoke(targetType, token);
			if (res != null)
				return ParseNext(assemblies, token, targetType, parser, res, isInArgs);

			switch (IgnoreCase ? token.ToLower() : token)
			{
			case "this":
				return ParseNext(assemblies, token, targetType, parser, new ThisDataAccessor(targetType), isInArgs);
			case "true":
				return ParseNext(assemblies, token, targetType, parser, new ConstDataAccessor(true), isInArgs);
			case "false":
				return ParseNext(assemblies, token, targetType, parser, new ConstDataAccessor(false), isInArgs);
			}

			res = ParseMemberOrExtension(assemblies, token, targetType, new ThisDataAccessor(targetType), parser, true, isInArgs);
			if (res != null)
				return res;
		}
		else
		{
			token = parser.GetToken();
			parser.SkipSpaces();
		}

		var typeName = new EnumBuilder(".");

		while (parser.Is('.'))
		{
			typeName.Append(token);
			parser.SkipSpaces();
			token = parser.GetToken();

			foreach (var assy in assemblies)
			{
				var type = assy.FindType(typeName, IgnoreCase);
				if (type != null)
				{
					res = ParseMemberOrExtension(assemblies, token, targetType, new StaticTypeAccessor(type), parser, true, isInArgs);
					if (res != null)
						return res;
				}

				var nsSegments = (targetType?.Namespace ?? "").SplitNonEmpty('.');
				for (var i = nsSegments.Length; i > 0; i--)
				{
					var tp = string.Join(".", nsSegments.Take(i).AppendAfter(typeName));
					type = assy.FindType(tp, IgnoreCase);
					if (type != null)
					{
						res = ParseMemberOrExtension(assemblies, token, targetType, new StaticTypeAccessor(type), parser, true, isInArgs);
						if (res != null)
							return res;
					}
				}
			}
			parser.SkipSpaces();
		}
		throw new ParsingException($"Unrecognized {typeName}");
	}

	protected IDataAccessor ParseMemberOrExtension(HashSet<Assembly> assemblies, string token, TypeEx targetType, IDataAccessor da, StringParser parser, bool @static, bool isInArgs)
	{
		parser.SkipSpaces();

		if (parser.Is('('))
		{
			parser.SkipSpaces();


			var methods = da.ValueType.FindMethods(new ReflectionFilter(token)
			{
				IsStatic = @static ? null : false,
				IgnoreCase = IgnoreCase
			}).Concat(assemblies.SelectMany(a => a.GetTypes()).Select(TypeEx.Get)
				.Where(type => type.IsSealed && !type.IsGenericType && !type.IsNested)
				.SelectMany(type => type.FindMethods(new ReflectionFilter(token)
				{
					IgnoreCase = IgnoreCase
				})).Where(m => string.Equals(m.Name, token, IgnoreCase ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal) &&
				               m.HasAttribute<ExtensionAttribute>(false) &&
				               m.Parameters[0].ParameterType.IsAssignableFrom(da.ValueType)));
				
			var arguments = new List<IDataAccessor>();
			List<IDataAccessor> extArguments = null;

			while (!parser.Is(')'))
				arguments.Add(ParseMemberOrType(assemblies, targetType, parser, true));

			foreach (var mtd in methods)
			{
				if (!CheckAccess(mtd))
					continue;

				var args = arguments;
				if (mtd.HasAttribute<ExtensionAttribute>(false))
				{
					if (extArguments == null)
					{
						extArguments = new List<IDataAccessor> {da};
						extArguments.AddRange(arguments);
					}
					args = extArguments;
				}
				var param = mtd.Parameters;
				var cnt = param.TakeWhile(p => !p.IsOptional).Count();

				if (cnt > args.Count || args.Count > param.Length)
					continue;

				var valid = true;

				var i = 0;
				for (; i < Math.Min(param.Length, args.Count); i++)
					if (!args[i].ValueType.CanCastTo(param[i].ParameterType))
					{
						valid = false;
						break;
					}

				if (!valid)
					continue;

				var defaults = param.Select(p => p.IsOptional ? p.DefaultValue : TypeEx.Get(p.ParameterType).DefaultValue).ToArray();

				var res = new LambdaDataAccessor(mtd.ReturnType, o =>
				{
					var values = new object[param.Length];
					for (var k = 0; k < param.Length; k++)
					{
						if (k < args.Count)
						{
							var val = args[k].GetValue(o);
							if (args[k].ValueType.TryCast(val,param[k].ParameterType,  out var r))
							{
								values[k] = r;
								continue;
							}
						}
						values[k] = defaults[k];
					}
					return mtd.Invoke(o, values); //TODO: использовать mtd.CastInvoke
				}, null);
				return ParseNext(assemblies, token, targetType, parser, res, isInArgs);
			}
			throw new ParsingException($"Method {da.ValueType.Name}.{token} not found");
		}

		if (@static)
		{
			var sub = assemblies.Select(a => a.GetType($"{da.ValueType.FullName}+{token}", false, IgnoreCase)).FirstOrDefault(t => t != null);
			if (sub != null && parser.Is('.'))
			{
				parser.SkipSpaces();
				token = parser.GetToken();
				var res = ParseMemberOrExtension(assemblies, token, targetType, new StaticTypeAccessor(sub), parser, true, false);
				if (res != null)
					return res;

				throw new ParsingException($"Expected property or method: {token}{parser}");
			}
		}

		if (da is StaticTypeAccessor && da.ValueType.IsEnum)
		{
			var val = Enum.Parse(da.ValueType, token, IgnoreCase);
			return ParseNext(assemblies, token, targetType, parser, new ConstDataAccessor(val), isInArgs);
		}

		var mem = da.ValueType.FindDataMember(new ReflectionFilter(token) {IsStatic = @static ? null : false});
		if (mem != null && CheckAccess(mem))
			return ParseNext(assemblies, token, targetType, parser, mem, isInArgs);
			
		return null;
	}

	IDataAccessor ParseNext(HashSet<Assembly> assemblies, string token, TypeEx targetType, StringParser parser, IDataAccessor da, bool isInArgs)
	{
		if (isInArgs)
		{
			if (parser.IsEol)
				throw new ParsingException("Expected ')'");

			if (parser.Is(',') || parser.Is(')', false))
				return da;
		}

		if (parser.IsEol)
			return da;

		if (parser.Is('.'))
		{
			token = parser.GetToken();
			var res2 = ParseMemberOrExtension(assemblies, token, targetType, da, parser, false, isInArgs);
			if (res2 != null)
				return new LambdaDataAccessor(res2.ValueType, o =>
				{
					var obj = da.GetValue(o);
					return obj.With(_ => res2.GetValue(obj));
				}, null);
		}
		throw new ParsingException($"Expected property or method: {token}{parser}");
	}

	protected override IDataAccessor CreateDataAccessor(ParsingContext context, string expression)
	{
		var i = expression.LastIndexOf(":");
		if (i > 0)
		{
			var nm = expression.Substring(i + 1);
			var fmt = Formatters.SafeGet(nm) ?? (o => StringConverter.Default.ToString(o, nm));
			var da = GetOrAddDataAccessor(context, expression.Substring(0, i));
			return new LambdaDataAccessor(typeof (string), o => fmt(da.GetValue(o)), null, expression);
		}

		var parser = new StringParser(expression);

		var assemblies = new HashSet<Assembly>();
		var entry = Assembly.GetEntryAssembly();
		if (entry != null) assemblies.Add(entry);
		if (context.TargetType != null) assemblies.Add(context.TargetType.Assembly);
		if (AdditionalAssemblies != null) assemblies.AddRange(AdditionalAssemblies);

		return ParseMemberOrType(assemblies, context.TargetType, parser, false);
	}
}