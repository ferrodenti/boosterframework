#if !NETSTANDARD
using Booster;

// ReSharper disable once CheckNamespace
namespace System.Diagnostics.CodeAnalysis;

public class NotNullIfNotNullAttribute : Attribute
{
	public NotNullIfNotNullAttribute(string name)
		=> Utils.Nop(name);
}

public class NotNullWhenAttribute : Attribute
{
	public NotNullWhenAttribute(bool value)
		=> Utils.Nop(value);
}

public class NotNullAttribute : Attribute
{
	public NotNullAttribute()
	{
	}
}
#endif