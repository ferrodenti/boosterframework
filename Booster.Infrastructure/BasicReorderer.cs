using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Booster.AsyncLinq;
using Booster.Interfaces;

namespace Booster;

public class BasicReorderer : IReorderer
{
	public int Step { get; set; } = 10;

	protected virtual void DoReorder(Action act)
		=> act();

	protected virtual void ChangeOrder(IReorderable target, int newOrder)
		=> target.Order = newOrder;

	public void ReorderAfter(IEnumerable collection, IReorderable target, IReorderable after)
	{
		var toReorder = new List<IReorderable>
		{
			target
		};
		var minOrder = after.With(a => a.Order);

		foreach (IReorderable r in collection)
		{
			var step = ((double) r.Order - minOrder)/(toReorder.Count + 1);

			if (step < 1.0)
				toReorder.Add(r);
			else
			{
				double reorderStart = minOrder;
				var orCheck = minOrder;

				DoReorder(() =>
				{
					foreach (var e in toReorder)
					{
						reorderStart += step;
						var newOrder = (int) Math.Floor(reorderStart);

						if (orCheck >= newOrder || newOrder >= r.Order)
							throw new Exception();

						orCheck = newOrder;

						if (e.Order != newOrder)
							ChangeOrder(e, newOrder);
					}
				});

				return;
			}
		}
		{
			var newOrder = minOrder;

			DoReorder(() =>
			{
				foreach (var e in toReorder)
				{
					newOrder += Step;
					if (e.Order != newOrder)
						ChangeOrder(e, newOrder);
				}
			});
		}
	}
		
	protected virtual Task DoReorderAsync(Func<Task> act)
		=> act();

	protected virtual Task ChangeOrderAsync(IReorderable target, int newOrder)
	{
		target.Order = newOrder;
		return Task.CompletedTask;
	}

	public async Task ReorderAfter(IAsyncEnumerable<IReorderable> collection, IReorderable target, IReorderable after)
	{
		var toReorder = new List<IReorderable>
		{
			target
		};
		int newOrder, minOrder = after.With(a => a.Order);
			
#if NETSTANDARD2_1
			await foreach (var r in collection.NoCtx())
			{
				var step = ((double) r.Order - minOrder) / (toReorder.Count + 1);

				if (step < 1.0)
					toReorder.Add(r);
				else
				{
					double reorderStart = minOrder;
					var orCheck = minOrder;

					await DoReorderAsync(async () =>
					{
						foreach (var e in toReorder)
						{
							reorderStart += step;
							newOrder = (int) Math.Floor(reorderStart);

							if (orCheck >= newOrder || newOrder >= r.Order)
								throw new Exception();

							orCheck = newOrder;

							if (e.Order != newOrder)
								await ChangeOrderAsync(e, newOrder).NoCtx();
						}
					}).NoCtx();
					return;
				}
			}
#else
		var ret = false;
		await collection.ForEach(async r =>
		{
			var step = ((double) r.Order - minOrder) / (toReorder.Count + 1);

			if (step < 1.0)
				toReorder.Add(r);
			else
			{
				double reorderStart = minOrder;
				var orCheck = minOrder;

				await DoReorderAsync(async () =>
				{
					foreach (var e in toReorder)
					{
						reorderStart += step;
						newOrder = (int) Math.Floor(reorderStart);

						if (orCheck >= newOrder || newOrder >= r.Order)
							throw new Exception();

						orCheck = newOrder;

						if (e.Order != newOrder)
							await ChangeOrderAsync(e, newOrder).NoCtx();
					}
				}).NoCtx();

				ret = true;
				return false;
			}

			return true;
		}).NoCtx();

		if (ret)
			return;
#endif
		newOrder = minOrder;
		await DoReorderAsync(async () =>
		{
			foreach (var e in toReorder)
			{
				newOrder += Step;
				if (e.Order != newOrder)
					await ChangeOrderAsync(e, newOrder).NoCtx();
			}
		}).NoCtx();
	}
}