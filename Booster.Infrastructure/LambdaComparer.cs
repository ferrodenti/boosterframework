using System;
using System.Collections;
using System.Collections.Generic;

namespace Booster;

public class LambdaComparer<T> : IComparer<T>, IEqualityComparer<T>, IEqualityComparer
{
	readonly Func<T, object>[] _getters;

	public LambdaComparer(params Func<T, object>[] getters)
		=> _getters = getters;

	public int Compare(T x, T y)
	{
		if (object.Equals(x, default(T)))
			return object.Equals(y, default(T)) ? 0 : -1;

		if (object.Equals(y, default(T)))
			return 1;

		foreach (var getter in _getters)
		{
			var a = getter(x);
			var b = getter(y);

			if (a == null)
			{
				if (b == null)
					continue;

				return -1;
			}
			if (b == null)
				return 1;

			if (a is IComparable cmp)
			{
				var r = cmp.CompareTo(b);
				if (r == 0)
					continue;

				return r;
			}

			var q = string.Compare(a.ToString(), b.ToString(), StringComparison.Ordinal);
			if (q == 0)
				continue;

			return q;
		}
		return 0;
	}

	public bool Equals(T x, T y)
		=> Compare(x, y) == 0;

	public int GetHashCode(T obj)
	{
		if (Utils.IsNull(obj))
			return 0;

		var res = new HashCode();

		foreach (var getter in _getters)
			res.Append(getter.Invoke(obj));

		return res;
	}

	public new bool Equals(object x, object y)
		=> x is T variable && y is T variable1 && Equals(variable, variable1);

	public int GetHashCode(object obj)
		=> obj.GetHashCode();
}