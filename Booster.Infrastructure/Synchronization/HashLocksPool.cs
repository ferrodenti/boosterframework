using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Synchronization;

[PublicAPI]
public class HashLocksPool
{
	readonly AsyncLock?[] _locks;
	readonly int _count;
	readonly int _seed;
	readonly AsyncLock _lock = new();

	public HashLocksPool(int count, int seed = 0)
	{
		_count = count;
		_seed = seed;
		_locks = new AsyncLock[count];
	}

	public AsyncLock Get(object obj)
	{
		var hash = Math.Abs(obj.GetHashCode() + _seed) % _count;

		// ReSharper disable once InconsistentlySynchronizedField
		var res = _locks[hash];
		if (res == null)
			using (_lock.Lock())
				if ((res = _locks[hash]) == null)
					_locks[hash] = res = new AsyncLock();

		return res;
	}
	
	public async Task<AsyncLock> GetAsync(object obj, CancellationToken token = default)
	{
		var hash = Math.Abs(obj.GetHashCode() + _seed) % _count;

		// ReSharper disable once InconsistentlySynchronizedField
		var res = _locks[hash];
		if (res == null)
		{
			using var _ = await _lock.LockAsync(token).NoCtx();

			if ((res = _locks[hash]) == null)
				_locks[hash] = res = new AsyncLock();
		}

		return res;
	}

	public IDisposable Lock(object obj)
		=> Get(obj).Lock();

	public Task<IDisposable> LockAsync(object obj)
		=> Get(obj).LockAsync();

	public IDisposable? Lock(object obj, int timeout, CancellationToken token = default)
		=> Get(obj).Lock(timeout, token);

	public Task<IDisposable?> LockAsync(object obj, int timeout, CancellationToken token = default)
		=> Get(obj).LockAsync(timeout, token);

	public ActionDisposable? LockAll(CancellationToken token = default)
	{
		var masterKey = _lock.Lock(token);
		var keys = new List<IDisposable>(_locks.Length);
		
		foreach (var lck in _locks)
		{
			var key = lck?.Lock(token);
			if (key != null)
				keys.Add(key);
		}

		var result = new ActionDisposable(() =>
		{
			foreach (var key in keys)
				key.Dispose();
			
			masterKey?.Dispose();
		});

		if (token.IsCancellationRequested)
		{
			result.Dispose();
			return null;
		}

		return result;
	}
	
	public async Task<IDisposable?> LockAllAsync(CancellationToken token = default)
	{
		var masterKey = await _lock.LockAsync(token).NoCtx();
		var keys = new List<IDisposable>(_locks.Length);
		
		foreach (var lck in _locks)
			if (lck != null)
			{
				var key = await lck.LockAsync(token).NoCtx();
				if (key != null)
					keys.Add(key);
			}

		var result = new ActionDisposable(() =>
		{
			foreach (var key in keys)
				key.Dispose();
			
			masterKey?.Dispose();
		});

		if (token.IsCancellationRequested)
		{
			result.Dispose();
			return null;
		}

		return result;
	}
}