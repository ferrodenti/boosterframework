using System.Threading;
using Booster.Interfaces;
using JetBrains.Annotations;

namespace Booster.Synchronization;

[PublicAPI]
public abstract class BaseStartStop : IStartStop
{
	readonly object _lock = new();

	CancellationTokenSource _cancellationTokenSource;

	CancellationToken? _cancellationToken;
	protected CancellationToken CancellationToken => _cancellationToken ??= _cancellationTokenSource?.Token ?? CancellationToken.None;

	protected BaseStartStop(bool isWorking = true)
		=> IsWorking = isWorking;

	volatile bool _isWorking;
	public bool IsWorking
	{
		get => _isWorking;
		set
		{
			if (value)
				Start();
			else
				Stop();
		}
	}

	public virtual bool Start()
	{
		if(!_isWorking)
			lock(_lock)
				if (!_isWorking)
				{
					_cancellationTokenSource = new CancellationTokenSource();
					_cancellationToken = null;
					OnStart();
					_isWorking = true;
					return true;
				}

		return false;
	}

	public virtual bool Stop()
	{
		if(_isWorking)
			lock(_lock)
				if (_isWorking)
				{
					OnStop();
					_cancellationTokenSource?.Cancel();
					_isWorking = false;
					return true;
				}

		return false;
	}
	protected virtual void OnStart() { }

	protected virtual void OnStop() { }

	public virtual void Dispose()
		=> Stop();
}