using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Booster.Log;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Synchronization;

[PublicAPI]
public abstract class ProcessingQueue<T>
{
	readonly ConcurrentQueue<T> _queue = new();
	readonly object _lock = new();

	volatile Task? _sendTask;
	protected ILog? Log;

	protected ProcessingQueue(ILog? log)
		=> Log = log.Create(this);

	public void Enqueue(T item)
	{
		lock (_lock)
		{
			_queue.Enqueue(item);
			// ReSharper disable once NonAtomicCompoundOperator
			_sendTask ??= Utils.StartTaskNoFlow(ProccessProc, null);
		}
	}

	async Task ProccessProc()
	{
		try
		{
			using var context = await CreateContext();
			while (true)
			{
				T item;

				lock (_lock)
					if (!_queue.TryDequeue(out item))
					{
						_sendTask = null;
						return;
					}

				try
				{
					await Proccess(item, context).NoCtx();
				}
				catch (Exception e)
				{
					Log.Error(e);
				}
			}
		}
		catch (Exception e)
		{
			Log.Error(e);
		}
	}

	protected virtual Task<IDisposable?> CreateContext()
		=> Task.FromResult<IDisposable?>(null);

	protected abstract Task Proccess(T item, IDisposable? context);
}