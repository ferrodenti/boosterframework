using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Booster.Collections;
using Booster.DI;
using Booster.Interfaces;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Synchronization;

[PublicAPI]
public class AsyncLock
{
	const int _infiniteWaitCheckInterval = 100;
	readonly SemaphoreSlim _semaphore = new(0, 1);
	readonly CtxLocal<object> _local = new();

	readonly object _lock = new();
	volatile int _recursion;
	object? _owner;

	public int RetryInterval = 100;

	bool TryEnter()
	{
		var local = _local.Value ??= new object();
			
		lock (_lock)
			if (_owner == null || _owner == local)
			{
				_owner = local;
				Interlocked.Increment(ref _recursion);
				return true;
			}

		return false;
	}

	public void Release()
	{
		lock (_lock)
			if (Interlocked.Decrement(ref _recursion) == 0)
			{
				_owner = null;
				_local.Value = null;

				if (_semaphore.CurrentCount == 0)
					_semaphore.Release();
			}
	}

	public IDisposable Lock()
	{
		while (!TryEnter()) 
			_semaphore.Wait();

		return new ActionDisposable(Release);
	}

	public IDisposable? Lock(CancellationToken token)
	{
		while (!TryEnter())
		{
			_semaphore.Wait(token);

			if (token.IsCancellationRequested)
				return null;
		}

		return new ActionDisposable(Release);
	}

	public IDisposable? Lock(int timeout, CancellationToken token = default)
	{
		while (!TryEnter())
		{
			var delta = Math.Min(timeout, RetryInterval);
			timeout -= delta;
			var success = _semaphore.Wait(delta, token);

			if (!success && (token.IsCancellationRequested || timeout <= 0))
				return null;
		}

		return new ActionDisposable(Release);
	}

	//Очень странный глюк что CtxLocal не хранит значение при наличии async
	/*public async Task<IDisposable> LockAsync(int timeout = -1, CancellationToken token = default)
	{
		while (!TryEnter())
			await _semaphore.WaitAsync(timeout, token).NoCtx();
		
		return new ActionDisposable(Release);
	}*/

	async Task<IDisposable?> WaitAndEnter(int timeout, CancellationToken token)
	{
		if (timeout < 0)
			return await WaitAndEnter(token).NoCtx();

		do
		{
			var delta = Math.Min(timeout, RetryInterval);
			timeout -= delta;

			var success = await _semaphore.WaitAsync(delta, token).NoCtx();
			if (!success && (token.IsCancellationRequested || timeout <= 0))
				return null;

		} while (!TryEnter());

		return new ActionDisposable(Release);
	}

	async Task<IDisposable?> WaitAndEnter(CancellationToken token)
	{
		if (!token.CanBeCanceled)
			// ReSharper disable once MethodSupportsCancellation
			return await WaitAndEnter().NoCtx();

		do
		{
			var success = await _semaphore.WaitAsync(_infiniteWaitCheckInterval, token).NoCtx();
			if (!success && token.IsCancellationRequested)
				return null;

		} while (!TryEnter());

		return new ActionDisposable(Release);
	}

	async Task<IDisposable> WaitAndEnter()
	{
		do
			await _semaphore.WaitAsync(_infiniteWaitCheckInterval).NoCtx();
		while (!TryEnter());

		return new ActionDisposable(Release);
	}

	public async Task<IDisposable> LockAsync()
		=> TryEnter()
			? new ActionDisposable(Release)
			: await WaitAndEnter().NoCtx();

	public async Task<IDisposable?> LockAsync(int timeout, CancellationToken token = default)
		=> TryEnter()
			? new ActionDisposable(Release)
			: await WaitAndEnter(timeout, token).NoCtx();

	public async Task<IDisposable?> LockAsync(CancellationToken token)
		=> TryEnter()
			? new ActionDisposable(Release)
			: await WaitAndEnter(token).NoCtx();

	public async Task<bool> Enter(CancellationToken token = default, int timeout = -1)
		=> TryEnter() || await WaitAndEnter(timeout, token) != null;


	public T Evaluate<T>(Func<T> evaluator)
	{
		using (Lock())
			return evaluator();
	}

	public async Task<T> EvaluateAsync<T>(Func<T> evaluator)
	{
		using (await LockAsync().NoCtx())
			return evaluator();
	}

	public async Task<T> EvaluateAsync<T>(Func<Task<T>> evaluator)
	{
		using (await LockAsync().NoCtx())
			return await evaluator().NoCtx();
	}

	static readonly Lazy<CacheDictionary<WeakReference, AsyncLock>> _locksForObjects = new(() =>
	{
		var result = new CacheDictionary<WeakReference, AsyncLock>();

		TimeSchedule.GC.Subscribe(typeof(AsyncLock), TimeSpan.FromMinutes(13), () =>
		{
			foreach(var reff in result.Keys.ToArray())
				if (!reff.IsAlive)
					result.Remove(reff);
		});
			
		return result;
	});

	public static AsyncLock GetFor(object obj)
	{
		if (obj is IAsyncLockable lockable)
			return lockable.AsyncLock;

		lock (_locksForObjects)
			return _locksForObjects.Value.GetOrAdd(new WeakReference(obj), _ => new AsyncLock());
	}

	public static  IDisposable For(object obj)
		=> GetFor(obj).Lock();

	public static Task<IDisposable> ForAsync(object obj)
		=> GetFor(obj).LockAsync();

	public static IDisposable? For(object obj, int timeout, CancellationToken token = default) 
		=> GetFor(obj).Lock(timeout, token);

	public static Task<IDisposable?> ForAsync(object obj, int timeout, CancellationToken token = default)
		=> GetFor(obj).LockAsync(timeout, token);
}