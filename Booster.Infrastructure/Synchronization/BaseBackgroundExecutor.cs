using System;
using System.Threading;
using System.Threading.Tasks;
using Booster.Debug;
using Booster.Log;

namespace Booster.Synchronization;

public abstract class BaseBackgroundExecutor : BaseStartStop
{
	volatile Task _task;
	protected readonly object TaskLock = new();

	protected readonly ILog Log;

	protected bool ContinueOnError;

	protected BaseBackgroundExecutor(ILog log)
		=> Log = log;

	protected virtual bool EnsureTaskRunning()
	{
		lock(TaskLock)
			if (_task == null && IsWorking)
			{
				_task = Utils.StartTaskNoFlow(Proc);
				return false;
			}

		return true;
	}

	int _lock;

	protected virtual async Task Proc()
	{
		if (Interlocked.Increment(ref _lock) != 1)
			return;

		var taskId = Task.CurrentId ?? -1;

		Log.Trace($"Started, id: {taskId}");

		var done = false;
		while (!done)
		{
			var hasMore = false;
			try
			{
				hasMore = await ProccessOne().NoCtx();
			}
			catch (Exception e)
			{
				Log.Error(e, TaskInfo.TaskName);
					
				if (ContinueOnError)
					hasMore = true;
			}
			finally
			{
				if (!hasMore)
					lock (TaskLock)
						if (!IsWorking || CheckDone())
						{
							Log.Trace($"Finished, id: {taskId}");
							Interlocked.Exchange(ref _lock, 0);
							// ReSharper disable once InconsistentlySynchronizedField
							_task = null;
							done = true;
						}
			}
		}
	}

	protected virtual bool CheckDone() 
		=> true;

	protected override void OnStart() 
		=> EnsureTaskRunning();

	protected abstract Task<bool> ProccessOne();
}