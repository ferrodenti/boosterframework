using System;
using Booster.Interfaces;
using Booster.Reflection;

#nullable enable

namespace Booster;

public class LambdaDataAccessor : IDataAccessor
{
	readonly Func<object?, object?>? _getter;
	readonly Func<object?, object?, bool>? _setter;

	public LambdaDataAccessor(
		TypeEx valueType,
		Func<object?, object?>? getter,
		Func<object?, object?, bool>? setter,
		string? name = null)
	{
		_getter = getter;
		_setter = setter;
		ValueType = valueType;
		Name = name;
	}

	public string? Name { get; }
	public TypeEx ValueType { get; }
	public bool CanRead => _getter != null;
	public bool CanWrite => _setter != null;

	public object? GetValue(object? obj)
		=> _getter?.Invoke(obj);

	public bool SetValue(object? obj, object? value)
		=> _setter != null && _setter(obj, value);
}