﻿#if NETCOREAPP || NETSTANDARD
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Booster.Helpers;
using Booster.Mvc.Helpers;
using Booster.Mvc.TagHelpers.Kitchen;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Newtonsoft.Json;

// ReSharper disable once CheckNamespace
namespace Booster.Mvc.TagHelpers;

public class BoosterGridTagHelper : TagHelper //TODO: почему-то не удается перенести в Booster.Mvc.Grid
{
	[ViewContext]
	public ViewContext ViewContext { get; set; }
	
	public bool IsAjax { get; set; }

	public string JsRoot { get; set; } = "~/js";
	public string CssRoot { get; set; } = "~/css";
	public string NewLine { get; set; } = "\n";
	public string Tab { get; set; } = "\t";
	public string Id { get; set; }
	public string Filter { get; set; }
	public string ApplyFilterControl { get; set; }
	public string Controller { get; set; }
	//public string ReloadButton { get; set; }
	public object Model { get; set; }
	public bool? UpdatePageTitle { get; set; }
	public bool? UpdateCaption { get; set; }
	//public bool? ManualReload { get; set; }			 
	public bool? UseFileUpload { get; set; }
	public bool UseContentVersions { get; set; }

	/// <summary lang="en-US">A content versions' seed used to initialize the hashing algorithm</summary>
	/// <summary lang="ru-RU">Общий сид версий контента используемый для инициализации алгоритма хеширования</summary>
	public int ContentVersionSeed { get; set; }

	public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
	{
		var model = Model ?? ViewContext.ViewData.Model;
		if (model == null)
			throw new ArgumentException("A model must be provided either throu View.Model or BoosterGridTagHelper.Model property");

		string controller, id = null, pageTitle = null; 
		//string filter = null, reloadButton = null
		var updatePageTitle = true;
		//bool manualReload = false;
		var useFileUpload = false;

		var jsInitDict = new Dictionary<string, object>();
		
		switch (model)
		{
		case string smodel:
			controller = smodel;
			jsInitDict["controller"] = controller;
			break;
		case Type tmodel:
			controller = Grid.Grid.KeyOf(tmodel);
			jsInitDict["controller"] = controller;
			break;
		default:
		{
			var anon = new AnonTypeHelper(model);
			id = anon.Get<string>("id");
			controller = anon.Get<string>("controller");
			//filter = anon.Get<string>("filterForm");
			//manualReload = anon.Get<bool>("manualReload");
			useFileUpload = anon.Get<bool>("useFileUpload");
			updatePageTitle = anon.Get<bool>("updatePageTitle");
			pageTitle = anon.Get<string>("updatePageTitle");
			//reloadButton = anon.Get<string>("reloadButton");

			jsInitDict.AddRange(anon.Properties.Select(p => new KeyValuePair<string, object>(p.Key, p.Value)));
			break;
		}
		}

		if (Id != null) id = Id;
		if (Filter != null) jsInitDict["filterForm"] = Filter;
		if (Controller != null) {controller = Controller; jsInitDict["controller"] = controller;}
		if (UpdatePageTitle.HasValue) {updatePageTitle = UpdatePageTitle.Value; jsInitDict["updatePageTitle"] = updatePageTitle;}
		//if (ManualReload.HasValue) {manualReload = ManualReload.Value; jsInitDict["manualReload"] = manualReload;}
		if (UseFileUpload.HasValue) {useFileUpload = UseFileUpload.Value; jsInitDict["useFileUpload"] = useFileUpload;}
		if (UpdateCaption.HasValue) jsInitDict["updateCaption"] = UpdateCaption.Value;
		//if (ReloadButton != null) reloadButton = ReloadButton;
		if (ApplyFilterControl != null) jsInitDict["applyFilterControl"] = ApplyFilterControl;
		
		id ??= "gridEditor";
		if (controller == null)
			throw new ArgumentException("A controller name must be provided either throu View.Model or BoosterGridTagHelper.Controller property");
					
		if (updatePageTitle && pageTitle != null)
			ViewContext.ViewBag.Title = pageTitle;

		var html = new EnumBuilder(NewLine);
		var js = new EnumBuilder(NewLine);
		
		html.Append($"<div id=\"{id}\"></div>");
		
		js.Append(@"<script type=""text/javascript"">");
		js.Append($@"{Tab}$(function () {{");
		js.Append($@"{Tab}{Tab}var grid = $(""#{id}"").boosterGrid({JsonConvert.SerializeObject(jsInitDict)});");
		
//			if (filter != null && !manualReload)
//				js.Append($@"{Tab}{Tab}$(""{filter}"").on(""change"", ""[name]"", function () {{ grid.boosterGrid(""reloadGrid"", true); }});");
//			
//			if(reloadButton != null)
//				js.Append($@"{Tab}{Tab}$(""{reloadButton}"").click(function () {{ grid.boosterGrid(""reloadGrid"", false); }});");
			
		
		js.Append($@"{Tab}}});");
		js.Append(@"</script>");
		
		if (!IsAjax)
		{
			var url = MvcHelper.CreateUrlHelper();

			Task<string> AddVer(string path)
				=> HashedContentFiles.GetUrl(url.Content(path), UseContentVersions, ContentVersionSeed);
			
			var cssDict = new CssTagHelper().With(h => h.Includes ?? (h.Includes = new IncludesDictionary())); 
			var jsDict = new JsTagHelper().With(h => h.Includes ?? (h.Includes = new IncludesDictionary())); 
			var viewName = ViewContext.ExecutingFilePath;
			
			cssDict.Add($"<link href=\"{await AddVer($"{CssRoot}/booster-grid.css").NoCtx()}\" type=\"text/css\" rel=\"stylesheet\" />", viewName);
			cssDict.Add($"<link href=\"{await AddVer($"{CssRoot}/bootstrap-select.min.css").NoCtx()}\" type=\"text/css\" rel=\"stylesheet\" />", viewName);
			cssDict.Add($"<link href=\"{await AddVer($"{CssRoot}/intpicker.css").NoCtx()}\" type=\"text/css\" rel=\"stylesheet\" />", viewName);

			jsDict.Add($"<script type=\"text/javascript\" Src=\"{await AddVer($"{JsRoot}/booster-url.js").NoCtx()}\"></script>", viewName);
			jsDict.Add($"<script type=\"text/javascript\" Src=\"{await AddVer($"{JsRoot}/booster-paginator.js").NoCtx()}\"></script>", viewName);
			jsDict.Add($"<script type=\"text/javascript\" Src=\"{await AddVer($"{JsRoot}/booster-editor.js").NoCtx()}\"></script>", viewName);
			jsDict.Add($"<script type=\"text/javascript\" Src=\"{await AddVer($"{JsRoot}/booster-grid.js").NoCtx()}\"></script>", viewName);
			jsDict.Add($"<script type=\"text/javascript\" Src=\"{await AddVer($"{JsRoot}/bootstrap-datepicker.js").NoCtx()}\"></script>", viewName);
			jsDict.Add($"<script type=\"text/javascript\" Src=\"{await AddVer($"{JsRoot}/bootstrap-datepicker.ru.js").NoCtx()}\"></script>", viewName);
			jsDict.Add($"<script type=\"text/javascript\" Src=\"{await AddVer($"{JsRoot}/intPicker.js").NoCtx()}\"></script>", viewName);
			jsDict.Add($"<script type=\"text/javascript\" Src=\"{await AddVer($"{JsRoot}/bootstrap-select.min.js").NoCtx()}\"></script>", viewName);
			//TODO: не добавляет версии скриптов
			if (useFileUpload) //TODO: возможно, тут нужен полее детальный резолв положения скриптов
			{
				cssDict.Add($"<link href=\"{await AddVer($"{CssRoot}/file-upload.css").NoCtx()}\" type=\"text/css\" rel=\"stylesheet\" />", viewName);

				jsDict.Add($"<script type=\"text/javascript\" Src=\"{await AddVer($"{JsRoot}/fileUpload/jquery.ui.widget.js").NoCtx()}\"></script>", viewName);
				jsDict.Add($"<script type=\"text/javascript\" Src=\"{await AddVer($"{JsRoot}/fileUpload/jquery.iframe-transport.js").NoCtx()}\"></script>", viewName);
				jsDict.Add($"<script type=\"text/javascript\" Src=\"{await AddVer($"{JsRoot}/fileUpload/jquery.fileupload.js").NoCtx()}\"></script>", viewName);
			}
			
			jsDict.Add(js, viewName);
		}
		else
			html.Append(js);
		
		output.Content.SetHtmlContent(html);
		output.TagName = "";
	}
}

#endif