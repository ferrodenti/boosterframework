﻿using Booster.Mvc.Editor.Interfaces;
using Newtonsoft.Json;

namespace Booster.Mvc.Grid.Interfaces;

public interface IGridDataSchema : IEditorDataSchema
{
	[JsonProperty("sortExpression", NullValueHandling=NullValueHandling.Ignore)]
	string SortExpression { get; }

	[JsonProperty("sortable")]
	bool Sortable { get; }
}