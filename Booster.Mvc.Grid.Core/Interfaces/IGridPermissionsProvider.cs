using System.Threading.Tasks;
using Booster.Mvc.Editor.Interfaces;
using Booster.Mvc.Grid.Orm;

namespace Booster.Mvc.Grid.Interfaces;

public interface IGridPermissionsProvider
{
	Task<GridPermissions> GetPermissions(IGridController controller, object entity, GridPermissions permissions = GridPermissions.All);
	Task<GridPermissions> GetPermissions(IGridController controller, IEditorDataSchema column, GridPermissions permissions = GridPermissions.All);
}