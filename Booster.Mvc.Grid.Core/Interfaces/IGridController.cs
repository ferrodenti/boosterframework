using System;
using System.Threading.Tasks;
using Booster.Mvc.Grid.Kitchen;
using Booster.Reflection;

#if !NETCOREAPP
using Booster.AsyncLinq;
#else
using System.Collections.Generic;
#endif

#if NETSTANDARD

#elif !NETCOREAPP
using System.Web;
#endif

namespace Booster.Mvc.Grid.Interfaces
{
	public interface IGridController
	{
		string Key { get; }
		string Name { get; } //TODO: remove ?
		string Caption { get; }
		TypeEx EntityType { get; }
		TypeEx DecoratorType { get; }
		
		IGridPermissionsProvider GridPermissionsProvider { get; set; }

		Task<object> Select(GridRequest request);
		Task<object> Delete(GridRequest request);
		Task<object> GetNew(GridRequest request);
		Task<object> GetEdit(GridRequest request);
		Task<object> Save(GridRequest request);
		Task<object> Reorder(GridRequest request);
		Task<object> UserSelect(GridRequest request);
		Task<object> Sync(GridRequest request);

#if !NETSTANDARD && !NETCOREAPP
		Task<object> FileDownload(GridRequest request);
		Task<object> FileUpload(GridRequest request, HttpPostedFile postedFile);
#endif
		IGridSelectionState GetSelectionState(string sesKey = null);
		IAsyncEnumerable<object> GetUserSelection(string sesKey = null);

		void OnError(string action, GridRequest request, Exception ex);
	}
}