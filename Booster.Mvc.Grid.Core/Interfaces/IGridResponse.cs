﻿using System;
using System.Collections.Generic;
using Booster.Interfaces;
using Newtonsoft.Json;

namespace Booster.Mvc.Grid.Interfaces;

public interface IGridResponse
{
	[JsonProperty("schema")]
	object Schema { get; }

	[JsonProperty("caption", NullValueHandling = NullValueHandling.Ignore)]
	string Caption { get; }

	[JsonProperty("allowWrite")]
	bool AllowWrite { get; }
	[JsonProperty("allowDelete")]
	bool AllowDelete { get; }
	[JsonProperty("allowInsert")]
	bool AllowInsert { get; }
	[JsonProperty("allowReorder")]
	bool AllowReorder { get; }
	[JsonProperty("allowSelect")]
	bool AllowSelect { get; }

	[JsonProperty("allSelected", NullValueHandling = NullValueHandling.Ignore)]
	object AllSelected { get; }

	[JsonProperty("hasSelection", NullValueHandling = NullValueHandling.Ignore)]
	object SelectedRows { get; }

	[JsonProperty("rows")]
	ICollection<IGridRow> Rows { get; }

	[JsonProperty("pageTitle", NullValueHandling = NullValueHandling.Ignore)]
	string PageTitle { get; }

	[JsonProperty("sortColumn", NullValueHandling = NullValueHandling.Ignore)]
	string SortColumn { get; }

	[JsonProperty("sortDir", NullValueHandling = NullValueHandling.Ignore)]
	string SortDir { get; }

	[JsonProperty("validationResponse", NullValueHandling = NullValueHandling.Ignore)]
	IValidationResponse ValidationResponse { get; }

	[JsonProperty("totalRows")]
	long TotalRows { get; }

	[JsonProperty("editUrlFormat")]
	string EditUrlFormat { get; }

	[JsonProperty("insertUrl")]
	string InsertUrl { get; }

	[JsonProperty("editorNewTab")]
	bool EditorNewTab { get; }
		
	[JsonProperty("version")]
	Guid Version { get; }

	void AddRow(IGridRow row);
}