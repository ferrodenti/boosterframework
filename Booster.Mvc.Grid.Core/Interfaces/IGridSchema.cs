﻿using System;
using System.Threading.Tasks;
using Booster.Evaluation;
using Booster.Mvc.Editor.Interfaces;
using Newtonsoft.Json;


namespace Booster.Mvc.Grid.Interfaces;

public interface IGridSchema : IEditorSchema
{
	[JsonProperty("caption", NullValueHandling = NullValueHandling.Ignore)]
	string Caption { get; }

	[JsonProperty("deleteText", NullValueHandling = NullValueHandling.Ignore)]
	string DeleteTextStr { get; }

	[JsonIgnore]
	DynamicParameter DeleteText { get; }

	[JsonIgnore]
	string ShortName { get; }

	[JsonIgnore]
	DynamicParameter InsertText { get; }

	[JsonIgnore]
	DynamicParameter EditText { get; }

	[JsonIgnore]
	DynamicParameter EditUrl { get; }

	[JsonIgnore]
	DynamicParameter InsertUrl { get; }

	[JsonIgnore]
	bool EditorNewTab { get; }

	[JsonIgnore]
	DynamicParameter OverrideRowControls { get; }

	[JsonIgnore]
	DynamicParameter RowControls { get; }
		
	[JsonIgnore]
	Func<IEditorDataSchema, Task<bool>> VisibilityCheck { get; set; } //Getter используется в перекрытии с классом EditorSchema

	void AddColumn(IGridDataSchema data);
}