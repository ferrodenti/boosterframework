﻿using Newtonsoft.Json;

namespace Booster.Mvc.Grid.Interfaces;

public interface IGridCell
{
	[JsonProperty("name")]
	string Name { get; }

	[JsonProperty("value", NullValueHandling = NullValueHandling.Ignore)]
	string Value { get; }
}