﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Booster.Mvc.Grid.Interfaces;

public interface IGridRow
{
	[JsonProperty("key")]
	string Key { get; }

	[JsonProperty("allowWrite", NullValueHandling = NullValueHandling.Ignore)]
	object AllowWrite { get; }
	[JsonProperty("allowDelete", NullValueHandling = NullValueHandling.Ignore)]
	object AllowDelete { get; }
	[JsonProperty("allowSelect", NullValueHandling = NullValueHandling.Ignore)]
	object AllowSelect { get; }
	[JsonProperty("selected", NullValueHandling = NullValueHandling.Ignore)]
	object Selected { get; }
	[JsonProperty("editUrl", NullValueHandling = NullValueHandling.Ignore)]
	object EditUrl { get; }

	[JsonProperty("overrideControls", NullValueHandling = NullValueHandling.Ignore)]
	object OverrideControls { get; }

	[JsonProperty("controls", NullValueHandling = NullValueHandling.Ignore)]
	object Controls { get; }

	[JsonProperty("cells")]
	ICollection<IGridCell> Cells { get; }

	void AddCell(IGridCell cell);
}