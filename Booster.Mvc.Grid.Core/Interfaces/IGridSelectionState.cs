using System.Collections.Generic;

namespace Booster.Mvc.Grid.Interfaces;

public interface IGridSelectionState
{
	IEnumerable<string> SelectedKeys { get; }

	IEnumerable<string> AllKeys { get; set; }

	int Count { get; }

	bool All { get; set; }
		
	bool this[string key] { get; set; }
}