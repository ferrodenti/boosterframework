using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Booster.DI;
using Booster.Interfaces;
using Booster.Orm.Interfaces;
using Booster.Reflection;

namespace Booster.Mvc.Grid.Orm;

public class OrmReorderer : BasicReorderer
{
	readonly TypeEx _entityType;
	readonly string _instanceContext;

	readonly SyncLazy<IOrm> _orm = SyncLazy<IOrm>.Create<OrmReorderer>(self => InstanceFactory.Get<IOrm>(self._instanceContext));
	protected IOrm Orm => _orm.GetValue(this);

	readonly SyncLazy<IRepository> _repository = SyncLazy<IRepository>.Create<OrmReorderer>(self => self.Orm.GetRepo(self._entityType));
	protected IRepository Repository => _repository.GetValue(this);

	readonly SyncLazy<IColumnMapping> _reorderableMapping = SyncLazy<IColumnMapping>.Create<OrmReorderer>(self =>
	{
		var pi = TypeEx.Get<IReorderable>().FindProperty("Order")?.FindImplementation(self._entityType);
		if (pi == null)
			throw new Exception(
				$"{self._entityType.Name} is IReorderable, but IReorderable.Order implementation was failed to reflect");

		return self.Repository.TableMapping.Columns.FirstOrDefault(c => c.Member.Inner == pi);
	});
	public IColumnMapping ReorderableMapping => _reorderableMapping.GetValue(this);

	public OrmReorderer(TypeEx entityType, string instanceContext = "DEFAULT")
	{
		_entityType = entityType;
		_instanceContext = instanceContext;

		InstanceFactory.On<IOrm>().Changed += (_, _) =>
		{
			if (InstanceFactory.CurrentContext == _instanceContext)
			{
				_repository.Reset();
				_reorderableMapping.Reset();
			}
		};
	}

	protected override async Task DoReorderAsync(Func<Task> act)
	{
		using var conn = InstanceFactory.Get<IConnection>(_instanceContext);
		using var trans = conn.BeginTransaction();
		await act().NoCtx();
		trans.Commit();
	}

	protected override async Task ChangeOrderAsync(IReorderable target, int newOrder)
	{
		var col = ReorderableMapping;

		target.Order = newOrder;

		if (col != null)
		{
			using var conn = InstanceFactory.Get<IConnection>(_instanceContext);
			var formatter = Orm.SqlFormatter;
			var param = new List<DbParameter> { Orm.CreateParameter("newOrder", newOrder) };
			var w = new EnumBuilder(" and ");

			foreach (var c in Repository.TableMapping.PkColumns)
			{
				var pkParam = $"PK_{c.ColumnName}";
				w.Append($"{c.EscapedColumnName}={formatter.ParamName(pkParam)}");
				param.Add(Orm.CreateParameter(pkParam, c.Member.Inner.GetValue(target)));
			}

			await conn.ExecuteNoQueryAsync($"update {Repository.TableName} set {col.EscapedColumnName}={formatter.ParamName("newOrder")} where {w};", param.ToArray()).NoCtx();

			Repository.OnUpdated(target);
		}
		else
			await Repository.UpdateAsync(target).NoCtx();
	}
		
	protected override void DoReorder(Action act)
	{
		using var conn = InstanceFactory.Get<IConnection>(_instanceContext);
		using var trans = conn.BeginTransaction();
		act();
		trans.Commit();
	}

	protected override void ChangeOrder(IReorderable target, int newOrder)
	{
		var col = ReorderableMapping;

		target.Order = newOrder;

		if (col != null)
		{
			using var conn = InstanceFactory.Get<IConnection>(_instanceContext);
			var formatter = Orm.SqlFormatter;
			var param = new List<DbParameter> { Orm.CreateParameter("newOrder", newOrder) };
			var w = new EnumBuilder(" and ");

			foreach (var c in Repository.TableMapping.PkColumns)
			{
				w.Append($"{c.EscapedColumnName}={formatter.ParamName(c.ColumnName)}");
				param.Add(Orm.CreateParameter(c.ColumnName, c.Member.Inner.GetValue(target)));
			}

			conn.ExecuteNoQuery($"update {Repository.TableName} set {col.EscapedColumnName}={formatter.ParamName("newOrder")} where {w};", param.ToArray());

			Repository.OnUpdated(target);
		}
		else
			Repository.Update(target);
	}	
}