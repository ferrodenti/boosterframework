﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Booster.AsyncLinq;
using Booster.Debug;
using Booster.DI;
using Booster.Helpers;
using Booster.Interfaces;
using Booster.Localization.Pluralization;
using Booster.Log;
using Booster.Mvc.Declarations;
using Booster.Mvc.Editor.Interfaces;
using Booster.Mvc.Grid.Interfaces;
using Booster.Mvc.Grid.Kitchen;
using Booster.Mvc.Helpers;
using Booster.Orm;
using Booster.Orm.Interfaces;
using Booster.Reflection;
using JetBrains.Annotations;

#if NETCOREAPP
using IAsyncEnumerableIReorderable = System.Collections.Generic.IAsyncEnumerable<Booster.Interfaces.IReorderable>;
#else
using IAsyncEnumerableIReorderable = Booster.AsyncLinq.IAsyncEnumerable<Booster.Interfaces.IReorderable>;
#endif

namespace Booster.Mvc.Grid.Orm
{
	// ReSharper disable once ClassWithVirtualMembersNeverInherited.Global
	public class BasicOrmGridController : BaseGridController
	{
		readonly StringEscaper _keyEscaper = new("\\", ";");

		readonly SyncLazy<string> _entityInstanceContext = SyncLazy<string>.Create<BasicOrmGridController>(self => self.EntityType.FindAttribute<DbInstanceContextAttribute>()?.ContextName);
		protected string EntityInstanceContext => _entityInstanceContext.GetValue(this);

		string _instanceContext;
		public string InstanceContext
		{
			get => _instanceContext ?? EntityInstanceContext ?? InstanceFactory.CurrentContext;
			set
			{
				if (_instanceContext != value)
				{
					Reset();
					_instanceContext = value;
				}
			}
		}

		readonly SyncLazy<IOrm> _orm = SyncLazy<IOrm>.Create<BasicOrmGridController>(self => InstanceFactory.Get<IOrm>(self.InstanceContext));
		protected IOrm Orm => _orm.GetValue(this);

		readonly SyncLazy<IRepository> _repository = SyncLazy<IRepository>.Create<BasicOrmGridController>(self =>
		{
			var repo = self.Orm.GetRepo(self.EntityType);

			if (self.SyncType == GridSyncType.Orm)
				repo.Changed += self.RepositoryChanged;

			return repo;
		});
		protected IRepository Repository => _repository.GetValue(this);


		readonly SyncLazy<IDataAccessor[]> _pkAccessors = SyncLazy<IDataAccessor[]>.Create<BasicOrmGridController>(self =>
			self.Repository.TableMapping.PkColumns.Select(c => (IDataAccessor) c.Member.Inner).ToArray());
		protected IDataAccessor[] PkAccessors => _pkAccessors.GetValue(this);


		readonly SyncLazy<string[]> _pkColNames = SyncLazy<string[]>.Create<BasicOrmGridController>(self => self.PkAccessors.Select(g => g.Name).ToArray());
		protected string[] PkColNames => _pkColNames.GetValue(this);

		readonly SyncLazy<Method> _filterMethod = SyncLazy<Method>.Create<BaseGridController>(self =>
			self.DecoratorType.FindMethod(new ReflectionFilter
			{
				ReturnTypes = new[] {typeof(void), typeof(Task)},
				IsStatic = true
			}.RequireAttribute<GridFilterAttribute>()));
		
		protected Method FilterMethod => _filterMethod.GetValue(this);

		public bool AutogeneratePksOnDefaultValues { get; set; } = true;

		readonly SyncLazy<string> _selectKeysQuery = SyncLazy<string>.Create<BasicOrmGridController>(self =>
		{
			var str = new EnumBuilder("||';'||");

			foreach (var pk in self.Repository.TableMapping.PkColumns)
				str.Append(pk.ColumnName);

			return $"select {str} from {self.Repository.TableName}";
		});
		protected string SelectKeysQuery => _selectKeysQuery.GetValue(this);

		[PublicAPI]
		public BasicOrmGridController(TypeEx entityType, ILog log) 
			: this(entityType, null, null, log)
		{
		}

		public BasicOrmGridController(TypeEx entityType, string key = null, string instanceContext = null, ILog log = null) 
			: base(entityType, key, log)
		{
			_instanceContext = instanceContext;

			if (Key.IsEmpty())
				Key = Repository.TableName;

			InstanceFactory.On<IOrm>().Changed += (_, _) =>
			{
				if (InstanceFactory.CurrentContext == InstanceContext)
					Reset();
			};
		}


		protected override IGridSchema GetWriteSchema()
		{
			var res = base.GetWriteSchema();

			foreach (var col in res.Columns)
				if (Repository.TableMapping.TryGetColumnByName(col.Name, out var map))
				{
					col.AddSetting("Mapping", map.Column);
					col.AddSetting("DbType", map.ColumnNativeType?.Inner);
				}

			return res;
		}

		protected override void ValidateLength(IValidationResponse validationResponse, IEditorDataSchema column, object value)
		{
			var type = (column.GetSetting(null, "DbType") as Type).With(TypeEx.Get);

			if (column.GetSetting(null, "Mapping") is IColumnSchema map && type != null && map.DataLength > 0 && value != null)
			{
				var len = 0;

				if (type == typeof(byte[]))
				{
					if (value is byte[] arr)
						len = arr.Length;
					//TODO: uploaded file
				}
				else if (type == typeof(char) || type == typeof(string))
					len = StringConverter.Default.ToString(value)?.Length ?? 0;
				else if (type.IsInteger)
				{
					if (value.GetType().IsEnum)
						value = EnumHelper.GetIntValue(value);

					len = NumericHelper.ToString(value, map.Radix).TrimStart('-').Length;
				}
				else if (type.IsReal && map.Radix == 10)
					len = (value.ToString()?.TrimStart('-').Replace(".", "") ?? "").Length; 
				
				if (value?.GetType() == typeof(bool))
					len = 1;

				if (len > map.DataLength)
				{
					string cnt;
					switch (map.Radix)
					{
					case 2:
						cnt = Plural.Ru(map.DataLength, "бит", "бита", "битов");
						break;
					case 10:
						cnt = Plural.Ru(map.DataLength, "символ", "символа", "символов");
						break;
					default:
						var p = (int)Math.Ceiling(Math.Log10(Math.Pow(map.Radix, map.DataLength)));
						cnt = Plural.Ru(p, "символ", "символа", "символов");
						break;
					}
					
					validationResponse.Error(column.Name, $"Превышен максимально допустимый размер значения ({cnt})");
				}
			}
		}

		void Reset()
		{
			if (_repository.IsValueCreated && SyncType == GridSyncType.Orm)
				Repository.Changed -= RepositoryChanged;

			_repository.Reset();
			_pkAccessors.Reset();
			_pkColNames.Reset();
			_selectKeysQuery.Reset();
			_reorderer.Reset();
			_orderColFormatted.Reset();
			_orderBy.Reset();
			_orderByDesc.Reset();
			_reorderableComparer.Reset();
		}

		readonly ConcurrentDictionary<string, object> _sortExpressions = new();

		void RepositoryChanged(object sender, RepositoryChangedEventArgs e)
			=> UpdateVersion();

		protected virtual string GetPageTitle() 
			=> ReadSchema.Caption;

		public override async Task<object> Select(GridRequest request)
		{
			var permissions = await GetPermissions(request, null /* CreateEntity(request)*/);
			
			if(CheckInsertPermissionWithNewEntity && permissions.HasFlag(GridPermissions.Insert))
				try
				{
					var insert = await GetPermissions(request, await CreateEntityForSecurityCheck(request));
					if (!insert.HasFlag(GridPermissions.Insert))
						permissions &= ~GridPermissions.Insert;
				}
				catch { /*ignore*/ }

			if (!permissions.HasFlag(GridPermissions.Read))
				return HttpStatusHelper.Forbidden($"Does not have read permission for grid controller {request.Controller}");

			var state = GetSelectionState();

			if (state == null)
				permissions = permissions.Reset(GridPermissions.Select);

			if (!Reorderable || request.SortDir != OrderType.None)
				permissions = permissions.Reset(GridPermissions.Reorder);

			var response = new GridResponse(Version)
			{
				PageTitle = Caption,
				GridSchema = ReadSchema,
				Permissions = permissions,
				SortColumn = request.Sort,
				SortDir = request.SortDir != OrderType.None ? request.SortDir.ToString().ToLower() : null,
			};

			var condition = await GetCondition(request);

			var repo = Repository;

			if (state?.Count > 0)
			{
				var keys = (await GetAllKeys(request).NoCtx()).Select(k => k.ToString()).ToArray();
				state.AllKeys = keys;
				response.TotalRows = keys.Length;
				response.SelectedRows = state.Count;
				response.AllSelected = state.All;
			}
			else
				response.TotalRows = await repo.CountAsync(condition);

			IAsyncEnumerable<object> ie;
			if (request.SortDir == OrderType.None || string.IsNullOrEmpty(request.Sort))
				ie = repo.SelectAsync(request.Skip, request.Take, condition).Select(Decorate);
			else
			{
				var sort = _sortExpressions.GetOrAdd(request.Sort, _ =>
				{
					IColumnMapping colMapping;
					
					if (ReadSchema.Columns.FirstOrDefault(c => c.Name == request.Sort) is not IGridDataSchema res)
					{
						colMapping = repo.TableMapping.Columns.FirstOrDefault(c => c.MemberName == request.Sort);
						if (colMapping != null)
							return colMapping.ColumnName;
						
						throw new Exception($"Column {request.Sort} not found in a grid schema");
					}
						

					if (res.SortExpression != null)
						return res.SortExpression;

					colMapping = repo.TableMapping.Columns.FirstOrDefault(c => c.MemberName == res.Name);
					if (colMapping != null)
						return colMapping.ColumnName;

					return res;
				});

				void SelectSort(FormattableString fmt)
				{
					condition.AppendOrder(fmt, request.SortDir);
					
					ie = repo.SelectAsync(request.Skip, request.Take, condition)
						.Select(Decorate);
				}

				switch (sort)
				{
				case string str:
					SelectSort($"{Orm.SqlFormatter.EscapeName(str)}");
					break;
				case FormattableString fmt:
					SelectSort(fmt);
					break;
				default:
				{
					var col = (IGridDataSchema) sort;
					var ie2 = repo.SelectAsync().Select(Decorate);

					ie2 = (await (request.SortDir == OrderType.None
						? ie2.OrderBy(o => col.DataAccessor.GetValue(o))
						: ie2.OrderByDescending(o => col.DataAccessor.GetValue(o)))).AsAsyncEnumerable();

					ie = ie2.Skip(request.Skip).Take(request.Take);
					break;
				}
				}
			}

			var selState = GetSelectionState();
			var hiddenColumns = new HashSet<string>(request.HiddenColumns ?? Array.Empty<string>());

			if (response.GridSchema.EditUrl?.IsStatic == true)
				response.EditUrlFormat = response.GridSchema.EditUrl.StaticValue?.ToString();

			if (response.GridSchema.InsertUrl != null)
			{
				response.InsertUrl = response.GridSchema.InsertUrl.Evaluate(null)?.ToString();
				response.EditorNewTab = response.GridSchema.EditorNewTab;
			}

			var cnt = request.Skip;
#if NETCOREAPP
			await foreach (var entity in ie.NoCtx())
				await AddRow(request, hiddenColumns, response, entity, GetKey(entity), ++cnt, selState).NoCtx();
#else
			await ie.ForEach(async entity
				=> await AddRow(request, hiddenColumns, response, entity, GetKey(entity), ++cnt, selState), ctx: true);
#endif
			(request.SecurityCheckContext as IDisposable)?.Dispose();

			return response;
		}

		// TODO: Раньше тут стояла пометка "Не возвращает QueryBuilder т.к. при использовании базового класса может не быть сборки"
		protected override async Task<QueryBuilder> GetCondition(GridRequest request)
		{
			if (FilterMethod != null)
			{
				var bld = new QueryBuilder(Repository);

				await FilterMethod.InvokeAsync(null, 
					GetMethodArgsFromForm(request, bld, request.Form, FilterMethod.Parameters).ToArray()).NoCtx();

				return bld;
			}

			return await base.GetCondition(request).NoCtx();
		}


		public override async Task<HashSet<object>> GetAllKeys(GridRequest request)
		{
			var condition = await GetCondition(request).NoCtx();
			return await AllKeys(condition).NoCtx();
		}

		async Task<HashSet<object>> AllKeys(QueryBuilder condition)
		{
			var query = new QueryBuilder($"{SelectKeysQuery}");
			if (condition.IsSome)
				query.Append($" where {condition}");

			using var conn = InstanceFactory.Get<IConnection>(InstanceContext);
			return await conn.ExecuteEnumerableAsync<object>(query).ToHashSet();
		}
 
		protected override string GetKey(object entity, bool allowNull = false) 
			=> _keyEscaper.Join(GetKeys(entity, allowNull).Select(k => StringConverter.Default.ToString(k) ?? ""), ";");

		IEnumerable<object> GetKeys(object entity, bool allowNull = false)
		{
			if (entity == null)
			{
				if (!allowNull)
					throw new ArgumentNullException(nameof(entity));

				foreach (var getter in PkAccessors)
					yield return getter.ValueType.DefaultValue;
			}
			else
				foreach (var getter in PkAccessors)
					yield return getter.GetValue(Undecorate(entity));
		}

		object[] ParseKeys(string key)
		{
			var oldSKeys = _keyEscaper.Split(key, ";", false, true).ToArray();
			var oldKeys = new object[PkAccessors.Length];

			if (oldKeys.Length != oldSKeys.Length)
				throw new Exception($"Invalid key \"{key}\": expected {PkAccessors.ToString(g => g.ValueType.Name, new EnumBuilder("};{", "{", "}", "???"))}");

			for (var i = 0; i < PkAccessors.Length; i++)
				oldKeys[i] = StringConverter.Default.ToObject(oldSKeys[i], PkAccessors[i].ValueType);
			return oldKeys;
		}

		protected override Task Delete(object entity) 
			=> Repository.DeleteAsync(Undecorate(entity));

		protected override async Task Save(GridRequest request, object entity)
		{
			entity = Undecorate(entity);
			
			if (PkColNames.Intersect(request.Data.Keys).Any())
			{
				var newKey = GetKey(entity);
				if (request.Key != newKey)
				{
					if (request.Key == null)
					{
						if (!AutogeneratePksOnDefaultValues || ParseKeys(newKey).Any(k => k != null && !Equals(k, TypeEx.Of(k).DefaultValue)))
						{
							await UpdateOrder(entity);
							await Repository.InsertWithPksAsync(entity).NoCtx();
							return;
						}
					}
					else
					{
						var oldKeys = ParseKeys(request.Key);
						await Repository.UpdateWithPksAsync(entity, oldKeys).NoCtx();
						return;
					}
				}
			}

			if (request.Key == null)
			{
				await UpdateOrder(entity);
				await Repository.InsertAsync(entity).NoCtx();
			}
			else
				await Repository.UpdateAsync(entity).NoCtx();
		}

		protected virtual async Task UpdateOrder(object entity)
		{
			if (entity is IReorderable {Order: 0} ireord)
				try
				{
					using var conn = Repository.Orm.GetCurrentConnection();
					ireord.Order = await conn.ExecuteScalarAsync<int>($"select max({OrderColFormatted}) from {Repository.TableName}").NoCtx() + 10;
				}
				catch (Exception ex)
				{
					Utils.Nop(ex);
					SafeBreak.Break();
				}
		}

		protected override async Task<object> GetEntity(string byKey, bool decorate)
		{
			if (byKey.IsEmpty())
				return null;

			var result = await Repository.LoadByPksAsync(ParseKeys(byKey)).NoCtx();
			return decorate ? Decorate(result) : result;
		}

		protected override IAsyncEnumerable<object> GetEntities(IEnumerable<string> keys, bool decorate)
		{
			var result = Repository.SelectByPksAsync(keys.SelectMany(ParseKeys));
			return decorate ? result.Select(Decorate) : result;
		}

		#region Reorder

		readonly SyncLazy<IReorderer> _reorderer = SyncLazy<IReorderer>.Create<BasicOrmGridController>(self => new OrmReorderer(self.EntityType, self.InstanceContext));
		protected override IReorderer Reorderer => _reorderer.GetValue(this);

		protected virtual IColumnMapping ReorderableMapping => ((OrmReorderer)Reorderer).ReorderableMapping;

		readonly SyncLazy<string> _orderColFormatted = SyncLazy<string>.Create<BasicOrmGridController>(
			self => self.ReorderableMapping.With(m => m.EscapedColumnName));

		protected string OrderColFormatted => _orderColFormatted.GetValue(this);

		readonly SyncLazy<string> _orderBy = SyncLazy<string>.Create<BasicOrmGridController>(self =>
		{
			if (self.OrderColFormatted != null)
				return self.OrderColFormatted + self.Repository.TableMapping.PkColumns.ToString(c => c.EscapedColumnName, new EnumBuilder(",", ","));

			return null;
		});
		protected string OrderBy => _orderBy.GetValue(this);

		readonly SyncLazy<string> _orderByDesc = SyncLazy<string>.Create<BasicOrmGridController>(self =>
		{
			if (self.OrderColFormatted != null)
				return $"{self.OrderColFormatted} desc{self.Repository.TableMapping.PkColumns.ToString(c => $"{c.EscapedColumnName} desc", new EnumBuilder(",", ","))}";

			return null;
		});
		protected string OrderByDesc => _orderByDesc.GetValue(this);

		readonly SyncLazy<IComparer<IReorderable>> _reorderableComparer = SyncLazy<IComparer<IReorderable>>.Create<BasicOrmGridController>(
			self => new LambdaComparer<IReorderable>(self.PkAccessors.Select(g => new Func<IReorderable, object>(g.GetValue)).AppendBefore(o => o.Order).ToArray()));

		protected IComparer<IReorderable> ReorderableComparer => _reorderableComparer.GetValue(this);

		protected override async Task<IReorderable> GetPreviousEntity(IReorderable entity)
		{
			var col = ReorderableMapping;
			var pks = GetKeys(entity).ToArray();
			

			if (pks.Length == 0)
				throw new Exception("Entity has no primary key(s)");

			IReorderable result = null;
			if (col != null)
			{
				var neibFound = false;
#if NETCOREAPP
				await foreach (IReorderable r in Repository.SelectAsync($"{OrderColFormatted}<={Orm.SqlFormatter.ParamName("minOrder")}", OrderByDesc, 5, new { minOrder = entity.Order }).NoCtx())
					if (pks.SequenceEqual(GetKeys(r)))
						neibFound = true;
					else if (neibFound)
					{
						result = r;
						break;
					}
#else
				await Repository.SelectAsync($"{OrderColFormatted}<={Orm.SqlFormatter.ParamName("minOrder")}", OrderByDesc, 5, new {minOrder = entity.Order}).Cast<IReorderable>()
					.ForEach(r =>
					{
						if (pks.SequenceEqual(GetKeys(r)))
							neibFound = true;
						else if (neibFound)
						{
							result = r;
							return false;
						}

						return true;
					}).NoCtx();
#endif

				return result;
			}

			IReorderable last = null;
			foreach(var r in await Repository.SelectAsync("", 30).Cast<IReorderable>().OrderBy(r => r, ReorderableComparer))
			{
				if (pks.SequenceEqual(GetKeys(r)))
					return last;

				last = r;
			}
			return null;
		}


		protected override async Task<IAsyncEnumerableIReorderable> GetCollectionForReorder(IReorderable entity, IReorderable after)
		{
			var col = ReorderableMapping;

			var afterPks = after.With(a => GetKeys(a).ToArray());
			var entityPks = GetKeys(entity).ToArray();

			if (afterPks.With(a => a.Length == 0) || entityPks.Length == 0)
				throw new Exception("Entity has no primary key(s)");

			IAsyncEnumerableIReorderable res;

			if (col != null)
				res = (after != null
					? Repository.SelectAsync($"{OrderColFormatted}>={Orm.SqlFormatter.ParamName("minOrder")}", OrderBy, 5, new {minOrder = after.Order})
					: Repository.SelectAsync("", OrderBy, 5)).Cast<IReorderable>();
			else
				res = (await Repository.SelectAsync().Cast<IReorderable>().OrderBy(r => r, ReorderableComparer).NoCtx()).AsAsyncEnumerable();

			return res.Where(r =>
			{
				if (afterPks != null || entityPks != null)
				{
					var pks = GetKeys(r).ToArray();
					if (afterPks != null)
					{
						if (pks.SequenceEqual(afterPks))
						{
							afterPks = null;
							return false;
						}
						return false;
					}

					if (entityPks != null && pks.SequenceEqual(entityPks))
					{
						entityPks = null;
						return false;
					}
				}

				return true;
			});
		}

		protected override bool EntityEquals(object a, object b) 
			=> GetKeys(a).SequenceEqual(GetKeys(b));

		#endregion
	}
}
