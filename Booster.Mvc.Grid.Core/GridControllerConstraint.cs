﻿using JetBrains.Annotations;

namespace Booster.Mvc.Grid;

#if NETSTANDARD || NETCOREAPP
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
	
[PublicAPI]
public class GridControllerConstraint : IRouteConstraint
{
	public bool Match(HttpContext httpContext, IRouter route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
	{
		var key = values[parameterName].With(v => v.ToString());
		if (key.IsSome())
			return Grid.GetController(key) != null;

		return false;
	}
}
#else
using System.Web;
using System.Web.Routing;
	
[PublicAPI]
public class GridControllerConstraint : IRouteConstraint
{
	public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
	{
		var key = values[parameterName].With(v => v.ToString());
		if (key.IsSome())
			return Grid.GetController(key) != null;

		return false;
	}
}
#endif