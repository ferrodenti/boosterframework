using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Booster.Collections;
using Booster.DI;
using Booster.Evaluation;
using Booster.Mvc.Declarations;
using Booster.Mvc.Editor.Interfaces;
using Booster.Mvc.Grid.Interfaces;
using Booster.Mvc.Grid.Orm;
using Booster.Reflection;

namespace Booster.Mvc.Grid;

public class BasicGridPermissionsProvider : IGridPermissionsProvider
{
	public IPermissionsProvider PermissionProvider { get; set; }

	protected class Permissions
	{
		readonly TypeEx _entityType;
		public object Read { get; }
		public object Write { get; }
		public object Delete { get; }
		public object Insert { get; }
		public object Reorder { get; }
		public object Select { get; }

		protected Permissions(TypeEx entityType)
			=> _entityType = entityType;

		public Permissions(TypeEx entityType, object read, object write) : this(entityType)
		{
			var cache = new ListDictionary<object, object>();

			Read = CreatePermission(cache, read);
			Write = CreatePermission(cache, write);
		}

		public Permissions(TypeEx entityType, DataPermissions permissions) : this(entityType)
		{
			var cache = new ListDictionary<object, object>();
			if (permissions != null)
			{
				Read = CreatePermission(cache, permissions.Read);
				Write = CreatePermission(cache, permissions.Write);
				Delete = CreatePermission(cache, permissions.Delete);
				Insert = CreatePermission(cache, permissions.Insert);
				Reorder = CreatePermission(cache, permissions.Reorder);
				Select = CreatePermission(cache, permissions.Select);
			}
		}

		object CreatePermission(ListDictionary<object, object> cache, object perm)
		{
			if (perm == null)
				return null;

			return cache.GetOrAdd(perm, _ =>
			{
				if (perm is string str)
					return new DynamicParameter(_entityType, str)
					{
						DataAccessorProvider = Grid.DataAccessorProvider
					};

				return perm;
			});
		}

		public object this[GridPermissions key] => key switch
												   {
													   GridPermissions.Read    => Read,
													   GridPermissions.Write   => Write,
													   GridPermissions.Delete  => Delete,
													   GridPermissions.Insert  => Insert,
													   GridPermissions.Reorder => Reorder,
													   GridPermissions.Select  => Select,
													   _                       => null
												   };
	}

	readonly ConcurrentDictionary<IGridController, Tuple<Permissions, Permissions>> _gridPermissions = new();
	readonly ConcurrentDictionary<IEditorDataSchema, Permissions> _gridColumnPermissions = new();

	public async Task<GridPermissions> GetPermissions(IGridController controller, object entity, GridPermissions permissions = GridPermissions.All)
	{
		var res = GridPermissions.None;
		var permissionsProvider = PermissionProvider ?? InstanceFactory.Get<IPermissionsProvider>(false);
		object user = null;
		if(permissionsProvider != null)
			user = await permissionsProvider.GetUserPermissions().NoCtx();

		var cache = new ListDictionary<object, bool>();
		var tup = GetGridPermissions(controller);
		var grid = tup.Item1;
		var row = tup.Item2;

		foreach (var perm in permissions.Split())
			if (perm != GridPermissions.None && perm != GridPermissions.All && CheckPermissionCache(permissionsProvider, cache, user, null, grid[perm]))
			{
				if (entity != null && !CheckPermissionCache(permissionsProvider, cache, user, entity, row[perm]))
					continue;

				res |= perm;
			}

		return res;
	}

	public async Task<GridPermissions> GetPermissions(IGridController controller, IEditorDataSchema column, GridPermissions permissions = GridPermissions.All)
	{
		var res = GridPermissions.None;
		var permissionsProvider = PermissionProvider ?? InstanceFactory.Get<IPermissionsProvider>(false);

		object user = null;
		if(permissionsProvider != null)
			user = await permissionsProvider.GetUserPermissions().NoCtx();
			
		var cache = new ListDictionary<object, bool>();
		var colPerm = GetGridColumnPermissions(controller, column);

		foreach (var perm in permissions.Split())
			if (perm != GridPermissions.None && perm != GridPermissions.All && CheckPermissionCache(permissionsProvider, cache, user, null, colPerm[perm]))
				res |= perm;

		return res;
	}

	protected virtual bool CheckPermissionCache(IPermissionsProvider permissionsProvider, ListDictionary<object, bool> cache, object user, object entity, object value)
	{
		if (value == null)
			return true;

		return cache.GetOrAdd(Tuple.Create(entity ?? "", value), _ =>
		{
			var value1 = value;
			if (value1 is DynamicParameter dyna)
				value1 = dyna.Evaluate(entity);

			return permissionsProvider.With(pp => pp.ComparePermisssions(value1, user));
		});
	}

	protected Permissions GetGridColumnPermissions(IGridController controller, IEditorDataSchema column)
		=> _gridColumnPermissions.GetOrAdd(column, ReadGridColumnPermissions(controller, column));

	protected Tuple<Permissions, Permissions> GetGridPermissions(IGridController controller)
		=> _gridPermissions.GetOrAdd(controller, ReadGridPermissions);

	protected virtual Tuple<Permissions, Permissions> ReadGridPermissions(IGridController controller)
	{
		var type = controller.DecoratorType;
		var gridAttr = type.FindAttribute<GridPermissionsAttribute>();
		var rowAttr = type.FindAttribute<GridRowPermissionsAttribute>();

		return Tuple.Create(new Permissions(type, gridAttr?.Permissions), new Permissions(type, rowAttr?.Permissions));
	}

	protected virtual Permissions ReadGridColumnPermissions(IGridController controller, IEditorDataSchema column)
		=> new(controller.DecoratorType, column.ReadPermissions, column.WritePermissions);
}