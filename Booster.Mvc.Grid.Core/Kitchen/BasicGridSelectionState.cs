using System.Collections.Generic;
using Booster.Mvc.Grid.Interfaces;

namespace Booster.Mvc.Grid.Kitchen;

public class BasicGridSelectionState : IGridSelectionState
{
	HashSet<string> _allKeys = new();
	HashSet<string> _selectedKeys = new();

	public IEnumerable<string> SelectedKeys => _selectedKeys;

	public IEnumerable<string> AllKeys
	{
		get => _allKeys;
		set
		{
			if (!_allKeys.SetEquals(value))
			{
				_allKeys = new HashSet<string>(value);

				_selectedKeys.Clear();
				_all = false;
			}
		}
	}

	public int Count => _selectedKeys.Count;

	bool _all;
	public bool All
	{
		get => _all;
		set
		{
			if (_all != value)
			{
				_all = value;
				if (_all)
					_selectedKeys = new HashSet<string>(_allKeys);
				else
					_selectedKeys.Clear();
			}
		}
	}

	public bool this[string key]
	{
		get => _selectedKeys.Contains(key);
		set
		{
			if (_allKeys.Contains(key))
			{
				if (value)
					_selectedKeys.Add(key);
				else
					_selectedKeys.Remove(key);

				_all = _selectedKeys.SetEquals(_allKeys);
			}
		}
	}
}