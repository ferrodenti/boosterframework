using System;

namespace Booster.Mvc.Grid.Orm;

[Flags]
[Serializable]
public enum GridPermissions
{
	None = 0,
	Read = 1,
	Write = 2,
	Delete = 4,
	Insert = 8,
	Reorder = 16,
	Select = 32,
	All = 65535
}