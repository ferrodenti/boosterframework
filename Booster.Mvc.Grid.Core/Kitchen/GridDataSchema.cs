﻿using Booster.Mvc.Editor;
using Booster.Mvc.Grid.Interfaces;

namespace Booster.Mvc.Grid.Kitchen;

public class GridDataSchema : EditorDataSchema, IGridDataSchema
{
	public string SortExpression { get; set; }
	public bool Sortable { get; set; }

	// ReSharper disable NonReadonlyMemberInGetHashCode
	public override int GetHashCode()
		=> new HashCode(base.GetHashCode(), SortExpression, Sortable);
	// ReSharper restore NonReadonlyMemberInGetHashCode
}