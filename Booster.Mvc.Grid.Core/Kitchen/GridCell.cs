﻿using Booster.Mvc.Grid.Interfaces;

namespace Booster.Mvc.Grid.Kitchen;

public class GridCell : IGridCell
{
	public string Name { get; }
	public string Value { get; }

	public GridCell(string name, string value)
	{
		Value = value;
		Name = name;
	}
}