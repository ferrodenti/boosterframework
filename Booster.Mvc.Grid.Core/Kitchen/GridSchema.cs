﻿using Booster.Evaluation;
using Booster.Mvc.Editor;
using Booster.Mvc.Grid.Interfaces;

namespace Booster.Mvc.Grid.Kitchen;

public class GridSchema : EditorSchema, IGridSchema
{
	public string Caption { get; set; }
	public string DeleteTextStr => DeleteText.With(t => t.Expression);
    public string ShortName { get; set; }
	public DynamicParameter DeleteText { get; set; }
	public DynamicParameter InsertText { get; set; }
	public DynamicParameter EditText { get; set; }
	public DynamicParameter EditUrl { get; set; }
	public DynamicParameter InsertUrl { get; set; }
	public bool EditorNewTab { get; set; }
	public DynamicParameter OverrideRowControls { get;  set; }
	public DynamicParameter RowControls { get; set; }

	public void AddColumn(IGridDataSchema data)
        => base.AddColumn(data);
}