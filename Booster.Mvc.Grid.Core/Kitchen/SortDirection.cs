namespace Booster.Mvc.Grid.Kitchen;

public enum SortDirection
{
	No = 0,
	Asc = 1,
	Desc = 2
}