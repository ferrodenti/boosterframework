﻿using System.Collections.Generic;
using Booster.Mvc.Grid.Interfaces;

namespace Booster.Mvc.Grid.Kitchen;

public class GridRow : IGridRow
{
	public string Key { get; set; }
	public object Selected { get; set; }
	public object EditUrl { get; set; }
	public object OverrideControls { get; set; }
	public object Controls { get; set; }
	public object AllowWrite { get; set; }
	public object AllowDelete { get; set; }
	public object AllowSelect { get; set; }

	public ICollection<IGridCell> Cells { get; }

	public GridRow()
        => Cells = new List<IGridCell>();


    public void AddCell(IGridCell cell)
        => Cells.Add(cell);
}