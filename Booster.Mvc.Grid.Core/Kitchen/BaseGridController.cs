using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Booster.AsyncLinq;
using Booster.Evaluation;
using Booster.Interfaces;
using Booster.Log;
using Booster.Mvc.Declarations;
using Booster.Mvc.Editor;
using Booster.Mvc.Editor.Interfaces;
using Booster.Mvc.Grid.Interfaces;
using Booster.Mvc.Grid.Orm;
using Booster.Mvc.Helpers;
using Booster.Obsolete;
using Booster.Reflection;
using JetBrains.Annotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

#if !NETSTANDARD && !NETCOREAPP
using System.IO;
using System.Web;
#endif

#if !NETCOREAPP
using IAsyncEnumerableIReorderable = Booster.AsyncLinq.IAsyncEnumerable<Booster.Interfaces.IReorderable>;
#else
using IAsyncEnumerableIReorderable = System.Collections.Generic.IAsyncEnumerable<Booster.Interfaces.IReorderable>;
#endif
using QueryBuilder = Booster.Orm.QueryBuilder;

namespace Booster.Mvc.Grid.Kitchen;

[PublicAPI]
public abstract class BaseGridController : IGridController
{

	static readonly GridSchemaReader _schemaReader = new();
	static readonly StringEscaper _stringEscaper = new("\\", ";");
	public TypeEx EntityType { get; }
	public TypeEx DecoratorType { get; }

	public string Key { get; protected set; }
	public string Name => ReadSchema.ShortName;
	public string Caption => ReadSchema.Caption;
	public bool CheckInsertPermissionWithNewEntity { get; set; }
	protected Guid Version = Guid.NewGuid();
	protected readonly GridSyncType SyncType;
	protected readonly bool IsDecorator;
	protected readonly ILog Log;
		
	readonly SyncLazy<Func<string, IGridSelectionState>> _selectionStateGetter = SyncLazy<Func<string, IGridSelectionState>>.Create<BaseGridController>(self =>
	{
		var attr = self.DecoratorType.FindAttribute<GridUserSelectAttribute>();
		if (attr == null) 
			return null;
			
		var param = new DynamicParameter(self.DecoratorType, attr.SessionKey)
		{
			DataAccessorProvider = Grid.DataAccessorProvider
		};

		return key =>
		{
			IGridSelectionState res = null;
			var ses = MvcHelper.Session;
			if (ses == null) 
				return null;
				
			if (key.IsEmpty())
				key = param.Evaluate(null) as string;

			if (key.IsSome())
			{
				res = ses[key] as IGridSelectionState;
				if (res == null)
					ses[key] = res = new BasicGridSelectionState();
			}
			return res;
		};
	});

	public virtual IGridSelectionState GetSelectionState(string sesKey = null) 
		=> _selectionStateGetter.GetValue(this)?.Invoke(sesKey);


	IGridSchema _readSchema;
	protected IGridSchema ReadSchema => _readSchema ??= GetReadSchema();
		
	public IGridPermissionsProvider GridPermissionsProvider { get; set; }
		
	protected virtual IGridSchema GetReadSchema()
	{
		var res = (IGridSchema)_schemaReader.GetSchema(DecoratorType, false);

		if (res.Columns.Any(c => c.ReadPermissions != null))
			res.VisibilityCheck = async c => c.ReadPermissions == null || await CheckColumnPermissions(Grid.CurrentRequest, c, GridPermissions.Read).NoCtx();

		return res;
	}

	IGridSchema _writeSchema;
	protected IGridSchema WriteSchema => _writeSchema ??= GetWriteSchema();

	TypeEx _entityChangesType;
	public TypeEx EntityChangesType => _entityChangesType ??= typeof(EntityChanges<>).MakeGenericType(DecoratorType);

	#region Entity reflection

	readonly SyncLazy<Method> _validationMethod = SyncLazy<Method>.Create<BaseGridController>(
		self => self.DecoratorType.FindMethod(
			new ReflectionFilter()
				.Where(m => m is Method mtd &&
				            mtd.Parameters.Length > 1 &&
				            typeof(IValidationResponse).IsAssignableFrom(mtd.Parameters[0].ParameterType))
				.RequireAttribute<GridValidationAttribute>()
		));
		
	protected Method ValidationMethod => _validationMethod.GetValue(this);

	readonly SyncLazy<Method[]> _changesApplyMethods = SyncLazy<Method[]>.Create<BaseGridController>(
		self => self.DecoratorType.FindMethods(
			new ReflectionFilter { IsStatic = false }
				.RequireAttribute<GridChangesAppliedAttribute>()
		).ToArray());
		
	protected Method[] ChangesApplyingMethods => _changesApplyMethods.GetValue(this);


	bool? _shouldCreateEntityChanges;
	protected bool ShouldCreateEntityChanges => _shouldCreateEntityChanges ??= ChangesApplyingMethods.Length > 0;

	readonly SyncLazy<BaseMember> _rowControlsMember = SyncLazy<BaseMember>.Create<BaseGridController>(
		self =>
		{
			var result = self.DecoratorType.FindMember(
				new ReflectionFilter(MemberTypes.Method | MemberTypes.Property)
				{
					ReturnTypes = new[] { typeof(string), typeof(Task<string>) },
					ParametersCount = 0,
				}.RequireAttribute<GridRowControlsAttribute>());
				
			var attr = result?.FindAttribute<GridRowControlsAttribute>();
				
			if (attr?.Override != null && self.ReadSchema is GridSchema schema) //TODO: это бы вынести из lazy
				schema.OverrideRowControls = attr.Override is string str
					? new DynamicParameter(self.DecoratorType, str)
					: new DynamicParameter(attr.Override);

			return result;
		});
		
	protected BaseMember RowControlsMember => _rowControlsMember.GetValue(this);

	readonly SyncLazy<MethodInfo> _newEntryMethod = SyncLazy<MethodInfo>.Create<BaseGridController>(
		self => self.DecoratorType.FindMethod(new ReflectionFilter
		{
			RequiredAttributes = { TypeEx.Get<GridNewEntryAttribute>() },
			IsStatic = false
		}));
	protected Method NewEntryMethod => _newEntryMethod.GetValue(this);

	readonly SyncLazy<MethodInfo> _deleteEntryMethod = SyncLazy<MethodInfo>.Create<BaseGridController>(
		self => self.DecoratorType.FindMethod(new ReflectionFilter
		{
			RequiredAttributes = { TypeEx.Get<GridDeleteEntryAttribute>() },
			IsStatic = false
		}));
	protected Method DeleteEntryMethod => _deleteEntryMethod.GetValue(this);
		
	// А метод фильтрации находится в BasicOrmGridController, т.к. там используется QueryBuilder
		
	#endregion
		

	bool? _reorderable;

	protected virtual bool Reorderable => _reorderable ??= typeof(IReorderable).IsAssignableFrom(EntityType) &&
	                                                       typeof(IReorderable).IsAssignableFrom(DecoratorType);
		
	IReorderer _reorderer;
	protected virtual IReorderer Reorderer => _reorderer ??= new BasicReorderer();

	bool? _hasRowPermissions;
	protected virtual bool HasRowPermissions => _hasRowPermissions ??= DecoratorType.HasAttribute<GridRowPermissionsAttribute>();

	Dictionary<string, IDataAccessor> _fileDataAccessors;

	protected virtual IGridSchema GetWriteSchema()
	{
		var res = (IGridSchema)_schemaReader.GetSchema(DecoratorType, true);

		if (res.Columns.Any(c => c.WritePermissions != null))
			res.VisibilityCheck = async c => c.WritePermissions == null || await CheckColumnPermissions(Grid.CurrentRequest, c, GridPermissions.Write).NoCtx();

		_fileDataAccessors = new Dictionary<string, IDataAccessor>(StringComparer.OrdinalIgnoreCase);

		foreach (var col in res.Columns)
			if (col.EditorType == EditorType.File || col.EditorType == EditorType.Photo)
			{
				var oldDa = col.DataAccessor;
				var colName = col.Name;

				_fileDataAccessors[colName] = oldDa;

				if (typeof (IWebFile).IsAssignableFrom(col.DataAccessor.ValueType))
				{
					if (!col.ControlSettings.ContainsKey("fileName"))
						col.ControlSettings["fileName"] = new LambdaDynamicParameter<string>(o => (oldDa.GetValue(o) as IWebFile)?.FileName);

					if (!col.ControlSettings.ContainsKey("exists"))
						col.ControlSettings["exists"] = new LambdaDynamicParameter<bool>(o => oldDa.GetValue(o) is IWebFile);
				}

				col.ControlSettings["url"] = new DynamicParameter(MvcHelper.Url("/booster/grid/filedownload"));
				col.ControlSettings["uploadUrl"] = new DynamicParameter(MvcHelper.Url("/booster/grid/fileupload"));

				col.DataAccessor = new LambdaDataAccessor(typeof (string),
					o => oldDa.GetValue(o).With(() => Guid.NewGuid().ToString()),
					(o, v) =>
					{
						if ((v as string).IsEmpty())
						{
							oldDa.SetValue(o, null);
							return true;
						}

						var session = MvcHelper.Session;
						var key = $"GridFile.{Key}.{GetKey(o)}.{colName}.{v}";
						if (session?[key] is IWebFile file)
						{
							oldDa.SetValue(o, file);
							session.Remove(key);
							return true;
						}
						return false;
					});
			}

		return res;
	}

	protected BaseGridController(TypeEx entityType, string key, ILog log)
	{
		var ifc = entityType.FindGenericInterfaces(typeof(IGridDecarator<>)).FirstOrDefault();
		if (ifc != null)
		{
			IsDecorator = true;
			DecoratorType = entityType;
			EntityType = ifc.GenericArguments[0];
		}
		else
			EntityType = DecoratorType = entityType;

		// ReSharper disable once VirtualMemberCallInConstructor
		Key = key;

		if (DecoratorType.TryFindAttribute(out GridModelAttribute attr))
			SyncType = attr.SyncType;

		Log = log.Create(this, new {DecoratorType, key});
	}

	protected virtual object Decorate(object entity)
	{
		if (entity == null)
			return null;

		if (IsDecorator)
		{
			var result = (IGridDecarator)DecoratorType.Create();
			result.Target = entity;
			return result;
		}

		return entity;
	}

	protected virtual object Undecorate(object decorator)
	{
		if (decorator == null)
			return null;

		if (IsDecorator && decorator is IGridDecarator dec)
			return dec.Target;

		return decorator;
	}

	protected virtual Task<QueryBuilder> GetCondition(GridRequest request)
		=> Task.FromResult(new QueryBuilder());

	protected virtual async Task<object> CreateEntity(GridRequest request)
	{
		var entity =  Activator.CreateInstance(EntityType);
		var decorator = Decorate(entity);
			
		if (NewEntryMethod != null)
			await NewEntryMethod.InvokeAsync(decorator, 
				GetMethodArgsFromForm(request, null, request.Form, NewEntryMethod.Parameters).ToArray()).NoCtx();

		return decorator;
	}

	protected virtual Task<object> CreateEntityForSecurityCheck(GridRequest request)
	{
		try
		{
			request.IsSecurityCheck = true;
			return CreateEntity(request);
		}
		finally
		{
			request.IsSecurityCheck = false;
			(request.SecurityCheckContext as IDisposable)?.Dispose();
		}
	}

	protected virtual async Task AddRow(GridRequest request, HashSet<string> hiddenColumns, GridResponse response, object entity, string key, int number, IGridSelectionState selectionState)
	{
		var row = new GridRow { Key = key };

		if (HasRowPermissions)
		{
			var oldSeqCheck = request.IsSecurityCheck;
			try
			{
				request.IsSecurityCheck = true;

				var perm = await GetPermissions(request, entity);

				var allowDelete = perm.HasFlag(GridPermissions.Delete);
				if (allowDelete != response.Permissions.HasFlag(GridPermissions.Delete))
					row.AllowDelete = allowDelete;

				var allowWrite = perm.HasFlag(GridPermissions.Write);
				if (allowWrite != response.Permissions.HasFlag(GridPermissions.Write))
					row.AllowWrite = allowWrite;

				if (selectionState != null)
				{
					var allowSelect = perm.HasFlag(GridPermissions.Select);
					if (allowSelect != response.Permissions.HasFlag(GridPermissions.Select))
						row.AllowSelect = allowSelect;
				}
			}
			finally
			{
				request.IsSecurityCheck = oldSeqCheck;
			}
		}

		if (selectionState != null)
			row.Selected = selectionState[key];

		if (response.EditUrlFormat == null && response.GridSchema.EditUrl != null)
			row.EditUrl = response.GridSchema.EditUrl.Evaluate(entity);

		if (response.GridSchema.OverrideRowControls != null)
			row.OverrideControls = response.GridSchema.OverrideRowControls.Evaluate(entity);

		switch (RowControlsMember)
		{
		case Property prop:
			row.Controls = prop.GetValue(entity);
			break;
		case Method mtd:
			row.Controls = await mtd.InvokeAsync(entity);
			break;

		default:
			if (response.GridSchema.RowControls != null)
				row.Controls = response.GridSchema.RowControls.Evaluate(entity);
			break;
		}

		foreach (var col in response.GridSchema.Columns.Where(col => !hiddenColumns.Contains(col.Name))) 
			row.AddCell(new GridCell(col.Name, col.IsRowNumberColumn 
				? number.ToString() 
				: FormatCellText(col, entity)));

		response.AddRow(row);
	}

	protected virtual string FormatCellText(IEditorDataSchema col, object entity)
	{
		var value = col.DataAccessor.GetValue(entity);
		if (value != null)
			switch (value)
			{
			case Enum: //TODO: где указан этот empty?
				return EnumHelper.ToString(value, 
					col.GetSetting(entity, "separator") as string ?? ", ", 
					col.GetSetting(entity, "empty") as string ?? 
					col.GetSetting(entity, "emptyText") as string);
				
			case IEmptyState {IsEmpty: true} when col.GetSetting(entity, "emptyText") is string emptyText:
				return emptyText;
				
			default:
				var format = col.GetSetting(entity, "format") as string;
				var str = StringConverter.Default.ToString(value, format);
				var selectValues = col.SelectValues;
				if (selectValues != null)
				{
					var obj = selectValues.SafeGet(str) ?? str;
					return StringConverter.Default.ToString(obj, format);
				}

				return str;
			}

		return col.GetSetting(entity, "emptyText") as string;
	}


	public virtual async Task<object> GetNew(GridRequest request)
	{
		var entity = await CreateEntity(request).NoCtx();
		if (entity == null)
			return HttpStatusHelper.NotFound("Fail to create new entity");

		var errResponse = await CheckPermissions(request, entity, GridPermissions.Insert).NoCtx();
		if (errResponse != null)
			return errResponse;

		return new GridResponse(Version)
		{
			ModelSchema = new ModelSchema(WriteSchema, entity),
			//Permissions = permissions,
			Caption = ReadSchema.InsertText.With(t => t.Evaluate(entity) as string)
		};
	}

	public virtual async Task<object> GetEdit(GridRequest request)
	{
		var entity = await GetEntity(request.Key, true);
		if (entity == null)
			return HttpStatusHelper.NotFound($"Fail to retrive entity {request.Key}");

		var errResponse = await CheckPermissions(request, entity, GridPermissions.Write).NoCtx();
		if (errResponse != null)
			return errResponse;
			
		return new GridResponse(Version)
		{
			ModelSchema = new ModelSchema(WriteSchema, entity),
			//Permissions = permissions,
			Caption = ReadSchema.EditText.With(t => t.Evaluate(entity) as string)
		};
	}

	public virtual async Task<object> Save(GridRequest request)
	{
		var isUpdating = request.Key != null;
		var entity = isUpdating ? await GetEntity(request.Key, true) : await CreateEntity(request);

		if (entity == null)
			return HttpStatusHelper.NotFound($"Fail to retrive or to create entity {request.Key}");

		var errResponse = await CheckPermissions(request, entity, GridPermissions.Write).NoCtx();
		if (errResponse != null)
			return errResponse;
			
		var response = new ValidationResponse();

		object state = null;
		try
		{
			state = SaveState(entity);

			var schema = new ModelSchema(WriteSchema, entity);
			var newValues = await ValidateEntity(schema, request, response, entity).NoCtx();

			if (response.IsOk)
			{
				var applyTask = newValues
					.Select(pair => Tuple.Create(schema.GetColumn(pair.Key, false), pair.Value))
					.Where(tup => tup.Item1 != null)
					.ToDictionary(tup => tup.Item1, tup => tup.Item2);				
					
				EntityChanges changes = null;

				if (ShouldCreateEntityChanges)
				{
					changes = (EntityChanges) Activator.CreateInstance(EntityChangesType);
					// ReSharper disable once PossibleNullReferenceException cовершенно непонятно, как активатор может вернуть нал
					changes.Action = isUpdating ? GridActions.Update : GridActions.Insert;
				
					foreach (var pair in applyTask)
						changes.Set(pair.Key.Name, pair.Key.DataAccessor.CanRead ? pair.Key.DataAccessor.GetValue(entity) : null, pair.Value);

					await InvokeApplyMethod(GridChangesApplyStage.BeforeApply, entity, changes).NoCtx();
				}
						
				foreach (var pair in applyTask)
					pair.Key.DataAccessor.SetValue(entity, pair.Value);

				if (changes != null)
				{
					changes.IsApplied = true;
						
					await InvokeApplyMethod(GridChangesApplyStage.BeforeSave, entity, changes).NoCtx();
				}

				await Save(request, entity).NoCtx();
					
				if (changes != null)
				{
					changes.IsSaved = true;
					await InvokeApplyMethod(GridChangesApplyStage.AfterSave, entity, changes).NoCtx();
				}

				if (SyncType == GridSyncType.Actions)
					UpdateVersion();
			}
		}
		catch (Exception ex)
		{
			Utils.Nop(ex);
			if (state != null)
				RestoreState(entity, state);

			throw;
		}

		return new GridResponse(Version) { ValidationResponse = response };
	}

	async Task InvokeApplyMethod(GridChangesApplyStage stage, object entity, EntityChanges entityChanges)
	{
		foreach(var method in ChangesApplyingMethods)
			if (method != null && method.FindAttributes<GridChangesAppliedAttribute>().Any(a => a.ShouldInvoke(stage, entityChanges)))
				await method.InvokeAsync(entity, entityChanges).NoCtx();
	}
		
	public async Task<object> Reorder(GridRequest request)
	{
		var entity = (IReorderable) await GetEntity(request.Key, false);
		var neib = (IReorderable) await GetEntity(request.Neib, false);

		if (entity == null || neib == null)
			return HttpStatusHelper.NotFound($"Fail to retrive entities {request.Key}, {request.Neib}");

		var errResponse = await CheckPermissions(request, Decorate(entity), GridPermissions.Reorder).NoCtx();
		if (errResponse != null)
			return errResponse;

		if (!EntityEquals(neib, entity))
		{
			var after = request.After ? neib : await GetPreviousEntity(neib).NoCtx();

			if (after == null || ! EntityEquals(after, entity))
			{
				var collection = await GetCollectionForReorder(entity, after).NoCtx();

				await Reorder(collection, entity, after).NoCtx();

				if (SyncType == GridSyncType.Actions)
					UpdateVersion();
			}
		}
		return new GridResponse(Version);
	}

	public async Task<object> UserSelect(GridRequest request)
	{
		var errResponse = await CheckPermissions(request, null, GridPermissions.Select).NoCtx();
		if (errResponse != null)
			return errResponse;
			
		var state = GetSelectionState();
		if(state == null) 
			return HttpStatusHelper.NotFound("Fail to retrive user selection state");

		state.AllKeys = (await GetAllKeys(request).NoCtx()).Select(k => k.ToString());

		if (request.Data.TryGetValue("all", out var value))
			state.All = value as string == "true";
		else
			foreach (var pair in request.Data)
				state[pair.Key] = pair.Value as string == "true";
			
		return new GridResponse(Version) {TotalRows = state.Count, AllSelected = state.All};
	}


	public virtual IAsyncEnumerable<object> GetUserSelection(string sesKey = null)
	{
		var state = GetSelectionState(sesKey);
		return state != null 
			? GetEntities(state.SelectedKeys, true) 
			: AsyncEnumerable.Empty<object>();
	}

	protected virtual IAsyncEnumerable<object> GetEntities(IEnumerable<string> keys, bool decorate)
		=> (keys ?? Enumerable.Empty<string>()).SelectAsync(async e => await GetEntity(e, decorate).NoCtx());

	public async Task<object> FileDownload(GridRequest request)
	{
		var entity = await GetEntity(request.Key, true) ?? await CreateEntity(request);
		if (entity == null)
			return HttpStatusHelper.NotFound($"Fail to retrive entity {request.Key}");
			
		var errResponse = await CheckPermissions(request, entity, GridPermissions.Read).NoCtx();
		if (errResponse != null)
			return errResponse;
			
		if (!request.Custom.TryGetValue("field", out var field))
			return HttpStatusHelper.NotFound("Field not found");

		if (request.Ses != null)
			if (MvcHelper.Session![$"GridFile.{Key}.{GetKey(entity)}.{field}.{request.Ses}"] is IWebFile sesFile)
				return sesFile;

		if (_fileDataAccessors == null)
			GetWriteSchema();

		// ReSharper disable once PossibleNullReferenceException
		if (!_fileDataAccessors.TryGetValue((string) field, out var da))
			return HttpStatusHelper.NotFound($"Fail to retrive data accessor for {field}");

		return (IWebFile) da.GetValue(entity);
	}

#if !NETSTANDARD && !NETCOREAPP	
	public async Task<object> FileUpload(GridRequest request, HttpPostedFile postedFile) //TODO: Grid.FileUpload std implementation
	{
		object entity = null;

		if (request.Key.IsSome() && request.Key != "0")
		{
			entity = await GetEntity(request.Key, true);
			if (entity == null)
				return HttpStatusHelper.NotFound($"Fail to retrive entity {request.Key}");
		}
		else if (CheckInsertPermissionWithNewEntity)
			entity = CreateEntityForSecurityCheck(request);

		var errResponse = await CheckPermissions(request, entity, GridPermissions.Write).NoCtx();
		if (errResponse != null)
			return errResponse;

		if (!request.Custom.TryGetValue("field", out var field))
			return HttpStatusHelper.NotFound("Field not found");
			
		if (_fileDataAccessors == null)
			GetWriteSchema();

		// ReSharper disable once PossibleNullReferenceException
		if (!_fileDataAccessors.TryGetValue((string) field, out var da))
			return HttpStatusHelper.NotFound($"Fail to retrive data accessor for {field}");

		if (postedFile == null)
			return new {fileName = (string) null, ses = (string) null};


		object file;

		var fileName = Path.GetFileName(postedFile.FileName);

		if (da.ValueType == typeof (IWebFile))
			file = new WebFile(postedFile);
		else if (da.ValueType == typeof (HttpPostedFile))
			file = postedFile;
		else if (da.ValueType == typeof (byte[]))
		{
			var bytes = new byte[postedFile.ContentLength];
			await postedFile.InputStream.ReadAsync(bytes, 0, postedFile.ContentLength).NoCtx();
			file = bytes;
		}
		else if (typeof (IWebFile).IsAssignableFrom(da.ValueType))
		{
			var webfile = (IWebFile) da.ValueType.Create();
			webfile.FileName = Path.GetFileName(postedFile.FileName);
			webfile.ContentType = postedFile.ContentType;
			webfile.Data = new byte[postedFile.ContentLength];
			await postedFile.InputStream.ReadAsync(webfile.Data, 0, postedFile.ContentLength).NoCtx();
			file = webfile;
		}
		else
			throw new Exception($"Could not convert HttpPostedFile to a {da.ValueType}");

		var ses = Guid.NewGuid();
		MvcHelper.Session[$"GridFile.{Key}.{GetKey(entity, true)}.{field}.{ses}"] = file;
		return new {fileName, ses};
	}
#endif
	public virtual void OnError(string action, GridRequest request, Exception ex)
	{
		if (Log != null)
		{
			var req = $"Action: {action}, GridRequest: {{{JsonConvert.SerializeObject(request, Formatting.Indented)}}}";
			Log.Create(new {Url = MvcHelper.DisplayUri}).Error(ex, $"{req}:{ex.Message}"); //TODO: msg parameters
		}
	}

	protected virtual bool EntityEquals(object a, object b)
		=> Equals(a, b);

	protected virtual Task Reorder(IAsyncEnumerableIReorderable collection, IReorderable entity, IReorderable after)
		=> Reorderer.ReorderAfter(collection, entity, after);

	protected virtual object SaveState(object entity)
		=> null;

	protected virtual void RestoreState(object entity, object state)
	{
	}

	protected virtual async Task<Dictionary<string, object>> ValidateEntity(ModelSchema schema, GridRequest request, IValidationResponse response, object entity)
	{
		var newValues = new Dictionary<string, object>();
		foreach (var col in schema.Columns)
			try
			{
				var required = col.GetSetting<bool>(entity, "required");

				if (request.Data.TryGetValue(col.Name, out var val))
				{
					val = DealWithArrays(val, col.DataAccessor.ValueType);
					var format = (string) col.GetSetting(entity, "format");
					var needReformat = format.IsSome() || !Equals(TypeEx.Of(val), col.DataAccessor.ValueType);
					if (required || needReformat)
					{
						var svalue = ModelSchema.StringConverter.ToString(val, format);
						if (required && svalue.IsEmpty())
						{
							response.Error(col.Name, col.GetSetting(entity, "requiredText", "Поле является обязательным для заполнения"));
							continue;
						}

						if (needReformat)
							val = StringConverter.Default.ToObject(svalue, col.DataAccessor.ValueType, format);
					}

					if (col.GetSetting<bool>(entity, "requiredNonDefault") && Equals(val, col.DataAccessor.ValueType.DefaultValue))
					{
						response.Error(col.Name, col.GetSetting(entity, "requiredText", "Поле является обязательным для заполнения"));
						continue;
					}

					ValidateLength(response, col, val);
					newValues[col.Name] = val;
				}
				else if (required)
					response.Error(col.Name, col.GetSetting(entity, "requiredText", "Поле является обязательным для заполнения"));
			}
			catch (Exception ex)
			{
				response.Error(col.Name, ex.Message);
			}

		if (ValidationMethod != null)
		{
			var parameters = ValidationMethod.Parameters;
			var args = GetMethodArgsFromForm(request, null, newValues, parameters.Skip(1)).AppendBefore(response).ToArray();
			await ValidationMethod.InvokeAsync(ValidationMethod.IsStatic ? null : entity, args).NoCtx();

			Dictionary<string, string> names = null;
			for (var i = 1; i < parameters.Length; i ++)
			{
				var param = parameters[i];
				if (!param.ParameterType.IsByRef && !param.IsOut) 
					continue;
					
				names ??= newValues.ToDictionary(p => p.Key, p => p.Key, StringComparer.OrdinalIgnoreCase);

				if (names.TryGetValue(param.Name, out var nm))
					newValues[nm] = args[i];
			}
		}
		else
		{
#pragma warning disable 618
			var validatable = entity as IValidatable;
#pragma warning restore 618
			validatable?.Validate(newValues, response);
		}
		return newValues;
	}

	protected virtual void ValidateLength(IValidationResponse validationResponse, IEditorDataSchema column, object value)
	{
	}


	static Method _jarrayToObjectMi;
	protected static object DealWithArrays(object value, TypeEx typeTo)
	{
		switch (value)
		{
		case null:
			return typeTo.DefaultValue;
		case JArray when typeTo == typeof(JArray) || typeTo == typeof(object):
			return value;
		case JArray jarr when typeTo.IsArray:

			_jarrayToObjectMi ??= TypeEx.Get<JArray>().FindMethod(new ReflectionFilter("ToObject") {ParametersCount = 0});

			return _jarrayToObjectMi!.MakeGenericMethod(typeTo).Inner.Invoke(jarr, Array.Empty<object>()); //TODO: инвокатор почему-то не работает с ToObject<string[]>()

		case JArray jarr when typeTo.IsEnum:
			return Enum.Parse(typeTo, jarr.ToString(new EnumBuilder(",")));
		case JArray jarr:
			return $";{_stringEscaper.Join(jarr.Select(t => t.ToString()), ";")};";
		}

		if (typeTo.IsArray)
			if (value is string str)
			{
				if (StringConverter.Default.TryParse(str, typeTo.ArrayItemType, out var elem))
				{
					var arr = Array.CreateInstance(typeTo.ArrayItemType!, 1);
					arr.SetValue(elem, 0);
					return arr;
				}
				return Array.CreateInstance(typeTo.ArrayItemType!, 0);
			}

		return value;
	}
		
	protected static IEnumerable<object> GetMethodArgsFromForm(
		GridRequest request, 
		QueryBuilder queryBuilder,  
		IDictionary<string,object> values, 
		IEnumerable<ParameterInfo> parameters)
	{
		if (values == null)
		{
			foreach (var param in parameters)
				if (param.ParameterType == typeof(GridRequest))
					yield return request;
				else if (param.ParameterType == typeof(QueryBuilder))
					yield return queryBuilder;
				else
					yield return TypeEx.Get(param.ParameterType).DefaultValue; //TODO: parameterInfo

			yield break;
		}

		var dict = new Dictionary<string, object>(values, StringComparer.OrdinalIgnoreCase);
		foreach (var param in parameters)
		{
			if (param.ParameterType == typeof (GridRequest))
			{
				yield return request;
				continue;
			}
				
			if (queryBuilder != null && param.ParameterType == typeof (QueryBuilder))
			{
				yield return queryBuilder;
				continue;
			}

			if(dict.TryGetValue(param.Name ?? "", out var val))
			{
				var to = param.ParameterType;

				if (to.IsByRef)
					to = to.GetElementType();

				// ReSharper disable once PossibleNullReferenceException
				if (to.IsGenericType && to.GetGenericTypeDefinition() == typeof (Nullable<>))
					to = to.GetGenericArguments().FirstOrDefault();

				val = DealWithArrays(val, to);

				TypeEx from = val?.GetType() ?? to;
				if (from != to)
					val = from.TryCast(val, to!, out var res)
						? res
						: StringConverter.Default.ToObject(StringConverter.Default.ToString(val), to);

				yield return val;
			}
			else
				yield return TypeEx.Get(param.ParameterType).DefaultValue;
		}
	}

	public virtual async Task<object> Delete(GridRequest request)
	{
		var entity = await GetEntity(request.Key, true);

		var errResponse = await CheckPermissions(request, entity, GridPermissions.Delete).NoCtx();
		if (errResponse != null)
			return errResponse;
			
		EntityChanges changes = null;

		if (ShouldCreateEntityChanges)
		{
			changes = (EntityChanges) Activator.CreateInstance(EntityChangesType);
			// ReSharper disable once PossibleNullReferenceException совершенно непонятно, как активатор может вернуть нал
			changes.Action = GridActions.Delete;

			await InvokeApplyMethod(GridChangesApplyStage.BeforeApply, entity, changes).NoCtx();
			await InvokeApplyMethod(GridChangesApplyStage.BeforeSave, entity, changes).NoCtx();
		}

			
		if(DeleteEntryMethod != null)
			await DeleteEntryMethod.InvokeAsync(Decorate(entity), 
				GetMethodArgsFromForm(request, null, request.Form, DeleteEntryMethod.Parameters).ToArray()).NoCtx();
		else
			await Delete(entity).NoCtx();

		if (changes != null)
			await InvokeApplyMethod(GridChangesApplyStage.AfterSave, entity, changes).NoCtx();

		if (SyncType == GridSyncType.Actions)
			UpdateVersion();

		return new GridResponse(Version);
	}

	protected void UpdateVersion()
		=> Version = Guid.NewGuid();

	public async Task<object> Sync(GridRequest request)
	{
		var errResponse = await CheckPermissions(request, null, GridPermissions.Select).NoCtx();
		return errResponse ?? Version;
	}

	public virtual async Task<bool> CheckColumnPermissions(GridRequest request, IEditorDataSchema column, GridPermissions permissions)
	{
		var actual = GridPermissions.None;
		if (GridPermissionsProvider != null)
			actual = await GridPermissionsProvider.GetPermissions(this, column, permissions).NoCtx();
			
		return actual == permissions;
	}

	public virtual async Task<object> CheckPermissions(GridRequest request, object entity, GridPermissions permissions)
	{
		if (GridPermissionsProvider != null)
		{
			var actual = await GridPermissionsProvider.GetPermissions(this, entity, permissions).NoCtx();
			if (actual != permissions)
				return HttpStatusHelper.Forbidden($"Required permission(s) ({permissions})");
		}
		else
		{
			var actual = await GetPermissions(request, entity).NoCtx();
			if (!actual.HasFlag(permissions))
				return HttpStatusHelper.Forbidden($"Required permission(s) ({permissions})");
		}
		return null;
	}

	public virtual Task<GridPermissions> GetPermissions(GridRequest request, object entity)
		=> GridPermissionsProvider != null 
			? GridPermissionsProvider.GetPermissions(this, entity) 
			: Task.FromResult(GridPermissions.All);

	public abstract Task<object> Select(GridRequest request);
	public abstract Task<HashSet<object>> GetAllKeys(GridRequest request);
	protected abstract string GetKey(object entity, bool allowNull = false);
	protected abstract Task Delete(object entity);
	protected abstract Task Save(GridRequest request, object entity);
	protected abstract Task<object> GetEntity(string byKey, bool decorate);
	protected abstract Task<IAsyncEnumerableIReorderable> GetCollectionForReorder(IReorderable entity, IReorderable after);
	protected abstract Task<IReorderable> GetPreviousEntity(IReorderable entity);
}