﻿using System;
using System.Threading.Tasks;
using Booster.Mvc.Grid.Interfaces;
using Booster.Mvc.Helpers;
using JetBrains.Annotations;
#if NETSTANDARD	|| NETCOREAPP
using System.Web;
using Microsoft.AspNetCore.Mvc;

#else
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.SessionState;

using Booster.Mvc.Declarations;

using HttpGet = System.Web.Http.HttpGetAttribute;
using HttpPost = System.Web.Http.HttpPostAttribute;
#endif
#if CONCURRENCY_DEBUG
using Booster.Debug;
#endif

namespace Booster.Mvc.Grid.Kitchen
{
	[UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
#if !NETSTANDARD && !NETCOREAPP
	[SessionState(SessionStateBehavior.Required)]
#endif
	public class GridApiController : ApiController
	{
		[HttpGet, HttpPost]
		public Task<object> Select(GridRequest request)
			=> Exe(request, "Select", c => c.Select(request));
		
		[HttpGet, HttpPost]
		public Task<object> GetNew(GridRequest request)
			=> Exe(request, "GetNew", c => c.GetNew(request));

		[HttpGet, HttpPost]
		public Task<object> GetEdit(GridRequest request)
			=> Exe(request, "GetEdit", c => c.GetEdit(request));

		[HttpGet, HttpPost]
		public Task<object> Save(GridRequest request)
			=> Exe(request, "Save", c => c.Save(request));

		[HttpGet, HttpPost]
		public Task<object> Reorder(GridRequest request)
			=> Exe(request, "Reorder", c => c.Reorder(request));

		[HttpGet, HttpPost]
		public Task<object> Delete(GridRequest request)
			=> Exe(request, "Delete", c => c.Delete(request));

		[HttpGet, HttpPost]
		public Task<object> UserSelect(GridRequest request) 
			=> Exe(request, "UserSelect", c => c.UserSelect(request));

		[HttpGet, HttpPost]
		public Task<object> Sync(GridRequest request)
			=> Exe(request, "Sync", c => c.Sync(request));

#if !NETSTANDARD && !NETCOREAPP //TODO: FileDownload / upload with NETSTANDARD
		[HttpGet, HttpPost]
		public async Task<object> FileDownload(GridRequest request)
		{
			var response = await Exe(request, "FileDownload", c => c.FileDownload(request)).NoCtx();
			if (response is HttpStatusCodeResult err)
				return err;
			if (response is not IWebFile file)
				return HttpStatusHelper.NotFound($"A file for {request.Controller}/{request.Key} was not found");

			var result = new HttpResponseMessage(HttpStatusCode.OK)
			{
				Content = new ByteArrayContent(file.Data)
			};

			string contentDisposition;
			var httpRequest = MvcHelper.HttpContext.Request;
			if (httpRequest.Browser.Browser == "IE" && (httpRequest.Browser.Version == "7.0" || httpRequest.Browser.Version == "8.0"))
				contentDisposition = $"attachment; filename={EscapeFileName(file.FileName)}";
			else if (httpRequest.UserAgent != null && httpRequest.UserAgent.ToLowerInvariant().Contains("android")) // android built-in download manager (all browsers on android)
				contentDisposition = $"attachment; filename=\"{MakeAndroidSafeFileName(file.FileName)}\"";
			else
				contentDisposition = $"attachment; filename=\"{file.FileName}\"; filename*=UTF-8''{EscapeFileName(file.FileName)}";
	
			result.Content.Headers.Add("Content-Disposition", contentDisposition);
			result.Content.Headers.ContentType = new MediaTypeHeaderValue(file.ContentType);
			return result;
		}
	
		[HttpGet]
		public object Img(string ctrl, string field, string id) //TODO: не совсем понятно где это может быть использованно (/booster/grid/img), а так можно было бы удалить
			=> FileDownload(new GridRequest
			{
				Controller = ctrl,
				Key = id,
				Custom = new Dictionary<string, object> {{"field", field}}
			});

		[HttpPost]
		public object FileUpload(GridRequest request)
		{
			var httpRequest = MvcHelper.HttpContext.Request;
			var file = httpRequest.Files.Count > 0 ? httpRequest.Files[0] : null;
			return Exe(request, "FileUpload", c => c.FileUpload(request, file));
		}
	
		string EscapeFileName(string fileName)
		{
			var res = Uri.EscapeDataString(fileName);
			return res.Replace("(", "%28").Replace(")", "%29");
		}

		static readonly HashSet<char> _androidAllowedChars = new("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ._-+,@£$€!½§~'=()[]{}0123456789".ToCharArray());
		string MakeAndroidSafeFileName(string fileName)
		{
			var res = new StringBuilder();

			foreach (var c in fileName)
				res.Append(_androidAllowedChars.Contains(c) ? c : '_');

			return res.ToString();
		}
#endif
		async Task<object> Exe(GridRequest request, string action, Func<IGridController, Task<object>> proc)
		{
#if CONCURRENCY_DEBUG
			TaskInfo.Init($"Grid api: {request.Controller}.{action}");
#endif
#if !NETSTANDARD && !NETCOREAPP
			MvcHelper.OnNewRequest();
#endif

			var controller = Grid.GetController(request.Controller);
			if (controller == null)
				return HttpStatusHelper.NotFound($"A controller {request.Controller} was not found");
				
			using (Grid.Push(request, controller))
			{
#if !DEBUG
				try
#else
				Utils.Nop(action);
#endif
				{
					return HttpStatusHelper.ToApiResult( await proc(controller).NoCtx(), this);
				}
#if !DEBUG
				catch (Exception ex)
				{
					controller.OnError(action, request, ex);
					throw;
				}
#endif
			}
		}
		
		object Exe(GridRequest request, string action, Func<IGridController, object> proc)
		{
#if CONCURRENCY_DEBUG			
			TaskInfo.Init($"Grid api: {request.Controller}.{action}");
#endif			
			var controller = Grid.GetController(request.Controller);
			if (controller == null)
				return HttpStatusHelper.NotFound($"A controller {request.Controller} was not found");

			using (Grid.Push(request, controller))
			{
#if !DEBUG
				try
#else
				Utils.Nop(action);
#endif
				{
					return HttpStatusHelper.ToApiResult( proc(controller), this);
				}
#if !DEBUG
				catch (Exception ex)
				{
					controller.OnError(action, request, ex);
					throw;
				}
#endif
			}
		}
	}
}