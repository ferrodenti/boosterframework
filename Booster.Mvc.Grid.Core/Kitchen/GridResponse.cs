﻿using System;
using System.Collections.Generic;
using Booster.Interfaces;
using Booster.Mvc.Editor.Interfaces;
using Booster.Mvc.Grid.Interfaces;
using Booster.Mvc.Grid.Orm;
using Newtonsoft.Json;

namespace Booster.Mvc.Grid.Kitchen;

public class GridResponse : IGridResponse
{
	public object Schema { get; protected set; }

	public IGridSchema GridSchema
	{
		get => Schema as IGridSchema;
		set => Schema = value;
	}
	public IModelSchema ModelSchema
	{
		get => Schema as IModelSchema;
		set => Schema = value;
	}

	public string Caption { get; set; }

	[JsonIgnore]
	public GridPermissions Permissions { get; set; }

	public string PageTitle { get; set; }

	public bool AllowWrite => Permissions.HasFlag(GridPermissions.Write);
	public bool AllowDelete => Permissions.HasFlag(GridPermissions.Delete);
	public bool AllowInsert => Permissions.HasFlag(GridPermissions.Insert);
	public bool AllowReorder => Permissions.HasFlag(GridPermissions.Reorder);
	public bool AllowSelect => Permissions.HasFlag(GridPermissions.Select);

	public object AllSelected { get; set; }

	public object SelectedRows { get; set; }

	public ICollection<IGridRow> Rows { get; }

	public string SortColumn { get; set; }
	public string SortDir { get; set; }
	public long TotalRows { get; set; }
	public string EditUrlFormat { get; set; }
	public string InsertUrl { get; set; }
	public bool EditorNewTab { get; set; }
	public Guid Version { get; set; }

	public IValidationResponse ValidationResponse { get; set; }

	public void AddRow(IGridRow row)
		=> Rows.Add(row);

	public GridResponse(Guid version)
	{
		Version = version;
		Rows = new List<IGridRow>();
	}
}