﻿using Booster.Evaluation;
using Booster.Mvc.Declarations;
using Booster.Mvc.Editor;
using Booster.Mvc.Editor.Interfaces;
using Booster.Reflection;

namespace Booster.Mvc.Grid.Kitchen;

public class GridSchemaReader : EditorSchemaReader
{
	protected override IEditorSchema CreateSchema(TypeEx entityType)
	{
		var schema =  new GridSchema();

		foreach (var attr in entityType.FindAttributes<GridModelAttribute>())
		{
			if (attr.Caption != null)
				schema.Caption = attr.Caption;
			if (attr.ShortName != null)
				schema.ShortName = attr.ShortName;
			if (attr.DeleteText != null)
				schema.DeleteText = new DynamicParameter(entityType, attr.DeleteText);
			if (attr.InsertText != null)
				schema.InsertText = new DynamicParameter(entityType, attr.InsertText);
			if (attr.EditText != null)
				schema.EditText = new DynamicParameter(entityType, attr.EditText);
		}

		foreach (var attr in entityType.FindAttributes<GridEditUrlAttribute>())
		{
			schema.EditUrl = new DynamicParameter(entityType, attr.EditUrl);

			if (attr.InsertUrl.IsSome())
				schema.InsertUrl = new DynamicParameter(entityType, attr.InsertUrl);

			schema.EditorNewTab = attr.NewTab;
		}

		foreach (var attr in entityType.FindAttributes<GridRowControlsAttribute>())
		{
			schema.OverrideRowControls = attr.Override is string str
				? new DynamicParameter(entityType, str)
				: new DynamicParameter(attr.Override);

			schema.RowControls = new DynamicParameter(entityType, attr.Value);
		}
		if (schema.Caption == null && schema.ShortName != null)
			schema.Caption = schema.ShortName;
		else if (schema.ShortName == null && schema.Caption != null)
			schema.ShortName = schema.Caption;
		else if (schema.Caption == null && schema.ShortName == null)
			schema.Caption = schema.ShortName = entityType.Name;

		return schema;
	}

	protected override IEditorDataSchema CreateDataSchema(TypeEx entityType, BaseDataMember dataMemberMetadata, bool write)
	{
		var attrs = dataMemberMetadata.FindAttributes<GridAttribute>().AsArray();
		if (!write && attrs.Length == 0)
			return null;
			
		var schema = new GridDataSchema {DataAccessor = dataMemberMetadata, ModelType = entityType};

		object displayEditor = true;
		object displayColumn = true;
					
		foreach (var attr in attrs)
		{
			if (attr.DisplayEditor != null)
				displayEditor = attr.DisplayEditor;

			if (attr.DisplayColumn != null)
				displayColumn = attr.DisplayColumn;

			if (displayEditor is string s)
				schema.AddSetting("display", s);

			schema.Sortable = attr.Sortable;

			if (attr.DisplayName != null)
				schema.AddSetting("displayName", attr.DisplayName);

			if (attr.Order != 0)
				schema.Order = attr.Order;

			if (attr.SortExpression != null)
				schema.SortExpression = attr.SortExpression;

			schema.PreserveHtml = attr.PreserveHtml;

			if (attr is GridRowNumberAttribute)
				schema.IsRowNumberColumn = true;
		}
			
		if(!schema.ControlSettings.ContainsKey("displayName"))
			schema.AddSetting("displayName", dataMemberMetadata.Name);
			
		if (write && Equals(displayEditor, false) ||
		    !write && Equals(displayColumn, false))
			return null;

		return schema;
	}
}