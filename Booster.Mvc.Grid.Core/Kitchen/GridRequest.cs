using System;
using System.Collections.Generic;
using Booster.Mvc.ModelBinders;
using Booster.Orm;
using Newtonsoft.Json;

// ReSharper disable CollectionNeverUpdated.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Booster.Mvc.Grid.Kitchen;

#if NETSTANDARD	|| NETCOREAPP
using Microsoft.AspNetCore.Mvc;
#else
using System.Web.Mvc;
using ModelBinder2 = System.Web.Http.ModelBinding.ModelBinderAttribute;
	
[ModelBinder2(typeof(GetPostJsonBinder))]
[ValidateInput(false)]
#endif
[Serializable]
[ModelBinder(typeof(GetPostJsonBinder))]
public class GridRequest
{
	public string Controller { get; set; }
	public string Key { get; set; }
	public int Skip { get; set; }
	public int Take { get; set; }
	public string Neib { get; set; }
	public bool After { get; set; }
	public string Sort { get; set; }
	public OrderType SortDir { get; set; }
	public Dictionary<string, object> Form { get; set; }
	public Dictionary<string, object> Data { get; set; }
	public Dictionary<string, object> Custom { get; set; }
	public string[] HiddenColumns { get; set; }
	public string Context { get; set; }
	public string Ses { get; set; }

	[JsonIgnore] public bool IsSecurityCheck { get; set; }
	[JsonIgnore] public object SecurityCheckContext { get; set; }

	public override string ToString()
	{
		var str = new EnumBuilder(" ");
			
		str.Append($"Controller: {Controller}");
			
		if (Key.IsSome())
			str.Append($"Key: {Key}");

		if (Context.IsSome())
			str.Append($"Context: {Key}");

		return str;
	}
}