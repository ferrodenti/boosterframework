using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Reflection;
using Booster.AsyncLinq;
using Booster.DI;
#if NETSTANDARD	|| NETCOREAPP
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Builder;
#else
using System.Web.Http;
using System.Web.Routing;
using System.Web.Mvc;
#endif
using Booster.Interfaces;
using Booster.Log;
using Booster.Mvc.Declarations;
using Booster.Mvc.Grid.Interfaces;
using Booster.Mvc.Grid.Kitchen;
using Booster.Mvc.Helpers;
using Booster.Reflection;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Mvc.Grid
{
	[PublicAPI]
	public static class Grid
	{
		static readonly ConcurrentDictionary<string, IGridController> _controllers = new(StringComparer.OrdinalIgnoreCase);
		static readonly ConcurrentDictionary<TypeEx, string> _keys = new();

		public static string[] Keys => _keys.Values.ToArray();
		
#if NET5_0_OR_GREATER || NETCOREAPP
		public static void MapBoosterGrid(this IRouteBuilder routes)
		{
			routes.MapRoute(
				"Booster.Mvc.Grid.Api",
				"booster/grid/{action}",
				new {Controller = "GridApi"});

			routes.MapRoute(
				"Booster.Mvc.Grid.Api.Img",
				"booster/grid/img/{ctrl}/{field}/{id}.jpg",
				new {Controller = "GridApi", Action = "Img"});
		}

		public static void MapBoosterGrid(this IEndpointRouteBuilder routes)
		{
			routes.MapControllerRoute(
				"Booster.Mvc.Grid.Api",
				"booster/grid/{action}",
				new {Controller = "GridApi"});

			routes.MapControllerRoute(
				"Booster.Mvc.Grid.Api.Img",
				"booster/grid/img/{ctrl}/{field}/{id}.jpg",
				new {Controller = "GridApi", Action = "Img"});
		}
#else
		public static void RegisterRoutes()
		{
			// Почему-то при обфускации не получается передать методу анонимный объект. Какой-то глюк реактора
			var route = RouteTable.Routes.MapHttpRoute("Booster.Mvc.Grid.Api", "booster/grid/{action}");
			route.RouteHandler = new SessionRouteHandler();
			route.Defaults["Controller"] = "GridApi";

			route = RouteTable.Routes.MapHttpRoute("Booster.Mvc.Grid.Api.Img", "booster/grid/img/{ctrl}/{field}/{id}.jpg");
			route.RouteHandler = new SessionRouteHandler();
			route.Defaults["Controller"] = "GridApi";
			route.Defaults["Action"] = "Img";
		}
#endif

		/// <summary>
		/// Текущий объект запроса к гриду. Определен для текущего потока только на момент запроса, иначе null
		/// </summary>
		public static GridRequest? CurrentRequest => _currentRequest.Value;
		static readonly CtxLocal<GridRequest> _currentRequest = new();

		/// <summary>
		/// Текущий контроллер грида. Определен для текущего потока только на момент запроса, иначе null
		/// </summary>
		public static IGridController? CurrentController => _currentController.Value;
		static readonly CtxLocal<IGridController> _currentController = new();

#if NETSTANDARD || NETCOREAPP
		public static IUrlHelper? UrlHelper => _urlHelper.Value;
		static readonly CtxLocal<IUrlHelper> _urlHelper = new(() => MvcHelper.CreateUrlHelper());
#else 
		public static UrlHelper UrlHelper => _urlHelper.Value!;
		static readonly CtxLocal<UrlHelper> _urlHelper = new(() => MvcHelper.CreateUrlHelper());
#endif
		/// <summary>
		/// Получение ключа грида по модели
		/// </summary>
		/// <typeparam name="TGridModel">Тип модели</typeparam>
		/// <returns>Если модель зарегистрирована, то текстовый ключ (по умолчанию это название таблицы), иначе null</returns>
		public static string? KeyOf<TGridModel>()
			=> KeyOf(typeof (TGridModel));

		/// <summary>
		/// Получение ключа грида по модели
		/// </summary>
		/// <param name="type">Тип модели</param>
		/// <returns>Если модель зарегистрирована, то текстовый ключ (по умолчанию это название таблицы), иначе null</returns>
		public static string? KeyOf(TypeEx type)
			=> _keys.SafeGet(type);

		public static void RegisterController(IGridController controller, string? key = null)
		{
			key ??= controller.Key;

			if (!_controllers.TryAdd(key, controller))
				throw new Exception($"Controller with key \"{key}\" already registered");

			if (controller is BaseGridController ctrl)
				_keys[ctrl.DecoratorType] = key;
		}

		static TypeEx GetBasicOrmGridController()
			=> typeof (Orm.BasicOrmGridController);

		public static void RegisterControllers(
			Assembly assembly, 
			ILog? log = null, 
			Func<TypeEx, IGridController>? controllerFactory = null, 
			IGridPermissionsProvider? permissionsProvider = null)
		{
			permissionsProvider ??= new BasicGridPermissionsProvider();

			foreach (TypeEx type in assembly.GetTypes())
			{
				if (type.IsAbstract)
					continue;
				
				var attr = type.FindAttribute<IGridModelAttribute>();
				if (attr == null) 
					continue;
				
				var gridModelAttr = attr as GridModelAttribute;
				IGridController controller;

				if (controllerFactory != null)
				{
					controller = controllerFactory(type);

					if (controller == null)
					{
						log.Warn($"A Controller Factory did not create a controller for the type {type}. Skipping");
						continue;
					}
				}
				else
				{
					var controllerType = gridModelAttr?.ControllerType;
					if (controllerType == null)
					{
						controllerType = GetBasicOrmGridController();
						controller = (IGridController) controllerType.Create(type, log); //TODO: Creator.DefaultParameters
					}
					else
						controller = (IGridController) controllerType.Create();
				}

				

				var key = gridModelAttr?.ControllerKey ?? controller.Key; //TODO: assign attr key to controller

				if (_controllers.ContainsKey(key)) 
					continue;
				
				controller.GridPermissionsProvider = permissionsProvider;
				RegisterController(controller, key);
			}
		}
		public static IGridController? GetController(string key)
			=> _controllers.SafeGet(key);

		public static IGridController? GetController<T>()
			=> KeyOf<T>().With(GetController);

		/// <summary>
		/// Получение выбранных пользователем записей
		/// </summary>
		/// <typeparam name="T">Тип модели</typeparam>
		/// <param name="sesKey">Ключ сессии по которому хранится состояние выборки. 
		/// Null для значения из опрделения модели</param>
		/// <returns>Множество выбранных записей</returns>
		public static IAsyncEnumerable<T>? GetUserSelection<T>(string? sesKey = null)
			=> GetController<T>().With(c => c.GetUserSelection(sesKey).Cast<T>());

		#region internals
		internal static IDisposable Push(GridRequest request, IGridController controller)
		{
			var oldReq = CurrentRequest;
			_currentRequest.Value = request;

			var oldCtrl = CurrentController;
			_currentController.Value = controller;

			return new ActionDisposable(() =>
			{
				_currentRequest.Value = oldReq;
				_currentController.Value = oldCtrl;
			});
		}

		internal static IDataAccessor? DataAccessorProvider(TypeEx _, string? s)
			=> s?.ToLower() switch
			{
				"controller" => new LambdaDataAccessor(typeof(IGridController), _ => CurrentController, null, "controller"),
				"request" => new LambdaDataAccessor(typeof(GridRequest), _ => CurrentRequest, null, "request"),
#if NETSTANDARD || NETCOREAPP
				"urlhelper" => new LambdaDataAccessor(typeof(IUrlHelper), _ => UrlHelper, null, "urlhelper"),
#else
				"urlhelper" => new LambdaDataAccessor(typeof(UrlHelper), _ => UrlHelper, null, "urlhelper"),
#endif
				_ => null
			};

		#endregion
	}
}