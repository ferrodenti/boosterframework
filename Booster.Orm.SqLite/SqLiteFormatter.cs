﻿using System;
using Booster.Orm.Interfaces;
using Booster.Orm.Kitchen;

namespace Booster.Orm.SqLite;

public class SqLiteFormatter : BasicSqlFormatter
{
	public SqLiteFormatter(IOrm orm) : base(orm)
	{
	}

	protected override string FormatBoolean(bool value)
		=> FormatInteger(value ? 1 : 0, TypeCode.Int32);

	protected override string FormatDateTime(DateTime value)
		=> FormatFloat(Utils.JulianDate2Double(value), TypeCode.Double);
}