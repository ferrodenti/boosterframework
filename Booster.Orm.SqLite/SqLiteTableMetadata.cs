﻿using Booster.Log;
using Booster.Orm.Interfaces;
using Booster.Orm.Kitchen;

namespace Booster.Orm.SqLite;

public class SqLiteTableMetadata : BaseTableMetadata
{
	public SqLiteTableMetadata(BaseOrm orm, string tableName, bool isView, ILog log) : base(orm, tableName, isView, log)
	{
	}

	protected override void Write(IConnection conn, string value)
		=> conn.ExecuteNoQuery(conn.ExecuteScalar<int>(@"
						create table if not exists table_meta_data (table_name text not null primary key unique, value text); 
						select count(*) from table_meta_data where table_name=@tableName", new {tableName = TableName}) > 0
			? "update table_meta_data set table_name= @value where table_name=@tableName"
			: "insert into table_meta_data (table_name, value) values(@tableName, @value)", new {tableName = TableName, value});

	protected override string Read(IConnection conn)
		=> conn.ExecuteScalar<string>(@"
					create table if not exists table_meta_data (table_name text not null primary key unique, value text); 
					select value from table_meta_data where table_name=@tableName", new {tableName = TableName});
}