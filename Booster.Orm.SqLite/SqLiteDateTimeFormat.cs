using System.Diagnostics.CodeAnalysis;

namespace Booster.Orm.SqLite;

[SuppressMessage("ReSharper", "InconsistentNaming")]
public enum SqLiteDateTimeFormat
{
	ISO8601,
	Ticks
}