﻿using System;
using Booster.Orm.Interfaces;
using Booster.Orm.Kitchen;
using Booster.Reflection;

namespace Booster.Orm.SqLite;

public class SqLiteTypeDescriptor : BaseDbTypeDescriptor
{
	public SqLiteTypeDescriptor(IOrm orm) : base(orm)
	{
	}

	public override Method GetDataReaderMethod(TypeEx type)
		=> type == typeof (Guid) 
			? base.GetDataReaderMethod(typeof (object)) 
			: base.GetDataReaderMethod(type);

	public override TypeEx GetValueType(TypeEx type)
	{
		if (type == typeof (Guid))
			return typeof (byte[]);

		return base.GetValueType(type);
	}
		
	public override TypeEx GetType(string dbType, int length, int scale)
	{
		switch (ParseBaseType(dbType))
		{
		case "integer":
		case "int":
			return typeof (long);
		case "real":
			return typeof (double);
		case "decimal":
			return typeof (decimal);
		case "text":
			return typeof (string);
		case "blob":
			return typeof (byte[]);
		}

		return null;
	}

	protected override string GetTypeInt(TypeEx type)
	{
		if (type == typeof (byte[]))
			return "blob";
			
		switch (type.TypeCode)
		{
		case TypeCode.Boolean:
		case TypeCode.Char:
		case TypeCode.SByte:
		case TypeCode.Byte:
		case TypeCode.Int16:
		case TypeCode.UInt16:
		case TypeCode.Int32:
		case TypeCode.UInt32:
		case TypeCode.Int64:
		case TypeCode.UInt64:
			return "integer";
		case TypeCode.Single:
		case TypeCode.Double:
		case TypeCode.DateTime:
			return "real";
		case TypeCode.Decimal:
			return "decimal";
		case TypeCode.String:
			return "text";
		}


		return null;
	}
}