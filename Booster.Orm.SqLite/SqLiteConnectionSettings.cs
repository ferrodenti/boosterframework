using System;
using System.Globalization;
using System.IO;
using Booster.FileSystem;
using Booster.Orm.Kitchen;
using Booster.Reflection;

namespace Booster.Orm.SqLite;

public class SqLiteConnectionSettings : BaseConnectionSettings
{
	public override TypeEx OrmType => typeof (SqLiteOrm);

	public string FileName
	{
		get => Read<string>("FileName");
		set => Write("FileName", value);
	}

	public string RootDir
	{
		get => Read<string>("RootDir");
		set => Write("RootDir", value);
	}

	CultureInfo _noCaseCulture;
	public CultureInfo NoCaseCulture
	{
		get
		{
			if (_noCaseCulture == null)
			{
				var str = Read<string>("NoCaseCulture");
				_noCaseCulture = !string.IsNullOrWhiteSpace(str)
					? CultureInfo.GetCultureInfo(str)
					: CultureInfo.CurrentCulture;
			}

			return _noCaseCulture;
		}
		set
		{
			if (!Equals(_noCaseCulture, value))
			{
				_noCaseCulture = value;
				Write("NoCaseCulture", value.With(c => c.Name));
			}
				
		}
	}

	#region low Level settings
	public string DataSource
	{
		get => Read<string>("DataSource");
		set => Write("DataSource", value);
	}

	public int Version
	{
		get => Read("Version", 3);
		set => Write("Version", value);
	}

	public bool New
	{
		get => Read<bool>("New");
		set => Write("New", value);
	}

	public bool UseUtf16Encoding
	{
		get => Read<bool>("UseUTF16Encoding");
		set => Write("UseUTF16Encoding", value);
	}

	public bool LegacyFormat
	{
		get => Read<bool>("Legacy Format");
		set => Write("Legacy Format", value);
	}

	public bool Pooling
	{
		get => Read<bool>("Pooling");
		set => Write("Pooling", value);
	}

	public bool ReadOnly
	{
		get => Read<bool>("Read Only");
		set => Write("Read Only", value);
	}

	public bool BinaryGuid
	{
		get => Read<bool>("BinaryGUID");
		set => Write("BinaryGUID", value);
	}

	public bool FailIfMissing
	{
		get => Read<bool>("FailIfMissing");
		set => Write("FailIfMissing", value);
	}

	public string Enlist
	{
		get => Read<string>("Enlist");
		set => Write("Enlist", value);
	}

	public int MaxPoolSize
	{
		get => Read<int>("Max Pool Size");
		set => Write("Max Pool Size", value);
	}

	public int CacheSize
	{
		get => Read<int>("BinaryGUID");
		set => Write("BinaryGUID", value);
	}

	public int PageSize
	{
		get => Read<int>("Page Size");
		set => Write("Page Size", value);
	}

	public int MaxPageCount
	{
		get => Read<int>("Max Page Count");
		set => Write("Max Page Count", value);
	}

	public string Password
	{
		get => Read<string>("Password");
		set => Write("Password", value);
	}

	public SqLiteDateTimeFormat DateTimeFormat
	{
		get => Read<SqLiteDateTimeFormat>("DateTimeFormat");
		set => Write("DateTimeFormat", value);
	}

	public SqLiteJournalMode JournalMode
	{
		get => Read<SqLiteJournalMode>("Journal Mode");
		set => Write("Journal Mode", value);
	}

	public string Synchronous
	{
		get => Read<string>("Synchronous");
		set => Write("Synchronous", value);
	}

	#endregion

	public SqLiteConnectionSettings(string connectionString) : base(connectionString)
	{
		ProtectDataReaders = DataReadersProtection.Prefetch;
	}

	protected override bool IstHi(string key, string value, ref Action post)
	{
		switch (key)
		{
		case "filename":
		case "rootdir":
			post = () =>
			{
				var path = new FilePath(FileName);
				if (!path.IsAbsolute)
					value = new FilePath(RootDir.IsSome() ? RootDir : Directory.GetCurrentDirectory()).Combine(path);

				DataSource = value;
			};
			return true;

		case "datasource":
			post = () =>
			{
				try
				{
					var path = new FilePath(DataSource);
					FileName = path.MakeRelative(Directory.GetCurrentDirectory());
				}
				catch (Exception)
				{
					FileName = null;
				}
			};
			return false;
		case "nocaseculture":
			return true;
		}

		return base.IstHi(key, value, ref post);
	}
}