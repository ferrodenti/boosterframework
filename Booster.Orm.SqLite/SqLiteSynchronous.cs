namespace Booster.Orm.SqLite;

public enum SqLiteSynchronous
{
	Normal,
	Full,
	Off,
}