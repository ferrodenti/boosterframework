namespace Booster.Orm.SqLite;

public enum SqLiteJournalMode
{
	Default,
	Off,
	Persist
}