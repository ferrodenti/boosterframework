﻿using System;
using Booster.Log;
using Booster.Orm.Attributes;
using Booster.Orm.Interfaces;
using Booster.Orm.Kitchen;
using Booster.Reflection;
using JetBrains.Annotations;

#if !SKIP_EMBEDDED_LIBS
using Booster.Orm.NativeBin;
#endif

namespace Booster.Orm.SqLite
{
	[ProviderNames("sqlite")]
	public class SqLiteOrm : BaseOrm
	{
		public string RootDir
		{
			get => ((SqLiteConnectionSettings) ConnectionSettings).RootDir;
			set => ((SqLiteConnectionSettings) ConnectionSettings).RootDir = value;
		}

		public override bool ConvertCamelCaseNames2Underscope { get; set; }

		#if !SKIP_EMBEDDED_LIBS
		[PublicAPI] static SqLiteOrm()
		{
			NativeBinManager.UpdateBin(Resources.ResourceManager);
		}
		#endif

		[PublicAPI] public SqLiteOrm(ILog log = null) : base(null, log)
		{
			
		}

		[PublicAPI] public SqLiteOrm(string connectionString, ILog log = null)
			: base(new SqLiteConnectionSettings(connectionString), log)
		{
		}

		[PublicAPI] public SqLiteOrm(SqLiteConnectionSettings connectionSettings, ILog log = null)
			: base(connectionSettings, log)
		{
		}

		protected override bool CaseSensitiveNames => false;

		protected override IOrmFactory CreateFactory() 
			=> new SqLiteOrmFactory(this, Log);

		public override object PrepareValueForParam(object value)
		{
			TypeEx type = value?.GetType();
			if (value is Guid guid)
				return guid.ToByteArray();

			value = base.PrepareValueForParam(value);

			return type?.TypeCode switch
			{
				TypeCode.DateTime => Utils.JulianDate2Double((DateTime) value),
				_ => value
			};
		}
	}
}
