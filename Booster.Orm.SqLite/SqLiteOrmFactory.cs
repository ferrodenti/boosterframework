using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using Booster.Log;
using Booster.Orm.Interfaces;
using Booster.Orm.Kitchen;
#if NETSTANDARD
using System.Globalization;
using Microsoft.Data.Sqlite;
using SQLiteParameter = Microsoft.Data.Sqlite.SqliteParameter;
using SQLiteConnection = Microsoft.Data.Sqlite.SqliteConnection;
#else
using System.Data.SQLite;
#endif
namespace Booster.Orm.SqLite
{
	public class SqLiteOrmFactory : BaseOrmFactory
	{
		public SqLiteOrmFactory(SqLiteOrm orm, ILog log) : base(orm, log)
		{
		}

		public override IQueryCompiler CreateQueryCompiler()
			=> throw new System.NotImplementedException();

		public override IDbTypeDescriptor CreateDbTypeDescriptor()
			=> new SqLiteTypeDescriptor(Owner);

		public override IDbManager CreateDbManager()
			=> new SqLiteDbManager(Owner, Log);

		public override ISqlFormatter CreateSqlFormatter()
			=> new SqLiteFormatter(Owner);

		public override DbParameter CreateParameter()
			=> new SQLiteParameter();

		public override ITableMetadata CreateRelationMetadata(string tableName, bool isView)
			=> new SqLiteTableMetadata(Owner, tableName, isView, Log);

		public override async Task<DbConnection> CreateDbConnectionAsync(IConnectionSettings settings, CancellationToken token)
		{
#if NETSTANDARD
			var conn = new SqliteConnection(settings.ConnectionString);
			var culture = (settings as SqLiteConnectionSettings)?.NoCaseCulture ?? CultureInfo.CurrentCulture;
			
			conn.CreateCollation("NOCASE", (s1, s2) => string.Compare(s1, s2, culture, CompareOptions.IgnoreCase));
#else
			var conn = new SQLiteConnection(settings.ConnectionString, true);
#endif
			await conn.OpenAsync(token).NoCtx();
			return conn;
		}

		public override DbConnection CreateDbConnection(IConnectionSettings settings)
		{
#if NETSTANDARD
			var conn = new SqliteConnection(settings.ConnectionString);
			var culture = (settings as SqLiteConnectionSettings)?.NoCaseCulture ?? CultureInfo.CurrentCulture;
			
			conn.CreateCollation("NOCASE", (s1, s2) => string.Compare(s1, s2, culture, CompareOptions.IgnoreCase));
#else
			var conn = new SQLiteConnection(settings.ConnectionString, true);
#endif
			conn.Open();
			return conn;
		}

		public override IRepoQueries CreateRepoQueries(ITableMapping tableMapping)
			=> new SqLiteRepoQueries(Owner, tableMapping);
	}
}