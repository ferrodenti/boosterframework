#if !NETSTANDARD
using System.Data.SQLite;
using System.Globalization;

namespace Booster.Orm.SqLite
{
	[SQLiteFunction(FuncType = FunctionType.Collation, Name = "DefUtf8")]
	public class SqLiteDefaultUtf8Collation : SQLiteFunction
	{
		public static CultureInfo Culture { get; set; } = CultureInfo.CreateSpecificCulture("ru-RU");

		public static CompareOptions CompareOptions { get; set; } = CompareOptions.OrdinalIgnoreCase;

		public override int Compare(string x, string y)
			=> string.Compare(x, y, Culture, CompareOptions);
	}
}

#endif