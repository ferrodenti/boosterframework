﻿using System;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using Booster.Log;
using Booster.Orm.Interfaces;
using Booster.Orm.Kitchen;

using oci = Oracle.ManagedDataAccess.Client;

namespace Booster.Orm.Oracle;

public class OracleConnection : BaseConnection
{
	protected new readonly OracleConnectionSettings ConnectionSettings;
		
	public OracleConnection(IConnectionSettings  connectionSettings, Action releaser, OracleOrm orm, ILog log)
		: base (connectionSettings, releaser, orm, log)
		=> ConnectionSettings = connectionSettings as OracleConnectionSettings;

	protected override bool IsConnectionBroken(Exception ex)
	{
		if (ex is oci.OracleException oe)
			switch (oe.Number)
			{
			case 01013: //user requested cancel of current operation
				throw new TimeoutException("A command execution took too long");
			case 03113:
			case 03135:
			case 12570:
			case 12571:
			case 12537:
				return true;
			}

		var msg = ex.Message.ToLower();
		return msg switch
		       {
			       "connection must be open for this operation" => true,
			       _                                            => false
		       };
	}

	public override async Task<DbCommand> CreateCommandAsync(string command, DbParameter[] parameters, CancellationToken token = default)
	{
		var cmd = await base.CreateCommandAsync(TrimCommandEnd(command), parameters, token).NoCtx();
		return ApplyCommandParameters(cmd);
	}

	public override DbCommand CreateCommand(string command, DbParameter[] parameters)
	{
		var cmd = base.CreateCommand(TrimCommandEnd(command), parameters);
		return ApplyCommandParameters(cmd);
	}

	internal static string TrimCommandEnd(string command)
		=> command.TrimEnd(' ', '\t', '\n', '\r', ';');

	DbCommand ApplyCommandParameters(DbCommand cmd)
	{
		var oraCmd = (oci.OracleCommand) cmd;
			
		oraCmd.BindByName = true;

		if (ConnectionSettings?.CommandTimeout > 0)
			oraCmd.CommandTimeout = ConnectionSettings.CommandTimeout;

		return oraCmd;
	}
		
	protected override FormattableString GetPingCommand()
		=> $"select sysdate from dual";
}