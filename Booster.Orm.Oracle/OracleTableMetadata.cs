﻿using Booster.Log;
using Booster.Orm.Interfaces;
using Booster.Orm.Kitchen;

namespace Booster.Orm.Oracle;

public class OracleTableMetadata : BaseTableMetadata
{
	public OracleTableMetadata(BaseOrm orm, string tableName, bool isView, ILog log) : base(orm, tableName, isView, log)
	{
	}

	protected override void Write(IConnection conn, string value)
		=> conn.ExecuteNoQuery($"comment on table {TableName.ToUpper()} is '{value.Replace("'", "''")}'");

	protected override string Read(IConnection conn)
		=> conn.ExecuteScalar<string>($"select comments from user_tab_comments where lower(table_name) = {TableName.ToLower():@} ");
}