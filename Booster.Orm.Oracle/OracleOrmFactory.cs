﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Booster.Log;
using Booster.Orm.Interfaces;
using Booster.Orm.Kitchen;
using JetBrains.Annotations;
using oci = Oracle.ManagedDataAccess.Client;

namespace Booster.Orm.Oracle;

public class OracleOrmFactory : BaseOrmFactory
{
	readonly OracleOrm _orm;

	[PublicAPI]
	public Action<OracleConnectionSettings, List<QueryBuilder>> SessionSetup;
		
	public OracleOrmFactory(OracleOrm orm, ILog log) : base(orm, log)
		=> _orm = orm;

	public override IQueryCompiler CreateQueryCompiler()
		=> throw new NotImplementedException();

	public override IDbTypeDescriptor CreateDbTypeDescriptor() 
		=> new OracleTypeDescriptor(_orm);

	public override IDbManager CreateDbManager() 
		=> new OracleDbManager(Owner, Log);

	public override IRepoAdapterBuilder CreateAdapterBuilder(ITableMapping mapping, IRepoQueries repoQueries, IDbTypeDescriptor typeDescriptor) 
		=> new OracleRepoAdapterBuilder((OracleOrm)Owner, mapping, typeDescriptor, repoQueries, Log);

	public override ISqlFormatter CreateSqlFormatter() 
		=> new OracleSqlFormatter(Owner);

	public override DbParameter CreateParameter() 
		=> new oci.OracleParameter();

	public override ITableMetadata CreateRelationMetadata(string tableName, bool isView) 
		=> new OracleTableMetadata(Owner, tableName, isView, Log);

	public override IRepoQueries CreateRepoQueries(ITableMapping tableMapping) 
		=> new OracleSqlRepoQueries(Owner, tableMapping);

	public override IConnection CreateConnection(IConnectionSettings settings, Action releaser) 
		=> new OracleConnection(settings, releaser, (OracleOrm)Owner, Log);

	public override async Task<DbConnection> CreateDbConnectionAsync(IConnectionSettings settings, CancellationToken token)
	{
		var conn = new oci.OracleConnection(settings.ConnectionString);
		await conn.OpenAsync(token).NoCtx();

		foreach(var cmd in SetupSession(settings as OracleConnectionSettings, conn))
			try
			{
				await cmd.ExecuteNonQueryAsync(token).NoCtx();
			}
			finally
			{
				cmd.Dispose();
			}

		return conn;
	}

	public override DbConnection CreateDbConnection(IConnectionSettings settings)
	{
		var conn = new oci.OracleConnection(settings.ConnectionString);
		conn.Open();

		foreach(var cmd in SetupSession(settings as OracleConnectionSettings, conn))
			try
			{
				cmd.ExecuteNonQuery();
			}
			finally
			{
				cmd.Dispose();
			}

		return conn;
	}

	protected oci.OracleCommand[] SetupSession(OracleConnectionSettings settings, oci.OracleConnection connection)
	{
		var commands = new List<QueryBuilder>();

		if (settings != null)
		{
			if (settings.Schema.IsSome())
				commands.Add(new QueryBuilder(_orm, $"ALTER SESSION SET CURRENT_SCHEMA={settings.Schema}"));

			if (settings.DateFormat.IsSome())
				commands.Add(new QueryBuilder(_orm, $"ALTER SESSION SET nls_date_format='{settings.DateFormat}'"));
		}

		SessionSetup?.Invoke(settings, commands);
			
		return commands.Select(bld =>
		{
			var parameters = _orm.CreateParameters(bld.Parameters);
			var cmd = new oci.OracleCommand(bld.Query, connection);
			cmd.Parameters.AddRange(parameters);
			return cmd;
		}).ToArray();
	}
}