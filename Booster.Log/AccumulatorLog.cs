using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Log;

[PublicAPI]
public class AccumulatorLog : Log
{
	ConcurrentQueue<ILogMessage>? _messages = new();

	public int Count
	{
		get
		{
			if (_messages == null)
				return 0;
			
			lock (_messages)
				return _messages.Count;
		}
	}

	public AccumulatorLog()
		=> DateHeaders = false;
	
	public AccumulatorLog(IEnumerable<ILog> destinations, object? owner = null, object? parameters = null)
		: base(destinations, owner, parameters)
	{
	}
	
	public AccumulatorLog(ILog destination, object? owner = null, object? parameters = null)
		: base(destination, owner, parameters)
	{
	}
	
	public AccumulatorLog(object? parameters = null)
		: base(null, parameters)
		=> DateHeaders = false;

	

	public override IDisposable? WriteIntern(ILogMessage message, IDisposable? disposable)
	{
		if(_messages != null)
			lock (_messages)
				_messages.Enqueue(message);

		return base.WriteIntern(message, disposable);
	}

	public bool TryDequeue(out ILogMessage message)
	{
		if (_messages == null)
		{
			message = null!;
			return false;
		}
		lock (_messages)
			return _messages.TryDequeue(out message);
	}

	[Obsolete("Use Dump(ILog target)")]
	public int Unload(ILog target)
		=> Dump(target);

	public int Dump(ILog target)
	{
		var result = 0;

		if (_messages != null)
			lock (_messages)
				while (TryDequeue(out var message))
				{
					target.Write(message, null);
					++result;
				}

		return result;
	}

	public void StopAccumulation()
	{
		if (_messages != null)
			lock (_messages)
				_messages = null;
	}

	public void DumpAndBeNormal(ILog? target)
	{
		if (target != null)
		{
			Dump(target);
			AddDestination(target);
		}
		StopAccumulation();
	}
}

public static class AccumulatorLogExpander
{
	public static TLog MoveTo<TLog>(this AccumulatorLog? self, TLog target) 
		where TLog : ILog
	{
		self?.Dump(target);
		return target;
	}
}
