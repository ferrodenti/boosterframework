using System;
using System.Collections.Generic;
using Booster.Console;

#if NETSTANDARD
using Microsoft.Extensions.Configuration;
#endif

#pragma warning disable CS8632 //warning CS8632: The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.

namespace Booster.Log
{
	public class ConsoleLog : Log //TODO: move to Booster.Console, register in factory
	{
		public Dictionary<LogLevel, ConsoleColor> ForegroundColors { get; } = new()
		{
			{LogLevel.Fatal, ConsoleColor.Black},
			{LogLevel.Error, ConsoleColor.Red},
			{LogLevel.Warn, ConsoleColor.Yellow},
			{LogLevel.Info, ConsoleColor.Green},
			{LogLevel.Trace, ConsoleColor.Gray},
			{LogLevel.Debug, ConsoleColor.DarkGray},
			{LogLevel.All, ConsoleHelper.DefaultColor},
		};

		public Dictionary<LogLevel, ConsoleColor> BackgroundColors { get; } = new()
		{
			{LogLevel.Fatal, ConsoleColor.White},
			{LogLevel.Error, ConsoleHelper.DefaultColor},
			{LogLevel.Warn, ConsoleHelper.DefaultColor},
			{LogLevel.Info, ConsoleHelper.DefaultColor},
			{LogLevel.Trace, ConsoleHelper.DefaultColor},
			{LogLevel.Debug, ConsoleHelper.DefaultColor},
			{LogLevel.All, ConsoleHelper.DefaultColor},
		};

		public bool UseColors { get; set; } = true;

		public ConsoleLog() : this(null)
		{
		}
		public ConsoleLog(object? parameters = null) : base(name: null, parameters)
		{
			MessageFormatter.PrintLevels = false;
			ConsoleHelper.StoreDefaultColors();
		}

		public override IDisposable? WriteIntern(ILogMessage message, IDisposable disposable)
		{
			using (ConsoleHelper.Lock())
			{
				WriteDateHeader(message);
				if (UseColors)
				{
					IDisposable? fore = null;
					IDisposable? back = null;

					var param = message.Parameters;
					if (param.TryGetValue("BackColor", out var col) && StringConverter.Default.TryParse(col, out ConsoleColor color) ||
					    BackgroundColors.TryGetValue(message.Level, out color))
						back = ConsoleHelper.PushBackgroundColor(color);

					if (param.TryGetValue("TextColor", out col) && StringConverter.Default.TryParse(col, out color) ||
					    ForegroundColors.TryGetValue(message.Level, out color))
						fore = ConsoleHelper.PushForegroundColor(color);

					System.Console.WriteLine(Format(message));

					fore?.Dispose();
					back?.Dispose();
				}
				else
					System.Console.WriteLine(Format(message));
			}

			return null;
		}

#if NETSTANDARD
		public override void InitFromConfig(ILogFactory logFactory, IConfigurationSection configurationSection)
		{
			base.InitFromConfig(logFactory, configurationSection);

			var useColours = configurationSection["useColours"] ?? configurationSection["useColors"];
			
			// ReSharper disable once ConvertSwitchStatementToSwitchExpression
			switch (useColours?.ToLower())
			{
			case "true":
				UseColors = true;
				break;
			case "false":
				UseColors = false;
				break;
			}
		}
#endif
		protected override bool IsReservedConfigKey(string key) 
			=> key is "usecolours" or "usecolors" || base.IsReservedConfigKey(key);
	}
}