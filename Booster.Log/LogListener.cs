using System;

namespace Booster.Log;

public class LogListener : Log
{
	public event EventHandler<EventArgs<ILogMessage>> LogMessage;
	public Action<ILogMessage> MessageProc { get; set; }

	public LogListener(object parameters = null) : base(null, parameters)
	{
	}

	public override IDisposable WriteIntern(ILogMessage message, IDisposable disposable)
	{
		LogMessage?.Invoke(this, new EventArgs<ILogMessage>(message));
		MessageProc?.Invoke(message);

		return disposable;
	}
}