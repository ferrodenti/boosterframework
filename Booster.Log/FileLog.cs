using System;
using System.IO;
using System.Text;
using Booster.Synchronization;

#if NETSTANDARD
using Microsoft.Extensions.Configuration;
#endif

#nullable enable

namespace Booster.Log;

public class FileLog : Log //TODO: если меняется CurrentDirectory то запись происходит в новый файл, а должно быть в старый
{
	class LogFile : IDisposable
	{
		readonly FileStream _stream;
		readonly TextWriter _writer;

		public LogFile(string filename, Encoding encoding)
		{
			//Console.WriteLine("LogFile ctor");
			_stream = new FileStream(filename, FileMode.Append);
			_writer = new StreamWriter(_stream, encoding);
		}

		public void WriteLine(string line)
			=> _writer.WriteLine(line);

		public void Dispose()
		{
			//Console.WriteLine("LogFile dispose");
			_writer.Flush();
			_writer.Dispose();
			_stream.Dispose();
		}
	}

	string? _prevFilename;
	string? _prevDir;

	public Func<string>? FilenameGetter { get; set; }
	public Func<ILogMessage, string>? FilenameGetter2 { get; set; }

	public Encoding Encoding { get; set; } = Encoding.UTF8;

	string? _fileName;

	public string? Filename
	{
		get => _fileName ?? FilenameGetter?.Invoke();
		set => _fileName = value;
	}

	static readonly HashLocksPool _locks = new(65535);

	protected FileLog(object? parameters) : base(null, parameters)
		=> Async = true;

	public FileLog() : this((object?) null)
	{
	}

	public FileLog(string fileName, object? parameters = null) : this(parameters)
		=> _fileName = fileName;

	public FileLog(Func<string> fileNameGetter, object? parameters = null) : this(parameters)
	{
		DateHeaders = false;
		FilenameGetter = fileNameGetter;
	}

	public FileLog(Func<ILogMessage, string> fileNameGetter, object? parameters = null) : this(parameters)
	{
		DateHeaders = false;
		FilenameGetter2 = fileNameGetter;
	}

	public string? GetFilename(ILogMessage message)
	{
		var getter = FilenameGetter2;
		return getter != null ? getter(message) : Filename;
	}

	public override IDisposable? WriteIntern(ILogMessage message, IDisposable? disposable)
	{
		WriteDateHeader(message);

		var fileName = GetFilename(message);
		if (fileName != null)
			lock (_locks.Get(fileName))
			{
				var file = disposable as LogFile;

				if (_prevFilename != fileName)
				{
					file?.Dispose();
					file = null;

					_prevFilename = fileName;
					var dir = Path.GetDirectoryName(fileName);
					if (dir.IsSome() && dir != _prevDir)
					{
						_prevDir = dir;
						if (!Directory.Exists(dir))
							Directory.CreateDirectory(dir);
					}
				}

				message.LogFilename = fileName;

				var line = Format(message);
				if (line != null)
				{
					file ??= new LogFile(fileName, Encoding);
					file.WriteLine(line);
				}

				return file;
			}

		return null;
	}

#if NETSTANDARD
	public override void InitFromConfig(ILogFactory logFactory, IConfigurationSection configurationSection)
	{
		base.InitFromConfig(logFactory, configurationSection);

		var filename = configurationSection["filename"];
		if (filename != null)
			FilenameGetter2 = logFactory.CreateExpression(filename);
	}

#endif
	protected override bool IsReservedConfigKey(string? key)
		=> key == "filename" || base.IsReservedConfigKey(key);
}