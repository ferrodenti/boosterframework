using System;
#if NETSTANDARD
using Microsoft.Extensions.Configuration;
#endif

#nullable enable

namespace Booster.Log;

public interface ILogFactory
{
	Func<ILogMessage, bool> CreateMessageFilter(string expression);
	Func<ILogMessage, string> CreateExpression(string expression);

	ILog Create(IConfigurationSection configurationSection, Func<ILog>? defaults = null);
	bool TryCreate(IConfigurationSection configurationSection, string? typeName, out ILog log);
}
