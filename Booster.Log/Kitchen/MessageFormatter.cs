using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

#nullable enable

namespace Booster.Log.Kitchen;

public class MessageFormatter : IMessageFormatter
{
	static readonly Lazy<MessageFormatter> _default = new(() => new MessageFormatter());
	public static MessageFormatter Default => _default.Value;
		
	public string NewLine { get; set; } = "\r\n";
	public string Separator { get; set; } = " ";
	public string MemberSeparator { get; set; } = "::";
	public string ParamSeparator { get; set; } = ",";
	public string ParamOpener { get; set; } = " ";
	public string? ParamCloser { get; set; }
	public string MessagePrefix { get; set; } = ":";
	public string DateFormat { get; set; } = "== dd-MM-yy ==";
	public string TimeFormat { get; set; } = "HH:mm:ss";
	public int Priority { get; set; }
	public bool PrintLevels { get; set; } = true;

	public Func<ILogMessage, string?>? CustomFormat { get; set; }
	public Func<ILogMessage, string?>? CustomExceptionFormat { get; set; }

	static readonly Regex _tabsAtLineBegin = Utils.CompileRegex("^\t+");

	public string? Format(ILogMessage msg,  List<string>? parametersOfInterest)
	{
		if (CustomFormat != null)
			return CustomFormat(msg);

		var str = new EnumBuilder(Separator);

		if (PrintLevels)
		{
			str.AppendNoComma(msg.Level.ToString());
			str.AppendNoComma("\t");
		}

		if (TimeFormat.IsSome())
			str.AppendNoComma(msg.TimeStamp.ToString(TimeFormat));

		var beforeMsg = false;
			
		if ((msg.Exception != null || msg.Message.IsEmpty()) &&
		    (msg.Class.IsSome() || msg.Name.IsSome()) && msg.Member.IsSome())
		{

			if (msg.Name.IsSome())
			{
				str.Append(msg.Member.IsSome() ? $"{msg.Name}{MemberSeparator}{msg.Member}" : $"{msg.Name}");
				beforeMsg = true;
			}
			else if (msg.Class.IsSome())
			{
				str.Append(msg.Member.IsSome() ? $"{msg.Class}{MemberSeparator}{msg.Member}" : $"{msg.Class}");
				beforeMsg = true;
			}
			else if (msg.Member.IsSome())
			{
				str.Append($"{MemberSeparator}{msg.Member}");
				beforeMsg = true;
			}
		}
		else if (msg.Name.IsSome())
		{
			str.Append(msg.Name);
			beforeMsg = true;
		}

		var param = msg.Parameters;
		if(param.Count > 0 && parametersOfInterest != null)
		{
			var sparam = new EnumBuilder(ParamSeparator, ParamOpener, ParamCloser);
			foreach (var pname in parametersOfInterest)
				if (pname.Length > 0)
				{
					string s;
					if (pname[0] == '$')
					{
						var nm = pname.Substring(1);
						if (param.TryGetValue(nm, out s))
							sparam.Append($"{nm}: {s}");
					}
					else if (param.TryGetValue(pname, out s))
						sparam.Append(s);
				}

			if (sparam.IsSome)
			{
				str.AppendNoComma(sparam);
				beforeMsg = true;
			}
		}

		if (msg.Message.IsSome())
		{
			if(beforeMsg)
				str.AppendNoComma(MessagePrefix);

			str.Append(msg.Message);
		}

		if (msg.Exception != null)
			str.Append(FormatException(msg));

		return str;
	}

	public string? FormatException(ILogMessage msg)
	{
		if(CustomExceptionFormat != null)
			return CustomExceptionFormat(msg);

		var str = new EnumBuilder(NewLine);
		if (msg.Source.IsSome())
			str.Append($"{NewLine}{msg.Source} ({msg.Line}):");

		if(msg.Exception != null)
			foreach (var ex in msg.Exception.Expand())
			{
				str.Append($"{ex.GetType().Name} \"{ex.Message}\"");

				foreach (var key in ex.Data.Keys)
					str.Append($"{key}: {ex.Data[key]}");

				if (ex.StackTrace != null)
					str.Append(_tabsAtLineBegin.Replace(ex.StackTrace, ""));

				str.Append(NewLine);
			}

		return str;
	}
}