using System;
using System.Collections.Generic;
using Booster.Collections;
using Booster.Helpers;

#nullable enable

namespace Booster.Log.Kitchen;

[Serializable]
public class LogMessage : ILogMessage
{
	public LogLevel Level { get; set; }
	public string? Message { get; set; }
	public string? Name { get; set; }
	public string? Class { get; set; }
	public string? Member { get; set; }
	public string? Source { get; set; }
	public int Line { get; set; }
	public Exception? Exception { get; set; }
	public DateTime TimeStamp { get; set; }
	public string? ForamattedText { get; set; }
	public string? LogFilename { get; set; }

	public IMessageFormatter? MessageFormatter { get; set; }

	HybridDictionary<string, string>? _parameters;
	public IDictionary<string, string> Parameters
	{
		get
		{
			if (_parameters == null)
				lock (this)
					if (_parameters == null)
					{
						var p = new HybridDictionary<string, string>(StringComparer.OrdinalIgnoreCase);

						if (_parametersObj != null)
							p.AddRange(AnonTypeHelper.ToKVPairs<string, string>(_parametersObj));
						
						_parameters = p;
					}

			return _parameters;
		}
	}
	readonly object? _parametersObj;

	public LogMessage(LogLevel level, string? message, Exception? exception, DateTime timeStamp = default, object? parametersObj = null)
	{
		Level = level;
		Message = message;
		Exception = exception;
		_parametersObj = parametersObj;
		TimeStamp = timeStamp == default ? DateTime.Now :  timeStamp;
	}
		
	public IDisposable PushState()
	{
		var param = new HybridDictionary<string, string>(Parameters, StringComparer.OrdinalIgnoreCase);
		var formatted = ForamattedText;
		var formatter = MessageFormatter;

		return new ActionDisposable(() =>
		{
			if (Parameters.Count != param.Count)
				_parameters = param;

			ForamattedText = formatted;
			MessageFormatter = formatter;
		});
	}

	public override string? ToString() 
		=> (MessageFormatter ?? Booster.Log.Kitchen.MessageFormatter.Default).Format(this, new List<string>());
}