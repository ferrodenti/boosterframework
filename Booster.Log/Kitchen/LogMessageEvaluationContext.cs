using Booster.Parsing;

namespace Booster.Log.Kitchen;

class LogMessageEvaluationContext : IEvaluationContext
{
	readonly ILogMessage _message;

	public LogMessageEvaluationContext(ILogMessage message)
		=> _message = message;

	public bool CanRetriveParameters => false;
		
	public object GetVariable(string name)
        => name switch
           {
               "Level"     => _message.Level,
               "Exception" => _message.Exception,
               "Name"      => _message.Name,
               "Class"     => _message.Class,
               "Member"    => _message.Member,
               "Line"      => _message.Line,
               "Source"    => _message.Source,
               "TimeStamp" => _message.TimeStamp,
               _           => _message.Parameters.SafeGet(name)
           };

    public object GetParameter(string name)
		=> null;
}