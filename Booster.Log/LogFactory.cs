using System;
using System.Collections.Generic;
using System.Linq;
using Booster.Console;
using Booster.Evaluation;
using Booster.Log.Kitchen;
using Booster.Parsing;
using Booster.Reflection;
using JetBrains.Annotations;
#if NETSTANDARD
using Newtonsoft.Json.Linq;

using Microsoft.Extensions.Configuration;
#endif

namespace Booster.Log;

[PublicAPI]
public class LogFactory : ILogFactory
{
	readonly Dictionary<string, TypeEx> _logTypes = new(StringComparer.OrdinalIgnoreCase)
	{
		{"log", typeof(Log)},
		{"file", typeof(FileLog)},
		{"trace", typeof(TraceLog)},
		{"console", typeof(ConsoleLog)}
	};

	readonly Dictionary<string, object> _values = new();

	public event EventHandler<ExceptionEventArgs> Exception;

	protected virtual void OnException(Exception exception)
	{
		var handler = Exception;
		if (handler != null)
			Try.IgnoreErrors(() => handler(this, new ExceptionEventArgs(exception)));
	}

	public ILogFactory RegisterLog(TypeEx type, string name = null)
	{
		if (name == null)
		{
			if (!type.TryFindAttribute<LogNameAttribute>(out var attr) || attr.Name.IsEmpty())
				throw new ArgumentException($"Cannot resolve log name: {type}", nameof(type));

			name = attr.Name;
		}

		_logTypes[name] = type;
		return this;
	}

	public ILogFactory AddVariable(string name, object value)
	{
		_values[name] = value;
		return this;
	}

#if NETSTANDARD
	public ILog Create(JArray configurationSection, Func<ILog> defaults = null)
	{
		//TODO: create from JObject
		return defaults?.Invoke();
	}
#endif

	public ILog Create(IConfigurationSection configurationSection, Func<ILog> defaults = null)
	{
		if (configurationSection != null)
			try
			{
				var children = configurationSection.GetChildren().Where(c => c.Key != "#comment").ToArray();
				if (children.Length > 0)
				{
					var log = new Log {OnError = OnException};

					log.AddDestinations(this, children);

					return log;
				}
			}
			catch (Exception e)
			{
				if (defaults != null)
				{
					var log = defaults();
					log.Error(e, "Failed to parse logs configuration, using default logs,\nException:");
					return log;
				}

				ConsoleHelper.WriteLine($"Failed to parse logs configuration, using default logs,\nException: {e.Message}");
				return null;
			}

		if (defaults != null)
		{
			var log = defaults();
			log.Warn("Logs configuration not found, using default logs");
			return log;
		}

		ConsoleHelper.WriteLine("Logs configuration not found, using default logs");
		return null;
	}

	public bool TryCreate(IConfigurationSection configurationSection, string typeName, out ILog log)
	{
		if (typeName == null || !_logTypes.TryGetValue(typeName, out var type))
		{
			log = null;
			return false;
		}

		var logInt = (ILogInt) type.Create();
		log = logInt;
		logInt.OnError = OnException;
		logInt.InitFromConfig(this, configurationSection);
		return true;
	}

	public static ILog Create(IConfigurationSection config, params TypeEx[] logTypes)
	{
		var factory = new LogFactory();
		foreach (var type in logTypes)
			factory.RegisterLog(type);

		return factory.Create(config);
	}

	ExpressionParser _expressionParser;

	protected ExpressionParser ExpressionParser
	{
		get
		{
			if (_expressionParser == null)
			{
				var result = ExpressionParser.CreateCsParser();
				result.SpecialConstants["LogLevel.Trace"] = LogLevel.Trace;
				result.SpecialConstants["LogLevel.Debug"] = LogLevel.Debug;
				result.SpecialConstants["LogLevel.Info"] = LogLevel.Info;
				result.SpecialConstants["LogLevel.Warn"] = LogLevel.Warn;
				result.SpecialConstants["LogLevel.Error"] = LogLevel.Error;
				result.SpecialConstants["LogLevel.Fatal"] = LogLevel.Fatal;
				result.SpecialConstants["LogLevel.All"] = LogLevel.All;

				foreach (var pair in _values)
					result.SpecialConstants[pair.Key] = pair.Value;

				_expressionParser = result;
			}

			return _expressionParser;
		}
	}

	public Func<ILogMessage, bool> CreateMessageFilter(string expression)
	{
		var exp = ExpressionParser.Parse(expression);
		return msg => !Equals(exp.Evaluate(new LogMessageEvaluationContext(msg)), false);
	}

	public Func<ILogMessage, string> CreateExpression(string expression)
	{
		var param = new DynamicParameter(typeof(ILogMessage), expression)
		{
			DataAccessorProvider = (_, s)
				=> //TODO: создаем параметр для ILogMessage, а приходит LogMessage, не может создать геттер
			{
				return s switch //Приходится юзать LambdaDataAccessor
				{
					"Level" => new LambdaDataAccessor(typeof(ILogMessage), msg => (msg as ILogMessage)?.Level, null),
					"Exception" => new LambdaDataAccessor(typeof(ILogMessage), msg => (msg as ILogMessage)?.Exception,
						null),
					"Name" => new LambdaDataAccessor(typeof(ILogMessage), msg => (msg as ILogMessage)?.Name, null),
					"Class" => new LambdaDataAccessor(typeof(ILogMessage), msg => (msg as ILogMessage)?.Class, null),
					"Member" => new LambdaDataAccessor(typeof(ILogMessage), msg => (msg as ILogMessage)?.Member, null),
					"Line" => new LambdaDataAccessor(typeof(ILogMessage), msg => (msg as ILogMessage)?.Line, null),
					"Source" => new LambdaDataAccessor(typeof(ILogMessage), msg => (msg as ILogMessage)?.Source, null),
					"TimeStamp" => new LambdaDataAccessor(typeof(ILogMessage), msg => (msg as ILogMessage)?.TimeStamp, null),
					null => new LambdaDataAccessor(typeof(ILogMessage), _ => null, null),
					_ => _values.TryGetValue(s, out var value)
						? new LambdaDataAccessor(typeof(ILogMessage), _ => value, null)
						: new LambdaDataAccessor(typeof(ILogMessage),
							msg => (msg as ILogMessage)?.Parameters.SafeGet(s), null)
				};
			}
		};

		return msg => param.Evaluate(msg)?.ToString();
	}
}
