using System;

namespace Booster.Log;

[AttributeUsage(AttributeTargets.Class)]
public class LogNameAttribute : Attribute
{
	public string Name { get; }
		
	public LogNameAttribute(string name)
		=> Name = name;
}