﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Booster.Collections;
using Booster.Debug;
using Booster.DI;
using Booster.Helpers;
using Booster.Log.Kitchen;
using Booster.Reflection;
using JetBrains.Annotations;
#if NETSTANDARD
using Microsoft.Extensions.Configuration;
#endif

#nullable enable

namespace Booster.Log;

// ReSharper disable once RedundantExtendsListEntry
[PublicAPI]
[InstanceFactory(typeof(ILog))]
public class Log : ILogInt
{
	class AggDisposable : IDisposable
	{
		readonly IDisposable?[] _disposables;

		public AggDisposable(int capacity)
			=> _disposables = new IDisposable[capacity];

		public IDisposable? this [int key]
		{
			get => key < _disposables.Length ? _disposables[key] : null;
			set => _disposables[key] = value;
		}

		public void Dispose()
		{
			foreach (var d in _disposables)
				d?.Dispose();
		}
	}

	public string? Class { get; }
	public string? Name { get; }

	public LogLevel AcceptLevels { get; set; } = LogLevel.All;
	public bool Async { get; set; }
	public bool Enabled { get; set; } = true;
	public bool DateHeaders { get; set; } = true;
	public Action<Exception>? OnError { get; set; }

	ConcurrentQueue<ILogMessage>? _queue;
	protected ConcurrentQueue<ILogMessage> Queue => _queue ??= new ConcurrentQueue<ILogMessage>();
	volatile Task? _task;


	ManualResetEventSlim _messageWaiter = new(false);

	DateTime _lastDate;

	public IMessageFormatter MessageFormatter
	{
		get => _messageFormatter ??= new MessageFormatter();
		set => _messageFormatter = value;
	}

	IMessageFormatter? _messageFormatter;

	public Func<ILogMessage, bool>? MessageFilter { get; set; }

	public IDictionary<string, string> Parameters
	{
		get
		{
			if (_parameters == null)
				lock (this)
					if (_parameters == null)
					{
						var p = new HybridDictionary<string, string>(StringComparer.OrdinalIgnoreCase);

						if (_parametersObj != null)
							p.AddRange(AnonTypeHelper.ToKVPairs<string, string>(_parametersObj));
						
						_parameters = p;
					}

			return _parameters;
		}
	}
	volatile IDictionary<string, string>? _parameters;
	readonly object? _parametersObj;

	readonly List<ILog> _destinations = new();
	
	public List<string>? ParametersOfInterest { get; set; }

	internal Log(string? name = "", object? parameters = null) :
		this(Array.Empty<ILog>(), name, parameters)
	{
	}

	public Log(ILog destination, object? owner = null, object? parameters = null) :
		this(new[] {destination}, owner, parameters)
	{
	}

	public Log(IEnumerable<ILog> destinations, object? owner = null, object? parameters = null)
	{
		switch (owner)
		{
		case string str:
			Name = str;
			break;
		case Type type:
			Class = type.Name;
			break;
		case TypeEx type:
			Class = type.Name;
			break;
		default:
			Class = owner?.ToString();
			break;
		}

		_parametersObj = parameters;

		foreach (var dst in destinations.Where(d => d != null))
			AddDestination(dst);
	}
	
	public Log() {}
	public Log(params ILog[] destinations) : this(destinations, owner: null) { }
	public Log(string name, params ILog[] destinations) : this(destinations, name) { }
	public Log(string name, object parameters, params ILog[] destinations) : this(destinations, name, parameters) { }
	public Log(object? parameters, params ILog[] destinations) : this(destinations, null, parameters) { }

	public ILog CreateImpl(object? owner, object? parameters) 
		=> new Log(this, owner, parameters);

	public void AddDestination(ILog destination, bool transferParametersOfInterest = true)
	{
		_destinations.Add(destination);

		if (transferParametersOfInterest && ParametersOfInterest?.Count > 0)
		{
			destination.ParametersOfInterest ??= new List<string>();
			destination.ParametersOfInterest.AddRange(ParametersOfInterest);
		}
	}

	public IEnumerable<TLog> GetDestinations<TLog>()
		where TLog : ILog
		=> _destinations.WhereCast<TLog>();

	protected void SafeWrapper(Action act)
	{
		try
		{
			act();
		}
		catch (Exception ex)
		{
			Utils.Nop(ex);
			SafeBreak.Break();
		}
	}


	public virtual void Write(
		LogLevel level, 
		string? message, 
		Exception? exception, 
		object? parameters, 
		string? mem, 
		string? src, 
		int line,
		DateTime timeStamp = default)
		=> SafeWrapper(() =>
		{
			Write(new LogMessage(level, message, exception, timeStamp, parameters)
			{
				Name = Name,
				Class = Class,
				Member = mem,
				Source = src,
				Line = line,
			});
		});

	public virtual void Write(
		LogLevel level, 
		string? message, 
		Exception? exception,
		object? parameters, 
		DateTime timeStamp = default)
		=> SafeWrapper(() 
			=> Write(new LogMessage(level, message, exception, timeStamp, parameters)));

	protected virtual bool Filter(ILogMessage message)
	{
		if (!Enabled)
			return false;

		if(!AcceptLevels.HasFlag(message.Level))
			return false;

		if (MessageFilter != null)
			if (!MessageFilter(message))
				return false;

		return true;
	}

	protected void Init(ILogMessage message)
	{
		foreach (var pair in Parameters.Where(pair => !message.Parameters.ContainsKey(pair.Key)))
			message.Parameters[pair.Key] = pair.Value;

		if (_messageFormatter?.Priority > message.MessageFormatter?.Priority)
			message.MessageFormatter = _messageFormatter;
	}

	protected string? Format(ILogMessage message)
	{
		if (message.MessageFormatter?.Priority > _messageFormatter?.Priority)
			return message.ForamattedText ?? (message.ForamattedText = message.MessageFormatter?.Format(message, ParametersOfInterest));

		return MessageFormatter.Format(message, ParametersOfInterest);
	}

	object _dateHeadersLock = new();

	protected void WriteDateHeader(ILogMessage message)
	{
		if (DateHeaders)
		{
			var dt = message.TimeStamp.Date;
			if (dt > _lastDate)
				lock (_dateHeadersLock)
					if (dt > _lastDate)
					{
						_lastDate = dt;

						var header = new LogMessage(LogLevel.All, null, null)
						{
							ForamattedText = dt.ToString(MessageFormatter.DateFormat),
							MessageFormatter = new MessageFormatter { Priority = int.MaxValue }
						};

						//Init(header);
						WriteIntern(header, null)?.Dispose();
					}
		}
	}

	public void Write(ILogMessage message) 
		=> Write(message, null)?.Dispose();

	public virtual IDisposable? Write(ILogMessage message, IDisposable? disposable)
	{
		try
		{
			if (Filter(message))
			{
				if (Async)
				{
					Queue.Enqueue(message);

					_messageWaiter.Set();

					if (_task == null)
						lock (Queue)
							// ReSharper disable once ConvertIfStatementToNullCoalescingAssignment
							if (_task == null)
								_task = Utils.StartTaskNoFlow(WriteTask); 
					// Отказался от использования TimeSchedule, т.к. ресурсы у логов разные и лучше писать их параллельно
					// TODO: Вообще хотелось бы иметь некий класс который копит сообщения и обрабатывает их асинхронно
				}
				else
				{
					Init(message);

					WriteIntern(message, disposable);
				}
			}
		}
		catch (Exception e)
		{
			OnError?.Invoke(e);
		}
		return disposable;
	}

	public virtual void InitFromConfig(ILogFactory logFactory, IConfigurationSection configurationSection)
	{
		var enabled = configurationSection["enabled"];
		// ReSharper disable once ConvertSwitchStatementToSwitchExpression
		Enabled = enabled?.ToLower() switch
		{
			"true" => true,
			"false" => false,
			_ => Enabled
		};

		var param = configurationSection["parameters"];
		if (param != null)
			ParametersOfInterest = param.SplitNonEmpty(',', ';').ToList();
		
		var filter = configurationSection["filter"];
		if (filter != null)
			MessageFilter = logFactory.CreateMessageFilter(filter);
		
		var dst = configurationSection.GetSection("destinations");

		AddDestinations(logFactory, dst?.GetChildren() ?? configurationSection.GetChildren());
	}

	protected virtual bool IsReservedConfigKey(string? key)
		=> key switch
		{
			"enabled" or "parameters" or "filter" or "destinations" => true,
			"type" => GetType() != typeof(Log),
			_ => false
		};

	public void AddDestinations(ILogFactory logFactory, IEnumerable<IConfigurationSection> configSections)
	{
		var errors = new List<string>();

		foreach (var cfg in configSections)
		{
			void TryAdd(string? type)
			{
				if (logFactory.TryCreate(cfg, type, out var subLog))
					AddDestination(subLog);
				else
					errors.Add($"Logging type \"{type}\" was not registered in the factory!");
			}

			var cfgType = cfg["type"];
			if (cfgType != null)
				TryAdd(cfgType);
			else if(IsReservedConfigKey(cfg.Key?.ToLower()))
				Utils.Nop();
			else if (int.TryParse(cfg.Key, out _))
			{
				var log = new Log { OnError = OnError };

				log.InitFromConfig(logFactory, cfg);

				AddDestination(log);
			}
			else
				TryAdd(cfg.Key);
		}

		foreach (var error in errors)
			this.Error(error);
	}

	protected void WriteTask()
	{
		var exit = false;
		IDisposable? disp = null;
		do
			try
			{
				while (Queue.TryDequeue(out var msg))
				{
					Init(msg);
					disp = WriteIntern(msg, disp);
				}

				_messageWaiter.Reset();
			}
			catch (Exception ex)
			{
				OnError?.Invoke(ex);
				Utils.Nop(ex);
				SafeBreak.Break();

				disp?.Dispose();
				disp = null;
			}
			finally
			{
				if (Queue.Count == 0)
					if (!_messageWaiter.Wait(50))
					{
						_messageWaiter.Reset();

						lock (Queue)
							if (Queue.Count == 0)
							{
								exit = true;
								_task = null;
								disp?.Dispose();
								disp = null;
							}
					}
			}
		while (!exit);
	}

	public virtual IDisposable? WriteIntern(ILogMessage message, IDisposable? disposable)
	{
		if (_destinations.Count == 1)
		{
			_destinations[0].Write(message, disposable);
			return disposable;
		}

		var aggd = disposable as AggDisposable ?? new AggDisposable(_destinations.Count);

		for (var i = 0; i < _destinations.Count; i++)
			using (message.PushState())
				aggd[i] = _destinations[i].Write(message, aggd[i]);

		return aggd;
	}

	public async Task FlushAsync()
	{
		var task = _task;
		if (task != null)
			await task.NoCtx();

		foreach (var dst in _destinations)
			await dst.FlushAsync().NoCtx();
	}

	public void Flush()
		=> FlushAsync().NoCtxWait();

#if NETSTANDARD
	public async ValueTask DisposeAsync()
#else
	public async Task DisposeAsync()
#endif
		=> await FlushAsync().NoCtx();

	public void Dispose()
		=> Flush();
}
