using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using JetBrains.Annotations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;

namespace Booster.Log.Configuration;

[PublicAPI]
public class XmlLogConfig : IConfigurationSection, IXmlSerializable
{
	XmlDocument _document;
	protected XmlDocument Document => _document ??= new XmlDocument();
	XmlNode _inner;

	public XmlLogConfig()
	{
	}
		
	public XmlLogConfig(XmlNode inner, string key = null, string path = null)
	{
		_inner = inner;
		Key = key;
		Path = path;
		Value = inner.Value;
	}


	public XmlSchema GetSchema()
		=> null;

	public void ReadXml(XmlReader reader)
		=> _inner = Document.ReadNode(reader);

	public void WriteXml(XmlWriter writer)
	{
		foreach(var o in _inner.ChildNodes)
			if (o is XmlNode n)
				n.WriteTo(writer);
	}

	public IConfigurationSection GetSection(string key)
		=> GetChildren().FirstOrDefault(n => n.Key.EqualsIgnoreCase(key));

	public IEnumerable<IConfigurationSection> GetChildren()
		=> _inner.ChildNodes
			.Cast<XmlNode>().Concat(_inner.Attributes?.Cast<XmlNode>() ?? Enumerable.Empty<XmlNode>())
			.Where(n => n != null)
			.Select(n => new XmlLogConfig(n, n.Name, ".".JoinNonEmpty(Path, n.Name)));

	public IChangeToken GetReloadToken()
		=> null;

	public string this[string key]
	{
		get => GetSection(key)?.Value;
		set => throw new NotSupportedException();
	}

	public string Key { get; }
	public string Path { get; }
	public string Value { get; set; }
		
	public override string ToString()
		=> _inner.ToString();

	static readonly XmlSerializer _serializer = new(typeof(XmlLogConfig));
		
	public static XmlLogConfig FromString(string xml)
	{
		// ReSharper disable ConvertToUsingDeclaration
		using var rd = new StringReader(xml.Trim());
		using var reader = XmlReader.Create(rd);
		var result = new XmlLogConfig();
		result._inner = result.Document.ReadNode(reader);
		return result;
		// ReSharper restore ConvertToUsingDeclaration
	}
}