using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json.Linq;

namespace Booster.Log.Configuration;

[PublicAPI]
public class JsonLogConfig : IConfigurationSection
{
	readonly JToken _token;

	public JsonLogConfig(JToken token, string key = null, string path = null)
	{
		_token = token;
		Key = key;
		Path = path;

		if (_token is JValue jValue)
			Value = jValue.Value?.ToString();
	}


	public IConfigurationSection GetSection(string key)
	{
		if (_token is JObject jObject && jObject.TryGetValue(key, out var value))
			return new JsonLogConfig(value, key, ".".JoinNonEmpty(Path, key));

		return null;
	}

	public IEnumerable<IConfigurationSection> GetChildren()
	{
		switch (_token)
		{
			case JArray _:
				var cnt = 0;
				foreach (var c in _token.Children())
					yield return new JsonLogConfig(c, cnt.ToString(), ".".JoinNonEmpty(Path, $"[{cnt++}]"));
				break;
			case JObject jObject:
				foreach (var pair in jObject)
					yield return new JsonLogConfig(pair.Value, pair.Key, ".".JoinNonEmpty(Path, $"[{pair.Key}]"));
				break;
		}
	}

	public IChangeToken GetReloadToken()
		=> null;

	public string this[string key]
	{
		get => GetSection(key)?.Value;
		set => throw new NotSupportedException();
	}

	public string Key { get; }
	public string Path { get; }
	public string Value { get; set; }

	public override string ToString()
		=> _token.ToString();
}