using System;
using System.Diagnostics;
using JetBrains.Annotations;

namespace Booster.Log;

[PublicAPI]
public class TraceLog : Log
{
	public TraceLog()
		=> DateHeaders = false;
		
	public TraceLog(object parameters = null) : base(null, parameters)
		=> DateHeaders = false;

	public override IDisposable WriteIntern(ILogMessage message, IDisposable disposable)
	{
		lock (Utils.TraceLock)
		{
			WriteDateHeader(message);
			Trace.WriteLine(Format(message));
		}
		return null;
	}
}