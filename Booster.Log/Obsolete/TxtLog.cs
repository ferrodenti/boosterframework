using System;

#nullable enable

namespace Booster.Log.Obsolete;

[Obsolete("Используй FileLog")]
public class TxtLog : FileLog
{
	public TxtLog(string fileName, object? parameters = null) : base(fileName, parameters) { }
	public TxtLog(Func<string> fileNameGetter, object? parameters = null) : base(fileNameGetter, parameters) { }
	public TxtLog(Func<ILogMessage,string> fileNameGetter, object? parameters = null) : base(fileNameGetter, parameters) { }
}