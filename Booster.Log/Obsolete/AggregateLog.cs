﻿using System;

#nullable enable

namespace Booster.Log.Obsolete;

[Obsolete("Используй Log")]
public class AggregateLog : Log
{		 
	public AggregateLog(params ILog[] writers)
		: this(null, writers)
	{
	}
	public AggregateLog(object? parameters, params ILog[] writers)
		: base(writers, null, parameters)
	{
	}
}