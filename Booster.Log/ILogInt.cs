using System;
#if NETSTANDARD
using Microsoft.Extensions.Configuration;
#endif

#nullable enable

namespace Booster.Log;

interface ILogInt : ILog
{
	Action<Exception>? OnError { get; set; }

	void InitFromConfig(ILogFactory logFactory, IConfigurationSection configurationSection);
}
