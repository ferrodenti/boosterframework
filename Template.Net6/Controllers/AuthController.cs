using System.Threading.Tasks;
using Booster;
using Booster.Interfaces;
using Booster.Mvc.Helpers;
using Microsoft.AspNetCore.Mvc;
using Template.Net6.Models;

namespace Template.Net6.Controllers;

public class AuthController : Controller
{
	public IActionResult Login(string returnUrl = null)
		=> View((returnUrl, (string)null, (string)null));

	[HttpPost]
	public async Task<IActionResult> Login(string login, string password, string returnUrl)
	{
		var user = await Models.User.GetByLogin(login);
		if (user != null && user.IsPasswordCorrect(password))
		{
			await CurrentUser.SetAsync(user);

			if (returnUrl.IsEmpty() || returnUrl == "/")
				returnUrl = Url.Action("Index", "Home", new {area = ""});
				
			return new RedirectResult(returnUrl);
		}
			
		return View((returnUrl, login, "Неверное сочетание логина и пароля"));
	}

	public Task Logout()
		=> CurrentUser.SignOutAsync();
}