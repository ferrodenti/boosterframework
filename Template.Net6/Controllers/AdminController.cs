﻿using Booster.Mvc;
using Booster.Mvc.Grid;
using Microsoft.AspNetCore.Mvc;
using Template.Net6.Models;

namespace Template.Net6.Controllers;

[Access]
public class AdminController : Controller
{
	[Access(UserPermission.EditUsers)]
	public IActionResult Users()
		=> View("_Grid", Grid.KeyOf<User>());
		
	[Access(UserPermission.EditUserRoles)]
	public IActionResult UserRoles()
		=> View("_Grid", Grid.KeyOf<UserRole>());

	[Access(UserPermission.EditCronTasks)]
	public IActionResult CronTasks()
		=> View("_Grid", Grid.KeyOf<CronTask>());
}