using System.Threading.Tasks;
using Booster;
using Booster.Interfaces;
using Booster.Mvc.Declarations;
using Booster.Orm;

namespace Template.Net6.Models;

[DbDict]
[GridModel("Управление ролями пользователей")]
[GridPermissions(UserPermission.EditUserRoles)]
public class UserRole : IndexedEntity<UserRole>, IReorderable
{
	[DbReorder]
	public int Order { get; set; }

	[Db, Grid("Название")]
	public string Name { get; set; }

	[Db, Grid("Разрешения"), EditorSelect(Multiple = true, Delimiter = ",")]
	public UserPermission Permissions { get; set; }

	public override string ToString() => Name.Or(() => base.ToString());

	static readonly KeyQueryCache<UserRole,string> _byName = new(r => r.Name);
	public static Task<UserRole> GetByName(string name) => _byName.GetAsync(name);
		
	[Migration(MigrationOptions.TableCreated)]
	static async Task TableCreated()
	{
		await new UserRole
		{
			Name = "Пользователь", 
			Permissions = UserPermission.None
		}.InsertAsync().NoCtx();
			
		await new UserRole
		{
			Name = "Администратор", 
			Permissions = UserPermission.All
		}.InsertAsync().NoCtx();
	}
}