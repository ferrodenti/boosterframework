using Booster;
using Booster.Mvc.Helpers;

#nullable enable

namespace Template.Net6.Models;

public class CurrentUser : BaseCurrentUser<User, CurrentUser>
{
	public CurrentUser() 
		: base(MvcHelper.HttpContext, MvcHelper.Session)
	{
	}

	protected override User GetDefaultUser()
		=> User.GetAnonUser();

	protected override User? LoadUser(string? identity)
		=> LoadUserAsync(identity, CancellationToken.None).NoCtxResult();

	protected override Task<User?> LoadUserAsync(string? identity, CancellationToken token)
		=> User.GetByLogin(identity);

	protected override string GetUserIdentity(User user)
		=> user.Login;
}