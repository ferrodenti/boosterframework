using System;

namespace Template.Net6.Models;

[Flags]
public enum UserPermission
{
	None = 0,
	EditUsers = 1,
	EditUserRoles = 2,
	EditCronTasks = 4,
	Restart = 8,
	All = 65535
}