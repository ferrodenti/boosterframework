using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Booster;
using Booster.CodeGen;
using Booster.Cron;
using Booster.Cron.Kitchen;
using Booster.Debug;
using Booster.FileSystem;
using Booster.Interfaces;
using Booster.Log;
using Booster.Mvc.Declarations;
using Booster.Mvc.Helpers;
using Booster.Orm;
using Booster.Orm.Interfaces;

namespace Template.Net6.Models;

[DbDict]
[GridModel("Управление назначенными заданиями")]
[GridPermissions(UserPermission.EditCronTasks)]
public class CronTask : IndexedEntity<CronTask>, IReorderable
{
	[DbReorder] public int Order { get; set; }

	[Db] bool _enabled = true;
	[Grid("Вкл.")]
	public bool Enabled
	{
		get => _enabled;
		set
		{
			if (_enabled != value)
			{
				_enabled = value;
				UpdateNext();
			}
		}
	}


	[GridNoEditor("S&nbsp;&nbsp;M&nbsp;&nbsp;H&nbsp;&nbsp;DM&nbsp;&nbsp;M&nbsp;&nbsp;DW&nbsp;&nbsp;Y", PreserveHtml = true)]
	public string CronExpressionGrid => $@"
{FormatCronPart(CronExpression.Seconds, "S")}
{FormatCronPart(CronExpression.Minutes, "M")}
{FormatCronPart(CronExpression.Hours, "H")}
{FormatCronPart(CronExpression.DayOfMonth, "DM")}
{FormatCronPart(CronExpression.Month, "M")}
{FormatCronPart(CronExpression.DayOfWeek, "DW")}
{FormatCronPart(CronExpression.Year, "Y")}";

	static string FormatCronPart(BaseExpression expression, string name)
		=> expression.IsValid 
			? $"<span class='label label-success' title='{name}'>{expression}</span>" 
			: $"<span class='label label-danger' title='{name}: {expression.ParsingError}'>{expression}</span>";

	[Db] string _cronExpression;
	CronExpression _cronExpressionObj;
	[Editor("Время")]
	public CronExpression CronExpression
	{
		get => _cronExpressionObj ??= _cronExpression ?? "";
		set
		{
			if (_cronExpression != value)
			{
				_cronExpression = value;
				_cronExpressionObj = null;
				UpdateNext();
			}
		}
	}

	[Db] string _name;
	[Grid("Название")]
	public string Name
	{
		get => _name;
		set
		{
			if (_name != value)
			{
				_name = value;
				_log = null;
			}
		}
	}

	[GridNoEditor("Файл", PreserveHtml = true)]
	public string FileGrid => File.IsEmpty()
		? ""
		: Permission != UserPermission.None && CurrentUser.Value.Role?.Permissions.HasFlag(Permission) == true 
			? "Access denied"
			: !FileExists
				? $"<span title='File does not exist'>{File}</span>"
				: $"<a href='{MvcHelper.Url(File)}'>{File}</a>";

	[Db, Editor("Файл")] public string File { get; set; }

	[Db, EditorTextarea("Команда")]
	public string Command
	{
		get => _command;
		set
		{
			if (_command != value)
			{
				_command = value;
				_executer.Reset();
				UpdateNext();
			}
		}
	}
	string _command;
		
	static string ExcapeAttr(string s) => s?.Replace("\"", "&quot;");
	public static string Tooltip(string tooltip, string text = "?") =>
		tooltip.With(t => $"<span data-toggle=\"tooltip\" data-placement=\"right\" data-html=\"true\" title=\"{ExcapeAttr(t)}\">{text}</span>");

	[GridNoEditor("Статус", PreserveHtml = true, SortExpression = "nextexecution")]
	public string Status
	{
		get
		{
			var result = new EnumBuilder("<br/>");
			string icon = null;

			if (IsExecuting)
				icon =  "<i class='glyphicon glyphicon-fire text-danger'>&nbsp;</i>";
			else if (Instances.TaskSchedule[0] == Subscription)
				icon = "<i class='glyphicon glyphicon-exclamation-sign text-warning'>&nbsp;</i>";

			if (!CronExpression.IsValid) result.Append(Tooltip(CronExpression.Errors.ToString(new EnumBuilder("\n")), "<span class='text-danger'>Invalid time</span>"));
			if (Executer?.IsOk == false) result.Append(Tooltip(Executer.Errors.ToString(new EnumBuilder("\n")), "<span class='text-danger'>Compilation Error</span>"));


			if (NextExecution.IsSignificant()) result.Append($"Next: {NextExecution:dd.MM HH:mm}");
			if (icon.IsSome())
				result.AppendNoComma(icon);

			if (LastSuccess != LastExecution) result.Append( $"Success: {LastSuccess:dd.MM HH:mm}");
			if (LastExecution.IsSignificant()) result.Append($"Last: {LastExecution:dd.MM HH:mm}");

			if (ResultOk.HasValue)
				result.Append($@"Result: {Tooltip(Result, ResultOk.Value 
					? "<i class='glyphicon glyphicon-ok-sign text-success'>&nbsp;</i>" 
					: "<i class='glyphicon glyphicon-remove-sign text-danger'>&nbsp;</i>")}");

			return result;
		}
	}

	[Db] public DateTime LastExecution { get; set; }
	[Db] public DateTime LastSuccess { get; set; }
	[Db] public DateTime NextExecution { get; set; }
	[Db] public bool? ResultOk { get; set; }
	[Db] public string Result { get; set; }
	[Db, Editor] public string ContentType { get; set; } = "text/xml";
	[Db, EditorSelect(Multiple = true)] public UserPermission Permission { get; set; }

	string _dependecies;
	[Db, Editor]
	public string Dependencies
	{
		get => _dependecies;
		set
		{
			if (_dependecies != value)
			{
				_dependecies = value;
				UpdateDependencies(_dependecies);
			}
		}
	}

	string _refAssemblies;
	[Db, Editor]
	public string RefAssemblies
	{
		get => _refAssemblies;
		set
		{
			if (_refAssemblies != value)
			{
				_refAssemblies = value;
				_executer.Reset();
			}
		}
	}

	string _usings;
	[Db, Editor]
	public string Usings
	{
		get => _usings;
		set
		{
			if (_usings != value)
			{
				_usings = value;
				_executer.Reset();
			}
		}
	}

	[Db, Editor]
	public int OnErrorRetryInterval { get; set; } = 10 * 1000;

	[GridRowControls] 
	protected string Controls => $"<button class='btn btn-xs btn-info exe-task' data-task-id='{Id}'>Exe</button>";

	TimeSchedule.Subscription _subscription;
	protected TimeSchedule.Subscription Subscription
	{
		get
		{
			if (_subscription == null)
			{
				_subscription = Instances.TaskSchedule.Subscribe(this, Execute);
				_subscription.Error += (_,e) =>
				{
					Result = $"Error: {e.GetException().Message}";
					Save();
				};
			}
			return _subscription;
		}
	}

	ILog _log;
	protected ILog Log => _log ??= Instances.LogRoot.Create(this);

	CancellationTokenSource _cancellationTokenSource;

	public FilePath FilePath => File.IsSome() ? Instances.Config.MappedAppData.Combine(File) : null;
	public bool FileExists => File.IsSome() && System.IO.File.Exists(FilePath);

	readonly List<IRepository> _subsribedRepositories = new();

	protected IEnumerable<IRepository> ParseDependencies(string dependencies, bool throwException)
	{
		var result = new List<IRepository>();
		var errors = new EnumBuilder("\n");

		if (dependencies.IsSome())
			foreach (var dep in dependencies.Split(new[] {',', ' ', ';'}, StringSplitOptions.RemoveEmptyEntries))
			{
				var repo = Instances.Orm.Repositories.FirstOrDefault(r => r.EntityType.Name.EqualsIgnoreCase(dep));
				if (repo != null)
					result.Add(repo);
				else
					errors.Append($"{dep} - repository not found");
			}

		if (errors.IsSome && throwException)
			throw new Exception(errors);

		return result;
	}

	protected void UpdateDependencies(string dependencies)
	{
		void DependencyChanged(object sender, RepositoryChangedEventArgs e)
		{
			_cancellationTokenSource?.Cancel();

			if (AllowBackgroundRun && Subscription != null)
				Subscription.Next = NextExecution = CronExpression.GetNext(DateTime.Now.AddMinutes(1));
		}

		var toUnsubscribe = new HashSet<IRepository>(_subsribedRepositories);

		if(dependencies.IsSome())
			foreach (var repo in ParseDependencies(dependencies, false))
				if (toUnsubscribe.Contains(repo))
					toUnsubscribe.Remove(repo);
				else
				{
					_subsribedRepositories.Add(repo);
					repo.Changed += DependencyChanged;
				}

		foreach (var repo in toUnsubscribe)
		{
			_subsribedRepositories.Remove(repo);
			repo.Changed -= DependencyChanged;
		}

		UpdateNext();
	}


	readonly SyncLazy<Executer> _executer = SyncLazy<Executer>.Create<CronTask>(self => self.CreateExecuter(self.Command, self.Usings, self.RefAssemblies));
	protected Executer Executer => _executer.GetValue(this);

	static readonly bool _allowBackgroundRun = !Instances.Config.DisableCronTasks;
		
	protected bool IsOk => CronExpression.IsValid && Executer?.IsOk == true;
	public bool AllowBackgroundRun => _allowBackgroundRun && Enabled && IsOk;

	public bool IsExecuting;

	protected event EventHandler Done;

	Executer CreateExecuter(string code, string usings, string refAssemblies)
	{
		if(Command.IsEmpty())
			return null;

		var result = new Executer(code, Log)
		{
			Parameters =
			{
				{"log", typeof(ILog)},
				{"name", typeof(string)},
				{"filePath", typeof(string)},
				{"token", typeof(CancellationToken)}
			},
			//RefereceAllFromCurrentDomain = true
		};

		result.AddUsingFor(typeof(System.Collections.Specialized.NameValueCollection));
		result.AddUsingFor(typeof(Utils));
		result.AddUsingFor(typeof(Instances));
		result.AddUsingFor(typeof(User));
		result.AddUsingFor(typeof(CronTask));

		if(usings.IsSome())
			foreach (var str in usings.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries))
			{
				var ns = str.Trim();
				if (ns.IsSome())
					result.AdditionalUsings.Add(ns);
			}

		result.Alias("Tasks", "NetCore3Template.Models.Tasks");
		result.Alias("Services", "NetCore3Template.Models.Services");


		if(refAssemblies.IsSome())
			foreach (var s in refAssemblies.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries))
				result.RefAssembliesByName.Add(s.Trim());

		return result;
	}


	public void ScheduleNow(Action action = null)
	{
		void Handler(object sender, EventArgs e)
		{
			action();
			Done -= Handler;
		}
		if (action != null)
			Done += Handler;
			
		Subscription.Next = DateTime.Now;
	}
		
	async Task Execute()
	{
		if (!IsOk)
			return;

		using (Instances.Connection)
		{
			try
			{
				await ExecuteInt().NoCtx();
				UpdateNext(true);
			}
			catch (Exception ex)
			{
				ResultOk = false;
				Result = $"Error: {ex.Message}";
				Log.Error(ex, $"{Name} - task error:");

				if (OnErrorRetryInterval > 0)
					Subscription.Next = NextExecution = DateTime.Now.AddMilliseconds(OnErrorRetryInterval);
				else
					UpdateNext(true);
			}

			await SaveAsync().NoCtx();
		}
			
		Done?.Invoke(this, EventArgs.Empty);
	}

	async Task ExecuteInt()
	{
		try
		{
			if (Executer == null)
				throw new Exception("task command is empty");

			if (Executer.IsOk != true)
				throw new Exception(Executer.Errors.ToString(new EnumBuilder("\n")));

			IsExecuting = true;
			Log.Debug($"{Name} - task started ({TaskInfo.TaskName})");
			LastExecution = DateTime.Now;

			_cancellationTokenSource = new CancellationTokenSource();
				
			var res = await Executer.ExecuteAsync(Log, Name, FilePath, _cancellationTokenSource.Token).NoCtx();

			if (_cancellationTokenSource.IsCancellationRequested)
				Log.Debug($"{Name} - task cancelled");
			else
			{
				ResultOk = true;
				Result = res?.ToString();
				LastSuccess = LastExecution;
				Log.Debug($"{Name} - task finished");
			}
		}
		finally
		{
			IsExecuting = false;
		}
	}

	static readonly DateTime _startUpTime = DateTime.Now;

	void UpdateNext(bool force = false)
	{
		if (AllowBackgroundRun)
		{
			if (File.IsSome() && !System.IO.File.Exists(FilePath))
				NextExecution = DateTime.Now.AddSeconds(10);
			else if(Dependencies.IsSome() && LastExecution > _startUpTime)
				NextExecution = DateTime.MaxValue;
			else
			{
				var nxt = CronExpression.GetNext(DateTime.Now);

				if (force || NextExecution.IsMarginal() || NextExecution > nxt)
					NextExecution = nxt;
			}
		}
		else
			NextExecution = DateTime.MaxValue;

		if (Subscription != null)
			Subscription.Next = NextExecution;
	}

	public override string ToString() => Name.Or(() => base.ToString());
	public override void OnSave() => Utils.Nop(CronExpression);

	[GridValidation]
	protected void Validate(IValidationResponse response, string cronExpression, string command, string dependencies, string usings, string refAssemblies)
	{		
		CronExpression cronExpr = cronExpression;
		if (!cronExpr.IsValid)
			response.Error(this, t => CronExpression, $"<pre>{cronExpr.Errors.ToString(new EnumBuilder("\n"))}</pre>");

		if (command.IsSome())
		{
			var exe = CreateExecuter(command, usings, refAssemblies);
			if (!exe.IsOk)
				response.Error(this, t => Command, $"<pre>{exe.Errors.ToString(new EnumBuilder("\n"))}</pre>");
		}

		try { ParseDependencies(dependencies, true); }
		catch (Exception e) { response.Error(this, t => Dependencies, $"<pre>{e.Message}</pre>"); }
	}

	public static void InitAll() => All.ForEach(c => c.UpdateNext());

	static readonly KeyQueryCache<CronTask, string> _getByFile = new(task => task.File)
	{
		Prefetch = true, 
		CheckOnlyCache = true,
		CacheSettings =
		{
			SlidingExpiration = TimeSpan.MinValue,
			CacheAbsent = true
		}
	};

	public static bool TryGetByFile(string file, out CronTask cronTask)
		=> _getByFile.TryGet(file, out cronTask);
}