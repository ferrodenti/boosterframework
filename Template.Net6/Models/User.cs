using Booster;
using Booster.Interfaces;
using Booster.Mvc.Declarations;
using Booster.Mvc.Helpers;
using Booster.Orm;
using JetBrains.Annotations;

namespace Template.Net6.Models;

[DbDict]
[GridModel("Управление пользователями")]
[GridPermissions(UserPermission.EditUsers)]
public class User : IndexedEntity<User>, IPasswordValidationBytes
{
	[DbIndex, Grid]
	public string Login { get; set; }

	[Db] public byte[] PasswordHash { get; set; }
	[Db] public byte[] PasswordSalt { get; set; }

	[Db, Grid, EditorSelect("Role", Values = "{RoleValues}")]  
	public One<UserRole> RoleId { get; } = new();
	public UserRole Role
	{
		get => RoleId.Entity;
		set => RoleId.Entity = value;
	}
		
	[UsedImplicitly]
	protected Dictionary<int, string> RoleValues => UserRole.All.ToDictionary(r => r.Id, r => r.Name);

	[EditorPassword("New password", PreHtml = "<fieldset><legend>Change password</legend></fieldset>")]
	public string Password
	{
		get => null;
		set { if(value.IsSome()) this.SetPassword(value); }
	}

	[EditorPassword("Repeat password")]
	public string Password2 //TODO: общий метод валидации пароля (совпадение + сложность)
	{
		get => null;
		set => Utils.Nop(value);
	}

	// ReSharper disable once UnusedAutoPropertyAccessor.Global
	public bool IsAnon { get; init; }

	// readonly DependentLazy<Dictionary<int, int>> _relevantTagIds = DependentLazy<Dictionary<int, int>>.Create<User>(self =>
	// {
	// 	self._relevantTagIds.AddDependency<TagWeight>(w => w.UserId == self.Id);
	//
	// 	using (var conn = Instances.Connection)
	// 		return new Dictionary<int, int>(conn.ExecuteTuples<Tuple<int,int>>(
	// 			$@"select {nameof(TagWeight.TagId):?}, {nameof(TagWeight.Weight):?} from {typeof(TagWeight):?} 
	// 				where {nameof(TagWeight.UserId):?}={self.Id:@}").Select(t => new KeyValuePair<int, int>(t.Item1, t.Item2)));
	// });
	//
	// public Dictionary<int, int> RelevantTagIds => _relevantTagIds.GetValue(this);

	public override string ToString() => Login.Or(() => base.ToString());

	public static User GetAnonUser() => new()
	{
		Login = "anon",
		IsAnon = true
	};

	static readonly KeyQueryCache<User, string> _byLogin = new(u => u.Login);
		
	public static Task<User> GetByLogin(string login) 
		=> _byLogin.GetAsync(login);
		
	[Migration(MigrationOptions.TableCreated)]
	protected static async Task TableCreated(RepoInitContext e)
	{
		await e.WaitForAsync<UserRole>().NoCtx();

		await new User
		{
			Login = "admin", 
			Password = "123", 
			Role = await UserRole.GetByName("Администратор").NoCtx()
		}.InsertAsync().NoCtx();
			
		await new User
		{
			Login = "user", 
			Password = "test", 
			Role = await UserRole.GetByName("Пользователь").NoCtx()
		}.InsertAsync().NoCtx();
	}
}