using System.IO;
using System.Xml.Serialization;
using Booster;
using Booster.FileSystem;
using Booster.Mvc.Helpers;
using Microsoft.Extensions.Configuration;

namespace Template.Net6;

public class SiteConfig
{
	string _vendorName;
	[XmlElement("vendorName")]
	public string VendorName
	{
		get => _vendorName ??= ".rad";
		set => _vendorName = value;
	}
		
	string _applicationName;
	[XmlElement("applicationName")]
	public string ApplicationName
	{
		get => _applicationName ??= "NetCore3Template";
		set => _applicationName = value;
	}
		
	string _appData;
	[XmlElement("appData")]
	public string AppData
	{
		get => _appData ??= "~/App_Data";
		set => _appData = value;
	}
		
	string _logsPath;
	[XmlElement("logsPath")] public string LogsPath
	{
		get => _logsPath ?? $"{AppData}/logs/";
		set => _logsPath = value;
	}
		
	[XmlElement("disableCronTasks")] public bool DisableCronTasks { get; set; }
		
	FilePath _mappedAppData;
	[XmlIgnore] public FilePath MappedAppData => _mappedAppData ??= MapPath(AppData);
		
	string _mappedLogsPath;
	[XmlIgnore] public string MappedLogsPath => _mappedLogsPath ??= MapPath(LogsPath);
		
	public static FilePath MapPath(string path, bool create = true)
	{
		if (path.IsEmpty())
			return new FilePath("");
			
		if (MvcHelper.IsInitialized)
		{
			var result = MvcHelper.MapPath(path);
			if (create && !Directory.Exists(result))
				Directory.CreateDirectory(result);
				
			return result;
		}
		return null;
	}
		
	public IConfigurationSection Logs { get; set; }
}