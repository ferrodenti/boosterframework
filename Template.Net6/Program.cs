using System;
using System.IO;
using System.Text;
using Booster;
using Booster.DI;
using Booster.Log;
using Booster.Mvc;
using Booster.Mvc.Grid;
using Booster.Mvc.Helpers;
using Booster.Orm.Interfaces;
using Booster.Orm.PostgreSql;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Template.Net6;
using Template.Net6.Models;

var builder = WebApplication.CreateBuilder(args);
var services = builder.Services;
var configuration = builder.Configuration;

// Add services to the container.
//services.AddControllersWithViews();
Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

services.AddMemoryCache();
services.AddBoosterMvc(options =>
{
	options.EnableEndpointRouting = false;
});
			
services.AddSession();
services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
	.AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, options =>
	{
		options.LoginPath = "/login";
		options.SlidingExpiration = true;
		//options.ExpireTimeSpan = new TimeSpan(30, 0, 0, 0);
	});

var conn = configuration.GetConnectionString("local");
IOrm orm = new PostgreSqlOrm(conn, Instances.LogRoot);

InstanceFactory.Register(orm);
//InstanceFactory.Register(new EnumPermissionsProvider<UserPermission>(async () 
//	=> (await User.GetCurrent().NoCtx()).Role?.Permissions));
InstanceFactory.Register(new EnumPermissionsProvider<UserPermission>(() => UserPermission.All));
			
orm.Migration += (_, e) =>
{
	Utils.Nop(e.Message);
};

orm.Ping();
orm.RegisterRepos();
			
Grid.RegisterControllers(typeof(Instances).Assembly, Instances.LogRoot);

var app = builder.Build();
//MvcHelper.Init(app.Services);
			
AppDomain.CurrentDomain.SetData("DataDirectory", Path.Combine(app.Environment.ContentRootPath, "App_Data"));
AppDomain.CurrentDomain.SetData("Configuration", configuration);
			
var config = Instances.Config = new SiteConfig();
configuration.GetSection("Config").Bind(config);
		
var factory = new LogFactory(); //TODO: тут все упростить
factory.AddVariable("path", config.MappedLogsPath.TrimStart('~').TrimEnd('/', '\\'));
factory.Exception += (_, ea) => Try.IgnoreErrors(()
	=> File.AppendAllText(Path.Combine(config.MappedLogsPath, "init-log-errors.log"), $"Error: {ea.Exception}\n"));

Instances.LogRoot = factory.Create(config.Logs);
InstanceFactory.Register(Instances.LogRoot);
			
Instances.LogRoot.Info("Application is starting...");

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
	app.UseExceptionHandler("/Home/Error");
	// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
	app.UseHsts();
}

app.UseHttpsRedirection();
app.UseBoosterStaticFiles();
app.UseSession();
app.UseRouting();
app.UseAuthorization();
app.UseStatusCodePagesWithReExecute("/error/{0}");
app.UseBoosterMvc(routes =>
{
	routes.MapRoute("login", "login", new {controller = "auth", action = "login", area = ""});
	routes.MapRoute("home", "{area:exists}/{action:exists}", new {Controller = "Home"});
	routes.MapRoute("areas", "{area:exists}/{controller=Home}/{action=Index}");
	routes.MapRoute("default", "{controller=Home}/{action=Index}", new {area = ""});
	routes.MapBoosterGrid();
});
			
CronTask.InitAll();
app.Run();
