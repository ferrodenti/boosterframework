using System;
using Booster;
using Booster.DI;
using Booster.Log;
using Booster.Orm.Interfaces;

namespace Template.Net6;

public static class Instances
{
	public static string AppData => _appData.Value;
	static readonly Lazy<string> _appData = new(() => AppDomain.CurrentDomain.GetData("DataDirectory")?.ToString());
		
	public static SiteConfig Config { get; internal set; }
	public static ILog LogRoot { get; internal set; }

	static readonly Lazy<IOrm> _orm = new(() => InstanceFactory.Get<IOrm>());
	public static IOrm Orm => _orm.Value;
	public static IConnection Connection => Orm.GetCurrentConnection();
		
	static readonly Lazy<TimeSchedule> _taskSchedule = new(() => new TimeSchedule(LogRoot, "CronSchedule"));
	public static TimeSchedule TaskSchedule => _taskSchedule.Value;
}