﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using JetBrains.Annotations;
using OpenCvSharp;
using OpenCvSharp.Extensions;
using Size = OpenCvSharp.Size;

namespace Booster.Cv;

[PublicAPI]
public class MatPool : IDisposable
{
	static readonly Lazy<MatPool> _default = new(() => new MatPool());
	public static MatPool Default => _default.Value;

	public TimeSpan GcInterval
	{
		get => _gcSubscription.Interval;
		set => _gcSubscription.Interval = value;
	}

	readonly TimeSchedule.Subscription _gcSubscription;
	readonly List<MatPoolItem> _items = new();
	readonly object _lock = new();

	public MatPool()
		=> _gcSubscription = TimeSchedule.GC.Subscribe(this, TimeSpan.FromMilliseconds(10 * 1000), Gc);

	void Gc()
	{
		var interval = GcInterval;
		lock (_lock)
			foreach (var item in _items.ToArray())
				if (item.LastAccess < DateTime.Now - interval)
				{
					_items.Remove(item);
					item.Destroy();
				}
	}

	public MatPoolItem GetLike(Mat mat) //TODO: странное название
		=> Get(mat.Size(), mat.Type());

	public MatPoolItem Get(Size size, MatType type)
	{
		lock (_lock)
		{
			MatPoolItem bestMatch = null;
			var bestMatchDiff = int.MaxValue;

			var wh = size.Width + size.Height;

			foreach (var item in _items.Where(i => i.Type == type && i.Size.Width >= size.Width && i.Size.Height >= size.Height))
			{
				if (item.Size == size)
					lock (item)
					{
						_items.Remove(item);
						item.LastAccess = DateTime.Now;
						return item;
					}

				var diff = item.Size.Width + item.Size.Height - wh;

				if (diff < bestMatchDiff)
				{
					bestMatch = item;
					bestMatchDiff = diff;
				}
			}

			if (bestMatch == null)
				return new MatPoolItem(this, new Mat(size, type));

			bestMatch.LastAccess = DateTime.Now;
			_items.Remove(bestMatch);
			return new MatPoolItem(bestMatch, size);
		}
	}

	public MatPoolItem FromBitmap(Bitmap src)
	{
		var cols = src?.Width ?? throw new ArgumentNullException(nameof(src));
		var height = src.Height;
		var ch = src.PixelFormat switch
		         {
			         PixelFormat.Format24bppRgb    => 3,
			         PixelFormat.Format32bppRgb    => 3,
			         PixelFormat.Format1bppIndexed => 1,
			         PixelFormat.Format8bppIndexed => 1,
			         PixelFormat.Format32bppPArgb  => 4,
			         PixelFormat.Format32bppArgb   => 4,
			         _                             => throw new NotImplementedException()
		         };

		var result = Get(new Size(cols, height), MatType.CV_8UC(ch));

		// ReSharper disable once InvokeAsExtensionMethod
		BitmapConverter.ToMat(src, result.Mat);

		return result;
	}

	public MatPoolItem Create(Mat mat)
		=> new(this, mat);

	public MatPoolItem Create()
		=> new(this, new Mat());

	public void Release(MatPoolItem item)
	{
		//item.LastAccess = DateTime.Now;
			
		lock (_lock)
			_items.Add(item);
	}

	public void Dispose()
	{
		_gcSubscription.Dispose();

		lock (_lock)
		{
			foreach (var item in _items)
				item.Destroy();

			_items.Clear();
		}
	}
}