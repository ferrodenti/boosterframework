﻿using System;
using OpenCvSharp;

namespace Booster.Cv;

public class MatPoolItem : IDisposable
{
	readonly MatPoolItem _larger;
	readonly MatPool _pool;

	public Mat Mat;
	public DateTime LastAccess = DateTime.Now;

	public Size Size => Mat.Size();
	public MatType Type => Mat.Type();

	public int Width => Mat.Width;
	public int Height => Mat.Height;

	public MatPoolItem(MatPool pool, Mat mat)
	{
		_pool = pool;
		Mat = mat;
	}

	public MatPoolItem(MatPoolItem larger, Size size)
	{
		_larger = larger;
		Mat = new Mat(_larger.Mat, new Rect(new Point(), size));
	}

	public static implicit operator Mat(MatPoolItem item)
		=> item.Mat;

	public void Dispose()
	{
		if (_larger != null)
		{
			Mat.Dispose();
			_larger.Dispose();
		}
		else
			_pool.Release(this);
	}

	public void Destroy()
		=> Mat.Dispose();
}