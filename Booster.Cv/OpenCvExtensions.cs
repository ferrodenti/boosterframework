﻿using System;
using JetBrains.Annotations;
using OpenCvSharp;

namespace Booster.Cv;

[PublicAPI]
public static class OpenCvExtensions
{
	public static Point2d Div(this Point p, double v)
		=> new(p.X / v, p.Y / v);

	public static Point2d Div(this Point2d p, double v)
		=> new(p.X / v, p.Y / v);

	public static Point2d Div(this Point2f p, double v)
		=> new(p.X / v, p.Y / v);

	public static Point2d Mul(this Point p, double v)
		=> new(p.X * v, p.Y * v);

	public static Point2d Mul(this Point2d p, double v)
		=> new(p.X * v, p.Y * v);

	public static Point2d Mul(this Point2f p, double v)
		=> new(p.X * v, p.Y * v);


	public static Rect Div(this Rect p, double v)
		=> new((int)(p.X / v), (int)(p.Y / v), (int)(p.Width / v), (int)(p.Height / v));

	public static Rect Mul(this Rect p, double v)
		=> new((int)(p.X * v), (int)(p.Y * v), (int)(p.Width * v), (int)(p.Height * v));

	public static Size Div(this Size p, double v)
		=> new((int)(p.Width / v), (int)(p.Height / v));

	public static Size Mul(this Size p, double v)
		=> new((int)(p.Width * v), (int)(p.Height * v));

	public static Size Add(this Size a, Size b)
		=> new(a.Width + b.Width, a.Height + b.Height);

	public static Point ToPoint(this Size a)
		=> new(a.Width, a.Height);
	public static Size ToSize(this Point a)
		=> new(a.X, a.Y);

	public static Point Center(this Rect p)
		=> new(p.X + p.Width / 2, p.Y + p.Height / 2);

	public static double Sq(this double a)
		=> a * a;

	public static double Sq(this int a)
		=> a * a;

	public static double Len(this Point a, Point b)
		=> Math.Sqrt(Sq(a.X - b.X) + Sq(a.Y - b.Y));

	public static double Len(this Point a)
		=> Math.Sqrt(Sq(a.X) + Sq(a.Y));

	public static double Len(this Point2d a)
		=> Math.Sqrt(Sq(a.X) + Sq(a.Y));

	public static double Len(this Size a)
		=> Math.Sqrt(Sq(a.Width) + Sq(a.Height));

	public static double LenSq(this Point a, Point b)
		=> Sq(a.X - b.X) + Sq(a.Y - b.Y);

	public static double LenSq(this Point a)
		=> Sq(a.X) + Sq(a.Y);

	public static double LenSq(this Point2d a)
		=> Sq(a.X) + Sq(a.Y);

	public static double LenSq(this Size a)
		=> Sq(a.Width) + Sq(a.Height);


	public static Rect RectFromCenter(int x, int y, int width, int height)
		=> new(x - width / 2, y - height / 2, width, height);

	public static Rect RectFromCenter(Point center, int width, int height)
		=> RectFromCenter(center.X, center.Y, width, height);
	public static Rect RectFromCenter(Point center, Size size)
		=> RectFromCenter(center.X, center.Y, size.Width, size.Height);


	public static Rect Limit(this Rect rect, Size size)
	{
		var tl = new Point(Math.Max(0, rect.Left), Math.Max(0, rect.Top));
		var br = new Point(Math.Min(size.Width, rect.Right), Math.Min(size.Height, rect.Bottom));

		return new Rect(tl, (br - tl).ToSize());
	}

	public static Mat GetPaddedRoi(this Mat input, Rect rect, Scalar paddingColor)
	{
		var left = rect.Left;
		var top = rect.Top;
		var width = rect.Width;
		var height = rect.Height;

		var right = rect.Right;
		var bottom = rect.Bottom;

		if (left < 0 || top < 0 || right > input.Width || bottom > input.Rows)
		{
			// border padding will be required
			int borderLeft = 0, borderRight = 0, borderTop = 0, borderBottom = 0;

			if (left < 0)
			{
				width += left;
				borderLeft = -left;
				left = 0;
			}

			if (top < 0)
			{
				height += top;
				borderTop = -top;
				top = 0;
			}

			if (right > input.Width)
			{
				width -= right - input.Width;
				borderRight = right - input.Width;
			}

			if (bottom > input.Height)
			{
				height -= bottom - input.Height;
				borderBottom = bottom - input.Height;
			}

			var result = new Mat();
			Cv2.CopyMakeBorder(new Mat(input, new Rect(left, top, width, height)), result, borderTop, borderBottom, borderLeft, borderRight, BorderTypes.Constant, paddingColor);

			return result;
		}
		return new Mat(input, new Rect(left, top, width, height));
	}

	public static Size FitSize(this Size src, Size frame)
	{
		var w = frame.Width;
		var aspect = (double) src.Width / src.Height;
		var h = (int) (w / aspect);

		if (h > frame.Height)
		{
			h = frame.Height;
			w = (int) (h * aspect);
		}

		return new Size(w, h);
	}
}