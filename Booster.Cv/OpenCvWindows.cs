﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Booster.DI;
using Booster.Log;
using Booster.Synchronization;
using OpenCvSharp;

namespace Booster.Cv;

public class OpenCvWindows : BaseBackgroundExecutor
{
	class Frame
	{
		public Mat Mat;
		public bool Dispose;
	}

	static readonly Lazy<OpenCvWindows> _default = new(() => new OpenCvWindows(InstanceFactory.Get<ILog>(false)));
	public static OpenCvWindows Default => _default.Value;
	readonly ConcurrentDictionary<string, Frame> _frames = new();

	int _delay;
	int _fps;
	public int Fps
	{
		get => _fps;
		set
		{
			_fps = value;
			_delay = _fps > 0 ? 1000 / value : 0;
		}
	}


	public OpenCvWindows(ILog log) : base(log)
		=> Fps = 10;

	public void Show(string name, Mat mat, bool dispose = true)
	{
		var frame = _frames.GetOrAdd(name, _ => new Frame());
		lock (frame)
		{
			if (frame.Dispose)
				frame.Mat?.Dispose();

			frame.Mat = mat;
			frame.Dispose = dispose;
		}

		EnsureTaskRunning();
	}

	protected override Task<bool> ProccessOne()
	{
		Cv2.WaitKey(_delay);

		foreach (var pair in _frames)
		{
			var frame = pair.Value;
			lock (frame)
			{
				var mat = frame.Mat;
				if (mat != null)
				{
					Cv2.ImShow(pair.Key, mat);

					if (frame.Dispose)
						mat.Dispose();

					frame.Mat = null;
				}
			}
		}

		return Task.FromResult(!_frames.IsEmpty);
	}

	protected override bool CheckDone()
		=> _frames.IsEmpty;
}