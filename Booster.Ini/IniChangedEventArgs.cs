using System;
using System.Collections.Generic;

namespace Booster.Ini;

public class IniChangedEventArgs : EventArgs
{
	public Dictionary<string, object> Changes;
	public IniChangedEventArgs(Dictionary<string, object> changes)
	{
		Changes = changes;
	}
}