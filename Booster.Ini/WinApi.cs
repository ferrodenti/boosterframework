﻿using System.Runtime.InteropServices;

namespace Booster.Ini;

public static class WinApi
{
	[DllImport("KERNEL32.DLL", EntryPoint = "GetPrivateProfileStringW", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
	public static extern int GetPrivateProfileString(string lpAppName, string lpKeyName, string lpDefault, string lpReturnString, int nSize, string lpFilename);

	[DllImport("kernel32")]
	public static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
}