﻿using System;
using System.Collections.Generic;
using Booster.Reflection;

namespace Booster.Ini;

public interface IIniInternal : IIni, IDictionary<string, object>
{
	IniSerializer Serializer { get; }
	Dictionary<string, Tuple<TypeEx, object>> Fields { get; }
}