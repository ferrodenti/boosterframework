﻿using System;
using System.Linq.Expressions;

namespace Booster.Ini;

public interface IIni
{
}

public interface IIni<TSelf> : IIni
{
	IniEventWraper On(params Expression<Func<TSelf, object>>[] fields);
	void Bind<T>(Expression<Func<TSelf, T>> field, Action<T> setter);
}