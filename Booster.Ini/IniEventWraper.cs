﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Booster.Ini;

public class IniEventWraper
{
	readonly IniSerializer _serializer;
	readonly HashSet<string> _keys;
	bool _isAssigned;

	public IniEventWraper(IniSerializer serializer, HashSet<string> keys)
	{
		_serializer = serializer;
		_keys = keys;
	}

	void changedHandler(object sender, IniChangedEventArgs e)
	{
		var handler = _changed;
		if (handler != null)
			if (e.Changes.Keys.Any(key => _keys.Contains(key)))
				handler(((IniSerializer) sender).Ini, e);
	}

	EventHandler<IniChangedEventArgs> _changed;
	public event EventHandler<IniChangedEventArgs> Changed
	{
		[MethodImpl(MethodImplOptions.Synchronized)]
		add
		{
			_changed = (EventHandler<IniChangedEventArgs>) Delegate.Combine(_changed, value);

			if (!_isAssigned)
			{
				_serializer.Changed += changedHandler;
				_isAssigned = true;
			}
		}
		[MethodImpl(MethodImplOptions.Synchronized)]
		remove
		{
			_changed = (EventHandler<IniChangedEventArgs>) Delegate.Remove(_changed, value);

			if (_isAssigned && (_changed == null || _changed.GetInvocationList().Length == 0))
			{
				_serializer.Changed -= changedHandler;
				_isAssigned = false;
				_serializer.RemoveEventWraper(this);
			}

		}
	}

	public override bool Equals(object obj)
		=> obj is IniEventWraper b && b._serializer == _serializer && _keys.SequenceEqual(b._keys);

	public override int GetHashCode()
	{
		unchecked
		{
			var i = _serializer.GetHashCode();
			foreach (var key in _keys)
				i += key.GetHashCode();
			return i;
		}
	}
}