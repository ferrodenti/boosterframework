using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using Booster.CodeGen;
using Booster.Log;
using Booster.Reflection;
using JetBrains.Annotations;

namespace Booster.Ini;

[PublicAPI]
public class IniOrm<TConfig> where TConfig : IIni
{
	public bool CreateIfNotExists { get; set; } = true;

	public bool WriteDefaults { get; set; } = true;

	public bool AllowDebugDynamicSource { get; set; } = Debugger.IsAttached;

	public TConfig Load(string filename, ILog log)
	{
		if (!Path.IsPathRooted(filename))
			filename = Path.Combine(Directory.GetCurrentDirectory(), filename);

		if (!File.Exists(filename))
		{
			if (CreateIfNotExists)
				File.Create(filename);
			else
				throw new FileNotFoundException(filename);
		}

		var config = (TConfig) Activator.CreateInstance(ConfigType);
// ReSharper disable once PossibleInvalidCastException
		((IIniInternal)config).Serializer.Init(filename, log, WriteDefaults);

		//foreach (var pair in ((IIniInternal) config))
		//    logger.SafeWrite("{0}={1}", pair.Key, pair.Value);

		return config;
	}


// ReSharper disable once StaticFieldInGenericType
	static TypeEx? _configType;

	protected TypeEx ConfigType => _configType ??= CreateConfigType();

	protected TypeEx CreateConfigType()
	{
		var assys = new HashSet<Assembly>();
		var className = GetDynamicClassName(typeof(TConfig));
		var code = EmitConfigSource(className, assys);

		return new Compiler(null) {AllowDebugDynamicSource = AllowDebugDynamicSource} //TODO: add logging
			.CompileType(code, "Dynamic." + className, assys, null).NoCtxResult();
	}

	protected string GetDynamicClassName(TypeEx type)
	{
		var className = $"{type.Name}_implementation";
		var res = className;
		for (var i = 0;; i++)
		{
			if (TypeEx.Get(string.Join(".", "Dynamic", res), false, true) == null)
				return res;

			res = string.Join("", className, i);
		}
	}

	protected string EmitConfigSource(string className, HashSet<Assembly> assys)
	{
		TypeEx configType = typeof (TConfig);
		var src = new StringBuilder();

		var over = typeof (TConfig).IsInterface ? "" : " override";

		// Объявляем сам динамический класс
		src.AppendFormat(@"
namespace Dynamic
{{
	internal class {0} : {2}, {1}
	{{
		private {3} _serializer;
		public {3} Serializer {{ get{{ return _serializer ?? (_serializer = new {3}(this)); }} }}
", className, EmitType<IIniInternal>(assys), EmitType(configType, assys), EmitType<IniSerializer>(assys));

		var iniT = configType.FindInterface(typeof(IIni<>)); // Если класс поддерживает события изменения полей, надо создать соответствующий метод подписки на события

		if (iniT != null ) 
		{
			var arg = iniT.GenericArguments[0];
			TypeEx onArgs = typeof (Expression<>).MakeGenericType(typeof (Func<,>).MakeGenericType(arg, typeof (object))).MakeArrayType();

			if (configType.IsInterface || configType.FindMethod("On", onArgs).IsAbstract)
			{
				EmitType(typeof(Expression<>), assys); //Чтобы просто включить нужную сборку
				src.AppendFormat(@"
	public{2} {0} On(params System.Linq.Expressions.Expression<System.Func<{1},object>>[] fields )
	{{
		return this.Serializer.On(fields);
	}}", EmitType<IniEventWraper>(assys), EmitType(arg, assys), configType.IsInterface ? "" : " override");
			}
			if (configType.IsInterface || configType.FindMethod("Bind").IsAbstract)
			{
				EmitType(typeof(Expression<>), assys); //Чтобы просто включить нужную сборку
				src.AppendFormat(@"
	public{1} void Bind<T>(System.Linq.Expressions.Expression<System.Func<{0},T>> field, System.Action<T> setter)
	{{
		this.Serializer.Bind(field, setter);
	}}", EmitType(arg, assys), configType.IsInterface ? "" : " override");
						
			}
		}

		var fields = new Dictionary<string, Tuple<TypeEx, object>>();

		foreach (var pi in TypeEx.Get<TConfig>().Properties) // Идем по всем свойствам
		{
			if (pi.PropertyType.FindInterface(typeof (IIniSection)) != null ) // Ищем объявления секций
			{
				if (pi.GetMethod.IsAbstract)
				{
					var secClassName = GetDynamicClassName(pi.PropertyType); // Объявляем классы для секций
					var sectionType = pi.PropertyType;
					src.AppendFormat(@"
		public class {0} : {1}
		{{
			public {2} Serializer {{ get; private set; }}
			public {0}({2} serializer)
			{{
				Serializer = serializer;
			}}
			", secClassName, EmitType(sectionType, assys), EmitType<IniSerializer>(assys));

					foreach (var pi1 in sectionType.Properties) // Идем по свойствам секции...
						EmitProperty(src, pi1, pi.Name, fields);

					var configSectionT = pi.PropertyType.FindInterface(typeof(IIniSection<>)); // Если секция поддерживает инициализацию, но метод не объявлен или абстрактный
					if (configSectionT != null)
					{
						var arg = configSectionT.GenericArguments[0];
						if (sectionType.IsInterface || pi.PropertyType.FindMethod("Init", arg)?.IsAbstract == true) // Генерим инициализатор
							src.AppendFormat(@"
			public{1} void Init({0} ini)
			{{
			}}", EmitType(arg, assys), sectionType.IsInterface ? "" : "override");
					}

					src.Append(@"
		}");

					src.AppendFormat(configSectionT != null // Генерим свойство для секции 2мя способами в зависимости от того, поддерживается ли инициализация
						? @"
		private {0} {1};
		public{4} {0} {2} 
		{{
			get
			{{
				if({1} == null)
				{{
					{1} = new {3}(this.Serializer);
					{1}.Init(this);
				}}
				return {1};
			}}
		}}"
						: @"
		private {0} {1};
		public{4} {0} {2} {{ get {{ return {1} ?? ({1} = new {3}(this.Serializer)); }} }}
", EmitType(pi.PropertyType, assys), char.ToLower(pi.Name[0]) + pi.Name.Substring(1), pi.Name, secClassName, over);
				}
				continue;
			}
			EmitProperty(src, pi, null, fields);
		}
		src.AppendFormat(@"

		object _lock = new object();
		private System.Collections.Generic.Dictionary<string, System.Tuple<{0}, object>> _fields;
		public System.Collections.Generic.Dictionary<string, System.Tuple<{0}, object>> Fields 
		{{
			get
			{{
				if( this._fields == null )
					lock(this._lock)
					{{
						if(this._fields == null )
						{{
							var dict = new System.Collections.Generic.Dictionary<string, System.Tuple<{0}, object>>(System.StringComparer.OrdinalIgnoreCase);", EmitType(typeof(TypeEx), assys));

		foreach (var pair in fields) // Наконец, генерим свойство с описанием всех полей
		{
			var def = pair.Value.Item2 != null ? $"\"{pair.Value.Item2}\"" : "null";
			src.AppendFormat(@"
							this.Serializer.AddField(dict, ""{0}"", typeof({1}), {2});", pair.Key, EmitType(pair.Value.Item1, assys), def);
		}
		src.Append(@"
							this._fields = dict;
						}
					}
				return this._fields;
			}
		}

		public System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<string, object>> GetEnumerator() { return this.Serializer.Dict.GetEnumerator(); }
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() { return this.GetEnumerator(); }

		bool System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<string, object>>.Contains(System.Collections.Generic.KeyValuePair<string, object> item)
		{
			object obj;
			return TryGetValue(item.Key, out obj) && Equals(obj, item.Value);
		}
		void System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<string, object>>.CopyTo(System.Collections.Generic.KeyValuePair<string, object>[] array, int arrayIndex)
		{
			((System.Collections.Generic.IDictionary<string, object>) this.Serializer.Dict).CopyTo(array, arrayIndex);
		}

		void System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<string, object>>.Clear() { throw new System.NotSupportedException(); }
		bool System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<string, object>>.Remove(System.Collections.Generic.KeyValuePair<string, object> item) { throw new System.NotSupportedException(); }
		bool System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<string, object>>.IsReadOnly { get { return false; } }
		void System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<string, object>>.Add(System.Collections.Generic.KeyValuePair<string, object> item) { this.Add(item.Key, item.Value); }
		bool System.Collections.Generic.IDictionary<string, object>.Remove(string key) { throw new System.NotSupportedException(); }

		public int Count { get { return this.Serializer.Dict.Count; } }
		public bool ContainsKey(string key) { return this.Serializer.Dict.ContainsKey(key); }
		public void Add(string key, object value) { this[key] = value; }
		public bool TryGetValue(string key, out object value) { return this.Serializer.Dict.TryGetValue(key, out value); }
		public System.Collections.Generic.ICollection<string> Keys { get { return this.Serializer.Dict.Keys; } }
		public System.Collections.Generic.ICollection<object> Values { get { return this.Serializer.Dict.Values; } }

		public object this[string key]
		{
			get
			{ 
				object res;
				this.TryGetValue(key, out res);
				return res;
			}
			set { this.Serializer.Set(key, value); }
		}
	}
}"); // И заканчиваем реализацией IDictionary<string, object>
		return src.ToString();
	}

	protected string EmitType<T>(HashSet<Assembly> assys)
		=> EmitType(typeof(T), assys);

	protected string EmitType(TypeEx type, HashSet<Assembly> assys)
	{
		if (!assys.Contains(type.Assembly))
			assys.Add(type.Assembly);

		var res = type.FullName;
		res = res.Replace('+', '.');
		return res;
	}

	static void EmitProperty(StringBuilder src, Property pi, string section, Dictionary<string, Tuple<TypeEx, object>> fields)
	{
		string ident;
		if (section == null)
		{
			section = "";
			ident = "";
		}
		else
			ident = "\t";

		var gm = pi.GetMethod;
		var sm = pi.SetMethod;

		if ((gm != null && gm.IsAbstract) || (sm != null && sm.IsAbstract))
		{
// ReSharper disable once PossibleNullReferenceException
			src.AppendFormat(@"
{0}		public{3} {1} {2}
{0}		{{", ident, pi.PropertyType, pi.Name, pi.DeclaringType.IsInterface ? "" : " override");

			if (gm != null && gm.IsAbstract)
				src.AppendFormat(@"
{0}			get {{ return this.Serializer.Get<{1}>(""{2}.{3}"");  }}", ident, pi.PropertyType, section, pi.Name);

			if (sm != null && sm.IsAbstract)
				src.AppendFormat(@"
{0}			set {{ this.Serializer.Set(""{1}.{2}"", value);  }}", ident, section, pi.Name);
			src.AppendFormat(@"
{0}		}}", ident);
		}

		object def = null;
		var attr = pi.FindAttribute<DefaultValueAttribute>();

		if (attr != null)
			def = attr.Value;

		fields[string.Join(".", section, pi.Name)] = Tuple.Create(pi.PropertyType, def);
	}
}