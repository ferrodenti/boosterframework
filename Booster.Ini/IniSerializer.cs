using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using Booster.Log;
using Booster.Reflection;

namespace Booster.Ini;

public class IniSerializer
{
	public readonly IIniInternal Ini;

	FileSystemWatcher _watcher;
	Timer _reloadTimer;

	string _fileName;
	public string FileName
	{
		get => _fileName;
		set
		{
			if (_fileName != value)
			{
				_watcher?.Dispose();

				_fileName = value;
				if (_fileName == null)
					return;

				_watcher = new FileSystemWatcher(Path.GetDirectoryName(_fileName) ?? Directory.GetCurrentDirectory(), Path.GetFileName(_fileName))
				{
					NotifyFilter = NotifyFilters.CreationTime | NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName | NotifyFilters.Size,
					EnableRaisingEvents = true
				};

				_watcher.Changed += _watcher_proc;
				_watcher.Created += _watcher_proc;
				_watcher.Deleted += _watcher_proc;
				_watcher.Renamed += _watcher_proc;
			}
		}
	}

	public ILog Log { get; set; }

	readonly ConcurrentDictionary<string, object> _dict = new(StringComparer.OrdinalIgnoreCase);
	public ConcurrentDictionary<string, object> Dict
	{
		get
		{
			lock (_loaded)
				_loaded.WaitOne();

			return _dict;
		}
	}

	public bool WriteDefaults { get; set; }

	public event EventHandler<IniChangedEventArgs> Changed;

	protected virtual void OnChanged(Dictionary<string,object> changes)
		=> Changed?.Invoke(this, new IniChangedEventArgs(changes));

	void _watcher_proc(object sender, FileSystemEventArgs e)
	{
		lock (_loaded)
			if (!_loaded.WaitOne(1))
				return;

		lock (FileName)
		{
			if (_reloadTimer != null)
			{
				_reloadTimer.Dispose();
				_reloadTimer = null;
			}

			_reloadTimer = new Timer(_ =>
			{
				Log.Debug($"Файл {FileName} был изменен");
				Load(true, WriteDefaults);
			}, null, 300, Timeout.Infinite);
		}
	}


	public IniSerializer(IIniInternal ini)
	{
		Ini = ini;
	}

	public void Init(string fileName, ILog log, bool andWriteDefaults)
	{
		FileName = fileName;
		Log = log.Create(this, new {FileName = fileName});
		WriteDefaults = andWriteDefaults;

		Load(false, andWriteDefaults);
	}

	public void AddField(Dictionary<string, Tuple<TypeEx, object>> fields, string key, TypeEx type, string @default)
	{
		object val = null;
		if( @default != null)
			try
			{
				val = Parse(type, @default);
			}
			catch (Exception ex)
			{
				Log.Warn(ex, $"Не могу распарсить значение по умолчания конфига {key}({type.Name})=\"{@default}\" : {ex.Message}");
			}

		fields[key] = Tuple.Create(type, val);
	}

	readonly ManualResetEvent _loaded = new(true);

	protected void Load(bool generateEvents, bool andWriteDefaults)
	{
		lock (_loaded)
		{
			_loaded.Reset();

			var changes = generateEvents ? new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase) : null;

			try
			{
				var fields = new Dictionary<string, Tuple<TypeEx, object>>(Ini.Fields, StringComparer.OrdinalIgnoreCase);

				Log.Trace($"Загружаю {FileName}...");

				foreach (var section in GetInternal())
				foreach (var key in GetInternal(section))
					try
					{
						if (string.IsNullOrWhiteSpace(key))
							continue;

						var value = GetInternal(section, key).FirstOrDefault();
						var keyFull = string.Join(".", section, key);

						if (fields.TryGetValue(keyFull, out var field))
						{
							fields.Remove(keyFull);
							SetFieldValue(keyFull, field.Item1, value, field.Item2, generateEvents, andWriteDefaults, changes);
						}
						else
							Log.Warn($"Поле '{keyFull}' не ожидалось");
					}
					catch (Exception ex)
					{
						Log.Error(ex, $"Ошибка чтения поля {key}: {ex.Message}");
					}

				foreach (var pair in fields)
					if (pair.Value.Item2 != null)
						try
						{
							SetFieldValue(pair.Key, null, null, pair.Value.Item2, generateEvents, andWriteDefaults, changes);
						}
						catch (Exception ex)
						{
							Log.Error(ex, $"Ошибка записи {pair.Key}: {ex.Message}");
						}
			}
			catch (Exception ex)
			{
				Log.Error(ex, $"Ошибка чтения: {ex.Message}");
			}

			_loaded.Set();

			if (generateEvents && changes.Count > 0)
				OnChanged(changes);
		}
	}

	void SetFieldValue(string key, TypeEx type, string value, object @default, bool generateEvents, bool andWriteDefaults, Dictionary<string, object> changes)
	{
		var message = new StringBuilder();
		object val = null;
		if (value != null)
			try
			{
				val = Parse(type, value);
			}
			catch (Exception ex)
			{
				message.AppendFormat("Не могу распарсить значение кофига {0}({1})=\"{2}\" : {3}", key, type.Name, value, ex);
			}

		if (val == null && @default != null)
		{
			if (message.Length == 0)
				message.AppendFormat("{0} не найден", key);

			val = @default;

			if (andWriteDefaults)
			{
				SetInternal(key, val);
				message.Append(", записываю");
			}
			else
				message.Append(", использу");

			message.AppendFormat(" значение по умолчанию = \"{0}\"", @default);
		}

		if (message.Length > 0)
			Log.Warn(message.ToString());

		if (generateEvents && _dict.TryGetValue(key, out var oldValue) && !Equals(val, oldValue))
			changes[key] = oldValue;
			
		_dict[key] = val; // Здесь нужен именно прямой доступ к коллекции, для успешной загрузки
	}

	protected object Parse(TypeEx type, string value)
	{
		if (type.IsEnum && !string.IsNullOrEmpty(value))
			return Enum.Parse(type, value.Replace('|', ','), true);

		if (type.FindInterface<IConvertible>() != null)
			return Convert.ChangeType(value, type);

		if (type == typeof (DateTime))
			return DateTime.Parse(value);

		if (type == typeof (TimeSpan))
			return TimeSpan.Parse(value ?? throw new ArgumentNullException(nameof(value)));

		throw new InvalidCastException($"Не получается преобразовать \"{value}\" в тип {type.FullName}");
	}

	public TValue Get<TValue>(string key)
	{
		if (Dict.TryGetValue(key, out var value))
			return (TValue) value;

		return default;
	}


	public void Set(string key, object value)
	{
		if (Dict.TryGetValue(key, out var old) && Equals(old, value))
			return;

		if(!Ini.Fields.TryGetValue(key, out var o))
			throw new ArgumentException($"Неправильный тип \"{key}\"");

		if (value != null )
		{
			TypeEx type = value.GetType();
			if (type != o.Item1)
			{
				if (type == typeof (string))
					value = Parse(o.Item1, (string) value);
				else
					throw new ArgumentException($"Не могу преобразовать \"{value}\"({type.Name}) в {o.Item1.Name}");
			}
		}

		Dict[key] = value;
		SetInternal(key, value);
		OnChanged(new Dictionary<string, object> {{key, old}});
	}

	protected IEnumerable<string> GetInternal(string section = null, string key = null)
	{
		if (section == null && key != null)
			throw new ArgumentException($"null.{key}: Поля без секции пока ещё не поддерживаются библиотекой");

		var res = new string(' ', 65536);
		WinApi.GetPrivateProfileString(section, key, null, res, 32768, FileName);

		return res.SplitNonEmpty('\0');
	}

	protected void SetInternal(string key, object value)
	{
		var i = key.IndexOf('.');
		var section = i > 0 ? key.Substring(0, i) : null;
		var key1 = i >= 0 ? key.Substring(i + 1) : key;

		SetInternal(section, key1, value.ToString());
	}

	protected void SetInternal(string section, string key, string value)
	{
		if (section == null)
			throw new ArgumentException($"null.{key}={value}: Поля без секции пока ещё не поддерживаются библиотекой");

		if (string.IsNullOrWhiteSpace(FileName))
			Log.Warn($"Попытка записать {section}.{key}={value} до того, как конфиг загружен. Значения будут перезаписаны при загрузке конфига");
		else
			WinApi.WritePrivateProfileString(section, key, value, FileName);
	}

	readonly ConcurrentDictionary<IniEventWraper, IniEventWraper> _eventWrapers = new();

	public IniEventWraper On(LambdaExpression[] fields)
	{
		var keys = new HashSet<string>();
		foreach (var expression in fields)
		{
			var memebers = UnwarpMemberAccessExpression(expression.Body);
			if (memebers.Count > 2)
				throw new ArgumentException("Expression's {0} member access too deep to map is with the iniOrm");

			keys.Add(string.Join(".", memebers));
		}
		var wr = new IniEventWraper(this, keys);
		return _eventWrapers.GetOrAdd(wr, wr);
	}

	public void Bind<T>(LambdaExpression field, Action<T> set)
	{
		var memebers = UnwarpMemberAccessExpression(field.Body);

		var key = string.Join(".", memebers);
		set(Get<T>(key));

		var wr = new IniEventWraper(this, new HashSet<string> { key });
		_eventWrapers.GetOrAdd(wr, wr);
		wr.Changed += (_, _) => set(Get<T>(key));
	}

	internal void RemoveEventWraper(IniEventWraper wraper)
		=> _eventWrapers.TryRemove(wraper, out wraper);

	protected List<string> UnwarpMemberAccessExpression(Expression expr, int rec = 0)
	{
		if (expr.NodeType == ExpressionType.Convert)
		{
			if (expr is not UnaryExpression uexp)
				throw new ArgumentException("Could not parse expression: UnaryExpression expected");

			expr = uexp.Operand;
		}

		if (expr.NodeType != ExpressionType.MemberAccess)
			return new List<string>(rec);

		if (expr is not MemberExpression mexp)
			throw new ArgumentException("Could not parse expression: MemberExpression expected");

		var res = UnwarpMemberAccessExpression(mexp.Expression, rec + 1);
		res.Add(mexp.Member.Name);
		return res;
	}
}