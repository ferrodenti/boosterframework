﻿namespace Booster.Ini;

public interface IIniSection
{
}


public interface IIniSection<in TIni> : IIniSection where TIni : IIni
{
	void Init(TIni ini);
}