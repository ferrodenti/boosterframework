﻿using System.Collections;
using System.Collections.Generic;

namespace Booster.Mvc.Editor.Interfaces;

public interface ICollectionEditProxy : IEditProxy, IEnumerable
{
	IEnumerable<IEditProxy> EnumarateChanges();

	bool IsNew(object item);

	void Add(object item);
	bool Contains(object item);
}

public interface ICollectionEditProxy<T> : ICollectionEditProxy, IList<T>
{
}