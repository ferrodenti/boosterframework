﻿using System.Collections.Generic;
using Booster.Evaluation;
using Booster.Interfaces;
using Booster.Mvc.Declarations;
using Newtonsoft.Json;

namespace Booster.Mvc.Editor.Interfaces;

public interface IEditorDataSchema
{
	/// <summary>
	/// Имя элемента html
	/// </summary>
	[JsonProperty("name")]
	string Name { get; set; }

	/// <summary>
	/// Имя для элемента label
	/// </summary>
	[JsonProperty("displayName")]
	string DisplayName { get; }

	/// <summary>
	/// Тип контрола
	/// </summary>
	[JsonProperty("type")]
	string Type { get; }

	/// <summary>
	/// Отображать символы "&lt;", "&gt;" как "\&lt;" "\&gt;"
	/// </summary>
	[JsonProperty("preserveHtml", DefaultValueHandling = DefaultValueHandling.Ignore)]
	bool PreserveHtml { get; set; }

	[JsonIgnore]
	DynamicSettings ControlSettings { get; }
	//Dictionary<string, IDynamicParameter> ControlSettings { get; }

	[JsonIgnore]
	bool ReplaceEmptyWithDefault { get; set; }

	[JsonIgnore]
	int Order { get; set; }
		
	[JsonIgnore]
	bool IsRowNumberColumn { get; set; }

	[JsonIgnore]
	IDataAccessor DataAccessor { get; set; }

	[JsonIgnore]
	object[] ReadPermissions { get; set; }

	[JsonIgnore]
	object[] WritePermissions { get; set; }

	[JsonIgnore]
	EditorType EditorType { get; set; }

	void AddSetting(string key, object value);

	object GetSetting(object model, string key);
	T GetSetting<T>(object model, string key, T @default = default);
		
	[JsonIgnore]
	IDictionary<string,string> SelectValues { get; }
}