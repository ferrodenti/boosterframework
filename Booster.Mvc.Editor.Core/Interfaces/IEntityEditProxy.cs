﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Booster.Interfaces;

namespace Booster.Mvc.Editor.Interfaces;

public interface IEntityEditProxy<T> : IEntityEditProxy
{
	new T Target { get; }

	TValue GetValue<TValue>(Expression<Func<T, TValue>> accessor);
	bool SetValue<TValue>(Expression<Func<T, TValue>> accessor, TValue value);

	ICollectionEditProxy<TValue> GetCollection<TValue>(Func<T, ICollection<TValue>> accessor);
	IEntityEditProxy<TValue> GetEntity<TValue>(Func<T, TValue> accessor) where TValue : class;

	bool IsDirty(Expression<Func<T, object>> accessor);
}

public interface IEntityEditProxy : IEditProxy
{
	object GetValue(string name);
	bool SetValue(string name, object value);

	void RegisterChildRelation(IEditProxy proxy);
	IEnumerable<IEditProxy> GetDirtyChildren();

	bool IsDirty(string name);

	IEnumerable<string> GetDirtyFields();

	IDataAccessor CreateChangesDataAccessor(string name);
}