using System.Collections.Generic;
using Newtonsoft.Json;

#nullable enable

namespace Booster.Mvc.Editor.Interfaces;

public interface IModelSchema
{
	[JsonProperty("caption", NullValueHandling = NullValueHandling.Ignore)]
	string? Caption { get; set; }

	[JsonProperty("data", NullValueHandling = NullValueHandling.Ignore)]
	ICollection<IEditorDataSchema>? Columns { get; set; }

	[JsonProperty("html", NullValueHandling = NullValueHandling.Ignore)]
	string? Html { get;  set; }

	[JsonProperty("hash")]
	string? Hash { get; set; }

	[JsonProperty("controlSettings", NullValueHandling = NullValueHandling.Ignore)]
	Dictionary<string, Dictionary<string, object>>? ControlSettings { get; }

	IEditorDataSchema GetColumn(string name, bool ignoreCase = true);

	void PickChanges();
}