﻿using System;
using System.Collections.Generic;

#nullable enable

namespace Booster.Mvc.Editor.Interfaces;

public interface IEditProxyRepository
{
	ICollectionEditProxy<T> GetCollection<T>(IEditProxy parent, ICollection<T> collection);
	IEntityEditProxy<T> GetEntity<T>(IEditProxy parent, T entity);
	IEditProxy Get(IEditProxy parent, object target);

	event EventHandler<EventArgs<string>> ProxyChanged;

	void UpdateDirty(IEditProxy proxy, bool dirty = false);
	bool IsDirty(IEditProxy proxy);
	void SetError(IEditProxy proxy, bool value);

	string[] GetDirtyIds(Func<IEditProxy, string?> idGetter);
	string[] GetErrorsIds(Func<IEditProxy, string?> idGetter);
}