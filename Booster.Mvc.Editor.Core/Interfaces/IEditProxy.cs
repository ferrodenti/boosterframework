﻿using System;

#nullable enable

namespace Booster.Mvc.Editor.Interfaces;

public interface IEditProxy
{
	event EventHandler Removed;

	IEditProxy? Parent { get; }
	object Target { get; }
	bool IsDirty();
	void ApplyChanges();
	void OnRemoved();
}