﻿using System.Collections.Generic;
using Newtonsoft.Json;


namespace Booster.Mvc.Editor.Interfaces;

public interface IEditorSchema
{
	[JsonIgnore]
	string CustomView { get; set; }

	[JsonIgnore]
	ICollection<IEditorDataSchema> Columns { get; }

	[JsonProperty("data")]
	ICollection<IEditorDataSchema> VisibleColumns { get; }

	[JsonProperty("hash")]
	string Hash { get; }

	void AddColumn(IEditorDataSchema column);

	IEditorDataSchema GetColumn(string name);

	IModelSchema ToModelSchema(IEntityEditProxy model);
}