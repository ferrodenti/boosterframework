using System;
using Booster.Interfaces;
using Booster.Reflection;

namespace Booster.Mvc.Editor;

public class LambdaDynamicParameter<T> : IDynamicParameter
{
	readonly Func<object,T> _evaluator;

	public LambdaDynamicParameter(Func<object, T> evaluator)
		=> _evaluator = evaluator;

	public bool IsConstant { get; set; }
	public bool IsStatic { get; set; }
	public TypeEx ValueType => typeof (T);

	public object Evaluate(object model/*, Func<object,string, object> dataAccessor = null*/)
		=> _evaluator(model);
}