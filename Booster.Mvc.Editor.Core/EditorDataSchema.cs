﻿using System.Collections.Generic;
using Booster.Evaluation;
using Booster.Interfaces;
using Booster.Mvc.Declarations;
using Booster.Mvc.Editor.Interfaces;
using Booster.Reflection;
using Newtonsoft.Json;

namespace Booster.Mvc.Editor;

public class EditorDataSchema : IEditorDataSchema
{
	public string Name { get; set; }
	public string DisplayName => ControlSettings.Get(null, "displayName", "");
	public object Required { get; set; }
	public bool RequiredNonDefault { get; set; }
	public bool ReplaceEmptyWithDefault { get; set; }
	public int Order { get; set; }
	public bool IsRowNumberColumn { get; set; }
	public bool PreserveHtml { get; set; }

	[JsonIgnore]
	public TypeEx ModelType { get; set; }

	DynamicSettings _controlSettings;
	public DynamicSettings ControlSettings => _controlSettings ??= new DynamicSettings(ModelType);

	public object[] WritePermissions { get; set; }
	public EditorType EditorType { get; set; }
	[JsonIgnore]
	public IDataAccessor DataAccessor { get; set; }
	public object[] ReadPermissions { get; set; }


	public void AddSetting(string key, object value)
		=> ControlSettings.Add(key, value);

	public object GetSetting(object model, string key)
		=> ControlSettings[model, key];

	public T GetSetting<T>(object model, string key, T @default = default)
		=> ControlSettings.Get(model, key, @default);

	Box<IDictionary<string, string>> _selectValues;
	public IDictionary<string, string> SelectValues
	{
		get
		{
			if (_selectValues == null)
			{
				var value = Try.OrDefault(() => GetSetting<IDictionary<string, string>>(null, "selectValues"));
				_selectValues = new Box<IDictionary<string, string>>(value);
			}
			return _selectValues.Value;
		}
	}


	public string Type => EditorType.ToString().ToLower();

	public override string ToString()
	{
		if (DisplayName.IsSome() && DisplayName != Name)
			return $"{DisplayName} ({Name}): {EditorType}";
			
		return $"{Name}: {EditorType}";
	}

	// ReSharper disable NonReadonlyMemberInGetHashCode
	public override int GetHashCode()
		=> new HashCode(Name, DisplayName, Required, Order, /*Values,*/ EditorType);
	// ReSharper restore NonReadonlyMemberInGetHashCode
}