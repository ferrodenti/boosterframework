using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Booster.Mvc.Editor.Interfaces;
using Booster.Mvc.Helpers;
using Booster.Reflection;

namespace Booster.Mvc.Editor;

public class ModelSchema : IModelSchema
{
	readonly object _editProxyModel;
	readonly object _model;

	public string Caption { get; set; }
	public ICollection<IEditorDataSchema> Columns { get; set; }
	public string Html { get; set; }

	public static readonly StringConverter StringConverter = new()
	{
		EnumFormatter = (_, o) => EnumHelper.GetValues(o).ToString(v => v.Name),
		ArrayFormatter = (_, o) =>
		{
			var escaper = new StringEscaper("\\", ";");
			return escaper.Join(((IEnumerable)o).Cast<object>().Select(v => StringConverter.ToString(v)), ";");
		}
	};
	/// <summary>
	///  Key/Field/Value
	/// </summary>
	public Dictionary<string, Dictionary<string, object>> ControlSettings { get; private set; }

	public IEditorDataSchema GetColumn(string name, bool ignoreCase = true) 
		=> Columns.FirstOrDefault(c => string.Equals(c.Name, name, ignoreCase ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal));

	public void PickChanges()
	{
		var controlSettings = new Dictionary<string, Dictionary<string, object>>();
		var oldValues = ControlSettings.With(c => c.SafeGet("values"));
		Dictionary<string, object> newValues = null;

		foreach (var col in Columns)
		{
			var valInParam = false;

			if( col.ControlSettings != null )
				foreach (var pair in col.ControlSettings) //TODO: use DynamicSettings.GetChangedValues()
				{
					if (pair.Key == "values")
						valInParam = true;

					var value = pair.Value.Evaluate(_model);

					if (ControlSettings == null ||
					    !ControlSettings.TryGetValue(pair.Key, out var oldDict) ||
					    !pair.Value.IsConstant &&
					    (!oldDict.TryGetValue(col.Name, out var oldVal) ||
					     !Equals(oldVal, value)))
						controlSettings.GetOrAdd(pair.Key, _ => new Dictionary<string, object>())[col.Name] = value;
				}

			if (col.DataAccessor.CanRead && !valInParam)
			{
				var format = (string)col.GetSetting(_model, "format");
				var value = StringConverter.ToString(col.DataAccessor.GetValue(_editProxyModel), format);

				if (oldValues == null || !oldValues.TryGetValue(col.Name, out var oldVal) || !Equals(oldVal, value))
				{
					if (newValues == null)
						controlSettings["values"] = newValues = new Dictionary<string, object>();

					newValues[col.Name] = value;
				}
			}
		}
		ControlSettings = controlSettings.Count > 0 ? controlSettings : null;
	}

	string _hash;
	public string Hash
	{
		get
		{
			if (_hash == null)
			{
				HashCode hash = 0;

				if (Columns != null)
					hash.AppendEnumerable(Columns);
					
				if (Html != null)
					hash.Append(Html);

				if (ControlSettings != null)
					foreach (var pair in ControlSettings)
					{
						hash.Append(pair.Key);
						hash.AppendEnumerable(pair.Value);
					}

				_hash = hash.ToString();
			}
			return _hash;
		}
		set => _hash = value;
	}

	public ModelSchema()
		=> Columns = new List<IEditorDataSchema>();

	protected ModelSchema(IEditorSchema editorSchema, object model, object editProxyModel)
	{
		_editProxyModel = editProxyModel;
		_model = model;

		Columns = new List<IEditorDataSchema>();
		foreach (var col in editorSchema.VisibleColumns)
		{
			if(Equals(col.GetSetting(_model, "display"), false))
				continue;

			Columns.Add(col);
		}

		if (editorSchema.CustomView != null)
			Html = MvcHelper.RenderViewToString("__none__", editorSchema.CustomView, editProxyModel);

		PickChanges();
	}

	public ModelSchema(IEditorSchema editorSchema, IEntityEditProxy proxy) : this(editorSchema, proxy.Target, proxy)
	{
	}
	public ModelSchema(IEditorSchema editorSchema, object model): this(editorSchema, model, model)
	{
	}
}