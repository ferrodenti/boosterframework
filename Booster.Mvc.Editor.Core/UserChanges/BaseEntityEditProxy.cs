using System;
using System.Linq;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Booster.Interfaces;
using Booster.Mvc.Declarations;
using Booster.Mvc.Editor.Interfaces;

namespace Booster.Mvc.Editor.UserChanges;

public abstract class BaseEntityEditProxy : BaseEditProxy, IEntityEditProxy
{
	protected readonly bool IgnoreCase;

	protected readonly HashSet<IEditProxy> ChildRelations = new();
	protected readonly ConcurrentDictionary<string, object> Values;
	protected readonly ConcurrentDictionary<string, object> OldValues;
	protected readonly ConcurrentDictionary<string, IDataAccessor> Accessors;

	protected BaseEntityEditProxy(IEditProxyRepository repository, IEditProxy parent, object target, bool ignoreCase = false) : base(repository, parent, target)
	{
		IgnoreCase = ignoreCase;

		Values = ignoreCase ? new ConcurrentDictionary<string, object>(StringComparer.OrdinalIgnoreCase) : new ConcurrentDictionary<string, object>();
		OldValues = ignoreCase ? new ConcurrentDictionary<string, object>(StringComparer.OrdinalIgnoreCase) : new ConcurrentDictionary<string, object>();
		Accessors = ignoreCase ? new ConcurrentDictionary<string, IDataAccessor>(StringComparer.OrdinalIgnoreCase) : new ConcurrentDictionary<string, IDataAccessor>();
	}

	public void RegisterChildRelation(IEditProxy proxy)
	{
		proxy.Removed += ChildProxyRemoved;
		ChildRelations.Add(proxy);
	}

	void ChildProxyRemoved(object sender, EventArgs e)
	{
		var proxy = (IEditProxy) sender;
		proxy.Removed -= ChildProxyRemoved;
		ChildRelations.Remove(proxy);
	}

	public override void OnRemoved()
	{
		foreach (var child in ChildRelations.ToArray())
			child.OnRemoved();

		base.OnRemoved();
	}

	public IEnumerable<IEditProxy> GetDirtyChildren()
		=> ChildRelations.Where(c => Repository.IsDirty(c));


	public override bool IsDirty()
		=> Values.Count != 0 || GetDirtyChildren().Any();

	public override void ApplyChanges()
	{
		foreach (var pair in Values)
		{
			var da = GetDataAccessor(pair.Key);
			da.SetValue(Target, pair.Value);
			OldValues[pair.Key] = pair.Value;
		}
		Values.Clear();
		OldValues.Clear();

		Repository.UpdateDirty(this);
	}


	public object GetValue(string name)
	{
		var da = GetDataAccessor(name);
		return GetValue(da);
	}

	public bool SetValue(string name, object value)
	{
		var da = GetDataAccessor(name);
		return SetValue(da, value);
	}

	public bool IsDirty(string name)
		=> Values.ContainsKey(name);

	public IEnumerable<string> GetDirtyFields()
		=> Values.Keys;

	public IDataAccessor CreateChangesDataAccessor(string name)
		=> new EntityProxyDataAccessor(GetDataAccessor(name));

	public IEditProxy Get(string name)
		=> GetValue(name).With(t => Repository.Get(this, t));

	protected object GetValue(IDataAccessor da)
	{
		if (!da.CanRead)
			throw new Exception($"{Target.GetType().Name}.{da.Name} is writeonly");

		if (Values.TryGetValue(da.Name, out var value))
			return value;

		return OldValues.GetOrAdd(da.Name, _ => da.GetValue(Target));
	}

	protected bool SetValue(IDataAccessor da, object value)
	{
		if (!da.CanWrite)
			throw new Exception($"{Target.GetType().Name}.{da.Name} is readonly");

		var prev = Values.TryGetValue(da.Name, out var prevValue);

		if (prev && Equals(value, prevValue))
			return false;

		object initValue = null;
		var init = false;

#pragma warning disable 618 // IChangeObserver obsolete
		if (da.CanRead)
		{
			initValue = OldValues.GetOrAdd(da.Name, _ => da.GetValue(Target));
			init = true;

			if (Equals(value, initValue))
			{
				if (prev)
				{
					Values.Remove(da.Name);

					(Target as IChangeObserver)?.Changed(da.Name, prevValue);

					return true;
				}
				return false;
			}
		}
			
		Values[da.Name] = value;

		(Target as IChangeObserver)?.Changed(da.Name, prev ? prevValue : (init ? initValue : null));
#pragma warning restore 618

		return true;
	}

	public IDataAccessor GetDataAccessor(string name)
		=> Accessors.GetOrAdd(name, _ => CreaterDataAccessor(name));

	protected abstract IDataAccessor CreaterDataAccessor(string name);
}