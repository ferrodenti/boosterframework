﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Booster.Mvc.Declarations;
using Booster.Mvc.Editor.Interfaces;
using Booster.Reflection;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Mvc.Editor.UserChanges;

[PublicAPI]
public class EditProxyRepository : IEditProxyRepository
{
	readonly ConcurrentDictionary<object, IEditProxy> _proxies = new();

	string[]? _dirtyNodesIds;
	string[]? _errorNodesIds;
	protected readonly HashSet<IEditProxy> DirtyNodes = new();
	protected readonly HashSet<IEditProxy> ErrorsNodes = new();

	public event EventHandler<EventArgs<string>>? ProxyChanged;

	public void NotifyProxyChanged(IEditProxy proxy, string field)
	{
		ProxyChanged?.Invoke(proxy, new EventArgs<string>(field));
		UpdateDirty(proxy);
	}

	public ICollectionEditProxy<T> GetCollection<T>(IEditProxy parent, ICollection<T> collection)
		=> (ICollectionEditProxy<T>) GetOrAddCreateProxy(collection, () =>
		{
			if (parent == null)
				throw new ArgumentNullException(nameof(parent));

			return CreateCollectionProxy(parent, collection);
		});

	public IEntityEditProxy<T> GetEntity<T>(IEditProxy parent, T entity)
	{
		if (entity == null) throw new ArgumentNullException(nameof(entity));
		return (IEntityEditProxy<T>) GetOrAddCreateProxy(entity, () =>
		{
			if (parent == null) throw new ArgumentNullException(nameof(parent));

			return CreateEntityProxy(parent, entity);
		});
	}

	public IEditProxy Get(IEditProxy parent, object target)
		=> GetOrAddCreateProxy(target, () =>
		{
			if (parent == null)
				throw new ArgumentNullException(nameof(parent));

			if (TypeEx.Of(target).HasGenericTypeDefinition(typeof(ICollection<>)))
				return CreateCollectionProxy(parent, target);

			return CreateEntityProxy(parent, target);
		});

	public IEditProxy Get<T>(T target, Func<T, T> parent)
	{
		if (target == null) throw new ArgumentNullException(nameof(target));

		var p = parent(target);
		if (p == null)
			return Get(target);


		if (_proxies.TryGetValue(p, out var pp))
			return Get(pp, target);

		return Get(Get(p, parent), target);
	}


	public IEditProxy Get(object target)
		=> GetOrAddCreateProxy(target, () =>
		{
			if (TypeEx.Of(target).HasGenericTypeDefinition(typeof(ICollection<>)))
				return CreateCollectionProxy(null, target);

			return CreateEntityProxy(null, target);
		});

	public ICollectionEditProxy<T> GetCollection<T>(ICollection<T> collection) 
		=> (ICollectionEditProxy<T>)GetOrAddCreateProxy(collection, () => CreateCollectionProxy(null, collection));

	public IEntityEditProxy<T> GetEntity<T>(T entity)
	{
		if (entity == null) 
			throw new ArgumentNullException(nameof(entity));

		return (IEntityEditProxy<T>) GetOrAddCreateProxy(entity, () => CreateEntityProxy(null, entity));
	}

	IEditProxy GetOrAddCreateProxy(object target, Func<IEditProxy> creator)
	{
		if (target == null) 
			throw new ArgumentNullException(nameof(target));

		return _proxies.GetOrAdd(target, _ =>
		{
			var proxy = creator();
			proxy.Removed += ProxyRemoved;
			return proxy;
		});
	}

	protected virtual IEntityEditProxy CreateEntityProxy(IEditProxy? parent, object entity)
	{
		var type = TypeEx.Of(entity);

		if (type.TryFindAttribute<ProxyTypeAttribute>(out var attr))
			type = attr.Type;


		var proxyType = TypeEx.MakeGenericType(typeof(EntityEditProxy<>), type);
		return proxyType.CreateAs<IEntityEditProxy>(this, parent, entity);
	}

	protected virtual ICollectionEditProxy CreateCollectionProxy(IEditProxy? parent, object collection)
	{
		foreach (var ifc in collection.GetType().GetInterfaces())
			if (ifc.IsGenericType && ifc.GetGenericTypeDefinition() == typeof(ICollection<>))
			{
				var proxyType = TypeEx.MakeGenericType(typeof(CollectionEditProxy<>), ifc.GetGenericArguments());
				return proxyType.CreateAs<ICollectionEditProxy>(this, parent, collection);
			}

		throw new ArgumentOutOfRangeException(nameof(collection), "Expected ICollection<> instance");
	}

	void ProxyRemoved(object? sender, EventArgs e)
	{
		var proxy = (IEditProxy)sender!;

		proxy.Removed -= ProxyRemoved;

		_proxies.Remove(proxy.Target);

		if (DirtyNodes.Remove(proxy))
			_dirtyNodesIds = null;

		if (ErrorsNodes.Remove(proxy))
			_errorNodesIds = null;

	}

	public void UpdateDirty(IEditProxy proxy, bool dirty = false)
	{
		var col = proxy.Parent as ICollectionEditProxy;

		var value = dirty || col?.IsNew(proxy.Target) == true || proxy.IsDirty();

		if (DirtyNodes.Contains(proxy) != value)
		{
			_dirtyNodesIds = null;

			if (value)
				DirtyNodes.Add(proxy);
			else
				DirtyNodes.Remove(proxy);

			if (proxy.Parent != null)
				UpdateDirty(proxy.Parent);
		}
	}

	public bool IsDirty(IEditProxy proxy) 
		=> DirtyNodes.Contains(proxy);

	public void SetError(IEditProxy proxy, bool value)
	{
		_errorNodesIds = null;

		if (value)
			ErrorsNodes.Add(proxy);
		else
			ErrorsNodes.Remove(proxy);
	}

	public string[] GetDirtyIds(Func<IEditProxy, string?> idGetter) 
		=> _dirtyNodesIds ??= DirtyNodes.Select(idGetter).NonDefault().ToArray();

	public string[] GetErrorsIds(Func<IEditProxy, string?> idGetter) 
		=> _errorNodesIds ??= ErrorsNodes.Select(idGetter).NonDefault().ToArray();

	protected void Reset()
	{
		_proxies.Clear();
		_dirtyNodesIds = null;
		_errorNodesIds = null;
		DirtyNodes.Clear();
		ErrorsNodes.Clear();
	}
}