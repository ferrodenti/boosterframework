using Booster.Interfaces;
using Booster.Mvc.Editor.Interfaces;
using Booster.Reflection;

namespace Booster.Mvc.Editor.UserChanges;

public class EntityProxyDataAccessor : IDataAccessor
{
	readonly IDataAccessor _innerAccessor;

	public EntityProxyDataAccessor(IDataAccessor innerAccessor)
        => _innerAccessor = innerAccessor;

    public string Name => _innerAccessor.Name;
	public TypeEx ValueType => _innerAccessor.ValueType;
	public bool CanRead => _innerAccessor.CanRead;
	public bool CanWrite => _innerAccessor.CanWrite;

	public object GetValue(object obj)
	{
		var changes = (IEntityEditProxy) obj;
		return changes.GetValue(Name);
	}

	public bool SetValue(object obj, object value)
	{
		var changes = (IEntityEditProxy) obj;
		return changes.SetValue(Name, value);
	}
}