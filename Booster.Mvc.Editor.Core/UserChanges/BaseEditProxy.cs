using System;
using Booster.Mvc.Editor.Interfaces;

namespace Booster.Mvc.Editor.UserChanges;

public abstract class BaseEditProxy : IEditProxy
{
	protected IEditProxyRepository Repository { get; }
	public IEditProxy Parent { get; }
	public object Target { get; }
	public event EventHandler Removed;

	protected BaseEditProxy(IEditProxyRepository repository, IEditProxy parent, object target)
	{
		Repository = repository;
		Target = target;
		Parent = parent;

		if (Parent is IEntityEditProxy parentEntity)
			parentEntity.RegisterChildRelation(this);
	}

	public virtual void OnRemoved()
        => Removed?.Invoke(this, EventArgs.Empty);

    public abstract bool IsDirty();
	public abstract void ApplyChanges();

}