using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using Booster.Interfaces;
using Booster.Mvc.Declarations;
using Booster.Mvc.Editor.Interfaces;
using Booster.Reflection;

namespace Booster.Mvc.Editor.UserChanges;

public class EntityEditProxy<T> : BaseEntityEditProxy, IEntityEditProxy<T>
{
	public new T Target => (T)base.Target;

	public EntityEditProxy(IEditProxyRepository repository, IEditProxy parent, T target)
		: base(repository, parent, target, false)
	{
	}

	public EntityEditProxy(IEditProxyRepository repository, IEditProxy parent, T target, bool ignoreCase)
		: base(repository, parent, target, ignoreCase)
	{
	}

	public TValue GetValue<TValue>(Expression<Func<T, TValue>> accessor)
	{
		var mi = accessor.ParseMemberRef();
		var da = GetDataAccessor(mi);

		return (TValue)GetValue(da);
	}

	public bool SetValue<TValue>(Expression<Func<T, TValue>> accessor, TValue value)
	{
		var mi = accessor.ParseMemberRef();
		var da = GetDataAccessor(mi);

		return SetValue(da, value);
	}

	public IEntityEditProxy<TValue> GetEntity<TValue>(Func<T, TValue> accessor) where TValue : class 
		=> accessor(Target).With(t => Repository.GetEntity(this, t));

	public ICollectionEditProxy<TValue> GetCollection<TValue>(Func<T,  ICollection< TValue>> accessor) 
		=> accessor(Target).With(t => Repository.GetCollection(this, t));

	public bool IsDirty(Expression<Func<T, object>> accessor)
	{
		var mi = accessor.ParseMemberRef();
		return IsDirty(mi.Name);
	}

	public IDataAccessor GetDataAccessor(BaseDataMember mi)
        => Accessors.GetOrAdd(mi.Name, _ =>
        {
            CheckMemeberInfo(mi);
            return mi;
        });

    protected object GetOldValue(MemberInfo mi) 
		=> OldValues.GetOrAdd(mi.Name, _ => GetDataAccessor(mi).GetValue(Target));

	protected override IDataAccessor CreaterDataAccessor(string name)
	{
		var mi = TypeEx.Get<T>().FindDataMember(new ReflectionFilter(name) {IgnoreCase = IgnoreCase});
		CheckMemeberInfo(mi);
		return mi;
	}

	protected void CheckMemeberInfo(BaseDataMember mi)
	{
		if (!mi.HasAttribute<IProxyAccess>())
			throw new Exception($"A {mi.DeclaringType?.Name}.{mi.Name} is not accessible for edit due to lack of BaseEditorAttribute");
	}

	public override string ToString() 
		=> $"Proxy{{{Target}}}";
}