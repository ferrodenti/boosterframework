using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Booster.Mvc.Editor.Interfaces;

namespace Booster.Mvc.Editor.UserChanges;

public class CollectionEditProxy<T> : BaseEditProxy, ICollectionEditProxy<T>
{
	readonly HashSet<T> _newItems = new();
	readonly HashSet<T> _removedItems = new();

	public new ICollection<T> Target => (ICollection<T>)base.Target;

	public int Count => Target.Count + _newItems.Count - _removedItems.Count;

	public bool IsReadOnly => false;

	public bool IsNew(object obj)
		=> _newItems.Contains((T) obj);

	public override bool IsDirty()
		=> _newItems.Count > 0 || _removedItems.Count > 0 || EnumarateChanges().Any(c => Repository.IsDirty(c));

	public override void ApplyChanges()
	{
		foreach (var item in _removedItems)
			Target.Remove(item);

		foreach (var item in _newItems)
			Target.Add(item);

		_removedItems.Clear();
		_newItems.Clear();

		Repository.UpdateDirty(this);
	}


	public override void OnRemoved()
	{
		foreach (var item in EnumarateChanges().ToArray())
			item.OnRemoved();

		base.OnRemoved();
	}

	public T this[int index]
	{
		get
		{
			var i = 0;
			foreach (var item in Target)
			{
				if (_removedItems.Contains(item))
					continue;

				if (i++ == index)
				{
					_removedItems.Add(item);
					return item;
				}
			}

			foreach (var item in _newItems)
				if (i++ == index)
				{
					_newItems.Remove(item);
					return item;
				}

			throw new ArgumentException();
		}
		set => throw new NotSupportedException();
	}

	public CollectionEditProxy(IEditProxyRepository repository, IEditProxy parent, ICollection<T> target) : base(repository, parent, target)
	{
	}

	public void Add(object obj)
		=> Add((T) obj);

	public void Add(T obj)
	{
		if (!_removedItems.Remove(obj))
			_newItems.Add(obj);
	}

	public bool Remove(T obj)
	{
		if (_newItems.Remove(obj))
			return true;

		if (Target.Contains(obj))
		{
			_removedItems.Add(obj);
			return true;
		}
		return false;
	}

	public IEnumerable<IEditProxy> EnumarateChanges()
		=> this.Select(item => Repository.Get(this, item));

	public IEnumerator<T> GetEnumerator()
	{
		foreach (var item in Target)
		{
			if (_removedItems.Contains(item))
				continue;

			yield return item;
		}

		foreach (var item in _newItems)
			yield return item;
	}

	IEnumerator IEnumerable.GetEnumerator()
		=> GetEnumerator();

	public void Clear()
	{
		_newItems.Clear();

		foreach (var item in Target)
			_removedItems.Add(item);
	}

	public void RemoveAt(int index)
	{
		var i = 0;
		foreach (var item in Target)
		{
			if (_removedItems.Contains(item))
				continue;

			if (i++ == index)
			{
				_removedItems.Add(item);
				return;
			}
		}

		foreach (var item in _newItems)
			if (i++ == index)
			{
				_newItems.Remove(item);
				return;
			}
	}

	bool ICollectionEditProxy.Contains(object item)
		=> Contains((T) item);

	public bool Contains(T item)
	{
		if (_removedItems.Contains(item))
			return false;

		return Target.Contains(item) || _newItems.Contains(item);
	}
	public int IndexOf(T item)
		=> this.ToList().IndexOf(item);

	public void CopyTo(T[] array, int arrayIndex)
		=> this.ToList().CopyTo(array, arrayIndex);

	public override string ToString()
		=> $"Proxy[{Target}]";

	#region Not supported

	public void Insert(int index, T item)
		=> throw new NotSupportedException();

	#endregion
}