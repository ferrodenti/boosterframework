﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Booster.Comparers;
using Booster.Mvc.Editor.Interfaces;
using Newtonsoft.Json;

namespace Booster.Mvc.Editor;

public class EditorSchema : IEditorSchema
{
	public string CustomView { get; set; }

	[JsonIgnore]
	public Func<IEditorDataSchema, Task<bool>> VisibilityCheck { get; set; }

	public ICollection<IEditorDataSchema> Columns { get; } = new List<IEditorDataSchema>();
		
	public ICollection<IEditorDataSchema> VisibleColumns =>
		VisibilityCheck.With(_ => Columns.Where(c => VisibilityCheck(c).NoCtxResult()), Columns)
			.OrderBy(c => c, new LambdaComparer<IEditorDataSchema>(c => c.Order)).ToArray();

	string _hash;
	public string Hash => _hash ??= new HashCode(Columns).ToString();

	public void AddColumn(IEditorDataSchema column)
		=> Columns.Add(column);

	public IEditorDataSchema GetColumn(string name)
		=> Columns.FirstOrDefault(c => string.Equals(c.Name, name, StringComparison.OrdinalIgnoreCase));

	public IModelSchema ToModelSchema(IEntityEditProxy model)
		=> new ModelSchema(this, model);
}