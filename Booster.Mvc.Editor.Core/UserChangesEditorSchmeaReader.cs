﻿using Booster.Mvc.Editor.Interfaces;
using Booster.Reflection;

namespace Booster.Mvc.Editor;

public class UserChangesEditorSchmeaReader : EditorSchemaReader
{
	readonly IEntityEditProxy _changes;

	public UserChangesEditorSchmeaReader(IEntityEditProxy changes)
		=> _changes = changes;

	protected override IEditorDataSchema CreateDataSchema(TypeEx entityType, BaseDataMember dataMemberMetadata, bool write)
		=> new EditorDataSchema
		{
			DataAccessor = _changes.CreateChangesDataAccessor(dataMemberMetadata.Name),
			ModelType = entityType
		};
}