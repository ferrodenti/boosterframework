﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Booster.Interfaces;
using Booster.Mvc.Declarations;
using Booster.Mvc.Editor.Interfaces;
using Booster.Reflection;
using EditorAttribute = Booster.Mvc.Declarations.EditorAttribute;

namespace Booster.Mvc.Editor;

public class EditorSchemaReader
{
	protected virtual List<TypeEx> AttributeTypes => new() { TypeEx.Get<BaseEditorAttribute>() };

	protected virtual IEditorSchema CreateSchema(TypeEx entityType) => new EditorSchema();

	protected virtual IEditorDataSchema CreateDataSchema(TypeEx entityType, BaseDataMember dataMemberMetadata, bool write)
		=> new EditorDataSchema
		{
			DataAccessor = dataMemberMetadata,
			ModelType = entityType
		};


	public IEditorSchema GetSchema(TypeEx entityType, bool write)
	{
		var editorSchema = CreateSchema(entityType);
		if (editorSchema == null)
			return null;

		if (!write && entityType.TryFindAttribute<GridRowNumberAttribute>(out var rowNumAttr))
		{
			var data = new EditorDataSchema
			{
				Order = rowNumAttr.Order,
				Name = rowNumAttr.DisplayName,
				ModelType = entityType,
				IsRowNumberColumn = true
			};
			data.AddSetting("displayName", rowNumAttr.DisplayName);
			editorSchema.AddColumn(data);
		}

		foreach (var attr in entityType.FindAttributes<EditorViewAttribute>())
			if (attr.View != null)
				editorSchema.CustomView = attr.View;

		foreach (var mem in entityType.FindDataMembers(
			         new ReflectionFilter
			         {
				         RequiredAttributes = AttributeTypes,
				         Order = ReflectionFilterOrder.DelaringAncestorsToDescendants
			         }))
		{
			var editorData = CreateDataSchema(entityType, mem, write);
			if (editorData == null)
				continue;


			foreach (var attr in mem.FindAttributes<ISchemaAttribute>())
				attr.ApplySettings(editorData.ControlSettings);

			var convIfc = editorData.DataAccessor.ValueType.FindInterface(typeof (IConvertiableFrom<>));
			if (convIfc != null && write)
				editorData.DataAccessor = new ConvertiableDataAccessor(editorData.DataAccessor, convIfc);

			if (write)
			{
				if (!editorData.DataAccessor.CanWrite)
					continue;
			}
			else if (!editorData.DataAccessor.CanRead)
				continue;

			foreach (var attr in mem.FindAttributes<DescriptionAttribute>())
				if (attr.Description.IsSome())
					editorData.AddSetting("description", attr.Description);

			foreach (var attr in mem.FindAttributes<BaseEditorAttribute>())
			{
				if (attr.DisplayName != null)
					editorData.AddSetting("displayName", attr.DisplayName);

				if (attr.Order != 0)
					editorData.Order = attr.Order;

				if (attr is EditorAttribute edAttr)
				{
					if (edAttr.ReplaceEmptyWithDefault)
						editorData.ReplaceEmptyWithDefault = true;

					if (edAttr.EditorType != EditorType.General)
						editorData.EditorType = edAttr.EditorType;
				}
			}

			var readPermissions = new HashSet<object>();
			var writePermissions = new HashSet<object>();
			foreach (var attr in mem.FindAttributes<GridPermissionsAttribute>())
			{
				if (attr.Permissions.Read != null)
				{
					if (attr.Permissions.Read is ICollection col)
						readPermissions.AddRange(col.Cast<object>());
					else
						readPermissions.Add(attr.Permissions.Read);
				}
				if (attr.Permissions.Write != null)
				{
					if (attr.Permissions.Write is ICollection col)
						writePermissions.AddRange(col.Cast<object>());
					else
						writePermissions.Add(attr.Permissions.Write);
				}
			}

			if (readPermissions.Count > 0) editorData.ReadPermissions = readPermissions.ToArray();
			if (writePermissions.Count > 0) editorData.WritePermissions = writePermissions.ToArray();

			editorData.Name = mem.Name;

			if (editorData.DisplayName == null)
				editorData.AddSetting("displayName", editorData.Name);

			if (editorData.EditorType == EditorType.General)
			{
				var tp = editorData.DataAccessor.ValueType;

				if (tp == typeof (IWebFile))
					editorData.EditorType = EditorType.File;
				else if (tp.IsEnum)
					editorData.EditorType = EditorType.Select;
				else
					switch (tp.TypeCode)
					{
					case TypeCode.Boolean:
						editorData.EditorType = EditorType.Checkbox;
						break;
					case TypeCode.DateTime:
						editorData.EditorType = EditorType.Date;
						break;
					case TypeCode.Int64:
					case TypeCode.UInt64:
					case TypeCode.Int32:
					case TypeCode.UInt32:
					case TypeCode.Int16:
					case TypeCode.UInt16:
						editorData.EditorType = EditorType.Int;
						break;
					}
			}

			if (editorData.EditorType == EditorType.Select)
			{
				var tp = editorData.DataAccessor.ValueType;
				if (tp.IsEnum && !editorData.ControlSettings.With(s => s.ContainsKey("selectValues")))
				{
					var values = new Dictionary<string, string>();

					foreach (var descr in EnumHelper.GetValues(tp))
						values[descr.Name] = descr.DisplayName;

					editorData.AddSetting("selectValues", values); //TODO: make dynamic
				}
			}

			editorSchema.AddColumn(editorData);
		}
		return editorSchema;
	}
}