using System;
using Booster.Cron.Kitchen;

namespace Booster.Cron;

public class SecondsExpression : BaseExpression
{
	public SecondsExpression(string expression) : base(expression)
	{
		ValidFrom = 0;
		ValidTo = 59;
	}

	protected override int GetPart(DateTime dt) => dt.Second;
	protected override DateTime SetPart(DateTime dt, int value) => new(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, value);
	public static implicit operator SecondsExpression(string expression) => new(expression);
}