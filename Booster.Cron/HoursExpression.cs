using System;
using Booster.Cron.Kitchen;

namespace Booster.Cron;

public class HoursExpression : BaseExpression
{
	public HoursExpression(string expression) : base(expression)
	{
		ValidFrom = 0;
		ValidTo = 23;
	}

	protected override int GetPart(DateTime dt) => dt.Hour;
	protected override DateTime SetPart(DateTime dt, int value) => new(dt.Year, dt.Month, dt.Day, value, 0, 0);
	public static implicit operator HoursExpression(string expression) => new(expression);
}