using System;
using Booster.Cron.Kitchen;

namespace Booster.Cron;

public class YearExpression : BaseExpression
{
	public YearExpression(string expression) : base(expression)
	{
	}

	protected override int GetPart(DateTime dt) => dt.Year;
	protected override DateTime SetPart(DateTime dt, int value) => new(value, 1, 1);
	public static implicit operator YearExpression(string expression) => new(expression);
}