using System;
using System.Collections.Generic;
using System.Linq;
using Booster.Cron.Kitchen;
using Booster.Cron.Kitchen.RangeParts;
using Booster.Parsing;

using Range = Booster.Cron.Kitchen.Range;

namespace Booster.Cron;

public class DayOfMonthExpression : BaseExpression
{
	public DayOfMonthExpression(string expression) : base(expression)
	{
		ValidFrom = 1;
		ValidTo = 31;
	}

	internal override IEnumerable<Range> Parse(string str) => str == "?" ? Enumerable.Empty<Range>() : base.Parse(str);

	internal override IRangePart ParseRangePart(string str)
	{
		if(str.Contains('L'))
			return new LastDayOfMonthRangePart(str);

		if(str.Contains('W'))
			return new WeekDayOfMonthRangePart(str);

		if (!ParseRangePartNumber(str, out var i))
			throw new ParsingException($"Expected integer value in range {ValidFrom}-{ValidTo}");

		return new DayOfMonthRangePart(i);

	}

	protected override int GetPart(DateTime dt) => dt.Day;
	protected override DateTime SetPart(DateTime dt, int value) => new(dt.Year, dt.Month, value);
	public static implicit operator DayOfMonthExpression(string expression) => new(expression);
}