using System;
using System.Collections.Generic;
using System.Linq;
using Booster.Cron.Kitchen;
using Booster.Cron.Kitchen.RangeParts;

using Range = Booster.Cron.Kitchen.Range;

namespace Booster.Cron;

public class DayOfWeekExpression : BaseExpression
{
	public DayOfWeekExpression(string expression) : base(expression)
	{
		ValidFrom = 1;
		ValidTo = 7;
	}

	internal void SetShouldBeSpecified()
	{
		IsValid = false;
		ParsingError = "Either day of month or day of week should be specified";
	}

	internal override IEnumerable<Range> Parse(string str) => str == "?" ? Enumerable.Empty<Range>() : base.Parse(str);

	internal override IRangePart ParseRangePart(string str)
	{
		if (str == "L")
			return new RangePart(7);

		if(str.Contains('L'))
			return new LastDayOfWeekRangePart(str);

		if(str.Contains('#'))
			return new NthDayOfWeekRangePart(str);

		return base.ParseRangePart(str);
	}

	protected override bool ParseRangePartNumber(string str, out int value)
	{
		switch (str)
		{
		case "SUN": value = 1; return true;
		case "MON": value = 2; return true;
		case "TUE": value = 3; return true;
		case "WED": value = 4; return true;
		case "THU": value = 5; return true;
		case "FRI": value = 6; return true;
		case "SAT": value = 7; return true;
		}
		return base.ParseRangePartNumber(str, out value);
	}

	internal override bool TryUpdateFromRange(ref DateTime current, DateTime previous, bool increment, Range range, int minPart)
	{
		if (!(range.From is RangePart) || !(range.To is RangePart))
			return base.TryUpdateFromRange(ref current, previous, increment, range, previous.Day);

		var to = range.To.GetValue(previous);
		var from = range.From.GetValue(previous);
		var days = new List<int>();
		for (var i = from; i <= to; i += range.Step)
			days.Add(i);

		var dt = new DateTime(previous.Year, previous.Month, previous.Day);
		if (increment)
			dt = dt.AddDays(1);

		while (dt.Month == previous.Month)
		{
			var day = GetPart(dt);
			if (days.Contains(day))
			{
				current = dt;
				return true;
			}

			dt = dt.AddDays(1);
		}

		return false;
	}

	protected override int GetPart(DateTime dt) => (int)dt.DayOfWeek + 1;
	protected override DateTime SetPart(DateTime dt, int value) => new(dt.Year, dt.Month, value);
	public static implicit operator DayOfWeekExpression(string expression) => new(expression);
}