using System;
using Booster.Cron.Kitchen;

namespace Booster.Cron;

public class MinutesExpression : BaseExpression
{
	public MinutesExpression(string expression) : base(expression)
	{
		ValidFrom = 0;
		ValidTo = 59;
	}

	protected override int GetPart(DateTime dt) => dt.Minute;
	protected override DateTime SetPart(DateTime dt, int value) => new(dt.Year, dt.Month, dt.Day, dt.Hour, value, 0);
	public static implicit operator MinutesExpression(string expression) => new(expression);
}