using System;
using System.Collections.Generic;
using System.Linq;
using Booster.Cron.Kitchen;
using Booster.Interfaces;
using JetBrains.Annotations;

namespace Booster.Cron;

public class CronExpression : IScheduleProvider
{
	public SecondsExpression Seconds
	{
		get
		{
			EnsureParsed();
			return _seconds;
		}
		set
		{
			_seconds = value;
			Changed();
		}
	}
	SecondsExpression _seconds;

	public MinutesExpression Minutes
	{
		get
		{
			EnsureParsed();
			return _minutes;
		}
		set
		{
			_minutes = value;
			Changed();
		}
	}
	MinutesExpression _minutes;

	public HoursExpression Hours
	{
		get
		{
			EnsureParsed();
			return _hours;
		}
		set
		{
			_hours = value;
			Changed();
		}
	}
	HoursExpression _hours;

	public DayOfMonthExpression DayOfMonth
	{
		get
		{
			EnsureParsed();
			return _dayOfMonth;
		}
		set
		{
			_dayOfMonth = value;
			Changed();
		}
	}
	DayOfMonthExpression _dayOfMonth;

	public MonthExpression Month
	{
		get
		{
			EnsureParsed();
			return _month;
		}
		set
		{
			_month = value;
			Changed();
		}
	}
	MonthExpression _month;

	public DayOfWeekExpression DayOfWeek
	{
		get
		{
			EnsureParsed();
			return _dayOfWeek;
		}
		set
		{
			_dayOfWeek = value;
			Changed();
		}
	}
	DayOfWeekExpression _dayOfWeek;

	public YearExpression Year
	{
		get
		{
			EnsureParsed();
			return _year;
		}
		set
		{
			_year = value;
			Changed();
		}
	}
	YearExpression _year;

	bool? _isValid;
	public bool IsValid => _isValid ??= Expressions.All(e => e.IsValid);

	string[] _errors;
	public string[] Errors
	{
		get
		{
			if (_errors == null)
			{
				var result = new List<string>();
				if (!Seconds.IsValid) result.Add($"Seconds: {Seconds.ParsingError}");
				if (!Minutes.IsValid) result.Add($"Minutes: {Minutes.ParsingError}");
				if (!Hours.IsValid) result.Add($"Hours: {Hours.ParsingError}");
				if (!DayOfMonth.IsValid) result.Add($"DayOfMonth: {DayOfMonth.ParsingError}");
				if (!Month.IsValid) result.Add($"Month: {Month.ParsingError}");
				if (!DayOfWeek.IsValid) result.Add($"DayOfWeek: {DayOfWeek.ParsingError}");
				if (!Year.IsValid) result.Add($"S: {Year.ParsingError}");
				_errors = result.ToArray();
			}
			return _errors;
		}
	}

	string _initialExpression;
		
	BaseExpression[] _expressions;
	BaseExpression[] Expressions => _expressions ??= new[] {Year, Month, DayOfMonth.IsSome ? (BaseExpression) DayOfMonth : DayOfWeek, Hours, Minutes, Seconds};

	public CronExpression(string expression = "*")
		=> _initialExpression = expression;

	protected void EnsureParsed()
	{
		if (_initialExpression != null)
			lock (this)
				if (_initialExpression != null)
				{
					if (_initialExpression.IsEmpty())
						_initialExpression = "0";

					var parts = _initialExpression.SplitNonEmpty(' ', '\t').AsList();

					while (parts.Count < 7)
						parts.Add(parts.Count == 5 && parts[3] != "?" ? "?": "*");

					Seconds = parts[0];
					Minutes = parts[1];
					Hours = parts[2];
					DayOfMonth = parts[3];
					Month = parts[4];
					DayOfWeek = parts[5];
					Year = parts[6];

					if (parts[3] == "?" && parts[5] == "?")
						DayOfWeek.SetShouldBeSpecified();
				
					_initialExpression = null;
						
					Changed();
				}
	}


	[PublicAPI] public CronExpression(string second, string minutes, string hours, string dayOfMonth, string month, string dayOfWeek, string year = "*")
	{
		Seconds = second;
		Minutes = minutes;
		Hours = hours;
		DayOfMonth = dayOfMonth;
		Month = month;
		DayOfWeek = dayOfWeek;
		Year = year;
	}

	[PublicAPI] public DateTime GetNext() => GetNext(DateTime.Now);

	public DateTime GetNext(DateTime previous)
	{
		if(!IsValid)
			return DateTime.MaxValue;
			
		var result = new DateTime();
		var inc = false;

		int i = 0, cnt = Expressions.Length;

		while(true)
		{
			for (; i < cnt; ++i)
			{
				if (!Expressions[i].Update(ref result, previous, inc))
				{
					i -= 2;
					if (i < -1)
						return DateTime.MaxValue;

					inc = true;
					continue;
				}

				if (result > previous)
				{
					i++;
					break;
				}

				inc = false;
			}

			if (result <= previous)
			{
				i--;
				inc = true;
			}
			else
				break;
		}

		inc = false;
		for (; i < cnt; ++i)
		{
			if (!Expressions[i].Update(ref result, result, inc))
			{
				i -= 2;
				if (i < -1)
					return DateTime.MaxValue;

				inc = true;
				continue;
			}

			inc = false;
		}

		return result;
	}

	[PublicAPI] public IEnumerable<DateTime> CreateSchedule(DateTime previous)
	{
		while (previous.IsSignificant())
		{
			previous = GetNext(previous);
			if (previous.IsSignificant())
				yield return previous;
		}
	}

	void Changed()
	{
		_toString = null;
		_hashCode = null;
		_expressions = null;
	}

	string _toString;
	public override string ToString() 
		=> _toString ??= $"{Seconds} {Minutes} {Hours} {DayOfMonth} {Month} {DayOfWeek} {Year}";

	// ReSharper disable NonReadonlyMemberInGetHashCode
	int? _hashCode;
	public override int GetHashCode() 
		=> _hashCode ??= new HashCode(Seconds, Minutes, Hours, DayOfMonth, Month, DayOfWeek, Year);
	// ReSharper restore NonReadonlyMemberInGetHashCode

	public override bool Equals(object obj)
	{
		EnsureParsed();
		return obj is CronExpression other &&
			   other.Seconds.Equals(Seconds) &&
			   other.Minutes.Equals(Minutes) &&
			   other.Hours.Equals(Hours) &&
			   other.DayOfMonth.Equals(DayOfMonth) &&
			   other.Month.Equals(Month) &&
			   other.DayOfWeek.Equals(DayOfWeek) &&
			   other.Year.Equals(Year);
	}

	public static bool operator ==(CronExpression a, CronExpression b) => a?.Equals(b) == true;
	public static bool operator !=(CronExpression a, CronExpression b) => a?.Equals(b) != true;

	public static implicit operator CronExpression(string expression) => expression.With(e => new CronExpression(e));
	public static implicit operator string(CronExpression expression) => expression?.ToString();
}