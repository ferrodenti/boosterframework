using System;

namespace Booster.Cron.Kitchen.RangeParts;

interface IRangePart
{
	int GetValue(DateTime dt);
}