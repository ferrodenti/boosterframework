using System;

namespace Booster.Cron.Kitchen.RangeParts;

class DayOfMonthRangePart : RangePart
{
	public DayOfMonthRangePart(int value) : base(value)
	{
	}

	public override int GetValue(DateTime dt) => Math.Min(base.GetValue(dt), DateTime.DaysInMonth(dt.Year, dt.Month));
}