using System;
using Booster.Parsing;

namespace Booster.Cron.Kitchen.RangeParts;

class LastDayOfMonthRangePart : IRangePart
{
	readonly int _dayOfWeek;
	readonly bool _weekDay;
	readonly int _dec;

	public LastDayOfMonthRangePart(string str)
	{
		var l = str.IndexOf('L');
		if (l == 0)
		{
			if(str.Length == 1) // L = last
				return;
				
			switch (str[1])
			{
			case '-': // L-3 = last - 3
				if (!int.TryParse(str.Substring(2), out _dec) || _dec < 1 || _dec > 31)
					throw new ParsingException("Expected offset from last integer value (1-31)");
				return;
			case 'W': // LW = last weekday
				_weekDay = true;
				if (str.Length == 2)
					return;
				break;
			}
		}
		else if (l > 0) //4L = last wed
		{
			if (!int.TryParse(str.Substring(0, l), out _dayOfWeek) || _dayOfWeek < 1 || _dayOfWeek > 7)
				throw new ParsingException("Expected day of week number (1-7)");

			return;
		}

		throw new ParsingException($"Unexpected value: ${str}");
	}

	public int GetValue(DateTime dt)
	{
		var lastDay = DateTime.DaysInMonth(dt.Year, dt.Month);
		if (_dayOfWeek > 0) //4L = last wed
		{
			var d = new DateTime(dt.Year, dt.Month, lastDay);
			while ((int) d.DayOfWeek + 1 != _dayOfWeek)
				d = d.AddDays(-1);

			return d.Day;
		}

		if (_dec > 0)
			return lastDay - _dec; // L-3 = last - 3

		if (_weekDay) // LW = last weekday
		{
			var d = new DateTime(dt.Year, dt.Month, lastDay);
			while (d.DayOfWeek == DayOfWeek.Saturday || d.DayOfWeek == DayOfWeek.Sunday)
				d = d.AddDays(-1);

			return d.Day;
		}

		return lastDay;
	}

	public override string ToString()
	{
		if (_dayOfWeek > 0) return $"{_dayOfWeek}L";
		if (_dec > 0) return $"L-{_dec}";
		if (_weekDay) return "LW";
		return "L";
	}

	public override bool Equals(object obj) => obj is LastDayOfMonthRangePart other &&
	                                           _dayOfWeek == other._dayOfWeek &&
	                                           _weekDay == other._weekDay &&
	                                           _dec == other._dec;

	public override int GetHashCode() => new HashCode(_dayOfWeek, _weekDay, _dec, GetType());
}