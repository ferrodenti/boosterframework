using System;

namespace Booster.Cron.Kitchen.RangeParts;

class RangePart : IRangePart
{
	public readonly int Value;

	public RangePart(int value) => Value = value;

	public virtual int GetValue(DateTime dt) => Value;

	public override string ToString() => Value.ToString();
	public override bool Equals(object obj) => obj is RangePart other && other.Value == Value;
	public override int GetHashCode() => Value;
}