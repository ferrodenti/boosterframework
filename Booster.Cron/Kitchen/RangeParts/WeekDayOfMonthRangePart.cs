using System;
using Booster.Parsing;

namespace Booster.Cron.Kitchen.RangeParts;

class WeekDayOfMonthRangePart : IRangePart
{
	readonly int _close = 1;

	public WeekDayOfMonthRangePart(string str)
	{
		var w = str.IndexOf('W');// 15W = closest weekday to 15
		if (w > 0)
		{
			if (!int.TryParse(str.Substring(0, w), out _close) || _close < 1 || _close > 31)
				throw new ParsingException("Expected day of month number (1-31)");
				
			return;
		}

		if(str.Length == 1) //W = first weekday 
			return;

		throw new ParsingException($"Unexpected value: ${str}");
	}


	public int GetValue(DateTime dt)
	{
		var close = new DateTime(dt.Year, dt.Month, Math.Min(_close, DateTime.DaysInMonth(dt.Year, dt.Month)));
			
		for (var i = 0; i < 31; ++i) // 15W closest weekday to 15
		{
			var minus = close.AddDays(-i);
			if (minus.Month == dt.Month && minus.DayOfWeek != DayOfWeek.Saturday && minus.DayOfWeek != DayOfWeek.Sunday)
				return minus.Day;
				
			var plus = close.AddDays(i);
			if (plus.Month == dt.Month && plus.DayOfWeek != DayOfWeek.Saturday && plus.DayOfWeek != DayOfWeek.Sunday)
				return plus.Day;
		}

		return -1;
	}

	public override string ToString() => _close > 0 ? $"{_close}W" : "W";
	public override bool Equals(object obj) => obj is WeekDayOfMonthRangePart other && _close == other._close;
	public override int GetHashCode() => _close + GetType().GetHashCode();
}