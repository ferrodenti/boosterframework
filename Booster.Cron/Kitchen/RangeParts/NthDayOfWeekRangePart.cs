using System;
using Booster.Parsing;

namespace Booster.Cron.Kitchen.RangeParts;

class NthDayOfWeekRangePart : IRangePart
{
	readonly int _dayOfWeek;
	readonly int _n;

	public NthDayOfWeekRangePart(string str)
	{
		var l = str.IndexOf('#');
		if (!int.TryParse(str.Substring(0, l), out _dayOfWeek) || _dayOfWeek < 1 || _dayOfWeek > 7) //TODO: parse strings
			throw new ParsingException("Expected day of week (1-7)");

		if (!int.TryParse(str.Substring(l+1), out _n) || _n < 1 || _n > 5)
			throw new ParsingException("Expected integer (1-5)");
	}
		
	public int GetValue(DateTime dt)
	{
		var d = new DateTime(dt.Year, dt.Month, 1);
		while ((int) d.DayOfWeek + 1 != _dayOfWeek)
			d = d.AddDays(1);

		d = d.AddDays((_n-1) * 7);
		if (d.Month == dt.Month)
			return d.Day;

		return -1;
	}

	public override string ToString() => $"{_dayOfWeek}#{_n}";
	public override bool Equals(object obj) => obj is NthDayOfWeekRangePart other && _dayOfWeek == other._dayOfWeek && _n == other._n;
	public override int GetHashCode() => new HashCode(_dayOfWeek, _n, GetType());
}