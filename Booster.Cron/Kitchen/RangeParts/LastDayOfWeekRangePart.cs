using System;
using Booster.Parsing;

namespace Booster.Cron.Kitchen.RangeParts;

class LastDayOfWeekRangePart : IRangePart
{
	readonly int _dayOfWeek;

	public LastDayOfWeekRangePart(string str)
	{
		var l = str.IndexOf('L');
		if (!int.TryParse(str.Substring(0, l), out _dayOfWeek) || _dayOfWeek < 1 || _dayOfWeek > 7)
			throw new ParsingException("Expected day of week (1-7)");
	}

	public int GetValue(DateTime dt)
	{
		var d = new DateTime(dt.Year, dt.Month, DateTime.DaysInMonth(dt.Year, dt.Month));
		while ((int) d.DayOfWeek + 1 != _dayOfWeek)
			d = d.AddDays(-1);

		return d.Day;
	}

	public override string ToString() => $"{_dayOfWeek}L";
	public override bool Equals(object obj) => obj is LastDayOfWeekRangePart other && _dayOfWeek == other._dayOfWeek;
	public override int GetHashCode() => new HashCode(_dayOfWeek, GetType());
}