using Booster.Cron.Kitchen.RangeParts;

namespace Booster.Cron.Kitchen;

class Range
{
	public readonly IRangePart From;
	public readonly IRangePart To;
	public readonly int Step;
	readonly string _initial;

	public Range(IRangePart from, IRangePart to, int step, string initial)
	{
		Step = step;
		_initial = initial;
		if (from is RangePart sFrom && to is RangePart sTo && sFrom.Value > sTo.Value)
		{
			From = to;
			To = from;
		}
		else
		{
			From = from;
			To = to;
		}
	}

	public override string ToString()
		=> (_initial ?? (From.Equals(To) ? From.ToString() : $"{From}-{To}")) + (Step > 1 ? $"/{Step}" : "");

	public override bool Equals(object obj) 
		=> obj is Range other && other.From.Equals(From) && other.To.Equals(To) && other.Step == Step;

	public override int GetHashCode() 
		=> new HashCode(From, To, Step, GetType());
}