using System;
using System.Collections.Generic;
using System.Linq;
using Booster.Cron.Kitchen.RangeParts;
using Booster.Parsing;
using JetBrains.Annotations;

namespace Booster.Cron.Kitchen;

public abstract class BaseExpression
{
	protected int ValidFrom = 0;
	protected int ValidTo = int.MaxValue;

	readonly string _expression;

	Range[] _ranges;
	internal Range[] Ranges
	{
		get
		{
			EnsureParsed();
			return _ranges;
		}
	}

	string _parsingError;
	[PublicAPI]public string ParsingError
	{
		get
		{
			EnsureParsed();
			return _parsingError;
		}
		protected set => _parsingError = value;
	}

	bool _isValid;
	public bool IsValid
	{
		get
		{
			EnsureParsed();
			return _isValid;
		}
		protected set => _isValid = value;
	}

	public bool IsSome => !IsValid || Ranges?.Length > 0;

	protected BaseExpression(string expression) => _expression = expression ?? "";

	bool _parsed;
	protected void EnsureParsed()
	{
		if(!_parsed)
			lock(this)
				if (!_parsed)
				{
					_isValid = false;
					_parsingError = null;
					try
					{
						_ranges = Parse(_expression.ToUpper()).ToArray();
						_isValid = true;
					}
					catch (ParsingException e)
					{
						_parsingError = e.Message;
					}
					_parsed = true;
				}
	}

	internal virtual IEnumerable<Range> Parse(string str) 
		=> str.SplitNonEmpty(',').Select(ParseRange);

	static readonly StringEscaper _splitter = new("L", "-") {IgnoreCase = false}; //TODO: StringEscaper неправильно ищет буквы с игнором регистра

	internal Range ParseRange(string interval)
	{
		var step = 1;
		var i = interval.IndexOf('/');
		if (i > 0)
		{
			if (!int.TryParse(interval.Substring(i + 1), out step))
				throw new ParsingException("Expected integer value");

			interval = interval.Substring(0, i);
		}
				
		if (interval == "*" || interval.IsEmpty())
			return new Range(ParseRangePart(ValidFrom.ToString()), ParseRangePart(ValidTo.ToString()), step, interval);

		var rng = _splitter.Split(interval, "-").ToArray();

		switch (rng.Length)
		{
		case 1:
			var part1 = ParseRangePart(rng[0]);
			return new Range(part1, step > 1 ? ParseRangePart(ValidTo.ToString()) : part1, step, interval);
		case 2:
			return new Range(ParseRangePart(rng[0]), ParseRangePart(rng[1]), step, interval);
		default:
			throw new ParsingException("Expected range or a single value");
		}
	}

	internal virtual IRangePart ParseRangePart(string str)
	{
		if (!ParseRangePartNumber(str, out var i))
			throw new ParsingException($"Expected integer value in range {ValidFrom}-{ValidTo}");

		return new RangePart(i);
	}

	protected virtual bool ParseRangePartNumber(string str, out int value)
		=> int.TryParse(str, out value) && value >= ValidFrom && value <= ValidTo;

	protected abstract int GetPart(DateTime dt);
	protected abstract DateTime SetPart(DateTime dt, int number);

	public bool Update(ref DateTime current, DateTime previous, bool increment)
	{
		var minPart = GetPart(previous);
		foreach (var range in Ranges)
			if (TryUpdateFromRange(ref current, previous, increment, range, minPart))
				return true;

		return false;
	}

	internal virtual bool TryUpdateFromRange(ref DateTime current, DateTime previous, bool increment, Range range, int minPart)
	{
		var to = range.To.GetValue(previous);
		var from = range.From.GetValue(previous);
		if (from < 0)
			return false;

		var v = minPart <= from ? from : from + (minPart - from - 1) / range.Step * range.Step + range.Step;
		if (increment && v == minPart)
			v += range.Step;

		if (v > to)
			return false;

		if (from <= v)
		{
			current = SetPart(current, v);
			return true;
		}

		current = SetPart(current, from);
		return true;
	}

	protected virtual string Stringify() 
		=> !IsValid ? _expression : Ranges.ToString(new EnumBuilder(",", empty:"?"));

	string _toString;
	public override string ToString() 
		=> _toString ??= Stringify();

	// ReSharper disable NonReadonlyMemberInGetHashCode
	public override int GetHashCode() 
		=> ToString().GetHashCode() + GetType().GetHashCode();
	// ReSharper restore NonReadonlyMemberInGetHashCode

	public override bool Equals(object obj) 
		=> obj is BaseExpression other && other.GetType() == GetType() && other.ToString() == ToString();

	public static implicit operator string(BaseExpression expression) 
		=> expression?.ToString();
}