using System;
using Booster.Cron.Kitchen;

namespace Booster.Cron;

public class MonthExpression : BaseExpression
{
	public MonthExpression(string expression) : base(expression)
	{
		ValidFrom = 1;
		ValidTo = 12;
	}

	protected override bool ParseRangePartNumber(string str, out int value)
	{
		switch (str)
		{
		case "JAN": value = 1; return true;
		case "FEB": value = 2; return true;
		case "MAR": value = 3; return true;
		case "APR": value = 4; return true;
		case "MAY": value = 5; return true;
		case "JUN": value = 6; return true;
		case "JUL": value = 7; return true;
		case "AUG": value = 8; return true;
		case "SEP": value = 9; return true;
		case "OCT": value = 10; return true;
		case "NOV": value = 11; return true;
		case "DEC": value = 12; return true;
		}
		return base.ParseRangePartNumber(str, out value);
	}
	protected override int GetPart(DateTime dt) => dt.Month;
	protected override DateTime SetPart(DateTime dt, int value) => new(dt.Year, value, 1);
	public static implicit operator MonthExpression(string expression) => new(expression);
}