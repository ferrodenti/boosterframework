using System;
using Booster.Log;
using JetBrains.Annotations;

#if TEST_PROGRESS_BARS
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
#endif

namespace Booster.Console;

[PublicAPI]
public class ProgressBar
{
	readonly ILog? _log;
	bool _pendingChanges;
	bool _enableUpdate = true;
	public bool EnableUpdate
	{
		get => _enableUpdate;
		set
		{
			if (_enableUpdate != value)
			{
				_enableUpdate = value;
				if (_enableUpdate && _pendingChanges) 
					Update();
			}
		}
	}
	string? _text;
	public string? Text
	{
		get => _text;
		set
		{
			if (_text != value)
			{
				_text = value;
				Update();
			}
		}
	}

	int _percent;
	public int Percent
	{
		get => _percent;
		set
		{
			if (_percent != value)
			{
				_percent = value;
				Update();
			}
		}
	}

	int _size = 40;
	public int Size
	{
		get => _size;
		set
		{
			if (_size != value)
			{
				_size = value;
				Update();
			}
		}
	}
	long _total;
	public long Total
	{
		get => _total;
		set
		{
			if (_total != value)
			{
				_total = value;
				Percent = _total > 0 ? (int) (Current * 100 / _total) : 0;
			}
		}
	}

	long _current;
	public long Current
	{
		get => _current;
		set
		{
			if (_current != value)
			{
				_current = value;
				Percent =  (int)(Current * 100 / Total);
			}
		}
	}

	// ReSharper disable once AutoPropertyCanBeMadeGetOnly.Global
	/// <summary>
	/// Если элемент скрылся с экрана, то при обновлении статуса он опять появится в самом низу
	/// </summary>
	public bool EmergeWhenOffScreen { get; set; } = true;

	public int Top { get; private set; } = int.MinValue;

	public ProgressBar(int total, ILog? log)
	{
		_log = log.Create(this);
		_total = total;
		ConsoleHelper.EnableScrollTracking = true;
	}

	protected void Update()
	{
		if (EnableUpdate)
		{
			Draw();
			_pendingChanges = false;
		}
		else
			_pendingChanges = true;
	}

	public void Set(int current, string? text)
	{
		try
		{
			EnableUpdate = false;
			Current = current;
			Text = text;
		}
		finally
		{
			EnableUpdate = true;
		}
	}
	public IDisposable FreezeUpdates()
	{
		EnableUpdate = false;
		return new ActionDisposable(() => EnableUpdate = true);
	}
	
	public void Draw()
	{
		if (Top == int.MinValue && Text.IsEmpty() && Percent == 0)
			return;

		void DrawImpl(int scroll)
		{
			var top = Top - scroll;
			if (top < -1)
				return;

			if (top >= System.Console.BufferHeight)
			{
				top = System.Console.BufferHeight - 1;
				Top = top + scroll;
			}

			var top1 = Math.Max(0, top);
			try
			{
				System.Console.SetCursorPosition(0, top1);
			}
			catch (Exception e)
			{
				_log.Error(e, $"ProgressBar.DrawImpl SetCursorPosition (_top={Top}, scroll={scroll}, top1={top1})");
			}
			
			using (ConsoleHelper.PushBackgroundColor(ConsoleHelper.DefaultColor))
			using (ConsoleHelper.PushForegroundColor(ConsoleColor.Green))
			{
				if (top >= 0)
				{
					var width = System.Console.WindowWidth;
					var text = "";
					if (Text.IsSome())
						text = Text!.Length > width ? $"{Text.Substring(0, width - 3)}..." : Text;

					System.Console.Write(text);
					var sz = width - text.Length - 1;
					System.Console.WriteLine(sz > 0 ? new string(' ', sz) : "");
				}

				System.Console.ForegroundColor = ConsoleColor.Gray;
				
				var fill = Size * Percent / 100;
				System.Console.Write('[');
				
				if (fill > 0)
					System.Console.Write(new string('#', fill));
				
				fill = Size - fill;
				if (fill > 0)
					System.Console.Write(new string(' ', fill));
				
				//Console.WriteLine($"] {Percent}% top={Console.CursorTop}, scroll={ConsoleHelper.Scroll}, _top={_top}  ");
				System.Console.WriteLine($"] {Percent}%");
			}
		}

		using (ConsoleHelper.Lock())
		{
			var scroll = ConsoleHelper.Scroll;
			var cursorTop = System.Console.CursorTop;

			var reset = EmergeWhenOffScreen && Top - scroll + System.Console.WindowHeight <= cursorTop;
			if (reset)
			{
				Top = cursorTop + scroll;
				DrawImpl(scroll);
			}
			else
			{
				var left = System.Console.CursorLeft;
				DrawImpl(scroll);

				Try.IgnoreErrors(() => System.Console.SetCursorPosition(left, cursorTop));
			}
		}
	}
}
