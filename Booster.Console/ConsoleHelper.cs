﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Booster.Helpers;
using Booster.Kitchen;
using Booster.Synchronization;
using JetBrains.Annotations;

using CONSOLE = System.Console;

namespace Booster.Console;

[PublicAPI]
public static class ConsoleHelper
{
	static readonly AsyncLock _asyncLock = new();
		
	[Obsolete("Используй Lock() или AsyncLock()")]
	public static readonly object ConsoleLock = new();

	const int _stdInputHandle = -10;
	const int _quickEditMode = 64;
	const int _extendedFlags = 128;

	public static IDisposable Lock()
		=> _asyncLock.Lock();
		
	public static Task<IDisposable> LockAsync()
		=> _asyncLock.LockAsync();
		
	public static void DisableQuickEdit()
	{
		if (!OSType.CurrentOS.IsWindows)
			return;

		using (_asyncLock.Lock())
		{
			var conHandle = WinApi.GetStdHandle(_stdInputHandle);

			if (!WinApi.GetConsoleMode(conHandle, out var mode))
			{
				CONSOLE.WriteLine("GetConsoleMode fail");
				return;
			}

			mode &= ~(_quickEditMode | _extendedFlags);

			if (!WinApi.SetConsoleMode(conHandle, mode))
				CONSOLE.WriteLine("SetConsoleMode fail");
		}
	}

	static ConsoleColor _defaultForeground = ConsoleColor.Gray;
	static ConsoleColor _defaultBackground = ConsoleColor.Black;

	public const ConsoleColor DefaultColor = (ConsoleColor) (-1);
		
	public static void StoreDefaultColors()
	{
		_defaultForeground = CONSOLE.ForegroundColor;
		_defaultBackground = CONSOLE.BackgroundColor;
	}
		
	public static ConsoleColor FixColor(ConsoleColor color, ConsoleColor defaultColor)
	{
		if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
			switch (color)
			{
			case ConsoleColor.Black:
				return ConsoleColor.White;
			case ConsoleColor.White:
				return ConsoleColor.Black;
			}
			
		return color == DefaultColor ? defaultColor : color;
	}

	public static IDisposable? PushForegroundColor(ConsoleColor color)
	{
		color = FixColor(color, _defaultForeground);

		var old = FixColor(CONSOLE.ForegroundColor, _defaultForeground);
		if (color == old)
			return null;

		CONSOLE.ForegroundColor = color;

		return new ActionDisposable(() => CONSOLE.ForegroundColor = old);
	}


	public static IDisposable? PushBackgroundColor(ConsoleColor color)
	{
		color = FixColor(color, _defaultBackground);

		var old = FixColor(CONSOLE.BackgroundColor, _defaultBackground);
		if (color == old)
			return null;

		CONSOLE.BackgroundColor = color;

		return new ActionDisposable(() => CONSOLE.BackgroundColor = old);
	}

	static readonly SyncLazy<ConsoleScrollWatcher> _consoleScrollWatcher = new(() => new ConsoleScrollWatcher());
		
	public static bool EnableScrollTracking
	{
		get => _consoleScrollWatcher.IsValueCreated;
		set
		{
			var prev = _consoleScrollWatcher.IsValueCreated;
			if (prev != value)
				if (prev)
					_consoleScrollWatcher.DisposeReset();
				else
					_consoleScrollWatcher.EnsureCreated();
		}
	}

	public static int Scroll => _consoleScrollWatcher.ValueIfCreated?.Scroll ?? 0;

	public static async Task<string?> ReadLineAsync(CancellationToken token = default, int interval = 100)
	{
		var result = new StringBuilder();
		while (!token.IsCancellationRequested)
		{
			var key = await ReadKeyAsync(token, interval).NoCtx();
			if (key.HasValue)
				switch (key.Value.KeyChar)
				{
				case '\0':
					continue;
				case '\n':
				case '\r':
					return result.ToString();
				default:
					result.Append(key.Value.KeyChar);
					break;
				}
		}

		return null;
	}

	public static async Task<ConsoleKeyInfo?> ReadKeyAsync(CancellationToken token = default, int interval = 100)
	{
		while (!token.IsCancellationRequested)
		{ 
			if (CONSOLE.KeyAvailable)
				return CONSOLE.ReadKey();
				
			await Task.Delay(interval, token).NoCtx();

		}
		return null;
	}

	public static async Task WriteLineAsync(string value)
	{
		using var _ = await LockAsync().NoCtx();
		CONSOLE.WriteLine(value);
	}
	
	public static void WriteLine(string value)
	{
		using var _ = Lock();
		CONSOLE.WriteLine(value);
	}
	
	public static async Task WriteAsync(string value)
	{
		using var _ = await LockAsync().NoCtx();
		CONSOLE.Write(value);
	}
	
	public static void Write(string value)
	{
		using var _ = Lock();
		CONSOLE.Write(value);
	}
}