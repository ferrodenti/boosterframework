using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CONSOLE = System.Console;

namespace Booster.Console;

class ConsoleScrollWatcher : IDisposable
{
	class IntermediateStream  : Stream
	{
		readonly ConsoleScrollWatcher _watcher;
		readonly Stream _inner;

		public IntermediateStream(ConsoleScrollWatcher watcher)
		{
			_watcher = watcher;
			_inner = CONSOLE.OpenStandardOutput();
		}
			
		public override bool CanRead => _inner.CanRead;
		public override bool CanSeek => _inner.CanSeek;
		public override bool CanWrite => _inner.CanWrite;
		public override long Length => _inner.Length;
		
		public override long Position
		{
			get => _inner.Position;
			set => _inner.Position = value;
		}
		
		public override void Flush()
			=> _inner.Flush();

		public override int Read(byte[] buffer, int offset, int count)
			=> _inner.Read(buffer, offset, count);

		public override long Seek(long offset, SeekOrigin origin)
			=> _inner.Seek(offset, origin);

		public override void SetLength(long value)
			=>  _inner.SetLength(value);

		public override void Write(byte[] buffer, int offset, int count)
		{
			_watcher.OnWrite(buffer, offset, count);
			_inner.Write(buffer, offset, count);
		}

		public override Task WriteAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			_watcher.OnWrite(buffer, offset, count);
			return _inner.WriteAsync(buffer, offset, count, cancellationToken);
		}

		public override void WriteByte(byte value)
		{
			_watcher.OnWrite(new []{value}, 0, 1);
			base.WriteByte(value);
		}
	}

	readonly TextWriter _prevWriter;
	readonly StreamWriter _streamWriter;
		
	readonly Encoding _encoding;

	public int Scroll { get; private set; }

	internal ConsoleScrollWatcher()
	{
		_prevWriter = CONSOLE.Out;
		_encoding = CONSOLE.OutputEncoding;
		_streamWriter = new StreamWriter(new IntermediateStream(this), _encoding) {AutoFlush = true};
		CONSOLE.SetOut(_streamWriter);
	}

	protected void OnWrite(byte[] buffer, int offset, int count)
		=> Try.IgnoreErrors(() =>
		{

			var left = CONSOLE.CursorLeft;
			var top = CONSOLE.CursorTop;
			//var width = CONSOLE.WindowWidth; //TODO: под *никс тут вот так. Надо проверить все.
			//var height = CONSOLE.WindowHeight;
			var width = CONSOLE.BufferWidth;
			var height = CONSOLE.BufferHeight;

			void NextLine(int l = 0)
			{
				left = l;
				if (++top >= height)
					++Scroll;
			}

			var line = _encoding.GetString(buffer, offset, count);

			foreach (var ch in line)
			{
				switch (ch)
				{
				case '\r':
					left = 0;
					break;
				case '\t':
					left += 4 - left % 4;
					if (left >= width)
						NextLine();
					continue;
				case '\n':
					NextLine();
					break;
				default:
					++left;
					break;
				}

				if (left >= width)
					NextLine(1);
			}
		});

	public void Dispose()
	{
		CONSOLE.SetOut(_prevWriter);
		_streamWriter.Dispose();
	}
}