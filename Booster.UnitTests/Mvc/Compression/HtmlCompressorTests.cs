using System;
using System.Text.RegularExpressions;
using Booster.Mvc.Compression;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Mvc.Compression;

[TestClass]
public class HtmlCompressorTests
{
	[TestMethod]
	public void Test01()
	{
		var comp = new HtmlCompressor();

		const string input = HtmlCompressorTestData.Html01;
		var output = comp.Compress(input);

		Assert.IsNotNull(output);
		var ratio = Math.Round((double)output.Length / input.Length * 100.0, 2);
		Assert.IsTrue(ratio < 100.0);
	}

	[TestMethod]
	public void Test02()
	{
		var comp = new HtmlCompressor
		{
			PreservePatterns =
			{
				new Regex(@"<!-- PROTECTED: .*? -->", RegexOptions.Singleline | RegexOptions.IgnoreCase)
			}
		};

		const string input = HtmlCompressorTestData.Html02A;
		var output = comp.Compress(input);

		Assert.IsNotNull(output);
		var ratio = Math.Round((double)output.Length / input.Length * 100.0, 2);
		Assert.IsTrue(ratio < 100.0);
	}

	[TestMethod]
	public void TestEnabled()
	{
		var compressor = new HtmlCompressor(HtmlCompressorOptions.Disabled);
		var compress = compressor.Compress(HtmlCompressorTestData.TestEnabled);
		Assert.AreEqual(HtmlCompressorTestData.TestEnabledResult, compress);
	}

	[TestMethod]
	public void TestRemoveSpacesInsideTags()
	{
		var compressor = new HtmlCompressor(HtmlCompressorOptions.Default ^ HtmlCompressorOptions.RemoveMultiSpaces);

		var compress = compressor.Compress(HtmlCompressorTestData.TestRemoveSpacesInsideTags);
		Assert.AreEqual(HtmlCompressorTestData.TestRemoveSpacesInsideTagsResult, compress);

	}

	[TestMethod]
	public void TestRemoveComments()
	{
		var compressor = new HtmlCompressor(HtmlCompressorOptions.Default | HtmlCompressorOptions.RemoveComments | HtmlCompressorOptions.RemoveIntertagSpaces);
 
		var compress = compressor.Compress(HtmlCompressorTestData.TestRemoveComments);
		Assert.AreEqual(HtmlCompressorTestData.TestRemoveCommentsResult, compress);

	}

	[TestMethod]
	public void TestRemoveQuotes()
	{
		var compressor = new HtmlCompressor(HtmlCompressorOptions.Default | HtmlCompressorOptions.RemoveQuotes);
		var compress = compressor.Compress(HtmlCompressorTestData.TestRemoveQuotes);
		Assert.AreEqual(HtmlCompressorTestData.TestRemoveQuotesResult, compress);

	}

	[TestMethod]
	public void TestRemoveMultiSpaces()
	{
		var compressor = new HtmlCompressor(HtmlCompressorOptions.Enabled | HtmlCompressorOptions.RemoveMultiSpaces);
		var compress = compressor.Compress(HtmlCompressorTestData.TestRemoveMultiSpaces);
		Assert.AreEqual(HtmlCompressorTestData.TestRemoveMultiSpacesResult, compress);

	}

	[TestMethod]
	public void TestRemoveIntertagSpaces()
	{
		var compressor = new HtmlCompressor(HtmlCompressorOptions.Default | HtmlCompressorOptions.RemoveIntertagSpaces);
		var compress = compressor.Compress(HtmlCompressorTestData.TestRemoveIntertagSpaces);
		Assert.AreEqual(HtmlCompressorTestData.TestRemoveIntertagSpacesResult, compress);

	}

	[TestMethod]
	public void TestPreservePatterns()
	{
		//<?php ... ?> blocks
		//<% ... %> blocks
		//<!--# ... --> blocks
		//<jsp: ... > tags

		var compressor = new HtmlCompressor(HtmlCompressorOptions.Default | HtmlCompressorOptions.RemoveComments | HtmlCompressorOptions.RemoveIntertagSpaces)
		{
			PreservePatterns =  {
				HtmlCompressor.PhpTagPattern,
				HtmlCompressor.ServerScriptTagPattern,
				HtmlCompressor.ServerSideIncludePattern,
				new Regex("<jsp:.*?>", RegexOptions.Singleline | RegexOptions.IgnoreCase)
			}
		};

		var compress = compressor.Compress(HtmlCompressorTestData.TestPreservePatterns);
		Assert.AreEqual(HtmlCompressorTestData.TestPreservePatternsResult, compress);

	}
/*
        [TestMethod]
        public void TestCompressJavaScriptYui()
        {
            var compressor = new HtmlCompressor(HtmlCompressorOptions.Default | HtmlCompressorOptions.CompressJavaScript | HtmlCompressorOptions.RemoveIntertagSpaces);
            var compress = compressor.Compress(HtmlCompressorTestData.TestCompressJavaScript);
            Assert.AreEqual(HtmlCompressorTestData.TestCompressJavaScriptYuiResult, compress);

        }
*/
	/*
	[TestMethod]
	public void TestCompressJavaScriptClosure()
	{
	    var compressor = new HtmlCompressor(HtmlCompressorOptions.Default | HtmlCompressorOptions.CompressJavaScript | HtmlCompressorOptions.RemoveIntertagSpaces)
	    {
	        JavaScriptCompressor = new ClosureJavaScriptCompressor(CompilationLevel.ADVANCED_OPTIMIZATIONS)
	    };
	    var compress = compressor.Compress(HtmlCompressorTestData.TestCompressJavaScript);
	    Assert.AreEqual(HtmlCompressorTestData.TestCompressJavaScriptClosureResult, compress);

	}
	*/
/*
        [TestMethod]
        public void TestCompressCss()
        {
            var compressor = new HtmlCompressor(HtmlCompressorOptions.Default | HtmlCompressorOptions.CompressCss | HtmlCompressorOptions.RemoveIntertagSpaces);
            var compress = compressor.Compress(HtmlCompressorTestData.TestCompressCss);
            Assert.AreEqual(HtmlCompressorTestData.TestCompressCssResult, compress);

        }
*/
	[TestMethod]
	public void TestCompress()
	{
		var compressor = new HtmlCompressor();

		var compress = compressor.Compress(HtmlCompressorTestData.TestCompress);
		Assert.AreEqual(HtmlCompressorTestData.TestCompressResult, compress);
	}

	[TestMethod]
	public void TestSimpleDoctype()
	{
		var compressor = new HtmlCompressor(HtmlCompressorOptions.Default | HtmlCompressorOptions.SimpleDoctype);
		var compress = compressor.Compress(HtmlCompressorTestData.TestSimpleDoctype);
		Assert.AreEqual(HtmlCompressorTestData.TestSimpleDoctypeResult, compress);

	}

	[TestMethod]
	public void TestRemoveScriptAttributes()
	{
		var compressor = new HtmlCompressor(HtmlCompressorOptions.Default | HtmlCompressorOptions.RemoveScriptAttributes);
		var compress = compressor.Compress(HtmlCompressorTestData.TestRemoveScriptAttributes);
		Assert.AreEqual(HtmlCompressorTestData.TestRemoveScriptAttributesResult, compress);

	}

	[TestMethod]
	public void TestRemoveStyleAttributes()
	{
		var compressor = new HtmlCompressor(HtmlCompressorOptions.Default | HtmlCompressorOptions.RemoveStyleAttributes);
		var compress = compressor.Compress(HtmlCompressorTestData.TestRemoveStyleAttributes);
		Assert.AreEqual(HtmlCompressorTestData.TestRemoveStyleAttributesResult, compress);

	}

	[TestMethod]
	public void TestRemoveLinkAttributes()
	{
		var compressor = new HtmlCompressor(HtmlCompressorOptions.Default | HtmlCompressorOptions.RemoveLinkAttributes);
		var compress = compressor.Compress(HtmlCompressorTestData.TestRemoveLinkAttributes);
		Assert.AreEqual(HtmlCompressorTestData.TestRemoveLinkAttributesResult, compress);

	}

	[TestMethod]
	public void TestRemoveFormAttributes()
	{
		var compressor = new HtmlCompressor(HtmlCompressorOptions.Default | HtmlCompressorOptions.RemoveFormAttributes);
		var compress = compressor.Compress(HtmlCompressorTestData.TestRemoveFormAttributes);
		Assert.AreEqual(HtmlCompressorTestData.TestRemoveFormAttributesResult, compress);

	}

	[TestMethod]
	public void TestRemoveInputAttributes()
	{
		var compressor = new HtmlCompressor(HtmlCompressorOptions.Default | HtmlCompressorOptions.RemoveInputAttributes);
		var compress = compressor.Compress(HtmlCompressorTestData.TestRemoveInputAttributes);
		Assert.AreEqual(HtmlCompressorTestData.TestRemoveInputAttributesResult, compress);

	}

	[TestMethod]
	public void TestRemoveJavaScriptProtocol()
	{
		var compressor = new HtmlCompressor(HtmlCompressorOptions.Default | HtmlCompressorOptions.RemoveJavaScriptProtocol);
		var compress = compressor.Compress(HtmlCompressorTestData.TestRemoveJavaScriptProtocol);
		Assert.AreEqual(HtmlCompressorTestData.TestRemoveJavaScriptProtocolResult, compress);

	}

	[TestMethod]
	public void TestRemoveHttpProtocol()
	{
		var compressor = new HtmlCompressor(HtmlCompressorOptions.Default | HtmlCompressorOptions.RemoveHttpProtocol);
		var compress = compressor.Compress(HtmlCompressorTestData.TestRemoveHttpProtocol);
		Assert.AreEqual(HtmlCompressorTestData.TestRemoveHttpProtocolResult, compress);

	}

	[TestMethod]
	public void TestRemoveHttpsProtocol()
	{
		var compressor = new HtmlCompressor(HtmlCompressorOptions.Default | HtmlCompressorOptions.RemoveHttpsProtocol);
		var compress = compressor.Compress(HtmlCompressorTestData.TestRemoveHttpsProtocol);
		Assert.AreEqual(HtmlCompressorTestData.TestRemoveHttpsProtocolResult, compress);

	}

	[TestMethod]
	public void TestPreserveLineBreaks()
	{
		var compressor = new HtmlCompressor(HtmlCompressorOptions.Default | HtmlCompressorOptions.PreserveLineBreaks);
		var compress = compressor.Compress(HtmlCompressorTestData.TestPreserveLineBreaks);
		Assert.AreEqual(HtmlCompressorTestData.TestPreserveLineBreaksResult, compress);

	}

	[TestMethod]
	public void TestSurroundingSpaces()
	{
		var compressor = new HtmlCompressor(HtmlCompressorOptions.Default | HtmlCompressorOptions.RemoveIntertagSpaces)
		{
			RemoveSurroundingSpacesTags = "p,br"
		};
		var compress = compressor.Compress(HtmlCompressorTestData.TestSurroundingSpaces);
		Assert.AreEqual(HtmlCompressorTestData.TestSurroundingSpacesResult, compress);

	}
}