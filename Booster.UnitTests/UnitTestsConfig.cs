using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Booster.Collections;

#nullable enable

namespace Booster.UnitTests;

public class UnitTestsConfig
{
	public class ConnectionString
	{
		[XmlAttribute]
		public string? Name;

		[XmlAttribute]
		public bool Enabled = true;

		[XmlText]
		public string? Value;
	}

	[XmlElement("ConnectionString")]
	public ConnectionString[] SerializableDictionary
	{
		get => ConnectionStrings.Select(p => new ConnectionString { Name = p.Key, Value = p.Value }).ToArray();
		set
		{
			ConnectionStrings.Clear();

			foreach (var cstr in value)
			{
				if (!cstr.Enabled)
					continue;

				ConnectionStrings[cstr.Name!] = cstr.Value;
			}
		}
	}

	[XmlIgnore]
	public readonly HybridDictionary<string, string?> ConnectionStrings = new(StringComparer.OrdinalIgnoreCase);

	static readonly XmlSerializer _xmlSerializer = new(typeof(UnitTestsConfig));

	public static UnitTestsConfig Load(string filename)
	{
		using var stream = new FileStream(filename, FileMode.Open, FileAccess.Read);
		var result = (UnitTestsConfig)_xmlSerializer.Deserialize(stream)!;
		return result;
	}

	public static UnitTestsConfig Instance => _instance.GetValue(null).NoCtxResult();

	static readonly AsyncLazy<UnitTestsConfig> _instance = new()
	{
		Factory = () => Task.FromResult(Load("unittest_config.xml"))
	};
}