//using System;
//using Booster.Vectors;
//using Microsoft.VisualStudio.TestTools.UnitTesting;

//namespace Booster.UnitTests
//{
//	[TestClass]
//	public class VectorsTests
//	{
//		[TestMethod]
//		public void Angle2D()
//		{
//			for (int iter = 0; iter < 1000; iter++)
//			{
//				var angle1 = Rnd.Double(0, 360);
//				var angle2 = Rnd.Double(0, 360);

//				var len1 = Rnd.Double(0.1, 180);
//				var len2 = Rnd.Double(0.1, 180);

//				Vector2D a = new Vector2D(Math.Cos(angle1 * Math.PI / 180.0), Math.Sin(angle1* Math.PI / 180.0))*len1;
//				Vector2D b = new Vector2D(Math.Cos((angle1 + angle2)* Math.PI / 180.0), Math.Sin((angle1 + angle2)* Math.PI / 180.0))*len2;

//				var res = a.AngleTo(b)*180/Math.PI;
//				if (res < 0) res += 360;

//				var delta = Math.Abs(angle2 - res);

//				Assert.IsTrue(delta < 0.1);
//			}
//		}
//	}
//}