using System.Threading.Tasks;
using Booster.CodeGen;
using Booster.Reflection;
using JetBrains.Annotations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.CodeGen;

[TestClass]
public class ExecutorTests
{
	static Executer CreateExecuter(string code)
	{
		var result = new Executer(code, null);
		result.AddUsingFor(typeof(Utils));
		result.AddUsingFor(typeof(ExecutorTests));

		if (code.Contains("str"))
			result.Parameters.Add("str", TypeEx.Get<string>());

		return result;
	}

	[UsedImplicitly]
	public static string TestMethod()
		=> "Ok";

	[TestMethod]
	public async Task ExecutorGeneralBehaviourTests()
	{
		// ReSharper disable MethodHasAsyncOverload
		var exe = CreateExecuter("ExecutorTests.TestMethod()");
		Assert.AreEqual("Ok", exe.Execute());

		exe = CreateExecuter("ExecutorTests.TestMethod()");
		Assert.AreEqual("Ok", exe.Execute());

		exe = CreateExecuter("ExecutorTests.TestMethod();");
		Assert.AreEqual("Ok", exe.Execute());

		exe = CreateExecuter("ExecutorTests.TestMethod(); ExecutorTests.TestMethod();");
		Assert.AreEqual("Ok", exe.Execute());

		exe = CreateExecuter("{ ExecutorTests.TestMethod(); }");
		Assert.AreEqual("Ok", exe.Execute());

		exe = CreateExecuter("\"123\"");
		Assert.AreEqual("123", exe.Execute());

		exe = CreateExecuter("str.Length");
		Assert.AreEqual(3, exe.Execute("ABC"));

		exe =CreateExecuter("123");
		Assert.AreEqual(123, exe.Execute());
		// ReSharper restore MethodHasAsyncOverload

		exe = CreateExecuter("await Task.FromResult(str)");
		Assert.AreEqual("OK!", await exe.ExecuteAsync("OK!"));

		exe = CreateExecuter("{ await Task.Delay(100); return await Task.FromResult(str); }");
		Assert.AreEqual("OK!", await exe.ExecuteAsync("OK!"));

		exe = CreateExecuter("await Task.Delay(100); return await Task.FromResult(str);");
		Assert.AreEqual("OK!", await exe.ExecuteAsync("OK!"));
	}
}