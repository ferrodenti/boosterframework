using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Booster.BigFile;
using Booster.FileSystem;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.BigFile;

[TestClass]
public class BigFileTests
{
	[TestMethod]
	public void GeneralBehaviour()
	{
		const string fileName = "test.bin";

		for (var i = 0; i < 30; ++i)
		{
			FileUtils.DeleteFile(fileName);

			var settings = new Settings
			{
				DefaultIndexSize = Rnd.Int(5, 128),
				DefaultSectionSize = Rnd.Int(5, 128)
			};
				
			using (var bigFile = new FilesContainer(fileName,settings))
			{
				var files = new FileTable(bigFile);
					
				using (var stream = files.Open("TEMP"))
				using (var wr = new StreamWriter(stream))
					wr.Write(new string('A', 100));
	
				files.Delete("TEMP");
					
				using (var stream = files.Open("42"))
				using (var wr = new StreamWriter(stream))
				{
					wr.Write("0123456789 0123456789");
					wr.Flush();
					stream.SetLength(10);
				}
					
				using (var stream = files.Open("First file"))
				using (var wr = new StreamWriter(stream))
				{
					wr.Write("Hello world");
					wr.Write("123");
				}
					
				using (var stream = files.Open("Second file"))
				using (var wr = new StreamWriter(stream))
				using (var stream2 = files.Open("Third file"))
				using (var wr2 = new StreamWriter(stream2))
				{
					wr2.Write("This ");
					wr2.Write("Is ");
					wr.Write("Hello world 234");
					wr2.Write("A ");
					wr2.Write("Test ");
				}
					
				bigFile.Defrag();
			}

			using (var bigFile = new FilesContainer(fileName, settings))
			{
				var files = new FileTable(bigFile);
					
				using (var stream = files.Open("Third file", false))
				using (var rd = new StreamReader(stream))
				using (var wr = new StreamWriter(stream))
				{
					var actual = rd.ReadToEnd();
					Assert.AreEqual("This Is A Test ", actual);

					stream.Seek(5, SeekOrigin.Begin);
					wr.Write("is a t");
				}
					
				using (var stream = files.Open("Third file", false))
				using (var rd = new StreamReader(stream))
				{
					var actual = rd.ReadToEnd();
					Assert.AreEqual("This is a test ", actual);
				}
					
				using (var stream = files.Open("42", false))
				using (var rd = new StreamReader(stream))
				{
					var actual = rd.ReadToEnd();
					Assert.AreEqual("0123456789", actual);
				}


				using (var stream = files.Open("Second file", false))
				using (var rd = new StreamReader(stream))
				using (var stream2 = files.Open("First file", false))
				using (var rd2 = new StreamReader(stream2))
				{
					var actual = rd.ReadToEnd();
					Assert.AreEqual("Hello world 234", actual);

					var actual2 = rd2.ReadToEnd();
					Assert.AreEqual("Hello world123", actual2);
				}
			}
		}
	}

	[TestMethod]
	[SuppressMessage("ReSharper", "AccessToDisposedClosure")]
	public async Task ConcurrencyTest()  //TODO: fix test
	{
		const string fileName = "conc_test.bin";
		const int numThreads = 15;
		const int numStrings = 15;
		const int numIters = 200;
		const int numReloads = 10;

		var tasks = new List<Task>();

		await FileUtils.DeleteFileAsync(fileName);

		// Тут важно заполнение ascii строками, иначе потом ридеры врайтеры будут работать по неправильным смещениям
		var strings = Enumerable.Range(0, numThreads * numStrings).Select(_ => TestUtils.RandomAsciiString(Rnd.Int(5, 10))).ToArray();
			
		using (var bigFile = new FilesContainer(fileName, new Settings {DefaultIndexSize = Rnd.Int(20, 50), DefaultSectionSize = Rnd.Int(16, 150)}))
			for (var j = 0; j < numStrings * numThreads; ++j)
#if NETCOREAPP
				await using (var stream = bigFile.Open(j + 1))
				await using (var wr = new StreamWriter(stream))
#else
				using (var stream = bigFile.Open(j + 1))
				using (var wr = new StreamWriter(stream))
#endif
					await wr.WriteAsync(strings[j]);

		for(var m=0; m<numReloads; ++m)
		{
			using var bigFile = new FilesContainer(fileName, new Settings {DefaultIndexSize = Rnd.Int(20, 50), DefaultSectionSize = Rnd.Int(16, 150)});
			for (var t = 0; t < numThreads; ++t)
			{
				var taskNo = t;
				var m1 = m;
				var t1 = t;
				tasks.Add(Utils.StartTaskNoFlow(() =>
				{
					void CheckString(int j)
					{
						using var stream = bigFile.Open(j + 1);
						using var rd = new StreamReader(stream);
						var actual = rd.ReadToEnd();

						if (!StringCompareHelper.Instance.IsEqual(actual, strings[j], out var msg))
						{
							Trace.WriteLine(msg);
							Assert.Fail($"{msg} m={m1}, t={t1}");
						}
					}

					void CheckStrings()
					{
						for (var j = 0; j < numStrings; ++j)
							CheckString(taskNo * numStrings + j);
					}

					CheckStrings();

					for (var k = 0; k < numIters / numReloads; ++k)
					{
						var j = taskNo * numStrings + Rnd.Int(numStrings);

						var str = strings[j];
						var str2 = TestUtils.RandomAsciiString(Rnd.Int(10));
						int to;


						using (var stream = bigFile.Open(j + 1, false))
						using (var wr = new StreamWriter(stream))
							switch (Rnd.Int(6))
							{
							case 0:
								strings[j] = str + str2;
								stream.Seek(0, SeekOrigin.End);
								wr.Write(str2);
								wr.Flush();
								CheckString(j);
								break;
							case 1:
								var from = Rnd.Int(str.Length);
								to = from + str2.Length;
								strings[j] = str.Substring(0, from) + str2 + (to < str.Length ? str.Substring(from + str2.Length) : "");
								stream.Seek(from, SeekOrigin.Begin);
								wr.Write(str2);
								wr.Flush();
								CheckString(j);
								break;
							case 2:
								to = Rnd.Int(str.Length);
								strings[j] = str.Substring(0, to);
								stream.SetLength(to);
								stream.Flush();
								CheckString(j);
								break;
							case 3:
								to = Rnd.Int(10);
								strings[j] = str + new string('\0', to);
								stream.SetLength(str.Length + to);
								stream.Flush();
								CheckString(j);
								break;
							case 4:
								strings[j] = str2;
								stream.Seek(0, SeekOrigin.Begin);
								wr.Write(str2);
								stream.SetLength(str2.Length);
								wr.Flush();
								CheckString(j);
								break;
							case 5:
								var id = -taskNo - 999;
								var tmp = bigFile.Open(id);
								using (var wr2 = new StreamWriter(tmp))
									wr2.Write(TestUtils.RandomString(100));

								bigFile.Delete(id);
								break;
							}

						CheckStrings();

						//if (Rnd.Bool(60))
						//	bigFile.Defrag();

					}
				}));
			}

			foreach (var task in tasks)
				await task.NoCtx();
		}

		using (var bigFile = new FilesContainer(fileName))
			for (var j = 0; j < numStrings * numThreads; ++j)
#if NETCOREAPP
				await using (var stream = bigFile.Open(j + 1))
#else
				using (var stream = bigFile.Open(j + 1))
#endif
				using (var rd = new StreamReader(stream))
				{
					var actual = await rd.ReadToEndAsync();

					StringCompareHelper.Instance.AssertAreEqual(strings[j], actual);
				}
	}
}