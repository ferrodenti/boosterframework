using Booster.Parsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Infrastructure;

[TestClass]
public class WildcardsTests
{
	[TestMethod]
	public void GeneralBehaviour()
	{
		Assert.IsTrue(Wildcards.Compare("blah.jpg", "bL?h.*"));
		Assert.IsFalse(Wildcards.Compare("blah.jpg", "bL?h.*", false));
		Assert.IsFalse(Wildcards.Compare("1lah.jpg", "bl?h.*"));
		Assert.IsTrue(Wildcards.Compare("bl1h.jpg", "bl?h.*"));
		Assert.IsFalse(Wildcards.Compare("bla1.jpg", "bl?h.*"));
		Assert.IsTrue(Wildcards.Compare("1blah.jpg", "*bl?h.*"));
		Assert.IsTrue(Wildcards.Compare("blah.jpg", "*bl?h.*"));
		Assert.IsTrue(Wildcards.Compare("blah.jpg", "*"));
		Assert.IsFalse(Wildcards.Compare("blah.jpg", "a"));
		Assert.IsTrue(Wildcards.Compare("blah.jpg", "*.*"));
		Assert.IsTrue(Wildcards.Compare("blah.jpg", "b*g"));
		Assert.IsTrue(Wildcards.Compare("blah.jpg", "*b*g*"));
		Assert.IsTrue(Wildcards.Compare("blah.jpg", "*b*?g*"));
		Assert.IsFalse(Wildcards.Compare("blah.jpg", "*bl?h.*s"));
		Assert.IsTrue(Wildcards.Compare("test\\obj\\some\\thing", "*\\obj\\*"));
	}

}