using Booster.Parsing;
using Booster.Parsing.Operators;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Infrastructure;

[TestClass]
public class BaseExpressionParserTests
{
	[TestMethod]
	public void Tests()
	{
		var parser = new ExpressionParser();

		parser.AddOperator<AndOperator>(0, "&&");
		parser.AddOperator<OrOperator>(0, "||");
		parser.AddOperator<XorOperator>(0, "^");
		parser.AddOperator<NotOperator>(0, "!", true);

		parser.AddOperator<EqualsOperator>(1, "==" );
		parser.AddOperator<NotEqualsOperator>(1, "!=" );
		parser.AddOperator<LessOrEqualOperator>(1, "<=" );
		parser.AddOperator<GreaterOrEqualOperator>(1, ">=" );
		parser.AddOperator<LessOperator>(1, "<");
		parser.AddOperator<GreaterOperator>(1, ">");

		parser.AddOperator<NegOperator>(2, "-", true );

		parser.AddOperator<SumOperator>(3, "+");
		parser.AddOperator<SubOperator>(3, "-");

		parser.AddOperator<MulOperator>(4, "*");
		parser.AddOperator<DivOperator>(4, "/");


		//TODO: a || b || c считается как (a || b) || c, а быстрее/понятнее будет считать a || (b || c)

		var op = parser.Parse("a + b * c >= a * b + c && -a != -1.07 || !(a * (c+qwe) == \"hghfh \\\\\" njn\")"); //TODO: fix test
		Utils.Nop(op);
	}
}