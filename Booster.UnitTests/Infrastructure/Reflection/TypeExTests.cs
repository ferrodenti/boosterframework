using System;
using System.Linq;
using Booster.Reflection;
using JetBrains.Annotations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Infrastructure.Reflection;

[TestClass]
public class  TypeExTests
{
	[UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
	public interface ITest<T>
	{
		void Method(int a);

		T Property1 { get; set; }
		T Property2 { get; }
		T Property3 { set; }
	}

	[UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
	public class Test1
	{
		public int Property2 => 1;

		protected virtual event EventHandler OverridenEvent;

		public virtual int OverridenProp { get; set; }
		public virtual int OverridenMethod()
			=> 1;

		protected virtual void OnOverridedEvent()
			=> OverridenEvent?.Invoke(this, EventArgs.Empty);
	}

	public class Test2 : Test1, ITest<int>, ITest<string>
	{
		void ITest<int>.Method(int a)
			=> Utils.Nop(a);

		int ITest<int>.Property1 { get; set; }
		public int Property3 { set => Property1 = value; }


		public void Method(int a)
			=> Utils.Nop();

		public int Property1 { get; set; }

		string ITest<string>.Property1 { get; set; }
		string ITest<string>.Property3 { set { } }
		string ITest<string>.Property2 => null;

		public override int OverridenProp
		{
			get => 0;
			set => Utils.Nop(value);
		}
		protected override event EventHandler OverridenEvent
		{
			add => base.OverridenEvent += value;
			remove => base.OverridenEvent -= value;
		}
		public override int OverridenMethod()
			=> 2;
	}

	public class Test3 : Test2
	{
		public override int OverridenProp => 1;
		protected override event EventHandler OverridenEvent
		{
			add => base.OverridenEvent += value;
			remove => base.OverridenEvent -= value;
		}
		public override int OverridenMethod()
			=> 3;

		public new void Method(int a)
			=> Utils.Nop(a);
	}
		
	[TestMethod]
	public void Tests()
	{
		TypeEx ifc = typeof (ITest<int>);
		var im = ifc.FindMethod("Method")!;
		var ip1 = ifc.FindProperty("Property1");
		var ip2 = ifc.FindProperty("Property2");
		var ip3 = ifc.FindProperty("Property3");

		TypeEx cls1 = typeof (Test1);
		TypeEx cls2 = typeof (Test2);
		TypeEx cls3 = typeof (Test3);

		Assert.IsTrue(cls1 == typeof (Test1));
		Assert.IsTrue(cls1.Methods[0] == cls1.Methods[0].Inner);

		foreach (var tp in new[] {cls1, cls2, cls3})
		{
			Assert.AreEqual(1, tp.FindProperties("OverridenProp").Count());
			Assert.AreEqual(1, tp.FindEvents("OverridenEvent").Count());
			Assert.AreEqual(1, tp.FindMethods("OverridenMethod").Count());
		}

		var ifcName = ifc.CompliantName;
		Assert.AreEqual(cls3.FindMethod($"{ifcName}.Method", typeof (int)), im.FindImplementation(cls3));
		Assert.AreEqual(cls3.FindProperty($"{ifcName}.Property1"), ip1.FindImplementation(cls3));
		Assert.AreEqual(cls3.FindProperty("Property2"), ip2.FindImplementation(cls3));
		Assert.AreEqual(cls3.FindProperty("Property3"), ip3.FindImplementation(cls3));

		ifc = typeof (ITest<string>);
		im = ifc.FindMethod("Method");
		ip1 = ifc.FindProperty("Property1");
		ip2 = ifc.FindProperty("Property2");
		ip3 = ifc.FindProperty("Property3");

		ifcName = ifc.CompliantName;
		var actual = im!.FindImplementation(cls3);

		var ifcName2 = TypeEx.Get<ITest<int>>().CompliantName;
		var expect = cls2.FindMethod($"{ifcName2}.Method", new[] {typeof(int)}, filter => filter.Access = AccessModifier.Private);

		Assert.AreEqual(expect, actual);
		Assert.AreEqual(cls3.FindProperty($"{ifcName}.Property1"), ip1.FindImplementation(cls3));
		Assert.AreEqual(cls3.FindProperty($"{ifcName}.Property2"), ip2.FindImplementation(cls3));
		Assert.AreEqual(cls3.FindProperty($"{ifcName}.Property3"), ip3.FindImplementation(cls3));
	}
}