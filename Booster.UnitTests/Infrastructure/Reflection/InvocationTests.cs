using System;
using System.Diagnostics;
using System.Linq;
using Booster.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Infrastructure.Reflection;

[TestClass]
public class InvocationTests
{
	protected class TestClass
	{
		public string Str;

		public TestClass()
			=> Str = "Empty ctor";

		protected TestClass(int a, long b, decimal? c)
			=> Str = $"a={a}, b={b}, c={c}";

		protected string Method(int a, long b, decimal? c)
			=> Str = $"a={a}, b={b}, c={c}";

		protected string Method2(int a, long b, decimal? c = 42)
			=> Str = $"a={a}, b={b}, c={c}";

		protected string Method3(int a, long b, int n=1, params decimal[] c)
			=> Str = $"a={a}, b={b}, n={n}, c={c.ToString(new EnumBuilder(","))}";

		protected static string Method4(int a, long b, params decimal[] c)
			=> $"a={a}, b={b}, c={c.ToString(new EnumBuilder(","))}";

		protected string Method5(int a, long b, decimal? c = null)
			=> Str = $"a={a}, b={b}, c={c}";

		protected static void Method6(string str)
			=> Trace.WriteLine(str);
	}

	struct TestStruct
	{
		public long Field;

		// ReSharper disable once FieldCanBeMadeReadOnly.Local
		public static long StaticField = Rnd.Long();

		// ReSharper disable once UnusedMember.Local
		// ReSharper disable once InconsistentNaming
		const int Const = 42;
	}

	[TestMethod]
	public void ValueTypesInvocation()
	{
		TypeEx tint = typeof (int);
		var hash = tint.FindMethod("GetTypeCode");
		Assert.AreEqual(3.GetTypeCode(), hash.Invoke(3));

		hash = tint.FindMethod(new ReflectionFilter("CompareTo", typeof (int)) {CastType = CastType.Deny});
		Assert.AreEqual(3.CompareTo(2), hash.Invoke(3, 2));

		var dt = DateTime.Now;
		var tdt = TypeEx.Of(dt);
		hash = tdt.FindMethod("GetHashCode");
		Assert.AreEqual(dt.GetHashCode(), hash.Invoke(dt));

		var prop = tdt.FindProperty("Year");
		Assert.AreEqual(dt.Year, prop.GetValue(dt));

		var ts = new TestStruct {Field = Rnd.Long()};
		var tp = TypeEx.Of(ts);
			
		var field = tp.FindField("Field");
		Assert.AreEqual(ts.Field, field.GetValue(ts));

		field = tp.FindField("StaticField");
		Assert.AreEqual(TestStruct.StaticField, field.GetValue(null));

		field.SetValue(null, (long)42);
		Assert.AreEqual(42, TestStruct.StaticField);

		field = tp.FindField("Const");
		Assert.AreEqual(42, field.GetValue(null));
	}

	[TestMethod]
	public void CtorsAndMethods()
	{
		TypeEx t = typeof (TestClass);

		var o1 = (TestClass) t.Create();
		var o2 = (TestClass) t.Create(1, 2, null);
		var o3 = (TestClass) t.Create(1, 2, 3);

		Assert.AreEqual("Empty ctor", o1.Str);
		Assert.AreEqual("a=1, b=2, c=", o2.Str);
		Assert.AreEqual("a=1, b=2, c=3", o3.Str);

		var method = t.FindMethod("Method");
		Assert.IsTrue(method.CanBeInvokedWith(new[] {1, 2, 3}.Select(v => TypeEx.Of(v)).ToArray()));
		Assert.IsFalse(method.CanBeInvokedWith(new object[] {"", 2, 3}.Select(TypeEx.Of).ToArray()));
		Assert.AreEqual("a=1, b=2, c=3", method.Invoke(o1, 1, 2, 3));
		Assert.AreEqual("a=1, b=2, c=3", o1.Str);

		var method2 = t.FindMethod("Method2");
		Assert.IsTrue(method2.CanBeInvokedWith(new[] {1, 2}.Select(v => TypeEx.Of(v)).ToArray()));
		Assert.IsFalse(method2.CanBeInvokedWith(new[] {1, 2, 3, 4}.Select(v => TypeEx.Of(v)).ToArray()));
		Assert.AreEqual("a=1, b=2, c=42", method2.Invoke(o2, 1, 2));
		Assert.AreEqual("a=1, b=2, c=42", o2.Str);

		var method5 = t.FindMethod("Method5");
		Assert.IsTrue(method5.CanBeInvokedWith(new[] {1, 2}.Select(v => TypeEx.Of(v)).ToArray()));
		Assert.IsFalse(method5.CanBeInvokedWith(new[] {1, 2, 3, 4}.Select(v => TypeEx.Of(v)).ToArray()));
		Assert.AreEqual("a=1, b=2, c=", method5.Invoke(o2, 1, 2));
		Assert.AreEqual("a=1, b=2, c=", o2.Str);

		var method3 = t.FindMethod("Method3");
		Assert.IsTrue(method3.CanBeInvokedWith(new[] {1, 2, 3, 4, 5}.Select(v => TypeEx.Of(v)).ToArray()));
		Assert.IsFalse(method3.CanBeInvokedWith(new object[] {1, DateTime.Now, 3, 4, 5}.Select(TypeEx.Of).ToArray()));
		Assert.AreEqual("a=1, b=2, n=3, c=4,5", method3.Invoke(o3, 1, 2, 3, 4, 5));
		Assert.AreEqual("a=1, b=2, n=3, c=4,5", o3.Str);
		Assert.AreEqual("a=1, b=2, n=1, c=", method3.Invoke(o3, 1, 2));

		var method4 = t.FindMethod("Method4");
		Assert.IsTrue(method4.CanBeInvokedWith(new[] {1, 2, 3, 4, 5}.Select(v => TypeEx.Of(v)).ToArray()));
		Assert.IsFalse(method4.CanBeInvokedWith(new[] {1}.Select(v => TypeEx.Of(v)).ToArray()));
		Assert.AreEqual("a=1, b=2, c=3,4,5", method4.Invoke(null, 1, 2, 3, 4, 5));

		var method6 = t.FindMethod("Method6");
		method6.Invoke(null, "Test");
	}
}