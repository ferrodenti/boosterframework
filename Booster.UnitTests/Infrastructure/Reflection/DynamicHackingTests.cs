using System;
using System.Diagnostics;
using Booster.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Infrastructure.Reflection;

[TestClass]
public class SomeHackingTests
{
	public class MyClass
	{
		public Type Value = typeof (string);

		public void Test()
			=> Trace.WriteLine(Value.ToString());
	}

	[TestMethod]
	public void Tests()
	{
		var a = new MyClass();

		Trace.WriteLine(a.Value);

		Assert.IsTrue(TypeEx.Of(a).TryFindDataMember("Value", out var field));
		Assert.IsTrue(field.SetValue(a, DateTime.Now));

		Trace.WriteLine(a.Value);

		a.Test();
		
		
		try
		{
			Trace.WriteLine(a.Value.FullName);
		}
		catch (Exception e)
		{
			Trace.WriteLine(e.Message);
		}
	}
}