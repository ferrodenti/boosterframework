using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Infrastructure;

[TestClass]
public class PermutationGeneratorTests
{
    class PermutationSet<T>
    {
        readonly T[] _data;

        public PermutationSet(T[] data)
            => _data = data;

        public override int GetHashCode()
            => new HashCode(typeof(T)).Append(_data.Length).AppendEnumerable(_data);

        public override bool Equals(object obj)
            => obj is PermutationSet<T> b && b._data.Length == _data.Length && b._data.SequenceEqual(_data);

        public override string ToString()
            => _data.ToString(new EnumBuilder(", ", "[", "]"));
    }

    [TestMethod]
    public void Test()
    {
        var generator = PermutationGenerator.Instance;
        for (var n = 0; n <= 5; n++)
        {
            var factorial = (int) PermutationGenerator.Factorial(n);

            Trace.WriteLine($"N={n}, fact={factorial}");
            var dict = new HashSet<PermutationSet<int>>();

            foreach (var permutation in generator.GetPermutations(n))
            {
                var distinct = permutation.Distinct().ToArray();
                Assert.AreEqual(n, distinct.Length);
                var set = new PermutationSet<int>(permutation);
                Assert.IsTrue(dict.Add(set));
                Trace.WriteLine(set);
            }
            
            Assert.AreEqual(factorial, dict.Count);
            Trace.WriteLine("");
        }
    }
}