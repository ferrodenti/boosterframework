using Booster.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Infrastructure;

[TestClass]
public class ReplaceRegexTests
{
	[TestMethod]
	public void GeneralBehaviour()
	{
		var replace = new ReplaceRegex("(.+zz)$", "$1es");
		
		var res = replace.TryReplace("ButtBuzzSesationBuzz", out var act);
		Assert.IsTrue(res);
		Assert.AreEqual("ButtBuzzSesationBuzzes", act);
		
		res = replace.TryReplace("BUTTBUZZSESATIONBUZZ", out act);
		Assert.IsTrue(res);
		Assert.AreEqual("BUTTBUZZSESATIONBUZZES", act);
	}
}
