using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Infrastructure;

[TestClass]
public class RndTests
{
	const int _iters = 100000;

	[TestMethod]
	public void HiLimitTest()
	{
		for (var i=0; i < _iters; i++)
		{
			var hiInt = Rnd.Int(0, int.MaxValue);
			Assert.IsTrue(Rnd.Int(hiInt) < hiInt);
			var hiDouble = Rnd.Double(0, double.MaxValue);
			Assert.IsTrue(Rnd.Double(hiDouble) < hiDouble);
			var hiFloat = Rnd.Float(0, float.MaxValue);
			Assert.IsTrue(Rnd.Float(hiFloat) < hiFloat);
			var hiDecimal = Rnd.Decimal(0, decimal.MaxValue);
			Assert.IsTrue(Rnd.Decimal(hiDecimal) < hiDecimal);
			var hiLong = Rnd.Long(0, long.MaxValue);
			Assert.IsTrue(Rnd.Long(hiLong) < hiLong);
		}
	}
		
	[TestMethod]
	public void HiAndLowLimitTest()
	{
		for (var i=0; i < _iters; i++)
		{
			var hiInt = Rnd.Int(int.MinValue + 1, int.MaxValue);
			var lowInt = Rnd.Int(int.MinValue, hiInt);
			Assert.IsTrue(lowInt < hiInt);
			var rndInt = Rnd.Int(lowInt, hiInt);
			Assert.IsTrue(rndInt >= lowInt);
			Assert.IsTrue(rndInt < hiInt);

			var hiDouble = Rnd.Double(double.MinValue + 1, double.MaxValue);
			var lowDouble = Rnd.Double(double.MinValue, hiDouble);
			Assert.IsTrue(lowDouble < hiDouble);
			var rndDouble = Rnd.Double(lowDouble, hiDouble);
			Assert.IsTrue(rndDouble >= lowDouble);
			Assert.IsTrue(rndDouble < hiDouble);

			var hiFloat = Rnd.Float(float.MinValue + 1, float.MaxValue);
			var lowFloat = Rnd.Float(float.MinValue, hiFloat);
			Assert.IsTrue(lowFloat < hiFloat);
			double rndFloat = Rnd.Float(lowFloat, hiFloat);
			Assert.IsTrue(rndFloat >= lowFloat);
			Assert.IsTrue(rndFloat < hiFloat);

			var hiDecimal = Rnd.Decimal(decimal.MinValue + 1, decimal.MaxValue);
			var lowDecimal = Rnd.Decimal(decimal.MinValue, hiDecimal);
			Assert.IsTrue(lowDecimal < hiDecimal);
			var rndDecimal = Rnd.Decimal(lowDecimal, hiDecimal);
			Assert.IsTrue(rndDecimal >= lowDecimal);
			Assert.IsTrue(rndDecimal < hiDecimal);

			var hiLong = Rnd.Long(long.MinValue + 1, long.MaxValue);
			var lowLong = Rnd.Long(long.MinValue, hiLong);
			Assert.IsTrue(lowLong < hiLong);
			var rndLong = Rnd.Long(lowLong, hiLong);
			Assert.IsTrue(rndLong >= lowLong);
			Assert.IsTrue(rndLong < hiLong);
		}
	}

	[TestMethod]
	public void EnumerableShuffleTest()
	{
		for (var i = 0; i < _iters; ++i)
		{
			var expect = Enumerable.Range(0, Rnd.Int(10, 100)).Select(_ => Rnd.Int()).ToArray();
			var actual = expect.Shuffle();

			Assert.IsTrue(expect.HasSameElements(actual));
		}		
	}
}