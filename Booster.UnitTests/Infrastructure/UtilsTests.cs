﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

#nullable enable

namespace Booster.UnitTests.Infrastructure;

[TestClass]
public class UtilsTests
{
	[TestMethod]
	public void IsTest()
	{
		var arr = new[] { "1", "2", "3", "4", "5" };

		string? Find(string value)
			=> arr.FirstOrDefault(i => i == value);

		bool TryFind(string value, out string result)
			=> Find(value).Is(out result);

		Assert.IsTrue(TryFind("1", out var str));
		Assert.AreEqual("1", str);
		
		Assert.IsFalse(TryFind("6", out str));
	}
}
