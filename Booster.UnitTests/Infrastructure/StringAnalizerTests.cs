﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Booster.Parsing;
using Booster.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Infrastructure;

[TestClass]
public class StringAnalizerTests
{
	[TestMethod]
	public void Test()
	{
		var analizer = new StringAnalizer();

		var dict = new Dictionary<StringFlags, HashSet<StringFlags>>();
		var cache = new HashSet<StringFlags>();
		
		for (var ch = (char)0; ch < char.MaxValue; ch++)
		{
			var flags = analizer.Analize(ch);
			if (!cache.Add(flags))
				continue;
			
			var values = EnumHelper.GetValues(flags).PowerOfTwo.GetValues<StringFlags>();

			foreach (var flag in values)
			{
				var hashset = dict.GetOrAdd(flag, _ => new HashSet<StringFlags>());
				
				foreach (var flag2 in values) 
					hashset.Add(flag2);
			}
		}


		var keys = dict.Keys;
		var maxLen = keys.Max(k => k.ToString().Length) + 1;
		
		Trace.Write(new string(' ', maxLen));
		foreach (var col in keys)
		{
			Trace.Write(col);
			Trace.Write(' ');
		}
		Trace.Write("\n");
		
		foreach (var row in keys)
		{
			Trace.Write(row.ToString().PadRight(maxLen));

			var set = dict[row];
			foreach (var col in keys)
			{
				var len = col.ToString().Length + 1;
				Trace.Write(set.Contains(col) 
					? "v".PadRight(len, ' ') 
					: new string(' ', len));
			}
			Trace.Write("\n");
		}
		
		Trace.WriteLine($"' '={analizer.Analize(' ')}");
		Trace.WriteLine($"'\\r'={analizer.Analize('\r')}");
		Trace.WriteLine($"'\\n'={analizer.Analize('\n')}");
	}
}