using Booster.Localization.Pluralization;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Infrastructure.Expanders;

[TestClass]
public class CamelCaseConversionsTests
{
	[TestMethod]
	public void Tests()
	{
		//Assert.AreEqual("FACESTREAM TEST 12 QWE", string.Join(" ", "FACESTREAM_TEST12QWE".SplitCamelCase())); //TODO: Pass this

		Assert.AreEqual("This Is A Test", string.Join(" ", "ThisIsATest".SplitCamelCase()));
		Assert.AreEqual("This Is An ABBR Test", string.Join(" ", "ThisIsAnABBRTest".SplitCamelCase()));
		Assert.AreEqual("This Is ATEST", string.Join(" ", "ThisIsATEST".SplitCamelCase()));
	}

	[TestMethod]
	public void PluralizeTests()
	{
		Assert.AreEqual("UserRoles", EnPluralizer.PluralizeCamelCase("UserRole"));
		Assert.AreEqual("UserSQLS", EnPluralizer.PluralizeCamelCase("UserSQL"));
		Assert.AreEqual("RealMEN", EnPluralizer.PluralizeCamelCase("RealMAN"));
	}
}