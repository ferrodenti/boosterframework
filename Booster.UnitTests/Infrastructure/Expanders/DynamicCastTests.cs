using Booster.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Infrastructure.Expanders;

[TestClass]
public class DynamicCastTests
{
	#region Test types
	class A0
	{
		public readonly int I;

		public A0(int i)
		{
			I = i;
		}

		public static explicit operator A0(string s)
			=> new(int.Parse(s));

		public static implicit operator A0(int i)
			=> new(i);

		public static explicit operator string(A0 a)
			=> a.I.ToString();

		public static implicit operator int(A0 a)
			=> a.I;
	}
	class B
	{
		readonly int _i;

		public B(int i)
		{
			_i = i;
		}
		public static explicit operator B(A0 a)
			=> new(a.I);

		public static implicit operator B(int i)
			=> new(i);

		public static explicit operator B(string s)
			=> new(int.Parse(s));

		public static implicit operator string(B b)
			=> b._i.ToString();

		public static explicit operator int(B b)
			=> b._i;

		public static implicit operator A0(B b)
			=> new(b._i);
	}

	#endregion

	[TestMethod]
	public void Tests()
	{
		Assert.IsTrue(TypeEx.Get<int>().TryCast(42, typeof (A0), out var o));
		Assert.AreEqual(o.GetType(), typeof (A0));

		Assert.IsTrue(TypeEx.Get<int>().TryCast(42, typeof (B), out o));
		Assert.AreEqual(o.GetType(), typeof (B));

		Assert.IsTrue(TypeEx.Get<string>().TryCast("42", typeof (A0), out o));
		Assert.AreEqual(o.GetType(), typeof (A0));

		Assert.IsTrue(TypeEx.Get<string>().TryCast("42", typeof (B), out o));
		Assert.AreEqual(o.GetType(), typeof (B));

		Assert.IsTrue(TypeEx.Get<A0>().TryCast(new A0(42), typeof (int), out o));
		Assert.AreEqual(o.GetType(), typeof (int));

		Assert.IsTrue(TypeEx.Get<B>().TryCast(new B(42), typeof (int), out o));
		Assert.AreEqual(o.GetType(), typeof (int));

		Assert.IsTrue(TypeEx.Get<B>().TryCast(new B(42), typeof (A0), out o));
		Assert.AreEqual(o.GetType(), typeof (A0));

		Assert.IsTrue(TypeEx.Get<A0>().TryCast(new A0(42), typeof (B), out o));
		Assert.AreEqual(o.GetType(), typeof (B));
	}
}