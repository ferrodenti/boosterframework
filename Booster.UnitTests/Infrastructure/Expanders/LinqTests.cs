using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Infrastructure.Expanders;

[TestClass]
public class LinqTests
{
	bool _yieldInit;
	int _yieldReturn;

	IEnumerable<int> GetNumbers()
	{
		_yieldInit = true;
		for (var i = 0; i < 10; i++)
		{
			yield return i;
			_yieldReturn ++;
		}
	}

	[TestMethod]
	public void Now()
	{
		_yieldInit = false;
		_yieldReturn = 0;

		var ie = GetNumbers();

		Assert.IsFalse(_yieldInit);
		Assert.AreEqual(_yieldReturn, 0);
		ie = ie.Now();

		Assert.IsTrue(_yieldInit);
		Assert.AreEqual(_yieldReturn, 0);

		Utils.Nop(ie.ToArray());
		Assert.AreEqual(_yieldReturn, 10);
	}

	[TestMethod]
	public void Partition()
	{
		for (var test = 0; test < 10_000; ++test)
		{
			var size = Rnd.Int(0, 3000);
			var partSize = Rnd.Int(0, 1000);
			var expected = new int[size];
			for (var i = 0; i < size; ++i)
				expected[i] = Rnd.Int();

			var j = 0;
			var end = false;
			var partNo = 0;

			var actual = expected.Partition(partSize).SelectMany(ie => ie).ToArray();
			var actualPartSize = partSize > 0 ? partSize : expected.Length;
			TestUtils.AssertSequencesEqual(expected, actual);

			foreach (var part in expected.Partition(partSize).Select(ie => ie.ToArray()))
			{
				partNo++;
				Assert.IsFalse(end, "A non last part has invalid length");

				if (part.Length < actualPartSize)
					end = true;
				else if (part.Length > actualPartSize)
					Assert.Fail("Invalid part length");

				foreach (var item in part)
				{
					Assert.AreEqual(expected[j], item);
					j++;
				}
			}


			Utils.Nop(partNo);
			Assert.AreEqual(expected.Length, j);
		}
	}
}