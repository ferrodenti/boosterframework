﻿using System.Collections.Generic;
using System.Linq;
using Booster.Collections;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Infrastructure.Collections;

[TestClass]
public class BaseLazyIndexedCollectionTests
{
	const int _iters1 = 300;
	const int _iters2 = 1000;
		
	public class TestEntity
	{
		public readonly int Id;
		public readonly string Name;

		public TestEntity(int id, string name)
		{
			Id = id;
			Name = name;
		}

		public override bool Equals(object obj) 
			=> obj is TestEntity b && b.Id == Id && b.Name == Name;

		public override int GetHashCode() 
			=> Id + Name?.GetHashCode() ?? 0;

		public override string ToString() 
			=> $"{Id}:{Name}";
	}

	static readonly List<TestEntity> _entities = new()
	{
		new TestEntity(1, "alpha"),
		new TestEntity(2, "beta"),
		new TestEntity(3, "gamma"),
		new TestEntity(4, "delta"),
		new TestEntity(5, "epsilon"),
		new TestEntity(6, "zeta"),
		new TestEntity(7, "eta"),
		new TestEntity(8, "theta"),
		new TestEntity(9, "iota"),
		new TestEntity(10, "kappa"),
		new TestEntity(11, "lambda"),
		new TestEntity(12, "mu"),
		new TestEntity(13, "nu"),
		new TestEntity(14, "xi"),
		new TestEntity(15, "omicron"),
		new TestEntity(16, "pi"),
		new TestEntity(17, "rho"),
		new TestEntity(18, "sigma"),
		new TestEntity(19, "tau"),
		new TestEntity(20, "upsilon"),
		new TestEntity(21, "phi"),
		new TestEntity(22, "chi"),
		new TestEntity(23, "psi"),
		new TestEntity(24, "omega")
	};


	public class TestImpl : BaseLazyIndexedCollection<TestEntity, int>
	{
		public readonly StringEscaper StringEscaper = new("\\", ";") { IgnoreCase = false };
			
		public TestImpl(IEnumerable<int> keys) : base(keys)
		{
		}

		public TestImpl()
		{
		}

		public TestImpl(IEnumerable<TestEntity> items) : base(items)
		{
		}

		protected override int GetItemKey(TestEntity value) 
			=> value.Id;

		protected override IEnumerable<TestEntity> LoadItems(IEnumerable<int> keys)
		{
			var hash = new HashSet<int>(keys);
			return _entities.Where(e => hash.Contains(e.Id)).Shuffle().ToArray();
		}
			
		string _value;
		//[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public string Value
		{
			get
			{
				if (_value == null)
				{
					var records = Records;
					_value = records.Count > 0 ? $";{StringEscaper.Join(records.Select(k => StringConverter.Default.ToString(k.Key)), ";")};" : "";
				}
				return _value;
			}
			set
			{
				if (_value != value)
				{
					_value = value;
					ReloadRecords();
				}
			}
		}

		public override void Clear()
		{
			base.Clear();
			_value = "";
		}

		protected override void OnItemsChanged() 
			=> _value = null;

		protected override List<Record> InitRecords() 
			=> StringEscaper.Split(_value, ";", unescape: true).Select(k => new Record(StringConverter.Default.ToObject<int>(k))).ToList();
	}

	[TestMethod]
	public void BaseLazyIndexedCollectionRandomTest()
	{
		for (var k = 0; k < _iters1; ++k)
		{
			var unique = Rnd.Bool();
			var sorted = Rnd.Bool();
			var removeMissing = Rnd.Bool();
				
			List<TestEntity> expected;
			TestImpl actual;

			switch (Rnd.Int(3))
			{
			case 0:
				expected = new List<TestEntity>();
				actual = new TestImpl();
				break;
			case 1:
				expected = CreateList(unique, sorted);
				actual = new TestImpl(expected);
				break;
			default:
				expected = CreateList(unique, sorted);
				actual = new TestImpl(expected.Select(e => e.Id));
				break;
			}
				
			actual.Unique = unique;
			actual.Sorted = sorted;
			actual.RemoveMissing = removeMissing;
				
			for (var i = 0; i < _iters2; ++i)
			{
				var byId = Rnd.Bool();
				var el = Rnd.Element(_entities);
				var exp = false;
				var act =Rnd.Int(7);
					
				switch (act)
				{
				case 0:
					if (!unique || !expected.Contains(el))
					{
						if (sorted)
						{
							var j = 0;
							for (; j < expected.Count; ++j)
								if (expected[j].Id > el.Id)
									break;

							expected.Insert(j, el);
						}
						else
							expected.Add(el);

						exp = true;
					}

					Assert.AreEqual(exp, byId ? actual.Add(el.Id) : actual.Add(el));
						
					break;
				case 1:
					exp = expected.Remove(el);

					Assert.AreEqual(exp, byId ? actual.Remove(el.Id, false) : actual.Remove(el, false));
					break;

				case 2:
					while (expected.Remove(el))
						exp = true;
						
					Assert.AreEqual(exp, byId ? actual.Remove(el.Id) : actual.Remove(el));
						
					break;
				case 3:
					expected = CreateList(unique, sorted);
					actual.Keys = expected.Select(e => e.Id).ToArray();
					break;
				case 4:
					expected = CreateList(unique, sorted);
					actual.Set(expected);
					break;
				case 5:
					expected = CreateList(unique, sorted);
					actual.Value = Stringify(expected);
					break;
				case 6:
					expected.Clear();
					actual.Clear();
					break;
				}
					
				//if(!expected.SequenceEqual(actual))
				Assert.IsTrue(expected.SequenceEqual(actual));

				Assert.AreEqual(Stringify(expected), actual.Value);

			}
		}
	}

	[TestMethod]
	public void BaseLazyIndexedCollectionConcurrentTest() //TODO: BaseLazyIndexedCollectionConcurrentTest
	{
	}
		

	static List<TestEntity> CreateList(bool unique, bool sorted)
	{
		var cnt = Rnd.Int(0, _entities.Count);
		var ids = unique
			? Enumerable.Range(0, _entities.Count)
				.Shuffle()
				.Take(cnt)
			: Enumerable.Range(0, cnt)
				.Select(_ => Rnd.Int(0, _entities.Count));

		if (sorted)
			ids = ids.OrderBy(i => i);
				
		return ids.Select(i => _entities[i]).ToList();
	}

	static string Stringify(IEnumerable<TestEntity> expected) 
		=> expected.ToString(e => e.Id, new EnumBuilder(";", ";", ";"));
}