﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Infrastructure.Collections;

[TestClass]
public class PriorityQueueTests
{
	[TestMethod]
	public void PriorityQueueRandomTest()
	{
		var queue = new Booster.Collections.PriorityQueue<string, long>();
		var expect = new List<string>();

		for (var i = 0; i < 10000; ++i)
		{
			int idx;
			long min, max;

			do
			{
				idx = Rnd.Int(queue.Count + 1);
				min = long.MinValue;
				max = long.MaxValue;

				if (idx - 1 >= 0)
					min = queue.Priorities[idx - 1];

				if (idx < queue.Count)
					max = queue.Priorities[idx];

				min += 1;
				max -= 1;
			} while (min >= max);

			var str = Rnd.String(6);
			var val = Rnd.Long(min, max);

			Assert.IsTrue(val > min - 1);
			Assert.IsTrue(val < max + 1);

			expect.Insert(idx, str);
			queue.Enqueue(str, val);

			Assert.IsTrue(expect.SequenceEqual(queue));
		}
	}

}