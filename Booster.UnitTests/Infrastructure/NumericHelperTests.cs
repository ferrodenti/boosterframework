﻿using Booster.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Infrastructure;

[TestClass]
public class NumericHelperTests
{
	[TestMethod]
	public void SimpleSumTest()
	{
		const int a = 42;
		const long b = 420L;

		var c = NumericHelper.Sum(a, b, true);

		Assert.IsFalse(c.Is(out int _));
		Assert.IsTrue(c.Is(out long l));
		Assert.AreEqual(a + b, l);
	}
	
	[TestMethod]
	public void SumOverflowTest()
	{
		const sbyte a = 42;
		const byte b = (byte) sbyte.MaxValue + 1;

		var c = NumericHelper.Sum(a, b, true);

		Assert.IsTrue(c.Is(out int l));
		Assert.AreEqual(a + b, l);
	}
}