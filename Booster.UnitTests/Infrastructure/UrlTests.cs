﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Booster.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Infrastructure;

[TestClass]
public class UrlTests
{
	[TestMethod]
	public void BasicParsing()
	{
		var str = "http://login:pass@www.example.com:8080/test/test2/?a=3&b#hash";
		var url = new Url(str);
		url.Parse(url);
			
		var res = url.ToString();
		Assert.AreEqual(str, res);

		str = "C:/develop/khfds/sadsaf/dfgfgdf/";
		url = new Url(str);
		res = url.ToString();
		Assert.AreEqual(str, res);


		url.Host = "www.example.com";
		Assert.AreEqual("www.example.com/C:/develop/khfds/sadsaf/dfgfgdf/", url.ToString());
	}

	[TestMethod]
	public void Change()
	{
		Url url = "assortment/list";
		url.Query["test"] = "123";
		Assert.AreEqual("assortment/list?test=123", url.ToString());
	}

	[TestMethod]
	public void UrlEncodig()
	{
		TestEnc("Какой-то русский текст");
		TestEnc("SomeEnglishTextAllSafe");
		TestEnc("Some English Text Unsafe");
		TestEnc("一些中國文字");
		TestEnc("Смешанный \"mixed\" текст with symbols: ~!@#$%&&*()_+§¡™£¢∞§¶•ªº");


		Url u = $"?test={HttpUtility.UrlEncode("тест+3")}&{HttpUtility.UrlEncode("тест")}=test";

		Assert.AreEqual(u.Query["test"], "тест+3");
		Assert.AreEqual(u.Query["тест"], "test");
	}

	public void TestEnc(string str)
	{
		var enc = HttpUtility.UrlEncode(str);
		var act = Url.Encode(str);

		Assert.AreEqual(enc, act);

		var dec = Url.Decode(act);

		Assert.AreEqual(str, dec);
	}

	[TestMethod]
	public void DoubleSlashes()
	{
		var url = new Url("a.ru/a//b");
		Assert.AreEqual("a.ru/a//b", url.ToString());

		url = new Url("a.ru/a/b//");
		Assert.AreEqual("a.ru/a/b//", url.ToString());

		url = new Url("a.ru//a/b/");
		Assert.AreEqual("a.ru//a/b/", url.ToString());

		url = new Url("a.ru/a/b/c");
		Assert.AreEqual("a.ru/a/b/c", url.ToString());
			
		url = new Url("http://a/b/c");
		Assert.AreEqual("http://a/b/c", url.ToString());
			
		Assert.AreEqual("a", url.Host?.ToString()); //TODO: fix test
		Assert.AreEqual("b/c", url.Path?.ToString());
	}

	[TestMethod]
	public void RecognizeFilesAndDomains()
	{
		var url = new Url("test.txt?qwe");
		Assert.AreEqual("test.txt", url.Path.File);

		url = new Url("dkj3fhk?qwe");
		Assert.AreEqual("dkj3fhk", url.Path.File);

		url = new Url("page.asp?qwe");
		Assert.AreEqual("page.asp", url.Path.File);

		url = new Url("page.html");
		Assert.AreEqual("page.html", url.Path.File);

		url = new Url("page.asmx#dfffgd");
		Assert.AreEqual("page.asmx", url.Path.File);

		url = new Url("page.asp?qwe");
		Assert.AreEqual("page.asp", url.Path.File);

		url = new Url("page.ru");
		Assert.AreEqual("page.ru", url.Host.ToString());

		url = new Url("файл.рф?blablabla");
		Assert.AreEqual("файл.рф", url.Host.ToString());

		url = new Url("a.ru/test.txt?qwe");
		Assert.AreEqual("test.txt", url.Path.File);
	}

	[TestMethod]
	public void RandomParsing()
	{
		for (var k = 0; k < 10000; k++)
		{
			var bld = new StringBuilder();
				
			byte ip1 = 0, ip2 = 0, ip3 = 0, ip4 = 0;
			var port = 0;
			string protocol = null, login = null, pass = null, d1 = null, d2 = null, d3 = null, file = null, hash = null;

			var commands = new List<UrlCommand>();
			var path = new List<string>();
			var query = new Dictionary<string, string>();

			if (GetRndBool())
			{
				if (GetRndBool())
				{
					protocol = GetRndName();
					bld.Append($"{protocol}://");
				}
				if (GetRndBool(10))
				{
					login = GetRndName();
					pass = GetRndName();
					bld.Append($"{login}:{pass}@");
				}

				if (GetRndBool())
				{
					ip1 = Rnd.Byte(1, 255);
					ip2 = Rnd.Byte();
					ip3 = Rnd.Byte();
					ip4 = Rnd.Byte();
					bld.Append($"{ip1}.{ip2}.{ip3}.{ip4}");
				}
				else
				{
					if (GetRndBool())
					{
						d2 = "localhost";
						bld.Append(d2);
					}
					else
					{
						string[] domains = {"ru", "com", "travel", "org", "xxx", "edu", "at"};
						d1 = domains[Rnd.Int(domains.Length)];
						d2 = GetRndName();
						if (GetRndBool())
						{
							d3 = GetRndName();
							bld.Append($"{d3}.{d2}.{d1}");
						}
						else
							bld.Append($"{d2}.{d1}");
					}
				}
				if (GetRndBool())
				{
					port = Rnd.Int(1, 65535);
					bld.Append($":{port}");
				}
			}
			if (GetRndBool())
			{
				if (ip1 > 0 || d2 != null)
					bld.Append('/');
				else if (GetRndBool())
				{
					if (GetRndBool())
					{
						commands.Add(UrlCommand.AppRoot);
						bld.Append("~/");
					}
					else if (GetRndBool())
					{
						commands.Add(UrlCommand.SiteRoot);
						bld.Append('/');
					}
					else
						for (var i = 0; i < Rnd.Int(5) + 1; i++)
						{
							commands.Add(UrlCommand.Up);
							bld.Append("../");
						}
				}

				for (var i = 0; i < Rnd.Int(5) + 1; i++)
					if (i > 0 && GetRndBool(5))
					{
						bld.Append('/');
						path.Add("");
					}
					else
					{
						var s = GetRndName();
						path.Add(s);
						bld.Append($"{s}/");
					}

				if (GetRndBool())
				{
					file = GetRndName();
					if (GetRndBool())
						file = file + "." + GetRndName();
					path.Add(file);
					bld.Append(file);
				}
			}
			if (GetRndBool())
			{
				if (path.Count == 0 && bld.Length > 0 && bld[bld.Length - 1] != '/')
					bld.Append('/');

				bld.Append('?');
				for (var i = 0; i < Rnd.Int(5) + 1; i++)
				{
						
					var key = GetRndName();

					while (query.ContainsKey(key))
						key = GetRndName();

					var v = GetRndBool() ? GetRndName() : null;

					query.Add(key, v);

					if (i > 0)
						bld.Append('&');

					if (v != null)
						bld.Append($"{key}={v}");
					else
						bld.Append(key);
				}
			}
			if (GetRndBool())
			{
				hash = GetRndName();
				bld.Append($"#{hash}");
			}
			var ulrStr = bld.ToString();
			var url = new Url(ulrStr);
			Assert.AreEqual(ulrStr, url.ToString());

			if (url.Host is UrlIp4 ip)
			{
				Assert.AreEqual(ip1, ip[0]);
				Assert.AreEqual(ip2, ip[1]);
				Assert.AreEqual(ip3, ip[2]);
				Assert.AreEqual(ip4, ip[3]);
			}

			if (url.Host is UrlDomain domain)
			{
				Assert.AreEqual(d3, domain[0]);
				Assert.AreEqual(d2, domain[1]);
				Assert.AreEqual(d1, domain[2]);
			}
				
			Assert.AreEqual(protocol, url.Protocol);
			Assert.AreEqual(port, url.Port ?? 0);
			Assert.AreEqual(login, url.Login);
			Assert.AreEqual(pass, url.Password);
			Assert.AreEqual(hash, url.Hash);
			Assert.AreEqual(file, url.Path?.File);

			Assert.IsTrue(path.SequenceEqual((IEnumerable<string>)url.Path ?? new List<string>()));
			Assert.IsTrue(query.SequenceEqual((IDictionary<string,string>)url.Query ?? new Dictionary<string, string>()));
			Assert.IsTrue(commands.SequenceEqual((IEnumerable<UrlCommand>)url.Commands ?? new List<UrlCommand>()));

			var u2 = url.Clone();
			url.Host = new UrlDomain("www.example.ru", null);
			url.Hash = "sdfsdf";
			Assert.AreEqual(ulrStr, u2.ToString());
		}
	}

	[TestMethod]
	public void Combine()
	{
		Assert.AreEqual("http://example.com/a/b/c", new Url("http://example.com").Combine(new Url("a/b/c")).ToString());
		Assert.AreEqual("http://example.com/?blabla", new Url("http://example.com").Combine(new Url("?blabla")).ToString());
		Assert.AreEqual("http://example.com/#blabla", new Url("http://example.com").Combine(new Url("#blabla")).ToString());
		Assert.AreEqual("http://example.com/?q=1#blabla", new Url("http://example.com?q=1").Combine(new Url("#blabla")).ToString());
		Assert.AreEqual("http://example.com/?q=2#blabla", new Url("http://example.com?q=1").Combine(new Url("?q=2#blabla")).ToString());


		Assert.AreEqual("a/b/c/d/e", new Url("a/b/c/").Combine(new Url("d/e")).ToString());
		Assert.AreEqual("/d/e", new Url("a/b/c/").Combine(new Url("/d/e")).ToString());
		Assert.AreEqual("~/b", new Url("a/b/c/").Combine(new Url("~/b")).ToString());
		Assert.AreEqual("../b/", new Url("../b/c/").Combine(new Url("../")).ToString());
		Assert.AreEqual("../../b", new Url("../").Combine(new Url("../b")).ToString());
		Assert.AreEqual("~/", new Url("~/b/").Combine(new Url("../")).ToString());
		Assert.AreEqual("~/../", new Url("~/b/").Combine(new Url("../../")).ToString());
	}

	protected bool GetRndBool(int seed = 2)
		=> Rnd.Int()%seed == 0;

	protected string GetRndName()
	{
		string[] words =
		{
			"www", "ftp", "http", "hello", "world", "example", "a", "b", "c", "d", "e", "f", "com", "ru", "org", "1", "2", "3", "4", "5", "666", "777", "333", "mondial", "mondialbc", "europetravel", "test",
			"doom", "blablabla", "Exception", "date", "fire", "int", "integer", "rnd", "unique", "hash", "light", "jquery", "mvc", "mbc", "abc-def", "qwerty",
			"123456", "admin", "password", "test", "devry", "111111", "1234567", "KRAZY", "123123", "windows", "abc123", "helpme", "admin123", "000000", "123qwe", "administrator", "azerty", "hello", "cisco12",
			"pass", "root", "iloveyou", "1q2w3e4r", "chocolat", "Networkp4ss", "Forward", "domain", "host", "localhost1"
		};
		return GetRndBool(10) ? Base36.Convert(Rnd.Int(), 8, true) : Rnd.Element(words);
	}
}