﻿using System;
using System.Threading;
using Booster.Console;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Infrastructure;

[TestClass]
public class TimeScheduleTests
{
	[TestMethod]
	public void TimeScheduleRandomTest()
	{
		var unexpectedFires = false;
		for (var j = 0; j < 7; ++j)
		{
			using var schedule = new TimeSchedule();
			var j1 = j;
			var num = Rnd.Int(6, 10);
			var subscriptions = new TimeSchedule.Subscription[num];
			var cnts = new int[num];
			var starts = new DateTime[num];
			var intervals = new int[num];

			for (var i = 0; i < num; ++i)
				intervals[i] = Rnd.Int(100, 200);

			for (var i = 0; i < num; ++i)
			{
				var i1 = i;
				starts[i] = DateTime.Now;
				intervals[i] = Rnd.Int(100, 200);
				var interval = new TimeSpan(0, 0, 0, 0, intervals[i]);
				var initStart = Rnd.Bool();
				ConsoleHelper.WriteLine($"Task #{j1}.{i1} interval: {interval:s\\:fff}, init: {(initStart?"start":"post")}");
				var sub = initStart ? schedule.Subscribe(interval) : schedule.Subscribe(this);
				sub.CallbackEvent += (_, _) =>
				{
					Interlocked.Increment(ref cnts[i1]);
					ConsoleHelper.WriteLine($"Task fired: #{j1}.{i1} at {DateTime.Now - starts[i1]:s\\:fff}");
				};
				sub.Error += (_, e) => ConsoleHelper.WriteLine($"EXCEPTION: {e.GetException().Message}");
						
				subscriptions[i] = sub;

				if (!initStart)
					sub.Interval = interval;
			}

			const int wait = 1000;
			Thread.Sleep(wait);

			var stopSchedule = Rnd.Bool();
			if (stopSchedule)
			{
				schedule.Stop();
				ConsoleHelper.WriteLine($"Stopping schedule timer {DateTime.Now:s\\:fff}");
			}
			else
				ConsoleHelper.WriteLine($"Stopping subscriptions {DateTime.Now:s\\:fff}");
						

			for (var i = 0; i < num; ++i)
			{
				if (!stopSchedule)
					subscriptions[i].Stop();

				var i1 = i;
				subscriptions[i].CallbackEvent += (_, _) =>
				{
					ConsoleHelper.WriteLine($"UNEXPECTED FIRE OF #{j1}.{i1}");
					unexpectedFires = true;
				};
				var expected = (int) Math.Floor((double) (wait - 1) / intervals[i]);
				var actual = cnts[i];
				ConsoleHelper.WriteLine(actual == expected 
					? $"Task #{j1}.{i} was fired {actual} times, OK!" 
					: $"Task #{j1}.{i} was fired {actual} times, must be {expected}");

				Assert.IsTrue(actual <= expected + 1);
				Assert.IsTrue(actual > expected * 3 / 4); //TODO: fix test
			}
		}

		Assert.IsFalse(unexpectedFires);
	}
}