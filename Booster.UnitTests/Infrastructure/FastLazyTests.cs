using Microsoft.VisualStudio.TestTools.UnitTesting;
using static Booster.FastLazyStatic;

#nullable enable

namespace Booster.UnitTests.Infrastructure;

[TestClass]
public class FastLazyTests
{
	int _counter;

	int Inc()
		=> _counter++;
	
	FastLazy<int>? _lazy1;
	public int Lazy1 => _lazy1 ??= Inc();
	
	FastLazy<int>? _lazy2;
	public int Lazy2 => _lazy2 ??= FastLazy(Inc);
	
	[TestMethod]
	public void GeneralBehaviour()
	{
		var actual = Lazy1;
		Assert.AreEqual(0, actual);
		
		actual = Lazy1;
		Assert.AreEqual(0, actual);
		
		_lazy1 = null;
		actual = Lazy1;
		Assert.AreEqual(1, actual);
		
		actual = Lazy1;
		Assert.AreEqual(1, actual);
		
		actual = Lazy2;
		Assert.AreEqual(2, actual);
		
		actual = Lazy2;
		Assert.AreEqual(2, actual);

		_lazy2 = null;
		actual = Lazy2;
		Assert.AreEqual(3, actual);
		
		actual = Lazy1;
		Assert.AreEqual(1, actual);
		
		Assert.AreEqual(4, _counter);
	}
}
