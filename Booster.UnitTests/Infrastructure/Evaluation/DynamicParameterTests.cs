using Booster.Evaluation;
using JetBrains.Annotations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestNamespace1;

#region TestClassHierarcy

// ReSharper disable CheckNamespace
namespace TestNamespace1
{
	[PublicAPI]
	public enum TestEnum
	{
		Foo = 42
	}

	namespace TestNamespace2
	{
		[PublicAPI]
		public class Test1
		{
			public static string Prop => "2Test1PropRes";
			public static string Field = "2Test1FieldRes";
			public static bool Bool;
			public static bool Bool2 = true;
			public static int? IntN;
			public static Test1 Inst => new();

			public static string Mtd(bool val2, bool val3, bool val4 = true)
				=> val2 & val3 & val4 ? "val2=true" : "val2=false";
		}

		public static class Test3
		{
			public static string Prop => "1Test3Prop";
		}

		public static class Test1Expander
		{
			public static string Test(this Test1 o, int foo, int k, string str = "", int ?i = 0, int j = 1)
				=> " ExpanderRes" + str + i;
		}
	}

	[PublicAPI]
	public class Test1
	{
		public static class Sub
		{
			public static string Prop => "1TestSubProp";
		}

		public string Prop => "1Test1PropRes";

		public string Mtd(bool val)
			=> val ? "val=true" : "val=false";

		public static string Mtd2(bool val)
			=> val ? "val=true" : "val=false";
	}
}
#endregion


namespace Booster.UnitTests.Evaluation
{
	[TestClass]
	public class DynamicParameterTests
	{
#pragma warning disable 414
		[UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
		class TestModel
		{
			public string TestField1;
			public int TestField2;
			public string TestProperty { get; set; }
			public bool A { get; set; }
			public string _;
		}
#pragma warning restore 414

		[TestMethod]
		public void GeneralBehaviour()
		{
			var m = new TestModel
			{
				TestField1 = "qwe",
				TestField2 = 0,
				TestProperty = "123",
				A = true,
				_ = null
			};

			var param = new DynamicParameter(typeof(TestModel), "{TestProperty.Length}");
			Assert.AreEqual(3, param.Evaluate(m));

			param = new DynamicParameter(typeof(TestModel), "{{This {TestField1}is{TestField2}{A} {TestProperty.Length} {_} {");
			Assert.AreEqual("{This qweis0True 3  {", param.Evaluate(m));

			param = new DynamicParameter(typeof(TestModel), "{TestField1}");
			Assert.AreEqual("qwe", param.Evaluate(m));

			param = new DynamicParameter(typeof(TestModel), "{{test}}");
			Assert.AreEqual("{test}", param.Evaluate(m));

			param = new DynamicParameter(typeof(TestModel), "{A}");
			Assert.AreEqual(true, param.Evaluate(m));

			param = new DynamicParameter(typeof(TestModel), "");
			Assert.AreEqual("", param.Evaluate(m));

			param = new DynamicParameter(typeof(TestModel), " test ");
			Assert.AreEqual(" test ", param.Evaluate(m));
			param = new DynamicParameter(typeof(TestModel), "{ ");
			Assert.AreEqual("{ ", param.Evaluate(m));

			param = new DynamicParameter(typeof(TestModel), "{}");
			Assert.AreEqual("{}", param.Evaluate(m));

			param = new DynamicParameter(typeof(TestModel), null);
			Assert.AreEqual(null, param.Evaluate(m));

			var n = new Test1();
		
			param = new DynamicParameter(typeof (Test1), "{Prop} qwe");
			Assert.AreEqual("1Test1PropRes qwe", param.Evaluate(n));

			param = new DynamicParameter(typeof (Test1), "{Mtd(true)}");
			Assert.AreEqual("val=true", param.Evaluate(n));

			param = new DynamicParameter(typeof (Test1), "{Mtd2(true)}");
			Assert.AreEqual("val=true", param.Evaluate(n));

			param = new DynamicParameter(typeof (Test1), "{Sub.Prop}");
			Assert.AreEqual("1TestSubProp", param.Evaluate(n));

			param = new DynamicParameter(typeof (Test1), "{TestNamespace2.Test1.Prop}");
			Assert.AreEqual("2Test1PropRes", param.Evaluate(n));

			param = new DynamicParameter(typeof (Test1), "{TestNamespace2.Test1.Field}");
			Assert.AreEqual("2Test1FieldRes", param.Evaluate(n));

			param = new DynamicParameter(typeof (Test1), "{TestNamespace2.Test1.Mtd(TestNamespace2.Test1.Bool, TestNamespace2.Test1.Bool2)}");
			Assert.AreEqual("val2=false", param.Evaluate(n));

			param = new DynamicParameter(typeof (Test1), "{TestNamespace2.Test1.Inst.Test(TestEnum.Foo, 'qwe'.Length.GetHashCode(), \"te\\\"st\",12.3, TestNamespace2.Test1.IntN).Trim()}");
			Assert.AreEqual("ExpanderReste\"st12", param.Evaluate(n));

		}
	}
}