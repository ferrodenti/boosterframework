﻿using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SR = Booster.Hacks.System.SR;

namespace Booster.UnitTests.Infrastructure;

[TestClass]
public class HacksSrTests
{
	[TestMethod]
	public void CanExtractMessages()
	{
		TestString(SR.Argument_EmptyPath);
		TestString(SR.ArgumentNull_Path);
		TestString(SR.IO_EOF_ReadBeyondEOF);
		TestString(SR.IO_FileTooLong2GB);
		TestString(SR.Lazy_Value_RecursiveCallsToValue);
		TestString(SR.Lazy_ToString_ValueNotCreated);
		TestString(SR.Lazy_ctor_ModeInvalid);
		TestString(SR.Lazy_CreateValue_NoParameterlessCtorForT);
	}

	static void TestString(string message)
	{
		Trace.WriteLine(message);
		Assert.IsNotNull(message);		
	}
}