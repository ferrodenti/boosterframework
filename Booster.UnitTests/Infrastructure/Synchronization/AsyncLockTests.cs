using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Booster.Synchronization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
using JetBrains.Annotations;

namespace Booster.UnitTests.Infrastructure.Synchronization;

[TestClass]
public class AsyncLockTests
{
	readonly AsyncLock _lock = new();

	readonly AssertFlags<int> _results = new();

#pragma warning disable IDE0044 // Add readonly modifier
#pragma warning disable CS0649
	[UsedImplicitly] volatile int _expect;
#pragma warning restore IDE0044 // Add readonly modifier

	[TestMethod]
	public async Task AsyncLockTest1()
	{
		//Exception exception = null;

		await Task.WhenAll(Enumerable.Range(0, 10).Select(i => Utils.StartTaskNoFlow(() => DelaysAndLocksAsync(i))));

		//if (exception != null)
		//	throw exception;

		Assert.AreEqual(0, _results.Count);
	}

	[TestMethod]
	public void AsyncLockTest2()
		=> AsyncLockTest1().NoCtxWait();

	[TestMethod]
	public void AsyncLockTest3()
	{
		//Exception exception = null;

		Task.WhenAll(Enumerable.Range(0, 10).Select(i => Utils.StartTaskNoFlow(() => DelaysAndLocks(i)))).NoCtxWait();

		//if (exception != null)
		//	throw exception;

		Assert.AreEqual(0, _results.Count);
	}

	async Task DelaysAndLocksAsync(int i)
	{
		_results[i] = true;

		using (await _lock.LockAsync())
		{
			var local = AssertThreadImmutable.From(this, t => t._expect);
			local.Value = i;
			Trace.WriteLine($"{i}: Enter lock 1");
			await Task.Delay(10);
			using (await _lock.LockAsync())
			{
				local.AssertIsImmutable();
				Trace.WriteLine($"{i}: Enter lock 2");
				await Task.Delay(10);
				local.AssertIsImmutable();
				Trace.WriteLine($"{i}: Exit lock 2");
			}

			local.AssertIsImmutable();
			Trace.WriteLine($"{i}: Exit lock 1");

			_results[i] = false;
		}
	}

	void DelaysAndLocks(int i)
	{
		_results[i] = true;

		using (_lock.Lock())
		{
			var local = AssertThreadImmutable.From(this, t => t._expect);
			Trace.WriteLine($"{i}: Enter lock 1");
			Thread.Sleep(10);
			using (_lock.Lock())
			{
				local.Value = i;
				local.AssertIsImmutable();
				Trace.WriteLine($"{i}: Enter lock 2");
				Thread.Sleep(10);
				local.AssertIsImmutable();
				Trace.WriteLine($"{i}: Exit lock 2");
			}

			local.AssertIsImmutable();
			Trace.WriteLine($"{i}: Exit lock 1");

			_results[i] = false;
		}
	}
}