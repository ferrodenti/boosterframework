using System.Collections.Generic;
using System.Linq;
using Booster.Parsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Infrastructure.Parsing;

[TestClass]
public class StringParserTests
{
	[TestMethod]
	public void StringParserEscapingTests() //TODO: add random test
	{ 
		//         s    /s 2      3  -  \3\2 -  s  - -  - \s\1
		var exp = "\"qwe\" {value {1 {{2 } } }} \" \\\" } \"";
		var str = "This is a {{test}} {" + exp + "} asd";
		//                   s     s  1          \1

		var tokens = new List<string>();
		var parser = new StringParser(str);
        var start = parser;
        var nesting = 0;

		parser.SearchFor(
			new Term("{{", true), 
			new Term("}}", true),
			new Term("\"", "\'").HandleQuotes(_ => { }),
			new Term("{", s =>
			{
				if (nesting++ == 0)
					start = s.Parser.Clone();
            }),
			new Term("}", s =>
			{
				if (--nesting == 0)
				{
                    var token = s.Parser - 1 - start;
					tokens.Add(token);
                }
            }));
			
		Assert.AreEqual(0, nesting);
		Assert.IsTrue(tokens.SequenceEqual(new[] {exp}));
	}
}