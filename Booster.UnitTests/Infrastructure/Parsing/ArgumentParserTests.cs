using System.Text.RegularExpressions;
using Booster.Helpers;
using Booster.Parsing;
using JetBrains.Annotations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

#pragma warning disable 0649

namespace Booster.UnitTests.Infrastructure.Parsing;

[TestClass, PublicAPI]
public class ArgumentParserTests
{
	const string _appName = "test.exe";

	[CommandArgumentsDescription(ExecutableName = _appName, Description = "calculate X to the power of Y")]
	class Arguments
	{
		[OptionalCommandArgument("-v --verbose", "increase output verbosity", Group = 1)]
		public bool Verbose;

		[OptionalCommandArgument("-q --quiet", "decrease output verbosity", Group = 1)]
		public bool Quiet;

		[PositionalCommandArgument("the base")]
		public int X;

		[PositionalCommandArgument("the exponent", "Y")]
		public int Y = 42;

		public string A;
		public string B;

		[OptionalCommandArgument("--test", name: "A B")]
		void Setter(string a, string b)
		{
			A = a;
			B = b;
		}
	}

	[TestMethod]
	public void SyntheticModel()
	{
		Assert.IsTrue(ArgumentParser.TryParse<Arguments>("4 -v", out var result, out var message, _appName));
		Assert.IsTrue(Utils.IsNull(message));
		Assert.AreEqual(4, result.X);
		Assert.AreEqual(42, result.Y);
		Assert.AreEqual(true, result.Verbose);
		Assert.AreEqual(false, result.Quiet);
			
		Assert.IsTrue(ArgumentParser.TryParse("4 --test qwer \"dsfsdf sdf\"", out result, out message, _appName));
		Assert.AreEqual("qwer", result.A);
		Assert.AreEqual("dsfsdf sdf", result.B);
			
		Assert.IsFalse(ArgumentParser.TryParse("4 2 -vq", out result, out message));
		Assert.IsTrue(Utils.IsNull(result));

		message = _usage.Replace(message, "");
		Assert.AreEqual($"{_appName}: error: argument -q/--quiet: not allowed with argument -v/--verbose", message);


		Assert.IsFalse(ArgumentParser.TryParse("-h", out result, out message));
		Assert.IsTrue(Utils.IsNull(result));

		const string help = $@"Usage: {_appName} [-h] [-v | -q] [--test A B] x [Y]

calculate X to the power of Y

Positional arguments:
  x                     the base
  Y                     the exponent (default: 42)

Optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         increase output verbosity
  -q, --quiet           decrease output verbosity
  --test A B";

		StringCompareHelper.Instance.AssertAreEqual(help, message);
	}

	[TestMethod]
	public void Synthetic()
	{
		var parser = new ArgumentParser(_appName);
		parser.AddPositionalArgument("echo");

		TestError(parser, "", "the following arguments are required: echo");
		Test(parser, "foo", new {echo = "foo"});
		const string usage = @"[-h] echo

Positional arguments:
  echo

Optional arguments:
  -h, --help            show this help message and exit";
		TestHelp(parser, usage);
		TestHelp(parser, usage, "--help");

		parser = new ArgumentParser(_appName);
		parser.AddPositionalArgument("echo", "echo the string you use here");
		TestHelp(parser, @"[-h] echo

Positional arguments:
  echo                  echo the string you use here

Optional arguments:
  -h, --help            show this help message and exit");

		parser = new ArgumentParser(_appName);
		parser.AddPositionalArgument<int>("square", "display a square of a given number");

		Test(parser, "4", new {square = 4});
		TestError(parser, "four", "argument square: invalid int value: 'four'");
		TestError(parser, "", "the following arguments are required: square");

		parser = new ArgumentParser(_appName);
		parser.AddOptionalArgument("--verbosity", "increase output verbosity");
		Test(parser, "--verbosity 1", new {verbosity = "1"});
		Test(parser, "", new { });
		TestHelp(parser, @"[-h] [--verbosity VERBOSITY]

Optional arguments:
  -h, --help            show this help message and exit
  --verbosity VERBOSITY increase output verbosity");
		TestError(parser, "--verbosity", "argument --verbosity: expected 1 argument");

		parser = new ArgumentParser(_appName);
		parser.AddOptionalArgument<bool>("--verbose", "increase output verbosity");
		Test(parser, "--verbose", new {verbose = true});
		TestError(parser, "--verbose 1", "unrecognized arguments: 1");

		TestHelp(parser, @"[-h] [--verbose]

Optional arguments:
  -h, --help            show this help message and exit
  --verbose             increase output verbosity");

		parser = new ArgumentParser(_appName);
		parser.AddOptionalArgument<bool>("-v --verbose", "increase output verbosity");

		Test(parser, "-v", new {verbose = true});
		TestHelp(parser, @"[-h] [-v]

Optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         increase output verbosity");

		parser = new ArgumentParser(_appName);
		parser.AddPositionalArgument<int>("square", "display a square of a given number");
		parser.AddOptionalArgument<bool>("-v --verbose", "increase output verbosity");

		TestError(parser, "", "the following arguments are required: square");
		Test(parser, "4", new {square = 4});
		Test(parser, "4 --verbose", new {square = 4, verbose = true});
		Test(parser, "--verbose 4", new {square = 4, verbose = true});

		parser = new ArgumentParser(_appName);
		parser.AddPositionalArgument<int>("square", "display a square of a given number");
		parser.AddOptionalArgument("-v --verbosity", "increase output verbosity", choices: new[] {0, 1, 2});

		TestHelp(parser, @"[-h] [-v {0,1,2}] square

Positional arguments:
  square                display a square of a given number

Optional arguments:
  -h, --help            show this help message and exit
  -v, --verbosity {0,1,2} 
						increase output verbosity");

		parser = new ArgumentParser(_appName);
		parser.AddPositionalArgument<int>("square", "display a square of a given number");
		parser.AddOptionalArgument("-v --verbosity", "increase output verbosity", countKeys: true);

		Test(parser, "4", new {square = 4});
		Test(parser, "4 -v", new {square = 4, verbosity = 1});
		Test(parser, "4 -vv", new {square = 4, verbosity = 2});
		Test(parser, "4 -vvv", new {square = 4, verbosity = 3});
		Test(parser, "4 -v -v", new {square = 4, verbosity = 2});
		Test(parser, "4 --verbosity --verbosity", new {square = 4, verbosity = 2});
		TestError(parser, "4 -v 1", "unrecognized arguments: 1");

		TestHelp(parser, @"[-h] [-v] square

Positional arguments:
  square                display a square of a given number

Optional arguments:
  -h, --help            show this help message and exit
  -v, --verbosity       increase output verbosity");

		parser = new ArgumentParser(_appName);
		parser.AddPositionalArgument<int>("x", "the base");
		parser.AddPositionalArgument<int>("y", "the exponent");
		parser.AddOptionalArgument("-v --verbosity", "increase output verbosity", countKeys: true);

		TestError(parser, "", "the following arguments are required: x, y");
		Test(parser, "4 2", new {x = 4, y = 2});
		Test(parser, "4 2 -v", new {x = 4, y = 2, verbosity = 1});
		Test(parser, "-vv 4 2 -v", new {x = 4, y = 2, verbosity = 3});

		TestHelp(parser, @"[-h] [-v] x y

Positional arguments:
  x                     the base
  y                     the exponent

Optional arguments:
  -h, --help            show this help message and exit
  -v, --verbosity       increase output verbosity");

		parser = new ArgumentParser(_appName, "calculate X to the power of Y");
		var group = parser.AddMutuallyExclusiveGroup();
		group.AddOptionalArgument<bool>("-v --verbose");
		group.AddOptionalArgument<bool>("-q --quiet");
		parser.AddPositionalArgument<int>("x", "the base");
		parser.AddPositionalArgument<int>("y", "the exponent");

		Test(parser, "4 2", new {x = 4, y = 2});
		Test(parser, "4 2 -q", new {x = 4, y = 2, quiet = true});
		Test(parser, "4 2 -v", new {x = 4, y = 2, verbose = true});

		TestError(parser, "4 2 -vq", "argument -q/--quiet: not allowed with argument -v/--verbose");
		TestError(parser, "4 2 -v --quiet", "argument -q/--quiet: not allowed with argument -v/--verbose");

		TestHelp(parser, @"[-h] [-v | -q] x y

calculate X to the power of Y

Positional arguments:
  x                     the base
  y                     the exponent

Optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose
  -q, --quiet");
	}



	static readonly Regex _usage = new("Usage:.*$\n", RegexOptions.Compiled | RegexOptions.Multiline);

	protected static void TestError(ArgumentParser parser, string args, string errorMessage)
	{
		Assert.IsFalse(parser.TryParse(args, out _, out var message));

		message = _usage.Replace(message, "");
		Assert.AreEqual($"{_appName}: error: {errorMessage}", message);
	}

	protected static void TestHelp(ArgumentParser parser, string help, string args = "-h")
	{
		Assert.IsFalse(parser.TryParse(args, out _, out var message));

		help = $"Usage: {_appName} {help}";

		StringCompareHelper.Instance.AssertAreEqual(help, message);
	}

	protected static void Test(ArgumentParser parser, string args, object expectedValues)
	{
		Assert.IsTrue(parser.TryParse(args, out var values, out var message));
		Assert.IsTrue(Utils.IsNull(message));

		var dict = AnonTypeHelper.ReadProperties(expectedValues);

		Assert.AreEqual(dict.Count, values.Count);

		foreach (var pair in dict)
		{
			Assert.IsTrue(values.TryGetValue(pair.Key, out var actual), $"Expected {pair.Key} parameter");
			Assert.AreEqual(pair.Value, actual);
		}
	}
}