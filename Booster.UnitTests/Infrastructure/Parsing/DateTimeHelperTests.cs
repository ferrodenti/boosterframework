using System;
using System.Linq;
using System.Globalization;
using Booster.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Infrastructure.Parsing;

[TestClass]
public class DateTimeHelperTests
{
	[TestMethod]
	public void Tests()
    {
        var helper = DateTimeHelper.Instance;

		for (var i = 0; i < 100000; i++)
		{
			var expectedDateTime = new DateTime(Rnd.Int(1800, 2150), Rnd.Int(1, 12), 1).AddDays(Rnd.Int(30));
			var time = Rnd.Bool();
            var secs = Rnd.Bool();
            var ms = Rnd.Bool();
			if (time)
			{
				expectedDateTime = expectedDateTime.AddMinutes(Rnd.Int(24*60));
                if (secs)
                    expectedDateTime = expectedDateTime.AddSeconds(Rnd.Int(60));
                if (ms)
                    expectedDateTime = expectedDateTime.AddMilliseconds(Rnd.Int(999));
			}

            var yy = Rnd.Bool() && helper.ToShortYear(expectedDateTime.Year) != expectedDateTime.Year ? "yy" : "yyyy";
			var m = Rnd.Bool() && expectedDateTime.Month < 10 ? "M" : "MM";
			var d = Rnd.Bool() && expectedDateTime.Day < 10 ? "d" : "dd";
			var delim = Rnd.Bool() ? "." : Rnd.Bool() ? "/" : "-";

            var dateParts = new[] {d, m, yy};

            if (Rnd.Bool() && expectedDateTime.Day > 23 && (yy == "yyyy" || expectedDateTime.Year % 100 > 31))
                dateParts = dateParts.Shuffle(Rnd.Local).ToArray();

            var expectedFormat = dateParts.ToString(new EnumBuilder(delim));

            if (time)
            {
                var h = Rnd.Bool() && expectedDateTime.Hour < 10 ? "H" : "HH";
                var min = Rnd.Bool() && expectedDateTime.Minute < 10 ? "m" : "mm";
                var s = Rnd.Bool() && expectedDateTime.Second < 10 ? "s" : "ss";

                expectedFormat += " " + h + ":" + min;

                if (secs)
                    expectedFormat += ":" + s;

                if (ms)
                    expectedFormat += ".fff";
            }

            var str = expectedDateTime.ToString(expectedFormat, CultureInfo.InvariantCulture);

			Assert.IsTrue(helper.TryParseDateTimeFormat(str, out var actualDateTime, out var actualFormat));
			Assert.AreEqual(expectedFormat, actualFormat);
			Assert.AreEqual(expectedDateTime, actualDateTime);
        }
	}
}