﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Booster.FileSystem;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Infrastructure.FileSystem;

[TestClass]
public class FileWatcherTests
{
	long _noEvents;
	static FilePath _path;

	const int _writeDelay = 5;
	const int _wait1 = 50;
	const int _wait2 = 310;
	readonly TimeSpan _eventDelay = TimeSpan.FromMilliseconds(300);
	readonly TimeSpan _watchInterval = TimeSpan.FromMilliseconds(50);

	[ClassInitialize]
	public static void Init(TestContext context)
	{
		_path = Path.Combine(Path.GetTempPath(), "FileWatcherTests");
		_path += "/";
		if (!Directory.Exists(_path))
			Directory.CreateDirectory(_path);
	}

	[ClassCleanup]
	public static void CleanUp()
	{
		Directory.Delete(_path, true);
	}

	[TestMethod]
	public void SystemFileWatcher()
	{
		using var fw = new FileWatcher(_path + "*.test") { EventDelay = _eventDelay, WatchInterval = TimeSpan.Zero, JoinEvents = true };
		DoTest(fw);
	}
	[TestMethod]
	public void TimerFileWatcher()
	{
		using var fw = new FileWatcher(_path + "*.*", false) { EventDelay = _eventDelay, WatchInterval = _watchInterval, UseFileSystemWatcher = false, JoinEvents = true };
		DoTest(fw);
	}
	[TestMethod]
	public void MixedFileWatcher()
	{
		using var fw = new FileWatcher(_path) { EventDelay = _eventDelay, WatchInterval = _watchInterval, JoinEvents = true };
		DoTest(fw);
	}

	[TestMethod]
	public void SystemFileWatcherNoJoin()
	{
		using var fw = new FileWatcher(_path + "*.test", false) { EventDelay = _eventDelay, WatchInterval = TimeSpan.Zero, JoinEvents = false };
		DoTest(fw);
	}
	[TestMethod]
	public void TimerFileWatcherNoJoin()
	{
		using var fw = new FileWatcher(_path + "*.*") { EventDelay = _eventDelay, WatchInterval = _watchInterval, UseFileSystemWatcher = false, JoinEvents = false };
		DoTest(fw);
	}
	[TestMethod]
	public void MixedFileWatcherNoJoin()
	{
		using var fw = new FileWatcher(_path, false) { EventDelay = _eventDelay, WatchInterval = _watchInterval, JoinEvents = false };
		DoTest(fw);
	}

	public void DoTest(FileWatcher fileWatcher)
	{
		const int noOfFiles = 10;
		_noEvents = 0;
		fileWatcher.Event += Handler;
		fileWatcher.Start();

		Trace.WriteLine("creating...");
		var ints = new List<int>();
		for (var i = 0; i < noOfFiles; i++)
		{
			ints.Add(i);
			WriteTestFile(i);
		}

		var exp = 10;
#if !NETCOREAPP //TODO: почему-то тут не работает под маком
//			var testDir = _path + "TestDir";
//			if (!Directory.Exists(testDir))
//			{
//				Directory.CreateDirectory(testDir);
//				exp++;
//			}
		Directory.CreateDirectory(_path + "TestDir");
#endif
		WaitFor(exp);

		Trace.WriteLine("modifying...");
		for (var i = 0; i < 100; i++)
		{
			var j = Rnd.Int(noOfFiles);
			ints.Remove(j);
			WriteTestFile(j);
		}

		Trace.WriteLine($"rest : {ints.Count}");

		foreach(var i in ints)
			WriteTestFile(i);

		WaitFor(20);

		for (var i = 0; i < noOfFiles; i++)
			FileUtils.DeleteFile($"{_path}test{i}.test");

		WaitFor(30);
	}

	void WaitFor(int i)
	{
		while (Interlocked.Read(ref _noEvents) < i)
			Thread.Sleep(_wait1);

		Thread.Sleep(_wait2);

		Assert.AreEqual(i, _noEvents);
	}

	static void WriteTestFile(int i)
	{
		File.WriteAllText(_path + $"test{i}.test", TestUtils.RandomString(10));

		if (Rnd.Bool(5))
			Thread.Sleep(_writeDelay);
	}

	public void Handler(object sender, FileWatcherEventArgs ea)
	{
		foreach (var file in ea.Files)
			Trace.WriteLine(file.FullPath + " changed");

		Interlocked.Add(ref _noEvents, ea.Files.Count);
	}
}