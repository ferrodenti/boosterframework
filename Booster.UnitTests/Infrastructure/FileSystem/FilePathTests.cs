﻿using Booster.FileSystem;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Infrastructure.FileSystem;

[TestClass]
public class FilePathTests
{
	[TestMethod]
	public void GeneralBehaviour()
	{
		const string s = @"c:\test\test2\test3\a.txt";
		FilePath p = s;

		Assert.AreEqual(5, p.Segments.Count);
		Assert.IsTrue(p.IsAbsolute);
		Assert.AreEqual("a.txt", p.Filename?.ToString());
		Assert.AreEqual(s, p.ToString());

		var folder = p.Directory;

		Assert.AreEqual(@"c:\test\test2\test3\", folder.ToString());

		p = folder.Combine(@"~\test4\test5\");
		Assert.AreEqual(@"c:\test\test2\test3\test4\test5\", p.ToString());

		Assert.AreEqual(@"test4\test5\", p.MakeRelative(folder).ToString());
		Assert.AreEqual(@"..\test3\test4\test5\", p.MakeRelative(@"c:\test\test2\test0\").ToString());

		var p2 = new FilePath(@"c:\test\test2\test0\").Combine(@"..\test3\test4\test5\");

		Assert.AreEqual(p, p2);
	}
}