using Booster.Localization;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Infrastructure.Localization;

[TestClass]
public class TransliteratorTests
{
	[TestMethod]
	public void GeneralBehaviour()
	{
		Test(Transliterator.Gost16876, "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЫЭЮЯ");
		Test(Transliterator.Iso995, "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЫЮЯ");
		//Test(Transliterator.Pirate, "ПРЕВЕД");
	}

	static void Test(Transliterator transliterator, string expect)
	{
		var convert = transliterator.Convert(expect);
		var actual = transliterator.Backward.Convert(convert);

		StringCompareHelper.Instance.AssertAreEqual(expect, actual);
	}
}