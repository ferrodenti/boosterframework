using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Infrastructure;

[TestClass]
public class DelayedEventHandlerTests
{
	[TestMethod]
	public void Tests()
	{
		var h = new DelayedEventHandler();

		var ev = new ManualResetEvent(false);

		var timesFired = 0;

		h.Handler += (_, _) =>
		{
			timesFired ++;
			ev.Set();
		};

		for (var i = 0; i < 100; i++)
		{
			h.Invoke(this, EventArgs.Empty);
			Thread.Sleep(Rnd.Int(0, 50));
		}

		ev.WaitOne(1000);

		Assert.AreEqual(1, timesFired);
	}

}