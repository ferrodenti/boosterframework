﻿using System.Diagnostics;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Infrastructure;

[TestClass]
public class StringEscaperTests
{
	[TestMethod]
	public void Tests()
	{
		var esc = new StringEscaper(new StringEscaperRule("\\", "\\\\"), 
			new StringEscaperRule("\n", "\\n"),
			new StringEscaperRule("\r", "\\r"),
			new StringEscaperRule("\t", "\\t"),
			new StringEscaperRule("\b", "\\b"));

		TestString(esc, "This\nis\ta\r\nTest\b");
		TestRandom(esc);

		var esc1 = new StringEscaper("\\", "\n", "\r", "\t", "\b");
		TestRandom(esc1);


		var esc2 = new StringEscaper(
			new StringEscaperRule("\\", "\\\\"),
			new StringEscaperRule(":", "\\:"),
			new StringEscaperRule(";", "\\;"));


		TestString(esc2, "a:\\b;c:\\d;e:f\\g:h\\");

	}

	[TestMethod]
	public void SplitTests()
	{
		var esc = new StringEscaper(
			new StringEscaperRule("\\", "\\\\"),
			new StringEscaperRule(":", "\\:"),
			new StringEscaperRule(";", "\\;"));

		var speed = new SpeedComparer("Split");
		for (var i = 0; i < 100000; i++)
		{
			var str = new EnumBuilder(";");

			var arr = new string[10];

			int j;
			for (j = 0; j < 10; j++)
			{
				var e = GetRndString(esc, 5);

				arr[j] = e;
				//speed.Start();
				var s = esc.Escape(e);
				//speed.Stop();

				str.Append(s);
			}

			j = 0;
			speed.Start();
			foreach (var e in esc.Split(str, ";"))
			{
				speed.Stop();
				var a = esc.Unescape(e);
					
				//if(!StringCompareHelper.Instance.IsEqual(arr[j], a, out var msg))
				//	Trace.WriteLine(msg);

				StringCompareHelper.Instance.AssertAreEqual(arr[j], a); //TODO: здесь похоже ошибка, надо перепроверить тест
				StringCompareHelper.Instance.AssertAreEqual(arr[j++], a);
				speed.Start();
			}
			speed.Stop();
			Assert.AreEqual(10, j);
		}

		speed.TraceResults();
	}

	protected void TestString(StringEscaper esc, string str, SpeedComparer speed = null)
	{
		speed?.Start();
		var s1 = esc.Escape(str);
		if (speed != null)
		{
			speed.Stop();
			speed.Start();
		}
		var s2 = esc.Unescape(s1);
		speed?.Stop();

		//if(!StringCompareHelper.Instance.IsEqual(str, s2, out var msg))
		//	Trace.WriteLine(msg);

		StringCompareHelper.Instance.AssertAreEqual(str, s2);
	}

	protected void TestRandom(StringEscaper esc)
	{
		var speed = new SpeedComparer("Escape");
		for (var i = 0; i < 100000; i++)
			TestString(esc, GetRndString(esc, Rnd.Int(10, 100)), speed);
		speed.TraceResults();
	}

	static string GetRndString(StringEscaper esc, int len)
	{
		const int chars = 10;

		var str = new StringBuilder();
		for (var j = 0; j < len; j++)
		{
			var r = Rnd.Int(0, chars + esc.Rules.Count);
			if (r < chars)
				str.Append((char) ('a' + r));
			else
				str.Append(esc.Rules[r - chars].From);
		}
		return str.ToString();
	}
}