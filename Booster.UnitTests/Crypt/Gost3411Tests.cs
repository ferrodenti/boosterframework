﻿using System.Linq;
using Booster.Crypt;
using Booster.UnitTests.Crypt.Gvk;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Crypt;

[TestClass]
public class Gost3411Tests
{
	[TestMethod]
	public void Gost3411()
	{
		var cmp = new SpeedComparer("Gvk Gost3411", "Booster Gost3411");

		var gost1 = new Gost3411Stribog2012Turbo();
		var gost2 = new Gost3411();
		for (var i = 0; i < 1000; i++)
		{
			var len = Rnd.Int(8, 1000);
			var data = Rnd.Bytes(len);

			cmp.Start();
			var a = gost1.ComputeHash(data);
			cmp.Stop();

			cmp.Start();
			var b = gost2.ComputeHash(data);
			cmp.Stop();

			Assert.IsTrue(a.SequenceEqual(b));
		}
		cmp.TraceResults();
	}

	[TestMethod]
	public void HmacGost3411()
	{
		var cmp = new SpeedComparer("Gvk HMac", "Booster HMac");
		var gost = new Gost3411();

		for (var i = 0; i < 1000; i++)
		{
			var len = Rnd.Int(8, 1000);
			var key = Rnd.Bytes(len);
			len = Rnd.Int(8, 1000);
			var data = Rnd.Bytes(len);

			cmp.Start();
			var hmac1 = new HmacGost3411Stribog2012TurboH(key);
			hmac1.Initialize();
			var hash1 = hmac1.ComputeHash(data);
			var hash3 = hmac1.ComputeHash(hash1);
			cmp.Stop();

			cmp.Start();
			var hmac2 = new Hmac(gost, key);
			var hash2 = hmac2.ComputeHash(data);
			var hash4 = hmac1.ComputeHash(hash2);
			cmp.Stop();

			Assert.IsTrue(hash1.SequenceEqual(hash2));
			Assert.IsTrue(hash3.SequenceEqual(hash4));
		}
		cmp.TraceResults();
	}

	[TestMethod]
	public void Rfc2898Gost3411()
	{
		var cmp = new SpeedComparer("Gvk Rfc2898", "Booster Rfc2898");

		var functions = new CryptoFunctions(new Gost3411());

		for (var i = 0; i < 10; i++)
		{
			var len = Rnd.Int(8, 1000);
			var pass = Rnd.Bytes(len);
			len = Rnd.Int(8, 1000);
			var salt = Rnd.Bytes(len);

			cmp.Start();
			var hash1 = Rfc2898.Pbkdf2(pass, salt, 1000, 64);
			var hash3 = Rfc2898.Pbkdf2(hash1, salt, 1000, 64);
			cmp.Stop();

			cmp.Start();
			var hash2 = functions.Rfc2898(pass, salt, 1000, 64);
			var hash4 = Rfc2898.Pbkdf2(hash2, salt, 1000, 64);
			cmp.Stop();

			Assert.IsTrue(hash1.SequenceEqual(hash2));
			Assert.IsTrue(hash3.SequenceEqual(hash4));
		}
		cmp.TraceResults();
	}

	[TestMethod]
	public void PrfGost3411()
	{
		var cmp = new SpeedComparer("Gvk Prn768", "Booster Prn768");

		var functions = new CryptoFunctions(new Gost3411());

		for (var i = 0; i < 1000; i++)
		{
			var len = Rnd.Int(8, 1000);
			var key = Rnd.Bytes(len);

			len = Rnd.Int(8, 1000);
			var msg = Rnd.Bytes(len);

			cmp.Start();
			var prf = new Prf768(key);
			prf.GenerateKeys(msg);
			cmp.Stop();

			cmp.Start();
			var keys = functions.Prf(key, msg, 3);
			Assert.AreEqual(3, keys.Length);
			cmp.Stop();

			Assert.IsTrue(prf.Key1.SequenceEqual(keys[0]));
			Assert.IsTrue(prf.Key2.SequenceEqual(keys[1]));
			Assert.IsTrue(prf.Key3.SequenceEqual(keys[2]));
		}
		cmp.TraceResults();
	}
}