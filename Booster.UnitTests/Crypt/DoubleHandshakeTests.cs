﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Booster.Crypt;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Crypt;

[TestClass]
public class DoubleHandshakeTests
{
	[TestMethod]
	public void AuthenticationSuccessTest()
	{
		HashAlgorithm alg = new Gost3411();

		for (var i = 0; i < 20; i++)
		{
			var pass = Convert.ToBase64String(Rnd.Bytes(32));

			BaseDoubleHandshake server = new DoubleHandshakeServer(alg, pass);
			BaseDoubleHandshake client = new DoubleHandshakeClient(alg, pass);

			var msg = client.Message(null);
			msg = server.Message(msg);
			msg = client.Message(msg);
			msg = server.Message(msg);
			Assert.IsNotNull(msg);
			client.Message(msg);

			Assert.IsTrue(server.IsAuthorized);
			Assert.IsTrue(client.IsAuthorized);

			Assert.IsTrue(server.Sid1.SequenceEqual(client.Sid1));
			Assert.IsTrue(server.Sid2.SequenceEqual(client.Sid2));
		}
	}

	[TestMethod]
	public void AuthenticationMultitreadTest()
	{
		var tasks = new List<Task>();
		HashAlgorithm alg = new Gost3411();

		var success = true;
		for (var i = 0; i < 20; i++)
			tasks.Add(Task.Factory.StartNew(() =>
			{
				var pass = Convert.ToBase64String(Rnd.Bytes(32));

				BaseDoubleHandshake server = new DoubleHandshakeServer(alg, pass);
				BaseDoubleHandshake client = new DoubleHandshakeClient(alg, pass);

				var msg = client.Message(null);
				msg = server.Message(msg);
				msg = client.Message(msg);
				msg = server.Message(msg);
				if (msg == null)
				{
					success = false;
					return;
				}
				client.Message(msg);
				if (!server.Sid1.SequenceEqual(client.Sid1) || !server.Sid2.SequenceEqual(client.Sid2))
					success = false;
			}));
		foreach (var task in tasks)
		{
			Assert.IsTrue(success);
			task.Wait();
		}
		Assert.IsTrue(success);
	}

	[TestMethod]
	public void AuthenticationFailTest()
	{
		for (var i = 0; i < 20; i++)
		{
			var pass = Convert.ToBase64String(Rnd.Bytes(32));
			var j = Rnd.Int(pass.Length - 1);
			var pass2 = pass.Replace(pass[j], (char) (pass[j] + 1));

			BaseDoubleHandshake server = new DoubleHandshakeServer(SHA256.Create(), pass);
			BaseDoubleHandshake client = new DoubleHandshakeClient(SHA256.Create(), pass2);

			var msg = client.Message(null);
			msg = server.Message(msg);
			msg = client.Message(msg);
			msg = server.Message(msg);
			Assert.IsNull(msg);
			//Assert.IsTrue(server.Sid.SequenceEqual(client.Sid));
			Assert.IsFalse(server.IsAuthorized);
			Assert.IsFalse(client.IsAuthorized);
		}

	}
}