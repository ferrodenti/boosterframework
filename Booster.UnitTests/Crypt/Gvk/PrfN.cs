﻿using System;
using System.Collections.Generic;

namespace Booster.UnitTests.Crypt.Gvk;

/// <summary>
/// Вычисляет хэш-функцию со значением длиной N бит при помощи алгоритма HmacGost3411Stribog2012 
/// </summary>
public class PrfN
{
	readonly byte[] _key;

	public event EventHandler KeysReady;

	/// <summary>
	/// Initializes a new instance of the <see cref="T:Booster.UnitTests.Crypt.Gvk.PrfN"/> class.
	/// </summary>
	public PrfN(byte[] key)
		=> _key = key;

	/// <summary>
	/// Индикатор готовности ключей
	/// </summary>
	public bool IsKeysReady { get; protected set; }

	/// <summary>
	/// Объект синхронизации
	/// </summary>
	public object RootSync { get; } = new();

	public virtual byte[] Calculate(byte[] messageBytes, int n)
	{
		lock (RootSync)
		{
			IsKeysReady = false;
			var r = new List<byte>();
			var hashFunction = new Gost3411Stribog2012Turbo();
			hashFunction.Initialize();
			for (var i = 0; i < n / 32; i++)
			{
				var hm = new HmacGost3411Stribog2012TurboH(_key.ConcatByteArray(i.InvariantToByteArray()));
				hm.Initialize();
				var gostBytes = hm.ComputeHash(messageBytes);
				r.AddRange(gostBytes);
			}
			var result = L(r.ToArray(), 0, n);
			return result;
		}
	}

	/// <summary>
	/// Возвращает последовательность байтов, начиная с заданного байта.
	/// </summary>
	/// <param name="bytes">Исходный набор байтов</param>
	/// <param name="firstByte">Индекс первого байта</param>
	/// <param name="byteCount">Количество считываемых байт</param>
	/// <returns></returns>
	byte[] L(byte[] bytes, int firstByte, int byteCount)
	{
		var result = new byte[byteCount];
		Buffer.BlockCopy(bytes, firstByte, result, 0, byteCount);
		return result;
	}

	protected virtual void OnKeysReady()
		=> KeysReady?.Invoke(this, EventArgs.Empty);
}