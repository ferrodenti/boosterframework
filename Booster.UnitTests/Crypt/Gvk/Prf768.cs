﻿using System;

namespace Booster.UnitTests.Crypt.Gvk;

/// <summary>
/// Вычисляет хэш-функцию со значением длиной 768 бит при помощи алгоритма HmacGost3411Stribog2012 
/// </summary>
public class Prf768 : PrfN
{
	public byte[] Key1 { get; private set; }
	public byte[] Key2 { get; private set; }
	public byte[] Key3 { get; private set; }

	/// <summary>
	/// Initializes a new instance of the <see cref="T:Booster.UnitTests.Crypt.Gvk.Prf768"/> class.
	/// </summary>
	public Prf768(byte[] key) : base(key)
	{

	}

	/// <summary>
	/// Получение трех ключей на основе заданного при создании объекта
	/// </summary>
	/// <param name="messageBytes"></param>
	public void GenerateKeys(byte[] messageBytes)
	{
		var result = Calculate(messageBytes, 96);

		Key1 = new byte[32];
		Key2 = new byte[32];
		Key3 = new byte[32];

		Buffer.BlockCopy(result, 0, Key1, 0, 32);
		Buffer.BlockCopy(result, 32, Key2, 0, 32);
		Buffer.BlockCopy(result, 64, Key3, 0, 32);

		IsKeysReady = true;
		OnKeysReady();
	}
}