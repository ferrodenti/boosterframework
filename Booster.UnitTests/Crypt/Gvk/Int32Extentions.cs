﻿namespace Booster.UnitTests.Crypt.Gvk;

public static class Int32Extentions
{
	/// <summary>
	/// Независимо от типа системы возвращает байтовое представление числа в порядке "старшие байты впереди" независимо от платформы
	/// </summary>
	/// <param name="number"></param>
	/// <returns></returns>
	public static byte[] InvariantToByteArray(this int number)
	{
		var temp = number;
		var result = new byte[4];
		for (var i = 3; i >= 0; i--)
		{
			result[i] = (byte) (0x000000ff & temp);
			temp >>= 8;
		}
		return result;
	}
}