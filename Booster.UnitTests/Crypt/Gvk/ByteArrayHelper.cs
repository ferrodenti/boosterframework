﻿using System;

namespace Booster.UnitTests.Crypt.Gvk;

/// <summary>
/// Вспомогательные функции для работы с массивами байтов
/// </summary>
public static class ByteArrayHelper
{
	/// <summary>
	/// Выполнение операции Сложение по модулю для массивов байтов размером кратным 8.
	/// <remarks>Совпадение размеров массивов и кратность размера массива 8 не контролируются !</remarks>
	/// </summary>
	/// <param name="arr1"></param>
	/// <param name="arr2"></param>
	/// <param name="outArr"></param>
	/// <returns></returns>
	public static void Modulo512(byte[] arr1, byte[] arr2, byte[] outArr)
	{
		uint t = 0;
		for (var i = 63; i >= 0; i--)
		{
			t = (uint)(arr1[i] + arr2[i] + (t >> 8));
			outArr[i] = (byte)(t & 0xFF);
		}
		// Si fbeafaebef20fffbf0e1e0f0f520e0ed20e8ece0ebe5f0f2f120fff0eeec20f1 20faf2fee5e2202ce8f6f3ede220e8e6eee1e8f0f2d1202ee4d3d8d6d104adf1
	}

	/// <summary>
	/// Выполнение операции XOR для массивов байтов размером кратным 8.
	/// <remarks>Совпадение размеров массивов и кратность размера массива 8 не контролируются !</remarks>
	/// </summary>
	/// <param name="arr1"></param>
	/// <param name="arr2"></param>
	/// <param name="arr3"></param>
	public static void Xor(byte[] arr1, byte[] arr2, byte[] arr3)
	{
		var arrSize = arr1.Length;
		for (var i = 0; i < arrSize / 8; i++)
		{
			var uLong1 = BitConverter.ToUInt64(arr1, i * 8);
			var uLong2 = BitConverter.ToUInt64(arr2, i * 8);
			var xor = uLong1 ^ uLong2;
			var xorBytes = BitConverter.GetBytes(xor);
			Buffer.BlockCopy(xorBytes, 0, arr3, i * 8, 8);
		}
	}
}