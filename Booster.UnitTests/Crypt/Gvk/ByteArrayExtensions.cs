using System;

namespace Booster.UnitTests.Crypt.Gvk;

/// <summary>
/// Вспомогательные функции для работы с массивами байтов
/// </summary>
public static class ByteArrayExtensions
{

	/// <summary>
	/// Соединение двух массивов байтов
	/// </summary>
	/// <param name="arr1"></param>
	/// <param name="arr2"></param>
	/// <returns></returns>
	public static byte[] ConcatByteArray(this byte[] arr1, byte[] arr2)
	{
		var newArr = new byte[arr1.Length + arr2.Length];
		Buffer.BlockCopy(arr1, 0, newArr, 0, arr1.Length);
		Buffer.BlockCopy(arr2, 0, newArr, arr1.Length, arr2.Length);
		return newArr;
	}

	/// <summary>
	/// возвращает строковое представление массива байтов в шеснадцетиричном формате
	/// </summary>
	/// <param name="bytes"></param>
	/// <returns></returns>
	public static string ToStr(this byte[] bytes)
	{
		if (bytes == null)
			throw new ArgumentNullException(nameof(bytes), "Массив байтов для преобразования в строку не задан.");
		if (bytes.Length == 0)
			return string.Empty;

		var chars = new char[bytes.Length * 2];
		for (var i = 0; i < bytes.Length; i++)
		{
			var bt = bytes[i];

			var firstLetterByte = bt / 16;
			chars[i * 2] = firstLetterByte < 10 ? (char)(firstLetterByte + 48) : (char)(firstLetterByte - 10 + 65);

			var secondLetterByte = bt % 16;
			chars[i * 2 + 1] = secondLetterByte < 10 ? (char)(secondLetterByte + 48) : (char)(secondLetterByte - 10 + 65);
		}

		var str = new string(chars);
		return str;
	}

	/// <summary>
	/// Побайтовое сравнение двух массивов.
	/// Массив, в котором значение в соответствующей позиции больше, чем в другом - считается "больше".
	/// При различных размерах массивов "большим" считается тот, размер которого больше.
	/// Сравнение начинается с начала массивов. (аналогично little-endian числам)
	/// </summary>
	/// <param name="bytes1"></param>
	/// <param name="bytes2"></param>
	/// <returns></returns>
	public static int CompareTo(this byte[] bytes1, byte[] bytes2)
	{
		if (bytes1 == null)
			throw new ArgumentNullException(nameof(bytes1), "При сравнении массивов один из аргументов null");
		if (bytes1 == null)
			throw new ArgumentNullException(nameof(bytes2), "При сравнении массивов один из аргументов null");
		if (ReferenceEquals(bytes1, bytes2))
			return 0;
		if (bytes1.Length > bytes2.Length)
			return 1;
		if (bytes1.Length < bytes2.Length)
			return -1;

		for (var i = 0; i < bytes1.Length; i++)
		{
			if (bytes1[i] > bytes2[i])
				return 1;
			if (bytes1[i] < bytes2[i])
				return -1;
		}
		return 0;
	}

	/// <summary>
	/// Возвращает меньший из двух массивов после побайтового сравнения
	/// </summary>
	/// <param name="bytes1"></param>
	/// <param name="bytes2"></param>
	/// <returns></returns>
	public static byte[] Min(this byte[] bytes1, byte[] bytes2)
		=> bytes1.CompareTo(bytes2) < 0 ? bytes1 : bytes2;

	/// <summary>
	/// Возвращает больший из двух массивов после побайтового сравнения
	/// </summary>
	/// <param name="bytes1"></param>
	/// <param name="bytes2"></param>
	/// <returns></returns>
	public static byte[] Max(this byte[] bytes1, byte[] bytes2)
		=> bytes1.CompareTo(bytes2) >= 0 ? bytes1 : bytes2;
}