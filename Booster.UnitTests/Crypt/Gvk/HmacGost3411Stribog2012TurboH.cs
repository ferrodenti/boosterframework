﻿using System;
using System.Security.Cryptography;

namespace Booster.UnitTests.Crypt.Gvk;

public class HmacGost3411Stribog2012TurboH
{
	const int _blockSizeBytes = 64;
	readonly Gost3411Stribog2012Turbo _g3411;

	/// <summary>
	/// Initializes a new instance of the <see cref="T:Booster.UnitTests.Crypt.Gvk.HmacGost3411Stribog2012TurboH"/> class.
	/// </summary>
	public HmacGost3411Stribog2012TurboH(byte[] key)
	{
		Key = key;
		_g3411 = new Gost3411Stribog2012Turbo();
	}

	public byte[] Key { get; set; }

	public void Initialize()
		=> _g3411.Initialize();

	public byte[] ComputeHash(byte[] buffer)
		=> Hmac(buffer, _g3411);

	/// <summary>
	/// Реализация вычисления аутентификационного кода на основе заданной хэш-функции по алгоритму HMAC
	/// </summary>
	/// <param name="messageBytes">Сообщение</param>
	/// <param name="hashAlgorithm">Алгоритм вычисления хэш-функции</param>
	/// <returns></returns>
	byte[] Hmac(byte[] messageBytes, HashAlgorithm hashAlgorithm)
	{
		if (hashAlgorithm == null)
			throw new ArgumentException("Не задан алгоритм вычисления хэш-функции", nameof(hashAlgorithm));

		if (Key == null)
			throw new ArgumentException("Не задан ключ", new ArgumentException("Key"));

		// выравнивание ключа до размера блока
		if (Key.Length > _blockSizeBytes)
			Key = hashAlgorithm.ComputeHash(Key);
		if (Key.Length < _blockSizeBytes)
		{
			var keyNormalized = new byte[_blockSizeBytes];
			Key.CopyTo(keyNormalized, 0);
			Key = keyNormalized;
		}

		// инициализация ipad и opad
		var ipad = new byte[_blockSizeBytes];
		for (var i = 0; i < _blockSizeBytes; i++)
			ipad[i] = 0x36;
		var opad = new byte[_blockSizeBytes];
		for (var i = 0; i < _blockSizeBytes; i++)
			opad[i] = 0x5C;

		// xorKeyIpad = key ^ opad;
		var xorKeyIpad = new byte[ipad.Length];
		ByteArrayHelper.Xor(ipad, Key, xorKeyIpad);

		// xorKeyOpad = key ^ ipad;
		var xorKeyOpad = new byte[ipad.Length];
		ByteArrayHelper.Xor(opad, Key, xorKeyOpad);

		// xorKeyIpad | Message
		var concatedMessage1 = xorKeyIpad.ConcatByteArray(messageBytes);

		// hash(xorKeyIpad | Message)
		var hash1 = hashAlgorithm.ComputeHash(concatedMessage1);

		// xorKeyOpad || hash(xorKeyIpad | Message)
		var concatedMessage2 = xorKeyOpad.ConcatByteArray(hash1);

		// hash(xorKeyIpad | Message)
		var hash2 = hashAlgorithm.ComputeHash(concatedMessage2);

		return hash2;
	}

}