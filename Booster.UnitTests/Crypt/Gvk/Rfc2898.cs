﻿using System;
using System.Linq;

namespace Booster.UnitTests.Crypt.Gvk;

public class Rfc2898
{
	readonly HmacGost3411Stribog2012TurboH _hmac;
	readonly int _hLen;
	readonly byte[] _salt;
	readonly int _iterationsNumber;
	int _dkLen;

	public Rfc2898(byte[] password, byte[] salt, int iterations)
	{
//            if (iterations < 1000)
//                throw new ArgumentException("Аргумент iterations имеет значение  меньше 1000.", "iterations");
		if (salt.Length < 8)
			throw new ArgumentException("Аргумент salt имеет размер меньше 8 байт.", nameof(salt));
		_hmac = new HmacGost3411Stribog2012TurboH(password);
		_hLen = 256/8;
		_salt = salt;
		_iterationsNumber = iterations;
	}

	public byte[] GetDerivedKey(int keyLength)
	{
		_dkLen = keyLength;
		var num = Math.Ceiling((double)_dkLen / _hLen);
		var source1 = new byte[0];
		for (var i = 1; (double) i <= num; ++i)
			source1 = source1.ConcatByteArray(F(_salt, _iterationsNumber, i));
		return source1.Take(_dkLen).ToArray();
	}

	public static byte[] Pbkdf2(byte[] password, byte[] salt, int iterations, int dkLen)
		=> new Rfc2898(password, salt, iterations).GetDerivedKey(dkLen);

	byte[] F(byte[] salt, int iterationsNumber, int i)
	{
		var s1 = salt.ConcatByteArray(Int(i));
		var s2 = Prf(s1);
		var numArray = s2;
		for (var index1 = 1; index1 < iterationsNumber; ++index1)
		{
			s2 = Prf(s2);
			for (var index2 = 0; index2 < s2.Length; ++index2)
				numArray[index2] ^= s2[index2];
		}
		return numArray;
	}

	byte[] Prf(byte[] salt)
	{
		_hmac.Initialize();
		return _hmac.ComputeHash(salt);
	}

	static byte[] Int(int i)
	{
		var bytes = BitConverter.GetBytes(i);
		if (BitConverter.IsLittleEndian)
			Array.Reverse(bytes);
		return bytes;
	}
}