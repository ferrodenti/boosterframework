﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Crypt;

[TestClass]
public class SearchForTheFastestXor
{
	static readonly int _paralleldegree = Environment.ProcessorCount;

	public static unsafe void VectXor(byte[] arr1, byte[] arr2, byte[] result)
	{
		var len = arr1.Length;
		var i = 0;
		if (len >= sizeof(ulong))
		{
			i = len / sizeof(ulong);
			fixed (byte* pRes = result, pArr1 = arr1, pArr2 = arr2)
			{
				var pr = (ulong*)pRes;
				var p1 = (ulong*)pArr1;
				var p2 = (ulong*)pArr2;
				var pEnd = p1 + i;
				while (p1 < pEnd)
				{
					*pr = *p1 ^ *p2;
					p1++;
					p2++;
					pr++;
				}
			}
			i *= sizeof (ulong);
		}
		for (; i < len; i++)
			result[i] = (byte)(arr1[i] ^ arr2[i]);
	}

	public static unsafe byte[] ParallelXor(byte[] arr1, byte[] arr2)
	{
		var maxSize = Math.Max(arr1.Length, arr2.Length);
		var minSize = Math.Min(arr1.Length, arr2.Length);
		var result = new byte[maxSize];
		var ipar = 0;
		var block = maxSize / _paralleldegree + 1;

		while (block % sizeof(ulong) != 0)
			block++;

		void Proc()
		{
			var actidx = Interlocked.Increment(ref ipar) - 1;
			var max = Math.Min(block * (actidx + 1), minSize);
			var max2 = Math.Min(block * (actidx + 1), maxSize);
			var start = block * actidx / sizeof(ulong);

			fixed (byte* pRes = result, pArr1 = arr1, pArr2 = arr2)
			{
				var pr = (ulong*) pRes + start;
				var p1 = (ulong*) pArr1 + start;
				var p2 = (ulong*) pArr2 + start;
				while (pr < pRes + max)
				{
					*pr = *p2 ^ *p1;
					pr++;
					p1++;
					p2++;
				}

				var pl = arr1.Length > arr2.Length ? p1 : p2;
				while (pr < pRes + max2)
				{
					*pr = *pl;
					pr++;
					pl++;
				}
			}
		}

		var actions = new Action[_paralleldegree];
		for (var i = 0; i < _paralleldegree; i++)
			actions[i] = Proc;

		Parallel.Invoke(actions);

		return result;
	}


	public void GvkXor(byte[] arr1, byte[] arr2, byte[] arr3)
	{
		var arrSize = arr1.Length;
		var i = 0;
		for (; i < arrSize / 8; i++)
		{
			var uLong1 = BitConverter.ToUInt64(arr1, i * 8);
			var uLong2 = BitConverter.ToUInt64(arr2, i * 8);
			var xor = uLong1 ^ uLong2;
			var xorBytes = BitConverter.GetBytes(xor);
			Buffer.BlockCopy(xorBytes, 0, arr3, i * 8, 8);
		}
		for (i = i * 8; i < arrSize; i++)
			arr3[i] = (byte)(arr1[i] ^ arr2[i]);
	}

	public byte[] SimpleXor(byte[] arr1, byte[] arr2)
	{
		var res = new byte[arr1.Length];

		for (var i = 0; i < arr1.Length; i++)
			res[i] = (byte) (arr1[i] ^ arr2[i]);

		return res;
	}


	[TestMethod]
	public void Xor()
	{
		var cmp = new SpeedComparer("Simple xor", "Gvk xor", "Parallel xor", "Vector xor");

		for (var i = 0; i < 1000; i++)
		{
			var size = Rnd.Int(1, 2000);
			var arr1 = Rnd.Bytes(size);
			var arr2 = Rnd.Bytes(size);

			cmp.Start();
			var res1 = SimpleXor(arr1, arr2);
			cmp.Stop();

			cmp.Start();
			var res2 = new byte[size];
			GvkXor(arr1, arr2, res2);
			cmp.Stop();

			Assert.IsTrue(res1.SequenceEqual(res2));

			cmp.Start();
			var res3 = ParallelXor(arr1, arr2);
			cmp.Stop();
			Assert.IsTrue(res1.SequenceEqual(res3));

			cmp.Start();
			var res4 = new byte[size];
			VectXor(arr1, arr2, res4);
			cmp.Stop();
			Assert.IsTrue(res1.SequenceEqual(res4));

		}

		cmp.TraceResults();
	}
}