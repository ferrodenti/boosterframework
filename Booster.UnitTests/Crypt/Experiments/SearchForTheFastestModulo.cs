﻿using System.Linq;
using Booster.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Crypt;

[TestClass]
public class SearchForTheFastestModulo512
{
	const int _modulo = 64;

	public static unsafe void VectModulo512(byte[] arr1, byte[] arr2, byte[] result)
	{
		const int start = _modulo / sizeof(uint)-1;

		fixed (byte* pArr1 = arr1, pArr2 = arr2, pRes = result)
		{
			var p1 = (uint*) pArr1 + start;
			var p2 = (uint*) pArr2 + start;
			var pR = (uint*) pRes + start;

			ulong t = 0;
			while (p1 >= pArr1)
			{
				t = (ulong)EndianHelper.Swap32(*p1) + EndianHelper.Swap32(*p2) + (t >> 32);
				*pR = EndianHelper.Swap32((uint)t);
				p1--;
				p2--;
				pR--;
			}
		}
	}

	static void GvkModulo512(byte[] arr1, byte[] arr2, byte[] result)
	{
		uint t = 0;
		for (var i = _modulo - 1; i >= 0; i--)
		{
			t = (uint)(arr1[i] + arr2[i] + (t >> 8));
			result[i] = (byte)(t & 0xFF);
		}
	}
	static void ByteModulo512(byte[] arr1, byte[] arr2, byte[] result)
	{
		byte hi = 0;
		for (var i = _modulo - 1; i >= 0; i--)
		{
			var a = arr1[i];
			var r = (byte) (a + hi);
			hi = r < a ? (byte) 1 : (byte) 0;
			result[i] = a = (byte)(r + arr2[i]);
			if (a < r)
				hi ++;
		}
	}
		
	[TestMethod]
	public void Modulo512()
	{
		var cmp = new SpeedComparer("Gvk Modulo", "Vector Modulo", "Byte Modulo");

		for (var i = 0; i < 10000; i++)
		{
			const int size = _modulo;
			var arr1 = Rnd.Bytes(size);
			var arr2 = Rnd.Bytes(size);

			cmp.Start();
			var res1 = new byte[size];
			GvkModulo512(arr1, arr2, res1);
			cmp.Stop();

			cmp.Start();
			var res2 = new byte[size];

			//LowLevel.Modulo512(arr1, arr2, res2);
			VectModulo512(arr1, arr2, res2);
			cmp.Stop();
			for (var j = 0; j < 64; j++)
				if (res1[j] != res2[j])
				{

				}

			Assert.IsTrue(res1.SequenceEqual(res2));

			cmp.Start();
			var res3 = new byte[size];
			ByteModulo512(arr1, arr2, res3);
			cmp.Stop();
			Assert.IsTrue(res1.SequenceEqual(res3));

			Assert.IsTrue(res1.SequenceEqual(res2));

		}

		cmp.TraceResults();
	}
}