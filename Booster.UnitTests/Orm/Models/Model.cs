//#define CHANGE

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Booster.Debug;
using Booster.DI;
using Booster.Orm;
using Booster.Orm.Interfaces;
using Booster.Orm.Oracle;
using Booster.Orm.SqLite;


namespace Booster.UnitTests.Orm;

[Db("test_table1")]
public class Model : IndexedEntity<Model>
{
#if CHANGE //Manual schema update testing 
        [Db] public int Cha1;
        [Db] public string Cha2;
#else
	[Db] public string Cha1;
	[Db] public int Cha2;
#endif

	[Db, DbIndex] public string StrField;
	[Db] public string StrProperty { get; set; }

	[Db] public int Int { get; set; }
	[Db, DbType(typeof(string))] public One<Model2> String2Int { get; set; }
	[Db] public bool Bool { get; set; }
	[Db] public bool NewBool { get; set; }
	[Db] public DateTime DateTime { get; set; }
	[Db] long Long { get; set; }
	[Db] public byte[] Blob { get; set; }
		

	[Db] public float Float { get; set; }
	[Db] public decimal Decimal { get; set; }
	[Db] public double Double { get; set; }
	[Db] public TestEnum TestEnum { get; set; }

	[Db] public Guid Guid { get; set; }
	[Db] public Color Color { get; set; }

	[Db, DbNull(SaveDefaultValuesAsDbNull = false)] public int NullableInt { get; set; }
	[Db, DbNull(SaveDefaultValuesAsDbNull = true)] public int NullableInt2 { get; set; }
	[Db] public int? NullableInt3 { get; set; }

	[Db] public ConfClass ConfClass { get; set; }
	[Db] public ConfCastClass ConfCastClass { get; set; }
	[Db, DbType(typeof(string))] public CastClass CastClass { get; set; }
	[Db] public ConfStruct ConfStruct { get; set; }
	[Db] public ConfStruct ConfStructField;
	[Db] public ConfCastStruct ConfCastStruct { get; set; }


	public IEnumerable<Model2> Childs => Model2.Select($"{nameof(Model2.ModelId):?}={Id:@}");


	public static Model CreateRandom()
		=> new()
		{
			StrField = TestUtils.RandomString(Rnd.Int(-10, 1024)),
			StrProperty = TestUtils.RandomString(Rnd.Int(-10, 1024)),

			Int = Rnd.Int(),
			Bool = Rnd.Bool(),

			Float = Rnd.Float(),
			Decimal = Rnd.Decimal(),
			Double = Rnd.Double(double.MinValue, double.MaxValue),
			Long = Rnd.Long(long.MinValue, long.MaxValue),

			DateTime = DateTime.Now.AddMilliseconds(Rnd.Int()),
			Blob = Rnd.Bool() ? Rnd.Bytes(Rnd.Int(1024)) : null,

			TestEnum = (TestEnum) Rnd.Int(31),
			NullableInt = Rnd.Bool() ? 0 : Rnd.Int(),
			NullableInt2 = Rnd.Bool() ? 0 : Rnd.Int(),
			NullableInt3 = Rnd.Bool() ? null : (int?) (Rnd.Bool() ? 0 : Rnd.Int()),
			Guid = Guid.NewGuid(),
			Color = Color.FromArgb(Rnd.Int()),
		};

	public override int GetHashCode()
		// ReSharper disable once BaseObjectGetHashCodeCallInGetHashCode
		=> base.GetHashCode();

	public override bool Equals(object obj)
	{
		var b = obj as Model;

		var res = false;
		if (b != null)
		{
			// ReSharper disable CompareOfFloatsByEqualityOperator
			res = Id == b.Id &&
			      StrField == b.StrField &&
			      StrProperty == b.StrProperty &&
			      Int == b.Int &&
			      Math.Abs(Float - b.Float) <= 0.01f &&
			      Math.Abs(Double - b.Double) <= 0.01 &&
			      Bool == b.Bool &&
			      DateTime.ToString() == b.DateTime.ToString() &&
			      Long == b.Long &&
			      TestEnum == b.TestEnum &&
			      Guid == b.Guid &&
			      Color == b.Color &&
			      NullableInt == b.NullableInt &&
			      NullableInt2 == b.NullableInt2 &&
			      NullableInt3.Equals(b.NullableInt3) &&
			      (Blob == null && b.Blob == null || Blob != null && Blob.SequenceEqual(b.Blob));
			// ReSharper restore CompareOfFloatsByEqualityOperator

			var ormType = InstanceFactory.Get<IOrm>().GetType();

			if (ormType == typeof(SqLiteOrm))
				res &= Math.Abs(Decimal - b.Decimal) < Math.Abs(0.00000000000001m * Decimal); //Walk around lack of decimal support in SqLite
			else if (ormType == typeof(OracleOrm))
				res &= Math.Abs(Decimal - b.Decimal) < Math.Abs(0.0001m * Decimal); //Walk around lack of decimal support in Oracle
			else
				res &= Decimal == b.Decimal;

			if (!res)
			{
				LoadById(Id);
				SafeBreak.Break();
			}
		}
		return res;
	}
}