using Booster.Orm;

namespace Booster.UnitTests.Orm;

[Db("postgres_model")]
[DbInstanceContext("PostgreSql")]
public class PostgresOnlyModel : IndexedEntity<PostgresOnlyModel>
{
	[Db] public string Test;
}