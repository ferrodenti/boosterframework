using System;
using Booster.Orm;

namespace Booster.UnitTests.Orm;

[Db("test_table2")]
public class Model2 : IndexedEntity<Model2>
{
	[Db] public string StrField;
	[Db] public string StrProperty { get; set; }

	[Db] public int Int { get; set; }
	[Db] public bool Bool { get; set; }
	[Db] public DateTime DateTime { get; set; }
	[Db] public long Long { get; set; }

	[Db] public One<Model> ModelId;
	public Model Model
	{
		get => (Model)ModelId;
		set => ModelId = (One<Model>)value;
	}
	[Db]  public byte[] Blob { get; set; }

	public static Model2 CreateRandom(int modelId)
		=> new()
		{
			StrField = TestUtils.RandomString(Rnd.Int(-10, 1024)),
			StrProperty = TestUtils.RandomString(Rnd.Int(-10, 1024)),

			Int = Rnd.Int(),
			Bool = Rnd.Bool(),
			DateTime = DateTime.Now.AddMilliseconds(Rnd.Int()),
			Blob = Rnd.Bool() ? Rnd.Bytes(Rnd.Int(1024)) : null,
			Long = Rnd.Long(long.MinValue, long.MaxValue),
						
			ModelId = modelId
		};
}