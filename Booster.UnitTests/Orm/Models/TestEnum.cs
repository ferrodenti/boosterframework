using System;
using Booster.Interfaces;

namespace Booster.UnitTests.Orm;

[Flags]
public enum TestEnum
{
	A = 0,
	B = 1,
	C = 2,
	D = 4,
	E = 8,
	F = 16
}

public class ConfClass : IConvertiableFrom<string>
{
	public string Value { get; set; }
	public bool IsEmpty => Value != null;
}
public class ConfCastClass : IConvertiableFrom<string>
{
	public string Value { get; set; }
	public bool IsEmpty => Value != null;

	public static explicit operator string(ConfCastClass c)
		=> c?.Value;

	public static explicit operator ConfCastClass(string s)
		=> new() {Value = s};
}

public class CastClass
{
	public string Value { get; set; }

	public static explicit operator string(CastClass c)
		=> c?.Value;

	public static explicit operator CastClass(string s)
		=> new() {Value = s};
}

public struct ConfStruct : IConvertiableFrom<int>
{
	public int Value { get; set; }
	public bool IsEmpty => Value != 0;
}

public struct ConfCastStruct : IConvertiableFrom<int>
{
	public int Value { get; set; }
	public bool IsEmpty => Value != 0;

	public static explicit operator int(ConfCastStruct c)
		=> c.Value;

	public static explicit operator ConfCastStruct(int s)
		=> new() {Value = s};
}