using Booster.Orm;

namespace Booster.UnitTests.Orm;

[Db("SqLiteModel")]
[DbInstanceContext("SqLite")]
public class SqLiteOnlyModel : IndexedEntity<SqLiteOnlyModel>
{
	[Db] public string Test;
}