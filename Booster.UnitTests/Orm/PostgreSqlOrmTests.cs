using Booster.Orm;
using Booster.Orm.Interfaces;
using Booster.Orm.PostgreSql;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Orm;

[TestClass]
public class PostgreSqlOrmTests : BaseOrmTests
{
	[ClassInitialize]
	public static void ClassInit(TestContext context)
	{
		new PostgreSqlOrmTests().Init(context);
	}
		
	protected override IOrm CreateOrm()
	{
		var settings = GetConnectionString("PostgreSql");
		if (settings == null)
			return null;

		return new PostgreSqlOrm(settings, CreateLog())
		{
			ConnectionSettings = {DbUpdateOptions = DbUpdateOptions.All | DbUpdateOptions.ThrowExceptions}
			//AllowDebugAdapters = false
		};
	}

	protected override void TestCollation(Model model)
	{
		var act = Model.Load("upper(str_field)=@s", new {s = model.StrField.ToUpper()});
		Assert.AreEqual(model.Id, act.Id);

		act = Model.Load("lower(str_field)=@s", new {s = model.StrField.ToLower()});
		Assert.AreEqual(model.Id, act.Id);
	}


	/*

SELECT 
pg_terminate_backend(pid) 
FROM 
pg_stat_activity 
WHERE 
datname = 'test_db' and usename = 'test_user'

*/

}