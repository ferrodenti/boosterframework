using System.Threading.Tasks;
using Booster.DI;
using Booster.Log;
using Booster.Orm.Interfaces;
using Booster.Orm.PostgreSql;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Orm;

[TestClass]
public class ConnectionSharingTests
{
		
	[TestMethod]
	public async Task Tests()
	{
		var log = new TraceLog();

		if (!BaseOrmTests.TryGetConnectionString("PostgreSql", out var connStr) ||
		    !BaseOrmTests.TryGetConnectionString("PostgreSql", out var connStr2))
		{
			log.Debug("Skipping unit tests");
			return;
		}
			
		using (InstanceFactory.PushContext("Orm1"))
		{
			var orm1 = new PostgreSqlOrm(connStr, log);
			InstanceFactory.Register(orm1);
		}

		using (InstanceFactory.PushContext("Orm2"))
		{
			var orm2 = new PostgreSqlOrm(connStr2, log);
			InstanceFactory.Register(orm2);
			await orm2.RegisterRepoAsync(typeof (Model2));
		}

		using (InstanceFactory.Get<IConnection>("Orm1"))
		using (var conn = InstanceFactory.Get<IConnection>("Orm2"))
			await conn.ExecuteScalarAsync<int>($"select count(*) from {typeof(Model2):?}");
	}
}