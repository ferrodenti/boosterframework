﻿using Booster.Orm.Kitchen;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Orm;

[TestClass]
public class ParamExcapingTests
{
	[TestMethod]
	public void ParamExcapingSynteticTest()
	{
		var formatter = new BasicSqlFormatter(null);

		const string allowed = "fdsfsd LµЉl∆lљdџƒcmђ«dўљ÷“ DSFSD ВАЫВЫв ывавыа 324324 m,._+=-!@#%$^%*()|:\"\t\n\r";
		Assert.AreEqual(allowed, formatter.EscapeString(allowed));
		Assert.AreEqual("''''''", formatter.EscapeString("'`´"));
		Assert.AreEqual("asd dfsd", formatter.EscapeString($"asd{(char) 8}dfsd"));
	}
}