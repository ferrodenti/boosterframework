using Booster.Orm;
using Booster.Orm.Interfaces;
using Booster.Orm.Oracle;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Orm;

[TestClass]
public class OracleOrmTests : BaseOrmTests
{
	[ClassInitialize]
	public static void ClassInit(TestContext context)
		=> new OracleOrmTests().Init(context);

	protected override IOrm CreateOrm()
	{
		var settings =  GetConnectionString("Oracle");
		if (settings == null)
			return null;

		return new OracleOrm(settings, CreateLog())
		{
			ConnectionSettings = {DbUpdateOptions = DbUpdateOptions.All | DbUpdateOptions.ThrowExceptions}
		};
	}

	protected override void TestCollation(Model model)
	{
		//var act = Model.Load("NLS_UPPER(strfield) like :s", new {s = model.StrField.ToUpper()});
		//Assert.AreEqual(model.Id, act.Id);

		//act = Model.Load("NLS_LOWER(strfield) like :s", new {s = model.StrField.ToLower()});
		//Assert.AreEqual(model.Id, act.Id);
	}

	protected override Model CreateRandom()
	{
		var res = base.CreateRandom();
		res.StrField = res.StrField == "" ? null : res.StrField;
		res.StrProperty = res.StrProperty == "" ? null : res.StrProperty;
		res.Double = Rnd.Double(double.MinValue / 10E+185, double.MaxValue / 10E+185); 
				
		return res;
	}

	protected override Model2 CreateRandom2(int modelId)
	{
		var res =  base.CreateRandom2(modelId);
		res.StrField = res.StrField == "" ? null : res.StrField;
		res.StrProperty = res.StrProperty == "" ? null : res.StrProperty;
		return res;
	}
}