using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Booster.Debug;
using Booster.DI;
using Booster.Log;
using Booster.Log.Kitchen;
using Booster.Orm;
using Booster.Orm.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

#nullable enable

namespace Booster.UnitTests.Orm;

public abstract class BaseOrmTests
{
	protected abstract IOrm? CreateOrm();

	static TestContext? _context;

	public static string? GetConnectionString(string key)
		=> UnitTestsConfig.Instance.ConnectionStrings.SafeGet(key);

	public static bool TryGetConnectionString(string key, out string value)
		=> UnitTestsConfig.Instance.ConnectionStrings.TryGetValue(key, out value!);

	protected ILog CreateLog()
	{
		return new Log.Log(
			new TraceLog
			{
				//Async = true,
				MessageFormatter = new MessageFormatter
				{
					CustomExceptionFormat = msg =>
					{
						SafeBreak.Break(); 
						return msg.Exception?.ToString() ?? "Unknown error";
					}
				}
			},
			new ConsoleLog(),
			new FileLog(GetType().Name + "_tests_log.txt"));
	}

	public void Init(TestContext? context)
	{
		SafeBreak.Enabled = true;

		_context = context;

		var orm = CreateOrm();
		if (orm == null && _context != null)
		{
			_context.Properties["skip"] = true;
			return; //No connection string -- skip current orm tests
		}
		InstanceFactory.Register(orm, true);

		//Testing connection
		orm!.Ping();

		//orm.RegisterRepo(typeof(PreModel));
		orm.RegisterRepos();
	}

	[TestMethod]
	public void Tests()
	{
		if (Equals(_context?.Properties["skip"], true)) //No connection string -- skip current orm tests
			return;

		var cmp = new SpeedComparer(GetType().Name);

		var m20 = new Model2();

		Utils.Nop((int)m20.ModelId);
		Utils.Nop(m20.Model);
		m20.ModelId = 10;
		m20.Model = new Model();

		using (var conn = InstanceFactory.Get<IConnection>())
			for (var k = 0; k < 5; k++)
			{
				cmp.Start();
				//Clearing test database
				conn.ExecuteNoQuery($"delete from test_table1");
				conn.ExecuteNoQuery($"delete from test_table2");

				//Testing common behaviour
				var test = new List<Model>();
				const int len = 64;

				for (var i = 0; i < len; i++)
				{
					var m = CreateRandom();
					test.Add(m);
					m.Insert();
				}
	
				Model[] res;

				using (InstanceFactory.Get<IConnection>())
					res = Model.All.ToArray();


				for (var i = 0; i < 10; i++)
				{
					QueryBuilder qb = new QueryBuilder<Model>();

					if (Rnd.Bool())
					{
						var str = TestUtils.RandomString(Rnd.Int(0, 256));

						var j = Rnd.Int(0, str.Length);
						str = str.Insert(j, "\'");
						j = Rnd.Int(0, str.Length);
						str = str.Insert(j, "\"");

						qb.AppendOr($"{nameof(Model.StrField):?}={str:@}");
					}

					if (Rnd.Bool())
						qb.AppendOr($"{nameof(Model.DateTime):?}={DateTime.Now.AddMilliseconds(Rnd.Int(-1000000, 1000000)):@}");

					if (Rnd.Bool())
						qb.AppendOr($"{nameof(Model.Bool):?}={Rnd.Bool():@}");

					if (Rnd.Bool())
						qb.AppendOr($"{nameof(Model.Int):?}={Rnd.Int():@}");

					Utils.Nop(
						Model.Select(qb.Inlined, null!)
						.Cast<object>()
						.ToArray());
				}

				Assert.AreEqual(test.Count, res.Length, "Testing common behaviour failed: counts mismatch!");

				for (var i = 0; i < test.Count; i++)
				{
					var exp = test[i];
					var act = res[i];
					if (!exp.Equals(act))
						Utils.Nop();
					Assert.IsTrue(exp.Equals(act), "Testing common behaviour failed: models mismatch!");
				}

				//Testing string collation
				var model = test.Skip(Rnd.Int(10)).FirstOrDefault(m => m.StrField.IsSome());
				if (model != null)
					TestCollation(model);

				//Testing relations
				for (var i = 0; i < len; i++)
				{
					var j = Rnd.Int(1, test.Count - 1);
					var m = CreateRandom2(test[j].Id);
					m.Insert();
				}

				var l2 = 0;

				//Testing data readers' protection
				foreach (var m1 in Model.All)
				foreach (var m2 in m1.Childs)
				{
					l2++;
					m2.Save();
				}

				Assert.AreEqual(len, l2, "Testing relations failed: data corrupt!");
				cmp.Stop();
			}

		cmp.TraceResults();
	}

	protected virtual void TestCollation(Model model) // By default SQL collations are case insensitive. Orm.PostgreSql & Orm.Oracle overrides this method
	{
		var fmt = Instance<IOrm>.Value.SqlFormatter;

		var act = Model.Load("StrField=" + fmt.ParamName("s"), new {s = model.StrField.ToUpper()})!;
		Assert.AreEqual(model.Id, act.Id);

		act = Model.Load("StrField=" + fmt.ParamName("s"), new {s = model.StrField.ToLower()})!;
		Assert.AreEqual(model.Id, act.Id);
	}

	[TestMethod]
	public void ConcurrencyTest()
	{
		if (Equals(_context?.Properties["skip"], true)) //No connection string -- skip current orm tests
			return;

		var tasks = new List<Task>();
		(int min, int max) ids;
		int s = 0, err = 0;

		var sw = new Stopwatch();
		sw.Start();

		using (var c = InstanceFactory.Get<IConnection>())
			ids = c.ExecuteTuple<(int, int)>($"select min(ID), max(ID) from {c.Orm.GetRepo(typeof(Model)).TableMapping.TableName}");

		for (var i = 0; i < 10; i++)
			tasks.Add(Task.Factory.StartNew(() =>
			{
				for (var j = 0; j < 100; j++)
					try
					{
						using (InstanceFactory.Get<IConnection>())
						{
							var id = Rnd.Int(ids.min, ids.max);
							var m = Model.LoadById(id);
							if (m != null)
								unchecked
								{
									s += m.Int;
								}
						}
					}
					catch (Exception ex)
					{
						SafeBreak.Break();
						Utils.Nop(ex);
						err++;
					}
			}));

		foreach (var task in tasks)
			task.Wait();

		sw.Stop();

		Trace.WriteLine($"Concurrency test complete in {sw.ElapsedMilliseconds} ms, {err} errors. {s}");
	}


	[TestMethod]
	public void GuidPkTest()
	{
		if (Equals(_context?.Properties["skip"], true)) //No connection string -- skip current orm tests
			return;

		var model = new GuidPkModel();
		model.Insert();
		var guid = model.Id;

		model = GuidPkModel.LoadById(guid)!;
		model.Data = TestUtils.RandomString(100);
		model.Save();
		model.Delete();
	}

	protected virtual Model CreateRandom()
		=> Model.CreateRandom();

	protected virtual Model2 CreateRandom2(int modelId)
		=> Model2.CreateRandom(modelId);
}