using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Booster.Debug;
using Booster.DI;
using Booster.Log;
//using Booster.Log.Db;
using Booster.Orm;
using Booster.Orm.Interfaces;
using Booster.Orm.Kitchen;
using Booster.Orm.Oracle;
using Booster.Orm.PostgreSql;
using Booster.Orm.SqLite;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Orm;

[TestClass]
public class ParallelOrmsTests
{
	[TestMethod]
	public void Tests()
	{
		SafeBreak.Enabled = true;

		ILog log;
		
		if (BaseOrmTests.TryGetConnectionString("PostgreSql_logs", out var settings))
			using (InstanceFactory.PushContext("LOG"))
			{
				log = new Log.Log(
					// new DbLog(new PostgreSqlOrm(settings)
					// {
					// 	ConnectionSettings =
					// 	{
					// 		DbUpdateOptions = DbUpdateOptions.All | DbUpdateOptions.ThrowExceptions
					// 	}
					// }).Init(),
					new TraceLog());
				log.Info("Db logs");
			}
		else
		{
			log = new TraceLog();
			log.Info("Trace logs");
		}

		var tasks = new List<string>();

		CreateOrm(log, tasks, "PostgreSql", (s, l) => new PostgreSqlOrm(s, l));
		CreateOrm(log, tasks, "SqLite", (s, l) => new SqLiteOrm(s, l));
		CreateOrm(log, tasks, "Oracle", (s, l) => new OracleOrm(s, l));

		log.Debug($"{tasks.Count} orms created...");

		var i = 0;

		var ev = new ManualResetEvent(false);
		foreach (var ctx in tasks)
			Task.Factory.StartNew(() =>
			{
				InstanceFactory.CurrentContext = ctx;
				var log2 = InstanceFactory.Get<ILog>();
				try
				{
					var orm = InstanceFactory.Get<IOrm>();
					log2.Debug($"Initializing {orm}...");

					orm.Ping();
					orm.RegisterRepos();
				}
				catch (Exception ex)
				{
					log2.Error(ex);
					SafeBreak.Break();
				}
				if (++i == tasks.Count)
					ev.Set();
			});
		log.Debug("Waiting for orms to initialize...");

		if (tasks.Count == 0)
			ev.Set();
		
		ev.WaitOne();

		
		if(IsRegistred<Model>())
			try
			{
				Model.LoadById(1);
				Assert.Fail($"Default context has {InstanceFactory.Get<IOrm>().GetType()}");
			}
			catch (AssertFailedException ex)
			{
				log.Error(ex, ex.Message);
			}
			catch (Exception ex)
			{
				Utils.Nop(ex);
			}

		if (IsRegistred<SqLiteOnlyModel>())
		{
			var m1 = new SqLiteOnlyModel { Test = "SqLite" };
			m1.Insert();
		}

		if (IsRegistred<PostgresOnlyModel>())
		{
			var m2 = new PostgresOnlyModel { Test = "Postgres" };
			m2.Insert();
		}

		if (IsRegistred<SqLiteOnlyModel>())
			foreach (var m in SqLiteOnlyModel.Select("id > :n", new { n = 0 }))
				m.Delete();

		if (IsRegistred<PostgresOnlyModel>())
			foreach (var m in PostgresOnlyModel.Select("id > :n", new { n = 0 }))
				m.Delete();

		var waitHandles = tasks.Select(TestOrm).ToList();

		log.Debug("Waiting for tests to complete");

		foreach (var handle in waitHandles)
			handle.WaitOne();

		log.Debug("Done!");
	}

	protected static bool IsRegistred<TModel>()
	{
		try { return RepositoryInstance<TModel>.GetRepo(false) != null; }
		catch { return false; }
	}
	
	protected static bool CreateOrm(ILog log, List<string> tasks, string ctx, Func<dynamic, ILog, BaseOrm> creator)
	{
		if (!BaseOrmTests.TryGetConnectionString(ctx, out var settings)) 
			return false;
		
		using (InstanceFactory.PushContext(ctx))
		{
			var log2 = log.Create(new {Context = ctx});
			var orm = creator(settings, log2!);
			orm.ConnectionSettings.DbUpdateOptions = DbUpdateOptions.All | DbUpdateOptions.ThrowExceptions;

			InstanceFactory.Register(orm);
			InstanceFactory.Register(log2);
			tasks.Add(ctx);
		}

		return true;
	}

	protected static WaitHandle TestOrm(string ctx)
	{
		var ev = new ManualResetEvent(false);
		Task.Factory.StartNew(() =>
		{
			InstanceFactory.CurrentContext = ctx;
			Tuple<int, int> ids;
			var s = 0;

			using (var c = InstanceFactory.Get<IConnection>())
				ids = c.ExecuteTuple<Tuple<int, int>>($"select min(ID), max(ID) from {c.Orm.GetRepo<Model>().TableMapping.TableName}");

			const int numberOfTasks = 10;
			var numberOfCompletedTasks = 0;

			for (var i = 0; i < numberOfTasks; i++)
				Task.Factory.StartNew(() =>
				{
					InstanceFactory.CurrentContext = ctx;
					for (var j = 0; j < 50; j++)
						try
						{
							using (InstanceFactory.Get<IConnection>())
							{
								var id = Rnd.Int(ids.Item1, ids.Item2);
								var m = Model.LoadById(id);
								id = m?.Int ?? 0;
								unchecked
								{
									s += id;
								}

								var m2 = Model2.CreateRandom(id);
								m2.Insert();
							}
						}
						catch (Exception ex)
						{
							InstanceFactory.Get<ILog>().Error(ex);
							SafeBreak.Break();
						}

					if (++numberOfCompletedTasks == numberOfTasks)
						ev.Set();
				});

			Utils.Nop(s);
		});
		return ev;
	}
}