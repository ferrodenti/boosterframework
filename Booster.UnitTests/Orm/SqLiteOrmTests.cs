using Booster.Orm;
using Booster.Orm.Interfaces;
using Booster.Orm.SqLite;
using Microsoft.VisualStudio.TestTools.UnitTesting;

#nullable enable

namespace Booster.UnitTests.Orm;

[TestClass]
public class SqLiteOrmTests : BaseOrmTests
{
	[ClassInitialize]
	public static void ClassInit(TestContext context)
		=> new SqLiteOrmTests().Init(context);

	protected override IOrm? CreateOrm()
	{
		if(!TryGetConnectionString("SqLite", out var connString))
			return null;

		return new SqLiteOrm(connString, CreateLog())
		{
			ConnectionSettings =
			{
				DbUpdateOptions = DbUpdateOptions.All | DbUpdateOptions.ThrowExceptions
			}
		};
	}

}