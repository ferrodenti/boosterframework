using System;
using System.Collections.Generic;
using System.Linq;
using Booster.Console;
using Booster.Cron;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Cron;

[TestClass]
public class CronExpressionsTests
{
	const int _scheduleLength = 256;

	static void CheckSchedule(CronExpression cronExpression, DateTime start, IList<DateTime> expected)
	{
		ConsoleHelper.WriteLine($"CRON: {cronExpression}:");
		var result = cronExpression.CreateSchedule(start).Take(expected.Count).ToArray();
		var success = result.Length == expected.Count;

		ConsoleHelper.WriteLine($"	Actual({result.Length})			Expected({expected.Count})");

		for (var i = 0; i < Math.Min(result.Length, expected.Count); i++)
		{
			var act = result[i];
			var exp = expected[i];
			if (act != exp)
				success = false;
				
			ConsoleHelper.WriteLine($"{result[i]:dd.MM.yy HH:mm:ss}	{expected[i]:dd.MM.yy HH:mm:ss} {(act != exp ? " -- ERROR!" : "")}");
		}

		Assert.IsTrue(success);
	}
		
	[TestMethod]
	public void InvalidCronExpressionTest()
	{
		var expr = new CronExpression("0 0 1sdf2 * * ?"); // Invalid expression
		Assert.IsFalse(expr.IsValid);
		Assert.AreEqual(expr.GetNext(), DateTime.MaxValue);
	}

	[TestMethod]
	public void CronTest1()
	{
		var start = DateTime.Now;
		var expr = new CronExpression("0 0 12"); // Fire at 12pm (noon) every day
		var expected = new List<DateTime>();
		for (var dt = new DateTime(start.Year, start.Month, start.Day, 12, 0, 0); expected.Count < _scheduleLength; dt = dt.AddDays(1))
			if (dt > start)
				expected.Add(dt);
			
		CheckSchedule(expr, start, expected);
	}
		
	[TestMethod]
	public void CronTest2()
	{
		var start = DateTime.Now;
		var expr = new CronExpression("0 15 10 ? * *"); // Fire at 10:15am every day
		var expr2 = new CronExpression("0 15 10 * * ?"); // Fire at 10:15am every day
		var expr3 = new CronExpression("0 15 10 * * ? *"); // Fire at 10:15am every day
			
		var expected = new List<DateTime>();
		for (var dt = new DateTime(start.Year, start.Month, start.Day, 10, 15, 0); expected.Count<_scheduleLength; dt = dt.AddDays(1))
			if(dt > start)
				expected.Add(dt);

		CheckSchedule(expr, start, expected);
		CheckSchedule(expr2, start, expected);
		CheckSchedule(expr3, start, expected);
	}
		
	[TestMethod]
	public void CronTest3()
	{
		var start = DateTime.Now;
		var expr = new CronExpression($"0 15 10 * * ? {start.Year}"); // Fire at 10:15am every day during current year
		var expected = new List<DateTime>();
		for (var dt = new DateTime(start.Year, start.Month, start.Day, 10, 15, 0); expected.Count<_scheduleLength && dt.Year == start.Year; dt = dt.AddDays(1))
			if(dt > start)
				expected.Add(dt);

		CheckSchedule(expr, start, expected);
	}
		
	[TestMethod]
	public void CronTest4()
	{
		var start = DateTime.Now;
		var expr = new CronExpression("0 * 14"); // Fire every minute starting at 2pm and ending at 2:59pm, every day
		var expected = new List<DateTime>();
			
		for (var dt = new DateTime(start.Year, start.Month, start.Day, 14, 0, 0); expected.Count<_scheduleLength; dt = dt.AddDays(1))
		for (var i = 0; i < 60 && expected.Count < _scheduleLength; ++i)
		{
			var d = new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, i, 0);
			if (d > start)
				expected.Add(d);
		}

		CheckSchedule(expr, start, expected);
	}
		
	[TestMethod]
	public void CronTest5()
	{
		var start = DateTime.Now;
		var expr = new CronExpression("0 0/5 14"); // Fire every 5 minutes starting at 2pm and ending at 2:55pm, every day
		var expected = new List<DateTime>();
			
		for (var dt = new DateTime(start.Year, start.Month, start.Day, 14, 0, 0); expected.Count<_scheduleLength; dt = dt.AddDays(1))
		for (var i = 0; i < 60 && expected.Count < _scheduleLength; i+=5)
		{
			var d = new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, i, 0);
			if (d > start)
				expected.Add(d);
		}

		CheckSchedule(expr, start, expected);
	}
		
	[TestMethod]
	public void CronTest6()
	{
		var start = DateTime.Now;
		var expr = new CronExpression("0 0/5 14,18"); // Fire every 5 minutes starting at 2pm and ending at 2:55pm, AND fire every 5 minutes starting at 6pm and ending at 6:55pm, every day
		var expected = new List<DateTime>();

		for (var dt = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0); expected.Count < _scheduleLength; dt = dt.AddDays(1))
		{
			for (var i = 0; i < 60 && expected.Count < _scheduleLength; i += 5)
			{
				var d = new DateTime(dt.Year, dt.Month, dt.Day, 14, i, 0);
				if (d > start)
					expected.Add(d);
			}
			for (var i = 0; i < 60 && expected.Count < _scheduleLength; i += 5)
			{
				var d = new DateTime(dt.Year, dt.Month, dt.Day, 18, i, 0);
				if (d > start)
					expected.Add(d);
			}
		}

		CheckSchedule(expr, start, expected);
	}
		
	[TestMethod]
	public void CronTest7()
	{
		var start = DateTime.Now;
		var expr = new CronExpression("0 0-5 14"); // Fire every minute starting at 2pm and ending at 2:05pm, every day
		var expected = new List<DateTime>();

		for (var dt = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0); expected.Count < _scheduleLength; dt = dt.AddDays(1))
		for (var i = 0; i < 6 && expected.Count < _scheduleLength; i++)
		{
			var d = new DateTime(dt.Year, dt.Month, dt.Day, 14, i, 0);
			if (d > start)
				expected.Add(d);
		}

		CheckSchedule(expr, start, expected);
	}
		
	[TestMethod]
	public void CronTest8()
	{
		var start = DateTime.Now;
		var expr = new CronExpression("0 10,44 14 ? 3 WED"); // Fire at 2:10pm and at 2:44pm every Wednesday in the month of March.
		var expected = new List<DateTime>();

		for (var dt = new DateTime(start.Year, 3, 1, 0, 0, 0); expected.Count < _scheduleLength; dt = dt.AddYears(1))
		for (var dt2 = dt; dt2.Month == 3 && expected.Count < _scheduleLength; dt2 = dt2.AddDays(1))
			if (dt2.DayOfWeek == DayOfWeek.Wednesday)
			{
				var d = new DateTime(dt2.Year, dt2.Month, dt2.Day, 14, 10, 0);
				if (d > start && expected.Count < _scheduleLength)
					expected.Add(d);
				d = new DateTime(dt2.Year, dt2.Month, dt2.Day, 14, 44, 0);
				if (d > start && expected.Count < _scheduleLength)
					expected.Add(d);
			}

		CheckSchedule(expr, start, expected);
	}
		
	[TestMethod]
	public void CronTest9()
	{
		var start = DateTime.Now;
		var expr = new CronExpression("0 15 10 ? * MON-FRI"); // Fire at 10:15am every Monday, Tuesday, Wednesday, Thursday and Friday
		var expected = new List<DateTime>();

		for (var dt = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0); expected.Count < _scheduleLength; dt = dt.AddDays(1))
			if (dt.DayOfWeek != DayOfWeek.Sunday && dt.DayOfWeek != DayOfWeek.Saturday)
			{
				var d = new DateTime(dt.Year, dt.Month, dt.Day, 10, 15, 0);
				if (d > start)
					expected.Add(d);
			}

		CheckSchedule(expr, start, expected);
	}

	[TestMethod]
	public void CronTest10()
	{
		var start = DateTime.Now;
		var expr = new CronExpression("0 15 10 15"); // Fire at 10:15am on the 15th day of every month
		var expected = new List<DateTime>();

		for (var dt = new DateTime(start.Year, start.Month, 15, 10, 15, 0); expected.Count < _scheduleLength; dt = dt.AddMonths(1))
			if (dt > start)
				expected.Add(dt);

		CheckSchedule(expr, start, expected);
	}
		
	[TestMethod]
	public void CronTest11()
	{
		var start = DateTime.Now;
		var expr = new CronExpression("0 15 10 L"); // Fire at 10:15am on the last day of every month
		var expected = new List<DateTime>();

		for (var dt = new DateTime(start.Year, start.Month, 1, 0, 0, 0); expected.Count < _scheduleLength; dt = dt.AddMonths(1))
		{
			var d = new DateTime(dt.Year, dt.Month, DateTime.DaysInMonth(dt.Year, dt.Month), 10, 15, 0);
			if (d > start)
				expected.Add(d);
		}

		CheckSchedule(expr, start, expected);
	}
		
	[TestMethod]
	public void CronTest12()
	{
		var start = DateTime.Now;
		var expr = new CronExpression("0 15 10 L-2"); // Fire at 10:15am on the 2nd-to-last last day of every month
		var expected = new List<DateTime>();

		for (var dt = new DateTime(start.Year, start.Month, 1, 0, 0, 0); expected.Count < _scheduleLength; dt = dt.AddMonths(1))
		{
			var d = new DateTime(dt.Year, dt.Month, DateTime.DaysInMonth(dt.Year, dt.Month)-2, 10, 15, 0);
			if (d > start)
				expected.Add(d);
		}

		CheckSchedule(expr, start, expected);
	}
		
	[TestMethod]
	public void CronTest13()
	{
		var start = DateTime.Now;
		var expr = new CronExpression("0 15 10 ? * 6L"); // Fire at 10:15am on the last Friday of every month
		var expected = new List<DateTime>();

		for (var dt = new DateTime(start.Year, start.Month, 1, 0, 0, 0); expected.Count < _scheduleLength; dt = dt.AddMonths(1))
		{
			var d = new DateTime(dt.Year, dt.Month, DateTime.DaysInMonth(dt.Year, dt.Month), 10, 15, 0);
			while (d.DayOfWeek != DayOfWeek.Friday)
				d = d.AddDays(-1);
				
			if (d > start)
				expected.Add(d);
		}

		CheckSchedule(expr, start, expected);
	}
		
	[TestMethod]
	public void CronTest14()
	{
		var start = DateTime.Now;
		var expr = new CronExpression($"0 15 10 ? * 6L {start.Year+1}-{start.Year+2}"); // Fire at 10:15am on every last friday of every month during the years
		var expected = new List<DateTime>();

		for (var dt = new DateTime(start.Year+1, 1, 1, 0, 0, 0); dt.Year <= start.Year + 2 && expected.Count < _scheduleLength; dt = dt.AddMonths(1))
		{
			var d = new DateTime(dt.Year, dt.Month, DateTime.DaysInMonth(dt.Year, dt.Month), 10, 15, 0);
			while (d.DayOfWeek != DayOfWeek.Friday)
				d = d.AddDays(-1);
				
			if (d > start)
				expected.Add(d);
		}

		CheckSchedule(expr, start, expected);
	}
		
	[TestMethod]
	public void CronTest15()
	{
		var start = DateTime.Now;
		var expr = new CronExpression("0 15 10 ? * 6#3"); // Fire at 10:15am on the third Friday of every month
		var expected = new List<DateTime>();

		for (var dt = new DateTime(start.Year, start.Month, 1, 0, 0, 0); expected.Count < _scheduleLength; dt = dt.AddMonths(1))
		{
			var d = new DateTime(dt.Year, dt.Month, 1, 10, 15, 0);
			for(var i=0; d.Month == dt.Month; d = d.AddDays(1))
				if (d.DayOfWeek == DayOfWeek.Friday && ++i == 3)
				{
					if (d > start)
						expected.Add(d);
					break;
				}
		}

		CheckSchedule(expr, start, expected);
	}

	[TestMethod]
	public void CronTest16()
	{
		var start = DateTime.Now;
		var expr = new CronExpression("0 0 12 1/5"); // Fire at 12pm (noon) every 5 days every month, starting on the first day of the month.
		var expected = new List<DateTime>();

		for (var dt = new DateTime(start.Year, start.Month, 1, 12, 0, 0); expected.Count < _scheduleLength; dt = dt.AddMonths(1))
		for (var dt2 = dt; dt2.Month == dt.Month; dt2 = dt2.AddDays(5))
			if (dt2 > start)
				expected.Add(dt2);

		CheckSchedule(expr, start, expected);
	}
		
	[TestMethod]
	public void CronTest17()
	{
		var start = DateTime.Now;
		var expr = new CronExpression("0 0 12 15W"); // Fire at 12pm (noon) at nearest weekday to 15 day of every month.
		var expected = new List<DateTime>();

		for (var dt = new DateTime(start.Year, start.Month, 1, 0, 0, 0); expected.Count < _scheduleLength; dt = dt.AddMonths(1))
		for (var i = 0; i < 15; i++)
		{
			var dt2 = new DateTime(dt.Year, dt.Month, 15 - i, 12, 0, 0);
			if (dt2.DayOfWeek != DayOfWeek.Saturday && dt2.DayOfWeek != DayOfWeek.Sunday)
			{
				if (dt2 > start)
					expected.Add(dt2);
				break;
			}

			dt2 = new DateTime(dt.Year, dt.Month, 15 + i, 12, 0, 0);
			if (dt2.DayOfWeek != DayOfWeek.Saturday && dt2.DayOfWeek != DayOfWeek.Sunday)
			{
				if (dt2 > start)
					expected.Add(dt2);
				break;
			}

		}

		CheckSchedule(expr, start, expected);
	}
		
	[TestMethod]
	public void CronTest18()
	{
		var start = DateTime.Now;
		var expr = new CronExpression("0 11 11 11 11"); // Fire every November 11th at 11:11am.
		var expected = new List<DateTime>();

		for (var dt = new DateTime(start.Year, 11, 11, 11, 11, 0); expected.Count < _scheduleLength; dt = dt.AddYears(1))
			if (dt > start)
				expected.Add(dt);

		CheckSchedule(expr, start, expected);
	}
		
	[TestMethod]
	public void CronTest19()
	{
		var start = DateTime.Now;
		var expr = new CronExpression("0 15 10 LW"); // Fire at 10:15am on last weekday of every month
		var expected = new List<DateTime>();

		for (var dt = new DateTime(start.Year, start.Month, 1, 0, 0, 0); expected.Count < _scheduleLength; dt = dt.AddMonths(1))
		{
			var d = new DateTime(dt.Year, dt.Month, DateTime.DaysInMonth(dt.Year, dt.Month), 10, 15, 0);
			while (d.DayOfWeek == DayOfWeek.Sunday || d.DayOfWeek == DayOfWeek.Saturday)
				d = d.AddDays(-1);
				
			if (d > start)
				expected.Add(d);
		}

		CheckSchedule(expr, start, expected);
	}
		
	[TestMethod]
	public void CronTest20()
	{
		var start = DateTime.Now;
		var expr = new CronExpression($"0 15 10 6L * ? {start.Year+1}-{start.Year+2}"); // Fire at 10:15am on every last friday of every month during the years
		var expected = new List<DateTime>();

		for (var dt = new DateTime(start.Year+1, 1, 1, 0, 0, 0); dt.Year <= start.Year + 2 && expected.Count < _scheduleLength; dt = dt.AddMonths(1))
		{
			var d = new DateTime(dt.Year, dt.Month, DateTime.DaysInMonth(dt.Year, dt.Month), 10, 15, 0);
			while (d.DayOfWeek != DayOfWeek.Friday)
				d = d.AddDays(-1);
				
			if (d > start)
				expected.Add(d);
		}

		CheckSchedule(expr, start, expected);
	}

	[TestMethod]
	public void CronTest21()
	{
		var start = DateTime.Now;
		var expr = new CronExpression("0"); // Fire every minute
		var expected = new List<DateTime>();

		for (var dt = new DateTime(start.Year, start.Month, start.Day, start.Hour, start.Minute, 10).AddSeconds(-70); expected.Count < _scheduleLength; dt = dt.AddMinutes(1))
			if (dt > start)
				expected.Add(dt);

		CheckSchedule(expr, start, expected);
	}
}