//using System;
//using System.Linq;
//using System.Net;
//using System.Text;
//using System.Threading;
//using Booster.Net;
//using Microsoft.VisualStudio.TestTools.UnitTesting;

//namespace Booster.UnitTests.Net
//{
//	[TestClass]
//	public class AsyncTcpTests
//	{
//		[TestMethod]
//		public void GeneralBehaviour()
//		{
//			byte[] dataSend = new byte[0];//Encoding.UTF8.GetBytes("Test!");
//			byte[] dataReceived = new byte[0];
//			byte[] dataAnswer = Encoding.UTF8.GetBytes("Answer!");
//			byte[] dataAnswered	 = new byte[0];

//			AsyncTcpServer server = new AsyncTcpServer(IPAddress.Parse("127.0.0.1"), 5555);
//			AsyncTcpClient client = new AsyncTcpClient("127.0.0.1", 5555);
//			ManualResetEventSlim received = new ManualResetEventSlim(false);
//			ManualResetEventSlim answered = new ManualResetEventSlim(false);

			
//			server.DataReceived += (sender, args) =>
//			{
//				dataReceived = args.Request;
//				args.Response = dataAnswer;
//				received.Set();
//			};

//			server.Error += (sender, args) =>
//			{

//			};
//			server.Start();


//			client.DataReceived += (sender, args) =>
//			{
//				dataAnswered = args.Data;
//				answered.Set();
//			};

//			client.Error += (sender, args) =>
//			{
//				((AsyncTcpClient) sender).Disconnect();
//				try
//				{
//					((AsyncTcpClient) sender).Connect();
//				}
//				catch (Exception)
//				{
//				}
//			};

//			for (int i = 0; i < 10; i++)
//			{
//				client.Connect();


//				if (i%3 == 2)
//				{
//					server.Stop();
//					server.Start();
//				}
//				else 
//				for (int j = 0; j < 10; j++)
//				{
//					dataReceived = null;
//					dataAnswered = null;

//					received.Reset();
//					answered.Reset();

//					client.Send(dataSend);

//					if (!received.Wait(2000))
//					    Assert.Fail("Data was not received!");

//					if (!answered.Wait(2000))
//					    Assert.Fail("Answer was not received!");

//					Assert.IsTrue(dataSend.SequenceEqual(dataReceived));
//					Assert.IsTrue(dataAnswer.SequenceEqual(dataAnswered));
//				}
//				client.Disconnect();
//			}
//			server.Stop();
//		}


//		[TestMethod]
//		public void TestReconnect()
//		{
//			byte[] dataSend = Encoding.UTF8.GetBytes("Test!");
//			byte[] dataReceived = new byte[0];

//			AsyncTcpServer server = new AsyncTcpServer(IPAddress.Parse("127.0.0.1"), 5555);
//			AsyncTcpClient client = new AsyncTcpClient("127.0.0.1", 5555);
//			ManualResetEventSlim received = new ManualResetEventSlim(false);

//			server.DataReceived += (sender, args) =>
//			{
//				dataReceived = args.Request;
//				received.Set();
//			};

//			server.Error += (sender, args) =>
//			{

//			};

//			server.Start();

//			for (int i = 0; i < 10; i++)
//			{
//				server.Stop();
//				try
//				{
//					client.Connect();
//				}
//				catch (Exception)
//				{
//				}
//				server.Start();
//			}
//			server.Stop();
//		}
//	}
//}