using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Trash;

[TestClass]
public class AsyncLocalBug
{
	readonly AsyncLocal<string> _asyncLocal = new();

	public string MyLocal
	{
		get => _asyncLocal.Value;
		set => _asyncLocal.Value = value;
	}

	public void SetSync(string value)
	{
		MyLocal = value;
		Thread.Sleep(10);
	}

	public async Task SetAsyncNaive(string value)
	{
		MyLocal = value;
		await Task.Delay(10);
	}

	public Task SetAsyncFixed(string value)
	{
		static async Task AsyncRemainder()
			=> await Task.Delay(10);

		MyLocal = value;
		return AsyncRemainder();
	}

	[TestMethod]
	public async Task AsyncLocalBugTest()
	{
		Assert.AreEqual(null, MyLocal);
		MyLocal = "It is work";
		Assert.AreEqual("It is work", MyLocal);

		SetSync("So as this");
		Assert.AreEqual("So as this", MyLocal);

		await SetAsyncNaive("Not working! WTF?");
		Assert.AreNotEqual("Not working WTF?", MyLocal);

		await SetAsyncFixed("It is fixed!!!");
		Assert.AreEqual("It is fixed!!!", MyLocal);
	}
}