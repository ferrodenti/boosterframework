using Booster.Console;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests.Trash;

[TestClass]
public class TrashTests
{
	[TestMethod]
	public void MinPosTest()
	{
		int MinPos1(int a, int b, int step)
		{
			var i = a;

			while (i < b)
				i += step;

			return i;
		}

		int MinPos2(int a, int b, int step)
		{
			if (b <= a)
				return a;

			return a + (b - a - 1) / step * step + step;
		}

		for (var i = 0; i < 10000; ++i)
		{
			var a = Rnd.Int(0, 10);
			var b = Rnd.Int(a, 15);
			var s = Rnd.Int(1, 5);

			var r1 = MinPos1(a, b, s);
			var r2 = MinPos2(a, b, s);
			if (r1 != r2)
			{
				ConsoleHelper.WriteLine($"a={a} b={b} s={s} result = {MinPos1(a, b, s)}");
				ConsoleHelper.WriteLine($"a={a} b={b} s={s} result2 = {MinPos2(a, b, s)}");
				Assert.AreEqual(r1, r2);
			}
		}
	}
}