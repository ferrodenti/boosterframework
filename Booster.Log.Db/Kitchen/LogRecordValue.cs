using Booster.Orm;

namespace Booster.Log.Db.Kitchen;

[Db("LogRecordValues")]
[DbIndex(nameof(RecordId), nameof(ValueId))]
public class LogRecordValue : IndexedEntity<LogRecordValue>
{
	[DbIndex] public One<LogRecord> RecordId = new();
	public LogRecord? Record
	{
		get => RecordId.Entity;
		set => RecordId.Entity = value;
	}

	[Db] public One<LogParameterValue> ValueId = new();
	public LogParameterValue? Value
	{
		get => ValueId.Entity;
		set => ValueId.Entity = value;
	}

	[Migration(MigrationOptions.Initialized)]
	protected static void OnInitialized(RepoInitContext ctx)
		=> DbLog.CurrentInitializingLog?.RepoInitialized(ctx);
}