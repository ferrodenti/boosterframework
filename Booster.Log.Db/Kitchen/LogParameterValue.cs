using Booster.Orm;

namespace Booster.Log.Db.Kitchen;

[Db("LogParameterValues")]
[DbIndex(nameof(ParameterId), nameof(Value))]
public class LogParameterValue : IndexedEntity<LogParameterValue>
{
	[Db] public One<LogParameter> ParameterId = new();
	public LogParameter? Parameter
	{
		get => ParameterId.Entity;
		set => ParameterId.Entity = value;
	}
	[DbIndex] public string? Value;

	[Migration(MigrationOptions.Initialized)]
	protected static void OnInitialized(RepoInitContext ctx)
		=> DbLog.CurrentInitializingLog?.RepoInitialized(ctx);
}