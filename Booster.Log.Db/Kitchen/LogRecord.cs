using Booster.Orm;

namespace Booster.Log.Db.Kitchen;

[Db("LogRecords")]
public class LogRecord : IndexedEntity<LogRecord>
{
	[DbIndex] public DateTime TimeStamp;

	[Migration(MigrationOptions.Initialized)]
	protected static void OnInitialized(RepoInitContext ctx)
		=> DbLog.CurrentInitializingLog?.RepoInitialized(ctx);
}