using Booster.Orm;

namespace Booster.Log.Db.Kitchen;

[DbDict("LogParameters"), DbDefaultSort("order, id")]
public class LogParameter : IndexedEntity<LogParameter>
{
	[Db] public int Order;
	[DbIndex] public string? Name;

	[Migration(MigrationOptions.Initialized)]
	protected static void OnInitialized(RepoInitContext ctx)
		=> DbLog.CurrentInitializingLog?.RepoInitialized(ctx);
}