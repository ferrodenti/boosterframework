using Booster.Collections;
using Booster.DI;
using Booster.Log.Db.Kitchen;
using Booster.Orm;
using Booster.Orm.Interfaces;
using Booster.Reflection;

namespace Booster.Log.Db;

public class DbLog : Log
{
	readonly object _lock = new();
	readonly IOrm _orm;
	volatile bool _initialized;

	readonly Dictionary<TypeEx, string> _tableNames = new();

	static readonly object _initializationlock = new();
	internal static volatile DbLog? CurrentInitializingLog;

	string InstanceContext { get; }

	public DbLog(IOrm orm, object? parameters = null) 
		: base(parameters)
	{
		DateHeaders = false;
		Async = true;
		InstanceContext = InstanceFactory.CurrentContext;
		_orm = orm;
	}

	internal void RepoInitialized(RepoInitContext ctx)
	{
		if (_tableNames.TryGetValue(ctx.EntityType, out var str))
			ctx.TableName = str;
	}

	public DbLog TableName<T>(string name)
	{
		_tableNames[typeof (T)] = name;
		return this;
	}

	public DbLog Init()
	{
		InstanceFactory.Register(_orm);

		_orm.Ping();

		lock (_initializationlock)
		{
			CurrentInitializingLog = this;

			_orm.RegisterRepos(
				new[]
				{
					typeof(LogRecord),
					typeof(LogParameter),
					typeof(LogParameterValue),
					typeof(LogRecordValue)
				});

			CurrentInitializingLog = null;
		}

		//_orm.WaitForEventsHandled();

		_initialized = true;

		return this;
	}

	public override IDisposable WriteIntern(ILogMessage message, IDisposable? disposable)
	{
		using (InstanceFactory.PushContext(InstanceContext))
		{
			if (!_initialized)
				Init();

			var conn = disposable as IConnection;

			try
			{
				conn ??= InstanceFactory.Get<IConnection>();

				var param = message.Parameters;
				var values = new List<LogParameterValue>(param.Count + 7);

				lock (_lock)
				{
					if (message.Level > 0)
						values.Add(GetOrAddLogParameterValue("Level", message.Level.ToString()));

					if (message.Name.IsSome())
						values.Add(GetOrAddLogParameterValue("Name", message.Name));
						
					if (message.Class.IsSome())
						values.Add(GetOrAddLogParameterValue("Class", message.Class));

					if (message.Member.IsSome())
						values.Add(GetOrAddLogParameterValue("Member", message.Member));

					if (message.Source.IsSome())
						values.Add(GetOrAddLogParameterValue("Source", message.Source));

					if (message.Line > 0)
						values.Add(GetOrAddLogParameterValue("Line", message.Line.ToString()));

					if (message.Message.IsSome())
						values.Add(GetOrAddLogParameterValue("Message", message.Message));

					values.AddRange(param.Select(pair => GetOrAddLogParameterValue(pair.Key, pair.Value)));

					if (message.Exception != null)
						values.Add(GetOrAddLogParameterValue("Exception", MessageFormatter.FormatException(message)));
				}

				using var trans = conn.BeginTransaction();
				var rec = new LogRecord {TimeStamp = message.TimeStamp};
				rec.Insert();

				foreach (var val in values)
					new LogRecordValue {RecordId = rec.Id, ValueId = val.Id}.Insert();

				trans.Commit();
			}
			catch (Exception ex)
			{
				Utils.Nop(ex);
			}

			return conn!;
		}
	}

	readonly CacheDictionary<string, LogParameter> _cache =
		new(StringComparer.OrdinalIgnoreCase);

	protected LogParameterValue GetOrAddLogParameterValue(string key, string? value)
	{
		var param = _cache.GetOrAdd(key, _ => LogParameter.Load($"name={key:@}") ?? new LogParameter {Name = key});

		if (param.Id <= 0)
			param.Insert();
		else if (LogParameterValue.Load($"parameter_id={param.Id:@} and value={value:@}") is { } result)
			return result;

		var newValue = new LogParameterValue {ParameterId = param.Id, Value = value};
		newValue.Insert();
		return newValue;
	}
}
/*
select id, time_stamp, array_to_string(array(
	select (
	select name from logparameters where id=parameter_id) || '=' || value 
		from logparametervalues where id in (select value_id from logrecordvalues where record_id=r.id)), ',') 
	from logrecords r order by id;
*/

