using System;

namespace Booster.UnitTests;

public class AssertException : Exception
{
	public AssertException()
	{
	}
	public AssertException(string message) : base(message)
	{
	}
}
