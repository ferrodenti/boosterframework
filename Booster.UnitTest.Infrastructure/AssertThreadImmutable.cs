using System;
using System.Linq.Expressions;
using JetBrains.Annotations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Booster.UnitTests;

public static class AssertThreadImmutable
{
	public static AssertThreadImmutable<T> From<T, TTarget>(TTarget target, Expression<Func<TTarget, T>> accessor)
	{
		var member = accessor.ParseMemberRef(false);
		return new AssertThreadImmutable<T>(() => (T)member.GetValue(target), value => member.SetValue(target, value));
	}

	public static AssertThreadImmutable<T> From<T>(Func<T> getter, Action<T> setter)
		=> new(getter, setter);
}

[PublicAPI]
public class AssertThreadImmutable<T>
{
	readonly Func<T> _getter;
	readonly Action<T> _setter;

	T _expectedValue;

	public T Value
	{
		get => Get();
		set => Set(value);
	}

	public AssertThreadImmutable(Func<T> getter, Action<T> setter)
	{
		_getter = getter;
		_expectedValue = getter();
		_setter = setter;
	}

	public void AssertIsImmutable()
	{
		var actual = _getter();

		if (!Equals(_expectedValue, actual))
			Utils.Nop();

		Assert.AreEqual(_expectedValue, actual);
	}

	public T Set(T value, bool assertIsImmutable = true)
	{
		var result = _expectedValue;

		if (assertIsImmutable)
			AssertIsImmutable();

		lock (_setter)
		{
			_expectedValue = value;
			_setter(value);
		}

		if (assertIsImmutable)
			AssertIsImmutable();

		return result;
	}

	public T Get(bool assertIsImmutable = true)
	{
		var result = _getter();

		if(assertIsImmutable)
			Assert.AreEqual(_expectedValue, result);

		return result;
	}
}