using System;
using System.Linq;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Booster.Localization;
using JetBrains.Annotations;

#nullable enable

namespace Booster.UnitTests;

[PublicAPI]
public class SpeedComparer
{
	protected class Competitor
	{
		public string Name { get; }

		readonly Stopwatch _sw = new();

		public Competitor(string name)
			=> Name = name;

		public void Start()
		{
			Iters ++;
			_sw.Start();
		}

		public void Stop()
			=> _sw.Stop();

		public TimeSpan TotalTime => _sw.Elapsed;

		public TimeSpan MeanTime
		{
			get
			{
				if (Iters == 0)
					return TimeSpan.MinValue;

				return new TimeSpan(_sw.Elapsed.Ticks/Iters);
			}
		}

		public int Iters { get; protected set; }

		public override string ToString()
			=> ToString(true, true, true);

		public string ToString(bool total, bool mean, bool iters)
		{
			var str = new EnumBuilder(", ");

			str.Append($"{Name}: ");
			str.ResetComma();
				
			if (total) str.Append($"total={TotalTime.ToString(TimeSpanPart.Minutes)}");
			if (mean) str.Append($"mean={MeanTime.ToString(TimeSpanPart.Minutes)}");
			if (iters) str.Append($"iters={Iters}");

			return str;
		}
	}

	readonly ConcurrentDictionary<object, Competitor> _competitors = new();

	readonly List<Competitor> _competitorsList = new();
	Competitor? _workingCompetitor;

	int _currentCompetitorNo = -1;

	public SpeedComparer() { }
	public SpeedComparer(params  object [] competitors)
	{
		foreach (var comp in competitors)
			AddÑompetitor(comp);
	}


	public void AddÑompetitor(string name)
	{
		var comp = new Competitor(name);
		_competitors[name] = comp;
		_competitorsList.Add(comp);
	}

	public void AddÑompetitor(string name, object obj)
	{
		var comp = new Competitor(name);
		_competitors[obj] = comp;
		_competitorsList.Add(comp);
	}

	public void AddÑompetitor(object obj)
	{
		var comp = new Competitor(obj.ToString());
		_competitors[obj] = comp;
		_competitorsList.Add(comp);
	}

	public void Start()
	{
		_currentCompetitorNo = ++_currentCompetitorNo%_competitors.Count;
		Start(_currentCompetitorNo);
	}

	public void Stop()
	{
		if (_workingCompetitor != null)
			_workingCompetitor.Stop();
		else
			Stop(_currentCompetitorNo);
	}

	public void Start(int competitorNo)
	{
		_workingCompetitor = _competitorsList[competitorNo];
		_workingCompetitor.Start();
	}

	public void Stop(int competitorNo)
		=> _competitorsList[competitorNo].Stop();

	public void Start(object obj)
	{
		_workingCompetitor = _competitors[obj];
		_workingCompetitor.Start();
	}

	public void Stop(object obj)
		=> _competitors[obj].Stop();

	public override string ToString()
		=> ToString(true);

	public string ToString(bool sorted)
	{
		var str = new StringBuilder();

		var ie = (sorted ? (IEnumerable<Competitor>) _competitorsList.OrderBy(c => c, new Judge()) : _competitorsList).ToArray();

		var firstIters = ie.FirstOrDefault().With(c => c.Iters);
		var noIters = ie.All(c => c.Iters == firstIters);

		if (noIters)
			str.AppendLine($"Iters = {firstIters}");

		var i = 0;
		foreach (var comp in ie)
			str.AppendLine((sorted ? $"{++i}) " : "") + comp.ToString(true, true, !noIters));

		return str.ToString();
	}

	class Judge : IComparer<Competitor>
	{
		public int Compare(Competitor? x, Competitor? y)
		{
			// ReSharper disable InvocationIsSkipped
			System.Diagnostics.Debug.Assert(x != null, $"{nameof(x)} != null");
			System.Diagnostics.Debug.Assert(y != null, $"{nameof(y)} != null");
			// ReSharper restore InvocationIsSkipped

			if (x!.Iters == y!.Iters)
				return x.TotalTime.CompareTo(y.TotalTime);

			return x.MeanTime.CompareTo(y.MeanTime);
		}
	}

	public void TraceResults()
	{
		lock (Utils.TraceLock)
			Trace.WriteLine(ToString());
	}

	public void WriteConsoleResults()
		=> Console.WriteLine(ToString());
}