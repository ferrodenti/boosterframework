using System;
using System.Diagnostics;
using System.Threading;
using JetBrains.Annotations;

#nullable enable

namespace Booster.UnitTests;

[PublicAPI]
public class RandomValue : IDisposable
{
	readonly double _min;
	readonly double _max = 10;
	double _prev;
	double _next;
	int _speed = 1000;
	readonly int _minSpeed = 500;
	readonly int _maxSpeed = 5000;
	readonly Stopwatch _sw = new();

	public RandomValue()
		=> NextVariable();
		
	public RandomValue(double min, double max, int minSpeed = 5000, int maxSpeed = 10000)
	{
		_min = min;
		_max = max;
		_minSpeed = minSpeed;
		_maxSpeed = maxSpeed;
		_next = Rnd.Double(_min, _max);
		NextVariable();
	}

	public double Value
	{
		get
		{
			//return _next;

			var res = _prev + (_next - _prev) * _sw.ElapsedMilliseconds / _speed;
			if (res < _min)
				return res;
			if (res > _max)
				return _max;

			return res;
		}
	}

	Timer? _timer;

	protected void NextVariable()
	{
		_sw.Restart();
		_prev = _next;
		_speed = Rnd.Int(_minSpeed, _maxSpeed);
		_next = Rnd.Double(_min, _max);

		_timer = new Timer(_ => NextVariable(), null, _speed, 0);
	}

	public void Dispose()
	{
		if (_timer != null)
		{
			_timer.Dispose();
			_timer = null;
		}
	}

	public static implicit operator double(RandomValue value)
		=> value.Value;

	public static implicit operator int(RandomValue value)
	{
		var res = (int) value.Value;
			
		if (res < value._min)
			return (int)(value._min + 1);
			
		if (res > value._max)
			return (int)(value._max - 1);
		return res;
	}

	public static RandomValue CreateInRange(double min, double max, double variation)
	{
		var half = variation / 2;
		var mid = Rnd.Double(min + half, max - half);
		return new RandomValue(mid - half, mid + half);
	}
}