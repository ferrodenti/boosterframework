﻿using System;
using System.Diagnostics;

#if !NETSTANDARD
using System.CodeDom;
using System.CodeDom.Compiler;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
#else
using Microsoft.VisualStudio.TestTools.UnitTesting;
#endif

namespace Booster.UnitTests;

public class StringCompareHelper
{
	public static readonly StringCompareHelper Instance = new();

	public int MaxStringLength = 160;
	public bool EscapeStrings = true;
	public bool IgnoreLineEndings = true;
	public bool TabsToSpaces = true;

	public void AssertAreEqual(string expected, string actual)
	{
		if (!Instance.IsEqual(expected, actual, out var msg))
		{
			Trace.WriteLine(msg);
			Assert.Fail(msg);
		}
	}

	public bool IsEqual(string string1, string string2, out string message, string name1 = "Expected ", string name2 = "  Actual ")
	{
		if (string1 == string2)
		{
			message = default!;
			return true;
		}

		if (IgnoreLineEndings)
		{
			string1 = string1.Replace("\r\n", "\n");
			string2 = string2.Replace("\r\n", "\n");
		}

		if (TabsToSpaces)
		{
			string1 = string1.Replace("\t", "    ");
			string2 = string2.Replace("\t", "    ");
		}

		string RenderDiffirence(int position)
		{
			var a = GetHighlightedLine(name1, string1, position);
			var b = GetHighlightedLine(name2, string2, position);
			return $"At pos {position}: \n{a} ≠ \n{b}";
		}

		var equal = true;
		var str = new EnumBuilder(", ", empty: "Equal!");
		var lenDif = false;

		if (string1.Length != string2.Length)
		{
			str.Append($"Different lengths: {string1.Length}≠{string2.Length}");
			lenDif = true;
			equal = false;
		}

		var len = Math.Min(string1.Length, string2.Length);

		int i;
		for (i = 0; i < len; i++)
			if (string1[i] != string2[i])
			{
				str.Append(RenderDiffirence(i));

				equal = false;
				break;
			}

		if (i == len && lenDif)
			str.Append(RenderDiffirence(i));

		message = str;
		return equal;
	}

#if NETSTANDARD
	static string EscapeString(string value)
		=> Microsoft.CodeAnalysis.CSharp.SymbolDisplay.FormatLiteral(value, false);
#else
	static string EscapeString(string value)
	{
		using var writer = new StringWriter();
		using var provider = CodeDomProvider.CreateProvider("CSharp");
		provider.GenerateCodeFromExpression(new CodePrimitiveExpression(value), writer, null);
		return writer.ToString();
	}
#endif

	static int GetLineNumber(string str, int pos)
	{
		var line = 1;
		var j = 0;
		do
		{
			var i = str.IndexOf('\n', j);
			if (i < 0 || i >= pos)
				return line;

			j = i + 1;
			++line;

		} while (true);
	}

	string GetHighlightedLine(string name, string str, int pos)
	{
		int begin, end;
		var min = pos - MaxStringLength / 2;
		var line = GetLineNumber(str, pos);

		var prefix = $"{name} {$"({line})".PadLeft(5, ' ')}: ";

		for (begin = pos - 1; begin >= 0; --begin)
		{
			if (begin < min)
			{
				prefix += "...";
				break;
			}

			if (str[begin] == '\n')
			{
				++begin;
				break;
			}
		}

		string suffix;
		var max = pos + MaxStringLength / 2;

		for (end = pos + 1;; ++end)
		{
			if (end > max)
			{
				suffix = "...";
				break;
			}

			if (end == str.Length)
			{
				suffix = "(eof)";
				break;
			}

			if (str[end] == '\n')
			{
				suffix = "\\n";
				break;
			}
		}

		var before = str.SubstringSafe(begin, pos - begin);
		var after = str.SubstringSafe(pos, end - pos);

		if (EscapeStrings)
		{
			before = EscapeString(before);
			after = EscapeString(after);
		}

		var firstLine = $"{prefix}{before}{after}{suffix}";
		var secondLine = $"{new string(' ', prefix.Length + before.Length)}^";

		var result = $"{firstLine}\n{secondLine}";
		return result;
	}

	//static string GetCharsAt(string str, int pos)
	//{
	//    const int addChars = 5;

	//    if (pos + addChars < str.Length)
	//        return str.Substring(pos, addChars);

	//    var len = Math.Min(pos + addChars, str.Length) - pos;
	//    if (len <= 0)
	//        return "(eol)";

	//    return $"{str.Substring(pos, len)}(eol)";
	//}
}