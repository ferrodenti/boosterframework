using System.Collections.Generic;
using JetBrains.Annotations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

#nullable enable

namespace Booster.UnitTests
{
	[PublicAPI]
	public class AssertFlags<T>
	{
		readonly HashSet<T> _inner;

		public object SyncRoot { get; } = new();

		public int Count
		{
			get
			{
				lock (SyncRoot)
					return _inner.Count;
			}
		}

		public AssertFlags(EqualityComparer<T>? comparer = null)
			=> _inner = new HashSet<T>(comparer ?? EqualityComparer<T>.Default);

		public AssertFlags(int capacity, EqualityComparer<T>? comparer = null)
			=> _inner = new HashSet<T>(capacity, comparer ?? EqualityComparer<T>.Default);

		public bool Set(T item, bool assert = true)
		{
			lock (SyncRoot)
			{
				var actual = _inner.Add(item);

				if (assert)
					Assert.IsFalse(!actual, $"Expected [{item}] = false");

				return actual;
			}
		}

		public bool Reset(T item, bool assert = true)
		{
			lock (SyncRoot)
			{
				var actual = _inner.Remove(item);

				if (assert)
					Assert.IsFalse(!actual, $"Expected [{item}] = true");

				return actual;
			}
		}

		public bool this[T item]
		{
			get
			{
				lock (SyncRoot)
					return _inner.Contains(item);
			}
			set
			{
				if (value)
					Set(item);
				else
					Reset(item);
			}
		}

		public void AssertIs(T item, bool expected)
		{
			var actual = this[item];
			Assert.AreEqual(expected, actual);
		}
	}
}