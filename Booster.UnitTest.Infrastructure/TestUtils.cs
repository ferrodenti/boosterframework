﻿using System.Collections.Generic;
using System.Text;

namespace Booster.UnitTests;

public static class TestUtils
{
	public static string RandomString(int len) //TODO: remove and use Rnd.String(len)
	{
		if (len < 0)
			return null;

		var str = new StringBuilder();
		for (var j = 0; j < len; j++)
			switch (Rnd.Int(5))
			{
			case 0:
				str.Append((char) ('a' + Rnd.Int(27)));
				break;
			case 1:
				str.Append((char) ('A' + Rnd.Int(27)));
				break;
			case 2:
				str.Append((char) ('а' + Rnd.Int(33)));
				break;
			case 3:
				str.Append((char) ('А' + Rnd.Int(33)));
				break;
			case 4:
				str.Append((char) ('0' + Rnd.Int(10)));
				break;
			}

		return str.ToString();
	}
		
	public static string RandomAsciiString(int len)
	{
		if (len < 0)
			return null;

		var str = new StringBuilder();
		for (var j = 0; j < len; j++)
			switch (Rnd.Int(3))
			{
			case 0:
				str.Append((char) ('a' + Rnd.Int(27)));
				break;
			case 1:
				str.Append((char) ('A' + Rnd.Int(27)));
				break;
			case 2:
				str.Append((char) ('0' + Rnd.Int(10)));
				break;
			}

		return str.ToString();
	}

	public static void AssertSequencesEqual<TItem>(IEnumerable<TItem> expected, IEnumerable<TItem> actual)
	{
		var counter = 0;

		bool Continuation(bool[] values)
		{
			if (!values[0])
				throw new AssertException($"An expected enumerable ends unexpectedly at element # {counter}");

			if (!values[1])
				throw new AssertException($"An actual enumerable ends unexpectedly at element # {counter}");
				
			return false;
		}

		foreach (var (a,b) in expected.Pairwise(actual, Continuation))
		{
			if (!a.Equals(b)) 
				throw new AssertException($"Assertion failed on item #{counter}");
				
			++counter;
		}
	}
}