
+(function ($) {
	"use strict";

	var BoosterEditor = function (element, options) {
		this.init("booster-editor", element, options);
	};

	BoosterEditor.VERSION = '0.0.1';

	BoosterEditor.DEFAULTS = {
	    rootUrl: "",
        prefferedSelect: "bs"
	};

	BoosterEditor.prototype.init = function (type, element, options) {
		this.options = $.extend({}, BoosterEditor.DEFAULTS, options);
		this.$element = $(element);

		if (this.options.schema)
			this.applySchema(this.options.schema);
	};

	BoosterEditor.prototype.applyHtml = function (html) {
		this.$element.html(html);
	};
	BoosterEditor.prototype.applySchema = function(schema) {
		var self = this;
		var html = null;
		var columns = null;
		
		var using = {};
		var settings;

		if (schema)
			switch (typeof schema) {
			case "string":
				html = schema;
			case "object":
				if (schema.constructor === Array)
					columns = schema;
				else {
					html = schema.html || null;
					columns = schema.data;
					settings = schema.controlSettings || schema || {};
				}
			};

		if (columns) {
			if (html === null)
				html = "";

			$(columns).each(function(j, col) {

				html +=  self._getSettings(settings, col.name, "preHtml", true, col.preHtml || "");

                if (col.type === "hidden") {
                    html += "<input type='hidden' name='" + col.name + "' class='form-control' />";
                }
				else if (col.type !== "custom") {
					html += "<div data-ctrl-name='" + col.name + "' class='form-group'>";

					var controlSize = self._getSettings(settings, col.name, "controlSize", true, col.type == "textarea" ? 8 : 5);
					var labelSize = self._getSettings(settings, col.name, "labelSize", true, 4);
					var icon = self._getSettings(settings, col.name, "icon", true);

					if (icon)
						icon = "<i class='" + icon + "'></i> ";
					else
						icon = "";


					if (col.type !== "checkbox")
						html += "<label for='" + col.name + "' class='col-sm-" + labelSize + " control-label'>" + icon + 
							self._getSettings(settings, col.name, "displayName", true, col.displayName) + 
							":</label><div class='col-sm-" + controlSize + "'>";

					var placeholder = self._getSettings(settings, col.name, "placeholder", true);

					do {
						var repeat = false;
						var attr = "";

						if (placeholder)
							attr += " placeholder='" + placeholder + "'";

					    switch (col.type) {
					    case "date":
					        if (!self._checkPlugin("datepicker")) {
					            repeat = true;
					            col.type = "general";
					            break;
					        }

					        attr += " data-date-format='" + self._getSettings(settings, col.name, "format", true, col.format || 'dd.mm.yyyy').toLowerCase() + "'";

					        html += "<div class='input-group date'" + attr + ">" +
					            "<input name='" + col.name + "' type='text' class='form-control datepicker' >" +
					            "<span class='input-group-btn'>" +
					            "<button type='button' title='Открыть календарь' class='btn btn-default' type='button'><i class='glyphicon glyphicon-calendar'></i></button>" +
					            "</span>" +
					            "</div>";

					        using.datepicker = true;
					        break;

					    case "checkbox":
					        var val = self._getSettings(settings, col.name, "value", false);
					        var def = self._getSettings(settings, col.name, "defaultValue", false);

					        if (def !== undefined && def != val)
					            delete settings.defaultValue[col.name];


					        html += "<div class='col-sm-4'></div>" +
					            "<div class='col-sm-8 checkbox'>" +
					            "<label>" +
					            "<input type='checkbox' name='" + col.name + "'" + attr + ">&nbsp;" + col.displayName +
					            "</label>" +
					            "</div>";
					        break;

					    case "integer":
					    case "int":
					    case "float":
					        if (!self._checkPlugin("intPicker")) {
					            repeat = true;
					            col.type = "general";
					            break;
					        }

					        if (col.type == "float") {

					            var precision = parseInt(self._getSettings(settings, col.name, "precision", true, 5));
					            var step = parseFloat(self._getSettings(settings, col.name, "step", true));
					            if (!step) {
					                step = "0.";
					                for (var i = 1; i < precision; i++)
					                    step += "0";
					                step += "1";
					            }

					            attr += " data-precision='" + precision + "' data-step='" + step + "'";
					        }

					        var val = self._getSettings(settings, col.name, "value", false);
					        var def = self._getSettings(settings, col.name, "defaultValue", false);

					        if (def !== undefined && def != val)
					            delete settings.defaultValue[col.name];

					        html += "<div class='input-group intPicker'>" +
					            "<input name='" + col.name + "' class='form-control'" + attr + " type='text'>" +
					            "<div class='input-group-addon'>" +
					            "<a href='javascript:;' class='spin-up' data-spin='up'><i class='fa fa-sort-asc'></i></a>" +
					            "<a href='javascript:;' class='spin-down' data-spin='down'><i class='fa fa-sort-desc'></i></a>" +
					            "</div></div>";
					        using.intpicker = true;
					        break;

					    case "select":
					        var options = "";
					        var cls = "form-control selectpicker";

					        var vals = self._getSettings(settings, col.name, "selectValues", true);
					        if (self._getSettings(settings, col.name, "multiple", true) === "true")
					            attr += " multiple data-selected-text-format='count>3'";

					        if (self._getSettings(settings, col.name, "combo") === "true")
					            cls += " combo";

					        var delim;
					        if ((delim = self._getSettings(settings, col.name, "delimiter", true)))
					            attr += " data-delimiter='" + delim + "'";

					        var emptyText;
					        if ((emptyText = self._getSettings(settings, col.name, "emptyText", true)))
					            attr += " data-none-selected-text='" + emptyText + "'";
					        
					        if (!vals)
					            vals = col.values;


					        if (vals) {
					            if (vals.constructor === Array)
					                $(vals).each(function() { options += "<option value='" + this + "'>" + this + "</option>"; });
					            else
					                for (var id in vals)
					                    options += "<option value='" + id + "'>" + vals[id] + "</option>";
					        }

					        html += "<select name='" + col.name + "' class='" + cls + "'" + attr + ">" + options + "</select>";

					        using.select = true;
					        break;
					    case "suggest":
					        var options = "";

					        attr += " data-ajax-url='" + self._getSettings(settings, col.name, "url", true) + "'";

					        var text = self._getSettings(settings, col.name, "text", true);
					        var val = self._getSettings(settings, col.name, "values", true);
					        if (text && val)
					            options += "<option value='" + val + "' selected>" + text + "</option>";

					        html += "<select name='" + col.name + "' class='form-control suggest'" + attr + " style='width: 100%'>" + options + "</select>";

					        using.suggest = true;
					        break;
					    case "textarea":

					        html += "<textarea rows='10' name='" + col.name + "'" + attr + " class='form-control'></textarea>";
					        break;

					    case "icon":
					        if (!self._checkPlugin("iconpicker")) {
					            repeat = true;
					            col.type = "general";
					            break;
					        }

					        html += "<input type='hidden' name='" + col.name + "' /><button id='icon' class='btn btn-default iconpicker'" + attr + " data-field='[name=\"" + col.name + "\"]'></button>";

					        using.iconpicker = true;
					        break;

					    case "filepath":
					    case "folderpath":
					        if (!self._checkPlugin("fileBrowser")) {
					            repeat = true;
					            col.type = "general";
					            break;
					        }

					        var controller = self._getSettings(settings, col.name, "controller", true);
					        var context = self._getSettings(settings, col.name, "context", true);

					        attr += " data-controller='" + controller + "' data-context='" + context + "'";

					        var baseDir = self._getSettings(settings, col.name, "baseDir", true);
					        if (baseDir)
					            attr += "data-base-dir='" + baseDir + "'";

					        html += "<div class='input-group'><input type='text' name='" + col.name + "'" + attr +
					            " class='form-control file-browser' data-type='" + col.type + "' >" +
					            "<span class='input-group-btn'>" +
					            "<button class='btn btn-default' type='button'>...</button>" +
					            "</span></div>";

					        using.filebrowser = true;
					        break;
					    case "password":
					        html += "<input type='password' name='" + col.name + "'" + attr + " class='form-control' />";
					        break;
					    case "file":
					    case "photo":

					        var fileName = self._getSettings(settings, col.name, "fileName", true);
					        var exists = self._getSettings(settings, col.name, "exists", true);
					        var emptyText = self._getSettings(settings, col.name, "emptyText", true) || "Нет";
					        var uploadText = self._getSettings(settings, col.name, "uploadText", true) || "Загрузить";
					        var deleteText = self._getSettings(settings, col.name, "deleteText", true) || "Удалить";
					        var src = self._getSettings(settings, col.name, "url", true);

					        if (col.type === "photo") {
					            html += "<input type='hidden' name='" + col.name + "' />" +
					                "<span class='no-file label label-default" + (exists ? " hide" : "") + "' style='margin-right: .5em;'>" + emptyText + "</span>" +
					                "<img class='has-file " + (!exists ? " hide" : "") + "' alt='" + fileName + "' title='" + fileName + "' src='" + src + "' data-src='" + src + "' style='width: 100%; height: auto; padding-bottom: .5em;'/>";
					        } else {
					            html += "<input type='hidden' name='" + col.name + "' />" +
					                "<span class='no-file label label-default" + (exists ? " hide" : "") + "' style='margin-right: .5em;'>" + emptyText + "</span>" +
					                "<a class='has-file btn-sm btn-success" + (!exists ? " hide" : "") + "' href='" + src + "' data-src='" + src + "' style='margin-right: .5em;'>" + (fileName || "") + "</a>";
					        }

					        var uploadUrl = self._getSettings(settings, col.name, "uploadUrl", true);
					        if (uploadUrl)
					            html += "<span class='btn btn-sm btn-danger delete-file" + (!exists ? " hide" : "") + "' data-upload-url='" + uploadUrl + "'>" +
					                "<i class='glyphicon glyphicon-remove'></i> " + deleteText + "</span>&nbsp;" +
					                "<span class='btn btn-warning  btn-sm btn-md fileinput-button'>" +
					                "<i class='glyphicon glyphicon-import'></i> " + uploadText +
					                "<input class='file-upload' data-upload-url='" + uploadUrl + "' type='file' /></span>";

					        using.fileupload = true;
					        break;
					    default:
					        html += "<input type='text' name='" + col.name + "'" + attr + " class='form-control' />";
					        break;
					    }
					} while (repeat);

					if (col.type != "checkbox")
						html += "</div>";
                        
					var descr = self._getSettings(settings, col.name, "description", true);
					if (descr != null) {
						html += "<span class='description-tooltip' data-toggle='tooltip' data-html='true' " + 
						"style='position: relative; left: -7px; top:-3px; background-color: #555; font-size:.7em; color: #fff; cursor: pointer; border-radius: 8px; padding: 1px 4px;' title='" + 
						descr.replace("\'", "\\\'") + "'>?</span>";
						using.descriptionTooltip = true;
					}

					html += "</div>";
				}
				html +=  self._getSettings(settings, col.name, "postHtml", true, col.postHtml || "");
			});

		}

		if (html !== null) {
			this.applyHtml(html);

			if (using.intpicker && this._checkPlugin("intPicker"))
				this.$element.find("div.intPicker").intPicker();

			if (using.datepicker && this._checkPlugin("datepicker")) {
			    this.$element.find("div.date").datepicker();
			}

			if (using.filebrowser && this._checkPlugin("fileBrowser"))
			    this.$element.find("input.file-browser").fileBrowser({
			        controllerUrl: this.options.rootUrl + "booster/fileBrowser/"
			    });

		    if (using.fileupload && this._checkPlugin("fileupload")) {

		        var updateUploadedFile = function(div, data) {
		            var file = div.find(".has-file");
		            file.toggleClass("hide", !data.fileName);
		            div.find("span.no-file").toggleClass("hide", !!data.fileName);
		            div.find("input[name]").val(data.ses);
		            div.find("span.btn-danger").toggleClass("hide", !data.fileName);

		            if (!!data.fileName) {
		                var img = div.find("img");
		                if (img.length > 0) {
		                    img.attr("src", img.data("src") + "&ses=" + data.ses);
		                    img.attr("alt", data.fileName);
		                    img.attr("title", data.fileName);
		                } else {
		                    file.text(data.fileName);
		                    file.attr("href", file.data("src") + "&ses=" + data.ses);
		                }
		            }
		        };

		        this.$element.find("input.file-upload").each(function() {
		            var $this = $(this);
		            var parent = $this.closest("div");
		            $this.fileupload({
		                url: $this.data("upload-url"),
		                error: function(e) {
		                    alert(e.responseText.match(/<!--[\s\S]*?-->/g));
		                },
		                done: function(e, data) {
		                    updateUploadedFile(parent, data.result);
		                }
		            });
		        });

		        this.$element.find("span.delete-file").click(function () {
		            var $this = $(this);
		            var url = $this.data("upload-url");
		            var dt = { formData: null };

		            $this.closest("div").find(".file-upload").trigger("fileuploadsubmit", dt);

		            $.post(url, dt.formData || {}, function (data) {
		                updateUploadedFile($this.closest("div"), data);
		            });
		        });
		    }


		    if (using.suggest && this._checkPlugin("select2")) {
			    this.$element.find("select.suggest").select2({
		            ajax: {
		                dataType: 'json',
		                type: 'POST',
		                processResults: function (data) {
		                    return {
		                        results: data.items,
		                        pagination: { more: data.more }
		                    };
		                },
		                cache: true
		            },
		            placeholder: { id: "", placeholder: "" }, // иначе не работает х
		            allowClear: true,
		            minimumInputLength: 1,
		            language: {
		                inputTooShort: function (args) { return "Наберите текст чтобы появились подсказки"; },
		                errorLoading: function () { return "Ошибка загрузки результатов"; },
		                loadingMore: function () { return "Загрзка дополнительных результатов"; },
		                noResults: function () { return "Не найдено"; },
		                searching: function () { return "Загрузка..."; },
		                maximumSelected: function (args) { return "Ошибка"; }
		            }
			    }).data("select-type", "select2");
		    }
		    if (using.select) {
			    var initSelect2 = function () {
			        if (this._checkPlugin("select2")) {
			            this.$element.find("select.selectpicker").select2({
			                minimumResultsForSearch: 20,
			                width: "100%",
			                theme: "bootstrap"
			            }).data("select-type", "select2");
			            return true;
			        }
			        return false;
			    };

			    var initBsSelect = function() {
			        if (this._checkPlugin("selectpicker")) {
			            document._bs_select_addSelectItem = function (t, ev) {
			                ev.stopPropagation(); ev.preventDefault();
			                var txt = $(t).prev().val().replace(/[|]/g, "");
			                if ($.trim(txt) === '') return;
			                var p = $(t).closest('.bootstrap-select').prev();
			                var o = $('option', p).eq(-2);
			                o.before($("<option>", { "selected": true, "text": txt, "value": txt }));
			                p.selectpicker('refresh');
			            };

			            document._bs_select_addSelectInpKeyPress = function (t, ev) {
			                ev.stopPropagation();
			                // do not allow pipe character
			                if (ev.which === 124) ev.preventDefault();
			                // enter character adds the option
			                if (ev.which === 13) {
			                    ev.preventDefault();
			                    document._bs_select_addSelectItem($(t).next(), ev);
			                }
			            };

			            var content = "<input type=text onKeyDown='event.stopPropagation();' onKeyPress='_bs_select_addSelectInpKeyPress(this,event)' onClick='event.stopPropagation()' placeholder='Add item'> <span class='glyphicon glyphicon-plus addnewicon' onClick='_bs_select_addSelectItem(this,event,1);'></span>";
			            var divider = $('<option/>').addClass('divider').data('divider', true);
			            var addoption = $('<option/>').addClass('additem').data('content', content);
			            this.$element.find("select.selectpicker.combo").append(divider).append(addoption);
			            this.$element.find("select.selectpicker").selectpicker().data("select-type", "bs");


			            return true;
			        }
			        return false;
			    }
			    var initialized = false;
			    switch (this.options.prefferedSelect) {
			    case "bs":
			        initialized = initBsSelect.call(this);
                    break;
			    case "select2":
			        initialized = initSelect2.call(this);
			        break;
			    }
			    if (!initialized)
			        initSelect2.call(this) || initSelect2.call(this);
			}

            if (using.descriptionTooltip) {
				this.$element.find(".description-tooltip").tooltip({
					placement: function(tip, element) {

						if (!self.gauge) {
							self.gauge = $("<div style='visibility: hidden; position: absolute; top: 0'></div>");
							self.$element.append(self.gauge);
						}
						self.gauge.html($(tip).find('.tooltip-inner')[0].outerHTML);
						var offset = $(element).offset();
						var tipHeight = self.gauge.height();
						if (offset.top - $(document).scrollTop() < tipHeight)
							return "bottom";
						return "top";
					}
				});
			}
		}

		if (settings.visibility)
			for (var name in settings.visibility)
				this.$element.find("[data-ctrl-name='" + name + "']").toggle(settings.visibility[name]);

		if (settings.placeholder)
			for (name in settings.placeholder)
				this.$element.find("[name='" + name + "']").attr("placeholder", settings.placeholder[name]);

		if (settings.defaultValue) {
			for (name in settings.defaultValue) {
				var ctrl = this.$element.find("[name='" + name + "']");
				var value = settings.defaultValue[name];

				ctrl.data("default", value);
				
				if (!settings.placeholder || settings.placeholder[name] === undefined)
					ctrl.attr("placeholder", value);

				if (settings.values && settings.values[name] == value)
					delete settings.values[name];
			}
		}

		if (settings.values)
			this.data(settings.values);

		if (using.iconpicker && this._checkPlugin("iconpicker"))
			this.$element.find("button.iconpicker").iconpicker({
				iconset: 'mix',
				emptyIcon: true,
				rows: 5,
				cols: 5,
			});
	};

	BoosterEditor.prototype._checkPlugin = function(name) {
		return $.fn[name] !== undefined && typeof $.fn[name] == "function";
	};

	BoosterEditor.prototype._getSettings = function(settings, name, key, remove, def) {
		if (settings) {
			var dict = settings[key];
			if (dict) {
				var val = dict[name];
				if (remove) {
					delete dict[name];

					for (var k in dict) {
						return val || def;
					}
					delete settings[key];
				}
				if (val !== undefined)
					return val;
			}
		}
		return def;
	};

    BoosterEditor.prototype.data20 = function() {
        var res = {};
        var self = this;

        this.$element.find("[name]").each(function() {
            var $this = $(this);
            res[$this.attr("name")] = self.сontrolValue($this);
        });

        return res;
    };


	BoosterEditor.prototype.data = function(value) {
		if (value === undefined) { //GET
			var arrayData, objectData;
			arrayData = this.$element.serializeArray(); //TODO: use сontrolValue here
			objectData = {};

			$.each(arrayData, function() {

				if (this.value != null) {
				    value = this.value;
				} else {
					value = '';
				}

				if (objectData[this.name] != null) {
					if (!objectData[this.name].push) {
						objectData[this.name] = [objectData[this.name]];
					}
					objectData[this.name].push(value);
				} else {
					objectData[this.name] = value;
				}
			});
		    this.$element.find('input[name][type=checkbox]:not(:checked)').each(function() {
		        objectData[this.name] = "off";
		    });
			return objectData;
		}

		var self = this; //SET

		if ($.isArray(value))
			$(value).each(function() {
				self.сontrolValue(self.$element.find("[name='" + this.name + "']"), this.value);
			});
		else
			for (var key in value)
				self.сontrolValue(self.$element.find("[name='" + key + "']"), value[key]);
	};

	BoosterEditor.prototype.showValidationMessages = function (errors) {

		for (var ctrlName in errors) {
			var ctrl = this.$element.find("[name='" + ctrlName + "']");
			showValidationMessage(this.$element, ctrl, errors[ctrlName]);
		}

		function showValidationMessage(form, elem, message) {

			var $block, $div;

			if (elem.length > 0) {
				$div = elem.closest('.form-group');

				if ($div.length > 0) {
					$block = $div.find("span.help-block");

					if ($block.length == 0) {
						var c = elem;
						do {

							c = c.parent().closest('div');
						} while (c.hasClass("input-group"));
						c.append($block = $('<span class="help-block" role="validation-message"></span>'));
					}
				} else
					($block = $('<span class="help-block"></span>')).appendAfter(elem);

			} else {
				$div = form.find("[role='validation-summary']");
				if ($div.length == 0)
					$div = $("<div role='validation-summary'></div>");

				$block = $div.find("span.help-block");
				if ($block.length == 0)
					$div.append($block = $('<span class="help-block" role="validation-message"></span>'));
			}

			if (!message) {
				$div.removeClass('has-error');
				$block.empty();
			} else {
				$div.addClass('has-error');
				$block.html(message);

				if (elem.length > 0 && !elem[0].clearValidationMessageAttached) {
					elem.on("change", function () {
						showValidationMessage(form, elem, null);
					});
					elem[0].clearValidationMessageAttached = true;
				}
			}
		};
	};

	BoosterEditor.prototype.clearValidationMessages = function () {
//		$.each(this.$element, function () {
//			var $form = $(this);
//		});
		this.$element.find('div.has-error').removeClass('has-error');
		this.$element.find('[role="validation-message"]').empty();
	};

	BoosterEditor.prototype.сontrolValue = function (control, value) {
	    var res = "";
	    var type = "";
		if (!(control instanceof jQuery))
			control = $(control);

		if (value !== undefined) {
			var def = control.data("default");
			if (value === def)
				value = "";
		}

		switch (control.prop('tagName').toLowerCase()) {
			case 'input':
				if (control.attr('type') == 'checkbox') {
					if (value === undefined)
						return control.prop('checked');
				    return control.prop('checked', value === true || value === "true");
				}
				if (control.hasClass("datepicker")) {
				    control.closest("div.date").datepicker("setDate", value);
                    return control.val();
                }
			case 'textarea':
				if (value === undefined) {
					return control.val();
				}
				return control.val(value);
			case 'select':
				if (control.attr('multiple') !== undefined) {
					var delimiter = control.data("delimiter");
					if (!delimiter)
						delimiter = ";";

					if (value === undefined) {
						$(control.val()).each(function() {
							if (res != "") res += delimiter;
							res += this;
						});
						return res;
					}
					if (control.attr('multiple') !== undefined && typeof value === "string") {
						value = value.replace(/^;|;$/g, "").split(delimiter);

					    for (var i = 0; i < value.length; i++) {
					        value[i] = value[i].trim();
					    }

						res = control.val(value);
						type = control.data("select-type");
					    switch (type) {
					    case "select2":
					        control.select2('val', value);
					        break;
					    case "bs":
					        control.selectpicker('render');
					    }
						return res;
					}
				}

				if (value === undefined)
					return control.val();


				res = control.val(value);
				type = control.data("select-type");
			    switch (type) {
			    case "select2":
			        if (control.hasClass('suggest') && value) {
			            var opt = control.find("option[value='" + value + "']");
			            control.select2('data', { id: value, label: opt.length === 0 ? value : opt.text() });
			            break;
			        }
			        control.select2('val', value);
			        break;
			    case "bs":
			        control.selectpicker('render');
			    }
			    return res;
			default:
				console.error('unexpected tag name: ', this.prop('tagName'));
		};
	};

	function Plugin(option) {
		var result;
		var args = arguments;
		var res = this.each(function () {
			var $this = $(this);
			var data = $this.data('booster-editor');
			var options = typeof option === 'object' && option;
			var selector = options && options.selector;

			if (!data && option == 'destroy')
				return;

			if (selector) {

				if (!data)
					$this.data('booster-editor', (data = {}));

				if (!data[selector])
					data[selector] = new BoosterEditor(this, options);

			} else {
				if (!data)
					$this.data('booster-editor', (data = new BoosterEditor(this, options)));
			}

			if (typeof option === 'string' && typeof data[option] == "function") {
				result = data[option].apply(data, Array.prototype.slice.call(args, 1));
			}
		});
		return result !== undefined ? result : res;
	};

	$.fn.BoosterEditor = Plugin;
	$.fn.BoosterEditor.Constructor = BoosterEditor;

})(jQuery);