/*globals jQuery, define, exports, require, window, document */

/* =========================================================================
 * BoosterGrid: booster-grid.js v0.0.1
 * Licensed under MIT (//TODO: add site/LICENSE)
 * ========================================================================= */
//TODO: add usings
//TODO: factory for require.js

+function($) {
	'use strict';

	// BOOSTER_GRID PUBLIC CLASS DEFINITION
	// ==================================

	var BoosterGrid = function(element, options) {
		this.$element = null;
		this.$table = null;
		this.$modal = null;
		this.$curtain = null;
		this.$caption = null;
		this.paginators = {};
		this.version = "";
		this.currentPage = 0;
		this.sort = null;
		this.sortDir = null;
		this.pauseSync = false;
		this.dragAndDrop = new DragAndDrop(this);
	    this.allColumns = {};
	    this.selectedRows = 0;
		this.init('booster-grid', element, options);
	};

	BoosterGrid.VERSION = '0.0.1';

	BoosterGrid.DEFAULTS = {
		controllerUrl: "/booster/grid/",
		pageSize: 50,
		filterForm: null,
		paginators: "both",
		useGet: false,
		updateCaption: true,
		updatePageTitle: true,
		updateUrlHash: true,
		applyFilterControl: null,
		displayBboxes: false,
		alertErrors: false,
		context: null,
		modalContainer: null,
		syncInterval: 0,
        hiddenColumns: [],
		paginatorOptions: {
			recordsPerPage: 30,
			displayedPages: 13
		}
	};

	BoosterGrid.prototype.init = function(type, element, options) {
		this.$element = $(element);
	    this.options = $.extend({}, BoosterGrid.DEFAULTS, options, this.$element.data());
		this.$element.addClass('booster-grid');
		this.options.paginatorOptions.recordsPerPage = this.options.pageSize;

		var self = this;

		if (this.options.updateUrlHash) {
			self.readHash();

			$(window).on('hashchange', function() {
				if (self.readHash())
					self.reloadGrid();
			});
		}

	    if (!this.options.noFirstLoad)
			this.reloadGrid();

		if (this.options.syncInterval)
			setInterval(function () {
				if(!self.pauseSync)
					self.query("Sync", null, function(resonse) {
						if (self.version !== resonse) {
							self.version = resonse;
							self.reloadGrid();
						}
					});
			}, this.options.syncInterval);

		if (this.options.applyFilterControl)
			$(this.options.applyFilterControl).click(function() {
				self.reloadGrid();
			});
	};

	BoosterGrid.prototype.readHash = function() {
		var hash = $.hash();
		var page = 0, sort = null, dir = null;

		if (hash.page)
			page = (parseInt(hash.page) - 1) || 0;
		if (hash.sortdir)
			dir = hash.sortdir;
		if (hash.sort && typeof hash.sort !== "function") {
			sort = hash.sort;
			if (!dir)
				dir = "asc";
		}

		if (page != this.currentPage || sort != this.sort || dir != this.sortDir) {
			this.currentPage = page;
			this.sort = sort;
			this.sortDir = dir;
			return true;
		}
		return false;
	};

	BoosterGrid.prototype.reloadGrid = function(resetPage) {

		this.showCurtain(true);
	    if (resetPage)
	        this.currentPage = 0;

		var self = this;

		this.query("Select", null, function(response) {
			self.updateGrid(response);
			self.showCurtain(false);
		});
	};
	BoosterGrid.prototype.showCurtain = function(show) {
		if (show) {
			this.initTable();

			if (!this.$curtain) {
				this.$curtain = $('<div class="curtain"></div>');
				this.$element.append(this.$curtain);
			}
			var elem = this.$table.find('table');
			if (elem.length == 0)
				elem = this.$table;

			var pos = elem.position();

			pos.width = elem.width();
			pos.height = elem.height();
			this.$curtain.css({
				"left": pos.left,
				"top": pos.top,
				"width": pos.height < 16 ? 16 : pos.width, // pos.height так и должен быть
				"height": pos.height < 16 ? 16 : pos.height
			});

			this.$curtain.show();
		} else if (this.$curtain) {
			this.$curtain.fadeOut(400);
		}
	};

	BoosterGrid.prototype.createQueryData = function (ext) {
	    var data = { controller: this.options.controller };

	    if (this.currentPage > 0) data.skip = this.currentPage * this.options.pageSize;
	    if (this.options.pageSize > 0) data.take = this.options.pageSize;
	    if (this.sort) data.sort = this.sort;
	    if (this.sortDir) data.sortDir = this.sortDir;
	    if (this.options.context) data.context = this.options.context;
	    if (this.options.hiddenColumns.length > 0) data.hiddenColumns = JSON.stringify(this.options.hiddenColumns);

	    if (this.options.filterForm)
	        data.form = JSON.stringify($(this.options.filterForm).BoosterEditor("data"));

	    if (ext)
	        $.extend(data, ext);

	    return data;
	};

    BoosterGrid.prototype.query = function(method, ext, done) {
		var self = this;
		this.pauseSync = true;
		$.ajax({
			type: this.options.useGet ? "GET" : "POST",
			url: this.options.controllerUrl + method,
			traditional: true,
			data: this.createQueryData(ext),
			success: function (response) {
				if (response.version)
					self.version = response.version;
				self.pauseSync = false;

				done(response);
			},
            error: function(data, stat, err) {
				self.pauseSync = false;
                var func = function(er) {
                    if (window.console && window.console.error) {
                        window.console.error(er);
                        if (!self.options.alertErrors)
                            return;
                    }
                    alert(er);
                };

                if (data.responseText)
                    func(data.responseText + "/ " + stat + " / " + err);
                else
                    func(data.statusText + "/ " + stat + " / " + err);
            }
        });
	};

	BoosterGrid.prototype.getRowKey = function(element) {
		if (!(element instanceof $))
			element = $(element);

		while (element.prop("tagName") !== "TR" && element.parent() != null) {
			element = element.parent();
		}
		return element.data("key");
	};

	BoosterGrid.prototype.paginatorClick = function(page) {
		if (this.currentPage != page - 1) {
			this.currentPage = page - 1;

			for (var name in this.paginators)
				this.paginators[name].paginator("currentPage", page);

			this.reloadGrid();
		}
	};

	BoosterGrid.prototype.updateGrid = function(response) {

		if (this.options.updateUrlHash)
			$.hash({
				page: this.currentPage ? this.currentPage + 1 : null,
				sort: this.sort,
				sortdir: this.sortDir === 'desc' ? 'desc' : null
			}, 'removeEmpty');

	    this.selectedRows = response.selectedRows || 0;
		this.deleteText = response.schema.deleteText || "Вы уверены, что хотите удалить эту запись?";
		var texts = this.deleteText;

		var self = this;

		this.initTable();

		this.initPaginator("upper", response.totalRows);

		if (this.options.updateCaption) {
			if (!this.$caption) {
				this.$caption = $("<h2 class='clearfix'></h2>");
				this.$caption.insertBefore(this.$table);
			}
			this.$caption.html(response.schema.caption);
		}

		if (this.options.updatePageTitle)
			document.title = response.pageTitle;

		this.initPaginator("lower", response.totalRows);

	    var html = "<table class='table table-bordered table-striped table-hover table-condensed'><thead><tr>";

	    var needFirstCell = false;
	    var colCount = 0;

	    if (response.allowReorder) {
	        html += "<th class='handle'></th>";
	        needFirstCell = true;
	        colCount++;
	    }
	    if (response.allowSelect) {
	        html += "<th" + (needFirstCell ? " class='firstCell'" : "") + "><input type='checkbox' role='select-all'" + (response.allSelected ? " checked" : "") + "/></th>";
	        needFirstCell = false;
            colCount++;
	    }

		var textRefferences = [];

        this.allColumns = {};

		$(response.schema.data).each(function(i, col) {

		    self.allColumns[col.name] = col.displayName;

		    if (self.options.hiddenColumns.indexOf(col.name) > -1)
		        return;

			if (texts.indexOf("{" + col.name + "}") > -1)
				textRefferences.push(col.name);

			var icon = "";
			var attr = "";

			if (needFirstCell) {
				attr += " class='firstCell'";
			    needFirstCell = false;
			}

			if (col.sortable) {
				icon = "<i class='glyphicon glyphicon-none'></i>";

				if (col.name == response.sortColumn) {
					if (response.sortDir == "asc")
						icon = "<i class='glyphicon glyphicon-sort-by-alphabet'></i>";
					else if (response.sortDir == "desc")
						icon = "<i class='glyphicon glyphicon-sort-by-alphabet-alt'></i>";
				}
				attr += " data-sort='" + col.name + "'";
			}
		    html += "<th" + attr + ">" + col.displayName + "&nbsp;" + icon + "</th>";
		    colCount++;
		});

		if (response.allowSelect || response.allowWrite || response.allowDelete) {
	        if (response.allowInsert) {
	            if (response.insertUrl)
	                html += "<th class='сontrols'><a class='btn btn-success btn-xs' role='insertRow' href='" + response.insertUrl + "' target='_blank'><i class='glyphicon glyphicon-plus'></i> Добавить</a></th>";
	            else
	                html += "<th class='сontrols'><button class='btn btn-success btn-xs' role='insertRow'><i class='glyphicon glyphicon-plus'></i> Добавить</button></th>";
	        } else {
	            html += "<th class='сontrols'></th>";
	        }
		    colCount++;
	    }

	    textRefferences = $(textRefferences);

	    html += "</tr></thead><tbody>";

	    if (response.rows.length == 0) {
	        html += "<tr><td colspan='" + colCount + "'>Нет записей</td></tr>";
	    } else {
	        $(response.rows).each(function(i, row) {

	            var tokens = "";

	            textRefferences.each(function(j, reff) {
	                for (var n = 0; n < row.cells.length; n++) {
	                    var cell = row.cells[n];
	                    if (cell.name == reff) {
	                        tokens += ' data-text-token-' + cell.name.toLowerCase() + '="' + cell.value.replace(/\"/, "\"") + '"';
	                        break;
	                    }
	                }
	            });

	            var allowEdit = (response.allowWrite && row.allowWrite !== false) || row.allowWrite === true;
	            if (allowEdit)
	                tokens += " data-edit='true'";

	            html += "<tr data-key='" + encodeURIComponent(row.key) + "' " + tokens + ">";

	            if (response.allowReorder)
	                html += "<td class='handle'><i class='fa fa-ellipsis-v'></td>";

	            if (response.allowSelect) {
	                html += "<td><input type='checkbox' role='select-row'" + (row.selected ? " checked" : "") + (row.allowSelect == false ? " disable" : "") + "/></td>";
	            }

	            $(response.schema.data).each(function(j, col) {
	                if (self.options.hiddenColumns.indexOf(col.name) > -1)
	                    return;

	                var value = "";
	                for (var n = 0; n < row.cells.length; n++) {
	                    var cell = row.cells[n];
	                    if (cell.name == col.name && cell.value !== undefined) {
	                        value = cell.value;
	                        break;
	                    }
	                }
	                if (col.type == "checkbox") {
	                    value = value === "true" ? "<i class='glyphicon glyphicon-ok'></i>" : "";
	                } else if (!col.preserveHtml)
	                    value = $('<div/>').text(value).html();

	                html += "<td>" + value + "</td>";
	            });

	            var ctrls = row.controls || "";

	            if (!row.overrideControls) {
	                if (allowEdit) {
	                    var url = row.editUrl;
	                    if (!url) {
	                        url = response.editUrlFormat;
	                        if (url) {
	                            url = url.replace(new RegExp('\\{0\\}', 'gm'), row.key);
	                        }
	                    }

	                    if (ctrls.length > 0)
	                        ctrls += "<span>&nbsp;&nbsp;</span>";

	                    if (!url) {
	                        ctrls += "<button class='btn btn-default btn-xs btn-edit' title='Редактировать запись' role='editRow'><i class='glyphicon glyphicon-pencil'></i></button>";
	                    } else {
	                        ctrls += "<a class='btn btn-default btn-xs btn-edit' title='Редактировать запись' role='editRow' target='edit_" + self.options.controller + "_" + row.key + "' href='" + url + "'><i class='glyphicon glyphicon-pencil'></i></a>";
	                       // ctrls += "<a class='btn btn-default btn-xs btn-edit' title='Редактировать запись' role='editRow' target='_blank' href='" + url + "'><i class='glyphicon glyphicon-pencil'></i></a>";
                        }
	                }

	                if ((response.allowDelete && row.allowDelete !== false) || row.allowDelete === true) {

	                    if (ctrls.length > 0)
	                        ctrls += "<span>&nbsp;&nbsp;</span>";

	                    ctrls += "<button class='btn btn-danger btn-xs btn-delete' title='Удалить запись' role='deleteRow'><i class='glyphicon glyphicon-remove'></i></button>";
	                }
	            }

	            if (ctrls.length > 0)
	                html += "<td class='controls'>" + ctrls + "</td>";

	            html += "</tr>";
	        });
	    }

	    html += "</tbody></table>";
	    this.$table.html(html);

		this.$table.find('thead th[data-sort]').on('click', function() {
			var $this = $(this);
			var sort = $this.data('sort');

			if (self.sort != sort) {
				self.sort = sort;
				self.sortDir = 'asc';
			} else if (self.sortDir == 'asc')
				self.sortDir = 'desc';
			else {
				self.sort = null;
				self.sortDir = null;
			}

			self.currentPage = 0;
			self.reloadGrid();
		});

		if (response.allowReorder)
			this.dragAndDrop.init();

	    this.$element.trigger("updated:booster-grid", [this.selectedRows]);
	};

    BoosterGrid.prototype.columnVisible = function(name, value) {
        var oldValue = $.inArray(name + "", this.options.hiddenColumns);
        if (value == undefined)
            return oldValue == -1;

        if ((oldValue == -1) != value) {
            if (value) {               
                this.options.hiddenColumns.splice(oldValue, 1);
            } else {
                this.options.hiddenColumns.push(name + "");
            }
            this.reloadGrid(false);
        }
    };

    BoosterGrid.prototype.initTable = function() {

		if (!this.$table) {
			this.$table = $("<div class='table-responsive'></div>");
			this.$element.append(this.$table);

			var self = this;
			this.$table.on('click', 'button[role="insertRow"]', function() { self.insertRow(); });
			this.$table.on('click', 'button[role="editRow"]', function() { self.editRow(self.getRowKey(this)); });
			this.$table.on('click', 'button[role="deleteRow"]', function() { self.deleteRow(self.getRowKey(this)); });
			this.$table.on('dblclick', "tr[data-key][data-edit='true']", function() { self.editRow(self.getRowKey(this)); });
			this.$table.on('mousedown', 'td.handle', function(e) { self.dragAndDrop.mouseDown(e, $(this)); });

			this.$table.on('click', 'input[role="select-all"]', function() {
			    var $this = $(this);
			    var val = $this.prop("checked");

			    self.query("userselect", { data: "{'all':'" + val + "'}" }, function(data) {
			        self.selectedRows = data.totalRows;
			        self.$table.find('[role="select-row"]').prop("checked", val);
			        self.$element.trigger("selected:booster-grid", [self.selectedRows]);
			    });
			});

			this.$table.on('click', 'input[role="select-row"]', function() {
			    var $this = $(this);
			    var val = $this.prop("checked");
			    var key = $this.closest("tr").data("key");

			    self.query("userselect", { data: "{'" + key + "':'" + val + "'}" }, function(data) {
			        self.selectedRows = data.totalRows;
			        self.$table.find('[role="select-all"]').prop("checked", data.allSelected);
			        self.$element.trigger("selected:booster-grid", [self.selectedRows]);
			    });
			});


		}
	};

	BoosterGrid.prototype.initPaginator = function(name, totalRecords) {

		if (this.options.paginators == "both" || this.options.paginators == name) {

			var paginator = this.paginators[name];

			if (!paginator) {
				var self = this;
				this.paginators[name] = paginator = $(this.options.updateCaption ? "<div class='pull-right'></div>" : "<div class='text-right'></div>");
				paginator.paginator(this.options.paginatorOptions);
				paginator.paginator("linkClicked", function(e, page) { self.paginatorClick(page); });

				if (name == "upper")
					paginator.insertBefore(this.$table);
				else
					paginator.insertAfter(this.$table);
			}

			paginator.paginator("numberOfRecords", totalRecords);
			paginator.paginator("currentPage", this.currentPage + 1);
		}
	};

	BoosterGrid.prototype.deleteRow = function(key) {

		var row = this.$element.find("tr[data-key='" + key + "']");
		var msg = this.deleteText.replace(/\{[^\}]*\}/g, function(str) {
			return row.data("text-token-" + str.toLowerCase().substring(1, str.length - 1));
		});

		if (!confirm(msg))
			return;

		this.showCurtain(true);

		var self = this;
		this.query("Delete", { key: key }, function() {
			self.reloadGrid();
		});
	};

	BoosterGrid.prototype.editRow = function (key) {
	    var row = this.$element.find("tr[data-key='" + key + "']");
	    var anc = row.find("a[role='editRow']");
	    if (anc.length === 1) {
	        window.open(anc.attr("href"), anc.attr("target"));
	        return;
	    }

		this.showCurtain(true);

		var self = this;
		this.query("GetEdit", { key: key }, function(response) {
			self.showModal(response, key);
			self.showCurtain(false);
		});	

	};

	BoosterGrid.prototype.insertRow = function() {
		this.showCurtain(true);
		var self = this;
		this.query("GetNew", null, function(response) {
			self.showModal(response);
			self.showCurtain(false);
		});
	};

	BoosterGrid.prototype.showModal = function(response, key) {
		var self = this;

		if (!this.$modal) {
			this.$modal = $("<div class='modal fade' role='dialog' aria-labelledby='dictEditModalLabel' aria-hidden='true'>" +
				"<div class='modal-dialog'>" +
				"<div class='modal-content'>" +
				"<div class='modal-header'>" +
				"<h4 class='modal-title'></h4>" +
				"</div>" +
				"<div class='modal-body'>" +
				"<form class='form-horizontal'></form>" +
				"</div>" +
				"<div class='modal-footer'>" +
				"<button type='button' class='btn btn-default' data-dismiss='modal'>Отмена</button>" +
				"<button type='button' class='btn btn-primary btn-submit'>Сохранить</button>" +
				"</div>" +
				"</div>" +
				"</div>" +
				"</div>");

			this.$modal.find('.btn-submit').on('click', function() {
				self.query("Save", { key: $(this).data("key"), data: JSON.stringify(self.$modalForm.BoosterEditor("data")) }, function(resp) {
					if (resp.validationResponse.isOk) {
						self.reloadGrid();
						self.$modal.modal('hide');
					} else {
						self.$modalForm.BoosterEditor("showValidationMessages", resp.validationResponse.errors);
					}
				});
			});

			var modalContainer = this.options.modalContainer ? $(this.options.modalContainer) : this.$element;
			modalContainer.append(this.$modal);

			this.$modalForm = this.$modal.find('.form-horizontal');
			this.$modalForm.BoosterEditor({});
		    this.$modalForm.on("fileuploadsubmit", "input.file-upload", function(e, data) { data.formData = self.getFileRequest(this); });
		}

		this.$modalForm.BoosterEditor("applySchema", response.schema);

//		if (!response.editor.hash || this.dlgSchemaHash != response.schema.hash) {
//			this.$modalForm.BoosterEditor("applySchema", response.editor);
//			this.dlgSchemaHash = response.schema.hash;
//		}

		if (key) {
			this.$modal.find('.btn-submit').data('key', key);
			this.$modal.find('.modal-title').text(response.caption || "Редактирование записи");
		} else {
			this.$modal.find('.btn-submit').removeData('key');
			this.$modal.find('.modal-title').text(response.caption || "Ввод новой записи");
		}

		this.$modalForm.find(".has-file").each(function () {
		    var $this = $(this);
		    var srcAttr = $this.prop("tagName").toLowerCase()  == "a" ? "href" : "src";
		    var request = self.getFileRequest($this);
		    var src = $this.data("src");
		    var sepa = "?";

		    for (var field in request) {
		        var value = request[field];
		        if (typeof (value) !== "string")
		            value = JSON.stringify(value);

		        src += sepa + field + "=" + value;
                sepa = "&";
		    }
		    $this.data("src", src);
	        $this.prop(srcAttr, src);
		});

		this.$modalForm.BoosterEditor("clearValidationMessages");
		this.$modal.modal('show');
	};

	BoosterGrid.prototype.getFileRequest = function (ctrl) {
	    var ext = {
	        custom: JSON.stringify({
	            field: $(ctrl).closest("div").find("input[name]").prop("name")
	        }),
	        key: this.$modal.find('.btn-submit').data('key')
	    };
	    return this.createQueryData(ext);
	};

    var DragAndDrop = function(grid) {
		this.grid = grid;

		this.init = function() {

			this._allRows = null;
			this._paginatorsBBoxes = null;
			this.currentBBox = null;

			var self = this;
		};

		this.allRows = function() {
			if (!this._allRows) {
				var res = [];

				this.grid.$table.find("tr[data-key][data-key!='" + this.$origRow.data('key') + "']").each(function() {
					res.push($(this));
				});

				this._allRows = res;
			}
			return this._allRows;
		};

		this.paginatorsBBoxes = function() {
			if (!this._paginatorsBBoxes) {
				this._paginatorsBBoxes = [];

				for (var name in this.grid.paginators)
					this._paginatorsBBoxes.push(new BoundingBox(this.grid.paginators[name]));

				this._paginatorsBBoxes = $(this._paginatorsBBoxes);
			}
			return this._paginatorsBBoxes;
		};

		this.mouseDown = function(e, $handle) {

			this.tableBBox = new BoundingBox(grid.$table.find('tbody'));
			this.tableBBox.expand(50);

			this._allRows = null;
			this.clickCoords = {
				x: $handle.offset().left - e.pageX,
				y: $handle.offset().top - e.pageY,
			};

			this.$origRow = $handle.closest("tr");

			if (this.grid.options.displayBboxes) {
				this.grid.$element.append(this.$bbox = $("<div style='position:absolute;z-index:999;border:1px solid #F00;opacity: 0.5;'></div>"));
				this.grid.$element.append(this.$bbox2 = $("<div style='position:absolute;z-index:999;border:1px solid #0F0;opacity: 0.5;'></div>"));
			}
			this.$prevNeib = null;
			this.$nextNeib = null;

			var $dragTable = $("<table class='table table-bordered table-condensed'></table>");
			$dragTable.append(this.$origRow.clone());

			this.$dragObj = $("<div class='drag-object' style='position:absolute;'></div>");
			this.$dragObj.append($dragTable);
			this.$dragObj.offset({
				left: e.pageX + this.clickCoords.x,
				top: e.pageY + this.clickCoords.y
			});
			$dragTable.width(this.$origRow.width() + 1);
			this.grid.$element.append(this.$dragObj);

			var oCells = this.$origRow.find('td');
			var cells = $dragTable.find('td');

			for (var i = 0; i < oCells.length; i++) {
				var oCell = $(oCells[i]);
				var cell = $(cells[i]);

				cell.width(oCell.width());
				cell.height(oCell.height());
			};

			this.$dropArea = this.$dropAreaProto = this.$origRow.clone();
			this.$dropAreaProto.find('td').each(function() {
				var cont = $("<div class='wrapper'></div>");
				cont.append($(this).html());
				$(this).html(cont);

			});
			this.$dropArea.addClass('drop-area');
			this.$dropArea.insertAfter(this.$origRow);

			this.$orderPreserver = $("<tr><td colspan='" + oCells.length + "'></td></tr>");
			this.$orderPreserver.insertBefore(this.$origRow);
			this.$orderPreserver.hide();

			this.$widthPreserverProto = this.$dropArea.clone();
			this.$widthPreserverProto.addClass('width-preserver');

			this.$origRowHeight = this.$origRow.height();
			this.$origRow.hide();

			var self = this;

			this._mouseMove = function(ev) { self.mouseMove(ev); };
			this._mouseUp = function(ev) { self.mouseUp(ev); };

			$(document).on('mousemove', this._mouseMove);
			$(document).on('mouseup', this._mouseUp);

			e.preventDefault();
		};

		this.mouseMove = function(e) {

			this.$dragObj.offset({
				left: e.pageX + this.clickCoords.x,
				top: e.pageY + this.clickCoords.y
			});

			if (this.tableBBox.hitX(e.pageX) == 0) {

				if (this.$dropArea) {
					if (!this.currentBBox) {
						this.currentBBox = new BoundingBox(this.$dropArea);
						//this.currentBBox.expand(0, 5);
					}

					this.currentBBox.updateVisual(this.$bbox);

					if (this.currentBBox.hitY(e.pageY) == 0)
						return;
				}

				var newNeib = this.findNewNeib(e.pageY);

				if (newNeib.prev != this.$prevNeib || newNeib.next != this.$nextNeib) {
					this.$prevNeib = newNeib.prev;
					this.$nextNeib = newNeib.next;
					this.currentBBox = null;
				}

				if (newNeib.hit === 0) {
					this.toggleDropArea(true);
					return;
				}
			}

			this.toggleDropArea(false);

			var paginatorClickedA = null;
			var self = this;

			this.paginatorsBBoxes().each(function() {
				if (!this.hit(e.pageX, e.pageY))
					return;

				this.$elem.find('li a').each(function() {
					var bbox = new BoundingBox(this);
					if (bbox.hit(e.pageX, e.pageY)) {

						if (self.paginatorClickedA != bbox.$elem) {

							if (self.paginatorTimeout)
								clearTimeout(self.paginatorTimeout);

							paginatorClickedA = bbox.$elem;
							self.paginatorTimeout = setTimeout(function() {
								if (self.paginatorClickedA == bbox.$elem)
									bbox.$elem.trigger('click');
							}, 500);

							return false;
						}
					}
					return null;
				});
			});

			this.paginatorClickedA = paginatorClickedA;
		};

		this.mouseUp = function() {
			this.$origRow.show();
			this.$orderPreserver.remove();
			if (this.$widthPreserver) {
				this.$widthPreserver.remove();
				this.$widthPreserver = null;
			}
			if (this.$bbox) this.$bbox.remove();
			if (this.$bbox2) this.$bbox2.remove();

			this.$dragObj.remove();

			if (this.$dropArea) {
				do {
					var neib, after = false;
					if (this.$prevNeib) {
						this.$origRow.insertAfter(this.$prevNeib);
						neib = this.$prevNeib.data('key');
						after = true;
					} else if (this.$nextNeib) {
						this.$origRow.insertBefore(this.$nextNeib);
						neib = this.$nextNeib.data('key');
					} else
						break;

					this.grid.showCurtain(true);

					var self = this;
					this.grid.query("Reorder", { key: this.$origRow.data('key'), neib: neib, after: after }, function() {
						self.grid.reloadGrid();
					});
				} while (false);
				this.$dropArea.remove();
				this.$dropArea = null;
			}

			$(document).off('mouseup', this._mouseUp);
			$(document).off('mousemove', this._mouseMove);
		};

		this.toggleDropArea = function(show) {
			if (show) {
				this.$dropArea = this.$dropAreaProto;

				if (this.$prevNeib)
					this.$dropArea.insertAfter(this.$prevNeib);
				else
					this.$dropArea.insertBefore(this.$nextNeib);

				this.$dropArea.show();

				this.toggleWidthPreserver(false);

			} else if (this.$dropArea) {
				this.$dropArea.remove();
				this.$dropArea = null;

				this.toggleWidthPreserver(true);
			}
		};

		this.toggleWidthPreserver = function(show) {

			if (show) {
				if (!this.$widthPreserver) {
					this.$widthPreserver = this.$widthPreserverProto;
					this.$origRow.parent().append(this.$widthPreserver); //TODO: можно его просто скрывать, а не добавлять каждый раз
				}
			} else if (this.$widthPreserver) {
				this.$widthPreserver.remove();
				this.$widthPreserver = null;
			}
		};

		this.findNewNeib = function(y) {
			var res = {
				prev: null,
				next: null,
				hit: null,
			};

			var moveDown = this.currentBBox && y > this.currentBBox.bottom;
			var bbox = new BoundingBox();
			bbox.left = this.tableBBox.left;
			bbox.right = this.tableBBox.right;

			var rows = this.allRows();
			if (rows.length == 0) {
				return res;
			}

			var first = rows[0];
			var last = rows[rows.length - 1];
			var top = first.offset().top;
			var height = last.offset().top + last.height() - top;
			var dy = y - top;
			var id = Math.round(((rows.length - 1) * dy) / height);

			var prevHit = 0;

			do {

				if (id < 0) {
					bbox.top = top;
					res.next = first;
				} else if (id >= rows.length) {
					bbox.top = last.offset().top + last.height();
					res.prev = last;
				} else {
					bbox.top = rows[id].offset().top;

					if (moveDown)
						res.prev = rows[id];
					else
						res.next = rows[id];
				}

				bbox.bottom = bbox.top + this.$origRowHeight;
				bbox.expand(0, 3);

				res.hit = bbox.hitY(y);

				if (res.hit === 0)
					break;

				if (prevHit == -res.hit) {
					res.hit = 0;
					break;
				}

				prevHit = res.hit;
				id += res.hit;
			} while (id >= 0 && id <= rows.length);

			bbox.updateVisual(this.$bbox2);

			return res;
		};
	};


	var BoundingBox = function(elem) {

		if (elem) {
			this.$elem = elem instanceof $ ? elem : $(elem);
			this.left = this.$elem.offset().left;
			this.top = this.$elem.offset().top;
			this.right = this.left + this.$elem.outerWidth();
			this.bottom = this.top + this.$elem.outerHeight();
		} else {
			this.$elem = null;
			this.left = this.top = 1;
			this.right = this.bottom = -1;
		}

		this.hit = function(x, y) {
			return this.hitX(x) == 0 && this.hitY(y) == 0;
		};
		this.hitX = function(x) {
			if (x < this.left)
				return -1;
			if (x > this.right)
				return 1;
			return 0;
		};
		this.hitY = function(y) {
			if (y < this.top)
				return -1;
			if (y > this.bottom)
				return 1;
			return 0;
		};

		this.expand = function(dx, dy) {
			if (dx) {
				this.left -= dx;
				this.right += dx;
			}
			if (dy) {
				this.top -= dy;
				this.bottom += dy;
			}

		};

		this.updateVisual = function(visual) {
			if (visual) {
				visual.offset({
					left: this.left,
					top: this.top
				});
				visual.outerWidth(this.right - this.left);
				visual.outerHeight(this.bottom - this.top);
			}
		};
	};

	var BrowserDetect = {
		init: function() {
			this.browser = this.searchString(this.dataBrowser) || "Other";
			this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "Unknown";
		},
		searchString: function(data) {
			for (var i = 0; i < data.length; i++) {
				var dataString = data[i].string;
				this.versionSearchString = data[i].subString;

				if (dataString.indexOf(data[i].subString) !== -1) {
					return data[i].identity;
				}
			}
			return null;
		},
		searchVersion: function(dataString) {
			var index = dataString.indexOf(this.versionSearchString);
			if (index === -1) {
				return null;
			}

			var rv = dataString.indexOf("rv:");
			if (this.versionSearchString === "Trident" && rv !== -1) {
				return parseFloat(dataString.substring(rv + 3));
			} else {
				return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
			}
		},

		dataBrowser: [
			{ string: navigator.userAgent, subString: "Chrome", identity: "Chrome" },
			{ string: navigator.userAgent, subString: "MSIE", identity: "Explorer" },
			{ string: navigator.userAgent, subString: "Trident", identity: "Explorer" },
			{ string: navigator.userAgent, subString: "Firefox", identity: "Firefox" },
			{ string: navigator.userAgent, subString: "Safari", identity: "Safari" },
			{ string: navigator.userAgent, subString: "Opera", identity: "Opera" }
		]

	};

	BrowserDetect.init();

	// BOOSTER_GRID PLUGIN DEFINITION
	// =========================

	function Plugin(option, value) {
		var result;
		var args = arguments;
		var res = this.each(function () {
			var $this = $(this);
			var data = $this.data('booster-grid');
			var options = typeof option === 'object' && option;
			var selector = options && options.selector;

			if (!data && option == 'destroy')
				return;

			if (selector) {

				if (!data)
					$this.data('booster-grid', (data = {}));

				if (!data[selector])
					data[selector] = new BoosterGrid(this, options);

			} else {
				if (!data)
					$this.data('booster-grid', (data = new BoosterGrid(this, options)));
			}
			if (typeof option === 'string' && typeof data[option] == "function") {
				result = data[option].apply(data, Array.prototype.slice.call(args, 1));
			}
		});
		return result !== undefined ? result : res;
	};

	// BOOSTER_GRID NO CONFLICT
	// ===================

	var old = $.fn.boosterGrid;
	$.fn.boosterGrid = Plugin;
	$.fn.boosterGrid.Constructor = BoosterGrid;

	$.fn.boosterGrid.noConflict = function() {
	    $.fn.boosterGrid = old;
		return this;
	};
}(jQuery);