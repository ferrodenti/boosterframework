﻿
/*----------------------------------------------------------------- onload, tree initialization --------------------- */
;
(function($) {
	"use strict";

	var TreeEditor = function(element, options) {
		this.$element = null;
		this.$tree = null;
		this.$editor = null;

		this.dirtyNodes = [];
		this.errorNodes = [];

		this.selectedNode = null;

		this.init("tree-editor", element, options);
	};

	TreeEditor.VERSION = '0.0.1';

	TreeEditor.DEFAULTS = {
		rootUrl : "",
		controllerUrl: "/booster/tree/",
		controller: "",
		context: "",
		treeNodeTypes: {},
		dataSourceUrl: "/",
		contextMenuUrl: "/",
		treeStateKey: "TreeEditor",
		useGet: false
	};

	TreeEditor.prototype.init = function(type, element, options) {
		var self = this;
		this.options = $.extend({}, TreeEditor.DEFAULTS, options);
		this.$element = $(element);

		var row = $('<div class="row"></div>');
		var cell1 = $('<div class="col-md-4"></div>');
		var cell2 = $('<div class="col-md-8 tree-editor"></div>');
		this.$tree = $('<div style="overflow-x: auto; overflow-y: hidden;"></div>');
		this.$topControls = $('<div style="width:100%; min-height: 34px; margin-bottom: 1em"></div>');
		this.$bottomControls = $('<div style="width:100%; min-height: 34px; margin-bottom: 1em"></div>');
		this.$editor = $('<form class="form-horizontal"></form>');
		this.$editor.BoosterEditor({rootUrl: this.options.rootUrl});

		cell1.append(this.$tree);
		cell2.append(this.$topControls);
		cell2.append(this.$editor);
		cell2.append(this.$bottomControls);
		row.append(cell1);
		row.append(cell2);
		this.$element.append(row);

		this.$tree.jstree({
			'core': {
				"multiple": false,
				"check_callback": true,
				'data': function (node, cb) {
					self.query("GetNodes", node.id, null, function(data) {

						if (data.dirtyNodes) {
							self.dirtyNodes = data.dirtyNodes;
							applyClass(data.nodes, data.dirtyNodes.slice(0), "dirty");
						}
						if (data.errorNodes) {
							self.errorNodes = data.errorNodes;
							applyClass(data.nodes, data.errorNodes.slice(0), "treeNodeError");
						}

						function applyClass(nodes, ids, cls) {
							$(nodes).each(function() {

								var i = $.inArray(this.id, ids);
								if (i != -1) {
									if (!this.a_attr)
										this.a_attr = {};

									if (this.a_attr.class === undefined)
										this.a_attr.class = cls;
									else
										this.a_attr.class += " " + cls;

									ids.splice(i, 1);
								}

								if (this.children && this.children.constructor === Array && ids.length > 0)
									applyClass(this.children, ids, cls);
							});
						};
						cb(data.nodes);

					});
				}
			},
			'contextmenu': {
				'items': function(node) {
					var url = self.options.contextMenuUrl + encodeURIComponent(node.id);
					var menus = $.ajax({ url: url, async: false }).responseJSON;
					self.evalActions(menus, "action", "action", "submenu");
					return menus;
				}
			},
			"types": this.options.treeNodeTypes,
			"state": { "key": self.options.treeStateKey + "" },
			"plugins": ["wholerow", "state", "contextmenu", "types", "conditionalselect"]
		})
		.on("changed.jstree", function(e, data) { self.treeNodeSelected(e, data); });

		this.treeRef = $.jstree.reference(this.$tree);
		this.$editor.on('change', '[name]', function(ev) { self.editorControlValueChanged(ev); });

		this.$topControls.on("click", "[data-nodeid]", commandClick);
		this.$bottomControls.on("click", "[data-nodeid]", commandClick);
		this.$element.on("click", ".auto-blur", function() { $(this).blur(); });

		function commandClick(ev) {
			ev.preventDefault();

			var $this = $(this);
			var script = $this.data("script");
			var cmd = $this.data("command");

			if (script && !eval(script))
				return;

			if (cmd)
				self.command(cmd, $this.data("nodeid"));
		}
	};

	TreeEditor.prototype.query = function(method, id, ext, done) {
		var params = { controller: this.options.controller, context: this.options.context, id: id };

		if (ext)
			$.extend(params, ext);

		$.ajax({
			url: this.options.controllerUrl + method,
			dataType: "json",
			method: this.options.useGet ? "get" : "post",
			data: params,
			success: done,
			error: function(data) {
				console.error("TreeEditor", method, data.responseText);
			}
		});
	};

	TreeEditor.prototype.treeNodeSelected = function(e, data) {
		if (data && data.selected && data.selected.length) {
			var selected = data.selected[0] + "";
			this.selectedNode = this.treeRef.get_node(selected);

			this.loadEditor(this.selectedNode);
		}
	};

	TreeEditor.prototype.loadEditor = function(selectedNode) {
		var self = this;
		this.$topControls.empty();
		this.$bottomControls.empty();
		this.$editor.html("<div class='spinner'>&nbsp;</div>Loading ...");

		this.query("GetEdit", selectedNode.id, null, function(data) {
			self.applyResponse(data, selectedNode.id);
		});
	};

	TreeEditor.prototype.command = function(cmd, nodeId) {
		var self = this;
		this.query("Command", nodeId, {command: cmd}, function(data) {
			self.applyResponse(data, nodeId);
		});

	};

	TreeEditor.prototype.editorControlValueChanged = function(ev) {
		var self = this;
		var $target = $(ev.target);
		var values = {};

		var value = this.$editor.BoosterEditor("сontrolValue", $target);
		var def = $target.data("default");

		if (def && value == def)
			this.$editor.BoosterEditor("сontrolValue", $target, "");

		values[$target.attr('name')] = value;

		this.query("Update", this.selectedNode.id, {
			values: JSON.stringify(values)
		}, function(data) {
			self.applyResponse(data, self.selectedNode.id);
		});
	};

	TreeEditor.prototype.applyResponse = function(data, nodeId) {
		var self = this, n, i;
		try {
			if (data.reload) {
				var reselectedPath = [];

				var afterRefresh = function() {
					for (i = 0; i < reselectedPath.length; i ++) {
						n = self.treeRef.get_node(reselectedPath[i]);
						if (n) {
							self.treeRef.deselect_all();
							self.treeRef.select_node(n);
							break;
						}
					}
				};

				n = this.selectedNode;
				while (n) {
					reselectedPath.push(n.id);
					n = this.treeRef.get_node(n.parent);
				}

				if (data.reload == "#") {
					this.treeRef.refresh(true);
					this.$tree.one("refresh.jstree", afterRefresh);
				} else {
					this.treeRef.refresh_node(this.treeRef.get_node(data.reload));
					this.$tree.one("refresh_node.jstree", afterRefresh);
				}

				
				return;
			}

			if (data.commands) {
				var sortedCommands = {};

				for (i = 0; i < data.commands.length; i++) {
					var cmd = data.commands[i];
					if (!sortedCommands[cmd.pos])
						sortedCommands[cmd.pos] = [cmd];
					else {
						if (cmd.isGroup)
							sortedCommands[cmd.pos].unshift(cmd);
						else
							sortedCommands[cmd.pos].push(cmd);
					}
				}
				this.$topControls.empty();
				this.$bottomControls.empty();


				if (sortedCommands.topLeft)
					this.$topControls.append("<div class='pull-left'>" + createButtons(sortedCommands.topLeft) + "</div>");
				if (sortedCommands.topRight)
					this.$topControls.append("<div class='pull-right'>" + createButtons(sortedCommands.topRight) + "</div>");
				if (sortedCommands.top)
					this.$topControls.append("<div>" + createButtons(sortedCommands.top) + "</div>");
				if (sortedCommands.bottomLeft)
					this.$bottomControls.append("<div class='pull-left'>" + createButtons(sortedCommands.bottomLeft) + "</div>");
				if (sortedCommands.bottomRight)
					this.$bottomControls.append("<div class='pull-right'>" + createButtons(sortedCommands.bottomRight) + "</div>");
				if (sortedCommands.bottom)
					this.$bottomControls.append("<div>" + createButtons(sortedCommands.bottom) + "</div>");
			}

			if (data.schema)
				this.$editor.BoosterEditor("applySchema", data.schema);

			if (data.dirtyControls) {

				for (i = 0; i < data.dirtyControls.length; i++)
					data.dirtyControls[i] = data.dirtyControls[i].toLowerCase();

				this.$editor.find('[name]').each(function() {
					var $this = $(this);
					var container = $this.parents(".form-group");
					if (container.length == 0)
						return;

					var name = $this.attr('name').toLowerCase();
					var dirty = false;
					for (i = 0; i < data.dirtyControls.length; i++)
						if (data.dirtyControls[i] == name) {
							dirty = true;
							break;
						}

					if (dirty)
						container.addClass("has-warning");
					else
						container.removeClass("has-warning");
				});
			}


			if (data.validation)
				this.$editor.BoosterEditor("showValidationMessages", data.validation);
			else
				this.$editor.BoosterEditor("clearValidationMessages");

			if (data.newNodes)
				$(data.newNodes).each(function() {
					self.treeRef.create_node(this.parentId, this, "last");
				});

			if (data.dirtyNodes) {
				updateNodes(this.dirtyNodes, data.dirtyNodes, "dirty");
				this.dirtyNodes = data.dirtyNodes;
			} 

			if (data.errorNodes) {
				updateNodes(this.errorNodes, data.errorNodes, "treeNodeError");
				this.errorNodes = data.errorNodes;
			} 

			if (data.changedNodes)
				for (i in data.changedNodes) {
					n = this.treeRef.get_node(i);
					this.treeRef.set_text(n, data.changedNodes[i]);
				}

			if (data.removedNodes)
				$(data.removedNodes).each(function() {
					n = self.treeRef.get_node(this);

					if (!data.redirect && n == self.selectedNode)
						data.redirect = self.treeRef.get_parent(n);

					self.treeRef.delete_node(n);
				});

			if (data.redirect) {
				n = self.treeRef.get_node(data.redirect);
				self.treeRef.deselect_all();
				self.treeRef.select_node(n);
			}
			if (data.script) {
				eval(data.script);
			}
		} catch (exception) {
			console.error("TreeEditor.applyResponse", exception);
		}

		function updateNodes(oldIds, ids, cls) {

			$(ids).each(function() {
				i = $.inArray(this, oldIds);

				if (i > -1)
					oldIds.splice(i, 1);
				else {
					n = self.treeRef.get_node(this);
					if (n) {
						if (n.a_attr['class'] == undefined)
							n.a_attr['class'] = cls;
						else
							n.a_attr['class'] += " " + cls;

						self.treeRef.redraw_node(n, false);
					}
				}
			});

			$(oldIds).each(function() {
				n = self.treeRef.get_node(this);
				if (n && n.a_attr['class'] != undefined) {
					n.a_attr['class'] = n.a_attr['class'].split(cls).join("");

					self.treeRef.redraw_node(n, false);
				}
			});
		}
		function createButtons(arr) {
			var list = "";
			var btns = "";
			var first;
			
			$(arr).each(function() {
				if (this.group)
					first = this;
				else if (this.customHtml) {
					list += "<li>" + this.customHtml + "</li>";
					btns += this.customHtml;
				} else {
					attrs = getAttrs(this);
					var cont = getIcon(this) + this.caption;

					list += "<li><a href='#'" + attrs + ">" + cont + "</a></li>";
					btns += "<button type='button' class='btn btn-" + this.class + "'" + attrs + ">" + cont + "</button>&nbsp";
				}
			});


			if (first) {
				if (list.length == 0) {
					if (first.customHtml)
						return first.customHtml;

					return "<button type='button' class='btn btn-" + first.class + "'" + getAttrs(first) + ">" + getIcon(first) + first.caption + "</button>";
				}

				var attrs = getAttrs(first);

				if (attrs.length > 0)
					return "<div class='btn-group'><button type='button' class='btn btn-" + first.class + " auto-blur' " + attrs + ">" + getIcon(first) + first.caption + "</button>" +
						"<button type='button' class='btn btn-" + first.class + " dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>" +
						"<span class='caret'></span><span class='sr-only'>Toggle Dropdown</span></button><ul class='dropdown-menu' role='menu'>" +
						list +
						"</ul></div>";

				return "<div class='btn-group'><button type='button' class='btn btn-" + first.class + " dropdown-toggle' " + attrs + " data-toggle='dropdown' aria-expanded='false'>" +
					getIcon(first) + first.caption +
					"&nbsp;<span class='caret'></span></button><ul class='dropdown-menu' role='menu'>" +
					list +
					"</ul></div>";
			}
			return btns;

			function getIcon(c) {
				return c.icon ? "<i class='" + c.icon + "'></i>&nbsp;" : "";
			}
			function getAttrs(c) {
				attrs = "";

				if (c.command || c.script)
					attrs += " data-nodeid='" + nodeId + "'";
				if (c.command)
					attrs += " data-command='" + c.command + "'";
				if (c.script)
					attrs += " data-script='" + c.script + "'";

				return attrs;
			}
		}
	};

	
	TreeEditor.prototype.evalActions = function(obj, actStrProp, actProp, childProp) {
		var funk = 1;
        for (var k in obj) {
            if (obj[k][actProp]) {
                eval("funk=function(){" + obj[k][actStrProp] + "}");
                obj[k][actProp] = funk;
            }
            if (obj[k][childProp])
                evalActions(obj[k][childProp], actStrProp, actProp, childProp);
        }
    };

	function Plugin(option) {
		var result;
		var args = arguments;
		var res = this.each(function() {
			var $this = $(this);
			var data = $this.data('tree-editor');
			var options = typeof option === 'object' && option;
			var selector = options && options.selector;

			if (!data && option == 'destroy')
				return;

			if (selector) {

				if (!data)
					$this.data('tree-editor', (data = {}));

				if (!data[selector])
					data[selector] = new TreeEditor(this, options);

			} else {
				if (!data)
					$this.data('tree-editor', (data = new TreeEditor(this, options)));
			}

			if (typeof option === 'string' && typeof data[option] == "function") {
				result = data[option].apply(data, Array.prototype.slice.call(args, 1));
			}
		});
		return result !== undefined ? result : res;
	};

	$.fn.TreeEditor = Plugin;
	$.fn.TreeEditor.Constructor = TreeEditor;

})(jQuery);


