﻿
/*----------------------------------------------------------------- onload, tree initialization --------------------- */

$(function() {
    var evalActions = function(obj, actStrProp, actProp, childProp) {
        for (var k in obj) {
            if (obj[k][actProp]) {
                eval("var funk=function(){" + obj[k][actStrProp] + "}");
// ReSharper disable once UseOfImplicitGlobalInFunctionScope
                obj[k][actProp] = funk;
            }
            if (obj[k][childProp])
                evalActions(obj[k][childProp], actStrProp, actProp, childProp);
        }
    };

    $('#tree').jstree({
        'core': {
            "multiple": false,
            "check_callback": true,
            'data': {
                'url': function ( /*node*/) { return window.dataSourceUrl; },
                'data': function(node) { return { 'id': node.id }; }
            }
        },
        'contextmenu': {
            'items': function(node) {
                var url = window.contextMenuUrl + encodeURIComponent(node.id);

                var menus = $.ajax({ url: url, async: false }).responseJSON;

                evalActions(menus, "action", "action", "submenu");

                return menus;
            }
        },
        "types": {
            "default": {},
            "hosts": { "icon": "glyphicon glyphicon-folder-open" },
            "host": { "icon": "fa fa-desktop" },
            "service": { "icon": "fa fa-cogs" },
            "configNode": {},
            "settings": { "icon": "glyphicon glyphicon-cog" },
            "schemas": { "icon": "glyphicon glyphicon-folder-open" },
            "schema": { "icon": "fa fa-file-code-o" },
            "schemaNode": { "icon": false },
            "users": { "icon": "fa fa-users" },
            "user": { "icon": "fa fa-user" },
            "error": { "icon": "glyphicon glyphicon-ban-circle" },
            "plugin": { "icon": "fa fa-cogs" },
            "method": { "icon": "fa fa-comments-o" }
       },
        "state": { "key": window.treeStateKey + "" },
        "plugins": ["wholerow", "state", "contextmenu", "types", "conditionalselect"]
    }).on("changed.jstree", function(e, data) {

        if (data.action != "select_node")
            return;

        var ref = $.jstree.reference(e.target);
        var selected = data.selected[0] + "";
        var selNode = ref.get_node(selected);

        var loadEditor = function(res) {
            if (!res) { // Сервер вернул ошибки
                ref.deselect_all();
                ref.noLoad = true;
                ref.select_node(ref.prevSelected);
                return;
            }

            window.editorController = {};

            var editor = $("#editor");
            editor.html("<div class='spinner'>&nbsp;</div>Loading ...");

            $.get(window.rootUrl + "Load/index?id=" + encodeURIComponent(selNode.id), function (d) {
                if (!ref.get_node(selNode.id)) { // Был загружен удаленный нод
                    ref.select_node(ref.prevSelected);
                    return;
                }
                d = $(d);
                var fs = d.find("fieldset[disabled]");
                if (fs.length > 0)
                    fs.find("input,select,button,a").attr("disabled", "disabled");

               editor.html(d);

                window.currentTreeNodeId = selected;

				editor.find('.selectpicker').selectpicker();
				editor.find('div.date').datepicker();
				editor.find("div.intPicker").intPicker();

                $("[data-initial]").each(function(i, ctrl) {
                    var elem = $(ctrl);
                    elem.data("prev-value", getControlValue(elem));
                    elem.on("change", function() { controlValueChanged(this); });

                    var validation = elem.data("validation");
                    //if (validation || elem.attr("current-tree-node-name") !== undefined) { //TODO: не все поля требуют валидации на лету
                        try {
                            var v = "";
                            if (!validation)
                                v = function () { return true; };
                            else
                                eval("v=window.editorController." + validation);

                            elem.on("keyup", function() { controlValidation($(this), v); });
                            elem.on("change", function() { controlValidation($(this), v); });
                        } catch (_) {

                        }
                    //}
                    controlValueChanged(ctrl, true);
                });

                updateSaveControls(true);

                ref.prevSelected = selected;
            });
        };
        if (ref.noLoad)
            ref.noLoad = false;
        else
            postEditorForm(null, loadEditor);
            

//        if (ref.prevSelected != selected)
//            postEditorForm(null, loadEditor);
//        else if (ref.noLoad)
//            ref.noLoad = false;
//        else
//            loadEditor(true);
    });
});

/*----------------------------------------------------------------- nodes --------------------- */

function selectNode(id) {
    var ref = $.jstree.reference('#tree');

    if (!id) {
        id = ref.get_selected();
        if (id.length > 0)
            id = id[0];

        //ref.prevSelected = null;
    }
    ref.deselect_all();

    if (id)
        ref.select_node(id);
}
function deleteNode(id, text) {
    if (confirm("Вы уверенны? " + text + "?"))
        $.get(window.rootUrl + "Delete/index?id=" + encodeURIComponent(id), serverResponse);
}

function insertNode(id, param) {
    var node = $.jstree.reference('#tree').get_node(id);

    postEditorForm(null, function(success) {
        var url = window.rootUrl + "Insert/index?id=" + encodeURIComponent(node.id);

        if (param && param.length > 0)
            url += "&param=" + param;

        $.get(url, serverResponse);
    });

    return false;
}

function updateNode(id, node) {
    var ref = $.jstree.reference('#tree');
    var n = ref.get_node(id);

    if (n) {
        if (node) {
            if (node.parent) {
                ref.create_node(n, node, typeof node.pos === "undefined" ? "last" : node.pos);
            } else {
                if (node.id) setNodeId(ref, n, node.id);
                if (node.text) ref.set_text(n, node.text);
            }
        } else
            ref.delete_node(n);
    }
}

function refreshNode(id) {
    var ref = $.jstree.reference('#tree');
    var n = ref.get_node(id);
    if (n && n.state.loaded)
        ref.refresh_node(n);
}

function setNodeId(ref, node, newId) {
    var oldId = node.id;
    if (oldId == newId)
        return;
    if (ref.prevSelected == oldId)
        ref.prevSelected = newId;
    if (window.currentTreeNodeId == oldId)
        window.currentTreeNodeId = newId;

    ref.set_id(node, newId);

    var i = oldId.indexOf('/') + 1;
    if (i > 0 && oldId.substring(0, i) == newId.substring(0, i)) {
        oldId = oldId.substring(i);
        newId = newId.substring(i);
    }

    var updateChildsReq = function (n1) {
        $(n1.children).each(function (_, id) {
            var firstSeg = "";
            var j = id.indexOf('/');
            if (j > 0)
                firstSeg = id.substring(0, ++j);
            else
                j = 0;

            var n2 = ref.get_node(id);
            if (id.indexOf(oldId, j) == j)
                ref.set_id(n2, firstSeg + newId + id.substring(j + oldId.length));

            updateChildsReq(n2);
        });
    };
    updateChildsReq(node);
}

function setNodesDirty(id, value) {
    var ref = $.jstree.reference('#tree');

    while(id && id != "#") {
        var node = ref.get_node(id);
        if (!node)
            return;

        var a = $(document.getElementById(id)).find('a');
        if (a.length > 0)
            a = $(a[0]);

        if (a.hasClass("dirty") == value)
            return;

        if (value) {
            a.addClass("dirty");
            node.a_attr.class = "dirty";
        } else {
            for (var i = 0; i < node.children.length; i++) {
                var b = $(document.getElementById(node.children[i])).find("a");
                if (b.length > 0) {
                    b = $(b[0]);
                    if (b.hasClass("dirty"))
                        return;
                }
            }

            a.removeClass("dirty");
            node.a_attr.class = null;
        }
        id = node.parent;
    };
}

/*----------------------------------------------------------------- form posting --------------------- */

function postEditorForm(action, postback) {
    var form = $(".form-horizontal");
    if (form.length == 0 || !document.getElementById(window.currentTreeNodeId)) {

        if (postback)
            postback(true);

        return;
    }
    var url = "/Update/Index?id=" + encodeURIComponent(window.currentTreeNodeId);

    //TODO: Validate current editor

    var dict = {};
    var cnt = 0;

    if (action) {
        dict["action"] = action;
        cnt++;
    }

    var pstCtrls = [];
    $("[data-initial]").each(function (i, e) {
        var elem = $(e);
        var val = getControlValue(elem);
        var prev = elem.data("prev-value") + "";

        if (val != prev) {
            dict[e.name] = val;
            cnt++;

            pstCtrls.push([elem, val]);
        }
    });

    if (cnt > 0) {
        $.ajax({
            url: url,
            data: dict,
            type: 'POST',
            success: function (res) {

                $(pstCtrls).each(function (i, e) {
                    e[0].data("prev-value", e[1]);
                });

                var success = serverResponse(res);
                if (postback)
                    postback(success);
            }
        });

    } else if (postback)
        postback(true);
}
function serverResponse(data) {
    eval("var res=" + data + "();");
    return res;
}
function saveConfigs(action) {
    postEditorForm(action);
}


/*----------------------------------------------------------------- controls --------------------- */
function getControlValue(elem) {
    if (elem.hasClass("selectpicker")) {
        if (elem.attr("multiple") == "multiple")
            return JSON.stringify(elem.val()).replace(/"/g, "'");

        return elem.val();
    }

    return elem.attr("type") == "checkbox" ? (elem.prop('checked') ? "true" : "false") : elem.val();
}

function controlValueChanged(control, isInit) {
    var elem = $(control);
    var container = elem.parents(".form-group");
    var val = getControlValue(elem);
    var init = elem.data("initial") + "";

    if (val == init)
        container.removeClass("has-warning");
    else
        container.addClass("has-warning");


    if (typeof window.editorController.updateControls == "function")
        window.editorController.updateControls(control.name, val);

    if (!isInit) {
        showValidationMessage(control.name, null);

        updateSaveControls(false);
    }
}

function updateSaveControls(isInit) {
    var form = $(".form-horizontal");
    if (form.length > 0) {
        var container = $("#saveContainer");
        container.empty();

        var changed = form.find(".has-warning").length > 0 || form.find("[name='isNewElement']").val() == "true";

        var lis = [];

        $("#saveUL").find("li").each(function (i, e) {
            var el1 = $(e);
            if (el1.data("dirty") || (changed && el1.data("current")))
                lis.push(el1);
        });

        if (lis.length == 1) {
            var el = lis[0];
            var a = el.find("a")[0];
            container.append("<button type='button' class='btn btn-danger' onclick=\"" + $(a).attr("onclick") + "\"><i class='fa fa-floppy-o'></i>&nbsp;" + a.innerHTML + "</button>");
        }
        else if (lis.length > 1) {
            container.append("<button type='button' class='btn btn-danger' onclick='saveConfigs(\"saveAll\")'><i class='fa fa-floppy-o'></i>&nbsp;Сохранить все изменения</button>\
            <button type='button' class='btn btn-danger dropdown-toggle' data-toggle='dropdown'>\
                <span class='caret'></span>\
                <span class='sr-only'>Toggle Dropdown</span>\
            </button>");
            var ul = $("<ul class='dropdown-menu' role='menu'>");
            container.append(ul);

            $(lis).each(function (i, e) {
                ul.append(e[0].outerHTML);
            });
        }
        if (lis.length > 0 && $("#deleteNode").length == 0 && $("#saveContainerOffset").length == 0)
            $("<span id='saveContainerOffset'><br/><br/></span>").insertAfter(container);

        if (!isInit && window.currentTreeNodeId)
            setNodesDirty(window.currentTreeNodeId, changed);
    }
}
/*----------------------------------------------------------------- ClientServerTask --------------------- */

function clientServerTask(url, id, resultId, postForm) {
    if (postForm === undefined)
        postForm = true;

    var result = $("#" + resultId);

    var callBack = function (res) {
        eval("res=" + res);

        switch (res.state) {
            case "working":
                result.attr("class", "alert alert-info");
                result.html("<div class='spinner'>&nbsp;</div>" + res.message);
                setTimeout(function () { $.get(window.rootUrl + "Load/GetClientServerTask?taskId=" + id, callBack); }, 300);
                break;
            case "error":
                result.attr("class", "alert alert-danger");
                result.html("Ошибка: " + res.message);
                break;
            case "done":
                result.attr("class", "alert alert-success");
                result.html(res.message ? res.message : "OK!");
        }
    };

    var doTask = function () { $.get(url.replace("__TASKID__", id), callBack); };

    if (postForm)
        postEditorForm(null, function(success) {
            if (!success) {
                result.empty();
                result.attr("class", "");
                return;
            }
            doTask();
        });
    else
        doTask();
}
/*----------------------------------------------------------------- validation --------------------- */
function controlValidation(elem, valdation) {
    var val = getControlValue(elem);
    var res = true;

    if (typeof valdation == "function")
        res = valdation(val);

    if (res !== true)
        showValidationMessage(elem, res);
    else {
        if (elem.attr("required") && (val + "").length == 0){
            showValidationMessage(elem, "Поле не может быть пустым");
            return;
        }

        showValidationMessage(elem, null);

        if (elem.attr("current-tree-node-name") !== undefined)
            updateCurrentTreeNodeName(val);
    }
};

function updateCurrentTreeNodeName(val) {

    if (!val) {
        var ctrl = $("[current-tree-node-name]");
        if (ctrl.length == 0)
            return;

        val = getControlValue(ctrl);
    }

    var bval = val;
    if (typeof window.editorController.prepareBreadcrumbName == "function")
        bval = window.editorController.prepareBreadcrumbName(bval);

    $("#breadcrumbCurrentNode").html(bval);

    if (window.currentTreeNodeId) {
        if (typeof window.editorController.prepareNodeName == "function")
            val = window.editorController.prepareNodeName(val);

        updateNode(window.currentTreeNodeId, { text: val });
    }
} 

function showValidationMessage(elem, message) {

    if (typeof elem === "string")
        elem = $("[name='" + elem + "']");

    var div = elem.closest('.form-group');

    if (!message) {
        div.find('span.help-block').remove();
        div.removeClass("has-error");
    } else {
        div.addClass("has-error");

        var help = div.find('span.help-block');
        if (help.length == 0) {
            help = $("<span class=\"help-block\"></span>");
            elem.closest('div').append(help);
        }
        help.html(message);
    }
}

function validateHttpPort(port) {
    if (port < 1024 || (port & 0xFFFF) != port) {
        return "Значение должно быть в диапазоне 1024...65535";
    }
    return true;
}


/*----------------------------------------------------------------- utility --------------------- */
function isUpper(str) {
    return str == str.toUpperCase();
}

function camelCaseToSpaced(str, upFirstLetter) {
    if (!str)
        return "";

    if (upFirstLetter !== false)
        upFirstLetter = true;

    var res = "";

    if (upFirstLetter)
        res += str[0].toUpperCase();

    var lastUp = upFirstLetter;
    var abbr = false;

    for (var i = upFirstLetter ? 1 : 0; i < str.length; i++) {
        var ch = str[i];

        if (isUpper(ch)) {
            var nextUp = i < str.length - 1 && isUpper(str[i + 1]) && (i == str.length - 2 || isUpper(str[i + 2]));

            if (!lastUp)
                res += " ";

            if (!lastUp && !nextUp) {
                res += ch.toLowerCase();
                continue;
            }
            if (nextUp || abbr) {
                res += ch.toUpperCase();
                abbr = nextUp;
                lastUp = nextUp;
                continue;
            }
            lastUp = true;
        } else
            lastUp = false;

        res += ch.toLowerCase();
    }
    return res;
}