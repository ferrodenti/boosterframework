;(function($) {
	"use strict";

	var FileBrowser = function(element, options) {
		var self = this;
		this.$target = $(element);
		this.options = $.extend({}, FileBrowser.DEFAULTS, options, this.$target.data());
		var parent = this.$target.parent();

		if (!parent.hasClass('input-group')) {
			var div = $("<div class='input-group'></div>");
			parent.append(div);
			div.append(this.$target);
		}

		this.$button = parent.find("span button");
		if (!this.$button) {
			var span = $("<span class='input-group-btn'></span>");
			parent.append(span);
			this.$button = $("<button class='btn btn-default' type='button'>...</button>");
			span.append(this.$button);
		}

		this.$button.on('click', function() { self.browseClicked(); });
		
		
	};

	FileBrowser.VERSION = '0.0.1';

	FileBrowser.DEFAULTS = {
		controllerUrl: "/booster/fileBrowser/",
		controller: null,
		context: null,
		treeStateKey: "file-browser",
		useGet: false
	};


	FileBrowser.prototype.browseClicked = function() {
		var self = this;

		var isFilePath = this.options.type == "filepath";

		if(!this.$modal) {
			this.$modal = $("<div class='modal fade'>" +
            "<div class='modal-dialog'>" +
            "<div class='modal-content'>" +
            "<div class='modal-header'>" +
            "<button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Закрыть</span></button>" +
            "<h4 class='modal-title'></h4>" +
            "</div>" +
            "<div class='modal-body'>" +
            "</div>" +
            "<div class='modal-footer'>" +
            "<button type='button' class='btn btn-default' data-dismiss='modal'>Отмена</button>" +
            "<button name='btnSelect' type='button' class='btn btn-primary' disabled>Выбрать</button>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "</div>");

			$(document.body).append(this.$modal);
			this.$modal.find(".btn-primary").on("click", function() {
				var old = self.$target.val();
				if (old != self.currentValue) {
					self.$target.val(self.currentValue);
					self.$target.trigger("change");
				}
				self.$modal.modal('hide');
			});
			
			var title = this.$modal.find(".modal-title");
			var body = this.$modal.find(".modal-body");
		
			title.html(isFilePath ? "Выберите файл" : "Выберите папку");
		
			var divRow = $("<div class='row'></div>");
    		body.append(divRow);
    		var leftCol = $("<div class='" + (isFilePath? "col-md-6" : "col-md-12") + "' style='padding:0'></div>");
    		this.$treeLeft = $("<div style='overflow-x: auto; overflow-y: auto; height: 30em;'></div>");
    	
    		divRow.append(leftCol);
    		leftCol.append(this.$treeLeft);

			if (isFilePath) {
				var rightCol = $("<div class='col-md-6' style='padding:0'></div>");

				this.$treeRight = $("<div style='overflow-x: auto; overflow-y: auto; height: 30em;'></div>");
				divRow.append(rightCol);
				rightCol.append(this.$treeRight);
			}
		}
    	this.$modal.modal('show');
		this.currentValue = this.$target.val();

		if (this.currentValue == "")
			this.currentValue = this.$target.attr("placeholder");

		if (this.folders) {
			this.folders.destroy();
			this.folders = null;
		}

		this.folders = this.initBrowserTree(this.$treeLeft, { getFolders: true, currentPath: this.currentValue }, null, !isFilePath);

		if (isFilePath) {

			this.$treeLeft.on("dblclick.jstree", function() {
				var id = self.getSelectedNode(self.folders);
				if (id != null)
					self.folders.toggle_node(id);
			});

			if (this.files) {
				this.files.destroy();
				this.files = null;
			}

			this.selectedFolder = null;

			this.$treeLeft.on("changed.jstree", function() {
				var sel = self.getSelectedNode(self.folders);

				if (self.selectedFolder != sel) {
					self.selectedFolder = sel;

					if (self.files)
						self.files.destroy();

					self.files = self.initBrowserTree(self.$treeRight, { getFiles: true, currentPath: self.currentValue }, function(id) {
						return self.getSelectedNode(self.folders);
					}, true);
				}
			});


		} else
			this.$treeRight = null;
	};

	FileBrowser.prototype.treeDblClick = function(target) {
		var sel = this.updateOkButton(target);
		if (sel !== null)
			this.$modal.find(".btn-primary").click();
	};


	FileBrowser.prototype.getSelectedNode = function(ref) {
		var id = ref.get_selected();
		if (id.length == 1) {
			id = id[0];
			if (ref.get_node(id).type != "error")
				return id;
		}

		return null;
	};

	FileBrowser.prototype.updateOkButton = function(ref) {

		this.currentValue = this.getSelectedNode(ref);



		if (this.currentValue !== null) {
			if (this.currentValue.indexOf(".\\") == 0)
				this.currentValue = this.currentValue.substring(2);

			this.$modal.find(".btn-primary").removeAttr("disabled");
			return this.currentValue;
		} else {
			this.$modal.find(".btn-primary").attr("disabled", '');
			return null;
		}

	};

	FileBrowser.prototype.initBrowserTree = function(container, ext, pathGetter, bindEvents) {
		var self = this;
		var inst = $.jstree.reference(container.jstree({
			'core': {
				"multiple": false,
				'data': function(node, cb) {

					var path = pathGetter ? pathGetter(node.id) : node.id;

					if (path === null) {
						cb([]);
						return;
					}

					self.query("Get", path, ext, function(data) {
						if (data)
							cb(data);


						container.one('loaded.jstree', function() {
							setTimeout(function() {

								var sel = bindEvents ? self.updateOkButton(inst) : self.getSelectedNode(inst);

								if (sel !== null) {
									var lis = container[0].getElementsByTagName("li");
									var nd = $.grep(lis, function(li) {
										return li.id == sel;
									});

									if (nd.length == 1) {
										nd = $(nd[0]);
										var top = nd.offset().top;
										top -= container.offset().top;
										var height = container.height() - (2 * nd.find('a').height());

										if (top > height)
											top -= height;
										else if (top > 0)
											return;

										container.animate({ scrollTop: top + 'px' }, 'fast');
									}
								}
							}, 100);
						});

					});
				}
			},
			"types": {
				"default": {},
				"file": { "icon": "glyphicon glyphicon-file" },
				"folder": { "icon": "glyphicon glyphicon-folder-open" },
				"error": { "icon": "glyphicon glyphicon-ban-circle" }
			},
			"plugins": ["wholerow", "types"]
		}));

		if (bindEvents) {
			container.on("changed.jstree", function() { self.updateOkButton(inst); });
			container.on("dblclick.jstree", function() { self.treeDblClick(inst); });
		}

		return inst;
	};

	FileBrowser.prototype.query = function(method, path, ext, done) {
		var params = { controller: this.options.controller, path: path, context: this.options.context, baseDir: this.options.baseDir };

		if (ext)
			$.extend(params, ext);

		$.ajax({
			url: this.options.controllerUrl + method,
			dataType: "json",
			method: this.options.useGet ? "get" : "post",
			data: params,
			success: done,
			error: function(data) {
				console.error("FileBrowser", method, data.responseText);
			}
		});
	};

	function Plugin(option) {
		var result;
		var args = arguments;
		var res = this.each(function() {
			var $this = $(this);
			var data = $this.data('file-browser');
			
			var options = typeof option === 'object' && option;
			var selector = options && options.selector;

			if (!data && option == 'destroy')
				return;

			if (selector) {

				if (!data)
					$this.data('file-browser', (data = {}));

				if (!data[selector])
					data[selector] = new FileBrowser(this, options);

			} else {
				if (!data)
					$this.data('file-browser', (data = new FileBrowser(this, options)));
			}

			if (typeof option === 'string' && typeof data[option] == "function") {
				result = data[option].apply(data, Array.prototype.slice.call(args, 1));
			}
		});
		return result !== undefined ? result : res;
	};

	$.fn.fileBrowser = Plugin;
	$.fn.fileBrowser.Constructor = FileBrowser;

})(jQuery);


