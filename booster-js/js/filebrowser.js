﻿
function browseFolder(ctrl, host) {
    var value = ctrl.val().toLowerCase();

    if (value.toLowerCase().indexOf("%appdata%") == 0)
        value = window.appDataFolder + value.substring("%appdata%".length);

    var nodesToOpen = [];

    var j = 0;
    do {
        var i = value.indexOf('/', j);
        if (i < 0) {
            i = value.indexOf('\\', j);
            if (i < 0)
                break;
        } else {
            var k = value.indexOf('\\', j);
            if (k >= 0)
                i = Math.min(i, k);
        }
        if (i > 0) {
            nodesToOpen.push(value.substring(0, i));
        }
        j = i + 1;
    } while (true)

    var modal = getBrowserModal();
    modal.modal('show');
    modal.find(".modal-title").html("Выбор папки");

    var body = modal.find(".modal-body");
    body.empty();

    var divRow = $("<div class='row'></div>");
    body.append(divRow);
    var divLeft = $("<div class='col-md-12' style='padding:0'></div>");
    divRow.append(divLeft);

    var tree = $("<div id='browserTree' style='overflow-x: auto; overflow-y: auto; height: 30em;'></div>");
    divLeft.append(tree);

    var needOpen = true;
    tree = initBrowserTree(tree, window.browseFoldersUrl + "?host=" + host, function () {
        var id = tree.get_selected();
        okBtn.attr("disabled", id.length > 0 ? null : "disabled");
    }, function () {
        if (needOpen)
            openAllNnodes(0);

        needOpen = false;
    });

    var openAllNnodes = function (index) {
        while (index < nodesToOpen.length) {
            if (!tree.get_node(nodesToOpen[index]))
                index++;
            else {
                tree.open_node(nodesToOpen[index], function () {
                    openAllNnodes(index + 1);
                });
                return;
            }
        }
        tree.select_node(nodesToOpen[index - 1]);
    };

    var okBtn = modal.find("[name='btnSelect']");
    okBtn.on("click", function () {
        var id = tree.get_selected();
        if (id.length > 0) {
            modal.modal('hide');
            ctrl.val(id[0]);
            ctrl.trigger('change');
        }
    });
}

function browseFile(ctrl, host) {
    var value = ctrl.val().toLowerCase();

    if (value.toLowerCase().indexOf("%appdata%") == 0)
        value = window.appDataFolder + value.substring("%appdata%".length);

    var nodesToOpen = [];

    var j = 0;
    do {
        var i = value.indexOf('/', j);
        if (i < 0) {
            i = value.indexOf('\\', j);
            if (i < 0)
                break;
        } else {
            var k = value.indexOf('\\', j);
            if (k >= 0)
                i = Math.min(i, k);
        }
        if (i > 0) {
            nodesToOpen.push(value.substring(0, i));
        }
        j = i + 1;
    } while (true)

    var modal = getBrowserModal();
    modal.find(".modal-title").html("Выбор файла");
    modal.modal('show');

    var body = modal.find(".modal-body");
    body.empty();

    var divRow = $("<div class='row'></div>");
    body.append(divRow);
    var divLeft = $("<div class='col-md-6' style='padding:0'></div>");
    var divRight = $("<div class='col-md-6' style='padding:0'></div>");
    divRow.append(divLeft);
    divRow.append(divRight);

    var tree = $("<div id='browserTree' style='overflow-x: auto; overflow-y: auto; height: 30em;'></div>");
    var list = $("<div id='browserList' style='overflow-x: auto; overflow-y: auto; height: 30em;'></div>");
    divLeft.append(tree);
    divRight.append(list);

    var needOpen = true;
    tree = initBrowserTree(tree, window.browseFoldersUrl + "?host=" + host, function(id) {
        list.refresh();
    }, function() {
        if (needOpen)
            openAllNnodes(0);

        needOpen = false;
    });

    var openAllNnodes = function(index) {
        while (index < nodesToOpen.length) {
            if (!tree.get_node(nodesToOpen[index]))
                index++;
            else {
                tree.open_node(nodesToOpen[index], function() {
                    openAllNnodes(index + 1);
                });

                return;
            }
        }
        tree.select_node(nodesToOpen[index - 1]);

        setTimeout(function() {
            list.select_node(value);
        }, 700);
    };

    var okBtn = modal.find("[name='btnSelect']");
    okBtn.on("click", function() {
        var id = list.get_selected();
        if (id.length > 0) {
            modal.modal('hide');
            ctrl.val(id[0]);
            ctrl.trigger('change');
        }
    });

    list = initBrowserTree(list, function() {
        var id = tree.get_selected();
        return window.browseFilesUrl + "?host=" + host + "&path=" + id;
    }, function(e, data) {
        var id = list.get_selected();
        okBtn.attr("disabled", id.length > 0 ? null : "disabled");
    });
}

function initBrowserTree(container, url, changed, loaded) {
    var inst = container.jstree({
        'core': {
            "multiple": false,
            "check_callback": true,
            'data': {
                'dataType': 'JSON',
                'url': url,
                'data': function(node) { return { 'id': node.id }; }
            }
        },
        "types": {
            "default": {},
            "file": { "icon": "glyphicon glyphicon-file"},
            "folder": { "icon": "glyphicon glyphicon-folder-open" },
            "error": { "icon": "glyphicon glyphicon-ban-circle" }
        },
        "plugins": ["wholerow", "types", "conditionalselect"]
    });

    if( changed )
        inst.on("changed.jstree", changed);

    if (loaded)
        inst.on("loaded.jstree", loaded);

    return $.jstree.reference(inst);    
}

function getBrowserModal() {
    var res = $("#browser-modal");
    if (res.length == 0) {
        res = $("<div id='browser-modal' class='modal fade'>" +
            "<div class='modal-dialog'>" +
            "<div class='modal-content'>" +
            "<div class='modal-header'>" +
            "<button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Закрыть</span></button>" +
            "<h4 class='modal-title'></h4>" +
            "</div>" +
            "<div class='modal-body'>" +
            "</div>" +
            "<div class='modal-footer'>" +
            "<button type='button' class='btn btn-default' data-dismiss='modal'>Отмена</button>" +
            "<button name='btnSelect' type='button' class='btn btn-primary' disabled>Выбрать</button>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "</div>");

        $(document.body).append(res);
    }
    return res;
}