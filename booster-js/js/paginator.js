/*globals jQuery, define, exports, require, window, document */

/* ========================================================================
 * Paginator: paginator.js v0.0.1 
 * Licensed under MIT (//TODO: add site/LICENSE)
 * ========================================================================*/

//TODO: factory for require.js


+function ($) {
	'use strict';

	// PAGINATOR PUBLIC CLASS DEFINITION
	// ==================================

	var Paginator = function (element, options) {
		this.$element = null;

		this.options = {
			numberOfRecords: 0,
			numberOfPages: 0,
			recordsPerPage: 15,
			currentPage: 1,
			displayedPages: 11,
			prevNext: true,
			firstLast: true,
			urlPattern: null //"link{PageNo}";
		};

		this.init('paginator', element, options);
	};

	Paginator.VERSION = '0.0.1';

	Paginator.prototype.init = function (type, element, options) {
		this.$element = $(element);
		this.$element.on('clicked.paginator', $.proxy());

		$.extend(this.options, this.$element.data(), options);

		this.update();
	};

	Paginator.prototype.update = function () {
		this.options.numberOfPages = Math.ceil(this.options.numberOfRecords / this.options.recordsPerPage);

		if (this.options.currentPage > this.options.numberOfPages) {
			this.options.currentPage = this.options.numberOfPages;
			this.$element.trigger("click.paginator", this.options.currentPage);
		}


		this.createContent();
	};

	Paginator.prototype.createContent = function () {

		var i, ul = "";

		if (this.options.numberOfPages > 1) {
			ul = "<ul class='pagination'>";

			if (this.options.prevNext)
				ul += this.createLi(this.options.currentPage - 1, "&laquo;", true);

			if (this.options.numberOfPages <= this.options.displayedPages) {

				for (i = 1; i <= this.options.numberOfPages; i++)
					ul += this.createLi(i, i);

			} else {

				var n = this.options.displayedPages - 2;
				
				if(this.options.firstLast) {
					ul += this.createLi(1, 1);
					n -= 2;
				}
				

				var min = this.options.currentPage - Math.floor(n / 2);
				var max = this.options.currentPage + Math.floor(n / 2);

				if (n % 2 == 0) {
					if (this.options.currentPage % 2 == 0)
						max--;
					else
						min++;
				}

				while (min < 3) {
					min++;
					if (max < this.options.numberOfPages - 3)
						max++;
				}
				while (max > this.options.numberOfPages - 2) {
					max--;
					if (min > 2)
						min--;
				}

				if (min == 3)
					ul += this.createLi(2, 2);
				else
					ul += this.createLi(Math.floor((1 + min) / 2), "...", true);


				for (i = min; i <= max; i++)
					ul += this.createLi(i, i);


				if (max == this.options.numberOfPages - 2)
					ul += this.createLi(this.options.numberOfPages - 1, this.options.numberOfPages - 1);
				else
					ul += this.createLi(Math.floor((this.options.numberOfPages + max) / 2), "...", true);

				if(this.options.firstLast)
					ul += this.createLi(this.options.numberOfPages, this.options.numberOfPages);
			}
			if (this.options.prevNext)
				ul += this.createLi(this.options.currentPage + 1, "&raquo;", true);

			ul += "</li>";
		}

		this.$element.html(ul);
		var self = this;

		this.$element.find("a[data-page]").on('click', function () {
			var page = $(this).data("page");

			if (self.currentPage(page)) {
				self.$element.trigger("linkClicked.paginator", page);

				if (self.options.urlPattern)
					return true;
			}
			return false;
		});
	};

	Paginator.prototype.createLi = function (page, text, noAct) {
		if (!text)
			text = page;

		if (page < 1 || page > this.options.numberOfPages)
			return "<li class='disabled'>" + this.createLink(page, text, true) + "</li>";
		else if (page == this.options.currentPage && !noAct)
			return "<li class='active'>" + this.createLink(page, text) + "</li>";

		return "<li>" + this.createLink(page, text) + "</li>";
	};

	Paginator.prototype.createLink = function (page, text, disabled) {

		if (disabled)
			return "<a>" + text +  "</a>";

		var url = !this.options.urlPattern ? "#" : this.options.urlPattern.replace(/\{PageNo\}/i, page);
		var attrs = " href='" + url + "' title='Page " + page + "' data-page='" + page + "'";
		return "<a" + attrs + ">" + text + "</a>";
	};


	Paginator.prototype.numberOfPages = function () {
		return this.options.numberOfPages;
	};

	Paginator.prototype.numberOfRecords = function (value) {
		if (value === undefined)
			return this.options.numberOfRecords;

		if (this.options.numberOfRecords != value) {
			this.options.numberOfRecords = value;

			this.update();
			return true;
		}
		return false;
	};

	Paginator.prototype.recordsPerPage = function (value) {
		if (value === undefined)
			return this.options.recordsPerPage;

		if (this.options.recordsPerPage != value) {
			this.options.recordsPerPage = value;
			this.update();
			return true;
		}
		return false;
	};

	Paginator.prototype.currentPage = function (value) {
		if (value === undefined)
			return this.options.currentPage;

		if (value < 1 || value > this.options.numberOfPages)
			return false;

		if (this.options.currentPage != value) {
			this.options.currentPage = value;
			this.update();
			return true;
		}
		return false;
	};

	Paginator.prototype.displayedPages = function (value) {
		if (value === undefined)
			return this.options.displayedPages;

		if (this.options.displayedPages != value) {
			this.options.displayedPages = value;
			this.update();
			return true;
		}
		return false;
	};

	Paginator.prototype.urlPattern = function (value) {
		if (value === undefined)
			return this.options.urlPattern;

		if (this.options.urlPattern != value) {
			this.options.urlPattern = value;
			this.update();
		}
	};

	Paginator.prototype.prevNext = function (value) {
		if (value === undefined)
			return this.options.prevNext;

		if (this.options.prevNext != value) {
			this.options.prevNext = value;
			this.update();
		}
	};
	
	Paginator.prototype.firstLast = function (value) {
		if (value === undefined)
			return this.options.firstLast;

		if (this.options.firstLast != value) {
			this.options.firstLast = value;
			this.update();
		}
	};

	Paginator.prototype.linkClicked = function (fn) {
		this.bindHandler('linkClicked.paginator', fn);
	};

	Paginator.prototype.bindHandler = function (t, fn) {
		if ($.isFunction(fn)) {
			this.$element.on(t, fn);
		} else {
			this.$element.off(t);
		}
	};

	// PAGINATOR PLUGIN DEFINITION
	// =========================

	function Plugin(option, value) {
		var result;
		return this.each(function () {
			var $this = $(this);
			var data = $this.data('bs.paginator');
			var options = typeof option === 'object' && option;
			var selector = options && options.selector;

			if (!data && option == 'destroy')
				return;

			if (selector) {

				if (!data)
					$this.data('bs.paginator', (data = {}));

				if (!data[selector])
					data[selector] = new Paginator(this, options);

			} else {
				if (!data)
					$this.data('bs.paginator', (data = new Paginator(this, options)));
			}

			if (typeof option === 'string')
				result = data[option](value);
		});
	};

	// PAGINATOR NO CONFLICT
	// ===================

	var old = $.fn.paginator;
	$.fn.paginator = Plugin;
	$.fn.paginator.Constructor = Paginator;

	$.fn.paginator.noConflict = function () {
		$.fn.paginator = old;
		return this;
	};

	$('div[role="paginator"]').paginator(); //NOTE: what if noConflict() used??
	//FIXME: if element with role='paginator' added after this file is included
} (jQuery);