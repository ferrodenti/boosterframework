﻿/* globals jQuery, location */
;(function ($) {
	$.extend({
		hash: function(key, value, state) {
			var data = {}, hash;

			var values = location.href.indexOf('#');
			if (values !== -1) {
				values = location.href.substr(values + 1).split('&');
				for (var i = 0; i < values.length; i++) {
					if (values[i]) {
						hash = values[i].split('=');
						data[decodeURIComponent(hash[0])] = decodeURIComponent(hash[1]);
					}
				}
			}

			if (key !== undefined) {
			    var removeEmpty = false;
			    if (value == 'removeEmpty') {
			        removeEmpty = true;
			        value = null;
			    }

				if (typeof key === "string") {
				    var i = key.indexOf("=");
                    if (i >= 0) {
                        value = key.substring(i + 1);
                        key = key.substring(0, i);
                    }
                    
					if (value === undefined)
						return data[key];

					data[key] = value;
				} else {
					data = $.extend(data, key);
				}

			    if (removeEmpty) {
			        deleteNullProperties(data);
			    }
			    if (!$.isEmptyObject(data))
					window.location.hash = $.param(data);
				else if (window.location.hash != "") {
			        history.pushState(state || '', document.title, window.location.pathname + window.location.search);
				    $(window).trigger('hashchange');
				}
			}
			return data;

			function deleteNullProperties(test, recurse) {
			    for (i in test) {
			        if (test[i] === "" || test[i] === null || test[i] === undefined)
			            delete test[i];
			        else if (recurse && typeof test[i] === 'object')
			            deleteNullProperties(test[i], recurse);
			    }
			}
		}
	});

    $.extend({
        hashNavigation: function(callBack, initialState) {
            var self = this;
            this._state = $.extend({}, initialState);
            delete this._state["nop"];

            this.updateState = function(state, param) {
                $(param.split("&")).each(function() {
                    if (this.length) {
                        var val = this.split("=");
                        var key = decodeURIComponent(val[0]);
                        val = decodeURIComponent(val[1]);
                        if (val) {
                            state[key] = val;
                        } else {
                            delete state[key];
                        }
                    }
                });
            };

            this.stateToString = function(state) {
                var res = "";
                for (var key in state) {
                    if (res)
                        res += "&";
                    res += key + "=" + state[key];
                }
                return res;
            };

            this.set = function(href, param, title) {
                self.updateState(self._state, param);
                var url = location.href.split("#")[0];
                if (!href || href == "#" || href == url) {
                    var hash = $.hash();
                    self.updateState(hash, param);
                    hash = self.stateToString(hash);
                    if (hash)
                        hash = "#" + hash;

                    history.pushState(self._state, title || document.title, url + hash);
                } else {
                    history.pushState(self._state, title || document.title, href + location.hash);
                }

                if (title)
                    document.title = title;

                callBack(self._state, false);
            };

            $(document).on('click', '[data-hash-param]', function(ev) {
                ev.preventDefault();
                var $this = $(this);
                
                self.set($this.attr("href"), $this.data("hash-param"), $this.data("title"));
            });

            window.onpopstate = function(e) {
                if (history.state) {
                    self._state = history.state;
                    callBack(history.state, true);
                }
            };
            var initUrl = location.href.split("#");
            if (initUrl.length > 1) {
                self.updateState(self._state, initUrl[1]);
                callBack(self._state, true);
            }
            history.pushState(self._state, document.title, location.href);

            return this;
        }
    });

})(jQuery);