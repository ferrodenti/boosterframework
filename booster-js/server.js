$.mockjax({
	url: "/booster/grid/*",
	dataType: "json",
	response: function(req) {
		try {
			
			var server = window.server;
			if( !server )
				window.server = server = new ServerMock();
				
			var verb = req.url.substring("/booster/grid/".length).toLowerCase();
			
			if(!req.data.controller) {
				alert("controller is empty");
				return;
			}
			
			var controller = server.gridControllers[req.data.controller];
			if(!controller) {
				alert("A controller was not registred: 	" + req.data.controller);
				return;
			}
			
			if(!controller[verb]) {
				alert("Method " + verb + " was not found in a controller");
				return;
			}
			
			this.responseText = controller[verb](req.data);
		}
		catch(err) {
			alert("SERVER ERROR: " + err);
		}
	}
});

ServerMock = function(){
	this._version = 2;
	this.gridControllers = {};
	
	this.registerController("test", {
		caption: "test",
		data: [
			{ name: "integer", displayName: "Целое", sortable: true, filterable: true, type: "int"},
			{ name: "float", displayName: "Дробное", sortable: true, type: "float", precision: 2, step:.1},
			{ name: "string", displayName: "Строка", filterable: true, sortable: true},
			{ name: "date", displayName: "Дата", sortable: true, type: "date"},
			{ name: "order", displayName: "Порядок", sortable: true,  type: "int"}
		]}, 
		function() {
			var res = [];
			alert("CONTENT GENERATION!")
			for(var i=0; i<1000; i++)
				res.push({
					_key: i,
					integer: i+5,
					float: 10 + (i/20),
					string: function() {
						return Math.random().toString(36).replace(/[^a-z]/g, '').substr(0, 2 + (Math.random() * 10));
					}(),
					date: function (){
						var today = new Date();
						var yyyy = today.getFullYear();
						var mm = today.getMonth()+1;
						var dd  = today.getDate();
						return dd + "." + mm + "." + yyyy;
						}(),
					order: i*10
			});
	
			return res;
		});
};

ServerMock.prototype.registerController = function(name, readSchema, dataSource, writeSchema) {
	this.gridControllers[name] = new GridController(name, this._version, readSchema, dataSource, writeSchema);
};

GridController = function(controller, version, readSchema, dataSource, writeSchema) {
	this._controller = controller;
	this._readSchema = readSchema;
	this._writeSchema = writeSchema || readSchema;
	this._writeSchema.hash = 1;
	this._version = version;
	this._sortCache = {};
	
	if( this._supportsHtml5Storage() ) {
		var record = JSON.parse(localStorage.getItem("serverMock_grid_" + this._controller));
		
		if( record && record._version == this._version ) {
			this._data = record.data;
			return;
		}
	} 
	this._updateDataSource(dataSource());
}

GridController.prototype._supportsHtml5Storage = function() {
	try {
		return 'localStorage' in window && window['localStorage'] !== null;
	} catch (e) { return false; }
};

GridController.prototype._updateDataSource = function(data) {
	if(!data)
		data = this._data;
	else
		this._data = data;
		
	this._sortCache = {};
	
	if(this._supportsHtml5Storage()) {
		var self = this;
		setTimeout(function() {
			var key = "serverMock_grid_" + self._controller;
			var record = {};
			record.data = data;
			record._version = self._version;
			localStorage.setItem(key, JSON.stringify(record));
		}, 300);
	}
};

GridController.prototype._getRows = function(data, schema, skip, take) {
	var res = [], len;

	if(skip === undefined)
		skip = 0;
		
	if( take > 0 )
		len = Math.min(skip + take, data.length);
	else 
		len = data.length;
	
	for(var i=skip; i<len; i++) {
		var row = data[i];
		var cells = [];
		for(var j=0; j<schema.data.length; j++) {
			var col = schema.data[j];
			var val = row[col.name];
			//alert(val);
			cells.push({name:col.name, value:val});
		}
		
		res.push({cells:cells, key:row._key});
	}
	return res;
};

GridController.prototype._sortData = function(sort, sortDir) {
	var res = this._data.slice();
	var dir = sortDir == "desc" ? 1 : -1;
	
	res.sort(function(a, b) {
		if(a[sort] == b[sort])
			return 0;
		if(a[sort] < b[sort])
			return dir;
		return -dir;
	});
	return res;
};

GridController.prototype._getSortedData = function(sort, sortDir) {
	if(!sort || !sortDir)
		return this._data;
		
	var key = sort + "|" + sortDir;
		
	var data =  this._sortCache[key];
	if( !data )	{
		data = this._sortCache[sort + "|" + (sortDir == "desc" ? "asc" : "desc")];
		if( data ) {
			data = data.slice();
			data.reverse();
		}
		else
			data = this._sortData(sort, sortDir);
			
		this._sortCache[key] = data;
	}
		
	return data;
};

GridController.prototype.select = function(request) {
	var data = this._getSortedData(request.sort, request.sortDir);
			
	var result = {
		pageTitle: this._controller,
		totalRows: data.length,
		allowInsert: true,
		allowWrite: true,
		allowDelete: true,
		sortColumn: request.sort,
		sortDir: request.sortDir,
		schema : this._readSchema,
		rows: this._getRows(data, this._readSchema, request.skip, request.take)
	};
	
	return result;
};
GridController.prototype._getIndexByKey = function (key, exception) {
	
	for(var i=0; i<this._data.length; i++)
		if(this._data[i]._key == key)
			return i;
			
	if( exception !== false )
		throw "Entity " + key + " not found!";
		
	return 0;
};

GridController.prototype._getByKey = function (key, exception) {
	
	for(var i=0; i<this._data.length; i++)
		if(this._data[i]._key == key)
			return this._data[i];
			
	if( exception !== false )
		throw "Entity " + key + " not found!";
		
	return null;
};

GridController.prototype.delete = function(request) {
	var id = this._getIndexByKey(request.key);
	
	this._data.splice(id, 1);
	this._updateDataSource();
	
	return {};
};

GridController.prototype.getnew = function(request) {
	//TODO: fix new record with old fieds bug
	return {
		schema: this._writeSchema,
		rows: [{}]
	};
};

GridController.prototype.getedit = function(request) {
	var entity = this._getByKey(request.key);
	
	return {
		schema: this._writeSchema,
		rows: this._getRows([entity], this._writeSchema, 0, 1)
	};
};

GridController.prototype.save = function(request) {
	var entity = this._getByKey(request.key, false) || {};
	
	$.extend(entity, JSON.parse(request.data));
	
	if(!entity._key)
		this._data.push(entity);
		
	this._updateDataSource();
	
	return {validationResponse:{isOk:true}};
};