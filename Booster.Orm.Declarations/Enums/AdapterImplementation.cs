﻿using System;

namespace Booster.Orm;

[Flags]
public enum AdapterImplementation
{
	Inherit = 0,
		
	Select = 1 << 0,
	Load = 1 << 1,
	Insert = 1 << 2,
	Update = 1 << 3,
	Delete = 1 << 4,
		
	Sync = 1 << 5,
	Async = 1 << 6,
		
	WithPks = 1 << 7,
		
	ReadWriteUnmapped = 1 << 8,
		
	All = Select | Load | Insert | Update | Delete | Sync | Async | WithPks | ReadWriteUnmapped
}