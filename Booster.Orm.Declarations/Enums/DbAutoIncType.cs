﻿using System;

namespace Booster.Orm;

[Serializable]
public enum SequenceType
{
	DbSequence,
	Memory,
	ComputeOnInsert,
}