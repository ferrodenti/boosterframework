namespace Booster.Orm;

/// <summary>
/// Трассировка sql запросов
/// </summary>
public enum TraceSqlOptions
{
	/// <summary>
	/// Отключена
	/// </summary>
	False,

	/// <summary>
	/// Включена
	/// </summary>
	True,

	/// <summary>
	/// Вставить параметры в текст запроса
	/// </summary>
	Inline
}