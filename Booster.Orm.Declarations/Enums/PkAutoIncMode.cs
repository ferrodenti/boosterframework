namespace Booster.Orm;

public enum PkAutoIncMode
{
	Implicit,
	Triggers,
	Explicit
}