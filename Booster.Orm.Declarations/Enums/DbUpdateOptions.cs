﻿using System;

namespace Booster.Orm;

[Flags]
[Serializable]
public enum DbUpdateOptions
{
	Inherit = 0,
	CreateDatabase = 1,
	CreateTable = 2,
	CreateColumn = 4,
	ChangeColumnType = 8,
	RenameTable = 16,
	RenameColumn = 32,
	DropColumn = 64,

	CreateIndex = 128,
	DropIndex = 256,
		
	CreateConstraint = 512,
	DropConstraint = 1024,

	CreateSequence = 2048,

	DefaultValues2Null = 16384, //TODO:
	ThrowExceptions = 32768, //TODO:
		
	UpdateColumnComment = 65536, //TODO: UpdateColumnComment from xml documentation

	Custom = 131072, //TODO:
	All = CreateDatabase | CreateTable | CreateColumn | ChangeColumnType | RenameTable | RenameColumn | DropColumn | DefaultValues2Null | 
	      CreateIndex | DropIndex | CreateConstraint | DropConstraint | CreateSequence | UpdateColumnComment | Custom,
	Default = CreateDatabase | CreateTable | CreateColumn | DefaultValues2Null | CreateIndex | CreateConstraint | CreateSequence | 
	          UpdateColumnComment | RenameTable | RenameColumn | Custom,
	Deny = 0x40000000
}