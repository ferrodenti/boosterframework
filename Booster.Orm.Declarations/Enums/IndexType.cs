namespace Booster.Orm;

public enum IndexType
{
	BTree,
	Hash,
	Gist,
	Gin,
}