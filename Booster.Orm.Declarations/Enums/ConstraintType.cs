namespace Booster.Orm;

public enum ConstraintType
{
	Unique,
	PrimaryKey,
}