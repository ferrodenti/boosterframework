﻿namespace Booster.Orm;

public enum OrderType
{
	None = 0,
	Asc,
	Desc
}