namespace Booster.Orm;

public enum OrmType
{
	Unknown = 0,
	PostgreSql,
	SqLite,
	Oracle,
	MySql,
	Db2,
	SqlServer,
	SqlServerCe,
	Linter
}