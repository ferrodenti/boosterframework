﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using Booster.Collections;
using Booster.DI;
using Booster.Orm.Interfaces;
using Booster.Reflection;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Orm;

[PublicAPI]
public class QueryBuilder<TEntity> : QueryBuilder, ICloneable where TEntity : IEntity
{	
	public QueryBuilder() : base(RepositoryInstance<TEntity>.Value)
	{
	}

	public QueryBuilder(IOrm orm) : base(orm.GetRepo(typeof(TEntity)))
	{
	}

	public QueryBuilder(IOrm orm, FormattableString query) : base(orm.GetRepo(typeof(TEntity)), query)
	{
	}

	public QueryBuilder(FormattableString query) : base(RepositoryInstance<TEntity>.Value, query)
	{
	}
	public QueryBuilder(QueryBuilder<TEntity> source) : base(source)
	{
	}

	public new QueryBuilder<TEntity> Clone() 
		=> new(this);

	object ICloneable.Clone()
		=> Clone();

	#region AppendXXX methods

	public new QueryBuilder<TEntity> Append(FormattableString query)
		=> (QueryBuilder<TEntity>) base.Append(query);

	public new QueryBuilder<TEntity> Append(QueryBuilder queryBuilder)
		=> (QueryBuilder<TEntity>)base.Append(queryBuilder);
	
	public new QueryBuilder<TEntity> AppendAnd(FormattableString query, bool checkEmpty = false)
		=> (QueryBuilder<TEntity>)base.AppendAnd(query, checkEmpty);

	public new QueryBuilder<TEntity> AppendAnd(QueryBuilder queryBuilder, bool checkEmpty = false)
		=> (QueryBuilder<TEntity>)base.AppendAnd(queryBuilder, checkEmpty);

	public new QueryBuilder<TEntity> AppendOr(FormattableString query, bool checkEmpty = false)
		=> (QueryBuilder<TEntity>)base.AppendOr(query, checkEmpty);

	public new QueryBuilder<TEntity> AppendOr(QueryBuilder queryBuilder, bool checkEmpty = false)
		=> (QueryBuilder<TEntity>)base.AppendOr(queryBuilder, checkEmpty);

	public new QueryBuilder<TEntity> AppendAndInArray<T>(string columnName, IEnumerable<T>? array, bool checkEmpty = false)
		=> (QueryBuilder<TEntity>)base.AppendAndInArray(columnName, array, checkEmpty);

	public new QueryBuilder<TEntity> AppendAndInArray<T>(IEnumerable<T>? items,
		Func<T, FormattableString> oneTemplate,
		Func<T[], FormattableString> manyTemplate, bool checkEmpty = false)
		=> (QueryBuilder<TEntity>) base.AppendAndInArray(items, oneTemplate, manyTemplate, checkEmpty);

	#endregion

	public new QueryBuilder<TEntity> AppendOrder(FormattableString? order, OrderType type = OrderType.None)
		=> (QueryBuilder<TEntity>)base.AppendOrder(order, type);
}

[PublicAPI]
public class QueryBuilder : ICloneable //TODO: DebugView
{
	class FormatProvider : IFormatProvider
	{
		public readonly IOrm? Orm;
		public readonly IRepository? Repository;
		public readonly ISqlFormatter? SqlFormatter;
		public readonly bool Inline;
		public readonly HybridDictionary<string, object?> Parameters = new();
		readonly HybridDictionary<object?, string> _lookup = new();
		readonly Formatter _formatter = new();

		int _parametersCounter;

		public FormatProvider(IOrm? orm, IRepository? repository, bool inline)
		{
			Repository = repository;
			Orm = orm ?? Repository?.Orm;
			SqlFormatter = Orm?.SqlFormatter ?? throw new ArgumentNullException(nameof(orm));
			Inline = inline;
		}

		public void SetParameter(string name, object? value)
		{
			Parameters[name] = value;

			if (value != null)
				_lookup[value] = name;
		}

		public string GetOrAddParameter(object? value)
		{
			var name = value == null
				? SqlFormatter!.ParamName($"P{++_parametersCounter}")
				: _lookup.GetOrAdd(value, _ => SqlFormatter!.ParamName($"P{++_parametersCounter}"));

			Parameters[name] = value;
			return name;
		}

		public object? GetFormat(Type formatType)
			=> formatType == typeof(ICustomFormatter) ? _formatter : null;

		public string Build(IEnumerable<object> sequence)
		{
			var result = new StringBuilder();
			foreach (var obj in sequence)
				switch (obj)
				{
				case FormattableString fmt:
					result.AppendFormat(this, fmt.Format, fmt.GetArguments());
					break;
				case string str:
					result.Append(str);
					break;
				}

			return result.ToString();
		}
	}

	readonly IOrm? _orm;
	readonly IRepository? _repository;
	readonly List<object> _condition = new();
	readonly List<object> _order = new();

	string? _inlined;
	public string Inlined => _inlined ??= Build(true);

	string? _query;
	public string Query => _query ??= Build(false);
	
	public bool IsSome => _condition.Count + _order.Count > 0;
	public bool IsEmpty => _condition.Count + _order.Count == 0;
	public bool NeedComma;
	public bool NeedWhere;
	//public bool NeedOrder = true;
	public bool NeedOrderComma;

	string? _conditionStr;
	public string Condition
	{
		get
		{
			if (_conditionStr == null)
				Utils.Nop(Query);
			
			return _conditionStr!;
		}
	}
	
	string? _orderStr;
	public string Order
	{
		get
		{
			if (_orderStr == null)
				Utils.Nop(Query);
			
			return _orderStr!;
		}
	}
	

	IDictionary<string, object?>? _parameters; //TODO: хотелось бы иметь возможность менять параметры у существующего запроса, либо иметь инструмент заменяющий это

	public IDictionary<string, object?> Parameters
	{
		get
		{
			if (_parameters == null)
				Build(false);

			return _parameters!;
		}
	}

	bool _nonsense;
	public bool Nonsense
	{
		get => _nonsense;
		set
		{
			if (_nonsense != value)
			{
				_nonsense = value;
				AppendAnd($"1=2");
			}
		}
	}

	

	public QueryBuilder()
	{
	}

	public QueryBuilder(FormattableString query)
		=> Append(query);

	public QueryBuilder(IOrm? orm)
		=> _orm = orm;

	public QueryBuilder(IOrm orm, FormattableString query)
		: this(orm)
		=> Append(query);

	public QueryBuilder(IRepository repository, FormattableString? query = null, FormattableString? order = null)
	{
		_repository = repository;
		_orm = _repository.Orm;
		
		if (query != null)
			Append(query);

		if (order != null)
			AppendOrder(order);
	}

	public QueryBuilder(QueryBuilder source)
	{
		_orm = source._orm;
		_repository = source._repository;

		_condition = new List<object>(source._condition);
		_order = new List<object>(source._order);

		_inlined = source._inlined;
		_nonsense = source._nonsense;
		_parameters = source._parameters.With(p => new HybridDictionary<string, object?>(p));
		NeedComma = source.NeedComma;
		//NeedOrder = source.NeedOrder;
		NeedWhere = source.NeedWhere;
		NeedOrderComma = source.NeedOrderComma;
	}

	#region AppendXXX methods

	public QueryBuilder Append(FormattableString query)
	{
		if (query.ArgumentCount > 0)
			_condition.Add(query);
		else
			_condition.Add(query.Format);

		Dirty();

		return this;
	}

	public QueryBuilder Append(QueryBuilder queryBuilder)
	{
		_condition.AddRange(queryBuilder._condition);

		Dirty();
		return this;
	}

	void AppendKeywords(string delimiter)
	{
		if (NeedWhere)
		{
			_condition.Add(" where ");
			NeedWhere = false;
			NeedComma = true;
			return;
		}

		if (NeedComma)
			_condition.Add(delimiter);
		else
			NeedComma = true;
	}

	void AppendAnd()
		=> AppendKeywords(" and ");

	void AppendOr()
		=> AppendKeywords(" or ");

	public QueryBuilder AppendAnd(FormattableString query, bool checkEmpty = false)
	{
		if (checkEmpty && query.Format.IsEmpty())
			return this;

		AppendAnd();
		return Append(query);
	}


	

	public QueryBuilder AppendAnd(QueryBuilder queryBuilder, bool checkEmpty = false)
	{
		if (checkEmpty && queryBuilder.IsEmpty)
			return this;

		AppendAnd();

		return Append(queryBuilder);
	}

	public QueryBuilder AppendOr(FormattableString query, bool checkEmpty = false)
	{
		if (checkEmpty && query.Format.IsEmpty())
			return this;

		AppendOr();
		return Append(query);
	}

	public QueryBuilder AppendOr(QueryBuilder queryBuilder, bool checkEmpty = false)
	{
		if (checkEmpty && queryBuilder.IsEmpty)
			return this;

		AppendOr();
		return Append(queryBuilder);
	}

	public QueryBuilder AppendAndInArray<T>(string columnName, IEnumerable<T>? array, bool checkEmpty = false)
		=> AppendAndInArray(array,
			item => $"{columnName:?}={item:@}",
			arr => $"{columnName:?}=any({arr:@})", checkEmpty);

	public QueryBuilder AppendAndInArray<T>(IEnumerable<T>? items,
		Func<T, FormattableString> oneTemplate,
		Func<T[], FormattableString> manyTemplate, bool checkEmpty = false)
	{
		var array = items?.AsArray() ?? (checkEmpty ? Array.Empty<T>() : null);

		switch (array?.Length)
		{
		case null:
		case <= 0:
			Nonsense = true;
			break;
		case 1:
			AppendAnd(oneTemplate(array[0]));
			break;
		default:
			if (_orm?.SqlFormatter.AllowArrayParam == true)
				AppendAnd(manyTemplate(array));
			else
			{
				var bld = new QueryBuilder(_orm);

				foreach (var item in array)
					if (item.Is(out T norm))
						bld.AppendOr(oneTemplate(norm));

				if (bld.IsSome)
					AppendAnd(bld);
			}

			break;
		}

		return this;
	}

	#endregion

	public QueryBuilder AppendOrder(FormattableString? order, OrderType type = OrderType.None)
	{
		if (order == null)
			return this;
		
		// if (NeedOrder)
		// {
		// 	_order.Add(" order by ");
		// 	NeedOrder = false;
		// }

		if (NeedOrderComma) 
			_order.Add(", ");

		_order.Add(order);
		
		switch (type)
		{
		case OrderType.Asc:
			_order.Add(" asc ");
			break;
		
		case OrderType.Desc:
			_order.Add(" desc ");
			break;
		}

		NeedOrderComma = true;
		
		Dirty();
		return this;
	}

	protected void Dirty()
	{
		_query = null;
		_inlined = null;
		_parameters = null;
		_conditionStr = null;
		_orderStr = null;
	}

	public override string ToString()
		=> Query;

	object ICloneable.Clone()
		=> Clone();

	public virtual QueryBuilder Clone() 
		=> new(this);

	class Formatter : ICustomFormatter
	{
		public string? Format(string? format, object? arg, IFormatProvider formatProvider)
		{
			var provider = (FormatProvider)formatProvider;
			var inline = provider.Inline;
			var sqlFormatter = provider.SqlFormatter!;

			if (arg is QueryBuilder qb)
				return provider.Build(qb._condition);

			if (format != null)
			{
				if (format.StartsWith('@'))
				{
					if (arg is DbParameter parameter) //TODO: тут может быть задано имя параметра, которое по идее должно иметь приоритет
					{
						if (inline)
							return sqlFormatter.FormatConst(parameter.Value);

						provider.SetParameter(parameter.ParameterName, parameter.Value);
						return sqlFormatter.ParamName(parameter.ParameterName); //TODO: а не добавится ли тут двойной префикс?
					}

					if (inline)
						return sqlFormatter.FormatConst(arg);

					if (format.Length == 1)
						return provider.GetOrAddParameter(arg);

					var name = sqlFormatter.ParamName(format.Substring(1));
					provider.SetParameter(name, arg);
					return name;
				}

				if (format == "?")
					switch (arg)
					{
					case string columnName:
						var dbColumnName = provider.Repository?.TableMapping.GetColumnByName(columnName)?.EscapedColumnName;
						return dbColumnName ?? sqlFormatter.EscapeName(sqlFormatter.CreateColumnName(columnName));

					case Type entityType:
						var tableName = provider.Orm!.GetRepo(entityType, false)?.TableName;
						if (tableName != null)
							return tableName;

						break;
					}
			}

			return arg?.ToString(); //TODO: default string formating
		}
	}


	public void Build()
		=> Build(false);

	protected string Build(bool inline)
	{
		var formatProvider = new FormatProvider(_orm ?? InstanceFactory.Get<IOrm>(), _repository, inline);
		var condition = formatProvider.Build(_condition);
		var order = formatProvider.Build(_order);

		var result = new EnumBuilder(" ");

		result.Append(condition);

		if (order.IsSome())
		{
			result.Append("order by");
			result.Append(order);
		}
		
		if (!inline)
		{
			_parameters = formatProvider.Parameters;
			_conditionStr = condition;
			_orderStr = order;
		}

		return result;
	}

	public static QueryBuilder operator +(QueryBuilder left, QueryBuilder right)
	{
		var result = left.Clone();
		result.Append(right);
		return result;
	}

	public static QueryBuilder operator +(QueryBuilder left, FormattableString right)
		=> left + new QueryBuilder(right);

	public static QueryBuilder operator +(FormattableString left, QueryBuilder right)
	{
		var result = (QueryBuilder)TypeEx.Of(right).Create();
		result.Append(left);
		result.Append(right);
		return result;
	}
}