using System;
using JetBrains.Annotations;

namespace Booster.Orm;

/// <summary>
/// Позволяет пометить метод миграции таблицы
/// </summary>
[PublicAPI]
[MeansImplicitUse]
[AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
public class MigrationAttribute : Attribute
{
	/// <summary>
	/// Версия таблицы которая должна быть установлена в результате миграции.
	/// Отрицательное значение говорит о том, что метод выполняется для любой версии таблицы.
	/// Версия таблицы в этом случае не изменяется
	/// </summary>
	public int Version { get; set; }
		
	//TODO: добавить условное значение метаданных
		
	/// <summary>
	/// Флаги характеризующие момент времени и условие при которых выполняется метод миграции
	/// </summary>
	public MigrationOptions Options { get; set; }
		
	/// <summary>
	/// Позволяет пометить метод миграции таблицы
	/// </summary>
	/// <param name="version">Версия таблицы которая должна быть установлена в результате миграции.
	/// Отрицательное значение говорит о том, что метод выполняется для любой версии таблицы.
	/// Версия таблицы в этом случае не изменяется</param>
	/// <param name="options">Флаги характеризующие момент времени и условие при которых выполняется метод миграции.
	/// Значение по умолчанию Ready -- метод выполняется сразу после инициализации адаптера</param>
	public MigrationAttribute(int version, MigrationOptions options = MigrationOptions.Ready)
	{
		Version = version;
		Options = options;
	}

	/// <summary>
	/// Позволяет пометить метод миграции таблицы. Метод игнорирует версию таблицы
	/// </summary>
	/// <param name="options">Флаги характеризующие момент времени и условие при которых выполняется метод миграции.</param>
	public MigrationAttribute(MigrationOptions options) : this(-1, options)
	{
	}
}