using System;

namespace Booster.Orm;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = true)]
public class DbIndexAttribute : DbAttribute
{
	public string[] IndexColumns { get; set; }
	public IndexType IndexType { get; set; } = IndexType.BTree;
	public string IndexCustom { get; set; }
	public string IndexName { get; set; }
	public bool IndexCluster { get; set; }
	public bool UniqueIndex { get; set; } //TODO: не используется при создании
		
	public DbIndexAttribute()
	{
	}
	public DbIndexAttribute(params string[] columns)
		=> IndexColumns = columns;

	public DbIndexAttribute(IndexType indexType)
		=> IndexType = indexType;
}