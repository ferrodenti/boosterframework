using System;
using JetBrains.Annotations;

namespace Booster.Orm;

/// <summary>
/// Флаги характеризующие момент времени и условие при которых выполняется метод миграции
/// </summary>
[Flags, PublicAPI]
public enum MigrationOptions
{
	None = 0,
	/// <summary>
	/// Символизирует что таблица существуют. Должно комбинироваться с другими параметрами
	/// </summary>
	ShouldExist = 1,
	/// <summary>
	/// Возникает сразу после чтения метаданных таблицы
	/// </summary>
	Initialized = 2,
	/// <summary>
	/// Возникает перед переименованием таблицы
	/// </summary>
	BeforeRenameTable = 4,
	/// <summary>
	/// Возникает перед созданием таблицы
	/// </summary>
	BeforeCreateTable = 8,
	/// <summary>
	/// Возникает после создания таблицы
	/// </summary>
	AfterCreateTable = 16,
	/// <summary>
	/// Возникает перед чтением схемы существующей таблицы
	/// </summary>
	ReadingSchema = 32,
	/// <summary>
	/// Возникает сразу после чтения схемы таблицы
	/// </summary>
	SchemaRead = 64,
	/// <summary>
	/// Возникает после создания плана по автоматической миграции таблицы перед его исполнением
	/// </summary>
	BeforeUpdate = 128,
	/// <summary>
	/// Возникает после автоматической миграции таблицы
	/// </summary>
	AfterUpdate = 256,
	/// <summary>
	/// Символизирует о том, что событие миграции должно быть запущено после инициализации адаптера
	/// </summary>
	Ready = 512,
		
	/// <summary>
	/// Не писать в лог сообщение о выполнении миграции
	/// </summary>
	SuppressLog = 1024,
	/// <summary>
	/// Любая стадия иницализации репозитория
	/// </summary>
	AnyStage = Initialized | BeforeRenameTable | BeforeCreateTable | AfterCreateTable | ReadingSchema | SchemaRead | BeforeUpdate | AfterUpdate,
		
	/// <summary>
	/// Возникает после того как таблица была автоматически переименованна и адаптер инициализирован
	/// </summary>
	TableRenamed = BeforeRenameTable | ShouldExist | Ready,
		
	/// <summary>
	/// Возникает после того как таблица была создана и адаптер инициализирован
	/// </summary>
	TableCreated = AfterCreateTable | ShouldExist | Ready,
	/// <summary>
	/// Возникает после того как какое-либо из полей таблицы было автоматически мигрированно и адаптер инициализирован
	/// </summary>
	TableUpdated = AfterUpdate | ShouldExist | Ready
}