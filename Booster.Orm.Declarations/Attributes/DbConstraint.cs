using System;

namespace Booster.Orm;

[AttributeUsage(AttributeTargets.Class)]
public class DbConstraintAttribute : DbAttribute
{
	public string[] ConstraintColumns { get; set; }
	public string ConstraintName { get; set; }
	public string ConstraintCustom { get; set; }
		
	public DbConstraintAttribute()
	{
	}
	public DbConstraintAttribute(params string[] columns)
        => ConstraintColumns = columns;
}