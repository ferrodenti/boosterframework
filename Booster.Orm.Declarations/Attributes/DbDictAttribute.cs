﻿using System;
using JetBrains.Annotations;

namespace Booster.Orm;

[AttributeUsage(AttributeTargets.Class)]
public class DbDictAttribute : DbAttribute
{
	public int SlidingExpiration { get; set; }
	public int AbsoluteExpiration { get; set; }
	public int GCStart { get; set; } = 1000;
	public int AggressiveGCStart { get; set; } = 10000;
	public int GCInterval { get; set; }
	public bool Prefetch { get; set; }

	public CacheSettings CacheSettings => new()
	{
		SlidingExpiration = TimeSpan.FromMilliseconds(SlidingExpiration),
		AbsoluteExpiration = TimeSpan.FromMilliseconds(AbsoluteExpiration),
		GCInterval = TimeSpan.FromMilliseconds(GCInterval),
		GCStart = GCStart,
		AggressiveGCStart = AggressiveGCStart,
		Prefetch = Prefetch,
		CacheAbsent = false
	};

	[PublicAPI] public DbDictAttribute()
	{
	}

	public DbDictAttribute(string mapping)
		=> Mapping = mapping;
}