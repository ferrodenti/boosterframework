﻿using System;
using JetBrains.Annotations;

namespace Booster.Orm;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
[PublicAPI]
public class DbAutoIncAttribute : DbAttribute
{
	public string SequenceName;
	public long Start;
	public int Step = 1;
	public bool ApplyToColumn = true;
	public bool CheckExistingValues = true;
	public SequenceType Type;
		
	public DbAutoIncAttribute()
	{
	}
	public DbAutoIncAttribute(int step)
        => Step = step;
}