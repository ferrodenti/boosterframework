using System;
using JetBrains.Annotations;

namespace Booster.Orm;

[AttributeUsage(AttributeTargets.Class), MeansImplicitUse]
public class DbAdaptedImplementationAttribute : Attribute
{
	public AdapterImplementation Implementation { get; }
		
	public DbAdaptedImplementationAttribute(AdapterImplementation implementation)
		=> Implementation = implementation;
}