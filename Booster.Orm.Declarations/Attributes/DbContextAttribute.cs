﻿using System;

namespace Booster.Orm;

[AttributeUsage(AttributeTargets.Class)]
public class DbInstanceContextAttribute : Attribute
{
	public string ContextName { get; set; }

	public DbInstanceContextAttribute(string contextName)
        => ContextName = contextName;
}