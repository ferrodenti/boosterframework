﻿using System;
using JetBrains.Annotations;

namespace Booster.Orm;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Field | AttributeTargets.Property)]
[PublicAPI]
public class DbDefaultSortAttribute : DbAttribute
{
	public DbDefaultSortAttribute()
		=> DefaultSort = "";

	public DbDefaultSortAttribute(string value)
		=> DefaultSort = value;
}