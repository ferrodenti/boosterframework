﻿using System;

namespace Booster.Orm;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Field | AttributeTargets.Property)]
public class DbOldNameAttribute : DbAttribute
{
	public string[] OldNames { get; set; }

	public DbOldNameAttribute(params string[] name)
		=> OldNames = name;
}