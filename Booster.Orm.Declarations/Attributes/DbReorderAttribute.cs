﻿using System;

namespace Booster.Orm;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
public class DbReorderAttribute : DbAutoIncAttribute
{
	public DbReorderAttribute()
	{
		Step = 10;
		DefaultSort = "";
		Type = SequenceType.Memory;
	}
}