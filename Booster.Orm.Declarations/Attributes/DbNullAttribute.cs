﻿using System;

namespace Booster.Orm;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Field | AttributeTargets.Property)]
public class DbNullAttribute : Attribute //TODO: inherit DbAttribute
{ 
	public bool Value { get; set; }
	public bool SaveDefaultValuesAsDbNull { get; set; }

	public DbNullAttribute()
	{
		Value = true;
		SaveDefaultValuesAsDbNull = true;
	}

	public DbNullAttribute(bool value)
	{
		Value = value;
		SaveDefaultValuesAsDbNull = true;
	}
}