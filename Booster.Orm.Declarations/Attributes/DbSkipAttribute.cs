﻿using System;
using Booster.Evaluation;
using Booster.Reflection;

namespace Booster.Orm;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Method)]
public class DbSkipAttribute : Attribute
{
	public object Value;

	public DbSkipAttribute(bool value = true)
		=> Value = value;

	public DbSkipAttribute(string condition)
		=> Value = condition;

	public bool Evaluate(TypeEx type)
	{
		switch (Value)
		{
		case bool b:
			return b;
		case string s:
		{
			var val = new DynamicParameter(type, s).Evaluate(null);
			if (val is bool b1)
				return b1;

			if (val != null && StringConverter.Default.TryParse(val.ToString(), out b1))
				return b1;

			throw new Exception($"Invalid dynamic parameter result: {val} ({val?.GetType().Name ?? "null"})");
		}
		default:
			throw new Exception($"Invalid value: {Value} ({Value?.GetType().Name ?? "null"})");
		}
	}
}