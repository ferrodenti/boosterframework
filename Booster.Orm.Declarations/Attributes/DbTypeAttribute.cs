﻿using System;
using Booster.Reflection;

namespace Booster.Orm;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
public class DbTypeAttribute : DbAttribute
{
	public string Value { get; set; }
	public TypeEx Type { get; set; }

	public DbTypeAttribute(string value)
		=> Value = value;

	public DbTypeAttribute(Type type)
		=> Type = type;
}