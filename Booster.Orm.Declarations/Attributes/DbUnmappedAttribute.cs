using System;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Orm;

[AttributeUsage(AttributeTargets.Class, AllowMultiple = true), PublicAPI]
public class DbUnmappedAttribute : Attribute
{
	public string Mapping { get; set; }
	public string? DbType { get; set; }
	public Type? Type { get; set; }
	public bool? IsNullable { get; set; }

	public DbUpdateOptions DbUpdateOptions { get; set; }

	//public DbUnmappedAttribute()
	//{
	//}

	public DbUnmappedAttribute(string commaSeparatedMappings)
		=> Mapping = commaSeparatedMappings;

	public DbUnmappedAttribute(string mapping, Type fieldType)
	{
		Mapping = mapping;
		Type = fieldType;
	}

	public DbUnmappedAttribute(string mapping, string dbFieldType)
	{
		Mapping = mapping;
		DbType = dbFieldType;
	}
	
	public DbUnmappedAttribute(string mapping, string dbFieldType, bool isNullable)
		: this(mapping, dbFieldType)
		=> IsNullable = isNullable;
}