using System;

namespace Booster.Orm;

[AttributeUsage(AttributeTargets.Class)]
public class DbViewAttribute : DbAttribute
{
	public DbViewAttribute() 
		=> UpdateOptions = DbUpdateOptions.CreateTable;

	public DbViewAttribute(string name) : base(name) 
		=> UpdateOptions = DbUpdateOptions.CreateTable;
}