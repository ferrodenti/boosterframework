using System;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

#nullable enable

namespace Booster.Orm;

public static class DbDataReaderExpander
{
	public static async Task<string?> GetStringAsync(this DbDataReader reader, int ordinal, CancellationToken token = default)
		=> await reader.IsDBNullAsync(ordinal, token) ? null : reader.GetString(ordinal);

	public static async Task<DateTime> GetDateTimeAsync(this DbDataReader reader, int ordinal, CancellationToken token = default)
		=> await reader.IsDBNullAsync(ordinal, token) ? default : reader.GetDateTime(ordinal);

	public static async Task<int> GetInt32Async(this DbDataReader reader, int ordinal, CancellationToken token = default)
		=> await reader.IsDBNullAsync(ordinal, token) ? default : reader.GetInt32(ordinal);

	public static async Task<object?> GetValueAsync(this DbDataReader reader, int ordinal, CancellationToken token = default)
		=> await reader.IsDBNullAsync(ordinal, token) ? default : reader.GetValue(ordinal);
}