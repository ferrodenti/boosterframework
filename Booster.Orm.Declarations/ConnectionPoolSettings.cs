namespace Booster.Orm;

public class ConnectionPoolSettings
{
	public bool Enabled { get; set; } = true;
	public int Min { get; set; } = 4;
	public int Max { get; set; } = 80;
	public int Inc { get; set; } = 3;
	public int Dec { get; set; } = 10;
		
	public static ConnectionPoolSettings Default => new();

	public static ConnectionPoolSettings FromString(string str)
	{
		if (str.IsEmpty())
			return Default;

		var low = str.ToLower();
		if (low == "false")
			return new ConnectionPoolSettings {Enabled = false};

		var result = new ConnectionPoolSettings();
		low = low.Trim('{', '}');
		foreach (var part in low.SplitNonEmpty(','))
		{
			var pair = part.SplitNonEmpty(':');
			if (pair.Length == 2 && int.TryParse(pair[1].Trim('"', '\''), out var val))
				switch (pair[0].Trim('"', '\''))
				{
				case "min":
					result.Min = val;
					break;
				case "max":
					result.Max = val;
					break;
				case "inc":
					result.Inc = val;
					break;
				case "dec":
					result.Dec = val;
					break;
				}
		}

		return result;
	}

	public override string ToString()
	{
		if (!Enabled)
			return "false";

		var result = new EnumBuilder(", ", "{", "}");
		var def = Default;
		if (Min != def.Min) result.Append($"'min':{Min}");
		if (Max != def.Max) result.Append($"'max':{Max}");
		if (Inc != def.Inc) result.Append($"'inc':{Inc}");
		if (Dec != def.Dec) result.Append($"'dec':{Dec}");

		return result;
	}
}