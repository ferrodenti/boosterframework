using System;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Booster.Collections;
using Booster.DI;
using Booster.Interfaces;
using Booster.Log;
using Booster.Orm.Interfaces;
using Booster.Reflection;
using Booster.Synchronization;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Orm;

[PublicAPI]
public class KeyQueryCache<TEntity, TKey>
	where TEntity : class, IEntity, new()
{
	bool Store(TKey? key, TEntity? record)
		=> Store(Cache, key, record);

	bool Store(CacheDictionary<TKey, TEntity> cache, TEntity? record)
		=> Store(cache, GetKey(record), record);

	bool Store(CacheDictionary<TKey, TEntity> cache, TKey? key, TEntity? record)
	{
		if (key == null)
			return false;

		if (record == null && !CacheSettings.CacheAbsent)
		{
			cache.Remove(key);
			return false;
		}

		cache[key] = record!;
		return true;
	}

	static IRepository Repository => RepositoryInstance<TEntity>.Value;

	readonly SyncLazy<CacheDictionary<TKey, TEntity>> _cache
		= SyncLazy<CacheDictionary<TKey, TEntity>>.Create<KeyQueryCache<TEntity, TKey>>(self =>
		{
			try
			{
				if (self == null)
					throw new Exception($"{typeof(KeyQueryCache<TEntity, TKey>)}._cache: self is null!!!");

				var result = new CacheDictionary<TKey, TEntity>(self.CacheSettings);

				if (self.Prefetch)
					foreach (TEntity entity in Repository.Select())
						self.Store(result, entity);

				return result;
			}
			catch (Exception e)
			{
				InstanceFactory.Get<ILog>(false).Error(e);
				throw;
			}
		});

	protected CacheDictionary<TKey, TEntity> Cache => _cache.GetValue(this);

	readonly Lazy<BaseDataMember> _member;
	readonly SyncLazy<string> _columnName;

	public string ColumnName
	{
		get => _columnName.Value;
		set => _columnName.Value = value;
	}

	readonly Func<TKey?, TEntity?>? _loader;
	readonly Func<TKey?, CancellationToken, Task<TEntity?>>? _loaderAsync;
	readonly Func<TEntity?, TKey?>? _keyGetter;

	TKey? GetKey(TEntity? entity)
		=> _keyGetter.InvokeOrDefault(entity);

	readonly AsyncLock _lock = new();

	public IDisposable Lock()
		=> _lock.Lock();

	public Task<IDisposable> LockAsync()
		=> _lock.LockAsync();
	
	public Task<IDisposable?> LockAsync(CancellationToken token)
		=> _lock.LockAsync(token);

	public CacheSettings CacheSettings { get; set; } = new();
	public bool CheckOnlyCache { get; set; }

	[Obsolete("Use CacheSettings.IgnoreCase")]
	public bool IgnoreCase
	{
		get => CacheSettings.IgnoreCase;
		set => CacheSettings.IgnoreCase = value;
	}

	
	[Obsolete("Use CacheSettings.CacheAbsent")]
	public bool CacheMissingKeys
	{
		get => CacheSettings.CacheAbsent;
		set => CacheSettings.CacheAbsent = value;
	}

	public bool Prefetch
	{
		get => _prefetch;
		set
		{
			if (_prefetch != value)
			{
				_prefetch = value;

				if (_prefetch && _cache.IsValueCreated)
				{
					var cache = Cache;

					foreach (TEntity entity in Repository.Select())
						Store(cache, entity);
				}
			}
		}
	}

	bool _prefetch;
	readonly string _keyAccessorExpression;

	public KeyQueryCache(
		Expression<Func<TEntity?, TKey?>> keyAccessor,
		Func<TKey?, TEntity?>? loader = null,
		Func<TKey?, CancellationToken, Task<TEntity?>>? loaderAsync = null)
	{
		_loader = loader;
		_loaderAsync = loaderAsync;

		_keyGetter = keyAccessor.Compile();
		_keyAccessorExpression = keyAccessor.ToString();
		_member = new Lazy<BaseDataMember>(() => keyAccessor.ParseMemberRef(false));

		_columnName = new SyncLazy<string>(() =>
		{
			if(!Repository.TableMapping.TryGetColumnByName(_member.Value.Name, out var column))
				throw new Exception($"Could't find a column mapped to a \"{_member.Value.Name}\" (expression: {keyAccessor})");

			return column.EscapedColumnName;
		});

		if (Repository is ISqlDictionary dictionary)
			CacheSettings = dictionary.CacheSettings.Clone();

		Repository.Saved += (_, args) =>
		{
			if (args.Entity == null)
				_cache.Reset();
			else
			{
				var entity = (TEntity)args.Entity;
				var key = GetKey(entity);
				if (key != null && _cache.IsValueCreated)
				{
					if (Cache.ContainsKey(key) || args.Action != RepositoryAction.Insert)
						Store(key, entity);
					else
						_cache.Reset(); // неизвестно, какое значение было раньше в ключе, поэтому чистим кеш целиком
				}
			}
		};

		Repository.Deleted += (_, args) =>
		{
			if (args.Entity == null)
				_cache.Reset();
			else
			{
				var entity = (TEntity)args.Entity;
				var key = GetKey(entity);
				if (key != null && _cache.IsValueCreated)
				{
					if (Cache.ContainsKey(key))
						Store(key, null);
					else
						_cache.Reset(); // неизвестно, какое значение было раньше в ключе, поэтому чистим кеш целиком
				}
			}
		};
	}

	bool PrepareKey(ref TKey? key, bool throwInvalidKey = true)
	{
		if (key is string skey)
		{
			if (skey.IsEmpty())
			{
				if (throwInvalidKey)
					throw new ArgumentOutOfRangeException(nameof(key));
				
				return false;
			}

			if (CacheSettings.IgnoreCase)
			{
				key = (TKey)(object)skey.ToLower();
				return true;
			}
		}

		if (key == null)
		{
			if (throwInvalidKey)
				throw new ArgumentNullException(nameof(key));
			
			return false;
		}
		return true;
	}

	public TEntity? Get(TKey? key)
		=> TryGet(key, out var result) ? result : null;

	public bool TryGet(TKey? key, out TEntity result)
	{
		if (PrepareKey(ref key, false))
			using (Lock())
			{
				if (Cache.TryGetValue(key!, out var res) && res.Is(out result))
					return true;

				if (!CheckOnlyCache)
				{
					res = Load(key);
					Store(key, res);

					if (res != null)
					{
						result = res;
						return true;
					}
				}
			}

		result = null!;
		return false;
	}

	public async Task<bool> CacheItem(TEntity item)
	{
		var key = GetKey(item);
		if (key != null)
		{
			using var _ = await LockAsync().NoCtx();

			return Store(key, item);
		}

		return false;
	}

	public bool TryGetCache(TKey? key, out TEntity? result)
	{
		result = null;
		if (!PrepareKey(ref key, false))
			return false;

		using (Lock())
			return Cache.TryGetValue(key!, out result);
	}

	public async Task<TEntity?> GetAsync(TKey? key, CancellationToken token = default)
	{
		var i = 0;
		IDisposable? lck = null;

		if (!PrepareKey(ref key, false))
			return null;

		try
		{
			lck = await LockAsync(token).NoCtx();
			++i;
			if (!Cache.TryGetValue(key!, out var result) && !CheckOnlyCache)
			{
				i += 100;
				result = await LoadAsync(key, token).NoCtx();
				++i;
				Store(key, result);
				++i;
			}

			++i;
			return result;
		}
		catch (Exception e)
		{
			i += 1000;
			Console.WriteLine(e);
			throw;
		}
		finally
		{
			try
			{
				lck?.Dispose();
			}
			catch (Exception e)
			{
				Utils.Nop(i);
				lck?.Dispose();
				Console.WriteLine(e);
				throw;
			}
		}
	}

	protected virtual QueryBuilder<TEntity> CreateQuery(TKey? key)
	{
		var result = new QueryBuilder<TEntity>();

		if (key == null)
			result.Append($"{ColumnName} is null");
		else if (CacheSettings.IgnoreCase && typeof(TKey) == typeof(string))
			result.Append($"lower({ColumnName})=");
		else
			result.Append($"{ColumnName}=");

		result.Append($"{key:@}");
		return result;
	}

	public TEntity? Load(TKey? key)
	{
		if (_loader != null)
			return _loader(key);

		var query = CreateQuery(key);
		return (TEntity?)Repository.Load(query);
	}

	public async Task<TEntity?> LoadAsync(TKey? key, CancellationToken token = default)
	{
		if (_loaderAsync != null)
			return await _loaderAsync(key, token).NoCtx();

		var query = CreateQuery(key);
		var entity = await Repository.LoadAsync(query, token).NoCtx();

		return (TEntity?)entity;
	}

	public TEntity? GetOrAdd(TKey? key, Func<TKey, TEntity?> create, bool throwInvalidKey = true)
	{
		if (!PrepareKey(ref key, throwInvalidKey))
			return null;

		using (Lock())
			return Cache!.GetOrAdd(key, _ =>
			{
				var result = Load(key);
				if (result == null)
				{
					result = create.Invoke(key!);
					if (result != null)
					{
						_member.Value?.SetValue(result, key);
						result.Insert();
					}
				}

				return result;
			});
	}

	public TEntity? GetOrAdd(TKey? key, Action<TEntity>? init = null)
		=> GetOrAdd(key, _ =>
		{
			var result = new TEntity();
			init?.Invoke(result);
			return result;
		});

	public async Task<TEntity?> GetOrAddAsync(
		TKey? key, 
		Func<TKey, Task<TEntity?>> create, 
		CancellationToken token = default,
		bool throwInvalidKey = true)
	{
		if (!PrepareKey(ref key, throwInvalidKey))
			return null;

		using var _ = await LockAsync(token).NoCtx();

		return await Cache!.GetOrAddAsync(key, async _ =>
		{
			var result = await LoadAsync(key, token).NoCtx();
			if (result == null)
			{
				result = await create(key!).NoCtx();
				if (result != null)
				{
					var member = _member.Value;
					var memberType = member.ValueType;

					var convIfc = memberType.FindInterface(typeof(IConvertiableFrom<>));
					if (convIfc != null)
					{
						var target = member.GetValue(result);
						var valueMember = convIfc.FindProperty("Value")!;
						valueMember.SetValue(target, key);
					}
					else
						member.SetValue(result, key);

					await result.InsertAsync(token).NoCtx();
				}
			}

			return result!;
		}).NoCtx();
	}

	public Task<TEntity?> GetOrAddAsync(TKey? key, Func<TEntity, Task> initAsync, CancellationToken token = default)
		=> GetOrAddAsync(key, async _ =>
		{
			var result = new TEntity();
			await initAsync.Invoke(result).NoCtx();
			return result;
		}, token);

	public Task<TEntity?> GetOrAddAsync(
		TKey? key, 
		Action<TEntity>? init = null, 
		CancellationToken token = default,
		bool throwInvalidKey = true)
		=> GetOrAddAsync(key, async _ =>
		{
			await Utils.AsyncNop().NoCtx();

			var result = new TEntity();
			init?.Invoke(result);
			return result;
		}, token, throwInvalidKey);


	public void Remove(TKey key)
	{
		if (_cache.IsValueCreated)
			Cache.Remove(key);
	}

	public void Clear()
		=> _cache.Reset();

	public override string ToString()
		=> $"{_keyAccessorExpression}: {(_cache.IsValueCreated ? _cache.ValueIfCreated : "not accessed")}";
}