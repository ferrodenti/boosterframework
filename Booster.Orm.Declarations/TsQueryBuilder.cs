﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Booster.Orm;

public class TsQueryBuilder
{
	readonly Func<string, IEnumerable<string>> _synonymsGetter;

	public bool AllowIncompleteLastWord { get; set; }

	public TsQueryBuilder()
	{
		AllowIncompleteLastWord = true;
		_synonymsGetter = _ => Array.Empty<string>();
	}

	public TsQueryBuilder(Func<string, IEnumerable<string>> synonymsGetter) : this()
		=> _synonymsGetter = synonymsGetter;

	public string BuildAndQuery(string str)
	{
		var words = Tokenize(str).ToArray();
		if (words.Length <= 0)
			return null;

		var len = AllowIncompleteLastWord ? words.Length - 1 : words.Length;
		var res = new EnumBuilder("&");

		for (var i = 0; i < len; i++)
		{
			var w = words[i];
			var synonyms = _synonymsGetter(w).ToArray();

			res.Append(synonyms.Length > 0 ? $"({w}|{synonyms.ToString(new EnumBuilder("|"))})" : w);
		}

		if (AllowIncompleteLastWord)
		{
			var w = words[len];
			var synonyms = _synonymsGetter(w).ToArray();

			if (synonyms.Length > 0)
				res.Append(len == 0 ? $"{w}|{synonyms.ToString(new EnumBuilder("|"))}:*" : $"({w}|{synonyms.ToString(new EnumBuilder("|"))}:*)");
			else
			{
				res.Append(w);
				res.AppendNoComma(":*");
			}
		}

		if (words.Length == 1)
			return res.ToString().TrimStart('(').TrimEnd(')');

		return res;
	}


	public string BuildOrQuery(string str)
	{
		var words = Tokenize(str).ToArray();
		if (words.Length <= 0)
			return null;

		var len = AllowIncompleteLastWord ? words.Length - 1 : words.Length;
		var res = new EnumBuilder("|");

		for (var i = 0; i < len; i++)
		{
			var w = words[i];
			var synonyms = _synonymsGetter(w).ToArray();

			if (synonyms.Length > 0)
				res.AppendRange(synonyms);

			res.Append(w);
		}

		if (AllowIncompleteLastWord)
		{
			var w = words[len];
			var synonyms = _synonymsGetter(w).ToArray();

			res.Append(w);
			res.AppendNoComma(":*");

			if (synonyms.Length > 0)
				res.AppendRange(synonyms);
		}

		return res;
	}

	protected IEnumerable<string> Tokenize(string str)
	{
		var res = new StringBuilder();

// ReSharper disable once ForCanBeConvertedToForeach
		for (var i = 0; i < str.Length; i++)
		{
			var c = str[i];
			if (char.IsLetterOrDigit(c))
				res.Append(c);
			else if (res.Length > 0)
			{
				yield return res.ToString();
				res.Clear();
			}
		}

		if (res.Length > 0)
			yield return res.ToString();
	}
}