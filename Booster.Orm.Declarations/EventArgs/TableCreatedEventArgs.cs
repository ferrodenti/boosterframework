﻿using System.Threading.Tasks;

namespace Booster.Orm;

public class TableCreatedEventArgs : TableSchemaChangedEventArgs
{
	public Task InitTask { get; set; }

	public TableCreatedEventArgs(string tableName, int currentVersion) : base(tableName, currentVersion)
	{
	}

	public TableCreatedEventArgs(string tableName) : base(tableName)
	{
	}
}