﻿using System;

namespace Booster.Orm;

[Flags]
public enum RepositoryAction
{
	None = 0,
	Insert = 1,
	Update = 2,
	Delete = 4,
	Save = Insert | Update,
	Any = Save | Delete,
}