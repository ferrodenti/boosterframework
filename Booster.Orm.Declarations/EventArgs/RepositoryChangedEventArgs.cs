﻿using System;

#nullable enable

namespace Booster.Orm;

public class RepositoryChangedEventArgs : EventArgs
{
	public object? Entity { get; set; }
	public RepositoryAction Action { get; set; }

	public RepositoryChangedEventArgs(object? entity, RepositoryAction action)
	{
		Entity = entity;
		Action = action;
	}
}