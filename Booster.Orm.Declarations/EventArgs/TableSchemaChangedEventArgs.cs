﻿using System;

namespace Booster.Orm;

public class TableSchemaChangedEventArgs : EventArgs
{
	public string TableName { get; private set; }
	public int CurrentVersion { get; private set; }
	public int NewVersion { get; set; }

	public TableSchemaChangedEventArgs(string tableName, int currentVersion)
	{
		NewVersion = CurrentVersion = currentVersion;
		TableName = tableName;
	}

	public TableSchemaChangedEventArgs(string tableName)
        => TableName = tableName;
}