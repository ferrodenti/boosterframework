using System;
using Booster.Orm.Interfaces;
using Booster.Reflection;

namespace Booster.Orm;

public class DbMigrationEventArgs : EventArgs
{
	public IOrm Orm;
	public readonly string Message;
	public readonly DbUpdateOptions Type;
	public readonly string TableName;
	public readonly string ObjectName;
	public readonly BaseDataMember DataMember;

	public DbMigrationEventArgs(IOrm orm, string message, DbUpdateOptions type, string tableName, string objectName, BaseDataMember dataMember)
	{
		Orm = orm;
		Message = message;
		Type = type;
		TableName = tableName;
		ObjectName = objectName;
		DataMember = dataMember;
	}

	public override string ToString()
	{
		if (Message.IsSome())
			return Message;
			
		var bld = new EnumBuilder(", ");
		bld.Append($"type={Type}");
		if(TableName.IsSome())
			bld.Append($"tableName={TableName}");
		if(ObjectName.IsSome())
			bld.Append($"objectName={ObjectName}");
		if(DataMember != null)
			bld.Append($"dataMember={DataMember}");

		return $"Unexpected {bld}";
	}
}