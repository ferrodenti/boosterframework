using System;

namespace Booster.Orm;

/// <summary>
///	Параметры регистрации репозиториев в ОРМе
/// </summary>
public class RepositoryRegistrationOptions : ICloneable
{
	/// <summary>
	/// Если true, то в случае возникновения ошибок будет брошено исключение,
	/// Иначе будет создан экземпляр InvalidRepoAdapter, который выкинет исплючение при обращении к любому методу
	/// </summary>
	public bool ThrowExceptions { get; set; } = true;

	/// <summary>
	/// Количество потоков в которое происходит регистрация репозиториев
	/// </summary>
	public int NumberOfThreads { get; set; } = 1;
	/// <summary>
	/// Минимальное количество регистрируемых репозиториев на один поток
	/// </summary>
	public int MinimumItemsPerThread { get; set; } = 4;

	public RepositoryRegistrationOptions Clone()
		=> (RepositoryRegistrationOptions) MemberwiseClone();

	object ICloneable.Clone()
		=> Clone();
}