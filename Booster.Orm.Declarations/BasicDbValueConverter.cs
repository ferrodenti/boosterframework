using System;
using Booster.Orm.Interfaces;
using JetBrains.Annotations;

namespace Booster.Orm;

#nullable enable

/// <summary>
/// Базовый конвертор значений, который не конвертирует никакие типы значений.
/// Используется в качестве базового класса для конвертора, чтобы можно было перегрузить
/// только те методы, что необходимо.
/// </summary>
[PublicAPI]
public class BasicDbValueConverter : IDbValueConverter
{
	public virtual Type[] ActiveTypes { get; } = Type.EmptyTypes;

	public virtual string ConvertString(string value, int size)
		=> value;

	public virtual string ConvertInt(string value, int size)
		=> value;

	public virtual string ConvertLong(string value, int size)
		=> value;

	public virtual string ConvertDateTime(string value, int size)
		=> value;

	public T? Convert<T>(T? value, int size)
		=> (T?) Convert(value, typeof(T), size);

	protected virtual object? Convert(object? value, Type type, int size)
		=> type.IsInstanceOfType(value) ? value : null;
}