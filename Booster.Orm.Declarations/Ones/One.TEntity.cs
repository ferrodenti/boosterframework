using System;
using Booster.Orm.Interfaces;

#nullable enable

namespace Booster.Orm;

[Serializable]
public class One<TEntity> : One<TEntity, int> where TEntity : class, IEntity<int>
{
	public static implicit operator One<TEntity>(int id)
		=> new() { Index = id };

	public static implicit operator One<TEntity>(long id)
		=> new() { Index = (int)id };

	public static explicit operator One<TEntity>(TEntity? entity)
		=> new() {Entity = entity};

	public static explicit operator TEntity?(One<TEntity> one)
		=> one?.Entity;
}