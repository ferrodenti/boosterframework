using System;
using Booster.Orm.Interfaces;

#nullable enable

namespace Booster.Orm;

[Serializable]
public class LOne<TEntity> : One<TEntity, long> where TEntity : class, IEntity<long>
{
	public static implicit operator LOne<TEntity>(int id)
		=> new() { Index = id };

	public static implicit operator LOne<TEntity>(long id)
		=> new() { Index = id };

	public static explicit operator LOne<TEntity>(TEntity? entity)
		=> new() { Entity = entity };

	public static explicit operator TEntity?(LOne<TEntity>? one)
		=> one?.Entity;
}