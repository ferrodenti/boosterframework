using System;
using System.Threading.Tasks;
using Booster.Interfaces;
using Booster.Orm.Interfaces;

#nullable enable

namespace Booster.Orm;

[Serializable]
public class One<TEntity, TId> : IOne<TEntity, TId> 
	where TEntity : class, IEntity<TId>
{
	public static IRepository<TId> Repository => RepositoryInstance<TEntity>.GetValue<TId>();

	public bool IsLoaded { get; private set; }

	TId? _index;

	public TId Index
	{
		get => _entity != null ? _entity.Id : _index!;
		set
		{
			if (!Equals(_index, value))
			{
				IsLoaded = false;
				_index = value;
				_entity = null;
			}
		}
	}

	//[Obsolete("Use \"Index\"")]
	//public TId Value
	TId IConvertiableFrom<TId>.Value
	{
		get => Index;
		set => Index = value;
	}

	public bool IsEmpty => Equals(_index, default(TId));

	TEntity? _entity;

	public TEntity? Entity
	{
		get
		{
			if (!IsLoaded)
			{
				_entity = IsEmpty ? null : (TEntity?)Repository.LoadById(_index!);
				IsLoaded = _entity != null;
			}

			return _entity;
		}
		set
		{
			if (value == null)
			{
				_index = default;
				_entity = null;
				IsLoaded = true;
			}
			else
			{
				IsLoaded = true;
				_entity = value;
				_index = value.Id;
			}
		}
	}

	public async Task<TEntity?> GetEntityAsync()
	{
		if (!IsLoaded)
		{
			_entity = IsEmpty ? null : (TEntity?) await Repository.LoadByIdAsync(_index!).NoCtx();
			IsLoaded = _entity != null;
		}

		return _entity;
	}
	
	public async Task<TEntity> GetEntityAsync(TEntity @default)
	{
		if (!IsLoaded)
		{
			if (IsEmpty)
				Entity = @default;
			else
			{
				_entity = (TEntity?) await Repository.LoadByIdAsync(_index!).NoCtx() ?? @default;
				IsLoaded = true;
			}
		}

		return _entity!;
	}

	public async Task<TEntity> GetEntityAsync(Func<TEntity> @default)
	{
		if (!IsLoaded)
		{
			if (IsEmpty)
				Entity = @default();
			else
			{
				_entity = (TEntity?)await Repository.LoadByIdAsync(_index!).NoCtx() ?? @default();
				IsLoaded = true;
			}
		}

		return _entity!;
	}

	public async Task<TEntity> GetEntityAsync(Func<Task<TEntity>> @default)
	{
		if (!IsLoaded)
		{
			if (IsEmpty)
				Entity = await @default().NoCtx();
			else
			{
				_entity = (TEntity?) await Repository.LoadByIdAsync(_index!).NoCtx() ??
						  await @default().NoCtx();
				IsLoaded = true;
			}
		}

		return _entity!;
	}

	public override bool Equals(object obj)
	{
		var one = obj as One<TEntity, TId>;
		return one != null && Equals(_index, one._index);
	}

	// ReSharper disable once NonReadonlyMemberInGetHashCode
	public override int GetHashCode()
		=> IsEmpty ? 0 : _index!.GetHashCode();

	public override string ToString()
		=> Entity.With(e => e.ToString(), IsEmpty ? "" : _index!.ToString());

	public static implicit operator One<TEntity, TId>(TId id)
		=> new() { Index = id };

	public static implicit operator TId(One<TEntity, TId>? one)
		=> one.With(_ => _.Index)!;

	public static explicit operator One<TEntity, TId>(TEntity? entity)
		=> new() { Entity = entity };

	public static explicit operator TEntity?(One<TEntity, TId>? one)
		=> one?.Entity;

	public static bool operator ==(One<TEntity, TId>? a, One<TEntity, TId>? b)
		=> Equals(a.With(v => v._index), b.With(v => v._index));

	public static bool operator !=(One<TEntity, TId>? a, One<TEntity, TId>? b)
		=> !Equals(a.With(v => v._index), b.With(v => v._index));
}