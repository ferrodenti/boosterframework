using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Booster.AsyncLinq;
using Booster.Collections;
using Booster.DI;
using Booster.Interfaces;
using Booster.Orm.Interfaces;
using Booster.Reflection;
using Booster.Synchronization;
using JetBrains.Annotations;
using static Booster.FastLazyStatic;

#nullable enable

namespace Booster.Orm.Manies;

[PublicAPI]
public class Many<T> : /*ICollection<T>,*/ IAsyncEnumerable<T>
	where T : class, IEntityId
{
	#region Kithen

	record EngineParameters(TypeEx PrimaryType)
	{
		public TypeEx PrimaryType = PrimaryType;
		public TypeEx SecondaryType = typeof(T);

		public IOrm? Orm;
		public LambdaExpression? SecondaryKeyAccessor;
		public BaseDataMember? SecondaryKeyMember;
		public IRepository? SecondaryRepository;
		public IRepository? PrimaryRepository;
		public string? ParameterName;
		public QueryBuilder<T>? Query;
	}

	class EngineCache
	{
		readonly EngineParameters _parameters;

		public EngineCache(EngineParameters parameters)
			=> _parameters = parameters with { };

		public IOrm Orm => _parameters.Orm ??= InstanceFactory.Get<IOrm>();
		public IRepository PrimaryRepository => _parameters.PrimaryRepository ??= Orm.GetRepo(_parameters.PrimaryType);
		public IRepository SecondaryRepository => _parameters.SecondaryRepository ??= Orm.GetRepo(_parameters.SecondaryType);

		public string ParameterName => _parameters.ParameterName ??= FastLazy(() =>
		{
			var formatter = Orm.SqlFormatter;
			return formatter.ParamName(formatter.CreateColumnName(SecondaryKeyMember.Name));
		});

		public BaseDataMember SecondaryKeyMember => _parameters.SecondaryKeyMember ??= FastLazy(() =>
		{
			if (_parameters.SecondaryKeyAccessor == null)
			{
				var name = $"{_parameters.PrimaryType.Name}Id";
				
				if (SecondaryRepository.TableMapping.TryGetColumnByName(name, out var column)) 
					return column.Member.Inner;

				throw new Exception($"Column not found: {SecondaryRepository.EntityType.Name}.{name}");
			}

			return _parameters.SecondaryKeyAccessor!.ParseMemberRef();
		});

		FastLazy<BaseDataMember?>? _secondaryKeyValueProperty;
		BaseDataMember? SecondaryKeyValueProperty => _secondaryKeyValueProperty ??= FastLazy<BaseDataMember?>(() =>
		{
			var dstType = SecondaryKeyMember.ValueType;

			var convIfc = dstType.FindInterface(typeof(IConvertiableFrom<>));
			var valueMember = convIfc?.FindProperty("Value");
			return valueMember?.FindImplementation(dstType);
		});

		FastLazy<BaseDataMember?>? _ownerSetter;
		BaseDataMember? OwnerSetter => _ownerSetter ??= FastLazy<BaseDataMember?>(() =>
		{
			var dstType = SecondaryKeyMember.ValueType;
			var oneIfc = dstType.FindInterface(typeof(IOne<,>));
			var valueMember = oneIfc?.FindProperty("Entity");
			return valueMember?.FindImplementation(dstType);
		});

		public QueryBuilder<T> Query => _parameters.Query ??= FastLazy(() => new QueryBuilder<T>($"{SecondaryKeyMember.Name:?}={ParameterName}"));

		FastLazy<QueryBuilder<T>>? _clearQuery;
		QueryBuilder<T> ClearQuery => _clearQuery ??= FastLazy(()
			=> new QueryBuilder<T>($"delete from {typeof(T):?} where {Query}"));

		FastLazy<object>? _primaryKeyDefaultValue;
		public object PrimaryKeyDefaultValue => (_primaryKeyDefaultValue ??= FastLazy(() => PrimaryRepository.TableMapping.PkColumns.FirstOrDefault()?.MemberValueType.DefaultValue!)).Value;

		//FastLazy<object>? _secondaryKeyDefaultValue;
		//object SecondaryKeyDefaultValue => _secondaryKeyDefaultValue ??= FastLazy(() => SecondaryKeyMember.ValueType!.DefaultValue!);

		DbParameter CreateIdParameter(object id)
			=> Orm.CreateParameter(ParameterName, id, SecondaryKeyMember.ValueType);

		public async IAsyncEnumerable<T> SelectNoCacheAsync(IEntityId owner)
		{
			var id = owner.Id;
			if (Equals(id, PrimaryKeyDefaultValue))
				yield break;

			var param = CreateIdParameter(id);
			await foreach (T entity in SecondaryRepository.SelectAsync(Query.Query, new[] {param}).NoCtx())
			{
				SetOwner(entity, owner);
				yield return entity;
			}
		}

		public IEnumerable<T> SelectNoCache(IEntityId owner)
		{
			var id = owner.Id;
			if (Equals(id, PrimaryKeyDefaultValue))
				yield break;

			var param = CreateIdParameter(id);
			foreach (T entity in SecondaryRepository.Select(Query.Query, new[] {param}))
			{
				SetOwner(entity, owner);
				yield return entity;
			}
		}

		void SetOwner(T entity, IEntityId owner)
		{
			if (OwnerSetter != null)
			{
				var one = SecondaryKeyMember.GetValue(entity)!;
				OwnerSetter.SetValue(one, owner);
				return;
			}

			BaseDataMember dataMember;
			object target;
			if (SecondaryKeyValueProperty != null)
			{
				dataMember = SecondaryKeyValueProperty;
				target = SecondaryKeyMember.GetValue(entity)!;
			}
			else
			{
				dataMember = SecondaryKeyMember;
				target = entity;
			}

			if (!Cast.Try(owner.Id, dataMember.ValueType, out var key)) //YO
				throw new InvalidCastException($"Unable to cast {owner.Id} to type{dataMember.ValueType}");

			dataMember.SetValue(target, key);
		}

		public Task<IDictionary<object, T>> GetAsync(IEntityId owner)
			=> _cache.GetOrAddAsync(_cacheLock, owner,
					_ => SelectNoCacheAsync(owner)
						.ToDictionary<T, object, T, HybridDictionary<object, T>>(item => item.Id))
				.CastAsync<IDictionary<object, T>>();

		public IDictionary<object, T> Get(IEntityId owner)
			=> _cache.GetOrAdd(_cacheLock, owner,
				_ => SelectNoCache(owner)
					.ToDictionarySafe<T, object, T, HybridDictionary<object, T>>(item => item.Id));

		bool TryGetFormCache(IEntityId owner, out IDictionary<object, T> result)
		{
			result = null!;
			return _cache.TryGetValue(owner, out var items) && items.Is(out result);
		}

		public bool Add(IEntityId owner, T item, bool save)
		{
			SetOwner(item, owner);

			var dict = Get(owner);

			var result = dict.TryAdd(item.Id, item);

			if (save)
				item.Save();

			return result;
		}

		public async Task<bool> AddAsync(IEntityId owner, T item, bool save)
		{
			SetOwner(item, owner);

			if (save)
				await item.SaveAsync().NoCtx();

			var dict = await GetAsync(owner).NoCtx();

			var result = dict.TryAdd(item.Id, item);

			return result;
		}

		public int AddRange(IEntityId owner, IEnumerable<T> items, bool save)
		{
			var dict = Get(owner);
			var result = 0;

			foreach (var item in items)
			{
				SetOwner(item, owner);

				if (save)
					item.Save();

				if (!dict.ContainsKey(item.Id))
					++result;

				dict[item.Id] = item;
			}

			return result;
		}

		public async Task<int> AddRangeAsync(IEntityId owner, IEnumerable<T> items, bool save)
		{
			var dict = await GetAsync(owner).NoCtx();
			var result = 0;

			foreach (var item in items)
			{
				SetOwner(item, owner);

				if (save)
					await item.SaveAsync().NoCtx();

				if (!dict.ContainsKey(item.Id))
					++result;

				dict[item.Id] = item;

			}

			return result;
		}

		public bool Remove(IEntityId owner, T item, bool delete)
		{
			var result = false;

			if (TryGetFormCache(owner, out var dict))
				result = dict.Remove(item.Id);

			if (!delete)
				return result;

			return item.Delete();
		}
		
		public async Task<bool> RemoveAsync(IEntityId owner, T item, bool delete)
		{
			var result = false;

			if (TryGetFormCache(owner, out var dict))
				result = dict.Remove(item.Id);

			if (!delete)
				return result;

			return await item.DeleteAsync().NoCtx();
		}

		public int Clear(IEntityId owner)
		{
			using var lck = _cacheLock.Lock();
			using var conn = Orm.GetCurrentConnection();
			var param = CreateIdParameter(owner.Id);
			var result = conn.ExecuteNoQuery(ClearQuery.Query, new[] { param });
			_cache.Remove(owner);
			return result;
		}

		public async Task<int> ClearAsync(IEntityId owner)
		{
			using var lck = await _cacheLock.LockAsync().NoCtx();
			using var conn = Orm.GetCurrentConnection();
			var param = CreateIdParameter(owner.Id);
			var result = await conn.ExecuteNoQueryAsync(ClearQuery.Query, new[] { param }).NoCtx();
			_cache.Remove(owner);
			return result;
		}

		readonly AsyncLock _cacheLock = new();
		readonly CacheDictionary<object, HybridDictionary<object, T>> _cache = new(); //TODO: cache settings
	}

	#endregion

	public IEntityId Owner;
	readonly AsyncLock _lock = new(); //TODO: AsyncReadWriteLock
	IWeakEventHandle? _insertSubscribtion;

	FastLazy<EngineParameters>? _parameters;
	EngineParameters Parameters => _parameters ??= new EngineParameters(Owner.GetType());

	static readonly CacheDictionary<EngineParameters, EngineCache> _enginesCache = new();

	FastLazy<EngineCache>? _engine;
	EngineCache Engine => _engine ??= _enginesCache.GetOrAdd(Parameters, p => new EngineCache(p));

	void Dirty(Action action)
	{
		action();
		_engine = null;
	}

	public IOrm Orm
	{
		get => Engine.Orm;
		set => Dirty(() => Parameters.Orm = value);
	}

	public TypeEx PrimaryType
	{
		get => Parameters.PrimaryType;
		set => Dirty(() => Parameters.PrimaryType = value);
	}

	public TypeEx SecondaryType
	{
		get => Parameters.SecondaryType;
		set => Dirty(() => Parameters.SecondaryType = value);
	}

	public IRepository PrimaryRepository
	{
		get => Engine.PrimaryRepository;
		set
		{
			if (PrimaryRepository != value)
			{
				Dirty(() => Parameters.PrimaryRepository = value);
				SubscribePrimaryInserted();
			}
		}
	}

	public IRepository SecondaryRepository
	{
		get => Engine.SecondaryRepository;
		set => Dirty(() => Parameters.SecondaryRepository = value);
	}

	public LambdaExpression? SecondaryKeyAccessor
	{
		get => Parameters.SecondaryKeyAccessor;
		set => Dirty(() => Parameters.SecondaryKeyAccessor = value);
	}

	public BaseDataMember SecondaryKeyMember
	{
		get => Engine.SecondaryKeyMember;
		set => Dirty(() => Parameters.SecondaryKeyMember = value);
	}

	public string ParameterName
	{
		get => Engine.ParameterName;
		set => Dirty(() => Parameters.ParameterName = value);
	}

	public QueryBuilder<T> Query
	{
		get => Engine.Query;
		set => Dirty(() => Parameters.Query = value);
	}

	public int Count => Engine.Get(Owner).Values.Count;
	public bool IsReadOnly => false;

	bool _autoFlush;
	public bool AutoFlush
	{
		get => _autoFlush;
		set
		{
			if (_autoFlush != value)
			{
				_autoFlush = value;
				SubscribePrimaryInserted();
			}
		}
	}

	readonly SyncLazy<List<T>> _insertItems = new(() => new List<T>());
	public List<T> InsertItems => _insertItems.Value;


	protected bool OwnerNotIdDb => Equals(Owner.Id, Engine.PrimaryKeyDefaultValue);

	// ReSharper disable SuggestBaseTypeForParameterInConstructor
	public Many(IEntity<int> owner, Expression<Func<T, int>>? secondaryKeyAccessor = null)
		: this((IEntityId)owner, secondaryKeyAccessor)
	{
	}

	public Many(IEntity<long> owner, Expression<Func<T, long>>? secondaryKeyAccessor = null)
		: this((IEntityId)owner, secondaryKeyAccessor)
	{
	}

	public Many(IEntity<string> owner, Expression<Func<T, string>>? secondaryKeyAccessor = null)
		: this((IEntityId)owner, secondaryKeyAccessor)
	{
	}

	public Many(IEntity<Guid> owner, Expression<Func<T, Guid>>? secondaryKeyAccessor = null)
		: this((IEntityId)owner, secondaryKeyAccessor)
	{
	}

	protected Many(IEntityId owner, LambdaExpression? secondaryKeyAccessor = null)
	{
		Owner = owner ?? throw new ArgumentNullException(nameof(owner));
		PrimaryType = TypeEx.Of(Owner);
		SecondaryKeyAccessor = secondaryKeyAccessor;

		SubscribePrimaryInserted();
	}
	// ReSharper restore SuggestBaseTypeForParameterInConstructor

	public IAsyncEnumerable<T> SelectNoCacheAsync()
		=> Engine.SelectNoCacheAsync(Owner);

	public IEnumerable<T> SelectNoCache()
		=> Engine.SelectNoCache(Owner);

	void SubscribePrimaryInserted()
	{
		if (_insertSubscribtion != null)
		{
			_insertSubscribtion.Unsubscribe();
			_insertSubscribtion = null;
		}

		if (AutoFlush)
			_insertSubscribtion = WeakEvents.Subscribe<IRepository, Many<T>, RepositoryChangedEventArgs>(
				$"{PrimaryRepository.EntityType.FullName}=>Many<T>", PrimaryRepository, this,
				(s, eh) => { s.Inserted += eh; },
				(s, eh) => { s.Inserted -= eh; },
				(many, _, args) => many.ItemInserted(args.Entity, args));
	}

	void ItemInserted(object? target, RepositoryChangedEventArgs args)
		=> ItemInsertedAsync(target, args).NoCtxWait();

	async Task ItemInsertedAsync(object? target, RepositoryChangedEventArgs args)
	{
		if (target != Owner)
			return;

		await FlushAsync();
	}

	public void Flush()
	{
		using var lck = _lock.Lock();
		Engine.AddRange(Owner, InsertItems, true);
		_insertItems.Reset();
	}
	public async Task FlushAsync()
	{
		using var lck = await _lock.LockAsync().NoCtx();
		await Engine.AddRangeAsync(Owner, InsertItems, true).NoCtx();
		_insertItems.Reset();
	}

	//#region ICollection<T> implementation

	//bool ICollection<T>.Remove(T item)
	//	=> Remove(item);

	//void ICollection<T>.Add(T item)
	//	=> Add(item);

	//void ICollection<T>.Clear()
	//	=> Clear();

	//IEnumerator IEnumerable.GetEnumerator()
	//	=> GetEnumerator();

	//#endregion

	public async Task<int> GetCountAsync()
	{
		using var lck = await _lock.LockAsync().NoCtx();
		var items = await Engine.GetAsync(Owner).NoCtx();
		return items.Count;
	}

	public async Task EnsureLoadedAsync()
	{
		using var lck = await _lock.LockAsync().NoCtx();
		await Engine.GetAsync(Owner).NoCtx();
	}

	public async Task<T[]> ToArrayAsync()
	{
		using var lck = await _lock.LockAsync().NoCtx();
		var items = await Engine.GetAsync(Owner).NoCtx();
		return items.Values.ToArray();
	}

	public async Task<IEnumerator<T>> GetEnumeratorAsync()
	{
		using var lck = await _lock.LockAsync().NoCtx();
		var items = await Engine.GetAsync(Owner).NoCtx();
		return items.Values.GetEnumerator();
	}

	public async Task<bool> AddAsync(T item, bool save = true)
	{
		using var lck = await _lock.LockAsync().NoCtx();

		if (OwnerNotIdDb)
		{
			InsertItems.Add(item);
			return true;
		}

		return await Engine.AddAsync(Owner, item, save).NoCtx();
	}

	public async Task<int> AddRangeAsync(IEnumerable<T> items, bool save = true)
	{
		using var lck = await _lock.LockAsync().NoCtx();

		if (OwnerNotIdDb)
			return AddRangeToInsertItems(items);

		return await Engine.AddRangeAsync(Owner, items, save).NoCtx();
	}

	int AddRangeToInsertItems(IEnumerable<T> items)
	{
		var result = 0;
		foreach (var item in items)
		{
			InsertItems.Add(item);
			++result;
		}
		return result;
	}

	public async Task<int> ClearAsync()
	{
		using var lck = await _lock.LockAsync().NoCtx();

		if (OwnerNotIdDb)
			return ClearInsertItems();

		return await Engine.ClearAsync(Owner).NoCtx();
	}

	int ClearInsertItems()
	{
		var result = InsertItems.Count;
		InsertItems.Clear();
		return result;
	}

	public Task<bool> ContainsAsync(T item)
		=> ContainsKeyAsync(item.Id);

	public async Task<bool> ContainsKeyAsync(object key)
	{
		using var lck = await _lock.LockAsync().NoCtx();
		var items = await Engine.GetAsync(Owner).NoCtx();
		return items.ContainsKey(key);
	}

	public async Task CopyToAsync(T[] array, int arrayIndex)
	{
		using var lck = await _lock.LockAsync().NoCtx();

		if (OwnerNotIdDb)
		{
			InsertItems.CopyTo(array, arrayIndex);
			return;
		}

		var items = await Engine.GetAsync(Owner).NoCtx();
		items.Values.CopyTo(array, arrayIndex);
	}

	public async Task<bool> RemoveAsync(T item, bool delete = true)
	{
		using var lck = await _lock.LockAsync().NoCtx();

		if (OwnerNotIdDb)
			return InsertItems.Remove(item);

		return await Engine.RemoveAsync(Owner, item, delete).NoCtx();
	}

	public void EnsureLoaded()
	{
		using var lck = _lock.Lock();
		Engine.Get(Owner);
	}

	public T[] ToArray()
	{
		using var lck = _lock.Lock();

		if (OwnerNotIdDb)
			return InsertItems.ToArray();

		var items = Engine.Get(Owner);
		return items.Values.ToArray();
	}

	public IEnumerable<T> AsEnumerable()
	{
		using var lck = _lock.Lock();

		if (OwnerNotIdDb)
			foreach (var item in InsertItems)
				yield return item;

		else
			foreach (var pair in Engine.Get(Owner))
				yield return pair.Value;
	}

	public async IAsyncEnumerable<T> AsAsyncEnumerable()
	{
		using var lck = await _lock.LockAsync();

		if (OwnerNotIdDb)
			foreach (var item in InsertItems)
				yield return item;

		else
			foreach (var pair in await Engine.GetAsync(Owner))
				yield return pair.Value;
	}

	public IEnumerator<T> GetEnumerator()
		=> AsEnumerable().GetEnumerator();

	public IAsyncEnumerator<T> GetAsyncEnumerator(CancellationToken cancellationToken = default)
		=> AsAsyncEnumerable().GetAsyncEnumerator(cancellationToken);

	public bool Add(T item, bool save = true)
	{
		using var lck = _lock.Lock();

		if (OwnerNotIdDb)
		{
			InsertItems.Add(item);
			return true;
		}
		return Engine.Add(Owner, item, save);
	}

	public int AddRange(IEnumerable<T> items, bool save = true)
	{
		using var lck = _lock.Lock();

		if (OwnerNotIdDb)
			return AddRangeToInsertItems(items);

		return Engine.AddRange(Owner, items, save);
	}

	public int Clear()
	{
		using var lck = _lock.Lock();

		if (OwnerNotIdDb)
			return ClearInsertItems();

		return Engine.Clear(Owner);
	}

	public bool Contains(T item)
	{
		using var lck = _lock.Lock();

		if (OwnerNotIdDb)
			return InsertItems.Contains(item);

		var items = Engine.Get(Owner);
		return items.ContainsKey(item.Id);
	}

	public bool ContainsKey(object key)
	{
		using var lck = _lock.Lock();

		if (OwnerNotIdDb)
			return InsertItems.Any(i => Equals(i.Id, key));

		var items = Engine.Get(Owner);
		return items.ContainsKey(key);
	}

	public void CopyTo(T[] array, int arrayIndex)
	{
		using var lck = _lock.Lock();

		if (OwnerNotIdDb)
		{
			InsertItems.CopyTo(array, arrayIndex);
			return;
		}
		var items = Engine.Get(Owner);
		items.Values.CopyTo(array, arrayIndex);
	}

	public bool Remove(T item, bool delete = true)
	{
		using var lck = _lock.Lock();

		if (OwnerNotIdDb)
			return InsertItems.Remove(item);

		return Engine.Remove(Owner, item, delete);
	}

	public async Task<T?> FindAsync(object key)
	{
		using var lck = await _lock.LockAsync().NoCtx();
		var items = await Engine.GetAsync(Owner).NoCtx();
		if (items.TryGetValue(key, out var result))
			return result;

		return null;
	}

	public bool TryGetValue(object key, out T result)
	{
		using var lck = _lock.Lock();
		var items = Engine.Get(Owner);
		return items.TryGetValue(key, out result);
	}
}