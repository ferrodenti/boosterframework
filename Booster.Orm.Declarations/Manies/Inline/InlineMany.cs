﻿using System;
using System.Collections.Generic;
using Booster.Orm.Interfaces;

namespace Booster.Orm.Manies.Inline;

[Obsolete("Use InlineStringMany")]
public class InlineMany<TEntity> : InlineStringMany<TEntity, int> where TEntity : class, IEntity<int>
{
	public InlineMany()
	{
	}
	public InlineMany(string value) : base(value)
	{
	}
	public InlineMany(IEnumerable<int> keys) : base(keys)
	{
	}
	public InlineMany(IEnumerable<TEntity> items) : base(items)
	{
	}
}
	
[Obsolete("Use LInlineStringMany")]
public class LInlineMany<TEntity> : InlineStringMany<TEntity, long> where TEntity : class, IEntity<long>
{
	public LInlineMany()
	{
	}
	public LInlineMany(string value) : base(value)
	{
	}
	public LInlineMany(IEnumerable<long> keys) : base(keys)
	{
	}
	public LInlineMany(IEnumerable<TEntity> items) : base(items)
	{
	}
}
[Obsolete("Use InlineStringMany")]
public class InlineMany<TEntity, TId> : InlineStringMany<TEntity, TId> where TEntity : class, IEntity<TId>
{
	public InlineMany()
	{
	}
	public InlineMany(string value) : base(value)
	{
	}
	public InlineMany(IEnumerable<TId> keys) : base(keys)
	{
	}
	public InlineMany(IEnumerable<TEntity> items) : base(items)
	{
	}
}