using System.Collections.Generic;
using Booster.Orm.Interfaces;

namespace Booster.Orm.Manies.Inline;

public class LInlineArrayMany<TEntity> : InlineArrayMany<TEntity, long> where TEntity : class, IEntity<long>
{
	public LInlineArrayMany()
	{
	}
		
	public LInlineArrayMany(IEnumerable<long> ids) : base(ids)
	{
	}
		
	public static implicit operator LInlineArrayMany<TEntity>(long[] ids)
		=> new(ids);

	public static implicit operator LInlineArrayMany<TEntity>(List<long> ids)
		=> new(ids);
}