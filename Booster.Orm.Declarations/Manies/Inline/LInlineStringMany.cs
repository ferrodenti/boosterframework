﻿using System;
using System.Collections.Generic;
using Booster.Orm.Interfaces;

namespace Booster.Orm.Manies.Inline;

[Serializable]
public class LInlineStringMany<TEntity> : InlineStringMany<TEntity, long> where TEntity : class, IEntity<long>
{
	public LInlineStringMany()
	{
	}
	public LInlineStringMany(string value) : base(value)
	{
	}
	public LInlineStringMany(IEnumerable<long> keys) : base(keys)
	{
	}
	public LInlineStringMany(IEnumerable<TEntity> items) : base(items)
	{
	}
}