﻿using System;
using System.Collections.Generic;
using System.Linq;
using Booster.Collections;
using Booster.Interfaces;
using Booster.Orm.Interfaces;

namespace Booster.Orm.Manies.Inline;

[Serializable]
public class InlineStringMany<TEntity> : InlineStringMany<TEntity, int> where TEntity : class, IEntity<int>
{
	public InlineStringMany()
	{
	}
	public InlineStringMany(string value) : base(value)
	{
	}
	public InlineStringMany(IEnumerable<int> keys) : base(keys)
	{
	}
	public InlineStringMany(IEnumerable<TEntity> items) : base(items)
	{
	}
}
	
public class InlineStringMany<TEntity, TId> : BaseLazyIndexedCollection<TEntity, TId>, IConvertiableFrom<string> where TEntity : class, IEntity<TId>
{
	// ReSharper disable once StaticMemberInGenericType
	static readonly StringEscaper _defaultEscaper = new("\\", ";") {IgnoreCase = false};
	public StringEscaper StringEscaper { get; set; } = _defaultEscaper;
		
	protected static IRepository Repository => RepositoryInstance<TEntity>.Value;

	string _value;
	public string Value
	{
		get
		{
			if (_value == null)
				using (ReadLock())
				{
					var records = Records;
					_value = records.Count > 0 ? $";{StringEscaper.Join(records.Select(k => StringConverter.Default.ToString(k.Key)), ";")};" : "";
				}

			return _value;
		}
		set
		{
			if (_value != value)
			{
				_value = value;
				ReloadRecords();
			}
		}
	}

	protected override TId GetItemKey(TEntity value) 
		=> value.Id;

	protected override IEnumerable<TEntity> LoadItems(IEnumerable<TId> keys)
		=> Repository.SelectByPks(keys).Cast<TEntity>();

	public InlineStringMany()
	{
	}
	public InlineStringMany(string value) 
		=> Value = value;

	public InlineStringMany(IEnumerable<TId> keys) : base(keys)
	{
	}
	public InlineStringMany(IEnumerable<TEntity> items) : base(items)
	{
	}
		
	public override void Clear()
	{
		base.Clear();
		_value = "";
	}
		
	protected override void OnItemsChanged() 
		=> _value = null;

	protected override List<Record> InitRecords()
		=> StringEscaper.Split(_value, ";", unescape: true)
			.Select(k => new Record(StringConverter.Default.ToObject<TId>(k)))
			.ToList();
}