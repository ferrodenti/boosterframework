using System.Collections.Generic;
using System.Linq;
using Booster.Collections;
using Booster.Interfaces;
using Booster.Orm.Interfaces;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Orm.Manies.Inline;

public class InlineArrayMany<TEntity> : InlineArrayMany<TEntity, int> 
	where TEntity : class, IEntity<int>
{
	public InlineArrayMany()
	{
	}

	public InlineArrayMany(IEnumerable<int> ids) : base(ids)
	{
	}

	[PublicAPI]
	public InlineArrayMany(IEnumerable<TEntity> items) : base(items)
	{
	}

	public static implicit operator InlineArrayMany<TEntity>(int[] ids)
		=> new(ids);

	public static implicit operator InlineArrayMany<TEntity>(List<int> ids)
		=> new(ids);
}

public class InlineArrayMany<TEntity, TId> : BaseLazyIndexedCollection<TEntity, TId>, IConvertiableFrom<TId[]> 
	where TEntity : class, IEntity<TId>
{
	TId[] IConvertiableFrom<TId[]>.Value
	{
		get => Keys;
		set => Keys = value;
	}

	protected static IRepository Repository => RepositoryInstance<TEntity>.Value;

	protected override TId GetItemKey(TEntity value)
		=> value.Id;

	protected override IEnumerable<TEntity> LoadItems(IEnumerable<TId> keys)
		=> Repository.SelectByPks(keys).Cast<TEntity>();

	public InlineArrayMany()
	{
	}
	public InlineArrayMany(IEnumerable<TId> keys) : base(keys)
	{
	}
	public InlineArrayMany(IEnumerable<TEntity> items) : base(items)
	{
	}
}