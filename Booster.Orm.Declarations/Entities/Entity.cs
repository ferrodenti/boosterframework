﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Booster.AsyncLinq;
using Booster.Interfaces;
using Booster.Orm.Interfaces;
using Booster.Reflection;

#nullable enable

namespace Booster.Orm;

[Serializable]
public abstract class Entity<TThis> : IEntity, IValidateType
{
	public static IRepository Repository => RepositoryInstance<TThis>.Value;

	//protected Entity()
	//{
	//	Repository.Do(r => r.Init(this));
	//}

	public virtual void Update() 
		=> Repository.Update(this);

	public virtual Task UpdateAsync(CancellationToken token = default)
		=> Repository.UpdateAsync(this, token);

	public virtual void Insert() 
		=> Repository.Insert(this);

	public Task InsertAsync(CancellationToken token = default)
		=> Repository.InsertAsync(this, token);

	public virtual bool Delete() 
		=> Repository.Delete(this);

	public virtual Task<bool> DeleteAsync(CancellationToken token = default)
		=> Repository.DeleteAsync(this, token);

	public virtual void InsertWithPks() 
		=> Repository.InsertWithPks(this);

	public virtual Task InsertWithPksAsync(CancellationToken token = default)
		=> Repository.InsertWithPksAsync(this, token);

	public virtual void UpdateWithPks(object entity, object[] pks)
		=> Repository.UpdateWithPks(this, pks);

	public virtual Task UpdateWithPksAsync(object entity, object[] pks, CancellationToken token = default)
		=> Repository.UpdateWithPksAsync(this, pks, token);

	public virtual Task<TValue> ReadUnmappedAsync<TValue>(string fieldName, CancellationToken token = default)
		=> Repository.ReadUnmappedAsync<TValue>(this, fieldName, token);

	public virtual Task WriteUnmappedAsync<TValue>(string fieldName, TValue value, CancellationToken token = default)
		=> Repository.WriteUnmappedAsync(this, fieldName, value, token);

	public virtual TValue ReadUnmapped<TValue>(string fieldName) 
		=> Repository.ReadUnmapped<TValue>(this, fieldName);

	public virtual void WriteUnmapped<TValue>(string fieldName, TValue value) 
		=> Repository.WriteUnmapped(this, fieldName, value);

	public virtual TValue ReadUnmapped<TValue>(string fieldName, ref TValue store)
	{
		if (ReferenceEquals(store, null))
			store = ReadUnmapped<TValue>(fieldName);

		return store;
	}

	public virtual void WriteUnmapped<TValue>(string fieldName, TValue value, ref TValue store)
	{
		if (!ReferenceEquals(store, value))
		{
			WriteUnmapped(fieldName, value);
			store = value;
		}
	}

	public static TThis? Load(FormattableString query)
		=> (TThis?) Repository.Load(new QueryBuilder(Repository, query));

	public static IEnumerable<TThis> Select(FormattableString query)
		=> Repository.Select(new QueryBuilder(Repository, query)).Cast<TThis>();

	public static IEnumerable<TThis> Query(FormattableString query)
		=> Repository.Query(new QueryBuilder(Repository, query)).Cast<TThis>();

	public static int Count(FormattableString query)
		=> Repository.Count(new QueryBuilder(Repository, query));

	public static long LCount(FormattableString query)
		=> Repository.LCount(new QueryBuilder(Repository, query));

	public static IEnumerable<TThis> Select(int skip, int take, FormattableString query, string sort)
		=> Repository.Select(skip, take, new QueryBuilder(Repository, query), sort).Cast<TThis>();

	public static IEnumerable<TThis> Select(long skip, int take, FormattableString query, string sort)
		=> Repository.Select(skip, take, new QueryBuilder(Repository, query), sort).Cast<TThis>();

	public static TThis? Load(string query, DbParameter[] parameters) 
		=> (TThis?)Repository.Load(query, parameters);

	public static TThis? Load(string query, object parameters) 
		=> (TThis?)Repository.Load(query, parameters);

	public static TThis? Load(QueryBuilder queryBuilder) 
		=> queryBuilder.Nonsense ? default : (TThis?)Repository.Load(queryBuilder.Query, queryBuilder.Parameters);

	public static IEnumerable<TThis> Select(string query, DbParameter[] parameters) 
		=> Repository.Select(query, parameters).Cast<TThis>();

	public static IEnumerable<TThis> Select(string query, object parameters) 
		=> Repository.Select(query, parameters).Cast<TThis>();

	public static IEnumerable<TThis> Select(QueryBuilder queryBuilder) 
		=> queryBuilder.Nonsense ? Enumerable.Empty<TThis>() : Repository.Select(queryBuilder.Query, queryBuilder.Parameters).Cast<TThis>();

	public static IEnumerable<TThis> Select(int skip, int take, string condition, string sort, DbParameter[] parameters) 
		=> Repository.Select(skip, take, condition, sort, parameters).Cast<TThis>();

	public static IEnumerable<TThis> Select(long skip, int take, string condition, string sort, DbParameter[] parameters) 
		=> Repository.Select(skip, take, condition, sort, parameters).Cast<TThis>();

	public static IEnumerable<TThis> Select(int skip, int take, string condition, string sort, object parameters) 
		=> Repository.Select(skip, take, condition, sort, parameters).Cast<TThis>();

	public IEnumerable<TThis> Select(long skip, int take, string condition, string sort, object parameters) 
		=> Repository.Select(skip, take, condition, sort, parameters).Cast<TThis>();

	public static IEnumerable<TThis> Select(int skip, int take, QueryBuilder queryBuilder, string sort) 
		=> queryBuilder.Nonsense ? Enumerable.Empty<TThis>() :  Repository.Select(skip, take, queryBuilder.Query, sort, queryBuilder.Parameters).Cast<TThis>();

	public static IEnumerable<TThis> Select(long skip, int take, QueryBuilder queryBuilder, string sort) 
		=> queryBuilder.Nonsense ? Enumerable.Empty<TThis>() :  Repository.Select(skip, take, queryBuilder.Query, sort, queryBuilder.Parameters).Cast<TThis>();

	public static IEnumerable<TThis> Query(string query, DbParameter[] parameters) 
		=> Repository.Query(query, parameters).Cast<TThis>();

	public static IEnumerable<TThis> Query(string query, object parameters) 
		=> Repository.Query(query, parameters).Cast<TThis>();

	public static IEnumerable<TThis> Query(QueryBuilder queryBuilder) 
		=> queryBuilder.Nonsense ? Enumerable.Empty<TThis>() : Repository.Query(queryBuilder.Query, queryBuilder.Parameters).Cast<TThis>();

	public static int Count(string querty, DbParameter[] parameters) 
		=> Repository.Count(querty, parameters);

	public static int Count(string querty, object parameters) 
		=> Repository.Count(querty, parameters);

	public static int Count(QueryBuilder queryBuilder) 
		=> queryBuilder.Nonsense ? 0 : Repository.Count(queryBuilder.Query, queryBuilder.Parameters);

	public static long LCount(string querty, DbParameter[] parameters) 
		=> Repository.LCount(querty, parameters);

	public static long LCount(string querty, object parameters) 
		=> Repository.LCount(querty, parameters);

	public static long LCount(QueryBuilder queryBuilder)
		=> queryBuilder.Nonsense ? 0 : Repository.LCount(queryBuilder.Query, queryBuilder.Parameters);

	public static IEnumerable<TThis> All 
		=> Repository.Select().Cast<TThis>();

	public static async Task<TThis?> LoadAsync(FormattableString query, CancellationToken token = default)
		=> (TThis?) await Repository.LoadAsync(new QueryBuilder(Repository, query), token).NoCtx();

	public static IAsyncEnumerable<TThis> SelectAsync(FormattableString query)
		=> Repository.SelectAsync(new QueryBuilder(Repository, query)).Cast<TThis>();

	public static IAsyncEnumerable<TThis> QueryAsync(FormattableString query)
		=> Repository.QueryAsync(new QueryBuilder(Repository, query)).Cast<TThis>();

	public static Task<int> CountAsync(FormattableString query, CancellationToken token = default)
		=> Repository.CountAsync(new QueryBuilder(Repository, query), token);

	public static Task<long> LCountAsync(FormattableString query, CancellationToken token = default)
		=> Repository.LCountAsync(new QueryBuilder(Repository, query), token);

	public static IAsyncEnumerable<TThis> SelectAsync(int skip, int take, QueryBuilder query)
		=> Repository.SelectAsync(skip, take, query).Cast<TThis>(); 
	
	public static IAsyncEnumerable<TThis> SelectAsync(long skip, int take, QueryBuilder query)
		=> Repository.SelectAsync(skip, take, query).Cast<TThis>();

	public static IAsyncEnumerable<TThis> SelectAsync(int skip, int take, FormattableString query, FormattableString? sort = null)
		=> Repository.SelectAsync(skip, take, new QueryBuilder(Repository, query, sort)).Cast<TThis>();

	public static IAsyncEnumerable<TThis> SelectAsync(long skip, int take, FormattableString query, FormattableString? sort = null)
		=> Repository.SelectAsync(skip, take, new QueryBuilder(Repository, query, sort)).Cast<TThis>();

	public static async Task<TThis?> LoadAsync(string query, DbParameter[] parameters, CancellationToken token = default) 
		=> (TThis?) await Repository.LoadAsync(query, parameters, token).NoCtx();

	public static async Task<TThis?> LoadAsync(string query, object parameters, CancellationToken token = default)
		=> (TThis?) await Repository.LoadAsync(query, parameters, token).NoCtx();

	public static async Task<TThis?> LoadAsync(QueryBuilder queryBuilder, CancellationToken token = default)
		=> queryBuilder.Nonsense ? default : (TThis?) await Repository.LoadAsync(queryBuilder.Query, queryBuilder.Parameters, token).NoCtx();

	public static IAsyncEnumerable<TThis> SelectAsync(string query, DbParameter[] parameters) 
		=> Repository.SelectAsync(query, parameters).Cast<TThis>();

	public static IAsyncEnumerable<TThis> SelectAsync(string query, object parameters) 
		=> Repository.SelectAsync(query, parameters).Cast<TThis>();

	public static IAsyncEnumerable<TThis> SelectAsync(QueryBuilder queryBuilder) 
		=> queryBuilder.Nonsense ? AsyncEnumerable.Empty<TThis>() : Repository.SelectAsync(queryBuilder.Query, queryBuilder.Parameters).Cast<TThis>();

	public static IAsyncEnumerable<TThis> SelectAsync(int skip, int take, string condition, string sort, DbParameter[] parameters) 
		=> Repository.SelectAsync(skip, take, condition, sort, parameters).Cast<TThis>();

	public static IAsyncEnumerable<TThis> SelectAsync(long skip, int take, string condition, string sort, DbParameter[] parameters) 
		=> Repository.SelectAsync(skip, take, condition, sort, parameters).Cast<TThis>();

	public static IAsyncEnumerable<TThis> SelectAsync(int skip, int take, string condition, string sort, object parameters) 
		=> Repository.SelectAsync(skip, take, condition, sort, parameters).Cast<TThis>();

	public IAsyncEnumerable<TThis> SelectAsync(long skip, int take, string condition, string sort, object parameters) 
		=> Repository.SelectAsync(skip, take, condition, sort, parameters).Cast<TThis>();

	public static IAsyncEnumerable<TThis> SelectAsync(int skip, int take, QueryBuilder queryBuilder, string sort)
		=> queryBuilder.Nonsense ? AsyncEnumerable.Empty<TThis>() :  Repository.SelectAsync(skip, take, queryBuilder.Query, sort, queryBuilder.Parameters).Cast<TThis>();

	public static IAsyncEnumerable<TThis> SelectAsync(long skip, int take, QueryBuilder queryBuilder, string sort) 
		=> queryBuilder.Nonsense ? AsyncEnumerable.Empty<TThis>() :  Repository.SelectAsync(skip, take, queryBuilder.Query, sort, queryBuilder.Parameters).Cast<TThis>();

	public static IAsyncEnumerable<TThis> QueryAsync(string query, DbParameter[] parameters) 
		=> Repository.QueryAsync(query, parameters).Cast<TThis>();

	public static IAsyncEnumerable<TThis> QueryAsync(string query, object parameters) 
		=> Repository.QueryAsync(query, parameters).Cast<TThis>();

	public static IAsyncEnumerable<TThis> QueryAsync(QueryBuilder queryBuilder) 
		=> queryBuilder.Nonsense ? AsyncEnumerable.Empty<TThis>() : Repository.QueryAsync(queryBuilder.Query, queryBuilder.Parameters).Cast<TThis>();

	public static Task<int> CountAsync(string querty, DbParameter[] parameters, CancellationToken token = default) 
		=> Repository.CountAsync(querty, parameters, token);

	public static Task<int> CountAsync(string querty, object parameters, CancellationToken token = default) 
		=> Repository.CountAsync(querty, parameters, token);

	public static Task<int> CountAsync(QueryBuilder queryBuilder, CancellationToken token = default) 
		=> queryBuilder.Nonsense ? Task.FromResult(0) : Repository.CountAsync(queryBuilder.Query, queryBuilder.Parameters, token);

	public static Task<long> LCountAsync(string querty, DbParameter[] parameters, CancellationToken token = default) 
		=> Repository.LCountAsync(querty, parameters, token);

	public static Task<long> LCountAsync(string querty, object parameters, CancellationToken token = default) 
		=> Repository.LCountAsync(querty, parameters, token);

	public static Task<long> LCountAsync(QueryBuilder queryBuilder, CancellationToken token = default)
		=> queryBuilder.Nonsense ? Task.FromResult(0L) : Repository.LCountAsync(queryBuilder.Query, queryBuilder.Parameters, token);

	public static IAsyncEnumerable<TThis> AllAsync
		=> Repository.SelectAsync().Cast<TThis>();
		
	public override string ToString()
		=> GetType().Name;

	#region IEntity methods
	public virtual void OnLoad() { }
	public virtual void OnLoaded() { }
	public virtual void OnInsert() { }
	public virtual void OnInserted() { }
	public virtual void OnUpdate() { }
	public virtual void OnUpdated() { }
	public virtual void OnSave() { }
	public virtual void OnSaved() { }
	public virtual void OnDelete() { }
	public virtual void OnDeleted() { }
	public virtual void OnTableChange() { }
	public virtual void OnTableChanged() { }
	#endregion

	void IValidateType.ValidateType()
	{
		TypeEx arg = typeof(TThis);

		// ReSharper disable once PossibleNullReferenceException
		if (arg.BaseType!.IsGenericType && !Equals(arg.BaseType.GenericArguments.FirstOrDefault(), arg))
			throw new Exception($"{GetType().Name} type must inherit Entity<{GetType().Name}> class"); //TODO:  InvalidBaseTypeException
	}
}