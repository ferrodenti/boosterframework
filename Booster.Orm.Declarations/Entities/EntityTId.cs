﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Booster.Orm.Interfaces;

#nullable enable

namespace Booster.Orm;

[Serializable]
public abstract class Entity<TThis, TId> : Entity<TThis>, IEntity<TId>
{
	[DbPk, DbDefaultSort, DbAutoInc]
	public virtual TId Id { get; set; } = default!;

	object IEntityId.Id
	{
		get => Id!;
		set => Id = (TId) value;
	}

	public static new IRepository<TId> Repository => (IRepository<TId>)Entity<TThis>.Repository;

	public virtual void Save() 
		=> Repository.Save(this);

	public Task SaveAsync(CancellationToken token = default)
		=> Repository.SaveAsync(this, token);

	public static TThis? LoadById(TId id)
	{
		if (Equals(id, default(TId)))
			return default;

		return (TThis?)Repository.LoadById(id);
	}

	public static async Task<TThis?> LoadByIdAsync(TId id, CancellationToken token = default)
	{
		if (Equals(id, default(TId)))
			return default;

		return (TThis?) await Repository.LoadByIdAsync(id, token).NoCtx();
	}

	public override string ToString() => $"{GetType().Name}: {Id}";
}