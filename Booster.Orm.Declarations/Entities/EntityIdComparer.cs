using System.Collections;
using System.Collections.Generic;
using Booster.Orm.Interfaces;
using JetBrains.Annotations;

namespace Booster.Orm;

[PublicAPI]
public class EntityIdComparer<TEntity> : EntityIdComparer, IEqualityComparer<TEntity> where TEntity : IEntityId
{
	public static new readonly EntityIdComparer<TEntity> Instance = new();
	
	public bool Equals(TEntity x, TEntity y) 
		=> base.Equals(x, y);

	public int GetHashCode(TEntity obj) 
		=> base.GetHashCode(obj);
}

public class EntityIdComparer : IEqualityComparer<IEntityId>, IEqualityComparer
{
	public static readonly EntityIdComparer Instance = new();
	readonly bool _compareTypes;

	public EntityIdComparer() : this(false)
	{
	}

	public EntityIdComparer(bool compareTypes)
		=> _compareTypes = compareTypes;

	public bool Equals(IEntityId x, IEntityId y)
	{
		if (x == null)
			return y == null;

		if (y == null)
			return false;

		if(_compareTypes && x.GetType() != y.GetType())
			return false;

		return Equals(x.Id, y.Id);
	}

	public int GetHashCode(IEntityId obj)
		=> new HashCode(obj.Id, _compareTypes ? obj.GetType() : null);

	bool IEqualityComparer.Equals(object x, object y)
	{
		var a = x as IEntityId;
		if (a == null && x != null)
			return false;

		var b = y as IEntityId;
		if (b == null && y != null)
			return false;

		return Equals(a, b);
	}

	int IEqualityComparer.GetHashCode(object obj)
		=> obj is IEntityId ent ? GetHashCode(ent) : 0;
}