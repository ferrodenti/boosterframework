using System.Linq;
using System.Reflection;
using Booster.Orm.Interfaces;
using Booster.Reflection;

namespace Booster.Orm;

public class EntityInfo
{
	public TypeEx EntityType { get; }
	public Assembly Assembly { get; set; }
	public string Namespace { get; set; }
		
	public DbAttribute DbAttribute { get; set; }
	public DbDictAttribute DbDictAttribute { get; set; }
	public DbInstanceContextAttribute DbInstanceContextAttribute { get; set; }
	public DbOldNameAttribute DbOldNameAttribute { get; set; }
	public DbSkipAttribute DbSkipAttribute { get; set; }
		
	public string InstanceContext { get; set; }
	public bool IsDictionary { get; set; }
	public string TableName { get; set; } 
	public string[] OldTableNames { get; set; }
	public DbUpdateOptions AutoMigrationOptions { get; set; } = DbUpdateOptions.Inherit;
	public AdapterImplementation AdapterImplementation { get; set; } = AdapterImplementation.All;
	public string DefaultSort { get; set; } 
	public bool Skip { get; set; } 

	public object Tag { get; set; }
		
	public EntityInfo(TypeEx entityType, IOrm orm = null)
	{
		EntityType = entityType;
		Assembly = entityType.Assembly;
		Namespace = entityType.Namespace;

		DbAttribute = entityType.FindAttribute<DbAttribute>();
		DbDictAttribute = entityType.FindAttribute<DbDictAttribute>();
		DbInstanceContextAttribute = entityType.FindAttribute<DbInstanceContextAttribute>();
		DbOldNameAttribute = entityType.FindAttribute<DbOldNameAttribute>();
		DbSkipAttribute = entityType.FindAttribute<DbSkipAttribute>();

		if (DbAttribute != null)
		{
			TableName = DbAttribute.Mapping;
			AutoMigrationOptions = DbAttribute.UpdateOptions;
			DefaultSort = DbAttribute.DefaultSort;
		}

		if (DbSkipAttribute != null)
			Skip = DbSkipAttribute.Evaluate(EntityType);
			
		IsDictionary = DbDictAttribute != null;
		TableName ??= orm?.SqlFormatter.CreateTableName(entityType.Name) ?? entityType.Name;
		OldTableNames = DbOldNameAttribute?.OldNames.ToArray();
		InstanceContext = DbInstanceContextAttribute?.ContextName ?? orm?.InstanceContext ?? "DEFAULT";
	}

	public override string ToString()
		=> $"{(Skip ? "SKIP " : "")}{(IsDictionary ? "Dictionary" : "Table")} \"{TableName}\" ({EntityType.Name})";
}