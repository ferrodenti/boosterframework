﻿using System;

namespace Booster.Orm;

[Serializable]
public abstract class IndexedEntity<TThis> : Entity<TThis, int>
{
}