﻿using System;

namespace Booster.Orm;

[Serializable]
public abstract class LIndexedEntity<TThis> : Entity<TThis, long>
{
}