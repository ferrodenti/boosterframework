#if !NETSTANDARD2_1
using System;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using Booster.AsyncLinq;
using Booster.Orm.Interfaces;
using JetBrains.Annotations;

namespace Booster.Orm
{
	public class AsyncDbReader<T> : IAsyncEnumerable<T>
	{
		class Enumerator : IAsyncEnumerator<T>
		{
			readonly AsyncDbReader<T> _parent;
			volatile IConnection? _connection;
			volatile DbDataReader? _reader;
			readonly CancellationToken _token;
			
			public Enumerator(AsyncDbReader<T> parent, CancellationToken token)
			{
				_parent = parent;
				_token = token;
			}

			public async Task<bool> MoveNextAsync()
			{
				if (_reader == null) //TODO: retry envelope here
				{
					var conn = _parent._connection ?? (_connection = _parent._connectionConstructor());
					_reader = await conn.ExecuteReaderAsync(() => _parent._commandConstructor(conn), _token).NoCtx();
				}

				return await _reader.ReadAsync(_token).NoCtx();
			}

			object IAsyncEnumerator.Current => Current;
			public T Current => _parent._body(_reader);

			public Task DisposeAsync()
			{
				_reader?.Dispose();
				_reader = null;

				_connection?.Dispose();
				_connection = null;

				return Task.CompletedTask;
			}
		}

		readonly IConnection? _connection;
		readonly Func<IConnection> _connectionConstructor;
		readonly Func<IConnection, Task<DbCommand>> _commandConstructor;
		readonly Func<DbDataReader, T> _body;

		/// <summary>
		/// Будет создано новое соединение при вызове GetAsyncEnumerator()...MoveNextAsync(), которое будет закрыто при удалении Enumerator
		/// </summary>
		/// <param name="connectionConstructor">Конструктор соединения</param>
		/// <param name="commandConstructor">Конструктор команды</param>
		/// <param name="body">Тело читателя записи</param>
		[PublicAPI("User in RepoAdapter implementation")]
		public AsyncDbReader(Func<IConnection> connectionConstructor,
			Func<IConnection, Task<DbCommand>> commandConstructor,
			Func<DbDataReader, T> body)
		{
			_connectionConstructor = connectionConstructor;
			_commandConstructor = commandConstructor;
			_body = body;
		}
		/// <summary>
		/// Используется существующее соединение, которое не закрывается при удалении Enumerator
		/// </summary>
		/// <param name="connection">Существующее соединение</param>
		/// <param name="commandConstructor">Конструктор команды</param>
		/// <param name="body">Тело читателя записи</param>
		public AsyncDbReader(IConnection connection,
			Func<IConnection, Task<DbCommand>> commandConstructor,
			Func<DbDataReader, T> body)
		{
			_connection = connection;
			_commandConstructor = commandConstructor;
			_body = body;
		}

		public IAsyncEnumerator<T> GetAsyncEnumerator(CancellationToken token = default) 
			=> new Enumerator(this, token);

		// IAsyncEnumerator IAsyncEnumerable.GetAsyncEnumerator(CancellationToken token) 
		// 	=> GetAsyncEnumerator(token);
	}
}
#endif