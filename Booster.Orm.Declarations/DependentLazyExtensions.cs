﻿using System;
using System.Collections.Generic;
using Booster.Interfaces;
using Booster.Orm.Interfaces;
using JetBrains.Annotations;

namespace Booster.Orm;

[PublicAPI]
public static class DependentLazyExtensions
{
	public static void AddDependency<TEntity>(this IDependentLazy lazy, TEntity entity) where TEntity : IEntityId 
		=> lazy.GetDependency().Add<TEntity>(o => o == null || Equals(entity.Id, o.Id));

	public static void AddDependency<TEntity>(this IDependentLazy lazy, Func<TEntity, bool> condition = null) 
		=> lazy.GetDependency().Add(condition);

	public static void AddDependency<T>(this IDependentLazy lazy, IEnumerable<T> entities)
	{
		var dep = lazy.GetDependency();
		foreach (var entity in entities)
			dep.Add(entity);
	}
}