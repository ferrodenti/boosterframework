using System.Collections.Generic;
using System.Linq;
using Booster.DI;
using Booster.Orm.Interfaces;
using Booster.Reflection;

#nullable enable

namespace Booster.Orm;

public static class RepositoryInstance<TEntity>
{
	static readonly object _lock = new();
	static readonly HashSet<IRepository> _repositories = new();

	static readonly CtxLocal<IRepository> _repository = new(true);
	static IRepository? Repository => _repository.Value;

	static readonly string? _constContext;

	static RepositoryInstance()
	{
		if (TypeEx.Get<TEntity>().TryFindAttribute(out DbInstanceContextAttribute attr))
			_constContext = attr.ContextName;

		InstanceFactory.On<IOrm>().Changed += (_, _) =>
		{
			lock (_lock)
			{
				if (TryFindRepo(_constContext ?? InstanceFactory.CurrentContext, out var repo))
					_repositories.Remove(repo);

				_repository.Value = null;
			}
		};
	}

	public static IRepository Value => GetRepo(true)!;

	public static IRepository? GetRepo(bool throwIfNotFound)
	{
		var ctx = _constContext ?? InstanceFactory.CurrentContext;
		var repo = Repository;
		if (repo != null && repo.InstanceContext == ctx)
			return repo;

		if (!TryFindRepo(ctx, out repo))
			lock (_lock)
				if (!TryFindRepo(ctx, out repo))
				{
					var orm = InstanceFactory.Get<IOrm>(ctx, throwIfNotFound);

					repo = orm?.GetRepo<TEntity>(throwIfNotFound);

					if (repo != null)
						_repositories.Add(repo);
				}

		if (repo != null)
			_repository.Value = repo;

		return repo;
	}

	public static T GetValueOf<T>()
		=> (T) Value;

	public static IRepository<TId> GetValue<TId>()
		=> (IRepository<TId>) Value;

	static bool TryFindRepo(string ctx, out IRepository result)
	{
		lock (_lock)
		{
			var repo = _repositories.FirstOrDefault(repo => repo.InstanceContext == ctx);
			if (repo != null)
			{
				result = repo;
				return true;
			}
			result = default!;
			return false;
		}
	}
}