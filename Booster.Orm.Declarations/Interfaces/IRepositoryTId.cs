﻿using System.Threading;
using System.Threading.Tasks;

#nullable enable

namespace Booster.Orm.Interfaces;

public interface IRepository<TId> : IRepository
{
	void Save(IEntity<TId> entity);
	Task SaveAsync(IEntity<TId> entity, CancellationToken token = default);
	IEntity<TId>? LoadById(TId id);
	Task<IEntity<TId>?> LoadByIdAsync(TId id, CancellationToken token = default);
}