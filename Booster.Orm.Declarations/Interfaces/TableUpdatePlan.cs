﻿using System.Collections.Generic;

namespace Booster.Orm.Interfaces;

public class TableUpdatePlan
{
	public readonly List<(IColumnSchema, IModelDataMember)> ToRename = new();
	public readonly List<IModelDataMember> ToCreate = new();
	public readonly List<IColumnSchema> ToDelete = new();
	public readonly List<IColumnMapping> ToChange = new();

	public bool IsSome => ToRename.Count > 0 || ToCreate.Count > 0 || ToDelete.Count > 0 || ToChange.Count > 0;
}