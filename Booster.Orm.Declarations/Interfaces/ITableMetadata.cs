namespace Booster.Orm.Interfaces;

public interface ITableMetadata
{
	string this[string key] { get; set; }

	int Version { get; set; }

	TValue Get<TValue>(string key);
	void Set(string key, object value);
}