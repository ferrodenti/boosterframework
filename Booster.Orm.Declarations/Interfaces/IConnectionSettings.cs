namespace Booster.Orm.Interfaces;

public interface IConnectionSettings
{
	string ConnectionString { get; set; }
	bool StaticConnection { get; set; }		
	bool AllowDebugAdapters { get; set; }
	bool Sharing { get; set; }
	bool DebugWrappers { get; set; }
	string AssembliesPath { get; set; }
	string InitialString { get; }

	DataReadersProtection ProtectDataReaders { get; set; }
	TraceSqlOptions TraceSql { get; set; }
	DbUpdateOptions DbUpdateOptions { get; set; }
	ConnectionPoolSettings ConnectionPool { get; set; }

	bool ContainsKey(string key);
}