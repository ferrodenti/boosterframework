﻿using System.Threading;
using System.Threading.Tasks;

namespace Booster.Orm.Interfaces;

public interface IEntityId : IEntity
{
	object Id { get; set; }

	void Save();
	Task SaveAsync(CancellationToken token = default);
}