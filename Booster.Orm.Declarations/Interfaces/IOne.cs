using Booster.Interfaces;

#nullable enable

namespace Booster.Orm.Interfaces;

public interface IOne<TEntity, TId> : IConvertiableFrom<TId>
{
	TId Index { get; set; }
	TEntity? Entity { get; set; }
}