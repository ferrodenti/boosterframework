namespace Booster.Orm.Interfaces;

public interface ISqlFormatter
{
	string ParamPrefix { get; }

	bool AllowArrayParam { get; }
		
	string CreateColumnName(string memberName); //TODO: CreateIdentifierName
	string CreateTableName(string className);
	string EscapeName(string name);
	string ParamName(string param);
	string FormatConst(object value);
	string EscapeString(string value);
}