﻿using System.Threading.Tasks;

#nullable enable

namespace Booster.Orm.Interfaces;

public interface IDbManager
{
	Task<bool> TableExists(string tableName, bool isView);
	Task<bool> RenameTable(string oldName, string newName, bool isView);
	Task<bool> ReadTable(string tableName, ITableSchema schema);
	TableUpdatePlan? GetUpdatePlan(IModelSchema schema, ITableMapping mapping);
	Task<bool> UpdateTable(IModelSchema schema, ITableSchema tableSchema, ITableMapping mapping, TableUpdatePlan plan);
	Task CheckAdditionalObjects(IModelSchema schema, ITableMapping mapping);
		
	Task<ITableSchema> CreateTable(IModelSchema schema, ITableSchema tableSchema);
	Task<IColumnSchema> CreateColumn(ITableMapping mapping, ITableSchema tableSchema, IModelDataMember dmSchema);
	Task DeleteColumn(string tableName, IColumnSchema col, IModelSchema schema);
	Task ChangeColumnType(string tableName, IColumnMapping col, IModelSchema schema);

	QueryBuilder GetSequenceNextValQuery(IDbSequence sequence);
	Task<bool> SequenceExist(IDbSequence sequence);
	Task CreateSequence(ITableMapping? mapping, IDbSequence sequence);
	Task OnDropSequence();
}