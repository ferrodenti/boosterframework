using System.Collections.Generic;
using Booster.Reflection;

namespace Booster.Orm.Interfaces;

public interface IModelSchema : IDbObject, IEnumerable<IModelDataMember>
{
	string TableName { get; }
	TypeEx ModelType { get; }
	string DefaultSort { get; }
	string InstanceContext { get; }
		
	AdapterImplementation AdapterImplementation { get; set; }

	string[] OldNames { get; }
	IDbIndex[] Indexes { get; }
	IDbConstraint[] Constraints { get; }
	IDbSequence[] Sequences { get; }
		
	bool IsDictionary { get; }
	bool IsView { get; }
	bool RowIdAsPk { get; }
	CacheSettings DictionaryCacheSettings { get; }
}