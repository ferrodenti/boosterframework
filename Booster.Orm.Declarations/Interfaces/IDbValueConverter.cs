using System;
using JetBrains.Annotations;

namespace Booster.Orm.Interfaces;

#nullable enable

/// <summary>
/// Интерфейс конвертора значений, используемого для изменения записей в БД
/// </summary>
[PublicAPI]
public interface IDbValueConverter
{
	/// <summary>
	/// Список типов, которые конвертер обрабатывает. Если тип параметра совпадает с одним из типов ActiveTypes,
	/// будет вызываться соответствующий метод преобразования значения, либо обобщенный метод Convert
	/// </summary>
	Type[] ActiveTypes { get; }
		
	string ConvertString(string value, int size);
	string ConvertInt(string value, int size);
	string ConvertLong(string value, int size);
	string ConvertDateTime(string value, int size);
		
	T? Convert<T>(T? value, int size);
}