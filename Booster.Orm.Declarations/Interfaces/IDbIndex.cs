namespace Booster.Orm.Interfaces;

public interface IDbIndex
{
	IModelDataMember[] Columns { get; }
	IndexType Type { get; }
	string Custom { get; }
	string Name { get; set; }
	string TableName { get; set; }
	bool Cluster { get; }
}