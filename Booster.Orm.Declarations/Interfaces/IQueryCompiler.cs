using Booster.Orm.Linq;
using Booster.Orm.Linq.Interfaces;
using Booster.Orm.Linq.Kitchen;

namespace Booster.Orm.Interfaces;

public interface IQueryCompiler
{
	CompiledQueryBuilder Compile(IQuery query, QueryCompilationContext context);
}