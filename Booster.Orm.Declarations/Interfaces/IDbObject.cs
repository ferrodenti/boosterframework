namespace Booster.Orm.Interfaces;

public interface IDbObject
{
	DbUpdateOptions DbUpdateOptions { get; }
}

// ReSharper disable once InconsistentNaming
public static class IDbObjectExpander
{
	public static bool AllowAutoUpdateDb(this IDbObject obj)
		=> !obj.DbUpdateOptions.HasFlag(DbUpdateOptions.Deny);

	public static bool AllowAutoUpdateDb(this IDbObject obj, DbUpdateOptions options)
		=> obj.AllowAutoUpdateDb() && obj.DbUpdateOptions.HasFlag(options);
}