using System.Threading.Tasks;

#nullable enable

namespace Booster.Orm.Interfaces;

public interface IDbSequence
{
	string? Name { get; set; }
	long Start { get; }
	long Step { get; }
	
	SequenceType Type { get; }
	IModelDataMember? Target { get; }
	bool ApplyToColumn { get; }
	bool CheckExistingValues { get; }

	Task EnsureExist(IOrm orm);
	Task<long> GetNextValue(IOrm orm);
}
