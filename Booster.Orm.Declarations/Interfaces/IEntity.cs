﻿using System.Threading;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace Booster.Orm.Interfaces;

[PublicAPI]
public interface IEntity
{
	void Update(); //TODO: Cascade?
	Task UpdateAsync(CancellationToken token = default);

	void Insert();
	Task InsertAsync(CancellationToken token = default);

	bool Delete(); //TODO: Cascade?
	Task<bool> DeleteAsync(CancellationToken token = default);

	void InsertWithPks();
	Task InsertWithPksAsync(CancellationToken token = default);

	void UpdateWithPks(object entity, object[] pks);
	Task UpdateWithPksAsync(object entity, object[] pks, CancellationToken token = default);


	void OnLoad();
	void OnLoaded();
	
	void OnInsert();
	void OnInserted();

	void OnUpdate();
	void OnUpdated();

	void OnSave();
	void OnSaved();

	void OnDelete();
	void OnDeleted();

	void OnTableChange();
	void OnTableChanged();

	TValue ReadUnmapped<TValue>(string fieldName);
	void WriteUnmapped<TValue>(string fieldName, TValue value);

	TValue ReadUnmapped<TValue>(string fieldName, ref TValue store);
	void WriteUnmapped<TValue>(string fieldName, TValue value, ref TValue store);

	Task<TValue> ReadUnmappedAsync<TValue>(string fieldName, CancellationToken token = default);
	Task WriteUnmappedAsync<TValue>(string fieldName, TValue value, CancellationToken token = default);
}