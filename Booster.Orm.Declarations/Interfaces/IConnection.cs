﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using JetBrains.Annotations;

#nullable enable

#if !NETSTANDARD2_1
using Booster.AsyncLinq;
#endif

namespace Booster.Orm.Interfaces
{
	[PublicAPI]
	public interface IConnection : IDisposable
	{
		IOrm Orm { get; }
		//bool TryReconnect { get; set; }

		void Entry();

		ITransaction BeginTransaction();

		Task<DbCommand> CreateCommandAsync(FormattableString formattable, CancellationToken token = default);
		Task<DbCommand> CreateCommandAsync(string command, DbParameter[]? parameters, CancellationToken token = default);
		Task<DbCommand> CreateCommandAsync(string command, object? parameters, CancellationToken token = default);

		Task<DbCommand> CreateCommandAsync(QueryBuilder queryBuilder, CancellationToken token = default);
		
		DbCommand CreateCommand(FormattableString formattable);
		DbCommand CreateCommand(string command, DbParameter[]? parameters);
		DbCommand CreateCommand(string command, object? parameters);
		
		void Ping();
		Task PingAsync(CancellationToken token = default);
		
		#region ExecuteXXX(DbParameter[])
		#region Sync
		DbDataReader ExecuteReader(string command, DbParameter[]? parameters);
		int ExecuteNoQuery(string command, DbParameter[]? parameters);
		object? ExecuteScalar(string command, DbParameter[]? parameters);
		T? ExecuteScalar<T>(string command, DbParameter[]? parameters);
		T? ExecuteScalar<T>(string command, DbParameter[]? parameters, T @default);
		
		IEnumerable<T?> ExecuteEnumerable<T>(string command, DbParameter[]? parameters);
		T? ExecuteTuple<T>(string command, DbParameter[]? parameters);
		IEnumerable<T> ExecuteTuples<T>(string command, DbParameter[]? parameters);
		#endregion
		
		#region Async
		Task<DbDataReader> ExecuteReaderAsync(Func<Task<DbCommand>> command, CancellationToken token = default);
		Task<DbDataReader> ExecuteReaderAsync(string command, DbParameter[]? parameters, CancellationToken token = default);
		Task<int> ExecuteNoQueryAsync(string command, DbParameter[]? parameters, CancellationToken token = default);
		Task<object?> ExecuteScalarAsync(string command, DbParameter[]? parameters, CancellationToken token = default);
		Task<T?> ExecuteScalarAsync<T>(string command, DbParameter[]? parameters, CancellationToken token = default);
		Task<T?> ExecuteScalarAsync<T>(string command, DbParameter[]? parameters, T @default, CancellationToken token = default);
		IAsyncEnumerable<T?> ExecuteEnumerableAsync<T>(string command, DbParameter[]? parameters, CancellationToken token = default);
		Task<T?> ExecuteTupleAsync<T>(string command, DbParameter[]? parameters, CancellationToken token = default);
		IAsyncEnumerable<T> ExecuteTuplesAsync<T>(string command, DbParameter[]? parameters, CancellationToken token = default);
		#endregion
		#endregion

		#region ExecuteXXX(object)
		#region Sync
		DbDataReader ExecuteReader(string command, object parameters);
		int ExecuteNoQuery(string command, object parameters);
		object? ExecuteScalar(string command, object parameters);
		T? ExecuteScalar<T>(string command, object parameters, T? @default = default);
		IEnumerable<T?> ExecuteEnumerable<T>(string command, object parameters);
		T ExecuteTuple<T>(string command, object parameters);
		IEnumerable<T> ExecuteTuples<T>(string command, object parameters);
		#endregion
		#region Async
		Task<DbDataReader> ExecuteReaderAsync(string command, object parameters, CancellationToken token = default);
		Task<int> ExecuteNoQueryAsync(string command, object parameters, CancellationToken token = default);
		Task<object?> ExecuteScalarAsync(string command, object parameters, CancellationToken token = default);
		Task<T?> ExecuteScalarAsync<T>(string command, object parameters, T? @default = default, CancellationToken token = default);
		IAsyncEnumerable<T?> ExecuteEnumerableAsync<T>(string command, object parameters, CancellationToken token = default);
		Task<T?> ExecuteTupleAsync<T>(string command, object parameters, CancellationToken token = default);
		IAsyncEnumerable<T> ExecuteTuplesAsync<T>(string command, object parameters, CancellationToken token = default);
		#endregion
		#endregion

		#region ExecuteXXX(FormattableString)
		#region Sync
		DbDataReader ExecuteReader(FormattableString formattable);
		int ExecuteNoQuery(FormattableString formattable);
		object? ExecuteScalar(FormattableString formattable);
		T? ExecuteScalar<T>(FormattableString formattables, T? @default = default);
		IEnumerable<T?> ExecuteEnumerable<T>(FormattableString formattable);
		T? ExecuteTuple<T>(FormattableString formattable);
		IEnumerable<T> ExecuteTuples<T>(FormattableString formattable);
		#endregion
		#region Async

		Task<DbDataReader> ExecuteReaderAsync(FormattableString formattable, CancellationToken token = default);
		Task<int> ExecuteNoQueryAsync(FormattableString formattable, CancellationToken token = default);
		Task<object?> ExecuteScalarAsync(FormattableString formattable, CancellationToken token = default);
		Task<T?> ExecuteScalarAsync<T>(FormattableString formattable, T? @default = default, CancellationToken token = default);
		IAsyncEnumerable<T?> ExecuteEnumerableAsync<T>(FormattableString formattable, CancellationToken token = default);
		Task<T?> ExecuteTupleAsync<T>(FormattableString formattable, CancellationToken token = default);
		IAsyncEnumerable<T> ExecuteTuplesAsync<T>(FormattableString formattable, CancellationToken token = default); 
		#endregion
		#endregion

		#region ExecuteXXX(QueryBuilder)
		#region Sync
		DbDataReader ExecuteReader(QueryBuilder query);
		int ExecuteNoQuery(QueryBuilder query);
		object? ExecuteScalar(QueryBuilder query);
		T? ExecuteScalar<T>(QueryBuilder query, T? @default = default);
		IEnumerable<T?> ExecuteEnumerable<T>(QueryBuilder query);
		T? ExecuteTuple<T>(QueryBuilder query);
		IEnumerable<T> ExecuteTuples<T>(QueryBuilder query);
		#endregion
		#region Async
		Task<DbDataReader> ExecuteReaderAsync(QueryBuilder query, CancellationToken token = default);
		Task<int> ExecuteNoQueryAsync(QueryBuilder query, CancellationToken token = default);
		Task<object?> ExecuteScalarAsync(QueryBuilder query, CancellationToken token = default);
		Task<T?> ExecuteScalarAsync<T>(QueryBuilder query, T? @default = default, CancellationToken token = default);
		IAsyncEnumerable<T?> ExecuteEnumerableAsync<T>(QueryBuilder query, CancellationToken token = default);
		Task<T?> ExecuteTupleAsync<T>(QueryBuilder query, CancellationToken token = default);
		IAsyncEnumerable<T> ExecuteTuplesAsync<T>(QueryBuilder query, CancellationToken token = default);
		#endregion
		#endregion
	}
}