using Booster.Reflection;

#nullable enable

namespace Booster.Orm.Interfaces;

public interface IDbTypeDescriptor
{
	bool IsInteger(string type);
	bool IsFloat(string type);
	bool IsString(string type);
	bool IsDateTime(string type);
	bool IsBoolean(string type);
	bool IsGuid(string type);
	bool IsBlob(string type);
	bool IsVariant(string type);

	bool IsInteger(TypeEx? type);
	bool IsFloat(TypeEx? type);
	bool IsString(TypeEx? type);
	bool IsDateTime(TypeEx? type);
	bool IsBoolean(TypeEx? type);
	bool IsGuid(TypeEx? type);
	bool IsBlob(TypeEx? type);
	bool IsVariant(TypeEx? type);

	bool IsNullable(TypeEx type);

	Method GetDataReaderMethod(TypeEx type);
	Method GetDataReaderMethod(string dbType);

	TypeEx? GetType(string dbType);
	TypeEx? GetType(string? dbType, int length, int scale);
	string? GetDbTypeFor(TypeEx type);

	string? ParseBaseType(string? type);
	string ParseType(string type, out int length, out int scale);

	TypeEx GetValueType(TypeEx type);
}