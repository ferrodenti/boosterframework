using System;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

namespace Booster.Orm.Interfaces;

public interface ITransaction : IDisposable
{
	Task<DbTransaction> GetInnerAsync(CancellationToken token);
	DbTransaction GetInner();
		
	void Entry();
	void Commit();
	void Rollback();
}