﻿using System.Collections.Generic;
using Booster.Reflection;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Orm.Interfaces;

public interface ITableMapping : IDbObject
{
	TypeEx ModelType { get; }
	string TableName { get; }
	string DefaultSort { get; }
	string InstanceContext { get; }
	TypeEx? PkType { get; }
	[PublicAPI("Для оракловых баз")]
	bool RowIdAsPk { get; }
	bool IsDictionary { get; }
	string Mold { get; }
		
	AdapterImplementation AdapterImplementation { get; }
	CacheSettings DictionaryCacheSettings { get; }

	ITableSchema TableSchema { get; }
	ITableMetadata Metadata { get; }

	List<IColumnMapping> Columns { get; }
	List<IColumnMapping> IdentityColumns { get; set; }
	List<IColumnMapping> ReadColumns { get; }
	List<IColumnMapping> WriteColumns { get; }
	List<IColumnMapping> PkColumns { get; }

	List<IModelDataMember> UnumappedDataMemebers { get; }
	List<IColumnSchema> UnumappedColumns { get; }

	void MapColumn(IModelDataMember dmSchema, IColumnSchema colSchema);

	IColumnMapping? GetColumnByName(string memberName, bool ignoreCase = true);
	IColumnMapping? GetColumnByDbName(string memberName, bool ignoreCase = true);

	bool TryGetColumnByName(string? columnName, out IColumnMapping columnMapping, bool ignoreCase = true);
	bool TryGetColumnByDbName(string? memberName, out IColumnMapping columnMapping, bool ignoreCase = true);
}