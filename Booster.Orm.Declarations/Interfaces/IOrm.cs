﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Booster.AsyncLinq;
using Booster.Interfaces;
using Booster.Reflection;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Orm.Interfaces;

[PublicAPI]
public interface IOrm : IUnregisterInstance, IDisposable
{
	IConnectionSettings ConnectionSettings { get; }
	RepositoryRegistrationOptions RepositoryRegistrationOptions { get; set; }

	string InstanceContext { get; }
	IDbManager DbManager { get; }
	ISqlFormatter SqlFormatter { get; } 
	IQueryCompiler QueryCompiler { get; }
	IDbTypeDescriptor DbTypeDescriptor { get; } 
	ICollection<IRepository> Repositories { get; }

	// IDisposable BeginInitialization();
	// void EndInitialization();
	// IAsyncDisposable BeginInitializationAsync();
	// Task EndInitializationAsync();

	IRepository? GetRepo<T>(bool throwIfNotFound);
	IRepository? GetRepo(TypeEx enityType, bool throwIfNotFound);
	IRepository? GetRepo(string tableName, bool throwIfNotFound);
	
	IRepository GetRepo<T>();
	IRepository GetRepo(TypeEx enityType);
	IRepository GetRepo(string tableName);

	bool TryGetRepo<TEntity>(out IRepository result);
	bool TryGetRepo(TypeEx enityType, out IRepository result);
	bool TryGetRepo(string tableName, out IRepository result);

	bool WaitRepoReady(TypeEx enityType, int timeout);
	Task WaitRepoReadyAsync(TypeEx enityType);

	event EventHandler<DbMigrationEventArgs> Migration;
		
	IConnection GetCurrentConnection();
	ITransaction BeginTransaction();

	EntityInfo[] FindEntities(Assembly assembly, string? @namespace = null);
		
	IRepository RegisterRepo(TypeEx entityType);
	Task<IRepository> RegisterRepoAsync(TypeEx entityType);	

	IRepository[] RegisterRepos(string? @namespace = null);
	IRepository[] RegisterRepos(Assembly assembly, string? @namespace = null);
	IRepository[] RegisterRepos(IEnumerable<Type> entityTypes);
	IRepository[] RegisterRepos(IEnumerable<EntityInfo> entities);
		
	Task<IRepository[]> RegisterReposAsync(Assembly assembly, string? @namespace = null);
	Task<IRepository[]> RegisterReposAsync(string? @namespace = null);
	Task<IRepository[]> RegisterReposAsync(IEnumerable<Type> entityTypes);
	Task<IRepository[]> RegisterReposAsync(IEnumerable<EntityInfo> entities);

	DbParameter CreateParameter(string name, TypeEx type);
	DbParameter CreateParameter(string name, object? value);
	DbParameter CreateParameter(string name, object? value, TypeEx type);
	DbParameter[] CreateParameters(object? parameters);

	void Ping(bool removePass = true);
	Task PingAsync(bool removePass = true, CancellationToken token = default);
		
	IHybridDisposable BeginInitialization();

	void EndInitialization();
	Task EndInitializationAsync();
}