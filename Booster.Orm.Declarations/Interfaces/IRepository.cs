﻿using System;
using System.Collections;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using Booster.Reflection;
using JetBrains.Annotations;

#nullable enable

#if !NETSTANDARD2_1
using Booster.AsyncLinq;

#else
using System.Collections.Generic;
#endif

namespace Booster.Orm.Interfaces;

[PublicAPI]
public interface IRepository
{
	TypeEx EntityType { get; }
	string TableName { get; }
	string InstanceContext { get; }
	int Version { get; set; }
	ITableMapping TableMapping { get; }

	IOrm Orm { get; }
	//IColumnMapping GetColumnMapping(string name, bool byDbName, bool ignoreCase);
	
	event EventHandler<RepositoryChangedEventArgs>? Inserted;
	event EventHandler<RepositoryChangedEventArgs>? Updated;
	event EventHandler<RepositoryChangedEventArgs>? Deleted;

	event EventHandler<RepositoryChangedEventArgs>? Saved;
	event EventHandler<RepositoryChangedEventArgs>? Changed;

	void OnInserted(object? entity);
	void OnUpdated(object? entity);
	void OnDeleted(object? entity);

	void Insert(object entity);
	void InsertWithPks(object entity);
	void Update(object entity);
	void UpdateWithPks(object entity, object[] pks);
	bool Delete(object entity);

	
	TValue ReadUnmapped<TValue>(object entity, string fieldName);
	void WriteUnmapped<TValue>(object entity, string fieldName, TValue value);

	int Count(string query, DbParameter[] parameters);
	int Count(string query, object parameters);
	int Count(QueryBuilder queryBuilder);
	long LCount(string query, DbParameter[] parameters);
	long LCount(string query, object parameters);
	long LCount(QueryBuilder queryBuilder);

	object? Load(string query, DbParameter[] parameters);
	object? Load(string query, object parameters);
	object? Load(QueryBuilder queryBuilder);
	object? LoadByPks(object[] pks);
	IEnumerable SelectByPks(IEnumerable pks);
	
	IEnumerable Select(string query, DbParameter[] parameters);
	IEnumerable Select(QueryBuilder queryBuilder);
	
	IEnumerable Select(string query, string sort, int pageSize, DbParameter[] parameters);
	IEnumerable Select(string query, object parameters);
	IEnumerable Select(string query, string sort, int pageSize, object parameters);

	IEnumerable Query(string query, object parameters);
	IEnumerable Query(QueryBuilder queryBuilder);

	IEnumerable Select();
	IEnumerable Select(int skip, int take, string condition, string sort, DbParameter[] parameters);
	IEnumerable Select(int skip, int take, string condition, string sort, object parameters);
	IEnumerable Select(int skip, int take, QueryBuilder queryBuildern, string sort);
	IEnumerable Select(long skip, int take, string condition, string sort, DbParameter[] parameters);
	IEnumerable Select(long skip, int take, string condition, string sort, object parameters);
	IEnumerable Select(long skip, int take, QueryBuilder queryBuildern, string sort);

	Task InsertAsync(object entity, CancellationToken token = default);
	Task InsertWithPksAsync(object entity, CancellationToken token = default);
	Task UpdateAsync(object entity, CancellationToken token = default);
	Task UpdateWithPksAsync(object entity, object[] pks, CancellationToken token = default);
	Task<bool> DeleteAsync(object entity, CancellationToken token = default);

	
	Task<TValue> ReadUnmappedAsync<TValue>(object entity, string fieldName, CancellationToken token = default);
	Task WriteUnmappedAsync<TValue>(object entity, string fieldName, TValue value, CancellationToken token = default);

	Task<int> CountAsync(string query, DbParameter[] parameters, CancellationToken token = default);
	Task<int> CountAsync(string query, object parameters, CancellationToken token = default);
	Task<int> CountAsync(QueryBuilder queryBuilder, CancellationToken token = default);
	Task<long> LCountAsync(string query, DbParameter[] parameters, CancellationToken token = default);
	Task<long> LCountAsync(string query, object parameters, CancellationToken token = default);
	Task<long> LCountAsync(QueryBuilder queryBuilder, CancellationToken token = default);

	Task<object?> LoadAsync(string query, DbParameter[] parameters, CancellationToken token = default);
	Task<object?> LoadAsync(string query, object parameters, CancellationToken token = default);
	Task<object?> LoadAsync(QueryBuilder queryBuilder, CancellationToken token = default);
	Task<object?> LoadByPksAsync(object[] pks, CancellationToken token = default);

	IAsyncEnumerable<object> SelectByPksAsync(IEnumerable pks, CancellationToken token = default);
	
	IAsyncEnumerable<object> SelectAsync(string query, DbParameter[]? parameters = null, CancellationToken token = default);
	IAsyncEnumerable<object> SelectAsync(QueryBuilder queryBuilder, CancellationToken token = default);
	
	IAsyncEnumerable<object> SelectAsync(string query, string sort, int pageSize, DbParameter[]? parameters = null, CancellationToken token = default);
	IAsyncEnumerable<object> SelectAsync(string query, object parameters, CancellationToken token = default);
	IAsyncEnumerable<object> SelectAsync(string query, string sort, int pageSize, object parameters, CancellationToken token = default);

	IAsyncEnumerable<object> QueryAsync(string query, object parameters, CancellationToken token = default);
	IAsyncEnumerable<object> QueryAsync(QueryBuilder queryBuilder, CancellationToken token = default);

	IAsyncEnumerable<object> SelectAsync(CancellationToken token = default);
	IAsyncEnumerable<object> SelectAsync(int skip, int take, string condition, string? sort, DbParameter[] parameters, CancellationToken token = default);
	IAsyncEnumerable<object> SelectAsync(int skip, int take, string condition, string? sort, object parameters, CancellationToken token = default);
	IAsyncEnumerable<object> SelectAsync(int skip, int take, QueryBuilder queryBuilder, CancellationToken token = default);
	IAsyncEnumerable<object> SelectAsync(long skip, int take, string condition, string? sort, DbParameter[] parameters, CancellationToken token = default);
	IAsyncEnumerable<object> SelectAsync(long skip, int take, string condition, string? sort, object parameters, CancellationToken token = default);
	IAsyncEnumerable<object> SelectAsync(long skip, int take, QueryBuilder queryBuilder, CancellationToken token = default);
}

