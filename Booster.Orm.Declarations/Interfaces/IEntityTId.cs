﻿namespace Booster.Orm.Interfaces;

public interface IEntity<TId> : IEntityId
{
	new TId Id { get; set; }
}