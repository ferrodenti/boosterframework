namespace Booster.Orm.Interfaces;

public interface IDbConstraint
{
	IModelDataMember[] Columns { get; }
	string Custom { get; }
	string Name { get; set; }
	string TableName { get; }
	ConstraintType Type { get; }
}