using System.Collections.Generic;

namespace Booster.Orm.Interfaces;

public interface ITableSchema : IEnumerable<IColumnSchema>
{
	string DbSchema { get; }
	string TableName { get; }

	ITableMetadata Metadata { get; }

	IColumnSchema this[string name] { get; }

	void SetColumns(IEnumerable<IColumnSchema> columns);
	void AddColumn(IColumnSchema column);
	void ChanageColumnName(IColumnSchema updatedColumn, string newName);
}