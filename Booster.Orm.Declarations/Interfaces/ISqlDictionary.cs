namespace Booster.Orm.Interfaces;

public interface ISqlDictionary
{
	CacheSettings CacheSettings { get; }
	void Reset();
}
public interface ISqlDictionary<TId, TEntity> : ISqlDictionary, IRepository<TId> where TEntity : IEntity<TId>
{
	bool TryGetByPk(TId id, out TEntity entity);
	void Set(TEntity entity);
}