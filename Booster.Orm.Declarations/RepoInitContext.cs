using System.Threading.Tasks;
using Booster.DI;
using Booster.Orm.Interfaces;
using Booster.Reflection;
using JetBrains.Annotations;

namespace Booster.Orm;

[PublicAPI]
public class RepoInitContext
{
	public bool Exists { get; set; }
	public IOrm Orm { get; }

	public string OldTableName { get; set; }
	public string TableName { get; set; }

	public TypeEx EntityType { get; }
		
	int? _version;
	public int Version
	{
		get => _version ?? (_version = Metadata?.Version ?? 0).Value;
		set
		{
			if (_version != value)
			{
				_version = value;
				if (Metadata != null)
					Metadata.Version = value;
			}
		}
	}

	public IModelSchema ModelSchema { get; }

	ITableMetadata _metadata;
	public ITableMetadata Metadata
	{
		get => _metadata;
		set
		{
			_metadata = value;
			if (_metadata != null && _version.HasValue)
				_metadata.Version = _version.Value;
		}
	}

	public ITableSchema TableSchema { get; set; }
		
	public RepoInitContext(IOrm orm, IModelSchema modelSchema)
	{
		Orm = orm;
		ModelSchema = modelSchema;
		OldTableName = TableName = modelSchema.TableName;
		EntityType = modelSchema.ModelType;
	}

	public void WaitFor<TEntity>()
		=> WaitFor(typeof (TEntity));
	
	public void WaitFor(TypeEx entityType)
		=> GetEntityOrm(entityType)
			.WaitRepoReady(entityType, -1);
	
	public Task WaitForAsync<TEntity>()
		=> WaitForAsync(typeof (TEntity));

	public Task WaitForAsync(TypeEx entityType)
		=> GetEntityOrm(entityType)
			.WaitRepoReadyAsync(entityType);

	IOrm GetEntityOrm(TypeEx entityType)
	{
		if (entityType.TryFindAttribute<DbInstanceContextAttribute>(out var attr) &&
		    attr.ContextName != InstanceFactory.CurrentContext)
			return InstanceFactory.Get<IOrm>(attr.ContextName);

		return Orm;
	}
}