﻿using System;
using System.Linq.Expressions;
using Booster.Orm.Linq.Interfaces;

namespace Booster.Orm.Linq;

public class SelectVectorQuery<TModel, TResult> : BaseSelect<SelectVectorQuery<TModel, TResult>, TModel, TResult>, ISelectVectorQuery<TResult>
{
	public IValue<string> ToString(string separator)
		=> null;

	public SelectVectorQuery(Expression<Func<TModel,TResult>> member) : base(new Expression[]{member})
	{
	}
		
	public SelectVectorQuery(string field) : base(new Expression[]{Expression.Constant(new FieldRef(field))})
	{
	}
}