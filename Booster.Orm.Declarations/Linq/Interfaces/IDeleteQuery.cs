
using System.Linq.Expressions;

namespace Booster.Orm.Linq.Interfaces;

public interface IDeleteQuery : IQuery
{
	Expression WhereCondition { get; }
}