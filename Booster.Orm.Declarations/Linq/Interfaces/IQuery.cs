namespace Booster.Orm.Linq.Interfaces;

public interface IQuery
{
	string TableName { get; }
	IAlias AliasObj { get; }
}