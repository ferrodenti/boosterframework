namespace Booster.Orm.Linq.Interfaces;

public interface ICondition : IValue<bool>
{
	
}