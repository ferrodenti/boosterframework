namespace Booster.Orm.Linq.Interfaces;

public interface ISelectVectorQuery : ISelectQuery
{
		
}

public interface ISelectVectorQuery<TResult> : ISelectVectorQuery
{
		
}