namespace Booster.Orm.Linq.Interfaces;

public interface IValue
{
}
public interface IValue<T> : IValue
{
}