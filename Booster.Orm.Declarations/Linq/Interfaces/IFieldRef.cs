namespace Booster.Orm.Linq.Interfaces;

public interface IFieldRef : IValue
{
}
public interface IFieldRef<T> : IFieldRef
{
}