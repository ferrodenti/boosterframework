using System.Collections.Generic;
using System.Linq.Expressions;

namespace Booster.Orm.Linq.Interfaces;

public interface IUpdateQuery : IQuery
{
	Expression WhereCondition { get; }
	List<Setter> Setters { get; }
}