namespace Booster.Orm.Linq.Interfaces;

public interface IAlias
{
	string GetName(QueryCompilationContext context);
}