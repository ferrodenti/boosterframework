using System.Collections.Generic;

namespace Booster.Orm.Linq.Interfaces;

public interface IInsertQuery : IQuery
{
	List<Setter> Setters { get; }
	ISelectQuery FromQuery { get; }
	string[] OnConflictColumns { get; }
	InsertConflictUpdateSet InsertConflictUpdateSet { get; }
}