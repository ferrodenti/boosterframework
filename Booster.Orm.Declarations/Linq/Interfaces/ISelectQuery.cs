using System.Linq.Expressions;

namespace Booster.Orm.Linq.Interfaces;

public interface ISelectQuery : IQuery
{
	Expression[] Columns { get; }
	ISelectQuery FromQuery { get; }
	string FromQueryAs { get; }
	Expression WhereCondition { get; }
	Expression[] Groups { get; }
	Expression[] Orders { get; }
}