using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using Booster.Orm.Interfaces;
using Booster.Orm.Linq.Interfaces;
using Booster.Orm.Linq.Kitchen;
using Booster.Orm.Manies.Inline;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Orm.Linq;

[PublicAPI]
public abstract class CompiledQuery : IQuery
{
	internal abstract IOrm Orm { get; }

	public abstract string TableName { get; }
	public IAlias? AliasObj { get; protected set; }


	public static SelectEntityQuery<TModel> Select<TModel>(params Expression<Func<TModel, object>>[] fields)
		=> new(fields);

	[ExecuteImmediately]
	public static SelectVectorQuery<TModel, TResult> Select<TModel, TResult>(Expression<Func<TModel,TResult>> member)
		=> new(member);

	public static SelectVectorQuery<TModel, TResult> Select<TModel, TResult>(string field)
		=> new(field);

		
	public static DeleteQuery<TModel> Delete<TModel>()
		=> new();

	public static DeleteQuery<TModel> Delete<TModel>(Expression<Func<TModel, bool>> condition)
		=> new DeleteQuery<TModel>().Where(condition);

	public static SelectMemQuery Select(params Expression<Func<object>>[] fields)
		=> new(fields);
		
	public static InsertQuery<TModel> Insert<TModel>()
		=> new();
	
	public static InsertQuery<TModel> Insert<TModel>(params Expression<Func<TModel,object>>[] members)
		=> new(members);

	public static UpdateQuery<TModel> Update<TModel>()
		=> new();

	[ExecuteImmediately]
	public static ParamRef<T> Param<T>(string name)
		=> new(name);
		
	[Operator("Param")]
	public static ParamRef<T> Param<T>(T value)
		=> new();

	[Operator("Param")]
	public static ParamRef<T> Param<T>(T value, string name)
		=> new() {Name = name};
		
	[ExecuteImmediately]
	public static ParamRef<TResult> Param<TResult>()
		=> new();
		
	[ExecuteImmediately]
	public static FieldRef Field(string field)
		=> new(field);
		
	[ExecuteImmediately]
	public static FieldRef<T> Field<T>(string field)
		=> new(field);

	[Operator("As")]
	public static T As<T>(T expression, string name)
		=> expression;

	[Operator("Distinct")]
	public static T Distinct<T>(T expression)
		=> default!;

	[Operator("Any")]
	public static T Any<T>(Func<ISelectVectorQuery<T>> expression)
		=> default!;

	[Operator("Any")]
	public static T Any<T>(ISelectVectorQuery<T> expression)
		=> default!;
		
	[Operator("Any")]
	public static T Any<T>(IEnumerable<T> expression)
		=> default!;
		
	[Operator("Any")]
	public static T Any<T>(ParamRef<T[]> expression)
		=> default!;

	[Operator("Any")]
	public static TId Any<TEntity, TId>(InlineArrayMany<TEntity, TId> many) where TEntity : class, IEntity<TId>
		=> default!;


	[Operator("All")]
	public static bool All<T>(ISelectVectorQuery<T> expression)
		=> default;
		
	[Operator("All")]
	public static bool All<T>(IEnumerable<T> expression)
		=> default;
		
	[Operator("All")]
	public static bool All<T>(ParamRef<T[]> expression)
		=> default;

	[Operator("All")]
	public static TId All<TEntity, TId>(InlineArrayMany<TEntity, TId> many) where TEntity : class, IEntity<TId>
		=> default!;
		
	[Operator("Sum")]
	public static T Sum<T>(T expression)
		=> default!;

	[Operator("AggBitAnd")]
	public static T AggBitAnd<T>(T expression)
		=> default!;		
		
	[Operator("AggBitOr")]
	public static T AggBitOr<T>(T expression)
		=> default!;

	[Operator("AggBitXor")]
	public static T AggBitXor<T>(T expression)
		=> default!;
		
	[Operator("Count")]
	public static int Count()
		=> 0;

	[Operator("Count")]
	public static int Count(object expression)
		=> 0;

	public static SelectScalarQuery<TModel, int> Count<TModel>(Expression<Func<TModel, bool>> expression)
		=> new SelectScalarQuery<TModel, int>(_ => Count()).Where(expression);


	public static SelectScalarQuery<TModel, TResult> Scalar<TModel, TResult>(Expression<Func<TModel, TResult>> expression)
		=> new(expression);

	readonly SyncLazy<(string sql, QueryParameters parameters)> _compiled = SyncLazy<(string sql, QueryParameters parameters)>.Create<CompiledQuery>(self =>
	{
		var context = new QueryCompilationContext(self.Orm);
		var result = self.Orm.QueryCompiler.Compile(self, context);
		return (result.GetSql(), context.GetQueryParameters());
	});

	readonly SyncLazy<string> _sql = SyncLazy<string>.Create<CompiledQuery>(self => self.GetCompiled().sql);

	public string Sql => _sql.GetValue(this);

	public (string sql, QueryParameters parameters) GetCompiled()
		=> _compiled.GetValue(this);

	public override string ToString()
		=> Sql;

	public DbParameter[] GetDbParameters(object target)
		=> GetCompiled().parameters.ToDbParameters(target);

	public DbParameter[] MergeDbParameters(IEnumerable<DbParameter> first, object second)
		=> GetCompiled().parameters.Merge(first, second);

	static readonly ConcurrentDictionary<string, CompiledQuery> _cache = new();

	public static PreparedQuery CompileAndCache<TThis, TModel, TResult>(
		Func<CompiledQuery<TThis, TModel, TResult>> creator,
		[CallerFilePath] string? source = null,
		[CallerMemberName] string? member = null,
		[CallerLineNumber] int line = 0) where TThis : class
	{
		var query = (CompiledQuery<TThis, TModel, TResult>) _cache.GetOrAdd(string.Concat(source, member, line), _ => creator());
		return new PreparedQuery(query, creator.Target);
	}
}

// ReSharper disable once UnusedTypeParameter
public abstract class CompiledQuery<TThis, TModel, TResult> : CompiledQuery where TThis : class
{
	internal override IOrm Orm => Repository.Orm;
	internal IRepository Repository => RepositoryInstance<TModel>.Value;
	public override string TableName => Repository.TableName;

	internal string? GetColumnName<TOut>(Expression<Func<TModel, TOut>> member)
	{
		var mem = member.ParseMemberRef();
		return Repository.TableMapping.GetColumnByName(mem.Name)?.EscapedColumnName;
	}

	public TThis As(out Alias<TModel> aliasRef)
	{
		AliasObj = aliasRef = new Alias<TModel>();
		return (this as TThis)!;
	}
}