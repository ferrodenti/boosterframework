using System;
using System.Linq.Expressions;
using Booster.Orm.Linq.Interfaces;

namespace Booster.Orm.Linq;

public class DeleteQuery<TModel> : CompiledQuery<DeleteQuery<TModel>, TModel, int>, IDeleteQuery
{
	public Expression WhereCondition { get; private set; }

	public DeleteQuery<TModel> Where(Expression<Func<TModel, bool>> expression)
	{
		WhereCondition = expression;
		return this;
	}
}