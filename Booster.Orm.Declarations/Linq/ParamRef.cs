using System;
using Booster.Reflection;

namespace Booster.Orm.Linq;

public class ParamRef<T> : ParamRef
{
	/*public static T operator +(ParamRef<T> param, object value) => default;
	public static T operator -(ParamRef<T> param, object value) => default;
	public static T operator *(ParamRef<T> param, object value) => default;
	public static T operator /(ParamRef<T> param, object value) => default;
	public static bool operator ==(ParamRef<T> param, object value) => true;
	public static bool operator !=(ParamRef<T> param, object value) => true;
	public static T operator +(object value, ParamRef<T> param) => default;
	public static T operator -(object value, ParamRef<T> param) => default;
	public static T operator *(object value, ParamRef<T> param) => default;
	public static T operator /(object value, ParamRef<T> param) => default;
	public static bool operator ==(object value, ParamRef<T> param) => true;
	public static bool operator !=(object value, ParamRef<T> param) => true;*/
		
	public ParamRef()
		=> Type = TypeEx.Get<T>();
		
	public ParamRef(T value)
	{
		Type = TypeEx.Get<T>();
		ValueGetter = _ => value;
	}

	public ParamRef(string name) : base(name)
		=> Type = TypeEx.Get<T>();

	public static implicit operator T(ParamRef<T> param)
		=> default;
}

public class ParamRef
{
	public string Name;
	public TypeEx Type;
	public Func<object, object> ValueGetter;
	public object Expreession; 

	public ParamRef()
	{
	}

	public ParamRef(string name)
        => Name = name;
}