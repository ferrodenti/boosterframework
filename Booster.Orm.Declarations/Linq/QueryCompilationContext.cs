using System;
using System.Collections.Generic;
using System.Text;
using Booster.Orm.Interfaces;

namespace Booster.Orm.Linq;

public class QueryCompilationContext
{
	readonly IOrm _orm;

	public readonly Dictionary<string, ParamRef> Parameters = new(StringComparer.OrdinalIgnoreCase);
	int _anonParamCounter;
	int _anonAliasCounter;

	public QueryCompilationContext(IOrm orm)
		=> _orm = orm;

	public void AddParam(ParamRef paramRef)
	{
		if (paramRef.Name.IsEmpty())
			do
				paramRef.Name = $"p{_anonParamCounter++}";
			while (Parameters.ContainsKey(paramRef.Name));

		Parameters[paramRef.Name] = paramRef;
	}

	public string GetAliasName()
	{
		var result = new StringBuilder();
		var i = _anonAliasCounter++;
		const int letters = 'z' - 'a';

		do
		{
			var c = i % letters;
			result.Append((char) ('a' + c));
			i /= letters;
		} while (i > 0);

		return result.ToString();
	}

	public QueryParameters GetQueryParameters()
		=> new(_orm, Parameters);
}