using System.Linq.Expressions;

namespace Booster.Orm.Linq;

public class Setter
{
	public string Column;
	public Expression Set;
		
	public Setter(string column, Expression set)
	{
		Column = column;
		Set = set;
	}
}