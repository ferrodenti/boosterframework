using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Booster.Orm.Linq;

public class SelectEntityQuery<TModel> : BaseSelect<SelectEntityQuery<TModel>, TModel, object>
{
	public SelectEntityQuery(IEnumerable<Expression<Func<TModel, object>>> selectExpressins) : base(selectExpressins.Cast<Expression>().ToArray())
	{
	}
}