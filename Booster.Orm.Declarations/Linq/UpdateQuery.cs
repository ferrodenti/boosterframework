using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Booster.Orm.Linq.Interfaces;

#nullable enable

namespace Booster.Orm.Linq;

public class UpdateQuery<TModel> : CompiledQuery<UpdateQuery<TModel>, TModel, int>, IUpdateQuery
{
	public List<Setter> Setters { get; } = new();
	public Expression? WhereCondition { get; private set; }

	public UpdateQuery<TModel> Set<TOut>(Expression<Func<TModel, TOut>> member, TOut value)
	{
		Setters.Add(new Setter(GetColumnName(member), Expression.Constant(value)));
		return this;
	}

	public UpdateQuery<TModel> Set<TOut>(Expression<Func<TModel, TOut>> member, object value)
	{
		Setters.Add(new Setter(GetColumnName(member), Expression.Constant(value)));
		return this;
	}


	public UpdateQuery<TModel> Set<TOut>(Expression<Func<TModel, TOut>> member, Expression<Func<TModel, TOut>> value)
	{
		Setters.Add(new Setter(GetColumnName(member), value));
		return this;
	}
		
	public UpdateQuery<TModel> Set<TValue>(string field, IValue<TValue> value)
	{
		Setters.Add(new Setter(field, Expression.Constant(value)));
			
		return this;
	}
		
	public UpdateQuery<TModel> Set<TValue>(string field, Expression<Func<TModel, TValue>> value)
	{
		Setters.Add(new Setter(field, value));
		return this;
	}
		
	public UpdateQuery<TModel> Where(Expression<Func<TModel, bool>> epression)
	{
		WhereCondition = epression;
		return this;
	}
}