using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Booster.Orm.Linq.Interfaces;
using JetBrains.Annotations;

namespace Booster.Orm.Linq;

[PublicAPI]
public class InsertQuery<TModel> : CompiledQuery<InsertQuery<TModel>, TModel, int>, IInsertQuery
{
	public List<Setter> Setters { get; } = new();
	public ISelectQuery FromQuery { get; private set; }
	public string[] OnConflictColumns { get; private set; }
	public InsertConflictUpdateSet InsertConflictUpdateSet { get; private set; }

	public InsertQuery()
	{
	}

	public InsertQuery(IEnumerable<Expression<Func<TModel, object>>> members)
	{
		foreach (var exp in members)
			Setters.Add(new Setter(GetColumnName(exp), null));
	}
		
	public InsertQuery<TModel> Set<TOut>(Expression<Func<TModel, TOut>> member, TOut value)
	{
		Setters.Add(new Setter(GetColumnName(member), Expression.Constant(value)));
		return this;
	}

	public InsertQuery<TModel> Set<TOut>(Expression<Func<TModel, TOut>> member, object value)
	{
		Setters.Add(new Setter(GetColumnName(member), Expression.Constant(value)));
		return this;
	}

	public InsertQuery<TModel> Set<TOut>(Expression<Func<TModel, TOut>> member, Expression<Func<TModel, TOut>> value)
	{
		Setters.Add(new Setter(GetColumnName(member), value));
		return this;
	}
		
	public InsertQuery<TModel> Set<TValue>(string field, IValue<TValue> value)
	{
		Setters.Add(new Setter(field, Expression.Constant(value)));
			
		return this;
	}
		
	public InsertQuery<TModel> Set<TValue>(string field, Expression<Func<TModel, TValue>> value)
	{
		Setters.Add(new Setter(field, value));
		return this;
	}

	public InsertQuery<TModel> From(ISelectQuery selectQuery)
	{
		FromQuery = selectQuery;
		return this;
	}
		
	public InsertQuery<TModel> OnConflict(params Expression<Func<TModel, object>>[] fields)
	{
		OnConflictColumns = fields.Select(e => Repository.TableMapping.GetColumnByName(e.ParseMemberRef().Name)?.EscapedColumnName).ToArray();
		return this;
	}
		
	public InsertQuery<TModel> UpdateSet(Action<InsertConflictUpdateSet<TModel>> action)
	{
		var updateSet = new InsertConflictUpdateSet<TModel>(this);
		action(updateSet);
		InsertConflictUpdateSet = updateSet;
		return this;
	}
		
	public InsertQuery<TModel> OnConflictDoNothing(params Expression<Func<TModel, object>>[] fields)
		=> this;
}