using System;

namespace Booster.Orm.Linq.Kitchen;

[AttributeUsage(AttributeTargets.Method | AttributeTargets.Property)]
public class ExecuteImmediatelyAttribute : Attribute
{
}