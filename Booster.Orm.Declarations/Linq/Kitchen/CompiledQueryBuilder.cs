using System.Text;
using Booster.Reflection;

namespace Booster.Orm.Linq.Kitchen;

public class CompiledQueryBuilder
{
	readonly StringBuilder _stringBuilder = new();
	public TypeEx Type;
	public int Priority;
	//public bool RightToLeft;
	public bool HasBrackets;

	public void Append(object value)
		=> _stringBuilder.Append(value);
		
	public void AppendFormat(string format, params object[] args)
		=> _stringBuilder.AppendFormat(format, args);
		
	public string GetSql(int priority = int.MaxValue)
	{
		if (priority < Priority)
			return $"({_stringBuilder})";
			
		return _stringBuilder.ToString();
	}

	public override string ToString()
		=> $"{Type} {GetSql()}";
}