using System;

namespace Booster.Orm.Linq.Kitchen;

[AttributeUsage(AttributeTargets.Method | AttributeTargets.Property)]
public class OperatorAttribute : Attribute
{
	public readonly string Name;

	public OperatorAttribute(string name)
        => Name = name;
}