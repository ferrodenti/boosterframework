using System;
using System.Linq.Expressions;
using Booster.Orm.Linq.Interfaces;

namespace Booster.Orm.Linq;

public class SelectScalarQuery<TModel, TResult> : BaseSelect<SelectScalarQuery<TModel, TResult>, TModel, TResult>, IValue<TResult>
{
	public SelectScalarQuery(Expression<Func<TModel, TResult>> expression) : base(new Expression[]{expression})
	{
	}
}