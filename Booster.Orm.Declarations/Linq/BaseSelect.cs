using System;
using System.Linq;
using System.Linq.Expressions;
using Booster.Orm.Linq.Interfaces;

namespace Booster.Orm.Linq;

public class BaseSelect<TThis, TModel, TResult> : CompiledQuery<TThis, TModel, TResult>, ISelectQuery where TThis : class
{
	public Expression[] Columns { get; }
	public Expression WhereCondition { get; private set; }
	public ISelectQuery FromQuery { get; protected set; }
	public string FromQueryAs { get; protected set; }
	public Expression[] Groups { get; protected set; }
	public Expression[] Orders { get; protected set; }
		
	public BaseSelect(Expression[] selectExpressins)
		=> Columns = selectExpressins;

	public TThis Where(Expression<Func<TModel, bool>> expression)
	{
		WhereCondition = expression;
		return this as TThis;
	}

	public TThis GroupBy(params Expression<Func<TModel, object>>[] columns)
	{
		Groups = columns.Cast<Expression>().ToArray();
		return this as TThis;
	}
		
	public TThis OrderBy(params Expression<Func<TModel, object>>[] sorts)
	{
		Orders = sorts.Cast<Expression>().ToArray();
		return this as TThis;
	}

}