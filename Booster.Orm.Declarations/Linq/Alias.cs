using System;
using System.Linq.Expressions;
using Booster.Orm.Linq.Interfaces;

namespace Booster.Orm.Linq;

public class Alias<T> : IAlias
{
	string _name;

	public object this[Expression<Func<T, object>> mem] => null;
	public TResult Get<TResult>(Expression<Func<T, TResult>> mem) => default;

	public Alias(string name = null)
		=> _name = name;

	public string GetName(QueryCompilationContext context)
		=> _name ??= context.GetAliasName();
}