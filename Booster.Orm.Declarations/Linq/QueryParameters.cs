using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using Booster.Helpers;
using Booster.Orm.Interfaces;

namespace Booster.Orm.Linq;

public class QueryParameters
{
	readonly IOrm _orm;

	public readonly Dictionary<string, ParamRef> Parameters;


	public QueryParameters(IOrm orm, Dictionary<string, ParamRef> parameters)
	{
		_orm = orm;
		Parameters = parameters;
	}

	public DbParameter[] ToDbParameters(object target)
		=> Parameters.Values.Select(p =>
		{
			if (p.ValueGetter == null)
				return null;
				
			var val = p.ValueGetter(target);
			return _orm.CreateParameter(p.Name, val, p.Type);
		}).Where(p => p != null).ToArray();

	public DbParameter[] Merge(IEnumerable<DbParameter> first, object second)
	{
		if (second == null)
			return first.AsArray();

		var dict = first.ToDictionary(p => p.ParameterName, StringComparer.OrdinalIgnoreCase);
		if(second is IEnumerable<DbParameter> ie)
			foreach (var p in ie)
				dict[GetName(p.ParameterName)] = p;
		else
			foreach (var pair in AnonTypeHelper.ReadProperties(second))
			{
				var name = GetName(pair.Key);
				dict[name] = _orm.CreateParameter(name, pair.Value);
			}

		return dict.Values.ToArray();
	}

	string GetName(string name)
	{
		if (Parameters.TryGetValue(name, out var pr))
			return pr.Name;

		return name;
	}

		
}