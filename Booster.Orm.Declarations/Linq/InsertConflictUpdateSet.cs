using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Booster.Orm.Linq.Interfaces;

namespace Booster.Orm.Linq;

public class InsertConflictUpdateSet
{
	public List<Setter> Setters { get; } = new();

}
public class InsertConflictUpdateSet<TModel> : InsertConflictUpdateSet //: CompiledQuery<TModel, object>
{
	readonly InsertQuery<TModel> _owner;

	public InsertConflictUpdateSet(InsertQuery<TModel> owner)
		=> _owner = owner;

	public InsertConflictUpdateSet<TModel> Set<TOut>(Expression<Func<TModel, TOut>> member, TOut value)
	{
		Setters.Add(new Setter(_owner.GetColumnName(member), Expression.Constant(value)));
		return this;
	}

	public InsertConflictUpdateSet<TModel> Set<TOut>(Expression<Func<TModel, TOut>> member, object value)
	{
		Setters.Add(new Setter(_owner.GetColumnName(member), Expression.Constant(value)));
		return this;
	}

	public InsertConflictUpdateSet<TModel> Set<TOut>(Expression<Func<TModel, TOut>> member, Expression<Func<TModel, TOut>> value)
	{
		Setters.Add(new Setter(_owner.GetColumnName(member), value));
		return this;
	}
		
	public InsertConflictUpdateSet<TModel> Set<TValue>(string field, IValue<TValue> value)
	{
		Setters.Add(new Setter(field, Expression.Constant(value)));
			
		return this;
	}
		
	public InsertConflictUpdateSet<TModel> Set<TValue>(string field, Expression<Func<TModel, TValue>> value)
	{
		Setters.Add(new Setter(field, value));
		return this;
	}
}