using System.Collections.Generic;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using Booster.Log;
using Booster.Orm.Interfaces;
using JetBrains.Annotations;

#if !NETSTANDARD2_1
using Booster.AsyncLinq;
#endif

namespace Booster.Orm.Linq;

[PublicAPI]
public class PreparedQuery
{
	readonly CompiledQuery _query;
	readonly object _target;

	readonly SyncLazy<DbParameter[]> _parameters = SyncLazy<DbParameter[]>.Create<PreparedQuery>(self => self._query.GetDbParameters(self._target));

	public string Sql => _query.Sql;
	public DbParameter[] Parameters
	{
		get => _parameters.GetValue(this);
		set => _parameters.SetValue(value);
	}

	public PreparedQuery(CompiledQuery query, object target)
	{
		_query = query;
		_target = target;
	}

	DbParameter[] MergeParameters(object param)
		=> _query.MergeDbParameters(Parameters, param);

	public PreparedQuery SetParameters(object param)
	{
		Parameters = MergeParameters(param);
		return this;
	}

	#region ExecuteNoQuery

	public int ExecuteNoQuery(object param = null)
	{
		using var conn = _query.Orm.GetCurrentConnection();
		return ExecuteNoQuery(conn, param);
	}
		
	public int ExecuteNoQuery(IConnection connection, object param = null)
		=> connection.ExecuteNoQuery(Sql, MergeParameters(param));

	public async Task<int> ExecuteNoQueryAsync(object param = null, CancellationToken token = default)
	{
		using var conn = _query.Orm.GetCurrentConnection();
		return await ExecuteNoQueryAsync(conn, param, token).NoCtx();
	}
		
	public Task<int> ExecuteNoQueryAsync(IConnection connection, object param = null, CancellationToken token = default)
		=> connection.ExecuteNoQueryAsync(Sql, MergeParameters(param), token);

	#endregion

	#region ExecuteScalar

	public T ExecuteScalar<T>(object param = null, T @default = default)
	{
		using var conn = _query.Orm.GetCurrentConnection();
		return ExecuteScalar(conn, param, @default);
	}
		
	public T ExecuteScalar<T>(IConnection connection, object param = null, T @default = default)
		=> connection.ExecuteScalar(Sql, MergeParameters(param), @default);

	public async Task<T> ExecuteScalarAsync<T>(object param = null, T @default = default, CancellationToken token = default)
	{
		using var conn = _query.Orm.GetCurrentConnection();
		return await ExecuteScalarAsync(conn, param, @default, token).NoCtx();
	}
		
	public Task<T> ExecuteScalarAsync<T>(IConnection connection, object param = null, T @default = default, CancellationToken token = default)
		=> connection.ExecuteScalarAsync(Sql, MergeParameters(param), @default, token);

	#endregion

	#region ExecuteReader

	public DbDataReader ExecuteReader(object param = null)
	{
		using var conn = _query.Orm.GetCurrentConnection();
		return ExecuteReader(conn, param);
	}
		
	public DbDataReader ExecuteReader(IConnection connection, object param = null)
		=> connection.ExecuteReader(Sql, MergeParameters(param));

	public async Task<DbDataReader> ExecuteReaderAsync(object param = null, CancellationToken token = default)
	{
		using var conn = _query.Orm.GetCurrentConnection();
		return await ExecuteReaderAsync(conn, param, token).NoCtx();
	}
		
	public Task<DbDataReader> ExecuteReaderAsync(IConnection connection, object param = null, CancellationToken token = default)
		=> connection.ExecuteReaderAsync(Sql, MergeParameters(param), token);

	#endregion

	#region ExecuteTuples

	public IEnumerable<T> ExecuteTuples<T>(object param = null)
	{
		using var conn = _query.Orm.GetCurrentConnection();
		return ExecuteTuples<T>(conn, param);
	}
		
	public IEnumerable<T> ExecuteTuples<T>(IConnection connection, object param = null)
		=> connection.ExecuteTuples<T>(Sql, MergeParameters(param));

	public IAsyncEnumerable<T> ExecuteTuplesAsync<T>(object param = null, CancellationToken token = default)
	{
		using var conn = _query.Orm.GetCurrentConnection();
		return ExecuteTuplesAsync<T>(conn, param, token);
	}
		
	public IAsyncEnumerable<T> ExecuteTuplesAsync<T>(IConnection connection, object param = null, CancellationToken token = default)
		=> connection.ExecuteTuplesAsync<T>(Sql, MergeParameters(param), token);

	#endregion


	#region ExecuteEnumerable

	public IEnumerable<T> ExecuteEnumerable<T>(object param = null)
	{
		using var conn = _query.Orm.GetCurrentConnection();
		return ExecuteEnumerable<T>(conn, param);
	}
		
	public IEnumerable<T> ExecuteEnumerable<T>(IConnection connection, object param = null)
		=> connection.ExecuteEnumerable<T>(Sql, MergeParameters(param));

	public IAsyncEnumerable<T> ExecuteEnumerableAsync<T>(object param = null, CancellationToken token = default)
	{
		using var conn = _query.Orm.GetCurrentConnection();
		return ExecuteEnumerableAsync<T>(conn, param, token);
	}
		
	public IAsyncEnumerable<T> ExecuteEnumerableAsync<T>(IConnection connection, object param = null, CancellationToken token = default)
		=> connection.ExecuteEnumerableAsync<T>(Sql, MergeParameters(param), token);

	#endregion
		
	public PreparedQuery Log(ILog log, object param = null)
	{
		log.Debug($"SQL: {Sql}{MergeParameters(param).ToString(p => $"\t{p.DbType} {p.ParameterName}={p.Value}", new EnumBuilder("\n", "\nPARAMS:\n"))}");
		return this;
	}

	public override string ToString()
		=> string.Concat(Sql, MergeParameters(null).ToString(p => $"\t{p.DbType} {p.ParameterName}={p.Value}", new EnumBuilder("\n", "\nPARAMS:\n")));
}
