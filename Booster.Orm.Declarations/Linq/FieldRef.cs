using Booster.Reflection;

namespace Booster.Orm.Linq;

public class FieldRef<T> : FieldRef
{
	public FieldRef(string name) : base(name)
		=> Type = TypeEx.Get<T>();
		
	public static implicit operator T(FieldRef<T> param)
		=> default;

	/*public static T operator +(FieldRef<T> param, object value) => default;
	public static T operator -(FieldRef<T> param, object value) => default;
	public static T operator *(FieldRef<T> param, object value) => default;
	public static T operator /(FieldRef<T> param, object value) => default;
	public static bool operator ==(FieldRef<T> param, object value) => true;
	public static bool operator !=(FieldRef<T> param, object value) => true;
	public static T operator +(object value, FieldRef<T> param) => default;
	public static T operator -(object value, FieldRef<T> param) => default;
	public static T operator *(object value, FieldRef<T> param) => default;
	public static T operator /(object value, FieldRef<T> param) => default;
	public static bool operator ==(object value, FieldRef<T> param) => true;
	public static bool operator !=(object value, FieldRef<T> param) => true;*/
}
	
public class FieldRef
{
	public readonly string Name;
	public TypeEx Type;

	public FieldRef(string name)
        => Name = name;
}