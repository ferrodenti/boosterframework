using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Booster.Orm.Linq.Interfaces;

namespace Booster.Orm.Linq;

public class SelectMemQuery : BaseSelect<SelectMemQuery, MemoryTable, MemoryTable>
{
	public SelectMemQuery From(string name, ISelectQuery query)
	{
		FromQuery = query;
		FromQueryAs = name;
		return this;
	}
		
	public SelectMemQuery From(ISelectQuery query)
	{
		FromQuery = query;
		return this;
	}

	public SelectMemQuery(IEnumerable<Expression<Func<object>>> columns) : base(columns.Cast<Expression>().ToArray())
	{
	}
}