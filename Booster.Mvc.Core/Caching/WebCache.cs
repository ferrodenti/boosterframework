﻿
using Booster.Caching;
using Booster.Caching.Interfaces;
using Booster.Caching.Kitchen;
using Booster.DI;
using Booster.Interfaces;
#if NETSTANDARD || NETCOREAPP
using System.Threading;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Primitives;
using Booster.Mvc.Helpers;
#else
using System.Collections;
using System.Web;
using System.Web.Caching;
#endif

[assembly: AssemblyInstancesRegistration]

#nullable enable

namespace Booster.Mvc.Caching;

[InstanceFactory(typeof(ICacheWrapper), true)]
public class WebCache : BaseCacheWrapper
{
	public override ILazyDependency CreateLazyDependency(ICacheLazy target)
		=> new WebCacheDependency(target);

#if NETSTANDARD || NETCOREAPP
	CancellationTokenSource _clearAll = new();
	IMemoryCache? _memoryCache;
	protected IMemoryCache Cache => _memoryCache ??= MvcHelper.GetService<IMemoryCache>()!;

	public override CacheEntry? GetInt(string key)
	{
		Cache.TryGetValue(key, out var obj);
		return obj as CacheEntry;
	}

	protected override void SetInt(string key, CacheEntry entry, CacheSettings settings, CacheLazyPriority priority)
	{
		var options = new MemoryCacheEntryOptions();
		switch (priority)
		{
		case CacheLazyPriority.Low:
		case CacheLazyPriority.BelowNormal:
			options.SetPriority(CacheItemPriority.Low);
			break;
		case CacheLazyPriority.AboveNormal:
		case CacheLazyPriority.High:
			options.SetPriority(CacheItemPriority.High);
			break;
		case CacheLazyPriority.NotRemovable:
			options.SetPriority(CacheItemPriority.NeverRemove);
			break;
		default:
			options.SetPriority(CacheItemPriority.Normal);
			break;
		}
		
		if(settings.AbsoluteExpiration.IsBetweenZeroAndMax())
			options.SetAbsoluteExpiration(settings.AbsoluteExpiration);
				
		if(settings.SlidingExpiration.IsBetweenZeroAndMax())
			options.SetSlidingExpiration(settings.SlidingExpiration);
				
		if(entry.Dependency is WebCacheDependency webCacheDependency)
			options.AddExpirationToken(new CancellationChangeToken(webCacheDependency.CancellationToken));
			
		options.AddExpirationToken(new CancellationChangeToken(_clearAll.Token));
		
		Cache.Set(key, entry, options);
	}

	public override void ClearAll()
	{
		try
		{
			_clearAll.Cancel();
			_clearAll.Dispose();
		}
		catch { /* ignore */ }

		_clearAll = new CancellationTokenSource();
	}

	public override void Clear(string key)
		=> Cache.Remove(key);
#else

	public override CacheEntry? GetInt(string key)
		=> HttpRuntime.Cache.Get(key) as CacheEntry;

	protected override void SetInt(string key, CacheEntry entry, CacheSettings settings, CacheLazyPriority priority)
	{
		var dep = entry.Dependency as WebCacheDependency;
		HttpRuntime.Cache.Insert(key, entry, dep?.RuntimeDependency, settings.GetAbsoluteExpirationFromNow(), settings.SlidingExpiration, (CacheItemPriority) (int) priority, null);
	}

	public override void ClearAll()
	{
		var cache = HttpRuntime.Cache;
		foreach (DictionaryEntry elem in cache)
			cache.Remove(elem.Key.ToString());
	}

	public override void Clear(string key)
		=> HttpRuntime.Cache.Remove(key);
#endif
}