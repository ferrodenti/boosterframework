﻿using Booster.Interfaces;
using Booster.Kitchen;
#if NETSTANDARD || NETCOREAPP
using System.Threading;
#else
using System;
using System.Web.Caching;
#endif

namespace Booster.Mvc.Caching;

class WebCacheDependency : BasicLazyDependency
{
#if NETSTANDARD || NETCOREAPP
	readonly CancellationTokenSource _cancellation = new();

	public CancellationToken CancellationToken => _cancellation.Token;

	protected override void OnChanged()
	{
		try
		{
			_cancellation.Cancel();
		}
		catch
		{ /* ignore */
		}

		base.OnChanged();
	}
#else
	public class DependencyImpl : CacheDependency
	{
		public void NotifyDependencyChanged() 
			=> NotifyDependencyChanged(this, EventArgs.Empty);
	}
		 
	public DependencyImpl RuntimeDependency = new();
	
	protected override void OnChanged()
	{
		RuntimeDependency.NotifyDependencyChanged();
		base.OnChanged();
	}
#endif

	public WebCacheDependency(ILazy target) : base(target)
	{
	}
}