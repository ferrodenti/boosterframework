#if NETCOREAPP || NETSTANDARD
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Mvc;
using Booster.CodeGen;
using Booster.DI;
using Booster.Log;
using Booster.Reflection;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Razor.Language;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

#nullable enable

namespace Booster.Mvc;

public class DynamicView
{
	TypeEx _inherits = TypeEx.Get<DynamicViewTemplateBase>();
	public TypeEx Inherits
	{
		get => _inherits;
		set
		{
			if (Equals(_inherits, value))
				return;
			
			_inherits = value;
			_sourceCode.Reset();
			_referencedAssemblies.Reset();
			_templateInstance.Reset();
		}
	}

	TypeEx? _modelType ;
	public TypeEx? ModelType
	{
		get => _modelType;
		set
		{
			if (Equals(_modelType, value))
				return;
			
			_modelType = value;
			_sourceCode.Reset();
			_referencedAssemblies.Reset();
			_templateInstance.Reset();
		}
	}

	readonly SyncLazy<ObservableCollection<TypeEx>> _referencedTypes = SyncLazy<ObservableCollection<TypeEx>>.Create<DynamicView>(self =>
	{
		var result = new ObservableCollection<TypeEx>();

		result.CollectionChanged += (_, _) =>
		{
			self._referencedAssemblies.Reset();
			self._templateInstance.Reset();
		};

		void Add(TypeEx type)
		{
			if(!result.Contains(type))
				result.Add(type);
		}

		Add(TypeEx.Get<object>());
		Add(self.Inherits);
		Add(TypeEx.Get<DynamicObject>());

		if (self.ModelType != null)
			Add(self.ModelType);

		return result;
	});

	public ObservableCollection<TypeEx> ReferencedTypes => _referencedTypes.GetValue(this);

	readonly SyncLazy<ObservableCollection<Assembly>> _referencedAssemblies = SyncLazy<ObservableCollection<Assembly>>.Create<DynamicView>(self =>
	{
		var result = new ObservableCollection<Assembly>();

		result.CollectionChanged += (_, _) =>
		{
			self._templateInstance.Reset();
		};

		void Add(Assembly type)
		{
			if(!result.Contains(type))
				result.Add(type);
		}

		Add(Assembly.Load(new AssemblyName("Microsoft.CSharp")));
		Add(Assembly.Load(new AssemblyName("netstandard")));
		Add(Assembly.Load(new AssemblyName("System.Runtime")));

		foreach (var type in self.ReferencedTypes) 
			Add(type.Assembly);

		return result;
	});

	public ObservableCollection<Assembly> ReferencedAssemblies => _referencedAssemblies.GetValue(this);

	readonly SyncLazy<ObservableCollection<string>> _usings = SyncLazy<ObservableCollection<string>>.Create<DynamicView>(self =>
	{
		var result = new ObservableCollection<string>();

		result.CollectionChanged += (_, _) =>
		{
			self._sourceCode.Reset();
			self._templateInstance.Reset();
		};

		return result;
	});

	public ObservableCollection<string> Usings => _usings.GetValue(this);

	string _cshtml;
	public string Cshtml
	{
		get => _cshtml;
		set
		{
			if (_cshtml == value)
				return;
			
			_cshtml = value;
			_sourceCode.Reset();
			_templateInstance.Reset();
		}
	}

	readonly SyncLazy<string> _name = new(Path.GetRandomFileName);

	public string Name
	{
		get => _name.Value;
		set
		{
			if (_name.Value == value)
				return;
			
			_name.Value = value;
			_sourceCode.Reset();
			_templateInstance.Reset();
		}
	}

	string _namespace = "Booster.Mvc.DynamicViews";
	public string Namespace
	{
		get => _namespace;
		set
		{
			if (_namespace == value)
				return;
			
			_namespace = value;
			_sourceCode.Reset();
			_templateInstance.Reset();
		}
	}
	

	readonly SyncLazy<string> _sourceCode = SyncLazy<string>.Create<DynamicView>(self 
		=> GenerateViewSource(self.Name, self.Inherits, self.Namespace, self.Usings, self.Cshtml));

	public string SourceCode
	{
		get => _sourceCode.GetValue(this);
		set
		{
			if (_sourceCode.GetValue(this) == value)
				return;
			
			_sourceCode.Value = value;
			_templateInstance.Reset();
		}
	}

	readonly SyncLazy<Type> _templateInstance = SyncLazy<Type>.Create<DynamicView>(self
		=> CreateTemplateType(self.Name, self.Namespace, self.SourceCode, self.ReferencedAssemblies));

	public Type TemplateType
	{
		get => _templateInstance.GetValue(this);
		set => _templateInstance.Value = value;
	}

	[PublicAPI]
	public DynamicView(string cshtml)
		=> _cshtml = cshtml;

	[PublicAPI]
	public DynamicView(string name, string cshtml)
	{
		Name = name;
		_cshtml = cshtml;
	}

	public DynamicView Reference(TypeEx? type)
	{
		if (type != null)
			ReferencedTypes.Add(type);

		return this;
	}

	public DynamicView Reference<T>()
		=> Reference(TypeEx.Get<T>());

	public DynamicView Reference(object? obj)
		=> Reference(obj.GetTypeEx());

	public DynamicView Reference(Assembly? assembly)
	{
		if (assembly != null)
			ReferencedAssemblies.Add(assembly);

		return this;
	}

	public DynamicView Using(string? usng)
	{
		if (usng != null)
			Usings.Add(usng);

		return this;
	}

	protected static string GenerateViewSource(string name, TypeEx inherits, string? ns, IEnumerable<string> usings, string cshtml)
	{
		var engine = RazorProjectEngine.Create(
			RazorConfiguration.Default,
			RazorProjectFileSystem.Create(@"."),
			builder =>
			{
				if (ns.IsSome())
					builder.SetNamespace(ns);
			});


		var template = new EnumBuilder("\n\r");
		template.Append($"@inherits {inherits.FullName}");

		foreach (var usg in usings)
			template.Append($"@using {usg}");

		template.Append(cshtml);

		var document = RazorSourceDocument.Create(template, name);
		var codeDocument = engine.Process(
			document,
			null,
			new List<RazorSourceDocument>(),
			new List<TagHelperDescriptor>());

		var razorCSharpDocument = codeDocument.GetCSharpDocument();

		return razorCSharpDocument.GeneratedCode;
	}

	protected static MemoryStream Compile(string name, string code, IEnumerable<Assembly> assemblies)
	{
		var syntaxTree = CSharpSyntaxTree.ParseText(code);

		var references = assemblies.Select(assy => MetadataReference.CreateFromFile(assy.Location)).ToArray();

		var options = new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary);
		var compilation = CSharpCompilation.Create(
			name,
			new[] { syntaxTree },
			references,
			options);

		var memoryStream = new MemoryStream();

		var emitResult = compilation.Emit(memoryStream);

		emitResult.ThrowIfErrors(code, InstanceFactory.Get<ILog>(false));

		memoryStream.Position = 0;

		return memoryStream;
	}

	protected static Type CreateTemplateType(string name, string ns, string code, IEnumerable<Assembly> assemblies)
	{
		var stream = Compile(name, code, assemblies);
		var assembly = Assembly.Load(stream.ToArray()); 
		return assembly.GetType($"{ns}.Template")!;

		//var result = Activator.CreateInstance(templateType)!;

		//return (IDynamicViewTemplate)result;
	}
	public async Task<MvcHtmlString> Render(object? model = null)
	{
		var template = (IDynamicViewTemplate)Activator.CreateInstance(TemplateType)!;
		var result = await template.RenderAsync(model).NoCtx();

		return result;
	}
}
#endif