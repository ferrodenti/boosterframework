using System.Threading.Tasks;
using System.Web.Mvc;

#nullable enable

namespace Booster.Mvc;

public interface IDynamicViewTemplate
{
	Task<MvcHtmlString> RenderAsync(dynamic? model);
}