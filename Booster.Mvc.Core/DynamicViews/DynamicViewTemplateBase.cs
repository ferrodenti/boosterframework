using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

#nullable enable

namespace Booster.Mvc;

public abstract class DynamicViewTemplateBase : IDynamicViewTemplate
{
	readonly StringBuilder _result = new();

	public dynamic? Model { get; set; }

	public async Task<MvcHtmlString> RenderAsync(dynamic? model)
	{
		Model = model;
		await ExecuteAsync();
		var result = _result.ToString();
		return new MvcHtmlString(result);
	}

	public abstract Task ExecuteAsync();

	public void WriteLiteral(string literal)
		=> _result.Append(literal);

	public void Write(object obj)
		=> _result.Append(obj);
}