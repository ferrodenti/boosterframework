﻿#if !NETSTANDARD && !NETCOREAPP
using System.Web;
using System.Web.Routing;

namespace Booster.Mvc;

public class SessionRouteHandler : IRouteHandler
{
	IHttpHandler IRouteHandler.GetHttpHandler(RequestContext requestContext)
		=> new SessionControllerHandler(requestContext.RouteData);
}

#endif
