﻿using System;
using System.Net;
using System.Net.Http;
using JetBrains.Annotations;

#if NETSTANDARD || NETCOREAPP
#else
using System.Web.Http;

#endif


namespace Booster.Mvc;

/// <summary>
/// Класс-исключение для прерывания выполнения запроса и возврата стандартных http ошибок клиенту.
/// Может быть перехваченым фильтром <see cref="T:Booster.Mvc.HandleHttpStatusAttribute"/> для форматирования ответа клиенту.
/// Для создания экземпляров класса рекомендуется использование статических методов
/// </summary>
/// <example><code>throw HttpError.NotFound("Страница не найдена")</code></example>
[Obsolete("Используй HttpStatusHelper")]
[PublicAPI]
#if NETSTANDARD || NETCOREAPP
public class HttpError : Exception
{
	public HttpResponseMessage Response { get; }

	HttpError(HttpResponseMessage response)
		=> Response = response;
#else
	public class HttpError : HttpResponseException
	{
		HttpError(HttpResponseMessage response) : base (response) { }
#endif
	/// <summary>
	/// Стандартный код состояния HTTP ответа
	/// </summary>
	public HttpStatusCode StatusCode
	{
		get => Response.StatusCode;
		set => Response.StatusCode = value;
	}

	/// <summary>
	/// Фраза описывающая причину данного состояния HTTP ответа
	/// </summary>
	public string ReasonPhrase
	{
		get => Response.ReasonPhrase;
		set => Response.ReasonPhrase = value;
	}

	/// <summary>
	/// Содержимое ответа
	/// </summary>
	public HttpContent Content
	{
		get => Response.Content;
		set => Response.Content = value;
	}

	/// <summary>
	/// Содержимое ответа в формате строки
	/// </summary>
	public string StringContent
	{
		get => (Content as StringContent).With(s => s.ToString());
		set => Content = new StringContent(value);
	}

	public HttpError(HttpStatusCode code)
		: this(new HttpResponseMessage(code))
	{
	}

	public HttpError(HttpStatusCode code, string reason)
		: this(
			new HttpResponseMessage(code)
			{
				ReasonPhrase = reason
			})
	{
	}

	public HttpError(int code)
		: this(new HttpResponseMessage((HttpStatusCode) code))
	{
	}

	public HttpError(int code, string reason)
		: this(
			new HttpResponseMessage((HttpStatusCode) code)
			{
				ReasonPhrase = reason
			})
	{
	}

#region common errors & answers

	/// <summary>
	/// Сервер обнаружил в запросе клиента синтаксическую ошибку
	/// </summary>
	/// <param name="reason">Сопутствующий текст ошибки</param>
	/// <returns></returns>
	public static HttpError BadRequest(string reason = null)
		=> new(400, reason);

	/// <summary>
	/// Для доступа к запрашиваемому ресурсу требуется аутентификация. В заголовке ответ должен содержать поле WWW-Authenticate с перечнем условий аутентификации. Клиент может повторить запрос, включив в заголовок сообщения поле Authorization с требуемыми для аутентификации данными.
	/// </summary>
	/// <param name="reason">Сопутствующий текст ошибки</param>
	/// <returns></returns>
	public static HttpError Unauthorized(string reason = null)
		=> new(401, reason);

	/// <summary>
	/// Сервер понял запрос, но он отказывается его выполнять из-за ограничений в доступе для клиента к указанному ресурсу.
	/// </summary>
	/// <param name="reason">Сопутствующий текст ошибки</param>
	/// <returns></returns>
	public static HttpError Forbidden(string reason = null)
		=> new(403, reason);

	/// <summary>
	/// Сервер понял запрос, но не нашёл соответствующего ресурса по указанному URI. Если серверу известно, что по этому адресу был документ, то ему желательно использовать код 410. Ответ 404 может использоваться вместо 403, если требуется тщательно скрыть от посторонних глаз определённые ресурсы.
	/// </summary>
	/// <param name="reason">Сопутствующий текст ошибки</param>
	/// <returns></returns>
	public static HttpError NotFound(string reason = null)
		=> new(404, reason);

	/// <summary>
	/// Указанный клиентом метод нельзя применить к текущему ресурсу. В ответе сервер должен указать доступные методы в заголовке Allow, разделив их запятой. Эту ошибку сервер должен возвращать, если метод ему известен, но он не применим именно к указанному в запросе ресурсу, если же указанный метод не применим на всём сервере, то клиенту нужно вернуть код 501 (Not Implemented)
	/// </summary>
	/// <param name="reason">Сопутствующий текст ошибки</param>
	/// <returns></returns>
	public static HttpError MethodNotAllowed(string reason = null)
		=> new(405, reason);

	/// <summary>
	/// Запрошенный URI не может удовлетворить переданным в заголовке характеристикам. Если метод был не HEAD, то сервер должен вернуть список допустимых характеристик для данного ресурса.
	/// </summary>
	/// <param name="reason">Сопутствующий текст ошибки</param>
	/// <returns></returns>
	public static HttpError NotAcceptable(string reason = null)
		=> new(406, reason);

	/// <summary>
	/// Время ожидания сервером передачи от клиента истекло. Клиент может повторить аналогичный предыдущему запрос в любое время. Например, такая ситуация может возникнуть при загрузке на сервер объёмного файла методом POST или PUT. В какой-то момент передачи источник данных перестал отвечать, например, из-за повреждения компакт-диска или потери связи с другим компьютером в локальной сети. Пока клиент ничего не передаёт, ожидая от него ответа, соединение с сервером держится. Через некоторое время сервер может закрыть соединение со своей стороны, чтобы дать возможность другим клиентам сделать запрос. Этот ответ не возвращается, когда клиент принудительно остановил передачу по команде пользователя или соединение прервалось по каким-то иным причинам, так как ответ уже послать невозможно.
	/// </summary>
	/// <param name="reason">Сопутствующий текст ошибки</param>
	/// <returns></returns>
	public static HttpError RequestTimeout(string reason = null)
		=> new(408, reason);

	/// <summary>
	/// Запрос не может быть выполнен из-за конфликтного обращения к ресурсу. Такое возможно, например, когда два клиента пытаются изменить ресурс с помощью метода PUT.
	/// </summary>
	/// <param name="reason">Сопутствующий текст ошибки</param>
	/// <returns></returns>
	public static HttpError Conflict(string reason = null)
		=> new(409, reason);

	/// <summary>
	/// Такой ответ сервер посылает, если ресурс раньше был по указанному URL, но был удалён и теперь недоступен. Серверу в этом случае неизвестно и местоположение альтернативного документа, например, копии). Если у сервера есть подозрение, что документ в ближайшее время может быть восстановлен, то лучше клиенту передать код 404.
	/// </summary>
	/// <param name="reason">Сопутствующий текст ошибки</param>
	/// <returns></returns>
	public static HttpError Gone(string reason = null)
		=> new(410, reason);

	/// <summary>
	/// Любая внутренняя ошибка сервера, которая не входит в рамки остальных ошибок класса.
	/// </summary>
	/// <param name="reason">Сопутствующий текст ошибки</param>
	/// <returns></returns>
	public static HttpError InternalServerError(string reason = null)
		=> new(500, reason);

	/// <summary>
	/// Сервер не поддерживает возможностей, необходимых для обработки запроса. Типичный ответ для случаев, когда сервер не понимает указанный в запросе метод. Если же метод серверу известен, но он не применим к данному ресурсу, то нужно вернуть ответ 405
	/// </summary>
	/// <param name="reason">Сопутствующий текст ошибки</param>
	/// <returns></returns>
	public static HttpError NotImplemented(string reason = null)
		=> new(501, reason);

	#endregion

	/// <summary>
	/// Отдает описание состояния HTTP ответа на русском по коду состояния ответа.
	/// </summary>
	/// <param name="code">Код состояния</param>
	/// <returns>Текстовое описание кода на русском</returns>
	public static string DescribeCode(int code)
		=> code switch
		{
			200 => "OK",
			204 => "Нет содержания",
			301 => "Перемещено постоянно",
			303 => "Перемещено временно",
			304 => "Не изменилось",
			308 => "Перемещено постоянно",
			400 => "Плохой запрос",
			401 => "Требуется авторизация",
			402 => "Необходим платеж",
			403 => "Доступ закрыт",
			404 => "Страница не найдена",
			405 => "Метод не допустим",
			406 => "Недопустимо",
			408 => "Время ожидания истекло",
			409 => "Конфликт",
			410 => "Объект исчез",
			500 => "Внутренняя ошибка сервера",
			501 => "Не реализованно",
			502 => "Плохой шлюз",
			504 => "Время ожидания шлюза истекло",
			507 => "Недостаточно ресурсов для сохранения",
			444 => "Пустой ответ",
			_   => ""
		};
}