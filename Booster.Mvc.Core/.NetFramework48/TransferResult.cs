#if !NETSTANDARD && !NETCOREAPP
using System;
using System.Web;
using System.Web.Mvc;

namespace Booster.Mvc;

public class TransferResult : ActionResult //TODO: TransferResult std implementation
{
	public string Url { get; }

	public TransferResult(string url)
		=> Url = url;

	public override void ExecuteResult(ControllerContext context)
	{
		if (context == null)
			throw new ArgumentNullException(nameof(context));

		var httpContext = HttpContext.Current;

		HttpContext.Current.Items.Remove("RequestUrl");

		if (HttpRuntime.UsingIntegratedPipeline)
			httpContext.Server.TransferRequest(Url, true);
		else
		{
			httpContext.RewritePath(Url, false);

			IHttpHandler httpHandler = new MvcHttpHandler();
			httpHandler.ProcessRequest(httpContext);
		}
	}
}
#endif