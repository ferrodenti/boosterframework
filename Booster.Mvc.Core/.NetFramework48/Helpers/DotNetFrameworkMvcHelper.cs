#if !NETCOREAPP && !NETSTANDARD // Т.к. проект общий нельзя просто так взять и выключить файлы. 

using System;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Routing;
using Booster.DI;
using Booster.FileSystem;
using Booster.Helpers;

namespace Booster.Mvc.Helpers;

public class DotNetFrameworkMvcHelper : DependencyResolver, IMvcHelperImpl
{
#if STORE_HTTP_CONTEXT_IN_CTX_LOCAL
	static readonly CtxLocal<HttpContext> _httpContext = new(() => HttpContext.Current, true);

	public HttpContext GetHttpContext()
		=> HttpContext.Current ?? _httpContext.Value;

	public void RememberHttpContext() 
		=> _httpContext.Value = HttpContext.Current;
#else
	public HttpContext GetHttpContext()
		=> HttpContext.Current;

	public void RememberHttpContext()
	{
	}
#endif

	public IObjectSession Session => GetHttpContext()?.Session.With(ses => new DotNetFrameworkObjectSession(ses));
	public string DisplayUri => GetHttpContext()?.Request.RawUrl;
	public Url RequestUrl => GetHttpContext()?.Request.RawUrl;

	FilePath _hostingRoot;
	protected FilePath HostingRoot => _hostingRoot ??= HostingEnvironment.MapPath("~/");

	public FilePath MapPath(string path)
	{
		if (path.Length > 0)
			switch (path[0])
			{
			case '~':
				if (path.Length > 1)
					return path[1] switch
					{
						'\\' => HostingRoot.Combine(path.Substring(2)),
						'/' => HostingRoot.Combine(path.Substring(2)),
						_ => throw new ArgumentOutOfRangeException(nameof(path), $"invalid relative path: {path}")
					};

				return HostingRoot;
			}

		return HostingRoot.Combine(path);
	}

	public UrlHelper GetUrlHelper(string baseUri = null)
		=> GetUrlHelper(baseUri.With(u => new Uri(u)));

	public DotNetFrameworkMvcHelper()
		=> IsInitialized = true;

	public UrlHelper GetUrlHelper(Uri baseUri)
	{
		var httpContext = HttpContext.Current ?? CreateHttpContext(baseUri);

		if (httpContext.Items["UrlHelper"] is UrlHelper result)
			return result;

		if (httpContext.Items["RequestContext"] is not RequestContext requestContext)
			try
			{
				httpContext.Items["RequestContext"] = requestContext = httpContext.Request.RequestContext;
			}
			catch
			{
				var httpContextWrapper = new HttpContextWrapper(httpContext);
				httpContext.Items["RequestContext"] = requestContext = new RequestContext(httpContextWrapper, new RouteData());
			}

		httpContext.Items["UrlHelper"] = result = new UrlHelper(requestContext);
		return result;
	}

	static HttpContext CreateHttpContext(Uri baseUri = null)
	{
		var request = baseUri != null
			? new HttpRequest(baseUri.PathAndQuery, baseUri.ToString(), baseUri.Query)
			: new HttpRequest("/", "http://example.com", "");

		var response = new HttpResponse(new StringWriter());
		return new HttpContext(request, response);
	}

	public Task<string> RenderViewToStringAsync(string controllerName, string viewName, object model, object viewBag = null)
		=> Task.FromResult(RenderViewToString(controllerName, viewName, model, viewBag));

	public string RenderViewToString(string controllerName, string viewName, object model, object viewBag = null)
	{
		using var writer = new StringWriter();
		var context = /*HttpContext.Current ??*/ CreateHttpContext();
		var contextBase = new HttpContextWrapper(context);
		var routeData = new RouteData();
		routeData.Values.Add("controller", controllerName);
			
		var controllerContext = new ControllerContext(contextBase, routeData, new EmptyController());
		var razorViewEngine = new RazorViewEngine();
		var razorViewResult = razorViewEngine.FindView(controllerContext,
			viewName,
			"",
			false);
	
		var viewDataDict = new ViewDataDictionary(model);
		if (viewBag != null)
			foreach (var pair in AnonTypeHelper.ToKVPairs<string, object>(viewBag))
				viewDataDict[pair.Key] = pair.Value;

		var viewContext = new ViewContext(controllerContext,
			razorViewResult.View,
			viewDataDict,
			new TempDataDictionary(),
			writer);

		razorViewResult.View.Render(viewContext, writer);
		return writer.ToString();
	}

	class EmptyController : ControllerBase // For RenderViewToString
	{
		protected override void ExecuteCore()
		{
		}
	}
}

#endif