#if !NETCOREAPP && !NETSTANDARD // Т.к. проект общий нельзя просто так взять и выключить файлы. 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.SessionState;
using Booster.Collections;
using Booster.Synchronization;

#nullable enable

namespace Booster.Mvc.Helpers;

public class DotNetFrameworkObjectSession : IObjectSession
{
	readonly HttpSessionState _session; //TODO: эта хуйня не поддерживает Concurrency, надо написать свою нормальную сессию

	public DotNetFrameworkObjectSession(HttpSessionState session) => _session = session;

	public IEnumerable<string> Keys => _session.Keys.Cast<string>();

	static readonly CacheDictionary<string, HashLocksPool> _asyncHashLocks = new();

	protected HashLocksPool Locks => _asyncHashLocks.GetOrAdd(_session.SessionID, _ => new HashLocksPool(256));

	public object this[string key]
	{
		get => Get(key);
		set => Set(key, value);
	}

	public object Get(string key)
		=> _session[key];

	public void Set(string key, object? value)
		=> _session[key] = value;

	public bool TryGetValue<T>(string key, out T value)
	{
		var obj = _session[key];

		if (obj is T variable)
		{
			value = variable;
			return true;
		}
		value = default!;
		return false;
	}

	public bool TryRemove<T>(string key, out T value)
	{
		var obj = _session[key];

		if (obj is T variable)
		{
			_session.Remove(key);
			value = variable;
			return true;
		}
		value = default!;
		return false;
	}

	public T? SafeGet<T>(string key)
	{
		if (Get(key) is T result)
			return result;

		return default;
	}

	public T GetOrAdd<T>(string key, Func<T> creator) //Лочить тут ничего не надо, т.к. мы в фиксированном треде
	{
		if (!TryGetValue<T>(key, out var result))
			using (Locks.Lock(key))
				if (!TryGetValue(key, out result))
				{
					result = creator();
					Set(key, result);
				}

		return result!;
	}

	public async Task<T> GetOrAddAsync<T>(string key, Func<Task<T>> creator)
	{
		if (!TryGetValue<T>(key, out var result))
		{
			using var _ = await Locks.LockAsync(key).NoCtx();

			if (!TryGetValue(key, out result))
			{
				result = await creator().NoCtx();
				Set(key, result);
			}
		}
		return result!;
	}

	public void Remove(string key)
		=> _session.Remove(key);

	public void Clear()
	{
		using (Locks.LockAll())
			_session.Clear();
	}
}

#endif