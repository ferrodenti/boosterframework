using System;
using System.Collections.Generic;
using System.Linq;
using Booster.Reflection;
using JetBrains.Annotations;

#if NETSTANDARD || NETCOREAPP
using Microsoft.AspNetCore.Mvc;
#else
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
#endif

namespace Booster.Mvc;

[PublicAPI]
public class ControllerActionConstraint : ArrayConstraint
{
	public static HashSet<string> GetControllerActions(TypeEx controllerType)
	{
		var result = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
		
		result.AddRange(controllerType.FindMethods(new ReflectionFilter {Access = AccessModifier.Public, IsStatic = false})
			.Where(mi => !mi.HasAttribute<NonActionAttribute>() && !(mi.IsSpecialName && (mi.Name.StartsWith("set_") || mi.Name.StartsWith("get_"))))
			.Select(mi => mi.Name));

		return result;
	}

	public ControllerActionConstraint(TypeEx controllerType)
		: base(GetControllerActions(controllerType))
	{
	}
}
