﻿#if NETCOREAPP
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
#else
using System.Web;
using System.Web.Routing;
#endif

#nullable enable

namespace Booster.Mvc;

public abstract class BaseConstraint : IRouteConstraint
{
#if NETSTANDARD || NETCOREAPP
	public bool Match(HttpContext? httpContext, IRouter? route, string routeKey, RouteValueDictionary values, RouteDirection routeDirection)
#else
	public bool Match(HttpContextBase httpContext, Route route, string routeKey, RouteValueDictionary values, RouteDirection routeDirection)
#endif
	{
		if (!values.TryGetValue(routeKey, out var value))
			return true;

		var str = value?.ToString();
		return str != null && Match(str);
	}
	

	protected abstract bool Match(string value);
}
