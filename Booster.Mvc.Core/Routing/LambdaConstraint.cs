﻿using System;

#nullable enable

namespace Booster.Mvc;

public class LambdaConstraint : BaseConstraint
{
	readonly Func<string, bool> _check;

	public LambdaConstraint(Func<string, bool> check)
		=> _check = check;

	protected override bool Match(string value)
		=> _check(value);
}