﻿using System;
using System.Collections.Generic;

#nullable enable

namespace Booster.Mvc;

public class ArrayConstraint : BaseConstraint
{
	readonly HashSet<string> _values;
	
	public ArrayConstraint(IEnumerable<string> values, StringComparer? comparer = null) 
		=> _values = new HashSet<string>(values, comparer ?? StringComparer.OrdinalIgnoreCase);
	
	public ArrayConstraint(HashSet<string> values) 
		=> _values = values;

	protected override bool Match(string value)
		=> _values.Contains(value);
}
