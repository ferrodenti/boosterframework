﻿using System.Reflection;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;

#nullable enable

namespace Booster;

[PublicAPI]
public class ComplexObjectRouteBinderProvider : IModelBinderProvider
{
	class Binder : IModelBinder
	{
		public Task BindModelAsync(ModelBindingContext bindingContext)
		{
			var key = bindingContext.ModelName;
			if (bindingContext.ActionContext.RouteData.Values.TryGetValue(key, out var value))
				bindingContext.Result = ModelBindingResult.Success(value);

			return Task.CompletedTask;
		}
	}

	static readonly Assembly _thisAssembly = typeof(ComplexObjectRouteBinderProvider).Assembly;

	public IModelBinder? GetBinder(ModelBinderProviderContext context)
		=> context.Metadata.ModelType.Assembly == _thisAssembly 
			? new BinderTypeModelBinder(typeof(Binder)) 
			: null;
}
