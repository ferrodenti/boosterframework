﻿#if !NETSTANDARD && !NETCOREAPP3
using System.Web.Routing;

namespace Booster.Mvc
{
	public class SessionControllerHandler : System.Web.Http.WebHost.HttpControllerHandler, System.Web.SessionState.IRequiresSessionState
	{
		public SessionControllerHandler(RouteData routeData)
			: base(routeData)
		{ }
	}
}
#endif
