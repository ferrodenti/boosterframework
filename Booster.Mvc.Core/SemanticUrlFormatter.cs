﻿using System;
using Booster.Localization;
using JetBrains.Annotations;

namespace Booster.Mvc;

[PublicAPI]
public class SemanticUrlFormatter
{
	readonly Transliterator _translit;

	public SemanticUrlFormatter()
	{
		_translit = new Transliterator(Transliterator.Iso995);
		_translit.AddRule(" ", "-");
		_translit.AddRule("_", "-");
		_translit.AddRule("№", "no");
	}

	public string CreateUnique(string str, [InstantHandle]Func<string, bool> checkUnique, string counterFormat = "{0}")
	{
		var initial = Format(str);

		for (var i = 1;; i++)
		{
			str = i > 1 ? initial + string.Format(counterFormat, i) : initial;
			if (checkUnique(str))
				return str;
		}
	}

	public string Format(string str)
		=> _translit.Convert(str).ToLower().Replace("--", "-").Trim('-');

	public string FormatFile(string str, string defaultExt = null) //TODO: возможно, эта хрень не нужна. что-то она делает непонятное
	{
		var i = str.LastIndexOf('.');
		if (i > 0)
		{
			var extension = i < str.Length ? Format(str.Substring(i, str.Length - i)) : null;

			if (string.IsNullOrWhiteSpace(extension))
				extension = defaultExt;

			return extension == null 
				? Format(str.Substring(0, i)) 
				: $"{Format(str.Substring(0, i))}.{extension}";
		}

		return Format(str);
	}
}