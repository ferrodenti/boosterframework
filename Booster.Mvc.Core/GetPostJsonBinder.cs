﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using Booster.Reflection;

namespace Booster.Mvc;

#if NETSTANDARD || NETCOREAPP3
	using Microsoft.AspNetCore.Mvc.ModelBinding;
	using System.Threading.Tasks;

	public class GetPostJsonBinder : IModelBinder
	{
		public Task BindModelAsync(ModelBindingContext bindingContext)
		{				
			var request = bindingContext.HttpContext.Request;
		
			switch (request.Method.ToLower())
			{
			case "get":
				bindingContext.Result = ModelBindingResult.Success(BindModel(request.Query, bindingContext.ModelType));
				break;
			case "post":
				bindingContext.Result = ModelBindingResult.Success(BindModel(request.Form, bindingContext.ModelType));
				break;
			default:
				return Task.CompletedTask;
			}
		
			return Task.CompletedTask;
		}
#else	
using System.Collections.Specialized;
using System.Linq;
using System.Net.Http;
using System.Web.Http.Controllers;
	
public class GetPostJsonBinder : System.Web.Http.ModelBinding.IModelBinder, System.Web.Mvc.IModelBinder
{
	public bool BindModel(HttpActionContext actionContext, System.Web.Http.ModelBinding.ModelBindingContext bindingContext)
	{
		var req = actionContext.Request;

		if (req.Method == HttpMethod.Get)
		{
			bindingContext.Model = BindModel(req.GetQueryNameValuePairs().Select(p => new KeyValuePair<string, object>(p.Key, p.Value)), bindingContext.ModelType);
			return true;
		}

		if (req.Method == HttpMethod.Post)
		{
			bindingContext.Model = BindModel(req.Content.ReadAsFormDataAsync().NoCtxResult(), bindingContext.ModelType);
			return true;
		}

		return false;
	}

	public object BindModel(System.Web.Mvc.ControllerContext controllerContext, System.Web.Mvc.ModelBindingContext bindingContext)
	{
		var req = controllerContext.RequestContext.HttpContext.Request;
		var mtd = req.HttpMethod?.ToLower();

		return mtd switch
		       {
			       "get"  => BindModel(req.QueryString, bindingContext.ModelType),
			       "post" => BindModel(req.Form, bindingContext.ModelType),
			       _      => null
		       };
	}

	object BindModel(NameValueCollection nameValues, TypeEx modelType)
	{
		var ie = nameValues.AllKeys.Select(k => new KeyValuePair<string, object>(k, nameValues[k]));
		return BindModel(ie, modelType);
	}
#endif
	readonly ConcurrentDictionary<TypeEx, JsonArrModelBinder> _binders = new();
		
	object BindModel<TValues>(IEnumerable<KeyValuePair<string, TValues>> nameValues, TypeEx modelType)
	{
		var binder = _binders.GetOrAdd(modelType, m => new JsonArrModelBinder(m));
		var model = binder.CreateModel();

		foreach (var pair in nameValues)
			binder.BindValue(model, pair.Key, pair.Value);

		return model;
	}
}