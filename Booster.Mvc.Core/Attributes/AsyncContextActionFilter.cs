using System;
using System.Diagnostics;
using Booster.DI;
using Booster.Log;
#if NETSTANDARD || NETCOREAPP
using Microsoft.AspNetCore.Mvc.Filters;
#else
using System.Web.Mvc;
#endif
#if DEBUG_ASYNC_CONTEXT
using Booster.Mvc.Helpers;
#endif
namespace Booster.Mvc;

#nullable enable

[Obsolete($"Use AsyncContextMiddleware")]
public class AsyncContextActionFilter : IActionFilter, IResultFilter
{
	static ILog? _log;

	[Conditional("DEBUG_ASYNC_CONTEXT")]
	static void Log(string message)
	{
		var log = _log ??= InstanceFactory.Get<ILog>(false);
		log.Trace(message);
	}

	public void OnActionExecuting(ActionExecutingContext context)
	{
#if DEBUG_ASYNC_CONTEXT
			AsyncContext.InitNew(MvcHelper.DisplayUri);
#else
		AsyncContext.InitNew();
#endif
		Log($"OnActionExecuting: {AsyncContext.CurrentName}");
		//context.HttpContext.Items["__async_context"] = AsyncContext.InitNew(context.HttpContext?.Request?.GetDisplayUrl() ?? "unnamed request");	
	}

	public void OnActionExecuted(ActionExecutedContext context)
		=> Log($"OnActionExecuted: {AsyncContext.CurrentName}");

	public void OnResultExecuting(ResultExecutingContext context)
		=> Log($"OnResultExecuting: {AsyncContext.CurrentName}");

	//AsyncContext.Current = (AsyncContextDebug) context.HttpContext.Items["__async_context"];
	//Log.Trace($"OnResultExecuting2: {AsyncContext.CurrentName}");
	public void OnResultExecuted(ResultExecutedContext context)
	{
		Log($"OnResultExecuted: {AsyncContext.CurrentName}");
		AsyncContext.Dispose();
	}
}