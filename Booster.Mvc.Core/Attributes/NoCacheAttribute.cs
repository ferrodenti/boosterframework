using System;
using JetBrains.Annotations;

#if NETSTANDARD || NETCOREAPP
using Microsoft.AspNetCore.Mvc.Filters;
#else
using System.Web;
using System.Web.Mvc;
#endif

namespace Booster.Mvc;

[AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
[PublicAPI]
#if NETSTANDARD || NETCOREAPP
public sealed class NoCacheAttribute : Attribute, IResultFilter
{
	public void OnResultExecuting(ResultExecutingContext filterContext)
	{
	}

	public void OnResultExecuted(ResultExecutedContext filterContext)
	{
	}
}
#else
public sealed class NoCacheAttribute : FilterAttribute, IResultFilter
{
	public NoCacheAttribute()
	{		
		Order = int.MaxValue;	
	}

	public void OnResultExecuting(ResultExecutingContext filterContext)
	{
	}

	public void OnResultExecuted(ResultExecutedContext filterContext)
	{
		if (!Equals(filterContext.HttpContext.Items["CacheUsed"], true))
		{
			var cache = filterContext.HttpContext.Response.Cache;
			cache.SetCacheability(HttpCacheability.NoCache);
			cache.SetRevalidation(HttpCacheRevalidation.ProxyCaches);
			cache.SetExpires(DateTime.Now.AddYears(-5));
			cache.AppendCacheExtension("private");
			cache.AppendCacheExtension("no-cache=Set-Cookie");
			cache.SetMaxAge(TimeSpan.Zero);
		}
	}
}
#endif
