using Booster.DI;
#if NETSTANDARD || NETCOREAPP
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
#else
using System.Web.Mvc;
#endif

namespace Booster.Mvc;

public class AccessAttribute : ActionFilterAttribute
{
	public object[] RequiredPermissions { get; set; }

	/// <summary>
	/// Требуется пользователь с любыми разрешениями (Например, любой зарегистрированный)
	/// </summary>
	public AccessAttribute()
	{
	}

	public AccessAttribute(params object[] requiredPermissions)
		=> RequiredPermissions = requiredPermissions;

#if NETSTANDARD || NETCOREAPP
	public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
	{
		var pp = InstanceFactory.Get<IPermissionsProvider>();
		if (pp != null)
		{
			var userPermissions = await pp.GetUserPermissions().NoCtx();

			if (RequiredPermissions == null)
			{
				if (userPermissions != null)
				{
					await next().NoCtx();
					return;
				}
			}
			else if (pp.ComparePermisssions(RequiredPermissions, userPermissions))
			{
				await next().NoCtx();
				return;
			}

			context.Result = userPermissions == null
				? new ChallengeResult()
				: new StatusCodeResult(403);
		}
	}
#else
	public override void OnActionExecuting(ActionExecutingContext filterContext)
	{
		var pp = InstanceFactory.Get<IPermissionsProvider>();
		if (pp != null)
		{
			var userPermissions = pp.GetUserPermissions().NoCtxResult();

			if (RequiredPermissions == null)
			{
				if (userPermissions != null)
					return;
			}
			else if (pp.ComparePermisssions(RequiredPermissions, userPermissions))
				return;

			filterContext.Result = userPermissions == null
				? new HttpUnauthorizedResult()
				: new HttpStatusCodeResult(403);

		}
	}
#endif
}