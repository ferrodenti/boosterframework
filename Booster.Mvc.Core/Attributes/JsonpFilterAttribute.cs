﻿using System;
using Booster.Mvc.Results;
using JetBrains.Annotations;
#if NETSTANDARD || NETCOREAPP
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
#else
using System.Web.Mvc;
#endif

namespace Booster.Mvc;

[PublicAPI]
public class JsonpFilterAttribute : ActionFilterAttribute
{
	public override void OnActionExecuted(ActionExecutedContext filterContext)
	{
		if (filterContext == null)
			throw new ArgumentNullException(nameof(filterContext));

#if NETSTANDARD || NETCOREAPP
		string callback = filterContext.HttpContext.Request.Query["callback"];
#else
		var callback = filterContext.HttpContext.Request.QueryString["callback"];
#endif

		if (string.IsNullOrEmpty(callback))
			return;

		var jpResult = new JsonpResult();

		switch (filterContext.Result)
		{
		case JsonpResult:
			return;
		case JsonResult jr:
#if NETSTANDARD || NETCOREAPP
			jpResult.Data = jr.Value;
#else
			jpResult.ContentEncoding = jr.ContentEncoding;
			jpResult.Data = jr.Data;
#endif
			jpResult.ContentType = jr.ContentType;
			break;
		case JsonNetResult jn:
			jpResult.Data = jn.Data;
			jpResult.ContentType = jn.ContentType;
			jpResult.ContentEncoding = jn.ContentEncoding;
			break;
		default:
			throw new InvalidOperationException("JsonpFilterAttribute must be applied only on controllers and actions that return a JsonpResult, JsonResult or JsonNetResult.");
		}

		filterContext.Result = jpResult;
	}
}
