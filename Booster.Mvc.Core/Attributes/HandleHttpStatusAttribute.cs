#if !NETSTANDARD && !NETCOREAPP

using System;
using Booster.Reflection;
using JetBrains.Annotations;
#if NETSTANDARD
using Microsoft.Extensions.Primitives;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
#else
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
#endif


namespace Booster.Mvc;

/// <summary>
/// Позволяет задать представление для HTTP ошибкок и обработать исключения HttpResponseException 
/// </summary>
[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true), PublicAPI]
public class HandleHttpStatusAttribute : ActionFilterAttribute, IExceptionFilter //TODO: std implementation
{
	/// <summary>
	/// Тип обрабатываемого исключения
	/// </summary>
	public TypeEx ExceptionType;

#if !DNX
	/// <summary>
	/// Уникальный идентификатор аттрибута.
	/// </summary>
	public override object TypeId { get; } = new();
#endif
	/// <summary>
	/// Представление для ошибки. Если не задано, то будут использованы представления по умолчанию
	/// </summary>
	public string View;

	/// <summary>
	/// HTTP Код ошибки. Если 0, то будут обработанны все ошибки >= 400
	/// </summary>
	public int StatusCode;

	public bool AppendExceptionMessageToStatusDescription;

	/// <inheritdoc/>
	public HandleHttpStatusAttribute() { }
	/// <inheritdoc/>
	public HandleHttpStatusAttribute(int statusCode, [PathReference] string view)
	{
		StatusCode = statusCode;
		View = view;
	}

	/// <inheritdoc/>
	public HandleHttpStatusAttribute([PathReference] string view)
		=> View = view;


	/// <inheritdoc/>
	public virtual void OnException(ExceptionContext exceptionContext)
	{
		if (exceptionContext == null)
			throw new ArgumentNullException(nameof(exceptionContext));
#if !NETSTANDARD
		if (exceptionContext.IsChildAction)
			return;
#endif
		if (exceptionContext.ExceptionHandled)
			return;

		int statusCode;
		string statusDescription = null;

		var exception = exceptionContext.Exception;
#if NETSTANDARD
		if (exception is HttpError respExc)
#else
		if (exception is HttpResponseException respExc)		
#endif
		{

			statusCode = (int) respExc.Response.StatusCode;
			statusDescription = respExc.Response.ReasonPhrase;
			exception = null;
		}
		else
		{
			if (ExceptionType?.IsInstanceOfType(exception) == false)
				return;
#if NETSTANDARD
			statusCode = 500;
#else
			statusCode = new HttpException(null, exception).GetHttpCode();
#endif
			if (AppendExceptionMessageToStatusDescription)
				statusDescription = exception.Message;
		}

		if (!IsHttpCodeHandled(statusCode))
			return;

		exceptionContext.ExceptionHandled = true;

		if (!HandlePlainResult(exceptionContext, statusCode, statusDescription, exception))
		{
			if (View != null)
				SetResult(exceptionContext, exception, statusCode, statusDescription);
			else
#if NETSTANDARD
				SetResult(exceptionContext, statusCode, new StatusCodeResult(statusCode));
#else
				SetResult(exceptionContext, statusCode, new HttpStatusCodeResult(statusCode, statusDescription));
#endif
					
		}
	}

	static bool HandlePlainResult(ControllerContext filterContext, int statusCode, string statusDescription, Exception exception)
	{
#if NETSTANDARD
		var xplain = filterContext.HttpContext.Request.Headers["x-plain-errors"].ToString().ToLower();
#else
		var xplain = filterContext.HttpContext.Request.Headers["x-plain-errors"]?.ToLower();	
#endif
		if (xplain.IsSome() && xplain != "false")
		{
			if ((xplain == "long" || xplain == "full") && exception != null)
				statusDescription = exception.ToString();

			SetResult(filterContext, statusCode, new ContentResult {Content = $"Ошибка {statusCode}: {statusDescription}"});
			return true;
		}
		return false;
	}

	void SetResult(ControllerContext context, Exception exception, int statusCode, string statusDescription = null)
	{
#if NETSTANDARD
		var url = context.HttpContext?.Request.GetDisplayUrl();
		var nolayout = context.HttpContext?.Request.Headers["no-layout"].ToString().ToLower();
#else
		var url = context.HttpContext?.Request.RawUrl;
		var nolayout = context.HttpContext?.Request.Headers["no-layout"]?.ToLower();			
#endif
						
		var model = new HandleHttpStatusInfo
		{
			Url = url,
			ActionName = context.RouteData.Values.SafeGet("action") as string,
			ControllerName = context.RouteData.Values.SafeGet("controller") as string,
			Exception = exception,
			StatusCode =  statusCode,
			StatisDescription = statusDescription,
			NoLayout = nolayout == "true"
		};

		var result = new ViewResult
		{
			ViewName = View,
			ViewData = new ViewDataDictionary<HandleHttpStatusInfo>(model),
			TempData = context.Controller.TempData
		};

		SetResult(context, statusCode, result);
	}

	static void SetResult(ControllerContext context, int statusCode, ActionResult result)
	{
		switch (context)
		{
		case ActionExecutedContext aec:
			aec.Result = result;
			break;
		case ExceptionContext aec2:
			aec2.Result = result;
			break;
		}

		context.HttpContext.Response.Clear();
		context.HttpContext.Response.StatusCode = statusCode;
#if !NETSTANDARD
		context.HttpContext.Response.TrySkipIisCustomErrors = true;
#endif
	}

	static void SetResult(ExceptionContext context, int statusCode, ActionResult result)
	{
		context.Result = result;
		context.HttpContext.Response.Clear();
		context.HttpContext.Response.StatusCode = statusCode;
#if !NETSTANDARD
		context.HttpContext.Response.TrySkipIisCustomErrors = true;
#endif
	}

	/// <inheritdoc/>
	public override void OnActionExecuted(ActionExecutedContext filterContext)
	{
		if (View != null)
		{
			if (filterContext == null)
				throw new ArgumentNullException(nameof(filterContext));

#if NETSTANDARD	
			if (filterContext.Result is StatusCodeResult str && IsHttpCodeHanled(str.StatusCode))
			{
				if (!HandlePlainResult(filterContext, str.StatusCode, null, null))
					SetResult(filterContext, null, str.StatusCode, null);

				return;
			}
#else
			if (filterContext.Result is HttpStatusCodeResult str && IsHttpCodeHandled(str.StatusCode))
			{
				if (!HandlePlainResult(filterContext, str.StatusCode, str.StatusDescription, null))
					SetResult(filterContext, null, str.StatusCode, str.StatusDescription);

				return;
			}
#endif
		}
		base.OnActionExecuted(filterContext);
	}

	bool IsHttpCodeHandled(int code)
		=> code > 401 && (StatusCode == 0 || code == StatusCode);
}

#endif