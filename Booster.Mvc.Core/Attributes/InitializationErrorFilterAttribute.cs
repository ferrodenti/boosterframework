using System;
using System.IO;
using JetBrains.Annotations;
#if NETSTANDARD || NETCOREAPP
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
#else
using System.Text;
using System.Web.Mvc;
#endif

namespace Booster.Mvc;

[PublicAPI]
public class InitializationErrorFilterAttribute : ActionFilterAttribute
{
	readonly Exception _initializationError;

	public InitializationErrorFilterAttribute(Exception initializationError)
		=> _initializationError = initializationError;

	public override void OnActionExecuting(ActionExecutingContext filterContext)
	{
		if (_initializationError != null)
		{
#if NETSTANDARD || NETCOREAPP
			using var wr = new StreamWriter(filterContext.HttpContext.Response.Body);
#else
				using var wr = new StreamWriter(filterContext.HttpContext.Response.OutputStream);
#endif

			wr.WriteLine("<!DOCTYPE html><html lang=\"ru\"><head></head><title>FATAL ERROR</title><body>");
			wr.WriteLine("<h1>Application initialization error</h1>");

			var ex = _initializationError;
			do
			{
				wr.WriteLine($"<h2>{ex.GetType()}: {ex.Message}</h2>");
				wr.WriteLine("<h3>Stacktrace:</h3>");
				wr.WriteLine($"<p>{ex.StackTrace?.Replace(Environment.NewLine, "<br/>")}</p>");
				wr.WriteLine("<hr/>");
				ex = ex.InnerException;
			} while (ex != null);

			wr.WriteLine("</body></html>");
#if NETSTANDARD || NETCOREAPP
			filterContext.Result = new ContentResult {Content = wr.ToString(), ContentType = "text/html"};
#else
				filterContext.Result = new ContentResult {Content = wr.ToString(), ContentType = "text/html", ContentEncoding = Encoding.UTF8};
#endif
			//filterContext.HttpContext.Response.StatusCode = 500;
			//filterContext.HttpContext.Response.End();
			return;
		}

		base.OnActionExecuting(filterContext);
	}
}