using System.Threading.Tasks;

namespace Booster.Mvc;

/// <summary>
/// Интерфейс поставщика пользовательских разрешений
/// </summary>
public interface IPermissionsProvider
{
	object FromString(string str);
	/// <summary>
	/// Должен вернуть объект разрешений действующих для пользователя авторизованного текущим запросом.
	/// Тип возвращаемого значения не конкретизируется.
	/// </summary>
	Task<object> GetUserPermissions();
	/// <summary>
	/// Должен выполнить сравнение необходимых разрешений с предоставленными. Типы разрешения не конкретизируются.
	/// </summary>
	/// <param name="requiredPermission">Объект необходимых разрешений</param>
	/// <param name="userPermissions">Объект предоставляемых разрешений</param>
	/// <returns>true если предоставленныее разрешения удовлетворяют необходимым</returns>
	bool ComparePermisssions(object requiredPermission, object userPermissions);
}