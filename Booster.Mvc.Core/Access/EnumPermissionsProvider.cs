using System;
using System.Threading.Tasks;
using Booster.Reflection;
using JetBrains.Annotations;

namespace Booster.Mvc;

[PublicAPI] 
public class EnumPermissionsProvider<TPermissions> : BasicRolePermissionsProvider where TPermissions : struct
{
	readonly Func<TPermissions?> _permissionGetter;
	readonly Func<Task<TPermissions?>> _asyncPermissionGetter;

	public EnumPermissionsProvider(Func<TPermissions?> permissionGetter)
	{
		_permissionGetter = permissionGetter;
		PermissionGetterFunc = GetUserPermissionsBytes;
	}
		
	public EnumPermissionsProvider(Func<Task<TPermissions?>> permissionGetter)
	{
		_asyncPermissionGetter = permissionGetter;
		AsyncPermissionGetterFunc = GetUserPermissionsBytesAsync;
	}

	public override object FromString(string str)
	{
		if (str.IsEmpty())
			return null;
			
		if (Enum.TryParse<TPermissions>(str, out var value))
			return value;

		return str;
	}

	protected object GetUserPermissionsBytes()
		=> GetPermissionsBytes(_permissionGetter());
		
	protected async Task<object> GetUserPermissionsBytesAsync()
	{
		TPermissions? permissions;
		if (_asyncPermissionGetter != null)
			permissions = await _asyncPermissionGetter().NoCtx();
		else
			permissions = _permissionGetter();
			
		return GetPermissionsBytes(permissions);
	}

	protected override bool HasPermission(object expected, object actual)
	{
		var bAct = (byte[]) actual;
		var bExp = GetPermissionsBytes(expected);

		var len = Math.Min(bExp.Length, bAct.Length);
		var i = 0;
		for (; i < len; i++)
			if((bAct[i] & bExp[i]) != bExp[i])
				return false;

		for(; i < bExp.Length; i++)
			if (bExp[i] != 0)
				return false;

		return true;
	}

	protected virtual byte[] GetPermissionsBytes(object o)
	{
		if (o == null)
			return null;

		TypeEx type = o.GetType();
		if (type.IsEnum)
		{
			o = EnumExpander.GetIntValue(o);
			type = o.GetType();
		}

		return type.TypeCode switch
		       {
			       TypeCode.SByte  => BitConverter.GetBytes((sbyte) o),
			       TypeCode.Byte   => BitConverter.GetBytes((byte) o),
			       TypeCode.Int16  => BitConverter.GetBytes((short) o),
			       TypeCode.UInt16 => BitConverter.GetBytes((ushort) o),
			       TypeCode.Int32  => BitConverter.GetBytes((int) o),
			       TypeCode.UInt32 => BitConverter.GetBytes((uint) o),
			       TypeCode.Int64  => BitConverter.GetBytes((long) o),
			       TypeCode.UInt64 => BitConverter.GetBytes((ulong) o),
			       _               => throw new ArgumentOutOfRangeException()
		       };
	}
}