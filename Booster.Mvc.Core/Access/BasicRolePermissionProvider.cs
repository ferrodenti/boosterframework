using System;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using Booster.DI;

namespace Booster.Mvc;

[InstanceFactory(typeof(IPermissionsProvider))]
public class BasicRolePermissionsProvider : IPermissionsProvider
{
	protected Func<Task<object>> AsyncPermissionGetterFunc;
	protected Func<object> PermissionGetterFunc;

	protected BasicRolePermissionsProvider()
	{
	}

	public BasicRolePermissionsProvider(Func<object> permissionGetter) 
		=> PermissionGetterFunc = permissionGetter;		
		
	public BasicRolePermissionsProvider(Func<Task<object>> permissionGetter) 
		=> AsyncPermissionGetterFunc = permissionGetter;

	public virtual object FromString(string str)
		=> str;

	public virtual Task<object> GetUserPermissions()
		=> AsyncPermissionGetterFunc != null 
			? AsyncPermissionGetterFunc() 
			: Task.FromResult(PermissionGetterFunc());

	public virtual bool ComparePermisssions(object requiredPermission, object userPermissions)
	{
		switch (requiredPermission)
		{
		case null:
			return true;
		case bool b:
			return b;
		case string str:
			switch (str.ToLower())
			{
			case "true":
				return true;
			case "false":
				return false;
			}
			break;
		case IEnumerable coll when requiredPermission is ICollection:
			return coll.Cast<object>().Any(o => ComparePermisssions(o, userPermissions));
		}

		return userPermissions != null && HasPermission(requiredPermission, userPermissions);
	}

	protected virtual bool HasPermission(object expected, object actual) 
		=> Equals(expected, actual);
}