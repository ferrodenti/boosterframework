﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Booster.FileSystem;
using JetBrains.Annotations;
#if NETSTANDARD || NETCOREAPP
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
#else
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Booster.DI;
#endif

#nullable enable

namespace Booster.Mvc.Helpers;

[PublicAPI]
public static class MvcHelper
{
	internal static void Init(IServiceProvider services) 
		=> Impl.Init(services);

	public static T? GetService<T>() 
		=> Impl.GetService<T>();

#if NETSTANDARD || NETCOREAPP
	public static Url[] AppUrls => Impl.AppUrls;
	public static int DefaultAppPort => Impl.DefaultAppPort;

	public static IApplicationBuilder UseBoosterMvcEndpoints(IApplicationBuilder app, Action<IEndpointRouteBuilder> configureRoutes) 
		=> Impl.UseBoosterMvcEndpoints(app, configureRoutes);

	public static IApplicationBuilder UseBoosterMvc(IApplicationBuilder app, Action<IRouteBuilder> configureRoutes) 
		=> Impl.UseBoosterMvc(app, configureRoutes);

	internal static readonly IMvcHelperImpl Impl = new NetCoreMvcHelper();

	public static IUrlHelper CreateUrlHelper(string? baseUri = null) 
		=> Impl.GetUrlHelper(baseUri);

	public static IUrlHelper CreateUrlHelper(Uri baseUri) 
		=> Impl.GetUrlHelper(baseUri);
#else
	internal static readonly IMvcHelperImpl Impl = new DotNetFrameworkMvcHelper();

	public static UrlHelper CreateUrlHelper(string baseUri = null) 
	=> Impl.GetUrlHelper(baseUri);

	public static UrlHelper CreateUrlHelper(Uri baseUri) 
	=> Impl.GetUrlHelper(baseUri);
#endif

	public static bool IsInitialized => Impl.IsInitialized;

#if NET5_0_OR_GREATER
	public static string UrlAction(
		[AspMvcAction] string actionName, 
		[AspMvcController] string controllerName, 
		object? routeValues = null)
		=> Impl.UrlAction(actionName, controllerName, routeValues);
#else
	public static string UrlAction(
		[AspMvcAction] string actionName, 
		[AspMvcController] string controllerName, 
		object? routeValues = null) 
		=> CreateUrlHelper().Action(actionName, controllerName, routeValues);
#endif

	public static string Url(string url) 
		=> CreateUrlHelper().Content(url);

	[Obsolete("Use \"Url\" instead")]
	public static string Content(string contentPath) 
		=> Url(contentPath);

	const string _userCtxSesKey = "user";

	[Obsolete("Use \"BaseCurrentUser\" instead")]
	public static TUser? GetUser<TUser>(Func<string, TUser?> loader) where TUser : class 
	{
		TUser? user = null;

		var ctx = HttpContext;
		if (ctx != null)
		{
			user = ctx.Items[_userCtxSesKey] as TUser;
			if (user == null)
			{
				var userName = ctx.User.Identity?.Name;

				var ses = Session;
				if (ses != null)
				{
					user = ses[_userCtxSesKey] as TUser;
					if (user == null && userName != null)
						ses[_userCtxSesKey] = user = loader(userName);
				}

				if (userName != null)
					user ??= loader(userName);

				ctx.Items[_userCtxSesKey] = user;
			}
		}
		return user;
	}

	[Obsolete("Use \"BaseCurrentUser\" instead")]
	public static Task<TUser?> GetUserAsync<TUser>(Func<string, Task<TUser?>> loader) where TUser : class
		=> GetUserAsync((s, _) => loader(s), CancellationToken.None);

	[Obsolete("Use \"BaseCurrentUser\" instead")]
	public static async Task<TUser> GetUserAsync<TUser>(Func<string, Task<TUser?>> loader, Func<TUser> getDefaultUser) where TUser : class
	{
		var result = await GetUserAsync(async (s, _) =>
		{
			var result = await loader(s).NoCtx();
			return result ?? getDefaultUser();
		}, CancellationToken.None).NoCtx();

		return result ?? getDefaultUser();
	}

	[Obsolete("Use \"BaseCurrentUser\" instead")]
	public static async Task<TUser?> GetUserAsync<TUser>(Func<string, CancellationToken, Task<TUser?>> loader, CancellationToken token) where TUser : class 
	{
		TUser? user = null;

		var ctx = HttpContext;
		if (ctx != null)
		{
			user = ctx.Items[_userCtxSesKey] as TUser;
			if (user == null)
			{
				var ses = Session;
				var userName = ctx.User.Identity?.Name;
				if (ses != null)
				{
					user = ses[_userCtxSesKey] as TUser;
					if (user == null && userName != null)
						ses[_userCtxSesKey] = user = await loader(userName, token).NoCtx();
				}

				if (userName != null)
					user ??= await loader(userName, token).NoCtx();

				ctx.Items[_userCtxSesKey] = user;
			}
		}
		return user;
	}

	[Obsolete("Use \"BaseCurrentUser\" instead")]
	public static void SetUser<TUser>(TUser? user, string? name, bool persistent = true, string authenticationScheme = "Cookies")
	{
		var ctx = HttpContext;
		if (ctx != null)
		{
			if (user != null)
				ctx.Items[_userCtxSesKey] = user;
			else
				ctx.Items.Remove(_userCtxSesKey);

			var ses = Session;
			if (ses != null)
				ses[_userCtxSesKey] = user;
		}
#if NETSTANDARD || NETCOREAPP
		else
			throw new Exception("HttpContext is null. The method shoud be called when HttpContext defined.");
#endif

		Try.IgnoreErrors(() =>
		{
#if NETSTANDARD || NETCOREAPP
			if (user != null)
			{
				var userClaims = new List<Claim>
				{
					new(ClaimTypes.Name, name!)
				};

				var principal = new ClaimsPrincipal(new ClaimsIdentity(userClaims, "local"));
				ctx.SignInAsync(authenticationScheme, principal, new AuthenticationProperties
				{
					IsPersistent = persistent
				}).Wait();
			}
			else
				ctx.SignOutAsync(authenticationScheme).Wait();

#else
			if (user != null)
				FormsAuthentication.SetAuthCookie(name, persistent);
			else
				FormsAuthentication.SignOut();
#endif
		});
	}

	public static HttpContext? HttpContext => Impl.GetHttpContext();
	public static IObjectSession? Session => Impl.Session;
	public static string? DisplayUri => Impl.DisplayUri;
	public static Url? RequestUrl => Impl.RequestUrl;

	public static void RememberHttpContext() 
		=> Impl.RememberHttpContext();

#if !NETSTANDARD && !NETCOREAPP
	public static void OnNewRequest()
	{
		AsyncContext.InitNew(/*HttpContext.Current.Request.RawUrl*/);
		Impl.RememberHttpContext();
	}
#endif

	public static Task<string> RenderViewToStringAsync(
		string controllerName, 
		[PathReference] string viewName, 
		object? model, 
		object? viewBag = null) 
		=> Impl.RenderViewToStringAsync(controllerName, viewName, model, viewBag);

	public static string RenderViewToString(
		string controllerName, 
		[PathReference] string viewName, 
		object? model,
		object? viewBag = null) 
		=> Impl.RenderViewToString(controllerName, viewName, model, viewBag);


	public static string? GetClientIpAddress()
	{
#if NETSTANDARD || NETCOREAPP
		var request = HttpContext?.Request;
		if (request == null)
			return null;

		IEnumerable<string> GetValues(string key)
		{
#if NETCOREAPP
			return request.Headers[key];
#else
			return request.GetHeaderValues(key);
#endif
		}

		var result = GetValues("X-Forwarded-For").SelectMany(v => v.SplitNonEmpty(',')).FirstOrDefault();
		if (result.IsSome())
			return result;
		
		result = HttpContext?.Connection.RemoteIpAddress?.ToString();
		
		if (result.IsEmpty()) 
			result = GetValues("REMOTE_ADDR").FirstOrDefault();
		
		return result;

#else
		var ipAddress = HttpContext?.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

		if (ipAddress.IsSome())
		{
			ipAddress = ipAddress.SplitNonEmpty(',').FirstOrDefault();
			if (ipAddress.IsSome())
				return ipAddress;
		}

		return HttpContext?.Request.ServerVariables["REMOTE_ADDR"];
#endif
	}
	
	public static FilePath MapPath(string path) 
		=> Impl.MapPath(path);
}