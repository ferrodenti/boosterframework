﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Booster.Mvc.Helpers;

public interface IObjectSession
{
	IEnumerable<string> Keys { get; }

	object this[string key] { get; set; }
		
	bool TryGetValue<T>(string key, out T value);
	bool TryRemove<T>(string key, out T value);
	T SafeGet<T>(string key);
	void Set(string key, object value);

	T GetOrAdd<T>(string key, Func<T> creator);
	Task<T> GetOrAddAsync<T>(string key, Func<Task<T>> creator);

	void Remove(string key);
	void Clear();
}