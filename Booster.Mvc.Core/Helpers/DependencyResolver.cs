﻿using System;
#if NETSTANDARD
using Microsoft.Extensions.DependencyInjection;
#endif

namespace Booster.Mvc.Helpers;

public class DependencyResolver : IServiceProvider
{
	protected const string NotInitializedMessage = "MvcHelper not initialized. You should call app.UseBoosterMvcHelper() in your Startup class.";
	IServiceProvider _serviceProvider;

	public virtual void Init(IServiceProvider services)
	{
		_serviceProvider = services;
		IsInitialized = _serviceProvider != null;
	}

	public bool IsInitialized { get; protected set; }

	public IServiceProvider ServiceProvider
	{
		get
		{
			if (_serviceProvider == null)
				throw new InvalidOperationException(NotInitializedMessage);

			return _serviceProvider;
		}
	}

	public object GetService(Type serviceType)
		=> ServiceProvider.GetService(serviceType);

	public object GetRequiredService(Type serviceType)
	{
#if NETSTANDARD
			return ServiceProvider.GetRequiredService(serviceType);

#else
		var result = GetService(serviceType);
		if (result == null)
			throw new InvalidOperationException($"Cannot resolve {serviceType.Name} dependency");

		return result;
#endif
	}

	public T GetService<T>()
		=> (T) GetService(typeof(T));

	public T GetRequiredService<T>()
		=> (T) GetRequiredService(typeof(T));
}
