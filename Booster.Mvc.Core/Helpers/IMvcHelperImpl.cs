﻿using System;
using System.Threading.Tasks;
using Booster.FileSystem;
using JetBrains.Annotations;

#if NETSTANDARD || NETCOREAPP
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
#else
using System.Web;
using System.Web.Mvc;
#endif

#nullable enable

namespace Booster.Mvc.Helpers;

public interface IMvcHelperImpl : IServiceProvider
{
	void Init(IServiceProvider services);
	
	IServiceProvider ServiceProvider { get; }
	HttpContext? GetHttpContext();
	void RememberHttpContext();
	IObjectSession? Session { get; }
	string? DisplayUri { get; }
	Url? RequestUrl { get; }
	
	bool IsInitialized { get; }

	object GetRequiredService(Type serviceType);
	T? GetService<T>();
	T GetRequiredService<T>();

	FilePath MapPath(string path);
#if NETSTANDARD || NETCOREAPP
	Url[] AppUrls { get; }
	int DefaultAppPort { get; }
	ISession? RawSession { get; }

	IUrlHelper GetUrlHelper(string? baseUri = null);
	IUrlHelper GetUrlHelper(Uri baseUri);

#if NET5_0_OR_GREATER
	string UrlAction(string actionName, string controllerName, object? routeValues = null);
#endif
	IApplicationBuilder UseBoosterMvcEndpoints(IApplicationBuilder app, Action<IEndpointRouteBuilder> configureRoutes);
	IApplicationBuilder UseBoosterMvc(IApplicationBuilder app, Action<IRouteBuilder> configureRoutes);

#else	
	UrlHelper GetUrlHelper(string? baseUri = null);
	UrlHelper GetUrlHelper(Uri baseUri);
#endif
	Task<string> RenderViewToStringAsync(string controllerName, [PathReference] string viewName, object? model, object? viewBag = null);
	string RenderViewToString(string controllerName, [PathReference] string viewName, object? model, object? viewBag = null);
}