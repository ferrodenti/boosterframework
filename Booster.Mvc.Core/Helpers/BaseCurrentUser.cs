﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Booster.DI;
using JetBrains.Annotations;
#if NETCOREAPP
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
#else
using System.Web;
using System.Web.Security;
#endif

#nullable enable

namespace Booster.Mvc.Helpers;

[PublicAPI]
public abstract class BaseCurrentUser<TUser, TThis> 
	where TThis : BaseCurrentUser<TUser, TThis>, new()
{
	protected readonly string ContextKey = "current_user";
	protected readonly string SessionKey = "current_user";

	//static readonly ContextLazy<TThis> _instance = new(() => new TThis());
	public static TThis Instance => new();
	
	protected readonly CtxLocal<TUser?>? CtxLocal;
	protected readonly HttpContext? Context;
	protected readonly IObjectSession? Session;

	protected BaseCurrentUser(HttpContext? context, IObjectSession? session, CtxLocal<TUser?>? ctxLocal = null)
	{
		Context = context;
		Session = session;
		CtxLocal = ctxLocal;
	}
	
	protected BaseCurrentUser(CtxLocal<TUser?>? ctxLocal)
		: this(null, null, ctxLocal)
	{
	}
	
	public static TUser Value
	{
		get => Instance.GetOrDefault();
		set => Instance.SignInOrOutAsync(value).NoCtxWait();
	}
	
	public static Task<TUser> GetAsync()
		=> Instance.GetOrDefaultAsync();

	public static Task SetAsync(TUser? user, bool persistent = true)
		=> Instance.SignInOrOutAsync(user, persistent);
	
	public static Task SignOutAsync()
		=> Instance.SignInOrOutAsync(default);

	public virtual TUser GetOrDefault()
		=> GetUserFromCtxLocal() ?? GetDefaultUser();

	public virtual Task<TUser> GetOrDefaultAsync(CancellationToken token = default)
		=> GetUserFromCtxLocalAsync(token).OrAsync(GetDefaultUser());

	public Task SignInOrOutAsync(TUser? user, bool persistent = true)
	{
		if (Context != null)
			Context.Items[ContextKey] = user;

		if (Session != null)
			Session[SessionKey] = user;

		if (user != null)
		{
			var username = GetUserIdentity(user);
			return SignInImpl(username, persistent);
		}

		return SignOutImpl();
	}
	
	protected TUser? GetUserFromCtxLocal()
	{
		if (CtxLocal == null)
			return GetUserFromContext();

		if (CtxLocal.TryGetValue(out var result))
			return result;
		
		var result2 = GetUserFromContext();
		if (result2 != null)
			CtxLocal.Value = result2;

		return result2;
	}

	protected async Task<TUser?> GetUserFromCtxLocalAsync(CancellationToken token)
	{
		if (CtxLocal == null)
			return await GetUserFromContextAsync(token).NoCtx();

		if (CtxLocal.TryGetValue(out var result))
			return result;

		var result2 = await GetUserFromContextAsync(token).NoCtx();
		if (result2 != null)
			CtxLocal.Value = result2;

		return result2;
	}

	protected TUser? GetUserFromContext()
	{
		if (Context == null)
			return GetUserFromSession();

		if (Context.Items.TryGetAltValue(ContextKey, out TUser? result))
			return result;

		result = GetUserFromSession();
		if (result != null)
			Context.Items[ContextKey] = result;

		return result;
	}

	protected async Task<TUser?> GetUserFromContextAsync(CancellationToken token)
	{
		if (Context == null)
			return await GetUserFromSessionAsync(token).NoCtx();

		if (Context.Items.TryGetAltValue(ContextKey, out TUser? result))
			return result;

		result = await GetUserFromSessionAsync(token).NoCtx();
		if (result != null)
			Context.Items[ContextKey] = result;

		return result;
	}

	protected TUser? GetUserFromSession()
	{
		if (Session == null)
			return GetUser();

		if (Session.TryGetValue<TUser>(SessionKey, out var result))
			return result;

		var result2 = GetUser();
		if (result2 != null)
			Session[SessionKey] = result2;

		return result2;
	}
	
	protected async Task<TUser?> GetUserFromSessionAsync(CancellationToken token)
	{
		if (Session == null)
			return await GetUserAsync(token).NoCtx();

		if (Session.TryGetValue<TUser>(SessionKey, out var result))
			return result;

		var result2 = await GetUserAsync(token).NoCtx();
		if (result2 != null)
			Session[SessionKey] = result2;

		return result2;
	}

	protected virtual TUser GetUser()
		=> LoadUser(GetUserIdentity()) ?? GetDefaultUser();

	protected virtual Task<TUser> GetUserAsync(CancellationToken token)
		=> LoadUserAsync(GetUserIdentity(), token).OrAsync(GetDefaultUser);
	
	protected abstract TUser? LoadUser(string? identity);
	protected abstract Task<TUser?> LoadUserAsync(string? identity, CancellationToken token);
	protected abstract string GetUserIdentity(TUser user);
	
	protected virtual string? GetUserIdentity()
		=> Context?.User.Identity?.Name;
	
	protected virtual TUser GetDefaultUser()
		=> default!;
	
#if NETCOREAPP
	protected string AuthenticationScheme => "Cookies";
	
	protected Task SignInImpl(string name, bool persistent)
	{
		if (Context == null)
			return Task.CompletedTask;
		
		var userClaims = new List<Claim>
		{
			new(ClaimTypes.Authentication, name)
		};

		var principal = new ClaimsPrincipal(new ClaimsIdentity(userClaims, "local"));
		
		return Context.SignInAsync(AuthenticationScheme, principal, new AuthenticationProperties
		{
			IsPersistent = persistent
		});
	}

	protected virtual Task SignOutImpl()
		=> Context != null 
			? Context.SignOutAsync(AuthenticationScheme) 
			: Task.CompletedTask;
#else
	protected Task SignInImpl(string name, bool persistent)
	{
		FormsAuthentication.SetAuthCookie(name, persistent);
		return Task.CompletedTask;
	}

	protected Task SignOutImpl()
	{
		FormsAuthentication.SignOut();
		return Task.CompletedTask;
	}
#endif
}
