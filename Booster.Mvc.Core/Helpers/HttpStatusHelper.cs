﻿#if NETSTANDARD || NETCOREAPP
using Microsoft.AspNetCore.Mvc;
#else
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using System.Web.Http.Results;
#endif
using JetBrains.Annotations;

namespace Booster.Mvc.Helpers;

[PublicAPI]
public static class HttpStatusHelper
{
#if NETSTANDARD || NETCOREAPP
		public static IActionResult NotFound(string reason = null)
		{
			if(reason.IsEmpty())
				return new NotFoundResult();

			return new NotFoundObjectResult(reason);
		}

		public static IActionResult Forbidden(string reason = null)
			=> new ObjectResult(reason) {StatusCode = 403};
		
		public static IActionResult Gone(string reason = null)
			=> new ObjectResult(reason) {StatusCode = 410};

		public static object ToApiResult(object result, object controller)
			=> result;
#else
	class ApiResult : StatusCodeResult
	{
		readonly string _reason;

		public ApiResult(ApiController controller, int status, string reason) : base((HttpStatusCode)status, controller)
		{
			_reason = reason;
		}

		public override async Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
		{
			var result = await base.ExecuteAsync(cancellationToken);
			if (_reason.IsSome())
				result.Content = new StringContent(_reason);

			return result;

		}
	}

	static string Encode(string reason)
		=> reason.WithSome(HttpUtility.HtmlEncode);

	public static ActionResult BadRequest(string reason = null)
		=> new HttpStatusCodeResult(400, Encode(reason));

	public static ActionResult Forbidden(string reason = null)
		=> new HttpStatusCodeResult(403, Encode(reason));
			
	public static ActionResult NotFound(string reason = null) 
		=> new HttpNotFoundResult(Encode(reason));

	public static ActionResult Gone(string reason = null)
		=> new HttpStatusCodeResult(410, Encode(reason));

	public static object ToApiResult(object result, ApiController controller)
	{
		if (result is HttpStatusCodeResult statusCodeResult)
			return new ApiResult(controller, statusCodeResult.StatusCode, statusCodeResult.StatusDescription);

		return result;
	}
#endif
	/// <summary>
	/// Отдает описание состояния HTTP ответа на русском по коду состояния ответа.
	/// </summary>
	/// <param name="code">Код состояния</param>
	/// <returns>Текстовое описание кода на русском</returns>
	public static string DescribeCode(int code)
		=> code switch
		   {
			   200 => "OK",
			   204 => "Нет содержания",
			   301 => "Перемещено постоянно",
			   303 => "Перемещено временно",
			   304 => "Не изменилось",
			   308 => "Перемещено постоянно",
			   400 => "Плохой запрос",
			   401 => "Требуется авторизация",
			   402 => "Необходим платеж",
			   403 => "Доступ закрыт",
			   404 => "Страница не найдена",
			   405 => "Метод не допустим",
			   406 => "Недопустимо",
			   408 => "Время ожидания истекло",
			   409 => "Конфликт",
			   410 => "Объект исчез",
			   500 => "Внутренняя ошибка сервера",
			   501 => "Не реализованно",
			   502 => "Плохой шлюз",
			   504 => "Время ожидания шлюза истекло",
			   507 => "Недостаточно ресурсов для сохранения",
			   444 => "Пустой ответ",
			   _   => ""
		   };
}