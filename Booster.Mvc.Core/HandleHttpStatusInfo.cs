using System;
using JetBrains.Annotations;

#if !NETSTANDARD && !NETCOREAPP
using System.Web.Mvc;
#endif

namespace Booster.Mvc;

[PublicAPI]
public class HandleHttpStatusInfo
{
	public string ActionName { get; set; }
	public string ControllerName { get; set; }
	public Exception Exception { get; set; }
	public int StatusCode { get; set; }
	public string StatisDescription { get; set; }
	public Url Url { get; set; }
	public bool NoLayout { get; set; }

#if !NETSTANDARD && !NETCOREAPP
		public static implicit operator HandleErrorInfo(HandleHttpStatusInfo i)
			=> new(i.Exception ?? new Exception("not set"), i.ActionName ?? "not set", i.ControllerName ?? "not set");
#endif
}
