using System;
using System.Web.Mvc;

namespace Booster.Mvc;

public static class CopyrightMessage
{
	public static MvcHtmlString From(int year)
	{
		var currentYear = DateTime.Today.Year;
		if (currentYear > year)
			return new MvcHtmlString($"� {year}-{currentYear}");

		return new MvcHtmlString($"� {year}");
	}
}