using System;
using Booster.Mvc.Helpers;

namespace Booster.Mvc;

public class SessionLazy<T> : SyncLazy<T>
{
	public bool UseLocal { get; set; }
	public string Key { get; }

	public SessionLazy(string key, Func<T> factory)
	{
		Key = key;
		Factory = factory;
	}

	[Obsolete]
	public new SessionLazy<T> SetFactory<TTarget>(Func<TTarget, T> factory)
	{
		base.SetFactory(factory);
		return this;
	}

	[Obsolete]
	public new SessionLazy<T> SetFactory(Func<T> factory)
	{
		Factory = factory;
		return this;
	}

	public override T GetValue(object target) 
		=> UseLocal 
			? base.GetValue(target) 
			: CreateValue(target);

	protected override T CreateValue(object target)
	{
		var ses = MvcHelper.Session;
		if (ses != null)
			return ses.GetOrAdd(Key, () => base.CreateValue(target));

		var httpCtx = MvcHelper.HttpContext;
		if (httpCtx != null)
		{
			var obj = httpCtx.Items[Key];
			if (obj is T result)
				return result;

			result = base.CreateValue(target);
			httpCtx.Items[Key] = result;
			return result;
		}

		return base.CreateValue(target);
	}

	public override void SetValue(T value)
	{
		lock (this)
		{
			MvcHelper.Session?.Set(Key, value);

			var httpCtx = MvcHelper.HttpContext;
			if (httpCtx != null)
				httpCtx.Items[Key] = value;
				
			if (UseLocal)
				SetInternal(value);
		}
	}

	public static implicit operator T(SessionLazy<T> cache) 
		=> cache.Value;

	public override string ToString()
		=> $"{Key}: {base.ToString()}";
}