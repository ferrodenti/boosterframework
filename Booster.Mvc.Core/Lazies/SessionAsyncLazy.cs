using System.Threading.Tasks;
using Booster.Mvc.Helpers;

namespace Booster.Mvc;

public class SessionAsyncLazy<T> : AsyncLazy<T>
{
	public bool DependOnInternal { get; set; } = true;
	public bool UseLocal { get; set; }
	public string Key { get; }
		
	public SessionAsyncLazy(string key) 
		=> Key = key;

	public override Task<T> GetValue(object target)
		=> UseLocal 
			? base.GetValue(target) 
			: CreateValue(target);

	protected override async Task<T> CreateValue(object target)
	{
		var ses = MvcHelper.Session;
		if (ses != null)
			return await ses.GetOrAddAsync(Key, () => base.CreateValue(target)).NoCtx();

		var httpCtx = MvcHelper.HttpContext;
		if (httpCtx != null)
		{
			var obj = httpCtx.Items[Key];
			if (obj is T result)
				return result;

			result = await base.CreateValue(target).NoCtx();
			httpCtx.Items[Key] = result;
			return result;
		}

		return await base.CreateValue(target).NoCtx();
	}

	public override void SetValue(T value)
	{
		lock (this)
		{
			MvcHelper.Session?.Set(Key, value);

			var httpCtx = MvcHelper.HttpContext;
			if (httpCtx != null)
				httpCtx.Items[Key] = value;

			if (UseLocal)
				SetInternal(value);
		}
	}

	public override string ToString()
		=> $"{Key}: {base.ToString()}";
}