﻿using System;
using System.Threading.Tasks;
using Booster.Reflection;
using Microsoft.Extensions.Primitives;

#if NETCOREAPP
using Microsoft.AspNetCore.Mvc.ModelBinding;
#else
using System.Web.ModelBinding;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Mvc;
using ModelBindingContext = System.Web.Mvc.ModelBindingContext;
#endif

#nullable enable

namespace Booster.Mvc.ModelBinders;

public abstract class BaseModelBinder<TModel>
	: BaseModelBinder, IModelBinder<TModel>
{
}

public abstract class BaseModelBinder
#if NETCOREAPP
	: IModelBinder
{
	public async Task BindModelAsync(ModelBindingContext bindingContext)
	{
		var request = bindingContext.HttpContext.Request;
		var method = request.Method.ToLower();

		var data = method switch
		{
			"get" => new BindData(request.Query),
			"post" => new BindData(request.Form),
			_ => throw new Exception($"Unexpected HTTP method: {method}")
		};

		var (model, success) = await ReadModelAsync(data, bindingContext.FieldName, bindingContext.ModelType).NoCtx();
		
		bindingContext.Result = success 
			? ModelBindingResult.Success(model) 
			: ModelBindingResult.Failed();
	}
#else
	: System.Web.Http.ModelBinding.IModelBinder, 
		System.Web.Mvc.IModelBinder,
		System.Web.ModelBinding.IModelBinder
{
	

	public bool BindModel(HttpActionContext actionContext, System.Web.Http.ModelBinding.ModelBindingContext bindingContext)
	{
		var req = actionContext.Request!;
		BindData data;

		if (req.Method == HttpMethod.Get)
			data = new BindData(req.GetQueryNameValuePairs());
		else if (req.Method == HttpMethod.Post)
			data = new BindData(req.Content.ReadAsFormDataAsync().NoCtxResult());
		else
			throw new Exception($"Unexpected HTTP method: {req.Method}");

		var pair = ReadModelAsync(data, null, bindingContext.ModelType).NoCtxResult();

		bindingContext.Model = pair.Item1;
		return pair.Item2;
	}

	public object? BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
	{
		var req = controllerContext.RequestContext.HttpContext.Request;
		var mtd = req.HttpMethod?.ToLower();

		var data = mtd switch
		{
			"get" => new BindData(req.QueryString),
			"post" => new BindData(req.Form),
			_ => throw new Exception($"Unexpected HTTP method: {mtd}")
		};
		var pair = ReadModelAsync(data, null, bindingContext.ModelType).NoCtxResult();

		return pair.Item2 ? pair.Item2 : null;
	}

	public bool BindModel(ModelBindingExecutionContext modelBindingExecutionContext, System.Web.ModelBinding.ModelBindingContext bindingContext)
	{
		var req = modelBindingExecutionContext.HttpContext.Request;
		var mtd = req.HttpMethod?.ToLower();

		var data = mtd switch
		{
			"get" => new BindData(req.QueryString),
			"post" => new BindData(req.Form),
			_ => throw new Exception($"Unexpected HTTP method: {mtd}")
		};
		var pair = ReadModelAsync(data, null, bindingContext.ModelType).NoCtxResult();

		bindingContext.Model = pair.Item1;
		return pair.Item2;
	}

#endif
	protected virtual Task<(object?, bool)> ReadModelAsync(BindData bindData, string? fieldName, TypeEx modelType)
	{
		if (!bindData.TryGetValue(fieldName, out var values))
		{
			Utils.Nop(modelType);
			return Task.FromResult<(object?, bool)>((null, false));
		}

		return ReadModelAsync(values);
	}

	protected virtual Task<(object?, bool)> ReadModelAsync(StringValues values)
		=> ReadModelAsync(values.ToString());
	
	protected virtual Task<(object?, bool)> ReadModelAsync(string value)
	{
		Utils.Nop(value);
		return ModelResult(null, false);
	}

	protected static Task<(object?, bool)> ModelResult(object? model, bool success)
		=> Task.FromResult<(object?, bool)>((model, success));

}