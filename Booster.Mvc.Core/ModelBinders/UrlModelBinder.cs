﻿#nullable enable

namespace Booster.Mvc.ModelBinders;

public class UrlModelBinder : BasicModelBinder<Url>
{
	public UrlModelBinder() : base(1)
	{
	}
}