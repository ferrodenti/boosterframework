﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Web;
using Booster.Interfaces;
using Booster.Reflection;
using Newtonsoft.Json;

#nullable enable

namespace Booster.Mvc.ModelBinders;

public class JsonArrModelBinder
{
	readonly TypeEx _modelType;
	readonly ConcurrentDictionary<string, IDataAccessor?> _accessors = new(StringComparer.OrdinalIgnoreCase);
	readonly HashSet<string> _ignored = new(StringComparer.OrdinalIgnoreCase);

	public JsonArrModelBinder(TypeEx modelType)
	{
		_modelType = modelType;

		foreach (var mem in _modelType.FindDataMembers(
					 new ReflectionFilter
					 {
						 RequiredAttributes = { typeof(JsonPropertyAttribute), typeof(JsonIgnoreAttribute) },
						 Order = ReflectionFilterOrder.DelaringAncestorsToDescendants
					 }))
		{
			if (mem.FindAttribute<JsonIgnoreAttribute>() != null)
				_ignored.Add(mem.Name);

			foreach (var attr in mem.FindAttributes<JsonPropertyAttribute>())
				if (attr.PropertyName != null && !string.Equals(attr.PropertyName, mem.Name, StringComparison.OrdinalIgnoreCase))
				{
					_accessors[attr.PropertyName] = mem;
					_ignored.Add(mem.Name);
				}
		}
	}

	public void BindValue(object model, string key, object? v)
	{
		if (_ignored.Contains(key))
			return;

		var accessor = _accessors.GetOrAdd(key, k => _modelType.FindDataMember(new ReflectionFilter(k) {IgnoreCase = true})) ??
					   throw new Exception($"Could not reflect {_modelType.Name}.{key}");

		string? svalue = null;
		
		if (v != null)
		{
			svalue = v.ToString();

			if (accessor.ValueType == typeof (string))
				svalue = HttpUtility.UrlDecode(svalue); //TODO iconvertiable from
		}

		svalue ??= "";
		
		var value = accessor.ValueType.TypeCode == TypeCode.Object
			? JsonConvert.DeserializeObject(svalue, accessor.ValueType)
			: StringConverter.Default.ToObject(svalue, accessor.ValueType);

		accessor.SetValue(model, value);
	}

	public object CreateModel()
		=> _modelType.Create();
}