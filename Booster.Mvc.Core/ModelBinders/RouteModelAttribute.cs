﻿using System;

namespace Booster.Mvc.ModelBinders;

[AttributeUsage(AttributeTargets.Class)]
public class RouteModelAttribute : Attribute
{
}
