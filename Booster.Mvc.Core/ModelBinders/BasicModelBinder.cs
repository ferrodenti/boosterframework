﻿using System.Linq;
using System.Threading.Tasks;
using Booster.Reflection;
using JetBrains.Annotations;
using Microsoft.Extensions.Primitives;

#nullable enable

namespace Booster.Mvc.ModelBinders;

[PublicAPI]
public class BasicModelBinder<TModel> : BaseModelBinder<TModel>
{
	readonly Constructor _constructor;
	
	public BasicModelBinder(int numArguments)
	{
		var modelType = TypeEx.Get<TModel>();
		var arguments = Enumerable.Range(0, numArguments).Select(_ => typeof(string)).ToArray();
		if (!modelType.TryFindConstructor(new ReflectionFilter
			{
				Parameters = arguments
			}, out _constructor))
			throw new ReflectionException(modelType, $"Unable to find ctor({arguments.ToString(t => t.Name)})");
	}
	
	
	public BasicModelBinder()
	{
		var modelType = TypeEx.Get<TModel>();
		var stringType = TypeEx.Get<string>();
		if (!modelType.TryFindConstructor(new ReflectionFilter
			{
				Predicate = mem =>
				{
					var ctor = (Constructor)mem;
					if (ctor.Parameters.Length < 1)
						return false;

					return ctor.Parameters.All(p => p.ParameterType == stringType);
				}
			}, out _constructor))
			throw new ReflectionException(modelType, "Unable to find ctor(...)");
	}

	protected override Task<(object?, bool)> ReadModelAsync(StringValues values)
	{
		var arguments = new object [_constructor.Parameters.Length];
		var i = 0;
		foreach (var arg in values) 
			arguments[i++] = arg;

		var model = _constructor.Invoke(arguments);
		return ModelResult(model, true);
	}
}
