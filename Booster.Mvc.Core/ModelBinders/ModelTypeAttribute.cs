﻿using System;
using Booster.Reflection;

namespace Booster.Mvc.ModelBinders;

[AttributeUsage(AttributeTargets.Class)]
public class ModelTypeAttribute : Attribute
{
	public TypeEx ModelType { get; }
	public bool Deriveds { get; }
	
	public ModelTypeAttribute(Type modelType, bool deriveds = false)
	{
		ModelType = modelType;
		Deriveds = deriveds;
	}
}