﻿namespace Booster.Mvc.ModelBinders;

public interface IModelBinder<TModel> 
#if NETCOREAPP
	: Microsoft.AspNetCore.Mvc.ModelBinding.IModelBinder
#else
	: System.Web.Mvc.IModelBinder,
		System.Web.ModelBinding.IModelBinder,
		System.Web.Http.ModelBinding.IModelBinder
#endif
{
}
