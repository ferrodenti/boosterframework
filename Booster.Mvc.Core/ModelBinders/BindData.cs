﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using JetBrains.Annotations;
using Microsoft.Extensions.Primitives;

#nullable enable

namespace Booster.Mvc.ModelBinders;

[PublicAPI]
public class BindData : IEnumerable<(string key, StringValues values)>
{
	readonly object _lock = new();
	IDictionary<string, StringValues>? _dictionaryData;
	protected IDictionary<string, StringValues> DictionaryData 
	{
		get
		{
			if (_dictionaryData == null && _reader != null)
				lock (_lock)
				{
					_dictionaryData ??= _reader();
					_reader = null;
				}

			return _dictionaryData ??= new Dictionary<string, StringValues>();
		}
	}


	Func<IDictionary<string, StringValues>>? _reader;

	public BindData(IDictionary<string, StringValues> dictionary)
		=> _dictionaryData = dictionary;

	public BindData(NameValueCollection nameValueCollection, IEqualityComparer<string>? equalityComparer = null)
		=> _reader = () =>
		{
			var result = new Dictionary<string, StringValues>(equalityComparer ?? EqualityComparer<string>.Default);

			foreach (var key in nameValueCollection.AllKeys) 
				result[key!] = nameValueCollection[key];

			return result;

		};
	
	public BindData(IEnumerable<KeyValuePair<string, StringValues>> pairs, IEqualityComparer<string>? equalityComparer = null)
		=> _reader = () =>
		{
			var result = new Dictionary<string, StringValues>(equalityComparer ?? EqualityComparer<string>.Default);

			foreach (var pair in pairs)
				result[pair.Key] = pair.Value;

			return result;
		};
	
	public BindData(IEnumerable<KeyValuePair<string, string>> pairs, IEqualityComparer<string>? equalityComparer = null)
		: this(pairs.Select(p => new KeyValuePair<string, StringValues>(p.Key, p.Value)), equalityComparer)
	{
	}
	
	

	public bool TryGetValue(string? value, out StringValues result)
	{
		if (value.IsSome())
			return DictionaryData.TryGetValue(value, out result);

		result = default;
		return false;
	}

	public IEnumerator<(string key, StringValues values)> GetEnumerator()
		=> DictionaryData.Select(p => (p.Key, p.Value)).GetEnumerator();

	IEnumerator IEnumerable.GetEnumerator()
		=> GetEnumerator();
}