﻿using System;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.Extensions.Primitives;

#nullable enable

namespace Booster.Mvc.ModelBinders;

[PublicAPI]
public class LambdaModelBinder<TModel> : BaseModelBinder<TModel>
{
	readonly Func<StringValues, TModel> _creator;
	
	public LambdaModelBinder(Func<StringValues, TModel> creator)
		=> _creator = creator;

	public LambdaModelBinder(Func<string, TModel> creator)
		=> _creator = v => creator(v.ToString());
	
	protected override Task<(object?, bool)> ReadModelAsync(StringValues values)
	{
		var model = _creator(values);
		return ModelResult(model, true);
	}
}
