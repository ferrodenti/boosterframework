﻿using System.Collections.Concurrent;
using System.Threading.Tasks;
using Booster.Reflection;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Mvc.ModelBinders;

[PublicAPI]
public class GetPostJsonBinder : BaseModelBinder
{
	readonly ConcurrentDictionary<TypeEx, JsonArrModelBinder> _binders = new();

	protected override Task<(object?, bool)> ReadModelAsync(BindData bindData, string? fieldName, TypeEx modelType)
	{
		var binder = _binders.GetOrAdd(modelType, m => new JsonArrModelBinder(m));
		var model = binder.CreateModel();

		foreach (var (key, value) in bindData)
			binder.BindValue(model, key, value);

		return ModelResult(model, true);
	}
}