using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Booster.DI;
using Booster.Helpers;
using Booster.Mvc.Helpers;
using JetBrains.Annotations;

using static Booster.FastLazyStatic;

#if NETSTANDARD || NETCOREAPP
using Microsoft.AspNetCore.Html;
#else
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
#endif

#nullable enable

namespace Booster.Mvc;

public class Menu
{
	Menu? _parent;

	public string? Text { get; set; }
	public string? AltActiveRegex { get; set; }
	public object? AAttrs { get; set; }
	public object? LiAttrs { get; set; }
	public Func<Menu, bool>? Condition { get; set; }

	[PublicAPI]
	public bool PermissionsInheritance { get; set; } = true;

	bool _isSeparator;

	string? _action;
	public string? Action
	{
		[AspMvcAction] 
		get => _action;
		set
		{
			if (_action != value)
			{
				_url = null;
				_action = value;
			}
		}
	}

	object? _routeValues;
	public object? RouteValues
	{
		get => _routeValues;
		set
		{
			if (_routeValues != value)
			{
				_url = null;
				_routeValues = value;
			}
		}
	}


	FastLazy<int>? _level;
	protected int Level => _level ??= _parent.With(p => p.Level + 1);

	public static Menu Separator => new() { _isSeparator = true };

	FastLazy<string?>? _controller;
	public string? Controller
	{
		[AspMvcController]
		get => _controller ??= _parent?.Controller;
		set
		{
			_url = null;
			_permission = null;
			_controller = value;
		}
	}

	FastLazy<string?>? _area;
	public string? Area
	{
		get => _area ??= _parent?.Area;
		set
		{
			_url = null;
			_area = value;
		}
	}


	readonly SyncLazy<Regex?> _altActive = SyncLazy<Regex?>.Create<Menu>(
		self => self.AltActiveRegex.IsSome() 
			? Utils.CompileRegex(self.AltActiveRegex!) 
			: null);

	protected Regex? AltActive => _altActive.GetValue(this);


	FastLazy<ObservableCollection<Menu>>? _sub;

	public ObservableCollection<Menu> Sub => _sub ??= FastLazy(() =>
	{
		var result = new ObservableCollection<Menu>();
		result.CollectionChanged += (_, e) =>
		{
			if(e.NewItems != null)
				foreach (Menu item in e.NewItems)
					item._parent = this;
		};
		return result;
	});


	FastLazy<Url?>? _url;

	public Url? Url => _url ??= FastLazy<Url?>(() =>
	{
		if (Action == null || Controller == null)
			return null;

		if (RouteValues != null)
		{
			dynamic rv = new ExpandoObject();
			IDictionary<string, object?> dict = rv;
			foreach (var pair in AnonTypeHelper.ReadProperties(RouteValues))
				dict[pair.Key] = pair.Value;

			dict["Area"] = Area;
			return new Url(MvcHelper.UrlAction(Action, Controller, rv));
		}
			
		return MvcHelper.UrlAction(Action, Controller, new { Area });
	});


	FastLazy<object?>? _permission;

	public object? Permission
	{
		get => (_permission ??= FastLazy<object?>(() =>
		{
			if (Action == null || Controller == null)
				return null;

#if !NETSTANDARD && !NETCOREAPP //TODO: Menu.Permission std implementation
			try
			{
				var permissions = new List<object>();

				var httpContext = HttpContext.Current.Request.RequestContext.HttpContext;
				var controllerFactory = ControllerBuilder.Current.GetControllerFactory();

				var routeData = new RouteData
				{
					Values =
					{
						["Area"] = Area
					}
				};
				var otherController =
					(ControllerBase)controllerFactory.CreateController(new RequestContext(httpContext, routeData),
						Controller);
				var controllerDescriptor = new ReflectedControllerDescriptor(otherController.GetType());

				foreach (AccessAttribute attr in
				         controllerDescriptor.GetCustomAttributes(typeof(AccessAttribute), true))
					permissions.AddRange(attr.RequiredPermissions);

				if (Action != null)
					try
					{
						var controllerContext2 = new ControllerContext(
							new MockHttpContextWrapper(httpContext.ApplicationInstance.Context, "GET"), routeData,
							otherController);
						var actionDescriptor = controllerDescriptor.FindAction(controllerContext2, Action);

						if (actionDescriptor == null)
						{
							controllerContext2 = new ControllerContext(
								new MockHttpContextWrapper(httpContext.ApplicationInstance.Context, "POST"),
								routeData,
								otherController);
							actionDescriptor = controllerDescriptor.FindAction(controllerContext2, Action);
						}

						foreach (AccessAttribute attr in actionDescriptor.GetCustomAttributes(
							         typeof(AccessAttribute),
							         true))
							permissions.AddRange(attr.RequiredPermissions);
					}
					catch (HttpException)
					{
					}

				return permissions.Count switch
				{
					0 => true,
					1 => permissions.First(),
					_ => permissions
				};
			}
			catch (HttpException)
			{
				return true;
			}
#else
			return null;
#endif
		})).Value;
		set
		{
			_permission = value;
		}
	}

#if !NETSTANDARD && !NETCOREAPP
	class MockHttpContextWrapper : HttpContextWrapper
	{
		public override HttpRequestBase Request { get; }

		public MockHttpContextWrapper(HttpContext httpContext, string method)
			: base(httpContext) => Request = new MockHttpRequestWrapper(httpContext.Request, method);

		class MockHttpRequestWrapper : HttpRequestWrapper
		{
			public MockHttpRequestWrapper(HttpRequest httpRequest, string httpMethod)
				: base(httpRequest) => HttpMethod = httpMethod;

			public override string HttpMethod { get; }
		}
	}
#endif

	FastLazy<Dictionary<string, object?>>? _aAttrsDict;

	protected Dictionary<string, object?> AAttrsDict => _aAttrsDict ??= FastLazy(() =>
	{
		var dict = new Dictionary<string, object?>();

		if (Url?.IsSome == true)
			dict["href"] = Url.ToString();
		else
			dict["href"] = "#";

		if (Sub.Count > 0 && Level <= 1)
		{
			dict["class"] = "dropdown-toggle";
			dict["data-toggle"] = "dropdown";
			dict["aria-haspopup"] = "true";
			dict["aria-expanded"] = "false";
		}

		ExpandAttrs(dict, AAttrs);
		return dict;
	});

	Dictionary<string, object?>? _liAttrsDict;
	protected Dictionary<string, object?> LiAttrsDict
	{
		get
		{
			if (_liAttrsDict == null)
			{
				var dict = new Dictionary<string, object?>();

				if (Sub.Count > 0)
				{
					dict["class"] = Level <= 1 ? "dropdown" : "dropdown-submenu";
					dict["aria-haspopup"] = "true";
					dict["aria-expanded"] = "false";
				}

				ExpandAttrs(dict, LiAttrs);

				_liAttrsDict = dict;
			}

			return _liAttrsDict;
		}
	}


	static void ExpandAttrs(IDictionary<string, object?> dict, object? obj)
	{
		if (obj != null)
			foreach (var pair in AnonTypeHelper.ReadProperties(obj))
			{
				var key = pair.Key.SeparateCamelCase(false, "-").ToLower();
				var value = pair.Value;
				if (value != null)
				{
					if (value is Delegate)
						dict[key] = value;
					else
						dict[key] = StringConverter.Default.ToString(value);
				}
				else
					dict.Remove(key);
			}
	}


	public Menu(string text, string area, [AspMvcController] string controller, [AspMvcAction] string action, params Menu[] sub)
	{
		Text = text;
		Controller = controller;
		Action = action;
		Area = area;
		Sub.AddRange(sub);
	}

	public Menu(string text, [AspMvcController] string controller, [AspMvcAction] string action, params Menu[] sub)
	{
		Text = text;
		Controller = controller;
		Action = action;
		Sub.AddRange(sub);
	}

	public Menu(string text, string action)
	{
		Text = text;
		Action = action;
	}

	public Menu(string text, [AspMvcController] string controller, params Menu[] sub)
	{
		Text = text;
		Controller = controller;
		Sub.AddRange(sub);
	}

	public Menu(string text, params Menu[] sub)
	{
		Text = text;
		Sub.AddRange(sub);
	}

	public Menu(params Menu[] sub)
		=> Sub.AddRange(sub);

	public async Task<HtmlString> Render()
	{
		var currentUrl = MvcHelper.DisplayUri!;

		var str = new StringBuilder();

		var permissionsProvider = InstanceFactory.Get<IPermissionsProvider>(false);
		object?userPermissions = null;
		if (permissionsProvider != null)
			userPermissions = await permissionsProvider.GetUserPermissions().NoCtx();

		foreach (var sub in Sub)
			str.Append(sub.RenderInt(permissionsProvider, userPermissions, currentUrl, 0, out _));

		return new HtmlString(str.ToString());
	}

	protected string? RenderInt(IPermissionsProvider? permissionsProvider, object? userPermissions, string currentUrl, int level, out bool active)
	{
		if (_isSeparator)
		{
			active = false;
			return "<li role='separator' class='divider'></li>";
		}

		active = currentUrl.EndsWithIgnoreCase(Url ?? "");

		if (!active && AltActive != null)
			active = AltActive.IsMatch(currentUrl);

		if (Condition != null)
		{
			if (!Condition(this))
				return null;
		}
		else if (permissionsProvider?.ComparePermisssions(Permission, userPermissions) == false)
			return null;

		var aattrs = CreateAttrs(AAttrsDict);
		string? liattrs = null;

		if (Sub.Count > 0)
		{
			var subs = new EnumBuilder("", "<ul class='dropdown-menu'>", "</ul>");
			var fromSepa = 0;
			var needSepa = false;

			foreach (var menu in Sub)
			{
				if (menu._isSeparator)
				{
					if (fromSepa > 0)
						needSepa = true;

					continue;
				}

				var html = menu.RenderInt(permissionsProvider, userPermissions, currentUrl, level + 1, out var s);
				if (html != null)
				{
					if (needSepa)
					{
						needSepa = false;
						subs.Append("<li role='separator' class='divider'></li>");
						fromSepa = 0;
					}

					active |= s;
					subs.Append(html);
					fromSepa++;
				}
			}

			liattrs = CreateAttrs(LiAttrsDict, active ? "active" : "");

			if (subs.IsSome)
			{
				var text = Level == 1 ? $"{Text} <span class='caret'></span>" : Text;
				return $"<li{liattrs}><a{aattrs}>{text}</a>{subs}</li>";
			}

			if (Url == null)
				return null;
		}

		return $"<li{liattrs ?? CreateAttrs(LiAttrsDict, active ? "active" : "")}><a{aattrs}>{Text}</a></li>";
	}

	static string CreateAttrs(Dictionary<string, object?> dict, string? addClass = null)
	{
		var res = new EnumBuilder(" ", " ");
		foreach (var pair in dict)
		{
			string? value;
			if (pair.Value is Delegate d)
				value = StringConverter.Default.ToString(d.DynamicInvoke());
			else
				value = (string?)pair.Value;

			if (addClass != null && pair.Key == "class")
			{
				value = value.IsSome() ? $"{value} {addClass}" : addClass;
				addClass = null;
			}

			res.Append($"{pair.Key}=\"{value}\"");
		}

		if (addClass != null)
			res.Append($"class=\"{addClass}\"");

		return res;
	}
}