﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;

namespace Booster.Mvc.Compression;

public sealed class HtmlCompressor : ICompressor
{
	//compiled regex patterns
	const RegexOptions _compiled = RegexOptions.Compiled; // regex compilation could be globaly disabled
	const RegexOptions _lineNoCase = RegexOptions.Singleline | RegexOptions.IgnoreCase | _compiled;
	const RegexOptions _noCase = RegexOptions.IgnoreCase | _compiled;
	const RegexOptions _line = RegexOptions.Singleline | _compiled;
		
	public static readonly Regex PhpTagPattern = new("<\\?php.*?\\?>", _lineNoCase);
	public static readonly Regex ServerScriptTagPattern = new("<%.*?%>", _line);
	public static readonly Regex ServerSideIncludePattern = new("<!--\\s*#.*?-->", _line);

	static readonly Regex _emptyPattern = new("\\s", _compiled);
	static readonly Regex _skipPattern = new("<!--\\s*\\{\\{\\{\\s*-->(.*?)<!--\\s*\\}\\}\\}\\s*-->", _lineNoCase);
	static readonly Regex _condCommentPattern = new("(<!(?:--)?\\[[^\\]]+?]>)(.*?)(<!\\[[^\\]]+]-->)", _lineNoCase);
	static readonly Regex _commentPattern = new("<!---->|<!--[^\\[].*?-->", _lineNoCase);
	static readonly Regex _intertagPatternTagTag = new(">\\s+<", _lineNoCase);
	static readonly Regex _intertagPatternTagCustom = new(">\\s+%%%~", _lineNoCase);
	static readonly Regex _intertagPatternCustomTag = new("~%%%\\s+<", _lineNoCase);
	static readonly Regex _intertagPatternCustomCustom = new("~%%%\\s+%%%~", _lineNoCase);
	static readonly Regex _multispacePattern = new("\\s+", _lineNoCase);
	static readonly Regex _tagEndSpacePattern = new("(<(?:[^>]+?))(?:\\s+?)(/?>)", _lineNoCase);
	static readonly Regex _tagLastUnquotedValuePattern = new("=\\s*[a-z0-9-_]+$", _noCase);
	static readonly Regex _tagQuotePattern = new("\\s*=\\s*([\"'])([a-z0-9-_]+?)\\1(/?)(?=[^<]*?>)", _noCase);
	static readonly Regex _prePattern = new("(<pre[^>]*?>)(.*?)(</pre>)", _lineNoCase);
	static readonly Regex _taPattern = new("(<textarea[^>]*?>)(.*?)(</textarea>)", _lineNoCase);
	static readonly Regex _scriptPattern = new("(<script[^>]*?>)(.*?)(</script>)", _lineNoCase);
	static readonly Regex _stylePattern = new("(<style[^>]*?>)(.*?)(</style>)", _lineNoCase);
	static readonly Regex _tagPropertyPattern = new("(\\s\\w+)\\s*=\\s*(?=[^<]*?>)", _noCase);
	static readonly Regex _cdataPattern = new("\\s*<!\\[CDATA\\[(.*?)\\]\\]>\\s*", _lineNoCase);
	static readonly Regex _scriptCdataPattern = new("/\\*\\s*<!\\[CDATA\\[\\*/(.*?)/\\*\\]\\]>\\s*\\*/", _lineNoCase);
	static readonly Regex _doctypePattern = new("<!DOCTYPE[^>]*>", _lineNoCase);
	static readonly Regex _typeAttrPattern = new("type\\s*=\\s*([\\\"']*)(.+?)\\1", _lineNoCase);
	static readonly Regex _jsTypeAttrPattern = new("(<script[^>]*)type\\s*=\\s*([\"']*)(?:text|application)/javascript\\2([^>]*>)", _lineNoCase);
	static readonly Regex _jsLangAttrPattern = new("(<script[^>]*)language\\s*=\\s*([\"']*)javascript\\2([^>]*>)", _lineNoCase);
	static readonly Regex _styleTypeAttrPattern = new("(<style[^>]*)type\\s*=\\s*([\"']*)text/style\\2([^>]*>)", _lineNoCase);
	static readonly Regex _linkTypeAttrPattern = new("(<link[^>]*)type\\s*=\\s*([\"']*)text/(?:css|plain)\\2([^>]*>)", _lineNoCase);
	static readonly Regex _linkRelAttrPattern = new("<link(?:[^>]*)rel\\s*=\\s*([\"']*)(?:alternate\\s+)?stylesheet\\1(?:[^>]*)>", _lineNoCase);
	static readonly Regex _formMethodAttrPattern = new("(<form[^>]*)method\\s*=\\s*([\"']*)get\\2([^>]*>)", _lineNoCase);
	static readonly Regex _inputTypeAttrPattern = new("(<input[^>]*)type\\s*=\\s*([\"']*)text\\2([^>]*>)", _lineNoCase);
	static readonly Regex _booleanAttrPattern = new("(<\\w+[^>]*)(checked|selected|disabled|readonly)\\s*=\\s*([\"']*)\\w*\\3([^>]*>)", _lineNoCase);
	static readonly Regex _eventJsProtocolPattern = new("^javascript:\\s*(.+)", _lineNoCase);
	static readonly Regex _httpProtocolPattern = new("(<[^>]+?(?:href|src|cite|action)\\s*=\\s*['\"])http:(//[^>]+?>)", _lineNoCase);
	static readonly Regex _httpsProtocolPattern = new("(<[^>]+?(?:href|src|cite|action)\\s*=\\s*['\"])https:(//[^>]+?>)", _lineNoCase);
	static readonly Regex _relExternalPattern = new("<(?:[^>]*)rel\\s*=\\s*([\"']*)(?:alternate\\s+)?external\\1(?:[^>]*)>", _lineNoCase);
	static readonly Regex _eventPattern1 = new("(\\son[a-z]+\\s*=\\s*\")([^\"\\\\\\r\\n]*(?:\\\\.[^\"\\\\\\r\\n]*)*)(\")", _noCase);
	//unmasked: \son[a-z]+\s*=\s*"[^"\\\r\n]*(?:\\.[^"\\\r\n]*)*"

	static readonly Regex _eventPattern2 = new("(\\son[a-z]+\\s*=\\s*')([^'\\\\\\r\\n]*(?:\\\\.[^'\\\\\\r\\n]*)*)(')", _noCase);
	static readonly Regex _lineBreakPattern = new("(?:[ \t]*(\\r?\\n)[ \t]*)+", _compiled);


	Regex _surroundingSpacesPattern;
	static readonly Regex _surroundingSpacesAllPattern = new("\\s*(<[^>]+>)\\s*", _lineNoCase);

	//patterns for searching for temporary replacements
	static readonly Regex _tempCondCommentPattern = new("%%%~COMPRESS~COND~(\\d+?)~%%%", _compiled);
	static readonly Regex _tempPrePattern = new("%%%~COMPRESS~PRE~(\\d+?)~%%%", _compiled);
	static readonly Regex _tempTextAreaPattern = new("%%%~COMPRESS~TEXTAREA~(\\d+?)~%%%", _compiled);
	static readonly Regex _tempScriptPattern = new("%%%~COMPRESS~SCRIPT~(\\d+?)~%%%", _compiled);
	static readonly Regex _tempStylePattern = new("%%%~COMPRESS~STYLE~(\\d+?)~%%%", _compiled);
	static readonly Regex _tempEventPattern = new("%%%~COMPRESS~EVENT~(\\d+?)~%%%", _compiled);
	static readonly Regex _tempSkipPattern = new("%%%~COMPRESS~SKIP~(\\d+?)~%%%", _compiled);
	static readonly Regex _tempLineBreakPattern = new("%%%~COMPRESS~LT~(\\d+?)~%%%", _compiled);
		
	public const string BlockTagsMin = "html,head,body,br,p";
	public const string BlockTagsMax =
		$"{BlockTagsMin},h1,h2,h3,h4,h5,h6,blockquote,center,dl,fieldset,form,frame,frameset,hr,noframes,ol,table,tbody,tr,td,th,tfoot,thead,ul";
	public const string AllTags = "all";
		
	public List<Regex> PreservePatterns { get; private set; } = new();
	public ICompressor JavaScriptCompressor { get; set; }
	public ICompressor CssCompressor { get; set; }

	public HtmlCompressorOptions Options { get; set; }
		
	string _removeSurroundingSpacesTags;
	public string RemoveSurroundingSpacesTags
	{
		get => _removeSurroundingSpacesTags;
		set
		{
			if (_removeSurroundingSpacesTags != value)
			{
				_surroundingSpacesPattern = null;
				_removeSurroundingSpacesTags = value;
			}
		}
	}

	public HtmlCompressor(HtmlCompressorOptions options = HtmlCompressorOptions.Default) 
		=> Options = options;

	public string Compress(string html, out HtmlCompressorStatistics statistics)
	{
		statistics = new HtmlCompressorStatistics
		{
			OriginalMetrics =
			{
				FileSize = html.Length,
				EmptyChars = _emptyPattern.Matches(html).Count
			}
		};
		var stopWatch = new Stopwatch();
		stopWatch.Start();
		var result = Compress(html, statistics);
		stopWatch.Stop();
			
		statistics.Time = stopWatch.ElapsedTicks;
		statistics.CompressedMetrics.FileSize = html.Length;
		statistics.CompressedMetrics.EmptyChars =  _emptyPattern.Matches(html).Count; //calculate number of empty chars

		return result;
	}

	public string Compress(string html) 
		=> Compress(html, null);

	public string Compress(string html, HtmlCompressorStatistics statistics)
	{
		if (!Options.HasFlag(HtmlCompressorOptions.Enabled) || string.IsNullOrEmpty(html))
			return html;

		var condCommentBlocks = new List<string>(); //preserved block containers
		var preBlocks = new List<string>();
		var taBlocks = new List<string>();
		var scriptBlocks = new List<string>();
		var styleBlocks = new List<string>();
		var eventBlocks = new List<string>();
		var skipBlocks = new List<string>();
		var lineBreakBlocks = new List<string>();
		var userBlocks = new List<List<string>>();

		//preserve blocks
		html = PreserveBlocks(html, preBlocks, taBlocks, scriptBlocks, styleBlocks, eventBlocks, condCommentBlocks, skipBlocks, lineBreakBlocks, userBlocks);

		//process pure html
		html = ProcessHtml(html);

		//process preserved blocks
		ProcessPreservedBlocks(preBlocks, taBlocks, scriptBlocks, styleBlocks, eventBlocks, condCommentBlocks, skipBlocks, lineBreakBlocks, userBlocks, statistics);

		//put preserved blocks back
		html = ReturnBlocks(html, preBlocks, taBlocks, scriptBlocks, styleBlocks, eventBlocks, condCommentBlocks, skipBlocks, lineBreakBlocks, userBlocks);

		return html;
	}

	string PreserveBlocks(
		string html,
		ICollection<string> preBlocks,
		ICollection<string> taBlocks,
		ICollection<string> scriptBlocks,
		ICollection<string> styleBlocks,
		ICollection<string> eventBlocks,
		ICollection<string> condCommentBlocks,
		ICollection<string> skipBlocks, 
		ICollection<string> lineBreakBlocks,
		ICollection<List<string>> userBlocks)
	{
		for (int p = 0, len = PreservePatterns.Count; p < len; p++) //preserve user blocks
		{
			var userBlock = new List<string>();

			var matches = PreservePatterns[p].Matches(html);
			var index = 0;
			var sb = new StringBuilder();
			var lastValue = 0;

			foreach (Match match in matches)
				if (match.Groups[0].Value.Trim().Length > 0)
				{
					userBlock.Add(match.Groups[0].Value);

					sb.Append(html.Substring(lastValue, match.Index - lastValue));
					sb.Append(match.Result($"%%%~COMPRESS~USER{p}~{index++}~%%%"));

					lastValue = match.Index + match.Length;
				}

			sb.Append(html.Substring(lastValue));

			html = sb.ToString();
			userBlocks.Add(userBlock);
		}
			
		var skipBlockIndex = 0;

		{	//preserve <!-- {{{ ---><!-- }}} ---> skip blocks
			var matcher = _skipPattern.Matches(html);
			var sb = new StringBuilder();
			var lastValue = 0;

			foreach (Match match in matcher)
				if (match.Groups[1].Value.Trim().Length > 0)
				{
					skipBlocks.Add(match.Groups[1].Value);

					sb.Append(html.Substring(lastValue, match.Index - lastValue));
					sb.Append(match.Result($"%%%~COMPRESS~SKIP~{skipBlockIndex++}~%%%"));

					lastValue = match.Index + match.Length;
				}

			sb.Append(html.Substring(lastValue));

			html = sb.ToString();
		}
		{	//preserve conditional comments
			var condCommentCompressor = Clone();
			var matcher = _condCommentPattern.Matches(html);
			var index = 0;
			var sb = new StringBuilder();
			var lastValue = 0;

			foreach (Match match in matcher)
				if (match.Groups[2].Value.Trim().Length > 0)
				{
					condCommentBlocks.Add(
						match.Groups[1].Value + condCommentCompressor.Compress(match.Groups[2].Value) + match.Groups[3].Value);

					sb.Append(html.Substring(lastValue, match.Index - lastValue));
					sb.Append(match.Result($"%%%~COMPRESS~COND~{index++}~%%%"));

					lastValue = match.Index + match.Length;
				}

			sb.Append(html.Substring(lastValue));

			html = sb.ToString();
		}
		{	//preserve inline events
			var matcher = _eventPattern1.Matches(html);
			var index = 0;
			var sb = new StringBuilder();
			var lastValue = 0;

			foreach (Match match in matcher)
				if (match.Groups[2].Value.Trim().Length > 0)
				{
					eventBlocks.Add(match.Groups[2].Value);

					sb.Append(html.Substring(lastValue, match.Index - lastValue));
					sb.Append(match.Result($"$1%%%~COMPRESS~EVENT~{index++}~%%%$3"));

					lastValue = match.Index + match.Length;
				}

			sb.Append(html.Substring(lastValue));

			html = sb.ToString();
		}
		{
			var matcher = _eventPattern2.Matches(html);
			var index = 0;
			var sb = new StringBuilder();
			var lastValue = 0;

			foreach (Match match in matcher)
				if (match.Groups[2].Value.Trim().Length > 0)
				{
					eventBlocks.Add(match.Groups[2].Value);

					sb.Append(html.Substring(lastValue, match.Index - lastValue));
					sb.Append(match.Result($"$1%%%~COMPRESS~EVENT~{index++}~%%%$3"));

					lastValue = match.Index + match.Length;
				}

			sb.Append(html.Substring(lastValue));

			html = sb.ToString();
		}
		{	//preserve PRE tags
			var matcher = _prePattern.Matches(html);
			var index = 0;
			var sb = new StringBuilder();
			var lastValue = 0;

			foreach (Match match in matcher)
				if (match.Groups[2].Value.Trim().Length > 0)
				{
					preBlocks.Add(match.Groups[2].Value);

					sb.Append(html.Substring(lastValue, match.Index - lastValue));
					sb.Append(match.Result($"$1%%%~COMPRESS~PRE~{index++}~%%%$3"));

					lastValue = match.Index + match.Length;
				}

			sb.Append(html.Substring(lastValue));

			html = sb.ToString();
		}
		{	//preserve SCRIPT tags
			var matcher = _scriptPattern.Matches(html);
			var index = 0;
			var sb = new StringBuilder();
			var lastValue = 0;

			foreach (Match match in matcher) //ignore empty scripts
					
				if (match.Groups[2].Value.Trim().Length > 0)
				{
					var type = ""; //check type
					var typeMatcher = _typeAttrPattern.Match(match.Groups[1].Value);
					if (typeMatcher.Success)
						type = typeMatcher.Groups[2].Value.ToLowerInvariant();

					if (type.Length == 0 || type.Equals("text/javascript") || type.Equals("application/javascript"))
					{
						scriptBlocks.Add(match.Groups[2].Value); //javascript block, preserve and compress with js compressor

						sb.Append(html.Substring(lastValue, match.Index - lastValue));
						sb.Append(match.Result($"$1%%%~COMPRESS~SCRIPT~{index++}~%%%$3"));

						lastValue = match.Index + match.Length;
					}
					else if (!type.Equals("text/x-jquery-tmpl")) //some custom script, preserve it inside "skip blocks" so it won't be compressed with js compressor 
					{
						skipBlocks.Add(match.Groups[2].Value);

						sb.Append(html.Substring(lastValue, match.Index - lastValue));
						sb.Append(match.Result($"$1%%%~COMPRESS~SKIP~{skipBlockIndex++}~%%%$3"));

						lastValue = match.Index + match.Length;
					}
				}

			sb.Append(html.Substring(lastValue));

			html = sb.ToString();
		}
		{	//preserve STYLE tags
			var matcher = _stylePattern.Matches(html);
			var index = 0;
			var sb = new StringBuilder();
			var lastValue = 0;

			foreach (Match match in matcher)
				if (match.Groups[2].Value.Trim().Length > 0)
				{
					styleBlocks.Add(match.Groups[2].Value);

					sb.Append(html.Substring(lastValue, match.Index - lastValue));
					sb.Append(match.Result($"$1%%%~COMPRESS~STYLE~{index++}~%%%$3"));

					lastValue = match.Index + match.Length;
				}

			sb.Append(html.Substring(lastValue));

			html = sb.ToString();
		}
		{	//preserve TEXTAREA tags
			var matcher = _taPattern.Matches(html);
			var index = 0;
			var sb = new StringBuilder();
			var lastValue = 0;

			foreach (Match match in matcher)
				if (match.Groups[2].Value.Trim().Length > 0)
				{
					taBlocks.Add(match.Groups[2].Value);

					sb.Append(html.Substring(lastValue, match.Index - lastValue));
					sb.Append(match.Result($"$1%%%~COMPRESS~TEXTAREA~{index++}~%%%$3"));

					lastValue = match.Index + match.Length;
				}

			sb.Append(html.Substring(lastValue));

			html = sb.ToString();
		}
		if (Options.HasFlag(HtmlCompressorOptions.PreserveLineBreaks)) //preserve line breaks
		{
			var matcher = _lineBreakPattern.Matches(html);
			var index = 0;
			var sb = new StringBuilder();
			var lastValue = 0;

			foreach (Match match in matcher)
			{
				lineBreakBlocks.Add(match.Groups[1].Value);

				sb.Append(html.Substring(lastValue, match.Index - lastValue));
				sb.Append(match.Result($"%%%~COMPRESS~LT~{index++}~%%%"));

				lastValue = match.Index + match.Length;
			}

			sb.Append(html.Substring(lastValue));

			html = sb.ToString();
		}

		return html;
	}

	string ReturnBlocks(
		string html,
		IReadOnlyList<string> preBlocks,
		IReadOnlyList<string> taBlocks,
		IReadOnlyList<string> scriptBlocks,
		IReadOnlyList<string> styleBlocks,
		IReadOnlyList<string> eventBlocks,
		IReadOnlyList<string> condCommentBlocks,
		IReadOnlyList<string> skipBlocks,
		IReadOnlyList<string> lineBreakBlocks,
		IReadOnlyList<List<string>> userBlocks)
	{
		if (Options.HasFlag(HtmlCompressorOptions.PreserveLineBreaks)) //put line breaks back
		{
			var matcher = _tempLineBreakPattern.Matches(html);
			var sb = new StringBuilder();
			var lastValue = 0;

			foreach (Match match in matcher)
			{
				var i = int.Parse(match.Groups[1].Value);
				if (lineBreakBlocks.Count > i)
				{
					sb.Append(html.Substring(lastValue, match.Index - lastValue));
					sb.Append(lineBreakBlocks[i]);

					lastValue = match.Index + match.Length;
				}
			}
			sb.Append(html.Substring(lastValue));

			html = sb.ToString();
		}
		{	//put TEXTAREA blocks back
			var matcher = _tempTextAreaPattern.Matches(html);
			var sb = new StringBuilder();
			var lastValue = 0;

			foreach (Match match in matcher)
			{
				var i = int.Parse(match.Groups[1].Value);
				if (taBlocks.Count > i)
				{
					sb.Append(html.Substring(lastValue, match.Index - lastValue));
					sb.Append(taBlocks[i]);

					lastValue = match.Index + match.Length;
				}
			}
			sb.Append(html.Substring(lastValue));

			html = sb.ToString();
		}
		{	//put STYLE blocks back
			var matcher = _tempStylePattern.Matches(html);
			var sb = new StringBuilder();
			var lastValue = 0;

			foreach (Match match in matcher)
			{
				var i = int.Parse(match.Groups[1].Value);
				if (styleBlocks.Count > i)
				{
					sb.Append(html.Substring(lastValue, match.Index - lastValue));
					sb.Append(styleBlocks[i]);

					lastValue = match.Index + match.Length;
				}
			}
			sb.Append(html.Substring(lastValue));

			html = sb.ToString();
		}
		{	//put SCRIPT blocks back
			var matcher = _tempScriptPattern.Matches(html);
			var sb = new StringBuilder();
			var lastValue = 0;

			foreach (Match match in matcher)
			{
				var i = int.Parse(match.Groups[1].Value);
				if (scriptBlocks.Count > i)
				{
					sb.Append(html.Substring(lastValue, match.Index - lastValue));
					sb.Append(scriptBlocks[i]);

					lastValue = match.Index + match.Length;
				}
			}
			sb.Append(html.Substring(lastValue));

			html = sb.ToString();
		}
		{	//put PRE blocks back
			var matcher = _tempPrePattern.Matches(html);
			var sb = new StringBuilder();
			var lastValue = 0;

			foreach (Match match in matcher)
			{
				var i = int.Parse(match.Groups[1].Value);
				if (preBlocks.Count > i)
				{
					sb.Append(html.Substring(lastValue, match.Index - lastValue));
					sb.Append(preBlocks[i]);

					lastValue = match.Index + match.Length;
				}
			}
			sb.Append(html.Substring(lastValue));

			html = sb.ToString();
		}
		{	//put event blocks back
			var matcher = _tempEventPattern.Matches(html);
			var sb = new StringBuilder();
			var lastValue = 0;

			foreach (Match match in matcher)
			{
				var i = int.Parse(match.Groups[1].Value);
				if (eventBlocks.Count > i)
				{
					sb.Append(html.Substring(lastValue, match.Index - lastValue));
					sb.Append(eventBlocks[i]);

					lastValue = match.Index + match.Length;
				}
			}
			sb.Append(html.Substring(lastValue));

			html = sb.ToString();
		}
		{	//put conditional comments back
			var matcher = _tempCondCommentPattern.Matches(html);
			var sb = new StringBuilder();
			var lastValue = 0;

			foreach (Match match in matcher)
			{
				var i = int.Parse(match.Groups[1].Value);
				if (condCommentBlocks.Count > i)
				{
					sb.Append(html.Substring(lastValue, match.Index - lastValue));
					sb.Append(condCommentBlocks[i]);

					lastValue = match.Index + match.Length;
				}
			}
			sb.Append(html.Substring(lastValue));

			html = sb.ToString();
		}
		{	//put skip blocks back
			var matcher = _tempSkipPattern.Matches(html);
			var sb = new StringBuilder();
			var lastValue = 0;

			foreach (Match match in matcher)
			{
				var i = int.Parse(match.Groups[1].Value);
				if (skipBlocks.Count > i)
				{
					sb.Append(html.Substring(lastValue, match.Index - lastValue));
					sb.Append(skipBlocks[i]);

					lastValue = match.Index + match.Length;
				}
			}
			sb.Append(html.Substring(lastValue));

			html = sb.ToString();
		}

		for (var p = PreservePatterns.Count - 1; p >= 0; p--)	//put user blocks back
		{
			var tempUserPattern = new Regex($"%%%~COMPRESS~USER{p}~(\\d+?)~%%%", _compiled);
			var matcher = tempUserPattern.Matches(html);
			var sb = new StringBuilder();
			var lastValue = 0;

			foreach (Match match in matcher)
			{
				var i = int.Parse(match.Groups[1].Value);
				if (userBlocks.Count > p && userBlocks[p].Count > i)
				{
					sb.Append(html.Substring(lastValue, match.Index - lastValue));
					sb.Append(userBlocks[p][i]);

					lastValue = match.Index + match.Length;
				}
			}
			sb.Append(html.Substring(lastValue));

			html = sb.ToString();
		}

		return html;
	}

	string ProcessHtml(string html)
	{
		if (Options.HasFlag(HtmlCompressorOptions.RemoveComments))
			html = _commentPattern.Replace(html, "");

		if (Options.HasFlag(HtmlCompressorOptions.SimpleDoctype))
			html = _doctypePattern.Replace(html, "<!DOCTYPE html>");

		if (Options.HasFlag(HtmlCompressorOptions.RemoveScriptAttributes))
		{
			html = _jsTypeAttrPattern.Replace(html, "$1$3"); //remove type from script tags
			html = _jsLangAttrPattern.Replace(html, "$1$3"); //remove language from script tags
		}
			
		if (Options.HasFlag(HtmlCompressorOptions.RemoveStyleAttributes)) //remove type from style tags
			html = _styleTypeAttrPattern.Replace(html, "$1$3");

		if (Options.HasFlag(HtmlCompressorOptions.RemoveLinkAttributes))
			html = RemoveLinkAttributes(html);

		if (Options.HasFlag(HtmlCompressorOptions.RemoveFormAttributes)) //remove method from form tags
			html = _formMethodAttrPattern.Replace(html, "$1$3");
			
		if (Options.HasFlag(HtmlCompressorOptions.RemoveInputAttributes)) //remove type from input tags
			html = _inputTypeAttrPattern.Replace(html, "$1$3");

		if (Options.HasFlag(HtmlCompressorOptions.SimpleBooleanAttributes)) //simplify bool attributes
			html = _booleanAttrPattern.Replace(html, "$1$2$4");

		if (Options.HasFlag(HtmlCompressorOptions.RemoveHttpProtocol)) //remove http from attributes
			html = RemoveHttpProtocol(html);
			
		if (Options.HasFlag(HtmlCompressorOptions.RemoveHttpsProtocol)) //remove https from attributes
			html = RemoveHttpsProtocol(html);

		if (Options.HasFlag(HtmlCompressorOptions.RemoveIntertagSpaces))
		{
			html = _intertagPatternTagTag.Replace(html, "><");
			html = _intertagPatternTagCustom.Replace(html, ">%%%~");
			html = _intertagPatternCustomTag.Replace(html, "~%%%<");
			html = _intertagPatternCustomCustom.Replace(html, "~%%%%%%~");
		}

		if (Options.HasFlag(HtmlCompressorOptions.RemoveMultiSpaces))
			html = _multispacePattern.Replace(html, " ");

		html = RemoveSpacesInsideTags(html); //remove spaces around equals sign and ending spaces

		if (Options.HasFlag(HtmlCompressorOptions.RemoveQuotes))
			html = RemoveQuotesInsideTags(html);

		if (_removeSurroundingSpacesTags != null)
			html = RemoveSurroundingSpaces(html);

		return html.Trim();
	}

	string RemoveSurroundingSpaces(string html)
	{
		var pattern = _surroundingSpacesPattern; //remove spaces around provided tags
		if (pattern == null)
			_surroundingSpacesPattern = pattern = _removeSurroundingSpacesTags.EqualsIgnoreCase(AllTags)
				? _surroundingSpacesAllPattern
				: new Regex($"\\s*(</?(?:{_removeSurroundingSpacesTags.Replace(",", "|")})(?:>|[\\s/][^>]*>))\\s*", _lineNoCase);

		var matcher = pattern.Matches(html);
		var sb = new StringBuilder();
		var lastValue = 0;

		foreach (Match match in matcher)
		{
			sb.Append(html.Substring(lastValue, match.Index - lastValue));
			sb.Append(match.Result("$1"));

			lastValue = match.Index + match.Length;
		}

		sb.Append(html.Substring(lastValue));

		return sb.ToString();
	}

	static string RemoveQuotesInsideTags(string html)
	{
		var matcher = _tagQuotePattern.Matches(html); //remove quotes from tag attributes
		var sb = new StringBuilder();
		var lastValue = 0;

		foreach (Match match in matcher) //if quoted attribute is followed by "/" add extra space
			if (match.Groups[3].Value.Trim().Length == 0)
			{
				sb.Append(html.Substring(lastValue, match.Index - lastValue));
				sb.Append(match.Result("=$2"));

				lastValue = match.Index + match.Length;
			}
			else
			{
				sb.Append(html.Substring(lastValue, match.Index - lastValue));
				sb.Append(match.Result("=$2 $3"));

				lastValue = match.Index + match.Length;
			}

		sb.Append(html.Substring(lastValue));

		return sb.ToString();
	}

	static string RemoveSpacesInsideTags(string html)
	{
		html = _tagPropertyPattern.Replace(html, "$1="); //remove spaces around equals sign inside tags
			
		var matcher = _tagEndSpacePattern.Matches(html); //remove ending spaces inside tags
		var sb = new StringBuilder();
		var lastValue = 0;

		foreach (Match match in matcher) //keep space if attribute value is unquoted before trailing slash
			if (match.Groups[2].Value.StartsWith("/") && _tagLastUnquotedValuePattern.IsMatch(match.Groups[1].Value))
			{
				sb.Append(html.Substring(lastValue, match.Index - lastValue));
				sb.Append(match.Result("$1 $2"));

				lastValue = match.Index + match.Length;
			}
			else
			{
				sb.Append(html.Substring(lastValue, match.Index - lastValue));
				sb.Append(match.Result("$1$2"));

				lastValue = match.Index + match.Length;
			}

		sb.Append(html.Substring(lastValue));

		return sb.ToString();
	}

	static string RemoveLinkAttributes(string html)
	{
		var matcher = _linkTypeAttrPattern.Matches(html); //remove type from link tags with rel=stylesheet
		var sb = new StringBuilder();
		var lastValue = 0;

		foreach (Match match in matcher) //if rel=stylesheet
			if (Matches(_linkRelAttrPattern, match.Groups[0].Value))
			{
				sb.Append(html.Substring(lastValue, match.Index - lastValue));
				sb.Append(match.Result("$1$3"));

				lastValue = match.Index + match.Length;
			}
			else
			{
				sb.Append(html.Substring(lastValue, match.Index - lastValue));
				sb.Append(match.Result("$0"));

				lastValue = match.Index + match.Length;
			}

		sb.Append(html.Substring(lastValue));

		return sb.ToString();
	}

	static string RemoveHttpProtocol(string html)
	{
		var matcher = _httpProtocolPattern.Matches(html); //remove http protocol from tag attributes
		var sb = new StringBuilder();
		var lastValue = 0;

		foreach (Match match in matcher) //if rel!=external
			if (!Matches(_relExternalPattern, match.Groups[0].Value))
			{
				sb.Append(html.Substring(lastValue, match.Index - lastValue));
				sb.Append(match.Result("$1$2"));

				lastValue = match.Index + match.Length;
			}
			else
			{
				sb.Append(html.Substring(lastValue, match.Index - lastValue));
				sb.Append(match.Result("$0"));

				lastValue = match.Index + match.Length;
			}

		sb.Append(html.Substring(lastValue));

		return sb.ToString();
	}

	static string RemoveHttpsProtocol(string html)
	{
		var matcher = _httpsProtocolPattern.Matches(html); //remove https protocol from tag attributes
		var sb = new StringBuilder();
		var lastValue = 0;

		foreach (Match match in matcher) //if rel!=external			
			if (!Matches(_relExternalPattern, match.Groups[0].Value))
			{
				sb.Append(html.Substring(lastValue, match.Index - lastValue));
				sb.Append(match.Result("$1$2"));

				lastValue = match.Index + match.Length;
			}
			else
			{
				sb.Append(html.Substring(lastValue, match.Index - lastValue));
				sb.Append(match.Result("$0"));

				lastValue = match.Index + match.Length;
			}

		sb.Append(html.Substring(lastValue));

		return sb.ToString();
	}
		
	// http://stackoverflow.com/questions/4450045/difference-between-matches-and-find-in-java-regex
	static bool Matches(Regex regex, string value) 
		=> new Regex($@"^{regex}$", regex.Options).IsMatch(value);

	void ProcessPreservedBlocks(
		IEnumerable<string> preBlocks, 
		IEnumerable<string> taBlocks, 
		IList<string> scriptBlocks,
		IList<string> styleBlocks, 
		IList<string> eventBlocks, 
		IEnumerable<string> condCommentBlocks,
		IEnumerable<string> skipBlocks, 
		IEnumerable<string> lineBreakBlocks,
		IEnumerable<List<string>> userBlocks, 
		HtmlCompressorStatistics statistics)
	{
		if (statistics != null)
		{
			foreach (var block in preBlocks)
				statistics.PreservedSize += block.Length;
				
			foreach (var block in taBlocks)
				statistics.PreservedSize += block.Length;
		}

		ProcessScriptBlocks(scriptBlocks, statistics);
		ProcessStyleBlocks(styleBlocks, statistics);
		ProcessEventBlocks(eventBlocks, statistics);
			
		if (statistics != null)
		{
			foreach (var block in condCommentBlocks)
				statistics.PreservedSize += block.Length;
				
			foreach (var block in skipBlocks)
				statistics.PreservedSize += block.Length;

			foreach (var blockList in userBlocks)
			foreach (var block in blockList)
				statistics.PreservedSize += block.Length;

			foreach (var block in lineBreakBlocks)
				statistics.PreservedSize += block.Length;
		}
	}

	void ProcessEventBlocks(IList<string> eventBlocks, HtmlCompressorStatistics statistics)
	{
		if (statistics != null)
			foreach (var block in eventBlocks)
				statistics.OriginalMetrics.InlineEventSize += block.Length;

		if (Options.HasFlag(HtmlCompressorOptions.RemoveJavaScriptProtocol))
			for (int i = 0, len = eventBlocks.Count; i < len; i++)
				eventBlocks[i] = RemoveJavaScriptProtocol(eventBlocks[i], statistics);
			
		else if (statistics != null)
			foreach (var block in eventBlocks)
				statistics.PreservedSize += block.Length;

		if (statistics != null)
			foreach (var block in eventBlocks)
				statistics.CompressedMetrics.InlineEventSize += block.Length;
	}

	static string RemoveJavaScriptProtocol(string source, HtmlCompressorStatistics statistics)
	{			
		var result = _eventJsProtocolPattern.Replace(source, @"$1", 1); //remove javascript: from inline events

		if (statistics != null)
			statistics.PreservedSize += result.Length;

		return result;
	}

	void ProcessScriptBlocks(IList<string> scriptBlocks, HtmlCompressorStatistics statistics)
	{
		if (statistics != null)
			foreach (var block in scriptBlocks)
				statistics.OriginalMetrics.InlineScriptSize += block.Length;

		if (Options.HasFlag(HtmlCompressorOptions.CompressJavaScript))
			for (int i = 0, len=scriptBlocks.Count; i < len; i++)
				scriptBlocks[i] = CompressJavaScript(scriptBlocks[i]);
			
		else if (statistics != null)
			foreach (var block in scriptBlocks)
				statistics.PreservedSize += block.Length;

		if (statistics != null)
			foreach (var block in scriptBlocks)
				statistics.CompressedMetrics.InlineScriptSize += block.Length;
	}

	void ProcessStyleBlocks(IList<string> styleBlocks, HtmlCompressorStatistics statistics)
	{
		if (statistics != null)
			foreach (var block in styleBlocks)
				statistics.OriginalMetrics.InlineStyleSize += block.Length;

		if (Options.HasFlag(HtmlCompressorOptions.CompressCss))
			for (int i = 0, len = styleBlocks.Count; i < len; i++)
				styleBlocks[i] = CompressCssStyles(styleBlocks[i]);
			
		else if (statistics != null)
			foreach (var block in styleBlocks)
				statistics.PreservedSize += block.Length;

		if (statistics != null)
			foreach (var block in styleBlocks)
				statistics.CompressedMetrics.InlineStyleSize += block.Length;
	}

	string CompressJavaScript(string source)
	{	
		if (JavaScriptCompressor == null)
			return source;
			
		var scriptCdataWrapper = false; //detect CDATA wrapper
		var cdataWrapper = false;
		var matcher = _scriptCdataPattern.Match(source);
		if (matcher.Success)
		{
			scriptCdataWrapper = true;
			source = matcher.Groups[1].Value;
		}
		else if (_cdataPattern.Match(source).Success)
		{
			cdataWrapper = true;
			source = matcher.Groups[1].Value;
		}

		var result = JavaScriptCompressor.Compress(source);

		if (scriptCdataWrapper)
			result = $"/*<![CDATA[*/{result}/*]]>*/";
		else if (cdataWrapper)
			result = $"<![CDATA[{result}]]>";

		return result;
	}

	string CompressCssStyles(string source)
	{			
		if (CssCompressor == null)
			return source;
		
		var cdataWrapper = false; //detect CDATA wrapper
		var matcher = _cdataPattern.Match(source);
		if (matcher.Success)
		{
			cdataWrapper = true;
			source = matcher.Groups[1].Value;
		}

		var result = CssCompressor.Compress(source);

		if (cdataWrapper)
			result = $"<![CDATA[{result}]]>";

		return result;

	}

	HtmlCompressor Clone() => new()
	{
		Options = Options, 
		JavaScriptCompressor = JavaScriptCompressor, 
		CssCompressor = CssCompressor, 
		PreservePatterns = PreservePatterns
	};
}