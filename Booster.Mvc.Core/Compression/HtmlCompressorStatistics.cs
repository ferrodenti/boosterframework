﻿namespace Booster.Mvc.Compression;

public sealed class HtmlCompressorStatistics
{
	public HtmlMetrics OriginalMetrics { get; } = new();
	public HtmlMetrics CompressedMetrics { get; } = new();
	public long Time { get; set; }
	public int PreservedSize { get; set; }

	public override string ToString() 
		=> $"Time={Time}, Preserved={PreservedSize}, Original={OriginalMetrics}, Compressed={CompressedMetrics}";
}