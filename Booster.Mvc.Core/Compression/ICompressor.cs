﻿namespace Booster.Mvc.Compression;

public interface ICompressor
{
	string Compress(string source);
}