﻿namespace Booster.Mvc.Compression;

public sealed class HtmlMetrics
{
	public int FileSize { get; set; }
	public int EmptyChars { get; set; }
	public int InlineScriptSize { get; set; }
	public int InlineStyleSize { get; set; }
	public int InlineEventSize { get; set; }

	public override string ToString() 
		=> $"Filesize={FileSize}, Empty Chars={EmptyChars}, Script Size={InlineScriptSize}, Style Size={InlineStyleSize}, Event Handler Size={InlineEventSize}";
}