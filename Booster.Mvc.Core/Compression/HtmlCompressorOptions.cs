﻿using System;

namespace Booster.Mvc.Compression;

[Flags]
public enum HtmlCompressorOptions
{
	Disabled = 0,
		
	Enabled = 1 << 0,
	CompressJavaScript = 1 << 1,
	CompressCss = 1 << 2,
	RemoveQuotes = 1 << 3,
	RemoveComments = 1 << 4,
	RemoveMultiSpaces = 1 << 5,
	RemoveIntertagSpaces = 1 << 6,
	SimpleDoctype = 1 << 7,
	RemoveScriptAttributes = 1 << 8,
	RemoveStyleAttributes = 1 << 9,
	RemoveLinkAttributes = 1 << 10,
	RemoveFormAttributes = 1 << 11,
	RemoveInputAttributes = 1 << 12,
	SimpleBooleanAttributes = 1 << 13,
	RemoveJavaScriptProtocol = 1 << 14,
	RemoveHttpProtocol = 1 << 15,
	RemoveHttpsProtocol = 1 << 16,
	PreserveLineBreaks = 1 << 17,
		                       
	Default = Enabled | RemoveComments | RemoveMultiSpaces
}