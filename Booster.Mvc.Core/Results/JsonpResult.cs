using Newtonsoft.Json;

namespace Booster.Mvc.Results;

#if NETSTANDARD || NETCOREAPP
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

public class JsonpResult : JsonResult
{
	public string Callback { get; set; }
	public Encoding ContentEncoding { get; set; } = Encoding.UTF8;

	public object Data
	{
		get => Value;
		set => Value = value;
	}
		
	public override Task ExecuteResultAsync(ActionContext context)
	{
		var response = context.HttpContext.Response;

		response.ContentType = ContentType.Or("application/javascript");
		response.Headers["ContentEncoding"] = ContentEncoding.EncodingName;

		if (string.IsNullOrEmpty(Callback))
			Callback = context.HttpContext.Request.Query["callback"];

		if (Value != null)
		{
			var ser = JsonConvert.SerializeObject(Value);
			var buffer = ContentEncoding.GetBytes($"{Callback}({ser});");
			response.Body.Write(buffer, 0, buffer.Length);
		}
			
		return Task.CompletedTask;
	}

	public JsonpResult() : base(null)
	{
	}
}
#else
using System;	
using System.Web.Mvc;
	
public class JsonpResult : JsonResult
{
	public string Callback { get; set; }

	public override void ExecuteResult(ControllerContext context)
	{
		if (context == null)
			throw new ArgumentNullException(nameof(context));

		var response = context.HttpContext.Response;

		response.ContentType = ContentType.Or("application/javascript");

		if (ContentEncoding != null)
			response.ContentEncoding = ContentEncoding;

		if (string.IsNullOrEmpty(Callback))
			Callback = context.HttpContext.Request.QueryString["callback"];

		if (Data != null)
		{
			var ser = JsonConvert.SerializeObject(Data);
			response.Write(Callback + "(" + ser + ");");
		}
	}
}
#endif