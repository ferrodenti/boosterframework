﻿#if NETSTANDARD || NETCOREAPP
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
#nullable enable

namespace Booster.Mvc.Results;

[DefaultStatusCode(_defaultStatusCode)]
public class ErrorResult : ObjectResult
{
	const int _defaultStatusCode = StatusCodes.Status500InternalServerError;

	public ErrorResult([ActionResultObjectValue] object? message)
		: base(message)
		=> StatusCode = _defaultStatusCode;
}
#endif