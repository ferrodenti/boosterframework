﻿using System;
using System.Text;
using JetBrains.Annotations;
using Newtonsoft.Json;
#if NETSTANDARD || NETCOREAPP
using System.IO;
using Microsoft.AspNetCore.Mvc;
#else
using System.Web.Mvc;
#endif

namespace Booster.Mvc.Results;

[PublicAPI]
public class JsonNetResult : ActionResult
{
	public Encoding ContentEncoding { get; set; }
	public string ContentType { get; set; }
	public object Data { get; set; }

	public JsonSerializerSettings SerializerSettings { get; set; }
	public Formatting Formatting { get; set; }

	public JsonNetResult(object data, Formatting formatting)
		: this(data)
		=> Formatting = formatting;

	public JsonNetResult(object data)
		: this()
		=> Data = data;

	public JsonNetResult()
	{
		Formatting = Formatting.None;
		SerializerSettings = new JsonSerializerSettings();
	}

#if NET5_0_OR_GREATER && !NETCOREAPP //TODO: remove !NETCOREAPP
		public override async Task ExecuteResultAsync(ActionContext context)
		{
			if (context == null)
				throw new ArgumentNullException(nameof(context));
			
			var response = context.HttpContext.Response;

			response.ContentType = !string.IsNullOrEmpty(ContentType)
				? ContentType
				: "application/json";

			if (Data == null)
				return;

			await using var textWriter = new StreamWriter(response.Body, ContentEncoding ?? Encoding.UTF8);
			var str = JsonConvert.SerializeObject(Data, Formatting, SerializerSettings);
			await textWriter.WriteAsync(str).NoCtx();
		}
#endif

#if NETSTANDARD || NETCOREAPP
	public override void ExecuteResult(ActionContext context)
#else
		public override void ExecuteResult(ControllerContext context)
#endif
	{
		if (context == null)
			throw new ArgumentNullException(nameof(context));

		var response = context.HttpContext.Response;

		response.ContentType = !string.IsNullOrEmpty(ContentType)
			? ContentType
			: "application/json";


#if !NETSTANDARD && !NETCOREAPP
			if (ContentEncoding != null)
				response.ContentEncoding = ContentEncoding;
#endif

		if (Data == null)
			return;

#if NETSTANDARD || NETCOREAPP
		var stream = response.Body;
		using var textWriter = new StreamWriter(stream, ContentEncoding ?? Encoding.UTF8);
#else
			var textWriter = response.Output;
#endif
		using var writer = new JsonTextWriter(textWriter) {Formatting = Formatting};
		var serializer = JsonSerializer.Create(SerializerSettings);
		serializer.Serialize(writer, Data); //TODO: сделать асинхронно, иначе это требует AllowSynchronousIO
		writer.Flush();
	}
}
