﻿#if NETSTANDARD || NETCOREAPP
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;

namespace Booster.Mvc.Results;

#nullable enable

[DefaultStatusCode(StatusCodes.Status404NotFound)]
public class StatusCodeMessageResult : ObjectResult
{
	public StatusCodeMessageResult(int statusCode, [ActionResultObjectValue] object? message)
		: base(message)
		=> StatusCode = statusCode;
}
#endif