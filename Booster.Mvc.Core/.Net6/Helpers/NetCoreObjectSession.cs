﻿#if NETCOREAPP // Т.к. проект общий нельзя просто так взять и выключить файлы. 

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Booster.Mvc.Helpers;

public class NetCoreObjectSession : IObjectSession
{
	readonly NetCoreSessionRepository _repository;

	public NetCoreObjectSession(ISession rawSession)
		=> _repository = NetCoreSessionRepository.Get(rawSession);

	public IEnumerable<string> Keys => _repository.Keys;

	public object this[string key]
	{
		get => _repository.Get<object>(key);
		set => _repository.Set(key, value);
	}

	public bool TryGetValue<T>(string key, out T value)
	{
		if (_repository.TryGetValue(key, out var obj) && obj is T result)
		{
			value = result;
			return true;
		}

		value = default;
		return false;
	}

	public bool TryRemove<T>(string key, out T value)
	{
		if (_repository.TryRemove(key, out var obj) && obj is T result)
		{
			value = result;
			return true;
		}

		value = default;
		return false;
	}

	public T SafeGet<T>(string key)
		=> _repository.Get<T>(key);

	public void Set(string key, object value)
		=> _repository.Set(key, value);

	public T GetOrAdd<T>(string key, Func<T> creator)
		=> _repository.GetOrAdd(key, creator);

	public Task<T> GetOrAddAsync<T>(string key, Func<Task<T>> creator)
		=> _repository.GetOrAddAsync(key, creator);

	public void Remove(string key)
		=> _repository.Remove(key);

	public void Clear()
		=> _repository.Clear();
}
#endif