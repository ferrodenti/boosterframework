﻿#if NETCOREAPP // Т.к. проект общий нельзя просто так взять и выключить файлы. 

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Booster.Synchronization;
using Microsoft.AspNetCore.Http;

namespace Booster.Mvc.Helpers;

public class NetCoreSessionRepository
{
	readonly string _id;
	readonly WeakReference<ISession> _rawSession;

	readonly ConcurrentDictionary<string, object> _items = new();
	static readonly ConcurrentDictionary<string, NetCoreSessionRepository> _repositories = new();

	readonly HashLocksPool _locks = new(256);

	static NetCoreSessionRepository()
		=> TimeSchedule.GC.Subscribe(typeof(NetCoreSessionRepository), TimeSpan.FromMinutes(10), () =>
		{
			foreach (var repo in _repositories.Values.ToArray())
				repo.TryDispose();
		});

	protected NetCoreSessionRepository(ISession session, string id)
	{
		_id = id;
		_rawSession = new WeakReference<ISession>(session);

		session.Set("_ses_init", new byte[] {0});

		Ping();
	}

	public void Ping()
		=> _rawSession.TryGetTarget(out _);

	public bool TryDispose()
	{
		if (!_rawSession.TryGetTarget(out _))
		{
			_items.Clear();
			_repositories.Remove(_id);
			return true;
		}

		return false;
	}

	public IEnumerable<string> Keys
	{
		get
		{
			Ping();
			return _items.Keys;
		}
	}

	public bool TryGetValue(string key, out object value)
	{
		Ping();

		return _items.TryGetValue(key, out value);
	}

	public bool TryRemove(string key, out object value)
	{
		Ping();

		return _items.TryRemove(key, out value);
	}

	public T Get<T>(string key)
	{
		Ping();

		if (_items.TryGetValue(key, out var value) && value is T result)
			return result;

		return default;
	}

	public void Set(string key, object value)
	{
		Ping();

		_items[key] = value;
	}

	public T GetOrAdd<T>(string key, Func<T> creator)
	{
		if (TryGetValue(key, out var obj) && obj is T result)
			return result;

		using (_locks.Lock(key))
		{

			if (TryGetValue(key, out obj) && obj is T result2)
				return result2;

			_items[key] = result = creator();
		}

		return result;
	}

	public async Task<T> GetOrAddAsync<T>(string key, Func<Task<T>> creator)
	{
		if (TryGetValue(key, out var obj) && obj is T result)
			return result;

		using var _ = await _locks.LockAsync(key).NoCtx();

		if (TryGetValue(key, out obj) && obj is T result2)
			return result2;

		_items[key] = result = await creator().NoCtx();

		return result;
	}

	public void Remove(string key)
	{
		Ping();

		_items.Remove(key);
	}

	public void Clear()
	{
		Ping();

		_items.Clear();
	}

	public override string ToString()
		=> _id;

	public static NetCoreSessionRepository Get(ISession rawSession)
	{
		var sesId = rawSession.GetString("SES_ID");
		if (sesId == null)
		{
			do
				sesId = Guid.NewGuid().ToString();
			while (_repositories.ContainsKey(sesId));
			rawSession.SetString("SES_ID", sesId);
		}

		return _repositories.GetOrAdd(sesId, _ => new NetCoreSessionRepository(rawSession, sesId));
	}
}

#endif