﻿#if NETCOREAPP // Т.к. проект общий нельзя просто так взять и выключить файлы. 

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Booster.FileSystem;
using Booster.Helpers;
using Booster.Mvc.Middlewares;
using Booster.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Hosting.Server.Features;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

#nullable enable

namespace Booster.Mvc.Helpers;

public class NetCoreMvcHelper : DependencyResolver, IMvcHelperImpl
{
	public HttpContext? GetHttpContext()
		=> GetRequiredService<IHttpContextAccessor>().HttpContext;

	public void RememberHttpContext()
	{
	}

	public IObjectSession? Session => RawSession.With(ses => new NetCoreObjectSession(ses));
	public string? DisplayUri => GetHttpContext()?.Request.GetDisplayUrl();

	public Url? RequestUrl
	{
		get
		{
			var request = GetHttpContext()?.Request;
			if (request == null)
				return null;

			return new Url
			{
				Protocol = request.Scheme,
				Host = request.Host.Host,
				Port = request.Host.Port,
				Path = string.Join(request.PathBase.ToUriComponent(), request.Path.ToUriComponent()).TrimStart('/'),
				Query = request.QueryString.ToUriComponent().TrimStart('?')
			};
		}
	}

		

	protected IWebHostEnvironment HostingEnvironment => GetRequiredService<IWebHostEnvironment>();

	IRouter? _router;

#if NET5_0_OR_GREATER

	readonly SyncLazy<Url[]> _appUrls = SyncLazy<Url[]>.Create<NetCoreMvcHelper>(self => self.GetAppUrls());

	Url[] GetAppUrls()
	{
		/*IEnumerable<string> From(KestrelServerOptions options)
		{
			if (TypeEx.Of(options)
				    .FindProperty(new ReflectionFilter("ListenOptions") {Access = AccessModifier.Any})
				    ?.GetValue(options) is IEnumerable<ListenOptions> listenOptions)
			{
				var opt = listenOptions.ToArray();
				if (opt.Length > 0)
					return opt.Select(o => o.ToString());
			}

			return null;
		}*/

		IEnumerable<string> Get()
		{
			var configuration = GetService<IConfiguration>();
			if (configuration != null)
			{
				var urls = configuration["ASPNETCORE_URLS"];
				if (urls.IsSome())
					return urls.SplitNonEmpty(';');
			}

			var server = GetService<IServer>();
			if (server != null)
			{
				var serverType = TypeEx.Of(server);
				if (serverType.Name == "IISHttpServer")
				{
					var options = serverType
						.FindField(new ReflectionFilter("_options") {Access = AccessModifier.Any})
						?.GetValue(server);

					if (options != null)
						if (TypeEx.Of(options)
								.FindProperty(new ReflectionFilter("ServerAddresses") {Access = AccessModifier.Any})
								?.GetValue(options) is string[] addresses)
							return addresses;
				}
				/*else if (serverType.Name == "KestrelServerImpl")
				{
					if (serverType
						    .FindProperty(new ReflectionFilter("Options") {Access = AccessModifier.Any})
						    ?.GetValue(server) is KestrelServerOptions options)
					{
						var addresses = From(options);
						if (addresses != null)
							return addresses;
					}
				}
				else
				{
					if (server is KestrelServer kestrelServer)
					{
						var addresses = From(kestrelServer.Options);
						if (addresses != null)
							return addresses;
					}
				}*/

				{
					var addresses = server.Features.Get<IServerAddressesFeature>();
					if (addresses?.Addresses.Count > 0)
						return addresses.Addresses;
				}
			}
				
			return new[] {"https://127.0.0.1", "http://127.0.0.1"};
		}

		return Get()
			.Select(u => new Url(u))
			.OrderByDescending(u => u.Protocol.EqualsIgnoreCase("https"))
			.ToArray();
	}

	public Url[] AppUrls => _appUrls.GetValue(this);

	readonly SyncLazy<int> _defaultAppPort = SyncLazy<int>.Create<NetCoreMvcHelper>(self =>
	{
		var url = self.AppUrls.FirstOrDefault() ?? "http://127.0.0.1";
		if (url.Port.HasValue)
			return url.Port.Value;

		if (url.Protocol.EqualsIgnoreCase("https"))
			return 443;

		return 80;
	});

	public int DefaultAppPort => _defaultAppPort.GetValue(this);

	public ISession? RawSession => GetHttpContext()?.Session;


	public IUrlHelper GetUrlHelper(string? baseUri = null)
		=> GetUrlHelper(baseUri.With(u => new Uri(u)));

	public IUrlHelper GetUrlHelper(Uri? baseUri)
	{
		var ctx = GetHttpContext();

		if (ctx == null)
			return CreateUrlHelper(baseUri);

		if (ctx.Items["UrlHelper"] is not IUrlHelper result)
			ctx.Items["UrlHelper"] = result = CreateUrlHelper(baseUri);

		return result;
	}
		
	public IUrlHelper CreateUrlHelper(Uri? baseUri)
	{
		HttpContext httpContext = new DefaultHttpContext { RequestServices = ServiceProvider };

		if (baseUri != null)
		{
			httpContext.Request.Scheme = baseUri.Scheme;
			httpContext.Request.Host = HostString.FromUriComponent(baseUri);
			httpContext.Request.PathBase = PathString.FromUriComponent(baseUri);
		}


		var actionContext = new ActionContext
		{
			HttpContext = httpContext,
			RouteData = new RouteData { Routers = { _router!/* ?? throw new InvalidOperationException("UseBoosterMvc() must be called first")*/} },
			ActionDescriptor = new ActionDescriptor()
		};

		var factory = GetRequiredService<IUrlHelperFactory>();
		return factory != null
			? factory.GetUrlHelper(actionContext)
			: new UrlHelper(actionContext);
	}

#endif
	public string UrlAction(string actionName, string controllerName, object? routeValues = null)
	{
		var gen = GetRequiredService<LinkGenerator>();
		var ctx = GetHttpContext() ?? new DefaultHttpContext { RequestServices = ServiceProvider };

		return gen.GetUriByAction(ctx, actionName, controllerName, routeValues)!;
	}

	public IApplicationBuilder UseBoosterMvcEndpoints(IApplicationBuilder app, Action<IEndpointRouteBuilder> configureRoutes)
	{
		Init(app.ApplicationServices);

		app.UseMiddleware<AsyncContextMiddleware>();

		app.UseEndpoints(configureRoutes);

		return app;
	}

#if NET5_0_OR_GREATER || NETCOREAPP
	public IApplicationBuilder UseBoosterMvc(IApplicationBuilder app, Action<IRouteBuilder> configureRoutes)
	{
		Init(app.ApplicationServices);

		app.UseMiddleware<AsyncContextMiddleware>();
		
		IRouteBuilder? rts = null;
		app.UseMvc(routes =>
		{
			configureRoutes(routes);
			rts = routes;
		});
		_router = rts!.Build();
		return app;
	}
#else
		public IApplicationBuilder UseBoosterMvc(IApplicationBuilder app, Action<IEndpointRouteBuilder> configureRoutes)
		{
			Init(app.ApplicationServices);
			
			app.UseMiddleware<AsyncContextMiddleware>();

			app.UseEndpoints(configureRoutes);
			
			return app;
		}
#endif

	public async Task<string> RenderViewToStringAsync(string controllerName, string viewName, object? model, object? viewBag = null)
	{
		var serviceScopeFactory = GetService<IServiceScopeFactory>();
		using var scope = serviceScopeFactory.CreateScope();
#if NETSTANDARD2_1 || NET5_0_OR_GREATER || NETCOREAPP
		await using var writer = new StringWriter();
#else
			using var writer = new StringWriter();
#endif
		var serviceProvider = scope.ServiceProvider;
		var httpContext = new DefaultHttpContext { RequestServices = serviceProvider };
		var routeData = new RouteData();
		routeData.Values.Add("controller", controllerName);
		var actionContext = new ActionContext(httpContext, routeData, new ActionDescriptor());
		var viewEngine = serviceProvider.GetService<IRazorViewEngine>() ?? throw new Exception("Could not get IRazorViewEngine instance");
		var viewResult = viewEngine.FindView(actionContext, viewName, false);

		if (!viewResult.Success)
			viewResult = viewEngine.GetView(null, viewName, false);

		if (!viewResult.Success)
			throw new ArgumentNullException($"{viewName} does not match any available view");

		var viewDictionary = new ViewDataDictionary(new EmptyModelMetadataProvider(), new ModelStateDictionary())
		{
			Model = model
		};

		if (viewBag != null)
			foreach (var (key, value) in AnonTypeHelper.ToKVPairs<string, object>(viewBag))
				viewDictionary[key] = value;

		var tempDataProvider = serviceProvider.GetService<ITempDataProvider>()!;
		var viewContext = new ViewContext(
			actionContext,
			viewResult.View!,
			viewDictionary,
			new TempDataDictionary(httpContext, tempDataProvider),
			writer,
			new HtmlHelperOptions()
		);

		if (viewContext.RouteData.Routers.Count == 0)
			viewContext.RouteData.Routers.AddRange((MvcHelper.HttpContext ?? httpContext).GetRouteData().Routers);

		await viewResult.View.RenderAsync(viewContext).NoCtx();
		return writer.ToString();
	}

	public string RenderViewToString(string controllerName, string viewName, object? model, object? viewBag = null)
		=> RenderViewToStringAsync(controllerName, viewName, model, viewBag).NoCtxResult();


	FastLazy<FilePath>? _webRoot;
	protected FilePath WebRoot => _webRoot ??= (FilePath)HostingEnvironment.WebRootPath.Or(ContentRoot);

	FastLazy<FilePath>? _contentRoot;
	protected FilePath ContentRoot => _contentRoot ??= (FilePath)HostingEnvironment.ContentRootPath.Or(FilePath.CurrectDirectory);

	internal readonly List<StaticFileOptions> StaticFileOptions = new();

	public FilePath MapPath(string path)
	{
		FilePath? result;
		if (path.Length > 0)
			switch (path[0])
			{
			case '~':
				path = path.Substring(1);

				if (path.Length > 0)
					return path[0] switch
					{
						'\\' => (ResolvePath(path, out result) ? result : ContentRoot.Combine(path))!,
						'/' => (ResolvePath(path, out result) ? result : ContentRoot.Combine(path))!,
						_ => throw new ArgumentOutOfRangeException(nameof(path), $"invalid relative path: {path}")
					};

				return ContentRoot;
			case '/':
				return ResolvePath(path, out result)
					? result!
					: ContentRoot.Combine(path);
			}

		return WebRoot.Combine(path);
	}

	bool ResolvePath(string path, out FilePath? result)
	{
		result = null;
		var fp = new FilePath(path.Substring(1));
		if (fp.Segments.FirstOrDefault() != "..")
		{
			var ps = new PathString(path);

			foreach (var opt in StaticFileOptions.Where(opt => ps.StartsWithSegments(opt.RequestPath)))
				if (fp.DirectoryEnding)
				{
					var info = (opt.FileProvider ?? HostingEnvironment.WebRootFileProvider).GetDirectoryContents(ps);
					if (info.Exists)
					{
						var file = info.FirstOrDefault();
						if (file != null)
						{
							result = Path.GetDirectoryName(file.PhysicalPath)!;
							return true;
						}
					}
				}
				else
				{
					var info = (opt.FileProvider ?? HostingEnvironment.WebRootFileProvider).GetFileInfo(ps);
					if (info.Exists)
					{
						result = info.PhysicalPath;
						return true;
					}
				}

			if (result != null)
				return true;
		}

		return false;
	}
}

#endif