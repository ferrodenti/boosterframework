﻿#if NETCOREAPP
using System;
using System.Collections.Generic;
using System.Threading;
using Booster.Collections;
using Booster.Reflection;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;

#nullable enable

namespace Booster.Mvc.ModelBinders;

public class BoosterBinderProvider : IModelBinderProvider
{
	readonly ReaderWriterLockSlim _lock = new(LockRecursionPolicy.NoRecursion);
	readonly HybridDictionary<TypeEx, TypeEx?> _binders = new();
	readonly HybridDictionary<TypeEx, TypeEx> _deriveds = new();
	readonly HashSet<TypeEx> _routeModelAttributes = new();

	public BoosterBinderProvider Add<TModel, TBinder>(bool deriveds = false)
		where TBinder : IModelBinder
		=> Add(typeof(TModel), typeof(TBinder), deriveds);
	
	public BoosterBinderProvider Add(TypeEx modelType, TypeEx binderType, bool deriveds = false)
	{
		using var _ = _lock.WriteLock();
		
		_binders.Add(modelType, binderType);
		
		if(deriveds)
			_deriveds.Add(modelType, binderType);
		
		return this;
	}

	public BoosterBinderProvider Add<TBinder>()
		where TBinder : IModelBinder
	{
		TypeEx binderType = typeof(TBinder);
		TypeEx modelType;
		bool deriveds;
				
		if (binderType.TryFindInterface(typeof(IModelBinder<>), out var ifc))
		{
			modelType = ifc.GenericArguments[0];
			deriveds = false;
		}
		else if (binderType.TryFindAttribute<ModelTypeAttribute>(out var attr))
		{
			modelType = attr.ModelType;
			deriveds = attr.Deriveds;
		}
		else
			throw new InvalidOperationException($"{binderType.Name}: expected {nameof(ModelTypeAttribute)} attribute or IModelBinder<TModel> interface");

		return Add(modelType, binderType, deriveds);
	}

	public BoosterBinderProvider AddRouteModelAttribute(TypeEx attributeType)
	{
		if (!typeof(Attribute).IsAssignableFrom(attributeType))
			throw new ArgumentException("Expected attribute type", nameof(attributeType));

		_routeModelAttributes.Add(attributeType);
		return this;
	}

	public BoosterBinderProvider AddRouteModelAttribute<TAttribute>()
		where TAttribute : Attribute
		=> AddRouteModelAttribute(typeof(TAttribute));

	[PublicAPI]
	public BoosterBinderProvider AddRouteModelAttribute()
		=> AddRouteModelAttribute<RouteModelAttribute>();

	public IModelBinder? GetBinder(ModelBinderProviderContext context)
	{
		using var _ = _lock.UpgradeableReadLock();
		
		var modelType = TypeEx.Get(context.Metadata.ModelType);
	
		if (_binders.TryGetValue(modelType, out var binderType))
			return binderType.With(t => new BinderTypeModelBinder(t));

		foreach (var pair in _deriveds)
			if (pair.Key.IsAssignableFrom(modelType))
				using (_lock.WriteLock())
				{
					_binders[modelType] = pair.Value;
					return new BinderTypeModelBinder(pair.Value);
				}

		foreach (var attr in _routeModelAttributes)
			if (modelType.HasAttribute(attr))
				using (_lock.WriteLock())
				{
					var type = typeof(PassModelBinder<>).MakeGenericType(modelType);
					_binders[modelType] = type;
					return new BinderTypeModelBinder(type);
				}

		using (_lock.WriteLock())
			_binders[modelType] = null;

		return null;
	}
}
#endif