﻿#if NETCOREAPP
using System.Threading.Tasks;
using Booster.Reflection;
using Microsoft.AspNetCore.Mvc.ModelBinding;

#nullable enable

namespace Booster.Mvc.ModelBinders;

public class PassModelBinder<TModel> : PassModelBinder, IModelBinder<TModel>
{
	public PassModelBinder() : base(typeof(TModel))
	{
	}
}

public class PassModelBinder : IModelBinder
{
	readonly TypeEx _type;
	
	public PassModelBinder(TypeEx type)
		=> _type = type;

	public Task BindModelAsync(ModelBindingContext bindingContext)
	{
		var request = bindingContext.HttpContext.Request;
		var value = request.RouteValues[bindingContext.FieldName];
		if (value == null) 
			bindingContext.Result = ModelBindingResult.Failed();
		else
		{
			var type = value.GetType();

			bindingContext.Result = _type.IsAssignableFrom(type)
				? ModelBindingResult.Success(value)
				: ModelBindingResult.Failed();
		}

		return Task.CompletedTask;
	}

}
#endif