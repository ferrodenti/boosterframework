﻿#if NETCOREAPP
using System.Reflection;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;

#nullable enable

namespace Booster.Mvc.ModelBinders;

[PublicAPI]
public class ComplexObjectRouteBinderProvider : IModelBinderProvider
{
	class ComplexObjectRouteBinder : IModelBinder
	{
		public Task BindModelAsync(ModelBindingContext bindingContext)
		{
			var key = bindingContext.ModelName;
			if (bindingContext.ActionContext.RouteData.Values.TryGetValue(key, out var value))
				bindingContext.Result = ModelBindingResult.Success(value);

			return Task.CompletedTask;
		}
	}

	static readonly Assembly _thisAssembly = typeof(BoosterBinderProvider).Assembly;

	public IModelBinder? GetBinder(ModelBinderProviderContext context)
	{
		if (context.Metadata.ModelType.Assembly == _thisAssembly)
			return new BinderTypeModelBinder(typeof(ComplexObjectRouteBinder));

		return null;
	}
}


#endif