﻿#if NETCOREAPP || NETSTANDARD
using Booster;
using Microsoft.AspNetCore.Html;

#nullable enable

namespace System.Web.Mvc;

public class MvcHtmlString : HtmlString
{
	public bool IsSome => Value.IsSome();
	public bool IsEmpty => Value.IsEmpty();

	public MvcHtmlString(string value) : base(value)
	{
	}

	//public static implicit operator string(MvcHtmlString? str)
	//	=> str?.Value ?? "";

	public static implicit operator MvcHtmlString(string? str)
		=> new(str ?? "");
}

#endif