﻿#if NETCOREAPP || NETSTANDARD
using JetBrains.Annotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// ReSharper disable once CheckNamespace
namespace System.Web;

[Controller]
[PublicAPI]
public abstract class ApiController
{
	[ActionContext] public ActionContext ActionContext { get; set; }

	public HttpContext HttpContext => ActionContext?.HttpContext;

	public HttpRequest Request => ActionContext?.HttpContext?.Request;

	public HttpResponse Response => ActionContext?.HttpContext?.Response;

	public IServiceProvider Resolver => ActionContext?.HttpContext?.RequestServices;
}
#endif