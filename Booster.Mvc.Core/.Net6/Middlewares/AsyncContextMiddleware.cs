#if NETCOREAPP // Т.к. проект общий нельзя просто так взять и выключить файлы. 
using System.Threading.Tasks;
using Booster.DI;
using Microsoft.AspNetCore.Http;

#if DEBUG_ASYNC_CONTEXT
using Microsoft.AspNetCore.Http.Extensions;
#endif

namespace Booster.Mvc.Middlewares;

public class AsyncContextMiddleware
{
	readonly RequestDelegate _next;

	public AsyncContextMiddleware(RequestDelegate next)
		=> _next = next;
		 
	public async Task Invoke(HttpContext context)
	{
#if DEBUG_ASYNC_CONTEXT
		using (AsyncContext.Push($"Request: {context.Request.GetDisplayUrl()}"))
#else
		using (AsyncContext.Push())
#endif
			await _next(context);
	}
}
#endif