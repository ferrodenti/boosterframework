﻿#if NETCOREAPP || NETSTANDARD
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Http;

namespace Booster.Mvc.Middlewares;

[PublicAPI]
public class ReturnDataMiddleware
{
	readonly byte[] _data;

	public ReturnDataMiddleware(RequestDelegate _, byte[] data) 
		=> _data = data;

	public async Task Invoke(HttpContext context)
	{
		var response = context.Response;

		response.Clear();

		await response.Body.WriteAsync(_data).NoCtx();
	}
}

#endif