﻿#if NETCOREAPP // Т.к. проект общий нельзя просто так взять и выключить файлы. 

using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Booster.Log;

#nullable enable

namespace Booster.Mvc.Middlewares;

public class ErrorHandleOptions
{
	public Func<HttpContext, Exception, Task>? Handle { get; set; }
	public bool Throw { get; set; }
	public bool Break { get; set; }
	public ILog? Log { get; set; }
}

#endif
