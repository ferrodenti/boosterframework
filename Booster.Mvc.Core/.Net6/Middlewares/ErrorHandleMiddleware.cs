﻿#if NETCOREAPP // Т.к. проект общий нельзя просто так взять и выключить файлы. 

using System;
using System.Threading.Tasks;
using Booster.Debug;
using Booster.Log;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

#nullable enable

namespace Booster.Mvc.Middlewares;

public class ErrorHandleMiddleware
{
	readonly ErrorHandleOptions _options;
	readonly RequestDelegate _next;

	public ErrorHandleMiddleware(RequestDelegate next, IOptions<ErrorHandleOptions> options)
	{
		_next = next;
		_options = options.Value;
		
		if (_options.Break)
			SafeBreak.Enabled = true;
	}

	public async Task Invoke(HttpContext context)
	{
		try
		{
			await _next(context).NoCtx();
		}
		catch (Exception e)
		{
			if (_options.Break)
				SafeBreak.Break();
			
			_options.Log.Error(e);
			
			if (_options.Handle != null)
				await _options.Handle(context, e).NoCtx();

			if (_options.Throw)
				throw;
		}
	}
}

#endif
