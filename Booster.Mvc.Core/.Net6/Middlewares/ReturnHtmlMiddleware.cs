﻿#if NETCOREAPP || NETSTANDARD
using System;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using Booster.Helpers;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Http;

namespace Booster.Mvc.Middlewares;

[PublicAPI]
public class ReturnHtmlMiddleware
{
	readonly Lazy<ReturnDataMiddleware> _next;

	public ReturnHtmlMiddleware(RequestDelegate _, 
		string body, 
		string title = null, 
		string h1 = null, 
		string language = null, 
		Encoding encoding = null)
		=> _next = new Lazy<ReturnDataMiddleware>(() =>
		{
			title ??= AssemblyHelper.BoosterCallingAssembly.GetName().Name;
			h1 ??= title;
			body =  h1.IsSome() ? $"<h1>{h1}</h1>{body}" : body;

			language ??= CultureInfo.CurrentCulture.IetfLanguageTag.ToLower();
			encoding ??= Encoding.UTF8;
			var encodingName = encoding.WebName;
			var html = $@"<!doctype html>
<html lang={language}>
<head>
<meta charset={encodingName}>
<title>{title}</title>
</head>
<body>
{body}
</body>
</html>";
			var data = encoding.GetBytes(html);

			return new ReturnDataMiddleware(_, data);
		});

	public Task Invoke(HttpContext context) 
		=> _next.Value.Invoke(context);
}
#endif