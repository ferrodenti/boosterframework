﻿#if NETCOREAPP || NETSTANDARD

using JetBrains.Annotations;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Booster.Mvc.TagHelpers;

[PublicAPI]
public class Options4EnumTagHelper : TagHelper
{
	public override void Process(TagHelperContext context, TagHelperOutput output)
		=> base.Process(context, output);
}

#endif