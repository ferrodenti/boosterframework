﻿#if NETCOREAPP || NETSTANDARD
using System.Threading.Tasks;

namespace Booster.Mvc.TagHelpers;

public class JsTagHelper : BaseIncludeTagHelper
{
	protected override string HttpContextItemName => "JsIncludes";

	protected override async Task<string> Format(string url)
		=> $"<script type=\"text/javascript\" src=\"{await HashedContentFiles.GetUrl(url, Version, Seed).NoCtx()}\"></script>";
}

#endif