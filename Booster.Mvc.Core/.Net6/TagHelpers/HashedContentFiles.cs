using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Booster.Mvc.TagHelpers.Kitchen;

namespace Booster.Mvc.TagHelpers;

public static class HashedContentFiles
{
	/// <summary lang="en-US">A common seed used to initialize the hashing algorithm</summary>
	/// <summary lang="ru-RU">Общий для всех файлов сид используемый для инициализации алгоритма хеширования</summary>
	public static int Seed { get; set; }

	static readonly ConcurrentDictionary<string, ContentFileHash> _fileHashes = new(StringComparer.OrdinalIgnoreCase);

	public static Task<string> GetHash(string path, int seed = 0)
		=> _fileHashes.GetOrAdd(path, _ => new ContentFileHash(path)).GetValue(Seed + seed);

	public static async Task<string> GetUrl(string path, bool appendVersion, int seed = 0)
		=> appendVersion
			? $"{path}?v={await GetHash(path, seed).NoCtx()}"
			: path;
}