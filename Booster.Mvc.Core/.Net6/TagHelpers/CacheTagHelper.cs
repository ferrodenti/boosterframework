#if NETCOREAPP || NETSTANDARD
using System;
using System.Threading.Tasks;
using Booster.Caching;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Razor.TagHelpers;

#nullable enable

namespace Booster.Mvc.TagHelpers;

[PublicAPI]
public class CacheTagHelper : TagHelper
{
	public string? Key { get; set; }
	public string? Key2 { get; set; }
	public string? Key3 { get; set; }

	public bool Enabled { get; set; } = true;

	public TimeSpan AbsoluteExpiration { get; set; }
	public TimeSpan SlidingExpiration { get; set; }

	public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
	{
		output.TagName = "";

		if (Enabled)
		{
			if (Key.IsEmpty())
				throw new ArgumentNullException(nameof(Key));

			var lazy = new CacheAsyncLazy<string>(Key, Key2, Key3);

			if (AbsoluteExpiration.IsSome())
				lazy.Settings.AbsoluteExpiration = AbsoluteExpiration;

			if (SlidingExpiration.IsSome())
				lazy.Settings.SlidingExpiration = SlidingExpiration;

			var content = await lazy.GetAsync(async () =>
			{
				var content = await output.GetChildContentAsync().NoCtx();
				return content.GetContent();
			}).NoCtx();

			output.Content.SetHtmlContent(content);
		}
	}
}

#endif