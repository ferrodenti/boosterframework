using System;
using System.Collections.Generic;
using System.Linq;
using Booster.Collections;

namespace Booster.Mvc.TagHelpers.Kitchen;

public class IncludesDictionary
{
	readonly OrderedDictionary<string, OrderedDictionary<string, int>> _values 
		= new(StringComparer.OrdinalIgnoreCase);

	public void Add(string value, string viewName) 
		=> _values.GetOrAdd(viewName, _ => new OrderedDictionary<string, int>(StringComparer.OrdinalIgnoreCase))[value] = 1;

	public IEnumerable<string> Enumerate()
	{
		var hash = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
		foreach(var p1 in _values.Reverse())
		foreach (var key in p1.Value.Keys)
			if (hash.Add(key))
				yield return key;
	}
}