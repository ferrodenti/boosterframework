using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Booster.FileSystem;
using Booster.Mvc.Helpers;
#if NETCOREAPP || NETSTANDARD
using Booster.Helpers;
#endif

namespace Booster.Mvc.TagHelpers.Kitchen;

class ContentFileHash : IDisposable
{
	const int _hashingBlockSize = 4096 * 4;

	readonly bool _network;
	readonly string _path;
	readonly FileWatcher _fileWatcher;
	readonly AsyncLazy<string> _hashValue = AsyncLazy<string>.Create<ContentFileHash>(self => self.CalculateHash());
	int _seed;

	public ContentFileHash(string path)
	{
		if (path.StartsWith("http"))
		{
			_network = true;
			_path = path;
		}
		else
		{
			_path = MvcHelper.MapPath(path);
			_fileWatcher = new FileWatcher(_path); // {UseFileSystemWatcher = false};
			_fileWatcher.Changed += (_, _) => _hashValue.Reset();
		}
	}

	public Task<string> GetValue(int seed = 0)
	{
		if (_seed != seed)
			lock (this)
				if (_seed != seed)
				{
					_hashValue.Reset();
					_seed = seed;
				}

		return _hashValue.GetValue(this);
	}

	async Task<string> CalculateHash()
	{
		try
		{
#if NETCOREAPP
			await using var stream = await ReadFile().NoCtx();
#else
				using var stream = await ReadFile().NoCtx();
#endif
			if (stream is not {CanRead: true})
				return "";

			using var md5 = System.Security.Cryptography.MD5.Create();

#if NETCOREAPP || NETSTANDARD
			using var rent = ArrayPools.Rent<byte>(_hashingBlockSize);
			var buffer = rent.Array;
#else
				var buffer = new byte[_hashingBlockSize];
#endif
			var seed = BitConverter.GetBytes(_seed);
			md5.TransformBlock(seed, 0, seed.Length, null, 0);

			int bytesRead;
			do
			{
				bytesRead = await stream.ReadAsync(buffer, 0, _hashingBlockSize).NoCtx();
				if (bytesRead > 0)
					md5.TransformBlock(buffer, 0, bytesRead, null, 0);

			} while (bytesRead > 0);

			md5.TransformFinalBlock(buffer, 0, 0);
			return Convert.ToBase64String(md5.Hash ?? Array.Empty<byte>()).TrimEnd('=');
			//return BitConverter.ToString(md5.Hash).Replace("-", "").ToLowerInvariant();
		}
		catch
		{
			return "_";
		}
	}

	async Task<Stream> ReadFile()
	{
		if (_network)
		{
			using var client = new HttpClient();
			using var message = await client.GetAsync(_path).NoCtx();
			message.EnsureSuccessStatusCode();

			var array = await message.Content.ReadAsByteArrayAsync().NoCtx();
			return new MemoryStream(array);
		}

		return !File.Exists(_path)
			? null
			: new FileStream(_path, FileMode.Open, FileAccess.Read, FileShare.Read, _hashingBlockSize, true);
	}

	public void Dispose()
		=> _fileWatcher?.Dispose();
}
