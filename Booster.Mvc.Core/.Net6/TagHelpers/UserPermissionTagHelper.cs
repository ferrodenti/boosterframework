#if NETCOREAPP || NETSTANDARD
using System.Threading.Tasks;
using Booster.DI;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Booster.Mvc.TagHelpers;

[HtmlTargetElement(Attributes = "user-permission")]
[PublicAPI]
public class UserPermissionTagHelper : TagHelper
{
	public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
	{
		var requiredStr = (output.Attributes["user-permission"]?.Value as HtmlString)?.Value;
		output.Attributes.RemoveAll("user-permission");
		
		var pp = InstanceFactory.Get<IPermissionsProvider>();
		if (pp != null)
		{
			var required = pp.FromString(requiredStr);
			var userPermissions = await pp.GetUserPermissions().NoCtx();
			if (pp.ComparePermisssions(required, userPermissions))
				return;
		}

		output.SuppressOutput();
	}
}
#endif