﻿#if NETCOREAPP || NETSTANDARD
using System.Threading.Tasks;

namespace Booster.Mvc.TagHelpers;

public class CssTagHelper : BaseIncludeTagHelper
{
	protected override string HttpContextItemName => "CssIncludes";

	protected override async Task<string> Format(string url)
		=> $"<link href=\"{await HashedContentFiles.GetUrl(url, Version, Seed).NoCtx()}\" type=\"text/css\" rel=\"stylesheet\" />";
}

#endif