﻿#if NETCOREAPP || NETSTANDARD
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Booster.Parsing;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Razor.TagHelpers;

#nullable enable

namespace Booster.Mvc.TagHelpers;

[PublicAPI]
public class SelectTagHelper : TagHelper
{
	public object? SelectedValue { get; set; }
	public IEnumerable? SelectedValues { get; set; }

	public bool IgnoreCase { get; set; }
	public bool Disabled { get; set; }

	public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
	{
		var html = output.Content.IsModified
			? output.Content.GetContent()
			: (await output.GetChildContentAsync().NoCtx()).GetContent();

		var selectedValues = new HashSet<string?>(IgnoreCase ? StringComparer.OrdinalIgnoreCase : StringComparer.Ordinal);

		if (SelectedValues != null)
		{
			foreach (var value in SelectedValues)
				if (value != null)
					selectedValues.Add(value.ToString());
		}
		else if (SelectedValue != null) 
			selectedValues.Add(SelectedValue.ToString());

		var parser = new StringParser(html) { IgnoreCase = true };
		var builder = new StringBuilder(html.Length + " selected ".Length);

		parser.SearchFor(new Term("<option", _ =>
		{
			parser.SearchFor(new Term("value=", _ =>
			{
				parser.SearchFor(
					new Term("\"", prev =>
					{
						builder.Append(prev);
						builder.Append('\"');
						var pos = parser.Pos;
						parser.SearchFor(
							new Term("\\\"", true),
							new Term("\"", false)
						);

						var val = html.SubstringSafe(pos, parser.Pos - pos - 1);

						builder.Append(val);
						builder.Append('\"');

						if (selectedValues.Contains(val)) 
							builder.Append(" selected=\"selected\" ");

						return true;
					}));

				return false;
			}));

			return true;
		}));

		if (!parser.IsEol)
			builder.Append(parser.RestString);

		output.Content.SetHtmlContent(builder.ToString());

		if (Disabled)
			output.Attributes.SetAttribute("disabled", "disabled");
	}
}
#endif