﻿#if NETCOREAPP || NETSTANDARD
using System.Threading.Tasks;
using Booster.Mvc.Helpers;
using Booster.Mvc.TagHelpers.Kitchen;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

#nullable enable

namespace Booster.Mvc.TagHelpers;

[PublicAPI]
public abstract class BaseIncludeTagHelper : TagHelper
{
	protected abstract string HttpContextItemName { get; }
	protected abstract Task<string> Format(string url);

	public string? Src { get; set; }

	[ViewContext] public ViewContext? ViewContext { get; set; }

	public bool Render { get; set; }
	public bool Now { get; set; }
	public bool Version { get; set; } = true;
	//public bool TryMinify { get; set; } = true; TODO: поиск минифицированных файлов
	public string NewLine { get; set; } = "\n\t";

	/// <summary lang="en-US">A seed used to initialize the hashing algorithm</summary>
	/// <summary lang="ru-RU">Сид используемый для инициализации алгоритма хеширования</summary>
	public int Seed { get; set; }

	public IncludesDictionary? Includes
	{
		get => MvcHelper.HttpContext?.Items[HttpContextItemName] as IncludesDictionary;
		set => MvcHelper.HttpContext.With(c => c.Items[HttpContextItemName] = value);
	}

	public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
	{
		var dict = Includes;

		if (Render)
		{
			if (dict != null)
			{
				var html = new EnumBuilder(NewLine);
				foreach (var item in dict.Enumerate())
					html.Append(item);

				output.Content.SetHtmlContent(html);
				output.TagName = "";

				Includes = null;
				return;
			}
		}
		else if (Now)
		{
			string content;
			if (Src.IsSome())
				content = await Format(MvcHelper.Url(Src)).NoCtx();
			else
				content = output.Content.IsModified
					? output.Content.GetContent()
					: (await output.GetChildContentAsync().NoCtx()).GetContent();

			output.Content.SetHtmlContent(content);
			output.TagName = "";
			return;
		}
		else
		{
			if (dict == null)
				Includes = dict = new IncludesDictionary();

			if (Src.IsSome())
				dict.Add(await Format(MvcHelper.Url(Src)).NoCtx(), ViewContext!.ExecutingFilePath);
			else
			{
				var content = output.Content.IsModified
					? output.Content.GetContent()
					: (await output.GetChildContentAsync().NoCtx()).GetContent();

				dict.Add(content, ViewContext!.ExecutingFilePath);
			}

		}

		output.SuppressOutput();
	}
}
#endif