#if NETCOREAPP || NETSTANDARD
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Booster.Mvc.TagHelpers;

[HtmlTargetElement(Attributes = "cond-visible")]
public class CondVisibleTagHelper : TagHelper
{
	public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
	{
		var condition = (output.Attributes["cond-visible"]?.Value as HtmlString)?.Value;
		output.Attributes.RemoveAll("cond-visible");
		if (condition.IsSome())
		{
			var content = (await output.GetChildContentAsync()).GetContent();
			if (content.IsSome() &&
				condition.SplitNonEmpty(',')
						 .Any(c => CheckSimpleCond(content, c.Trim())))
				return;

			output.SuppressOutput();
		}
	}

	static bool CheckSimpleCond(string content, string cond) //TODO: здесь по хорошему надо юзать региксы
	{
		switch (cond[0])
		{
		case '.':
			var clss = cond.Substring(1);
			return content.Contains($"class=\"{clss}\"");
		case '#':
			var id = cond.Substring(1);
			return content.Contains($"class=\"{id}\"");
		default:
			return content.Contains($"<{cond}");
		}
	}
}

#endif