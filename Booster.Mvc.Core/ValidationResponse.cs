﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Booster.Interfaces;
using Newtonsoft.Json;

#nullable enable

namespace Booster.Mvc;

public class ValidationResponse : IValidationResponse
{
	[JsonProperty("errors", NullValueHandling = NullValueHandling.Ignore)]
	public Dictionary<string, string>? Errors { get; private set; }

	[JsonProperty("isOk")]
	public bool IsOk { get; private set; } = true;

	public void Error(string parameterName, string message)
	{
		IsOk = false;
		Errors ??= new Dictionary<string, string>();
		Errors[parameterName] = message;
	}

	public void Error(string parameterName, string message, params object[] args) 
		=> Error(parameterName, string.Format(message, args));

	public void Error<T>(T obj, Expression<Func<T, object?>> accessor, string message)
	{
		var mi = accessor.ParseMemberRef();
		Error(mi.Name, message);
	}

	public void Error<T>(T obj, Expression<Func<T, object?>> accessor, string message, params object[] args) 
		=> Error(obj, accessor, string.Format(message, args));
}