using System;
using System.Collections.Generic;
using System.Linq;
using Booster.Reflection;
using JetBrains.Annotations;

#if NETSTANDARD || NETCOREAPP3
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
#else
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
#endif

namespace Booster.Mvc
{
	[PublicAPI]
	public class ControllerActionConstraint : IRouteConstraint
	{
		readonly TypeEx _controllerType;
		HashSet<string> _methods;
		public HashSet<string> Methods
		{
			get
			{
				if (_methods == null)
				{
					_methods = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
					_methods.AddRange(_controllerType.FindMethods(new ReflectionFilter {Access = AccessModifier.Public, IsStatic = false})
						.Where(mi => !mi.HasAttribute<NonActionAttribute>() && !(mi.IsSpecialName && (mi.Name.StartsWith("set_") || mi.Name.StartsWith("get_"))))
						.Select(mi => mi.Name));
				}
				return _methods;
			}
		}


		public ControllerActionConstraint(TypeEx controllerType)
			=> _controllerType = controllerType;

#if NETSTANDARD || NETCOREAPP3
		public bool Match(HttpContext httpContext, IRouter route, string routeKey, RouteValueDictionary values, RouteDirection routeDirection)
#else
		public bool Match(HttpContextBase httpContext, Route route, string routeKey, RouteValueDictionary values, RouteDirection routeDirection)
#endif
		{
			var val = values[routeKey].With(v => v.ToString());
			return val.IsSome() && Methods.Contains(val);
		}

	}
}