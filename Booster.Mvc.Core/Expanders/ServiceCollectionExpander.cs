#if NETCOREAPP || NETSTANDARD
using System;
using System.Linq;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.AspNetCore.Server.Kestrel.Core;
#if !NETCOREAPP && !NET5_0_OR_GREATER
using Microsoft.AspNetCore.Mvc.Razor;
#endif
using Microsoft.Extensions.DependencyInjection;

namespace Booster.Mvc;

[PublicAPI]
public static class ServiceCollectionExpander
{
	//public static void AddHttpContextAccessor(this IServiceCollection services)
	//	=> services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

	//public static void AddBoosterHttpContextAccessor(this IServiceCollection services)
	//	=> services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

	public static IServiceCollection AllowSynchronousIO(this IServiceCollection services)
	{
		services.Configure<KestrelServerOptions>(options => options.AllowSynchronousIO = true);
		services.Configure<IISServerOptions>(options => options.AllowSynchronousIO = true);
		return services;
	}

	public static IMvcBuilder AddBoosterMvc(this IServiceCollection services, Action<MvcOptions> setupAction = null)
	{
		services.AddHttpContextAccessor();

#if NETCOREAPP || NET5_0_OR_GREATER
		return services
			   .AddControllersWithViews(options => setupAction?.Invoke(options))
			   .AddNewtonsoftJson(options
				   => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
#else
			return services.AddMvc(options => setupAction?.Invoke(options));
#endif
	}

	class EmptyModelValidator : IObjectModelValidator
	{
		public void Validate(ActionContext actionContext, ValidationStateDictionary validationState, string prefix, object model)
		{
		}
	}

	public static IServiceCollection DisableDefaultModelValidation(this IServiceCollection services)
	{
		var serviceDescriptor = services.FirstOrDefault(s => s.ServiceType == typeof(IObjectModelValidator));
		if (serviceDescriptor != null)
		{
			services.Remove(serviceDescriptor);
			services.Add(new ServiceDescriptor(typeof(IObjectModelValidator), _ => new EmptyModelValidator(), ServiceLifetime.Singleton));
		}

		return services;
	}
}
#endif
