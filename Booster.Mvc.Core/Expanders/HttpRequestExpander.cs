﻿#if NETSTANDARD || NETCOREAPP
using System;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Http;

namespace Booster.Mvc.Helpers;

[PublicAPI]
public static class HttpRequestExpander
{
	public static string[] GetHeaderValues(this HttpRequest request, string headerName)
		=> request?.Headers?.TryGetValue(headerName, out var value) == true
			? value.ToArray()
			: Array.Empty<string>();
}
#endif
