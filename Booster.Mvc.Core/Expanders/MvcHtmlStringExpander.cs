using System.Web.Mvc;

namespace Booster.Mvc;

public static class MvcHtmlStringExpander
{
#if NETCOREAPP || NETSTANDARD
	public static bool IsSome(this MvcHtmlString str)
		=> str is {IsSome: true};
		
	public static bool IsEmpty(this MvcHtmlString str)
		=> str == null || str.IsEmpty;
#else
	public static bool IsSome(this MvcHtmlString str)
		=> str != null && str.ToString().IsSome();
		
	public static bool IsEmpty(this MvcHtmlString str)
		=> str == null || str.ToString().IsEmpty();
#endif
		
}