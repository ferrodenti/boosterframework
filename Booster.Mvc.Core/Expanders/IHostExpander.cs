#if NETSTANDARD || NETCOREAPP

using Booster.Mvc.Helpers;
using JetBrains.Annotations;
using Microsoft.Extensions.Hosting;

namespace Booster.Mvc;

[PublicAPI]
public static class HostExpander
{
	public static IHost UseBoosterMvcHelper(this IHost host)
	{
		MvcHelper.Init(host.Services);
		return host;
	}
}

#endif