#if NETCOREAPP || NETSTANDARD
using System;
using Booster.Mvc.Helpers;
using Booster.Mvc.Middlewares;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;

namespace Booster.Mvc;

[PublicAPI]
public static class ApplicationBuilderExpander
{
	public static IApplicationBuilder UseErrorHandling(this IApplicationBuilder app, Action<ErrorHandleOptions> initProc)
	{
		var options = new ErrorHandleOptions();
		initProc(options);
		return app.UseMiddleware<ErrorHandleMiddleware>(Microsoft.Extensions.Options.Options.Create(options));
	}

	public static IApplicationBuilder UseBoosterMvcHelper(this IApplicationBuilder app)
	{
		MvcHelper.Init(app.ApplicationServices);
		return app;
	}

	public static IApplicationBuilder UseBoosterMvc(this IApplicationBuilder app, Action<IRouteBuilder> configureRoutes)
		=> MvcHelper.UseBoosterMvc(app, configureRoutes);

	public static IApplicationBuilder UseBoosterMvcEndpoints(this IApplicationBuilder app, Action<IEndpointRouteBuilder> configureRoutes)
		=> MvcHelper.UseBoosterMvcEndpoints(app, configureRoutes);

	public static IApplicationBuilder UseBoosterStaticFiles(this IApplicationBuilder app, StaticFileOptions options = null)
	{
		options ??= new StaticFileOptions();
		((NetCoreMvcHelper) MvcHelper.Impl).StaticFileOptions.Add(options);
		return app.UseStaticFiles(options);
	}
}

#endif
