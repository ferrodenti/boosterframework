using System;
using System.Web.Mvc;
using Booster.Reflection;
using JetBrains.Annotations;

#if NETSTANDARD || NETCOREAPP
using Microsoft.AspNetCore.Mvc;
using HtmlHelper = Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper;
#else
using System.Collections.Generic;
using System.Web.WebPages;
using JetBrains.Annotations;
#endif

namespace Booster.Mvc.Helpers;

[PublicAPI]
public static class HtmlHelperExpander
{
#if NETSTANDARD || NETCOREAPP
	public static IUrlHelper CreateUrlHelper(this HtmlHelper html)
		=> MvcHelper.CreateUrlHelper();
#else
	public static UrlHelper CreateUrlHelper(this HtmlHelper html)
		=> new(html.ViewContext.RequestContext);

	#region Css & Js includes

	public static HelperResult Css(this HtmlHelper html, [PathReference]string css)
	{
		var urlHelper = html.CreateUrlHelper();

		if (html.ViewContext.TempData["CssDict"] is not HashSet<string> dict)
			html.ViewContext.TempData["CssDict"] = dict = new HashSet<string>();


		dict.Add($"<link href=\"{urlHelper.Content(css)}\" type=\"text/css\" rel=\"stylesheet\" />");
		return null;
	}

	public static HelperResult Css(this HtmlHelper html, Func<dynamic, HelperResult> contents)
	{
		if (html.ViewContext.TempData["CssDict"] is not HashSet<string> dict)
			html.ViewContext.TempData["CssDict"] = dict = new HashSet<string>();

		dict.Add(HelperResultToString(html, contents(html)));

		return null;
	}

	public static HelperResult Js(this HtmlHelper html, [PathReference]string js)
	{
		if (html.ViewContext.TempData["JsDict"] is not HashSet<string> dict)
			html.ViewContext.TempData["JsDict"] = dict = new HashSet<string>();


		var urlHelper = html.CreateUrlHelper();
		dict.Add($" <script type=\"text/javascript\" Src=\"{urlHelper.Content(js)}\"></script>");

		return null;
	}
	public static HelperResult Js(this HtmlHelper html, Func<dynamic, HelperResult> contents)
	{
		if (html.ViewContext.TempData["JsDict"] is not HashSet<string> dict)
			html.ViewContext.TempData["JsDict"] = dict = new HashSet<string>();

		dict.Add(HelperResultToString(html, contents(html)));

		return null;
	}
	#endregion

	// ReSharper disable once UnusedParameter.Local
	static string HelperResultToString(HtmlHelper html, HelperResult result)
		=> result.ToHtmlString();
#endif


	//TODO: -> TagHelper
	public static MvcHtmlString Options4Enum<T>(
		this HtmlHelper html,
		object selected = null,
		bool useIntValues = false,
		bool nonDefault = true,
		bool powerOf2 = false,
		bool uniqueFlags = false)
		where T : struct
		=> Options4Enum(html, typeof(T), selected, useIntValues, nonDefault, powerOf2, uniqueFlags);

	public static MvcHtmlString Options4Enum(
		this HtmlHelper html, 
		TypeEx enumType, 
		object selected = null, 
		bool useIntValues = false, 
		bool nonDefault = true,
		bool powerOf2 = false,
		bool uniqueFlags = false)
	{
		var str = new EnumBuilder("\r\n");

		var values = EnumHelper.GetValues(enumType);
		if (nonDefault)
			values = values.NonDefault;
		if (powerOf2)
			values = values.PowerOfTwo;
		else if (uniqueFlags)
			values = values.UniqueFlags;

		Func<EnumValue, bool> cmpFunc = v => v.Selected;

		if (selected != null)
		{
			TypeEx type = selected.GetType();
			if (type.Equals(enumType))
				cmpFunc = v => Equals(v.Value, selected);
			else if (type == typeof(string))
				cmpFunc = v => v.Name.EqualsIgnoreCase((string) selected);
			else if (type.IsNumeric)
				cmpFunc = v => Equals(v.IntValue, selected);
		}

		foreach (var v in values)
			str.Append($"<option value='{(useIntValues ? v.IntValue : v.Value)}'{(cmpFunc(v) ? " selected" : "")}{(v.Disabled ? " disabled" : "")}>{v.DisplayName}</option>");

		return new MvcHtmlString(str);
	}
}