using System.Windows.Forms;
using System.Xml.Serialization;

namespace Booster.WinForms.UISettings;

[XmlType("NumericUpDown")]
public class NumericUpDownSettings : BaseSettings<NumericUpDown>
{
	[XmlAttribute] public decimal Value { get; set; }

	public NumericUpDownSettings() { }

	public NumericUpDownSettings(NumericUpDown ctrl, string name) : base(ctrl, name) { }

	public override void BindEvents()
	{
		Control.TextChanged += (s, e) =>
		{
			Value = Control.Value;
		};
	}

	public override void UpdateControl()
	{
		Control.Value = Value;
	}

	public override void UpdateSettings()
	{
		Value = Control.Value;
	}
}