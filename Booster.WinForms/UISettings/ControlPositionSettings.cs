using System.Windows.Forms;
using System.Xml.Serialization;

namespace Booster.WinForms.UISettings;

public class ControlPositionSettings<TControl> : BaseSettings<TControl> where TControl : Control
{
	[XmlAttribute] public int Top { get; set; } 
	[XmlAttribute] public int Left { get; set; } 
	[XmlAttribute] public int Height { get; set; } 
	[XmlAttribute] public int Width { get; set; } 

	public ControlPositionSettings() { }

	public ControlPositionSettings(TControl ctrl, string name) : base(ctrl, name) {}

	public override void BindEvents()
	{
		Control.Move += (s, e) =>
		{
			Top = Control.Top;
			Left = Control.Left;
		};

		Control.SizeChanged += (s, e) =>
		{
			Height = Control.Height;
			Width = Control.Width;
		};
	}

	public override void UpdateControl()
	{
		if( Top != 0 ) Control.Top = Top;
		if (Height != 0) Control.Height = Height;
		if (Left != 0) Control.Left = Left;
		if (Width != 0) Control.Width = Width;
	}

	public override void UpdateSettings()
	{
		Top = Control.Top;
		Left = Control.Left;
		Height = Control.Height;
		Width = Control.Width;
	}
}