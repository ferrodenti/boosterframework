using System.Net;
using System.Xml.Serialization;
using Booster.WinForms.Controls;

namespace Booster.WinForms.UISettings;

[XmlType("IpAddress")]
public class IpAddressControlSettings : BaseSettings<IpAddressControl>
{
	[XmlAttribute] public string Value { get; set; }

	public IpAddressControlSettings() { }

	public IpAddressControlSettings(IpAddressControl ctrl, string name) : base(ctrl, name) { }

	public override void BindEvents()
	{
		Control.TextChanged += (s, e) =>
		{
			Value = Control.Value?.ToString();
		};
	}

	public override void UpdateControl()
	{
		IPAddress a;
		if (string.IsNullOrWhiteSpace(Value) || !IPAddress.TryParse(Value, out a))
			Control.Clear();
		else
			Control.Value = a;
	}

	public override void UpdateSettings()
	{
		Value = Control.Value?.ToString();
	}
}