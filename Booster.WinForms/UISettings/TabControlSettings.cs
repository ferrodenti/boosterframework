using System.Windows.Forms;
using System.Xml.Serialization;

namespace Booster.WinForms.UISettings;

[XmlType("TabControl")]
public class TabControlSettings : BaseSettings<TabControl>
{
	[XmlAttribute] public int SelectedIndex { get; set; }

	public TabControlSettings() { }

	public TabControlSettings(TabControl ctrl, string name) : base(ctrl, name) { }

	public override void BindEvents()
	{
		Control.Selected += (s, e) =>
		{
			SelectedIndex = Control.SelectedIndex;
		};
	}

	public override void UpdateControl()
	{
		Control.SelectedIndex = SelectedIndex;
	}

	public override void UpdateSettings()
	{
		SelectedIndex = Control.SelectedIndex;
	}
}