using System.Windows.Forms;
using System.Xml.Serialization;

namespace Booster.WinForms.UISettings;

[XmlType("TextBox")]
public class TextBoxSettings : BaseSettings<TextBox>
{
	[XmlAttribute] public string Text { get; set; }

	public TextBoxSettings() { }

	public TextBoxSettings(TextBox ctrl, string name) : base(ctrl, name) { }

	public override void BindEvents()
	{
		Control.TextChanged += (s, e) =>
		{
			Text = Control.Text;
		};
	}

	public override void UpdateControl()
	{
		Control.Text = Text;
	}

	public override void UpdateSettings()
	{
		Text = Control.Text;
	}
}