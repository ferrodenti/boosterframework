using System.Windows.Forms;
using System.Xml.Serialization;

namespace Booster.WinForms.UISettings;

public abstract class BaseSettings<TControl> : BaseSettings where TControl : Control
{
	[XmlIgnore] public new TControl Control
	{
		get => (TControl)base.Control;
		set => base.Control = value;
	}

	public override bool IsBinded => Control != null;

	protected BaseSettings() { }
	protected BaseSettings(TControl control, string name)
		: base(control, name)
	{
		// ReSharper disable VirtualMemberCallInConstructor
		UpdateSettings();
		BindEvents();
		// ReSharper restore VirtualMemberCallInConstructor
	}
}

public abstract class BaseSettings
{
	[XmlAttribute]
	public string Name { get; set; }

	[XmlIgnore]
	public abstract bool IsBinded { get; }

	Control _control;

	[XmlIgnore] public Control Control
	{
		get => _control;
		set
		{
			if (_control != value)
			{
				_control = value;

				if (_control != null)
				{
					UpdateControl();
					BindEvents();
				}
			}
		}
	}


	protected BaseSettings() {}
	protected BaseSettings(Control control, string name)
	{
		Name = name;
		_control = control;
	}

	public abstract void BindEvents();
	public abstract void UpdateControl();
	public abstract void UpdateSettings();
}