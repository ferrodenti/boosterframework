using System.Windows.Forms;
using System.Xml.Serialization;

namespace Booster.WinForms.UISettings;

[XmlType("SplitContainer")]
public class SplitContainerSettings : BaseSettings<SplitContainer>
{
	[XmlAttribute]
	public int Distance { get; set; }

	public SplitContainerSettings() { }

	public SplitContainerSettings(SplitContainer ctrl, string name) : base(ctrl, name) {}

	public override void BindEvents()
	{
		Control.SplitterMoved += (s, e) =>
		{
			Distance = Control.SplitterDistance;
		};
	}

	public override void UpdateControl()
	{
		if (Distance != 0)
			Control.SplitterDistance = Distance;
	}

	public override void UpdateSettings()
	{
		Distance = Control.SplitterDistance;
	}
}