using System.Windows.Forms;
using System.Xml.Serialization;

namespace Booster.WinForms.UISettings;

[XmlType("CheckBox")]
public class CheckBoxSettings : BaseSettings<CheckBox>
{
	[XmlAttribute] public bool Checked { get; set; }

	public CheckBoxSettings() { }

	public CheckBoxSettings(CheckBox ctrl, string name) : base(ctrl, name) { }

	public override void BindEvents()
	{
		Control.CheckedChanged += (s, e) =>
		{
			Checked = Control.Checked;
		};
	}

	public override void UpdateControl()
	{
		Control.Checked = Checked;
	}

	public override void UpdateSettings()
	{
		Checked = Control.Checked;
	}
}