using System.Windows.Forms;
using System.Xml.Serialization;

namespace Booster.WinForms.UISettings;

[XmlType("Form")]
public class FormSettings : ControlPositionSettings<Form>
{
	[XmlAttribute] public FormWindowState State { get; set; } 

	public FormSettings() { }

	public FormSettings(Form ctrl, string name) : base(ctrl, name) { }

	public override void BindEvents()
	{
		Control.Move += (s, e) =>
		{
			if (Control.WindowState == FormWindowState.Normal)
			{
				Top = Control.Top;
				Left = Control.Left;
			}
		};

		Control.SizeChanged += (s, e) =>
		{
			State = Control.WindowState;

			if (State == FormWindowState.Normal)
			{
				Height = Control.Height;
				Width = Control.Width;
			}
		};
	}

	public override void UpdateControl()
	{
		if (Top != 0 || Height != 0)
			Control.StartPosition = FormStartPosition.Manual;
		
		base.UpdateControl();

		Control.WindowState = State;
	}

	public override void UpdateSettings()
	{
		base.UpdateSettings();

		State = Control.WindowState;
	}
}