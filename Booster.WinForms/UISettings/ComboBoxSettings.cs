using System.Windows.Forms;
using System.Xml.Serialization;
using Booster.WinForms.Expanders;

namespace Booster.WinForms.UISettings;

[XmlType("ComboBox")]
public class ComboBoxSettings : BaseSettings<ComboBox>
{
	[XmlIgnore] public bool ByIndex { get; set; }
	[XmlAttribute] public string Value { get; set; }

	public ComboBoxSettings() { }
	public ComboBoxSettings(ComboBox ctrl, string name) : base(ctrl, name) { }

	public override void BindEvents()
	{
		if (ByIndex)
			Control.SelectedIndexChanged += (s, e) => UpdateSettings();
		else
			Control.SelectedValueChanged += (s, e) => UpdateSettings();
	}

	public override void UpdateControl()
	{
		if (ByIndex)
		{
			int i;
			if (int.TryParse(Value, out i))
				Control.SelectedIndex = i;

			if (Control.SelectedItem == null && Control.Items.Count > 0)
				Control.SelectedIndex = 0;
		}
		else
			Control.SelectByText(Value);
	}

	public override void UpdateSettings()
	{
		Value = ByIndex
			? Control.SelectedIndex.ToString()
			: Control.SelectedItem?.ToString();
	}
}