﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml.Serialization;
using Booster.WinForms.Controls;

namespace Booster.WinForms.UISettings;

public class UISettings
{
	List<BaseSettings> _settings;

	[XmlElement(typeof(FormSettings))]
	[XmlElement(typeof(SplitContainerSettings))]
	[XmlElement(typeof(TabControlSettings))]
	[XmlElement(typeof(ComboBoxSettings))]
	[XmlElement(typeof(TextBoxSettings))]
	[XmlElement(typeof(MaskedTextBoxSettings))]
	[XmlElement(typeof(NumericUpDownSettings))]
	[XmlElement(typeof(IpAddressControlSettings))]
	[XmlElement(typeof(CheckBoxSettings))]
	public List<BaseSettings> Settings
	{
		get => _settings ?? (_settings = new List<BaseSettings>());
		set => _settings = value;
	}

	[XmlIgnore]
	public bool AutoUnbindOnFormClosed { get; set; }

	public UISettings()
	{
	}
	public UISettings(bool autoUnbindOnFormClosed)
		=> AutoUnbindOnFormClosed = autoUnbindOnFormClosed;

	protected TSettings GetSettings<TSettings, TControl>(TControl ctrl, string name, Action<TSettings> configurator = null)
		where TControl : Control
		where TSettings : BaseSettings<TControl>
	{
		foreach (var settings in Settings)
			if (settings is TSettings set && string.Equals(name, set.Name) && !set.IsBinded)
			{
				configurator?.Invoke(set);
				set.Control = ctrl;
				return set;
			}

		var res = (TSettings)Activator.CreateInstance(typeof(TSettings), ctrl, name);
		configurator?.Invoke(res);
		Settings.Add(res);

		if (AutoUnbindOnFormClosed)
		{
			var form = ctrl as Form ?? ctrl.FindForm();
			if (form != null)
				form.Closed += (s, e) => { res.Control = null; };
		}

		return res;
	}

	public FormSettings Bind(Form form, string bindingName = null)
		=> GetSettings<FormSettings, Form>(form, bindingName);

	public SplitContainerSettings Bind(SplitContainer ctrl, string bindingName = null)
		=> GetSettings<SplitContainerSettings, SplitContainer>(ctrl, bindingName);

	public TabControlSettings Bind(TabControl ctrl, string bindingName = null)
		=> GetSettings<TabControlSettings, TabControl>(ctrl, bindingName);

	public ComboBoxSettings Bind(ComboBox ctrl, string bindingName = null, bool byIndex = false)
	{
		return GetSettings<ComboBoxSettings, ComboBox>(ctrl, bindingName, s => s.ByIndex = byIndex);
	}
	public TextBoxSettings Bind(TextBox ctrl, string bindingName = null)
		=> GetSettings<TextBoxSettings, TextBox>(ctrl, bindingName);

	public NumericUpDownSettings Bind(NumericUpDown ctrl, string bindingName = null)
		=> GetSettings<NumericUpDownSettings, NumericUpDown>(ctrl, bindingName);

	public IpAddressControlSettings Bind(IpAddressControl ctrl, string bindingName = null)
		=> GetSettings<IpAddressControlSettings, IpAddressControl>(ctrl, bindingName);

	public MaskedTextBoxSettings Bind(MaskedTextBox ctrl, string bindingName = null)
		=> GetSettings<MaskedTextBoxSettings, MaskedTextBox>(ctrl, bindingName);

	public CheckBoxSettings Bind(CheckBox ctrl, string bindingName = null)
		=> GetSettings<CheckBoxSettings, CheckBox>(ctrl, bindingName);

	public void Unbind(Control control)
	{
		foreach (var set in Settings)
			if (set.IsBinded && set.Control == control)
				set.Control = null;
	}
	public void UnbindAll()
	{
		foreach (var set in Settings)
			set.Control = null;
	}
}