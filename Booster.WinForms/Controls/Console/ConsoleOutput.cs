using System;
using System.Drawing;
using System.IO;
using System.Text;

namespace Booster.WinForms.Controls;

public class ConsoleOutput : TextWriter
{
	public override Encoding Encoding { get; } = Encoding.UTF8;

	public void Bind()
		=> Console.SetOut(this);

	public event EventHandler<ConsoleOutEventArgs> Out;
	protected virtual void OnOut(ConsoleOutEventArgs e)
	{
		var handler = Out;
		if (handler != null)
		{
			e.Background = ConsoleColorToColor(Console.BackgroundColor);
			e.Foreground = ConsoleColorToColor(Console.ForegroundColor);
			handler(this, e);
		}
	}

	readonly object _lock = new();

	public override void Write(string value)
	{
		lock (_lock)
			OnOut(new ConsoleOutEventArgs(value));
	}


	public override void Write(char value)
	{
		lock (_lock)
			OnOut(new ConsoleOutEventArgs(value.ToString()));
	}

	public override void WriteLine(string value)
	{
		lock (_lock)
		{
			OnOut(new ConsoleOutEventArgs(value));
			OnOut(new ConsoleOutEventArgs(Environment.NewLine));
		}
	}

	Color ConsoleColorToColor(ConsoleColor color)
	{
		return color switch
		       {
			       ConsoleColor.Black       => Color.Black,
			       ConsoleColor.DarkBlue    => Color.DarkBlue,
			       ConsoleColor.DarkGreen   => Color.DarkGreen,
			       ConsoleColor.DarkCyan    => Color.DarkCyan,
			       ConsoleColor.DarkRed     => Color.DarkRed,
			       ConsoleColor.DarkMagenta => Color.DarkMagenta,
			       ConsoleColor.DarkYellow  => Color.FromArgb(10192652),
			       ConsoleColor.Gray        => Color.LightGray,
			       ConsoleColor.DarkGray    => Color.DarkGray,
			       ConsoleColor.Blue        => Color.Blue,
			       ConsoleColor.Green       => Color.FromArgb(0x00FF00),
			       ConsoleColor.Cyan        => Color.Cyan,
			       ConsoleColor.Red         => Color.Red,
			       ConsoleColor.Magenta     => Color.Magenta,
			       ConsoleColor.Yellow      => Color.Yellow,
			       _                        => Color.White
		       };
	}
}