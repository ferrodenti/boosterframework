using System;
using System.Drawing;

namespace Booster.WinForms.Controls;

public class ConsoleOutEventArgs : EventArgs
{
	public int CursorTop { get; set; } = -1;
	public int CursorLeft { get; set; } = -1;

	public Color Background { get; set; }
	public Color Foreground { get; set; }
	public string String { get; set; }

	public ConsoleOutEventArgs(string str)
	{
		String = str;
	}
}