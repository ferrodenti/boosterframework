﻿using System;
using System.Windows.Forms;
using Booster.Helpers;
using JetBrains.Annotations;

namespace Booster.WinForms.Controls;

[PublicAPI]
public partial class ConsoleControl : UserControl
{
	ConsoleOutput _consoleOutput;
	public ConsoleOutput ConsoleConsoleOutput
	{
		get
		{
			if (_consoleOutput == null)
			{
				_consoleOutput = new ConsoleOutput();
				_consoleOutput.Out += ConsoleOutput;
			}
			return _consoleOutput;
		}
	}

	public ConsoleControl()
	{
		InitializeComponent();

		//richTextBoxConsole.SelectionStart = richTextBoxConsole.TextLength;
		//richTextBoxConsole.SelectionLength = 0;

		richTextBoxConsole.Focus();
	}
		
	public void BindStandardOutput()
	{
		var hWnd = WinApi.GetConsoleWindow();

		if (hWnd == IntPtr.Zero)
		{
			WinApi.AllocConsole();
			hWnd = WinApi.GetConsoleWindow();
		}
		WinApi.ShowWindow(hWnd, 0);
		ConsoleConsoleOutput.Bind();
	}

	void ConsoleOutput(object sender, ConsoleOutEventArgs e)
	{
		Invoke((Action)(() =>
		{
			var start = richTextBoxConsole.SelectionStart;
			var length = richTextBoxConsole.SelectionLength;
			var cur = Math.Max(0, richTextBoxConsole.TextLength);

			Append(e.String);

			richTextBoxConsole.SelectionStart = cur;
			richTextBoxConsole.SelectionLength = richTextBoxConsole.TextLength - cur;
			richTextBoxConsole.SelectionColor = e.Foreground;
			richTextBoxConsole.SelectionBackColor = e.Background;


			if (length > 0)
			{
				richTextBoxConsole.SelectionStart = start;
				richTextBoxConsole.SelectionLength = length;
			}
			else
			{
				richTextBoxConsole.SelectionStart = richTextBoxConsole.TextLength;
				richTextBoxConsole.SelectionLength = 0;
			}

		}));
	}

	void Append(string str)
		=> richTextBoxConsole.AppendText(str);
}