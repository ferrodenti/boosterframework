﻿using System;
using System.ComponentModel;
using System.Net;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Booster.WinForms.Controls;

public class IpAddressControl : TextBox 
{
	public class FieldChangedEventArgs : EventArgs 
	{
		public int Field { get; }
		public int Value { get; }

		public FieldChangedEventArgs(int field, int value)
		{
			Field = field;
			Value = value;
		}
	}

	[StructLayout(LayoutKind.Sequential)]
	struct Nmhdr 
	{
		readonly IntPtr HWndFrom;
		readonly UIntPtr IdFrom;
		public readonly int Code;
	}
	[StructLayout(LayoutKind.Sequential)]
	struct NmIpAddress 
	{
		public readonly Nmhdr Hdr;
		public readonly int Field;
		public readonly int Value;
	}
	[StructLayout(LayoutKind.Sequential)]
	struct InitCommonControlsExParams
	{
		public int Size;
		public int Icc;
	}

	const int _wmNotify = 0x004E,
		_wmUser = 0x0400,
		_wmReflect = _wmUser + 0x1C00,
		_ipnFirst = -860,
		_ipmSetrange = _wmUser + 103, 
		_ipmClearaddress = _wmUser + 100,
		_ipmIsblank = _wmUser + 105,
		_iccInternetClasses = 0x00000800,
		_csVredraw = 0x0001,
		_csHredraw = 0x0002,
		_csDblclks = 0x0008,
		_csGlobalclass = 0x4000,
		_wsChild = 0x40000000,
		_wsVisible = 0x10000000,
		_wsTabstop = 0x00010000,
		_wsExRight = 0x00001000,
		_wsExLeft = 0x00000000,
		_wsExRtlreading = 0x00002000,
		_wsExLtrreading = 0x00000000,
		_wsExLeftscrollbar = 0x00004000,
		_wsExRightscrollbar = 0x00000000,
		_wsExNoparentnotify = 0x00000004,
		_wsExClientedge = 0x00000200;

	readonly int[] _values = {0, 0, 0, 0};
	bool _initialized;		
	public event EventHandler<FieldChangedEventArgs> FieldChanged;

	[DllImport("comctl32")]
	static extern bool InitCommonControlsEx(ref InitCommonControlsExParams lpInitCtrls);

	protected virtual void OnFieldChanged(FieldChangedEventArgs e) => FieldChanged?.Invoke(this, e);

	protected override CreateParams CreateParams 
	{
		get 
		{
			if (!DesignMode)
			{
				if (!_initialized)
				{
					var ic = new InitCommonControlsExParams
					{
						Size = Marshal.SizeOf(typeof (InitCommonControlsExParams)),
						Icc = _iccInternetClasses
					};
					_initialized = InitCommonControlsEx(ref ic);
				}
				if (_initialized)
				{
					var cp = base.CreateParams;
					cp.ClassName = "SysIPAddress32";
					cp.Height = 23;
					cp.ClassStyle = _csVredraw | _csHredraw | _csDblclks | _csGlobalclass;
					cp.Style = _wsChild | _wsVisible | _wsTabstop | 0x80;
					cp.ExStyle = _wsExNoparentnotify | _wsExClientedge;

					if (RightToLeft == RightToLeft.No || (RightToLeft == RightToLeft.Inherit && Parent.RightToLeft == RightToLeft.No))
						cp.ExStyle |= _wsExLeft | _wsExLtrreading | _wsExRightscrollbar;
					else
						cp.ExStyle |= _wsExRight | _wsExRtlreading | _wsExLeftscrollbar;

					return cp;
				}
			}
			return base.CreateParams;
		}
	}

	public bool SetIpRange(int field, byte lowValue, byte highValue) 
	{
		if(!_initialized) 
			return false;
					
		var m = Message.Create(Handle, _ipmSetrange, (IntPtr)field, (IntPtr)((highValue << 8) + lowValue));
		WndProc(ref m);
		return m.Result.ToInt32() > 0;
	}

	[Browsable(false)]
	[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
	public IPAddress Value
	{
		get
		{
			if(_initialized && !IsBlank && IPAddress.TryParse(Text, out var res))
				return  res;

			return null;
		}
		set
		{
			if (_initialized)
				Text = value?.ToString() ?? "";
		}
	}

	[Browsable(false)]
	[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
	public bool IsBlank 
	{
		get 
		{
			if(!_initialized) 
				return !(Text.Length > 0);

			var m = Message.Create(Handle, _ipmIsblank, IntPtr.Zero, IntPtr.Zero);
			WndProc(ref m);
			return m.Result.ToInt32() > 0;
		}
	}

	public new void Clear() 
	{
		if(!_initialized) 
		{
			base.Clear();
			return;
		}		
		var m = Message.Create(Handle, _ipmClearaddress, IntPtr.Zero, IntPtr.Zero);
		WndProc(ref m);
	}
		
	protected override void WndProc(ref Message m) 
	{
		if (!DesignMode)
			if (m.Msg == _wmReflect + _wmNotify)
			{
				var ipInfo = (NmIpAddress) Marshal.PtrToStructure(m.LParam, typeof (NmIpAddress));
				if (ipInfo.Hdr.Code == _ipnFirst)
					if (_values[ipInfo.Field] != ipInfo.Value)
					{
						_values[ipInfo.Field] = ipInfo.Value;
						OnFieldChanged(new FieldChangedEventArgs(ipInfo.Field, ipInfo.Value));
					}
			}

		base.WndProc(ref m);
	}
}