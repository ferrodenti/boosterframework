using System.Collections;
using System.Reflection;
using System.Windows.Forms;

namespace Booster.WinForms.Expanders;

public static class ComboBoxExpander
{
	static MethodInfo _miRefreshItems;

	public static void RefreshItems(this ComboBox comboBox)
	{
		if (_miRefreshItems == null)
			_miRefreshItems = typeof(ComboBox).GetMethod("RefreshItems", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.InvokeMethod);

		_miRefreshItems?.Invoke(comboBox, new object[0]);
	}

	public static void SetItems(this ComboBox comboBox, IEnumerable newItems)
	{
		var txt = comboBox.Text;
		comboBox.Items.Clear();

		foreach (var it in newItems)
		{
			comboBox.Items.Add(it);
			if (comboBox.GetItemText(it) == txt)
				comboBox.SelectedItem = it;
		}
	}

	public static bool SelectByText(this ComboBox comboBox, string text)
	{
		foreach (var it in comboBox.Items)
			if (comboBox.GetItemText(it) == text)
			{
				comboBox.SelectedItem = it;
				return true;
			}

		comboBox.SelectedItem = null;
		return false;
	}

}