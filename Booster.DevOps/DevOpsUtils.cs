﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Booster.DevOps.Configuration;
using Booster.FileSystem;
using Booster.Log;
using Booster.Parsing;
using JetBrains.Annotations;

#nullable enable

namespace Booster.DevOps;

public static class DevOpsUtils
{
	static FastLazy<Encoding>? _win1251;
	public static Encoding Win1251 => _win1251 ??= new(() =>
	{
#if NETSTANDARD
		Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
#endif
		return Encoding.GetEncoding(1251);
	});
	
	static FastLazy<ILog?>? _log;

	public static ILog? Log
	{
		get => (_log ??= new FastLazy<ILog?>(() => Config.Instance.Log.Create(typeof(DevOpsUtils)))).Value;
		set => _log = new FastLazy<ILog?>(value);
	}

	internal static TaskRepeatOptions TaskRepeatOptions => new() {Log = Log, Throw = true};

	#region MainXXX

	public static int Main([InstantHandle] Action<ILog> proc, string[]? args = null, string? successMessage = "DEPLOY COMPLETE")
		=> Main(log =>
		{
			proc(log);
			return 0;
		}, args);

	public static int Main([InstantHandle] Func<ILog, int> proc, string[]? args = null, string? successMessage = "DEPLOY COMPLETE")
	{
		try
		{
			var result = proc(Log!);

			if (successMessage != null)
				Log.Info(successMessage);

			return result;
		}
		catch (Exception e)
		{
			Log.Fatal(e);
			return -1;
		}
		finally
		{
			System.Console.ReadKey();
		}
	}

	public static Task<int> MainAsync([InstantHandle] Func<ILog, Task> proc, string[]? args = null, string? successMessage = "DEPLOY COMPLETE")
		=> MainAsync(async log =>
		{
			await proc(log).NoCtx();
			return 0;
		}, args);

	public static async Task<int> MainAsync([InstantHandle] Func<ILog, Task<int>> proc, string[]? args = null, string? successMessage = "DEPLOY COMPLETE")
	{
		try
		{
			var result = await proc(Log!).NoCtx();

			if (successMessage != null)
				Log.Info(successMessage);

			return result;
		}
		catch (Exception e)
		{
			Log.Fatal(e);
			return -1;
		}
		finally
		{
			System.Console.ReadKey();
		}
	}

	#endregion
	
	public static void DeleteFiles(params string[] masks)
	{
		var current = FilePath.CurrectDirectory;

		foreach (var file in EnumerableFiles(current, masks))
			FileUtils.DeleteFile(file, TaskRepeatOptions);
	}

	public static void ClearDirectory(FilePath path)
	{
		foreach (FilePath dir in Directory.GetDirectories(path, "*", SearchOption.AllDirectories)) 
			FileUtils.DeleteDirectory(dir, true, TaskRepeatOptions);

		foreach (FilePath file in Directory.GetFiles(path, "*", SearchOption.AllDirectories))
			FileUtils.DeleteFile(file, TaskRepeatOptions);
	}

	public static IDisposable Cd(FilePath path)
	{
		var initial = FilePath.CurrectDirectory;
		FilePath.CurrectDirectory = initial.Combine(path);
		return new ActionDisposable(() => FilePath.CurrectDirectory = initial);
	}
		
	public static void CopyFile(FilePath source, FilePath destination, bool overwrite = true)
		=> FileUtils.CopyFile(source, destination, overwrite, TaskRepeatOptions);

	public static void MoveFiles(string mask, FilePath destination, bool overwrite = true, bool keepStructure = true)
		=> MoveFiles(new[] { mask }, destination, overwrite, keepStructure);

	public static void MoveFile(FilePath src, FilePath dst, bool overwrite = true)
		=> FileUtils.MoveFile(src, dst, true, TaskRepeatOptions);

	public static bool DirectoryExists(FilePath path)
	{
		var dst = Config.Resolve(path).MakeAbsolute();
		return Directory.Exists(dst);
	}

	public static void MoveFiles(string[] masks, FilePath destination, bool overwrite = true, bool keepStructure = true)
	{
		var current = FilePath.CurrectDirectory;
		var dst = Config.Resolve(destination).MakeAbsolute();

		FileUtils.EnsureDirectory(dst, TaskRepeatOptions);

		foreach (FilePath file in EnumerableFiles(current, masks))
		{
			var rel = keepStructure ? file.MakeRelative(current) : file.Filename!;
			FileUtils.MoveFile(file, dst + rel, overwrite, TaskRepeatOptions);
		}
	}

	public static void RemoveDirectory(FilePath path)
		=> FileUtils.DeleteDirectory(path, true, TaskRepeatOptions);

	public static IEnumerable<string> EnumerableFiles(string directory, string[] masks)
	{
		var ons = new List<string>();
		var offs = new List<string>();

		foreach (var mask in masks)
			if (mask.StartsWith("**"))
				offs.Add(mask.SubstringSafe(2));
			else
				ons.Add(mask);

		var current = FilePath.CurrectDirectory;

		foreach (FilePath file in Directory.GetFiles(current, "*", SearchOption.AllDirectories))
		{
			var rel = file.MakeRelative(current);

			if (ons.Any(mask => Wildcards.Compare(rel, mask)) &&
				offs.None(mask => Wildcards.Compare(rel, mask)))
				yield return file;
		}
	}

}