﻿using System;
using System.Xml.Serialization;

namespace Booster.DevOps.VersionUpdater.Configuration;

public abstract class BaseUpdate
{
	[XmlAttribute]
	public bool Optional { get; set; }

	[XmlAttribute] public UpdateValue Value { get; set; } = UpdateValue.Version;

	protected bool ShouldSerializeOptional()
		=> Optional;
	
	public abstract void Validate();
	public abstract void Replace(UpdateData data);

	protected void Replace(UpdateData data, string open, string close)
	{
		var i = data.Text.IndexOf(open);
		if (i < 0)
		{
			if (Optional)
				return;
			
			throw new Exception($"{data.Filename}: {open} not found! Add {Value} manualy, then repeat");
		}

		i += open.Length;
		var j = data.Text.IndexOf(close, i);
		if (j < 0)
			throw new Exception($"{data.Filename}: {close} not found! Invalid file format?");

		var value = data.Get(Value);

		data.Update(i, j, value);
	}
}