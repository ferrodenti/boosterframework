﻿using System;
using System.IO;
using System.Xml.Serialization;
using Booster.Log;

#nullable enable

namespace Booster.DevOps.VersionUpdater.Configuration;

[XmlRoot("Config")]
public class VersionConfig
{
	string? _versionString;
	[XmlElement("Version")]
	public string? VersionString
	{
		get => _versionString;
		set
		{
			_versionString = value;
			_version = null;
		}
	}

	FastLazy<Version?>? _version;
	[XmlIgnore]
	public Version? Version
	{
		get => _version ??= new(() =>
		{
			Version? result = null;

			if (VersionString.IsSome() && Version.TryParse(VersionString, out result))
				result = result.MakePositive();

			return result;
		});
		set => _version = value;
	}

	[XmlAttribute]
	public bool RelativeToMe { get; set; }
	
	[XmlElement("Group")]
	public TargetsGroup[]? Groups { get; set; }
	
	[XmlIgnore]
	public string? Filename { get; set; }

	static readonly XmlSerializer _serializer = new(typeof(VersionConfig));


	public VersionConfig SetGroups(params TargetsGroup[] groups)
	{
		Groups = groups;
		return this;
	}

	public static VersionConfig Load(string filename)
	{
		using var file = File.OpenRead(filename);
		var result = (VersionConfig)_serializer.Deserialize(file)!;
		result.Filename = filename;
		return result;
	}

	public static VersionConfig LoadAndValidate(string filename, ILog? log)
	{
		
		using var op = log.TryTrace($"Loading config: {filename}...");

		var config = Load(filename);
		var initialDir = Directory.GetCurrentDirectory();
		
		if (config.RelativeToMe)
		{
			var dir = Path.GetDirectoryName(filename);

			if (dir.IsSome())
				Directory.SetCurrentDirectory(dir);
		}

		config.Validate(log);
		
		op.Done();
		
		Directory.SetCurrentDirectory(initialDir);
		return config;
	}

	public void Validate(ILog? log)
	{
		if (Version == null!)
			throw new Exception("Expected non empty <Version> element");

		if (Groups == null || Groups.Length == 0)
			throw new Exception("Expected at least one <Group> element");

		foreach (var group in Groups)
			group.Validate(log);
	}
}