using System;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

namespace Booster.DevOps.VersionUpdater.Configuration;

public class RegexUpdate : BaseUpdate
{
	[XmlAttribute] public string? Pattern { get; set; }

	[XmlAttribute] public PadType Pad { get; set; } = PadType.None;
	[XmlAttribute] public int Length { get; set; }

	Regex _regex = null!;
	
	public override void Validate()
	{
		if (Pattern.IsEmpty())
			throw new ArgumentException("Expected non empty Pattern attribute");
		
		_regex = new Regex(Pattern, RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
	}

	public override void Replace(UpdateData data)
	{
		var i = 0;
		
		while (Match(data.Text, i, out var match))
		{
			var from = match.Index;
			var to = from + match.Length;

			PadMatch(ref from, ref to, data.Text.Length);
			
			var value = data.Get(Value);
			value = PadValue(Pad, Length, value);
			
			data.Update(from, to, value);

			i = from + value.Length;
		}
	}

	bool Match(string text, int start, out Match result)
	{
		result = _regex.Match(text, start);
		return result.Success;
	}

	void Move(PadType padType, ref int from, ref int to)
	{
		var length = to - from;
		var diff = Length - length;
		if (diff > 0)
			switch (padType)
			{
			case PadType.None:
				break;
			case PadType.Left:
				from -= diff;
				break;
			case PadType.Right:
				to += diff;
				break;
			case PadType.Center:
				var half = diff / 2;
				from -= half;
				to += diff - half;
				break;
			default:
				throw new ArgumentOutOfRangeException();
			}
	}
	
	void PadMatch(ref int from, ref int to, int length)
	{
		Move(Pad, ref from, ref to);

		if (from < 0)
		{
			from = 0;
			Move(PadType.Right, ref from, ref to);
			if (to > length)
			{
				to = length;
				return;
			}
		}

		if (to > length)
		{
			to = length;
			Move(PadType.Left, ref from, ref to);
			if (from < 0) 
				from = 0;
		}
	}
	
	public static string PadValue(PadType padType, int pad, string result)
		=> padType switch
		{
			PadType.None => result
			, PadType.Left => result.PadLeft(pad)
			, PadType.Right => result.PadRight(pad)
			, PadType.Center => PadCenter(result, pad)
			, _ => throw new ArgumentOutOfRangeException(nameof(padType), padType, null)
		};

	static string PadCenter(string value, int pad)
	{
		while (value.Length < pad)
		{
			value = $" {value}";

			if (value.Length < pad)
				value = $"{value} ";
		}

		return value;
	}
}