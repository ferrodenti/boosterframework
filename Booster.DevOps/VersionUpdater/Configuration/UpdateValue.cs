﻿namespace Booster.DevOps.VersionUpdater.Configuration;

public enum UpdateValue
{
	Version,
	Year,
	Filename
}