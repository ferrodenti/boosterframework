using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Booster.Log;
using Booster.Parsing;

#nullable enable

namespace Booster.DevOps.VersionUpdater.Configuration;

public class Target
{
	[XmlElement("Encoding")] public string EncodingName { get; set; } = "UTF-8";
	[XmlText] public string? Value { get; set; }

	[XmlElement] public bool Enabled { get; set; }
	[XmlElement] public bool Optional { get; set; }
	[XmlElement] public bool AllDirectories { get; set; } = true;
	
	[XmlElement("Exclude")]
	public string[]? Excludes { get; set; }
	
	[XmlIgnore]
	public Encoding Encoding = null!;

	[XmlIgnore]
	public (string filename, string text)[] Files = null!;

	public Target Exclude(params string[] excludes)
	{
		Excludes = excludes;
		return this;
	}

	public Target Enable(bool enable)
	{
		Enabled = enable;
		return this;
	}

	public void Validate(ILog? log)
	{
		Value = Value?.Trim();
		
		if (Value.IsEmpty())
			throw new Exception("Expceted non empty Value");

		Encoding = int.TryParse(EncodingName, out var codepage) 
			? Encoding.GetEncoding(codepage) 
			: Encoding.GetEncoding(EncodingName);

		if (!Enabled)
			return;

		Files = GetFiles(log).ToArray();
		
		if (Files.Length == 0)
		{
			var text = $"No files found matching the condition: {Value}";
			if (Optional)
				log.Warn(text);
			else
				throw new FileNotFoundException(text);
		}
	}

	IEnumerable<(string filename, string text)> GetFiles(ILog? log)
	{
		foreach (var filename in GetFilesNames(log))
		{
			if (!File.Exists(filename))
				throw new FileNotFoundException($"File not found: {filename}");
			
			var text = File.ReadAllText(filename, Encoding);
			yield return (filename, text);
		}
	}

	IEnumerable<string> GetFilesNames(ILog? log)
	{
		var value = Value!;
		if (value.Contains('*'))
		{
			var dir = Path.GetDirectoryName(value);
			var mask = Path.GetFileName(value);
				
			if (dir.IsEmpty())
				dir = Directory.GetCurrentDirectory();

			var option = AllDirectories ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
			foreach (var filename in Directory.GetFiles(dir, mask, option))
			{
				if (Excludes != null && Excludes.Any(wildcard => Wildcards.Compare(filename, wildcard)))
				{
					log.Trace($"Skipping {filename}");
					continue;
				}

				yield return filename;
			}
		}
		else
		{
			if (Path.IsPathRooted(value))
				yield return value;
			
			var dir = Directory.GetCurrentDirectory();
			yield return Path.Combine(dir, value);
		}
	}
}