﻿using System;
using System.Xml.Serialization;

#nullable enable

namespace Booster.DevOps.VersionUpdater.Configuration;

[Serializable]
public class TextUpdate : BaseUpdate
{
	[XmlAttribute] public virtual string? Open { get; set; }
	[XmlAttribute] public virtual string? Close { get; set; }

	protected virtual bool ShouldSerializeOpen() => true;
	protected virtual bool ShouldSerializeClose() => true;

	public override void Validate()
	{
		if (Open.IsEmpty())
			throw new Exception("Expceted non empty Open attribute");
		
		if (Close.IsEmpty())
			throw new Exception("Expceted non empty Close attribute");
	}
	
	public override void Replace(UpdateData data)
		=> Replace(data, Open!, Close!);
}