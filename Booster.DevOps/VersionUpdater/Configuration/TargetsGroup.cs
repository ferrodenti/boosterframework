﻿using System;
using System.Xml.Serialization;
using Booster.Log;

#nullable enable

namespace Booster.DevOps.VersionUpdater.Configuration;

public class TargetsGroup
{
	[XmlElement(typeof(TextUpdate), ElementName = "TextUpdate")]
	[XmlElement(typeof(XmlUpdate), ElementName = "XmlUpdate")]
	[XmlElement(typeof(RegexUpdate), ElementName = "RegexUpdate")]
	public BaseUpdate[]? Updates { get; set; }
	
	[XmlElement("Target")]
	public Target[]? Targets { get; set; }
	
	[XmlElement("Exclude")]
	public string[]? Excludes { get; set; }

	public TargetsGroup SetUpdates(params BaseUpdate[] updates)
	{
		Updates = updates;
		return this;
	}

	public TargetsGroup SetTargets(params Target[] targets)
	{
		Targets = targets;
		return this;
	}

	public void Validate(ILog? log)
	{
		if (Updates == null || Updates.Length == 0)
			throw new Exception("Expected at least one <Text/XmlUpdate> element");
		
		if (Targets == null || Targets.Length == 0)
			throw new Exception("Expected at least one <Targets> element");

		foreach (var update in Updates)
			update.Validate();
		
		foreach (var target in Targets)
			target.Validate(log);
	}
}