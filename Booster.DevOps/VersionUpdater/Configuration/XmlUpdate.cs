﻿using System;
using System.Xml.Serialization;

namespace Booster.DevOps.VersionUpdater.Configuration;

public class XmlUpdate : BaseUpdate
{
	string? _open;
	string? _close;
	
	[XmlAttribute]
	public string? Tag
	{
		get => _tag;
		set
		{
			_tag = value;
			_open = $"<{_tag}>";
			_close = $"</{_tag}>";
		}
	}
	string? _tag;

	public override void Validate()
	{
		if (Tag.IsEmpty())
			throw new ArgumentException("Expected non empty Tag attribute");
	}

	public override void Replace(UpdateData data)
		=> Replace(data, _open!, _close!);
}