﻿namespace Booster.DevOps.VersionUpdater.Configuration;

public enum PadType
{
	None,
	Left,
	Right,
	Center
}