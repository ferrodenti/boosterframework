﻿using System;
using System.Reflection;
using Booster.Console;
using Booster.DevOps.VersionUpdater.Configuration;
using Booster.FileSystem;
using Booster.Log;
using Booster.Parsing;
using JetBrains.Annotations;
#if NETSTANDARD
using System.Text;
using System.Threading.Tasks;
#endif

#nullable enable

namespace Booster.DevOps.VersionUpdater;

[PublicAPI]
public class VersionUpdaterUtil
{
	public int Main(params string[] args)
	{
		var log = new AccumulatorLog(new ConsoleLog(), new TraceLog());
		try
		{
			log.Info($"VersionUpdater {Assembly.GetExecutingAssembly().GetName().Version}");

#if NETSTANDARD
			Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
#endif
			if (!ArgumentParser.TryParse(args, out VersionUpdaterArguments arguments, out var message))
			{
				log.Error(message);
				System.Console.ReadKey();
				return -1;
			}

			var fileLog = arguments.LogFilename.IsSome()
				? new FileLog(new FilePath(arguments.LogFilename).MakeRooted())
				: null;

			log.DumpAndBeNormal(fileLog);

			var config = VersionConfig.LoadAndValidate(arguments.Config, log);
			if (arguments.SetVersion.IsSome())
				config.VersionString = arguments.SetVersion;


			if (!arguments.NoInput && !UserInput(config, arguments.IncrementIndex))
				return 0;

			log.Info($"Updating version to: {config.Version}");

			Updater.UpdateAll(config, log);
			return 0;
		}
		catch (Exception e)
		{
			log.Error(e);
			System.Console.ReadKey();
			return -1;
		}
		finally
		{
			log.Flush();
		}
	}

	public static bool UserInput(VersionConfig config, int incrementIndex = 3)
	{
		var version = config.Version.Duplicate();

		if (incrementIndex >= 0)
			version = version.Increment(incrementIndex);

		if (UserInput(config.Version!, ref version))
		{
			config.Version = version;
			return true;
		}

		return false;
	}
	public static bool UserInput(Version initial, ref Version version)
	{
		using var _ = ConsoleHelper.Lock();

		System.Console.WriteLine("Increment:         [1] [2] [3] [4], done: [Enter], reset: [Backspace], exit [Escape] ");
		System.Console.WriteLine("Decrement:         [q] [w] [e] [r]");
		//				       [1] [2] [3] [4] => [1] [2] [3] [4]

		var done = false;
		while (!done)
		{
			System.Console.Write('\r');
			System.Console.Write(initial.ToString().PadLeft(16));
			System.Console.Write(" => ");
			System.Console.Write($"{version.Major}.".PadRight(4));
			System.Console.Write($"{version.Minor}.".PadRight(4));
			System.Console.Write($"{version.Build}.".PadRight(4));
			System.Console.Write($"{version.Revision}".PadRight(4));
			System.Console.Write(" ? ");

			var key = System.Console.ReadKey(true).Key;
			switch (key)
			{
			case ConsoleKey.Enter:
				done = true;
				break;
			case ConsoleKey.D1:
				version = version.Increment(0);
				continue;
			case ConsoleKey.D2:
				version = version.Increment(1);
				continue;
			case ConsoleKey.D3:
				version = version.Increment(2);
				continue;
			case ConsoleKey.D4:
				version = version.Increment(3);
				continue;

			case ConsoleKey.Q:
				version = version.Decrement(0);
				continue;
			case ConsoleKey.W:
				version = version.Decrement(1);
				continue;
			case ConsoleKey.E:
				version = version.Decrement(2);
				continue;
			case ConsoleKey.R:
				version = version.Decrement(3);
				continue;
			case ConsoleKey.Escape:
				System.Console.WriteLine();
				System.Console.WriteLine("Updating version cancelled");
				return false;
			case ConsoleKey.Backspace:
				version = initial.Duplicate();
				continue;
			default:
				continue;
			}
		}

		System.Console.WriteLine();
		return true;
	}
	
	public static bool UpdateVersion(VersionConfig config, ILog? log)
	{
		config.Validate(log);

		if (!UserInput(config))
			return false;

		Updater.UpdateAll(config, log);
		return true;
	}
}