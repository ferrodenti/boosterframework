﻿using System;
using System.IO;
using Booster.DevOps.VersionUpdater.Configuration;
using Booster.Log;

#nullable enable

namespace Booster.DevOps.VersionUpdater;

public class Updater
{
	readonly ILog? _log;
	readonly VersionConfig _versionConfig;
	readonly string _currentDirectory;

	public Updater(VersionConfig versionConfig, ILog? log)
	{
		_log = log;
		_versionConfig = versionConfig;
		_currentDirectory = Directory.GetCurrentDirectory();
	}

	public static void UpdateAll(VersionConfig config, ILog? log)
	{
		var updater = new Updater(config, log);
		updater.Update();

		if (config.Filename.IsSome())
			updater.UpdateConfig();
	}

	public void Update()
	{
		foreach (var group in _versionConfig.Groups!) 
			UpdateGroup(group);
	}

	public void UpdateConfig()
	{
		var group = new TargetsGroup
		{
			Targets = new Target[]
			{
				new()
				{
					Value = _versionConfig.Filename
				}
			}
			, Updates = new BaseUpdate[]
			{
				new XmlUpdate
				{
					Optional = false, Tag = "Version"
				}
			}
		};
		group.Validate(_log);
		
		UpdateGroup(group);
	}

	void UpdateGroup(TargetsGroup group)
	{
		foreach (var target in group.Targets!)
			if(target.Enabled)
				foreach (var (filename, text) in target.Files)
				{
					var data = new UpdateData(_versionConfig.Version!, filename, text, target.Encoding);
					UpdateFile(group, data);
				}
	}

	void UpdateFile(TargetsGroup group, UpdateData data)
	{
		var logFilename = data.Filename;
		if (logFilename.StartsWith(_currentDirectory))
			logFilename = logFilename.Substring(_currentDirectory.Length).TrimStart('\\');

		using var op = _log.TryTrace($"Updating {logFilename}...");

		var oldText = data.Text;

		foreach (var update in group.Updates!)
			update.Replace(data);

		if (data.Text == oldText)
		{
			op.Result = "nothing to update";
			return;
		}

		File.WriteAllText(data.Filename, data.Text, data.Encoding);
		op.Done();
	}

}