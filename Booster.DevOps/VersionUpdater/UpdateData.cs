﻿using System;
using System.Text;
using Booster.DevOps.VersionUpdater.Configuration;

namespace Booster.DevOps.VersionUpdater;

public class UpdateData
{
	public readonly Version Version;
	public readonly DateTime DateTime;
	public readonly string Filename;
	public string Text;
	public readonly Encoding Encoding;

	public UpdateData(Version version, string filename, string text, Encoding encoding)
	{
		Version = version;
		Filename = filename;
		Text = text;
		Encoding = encoding;
		DateTime = DateTime.Now;
	}

	public string Get(UpdateValue value)
		=> value switch
		{
			UpdateValue.Version => Version.ToString(),
			UpdateValue.Year => DateTime.Year.ToString(), 
			UpdateValue.Filename => Filename,
			_ => throw new ArgumentOutOfRangeException(nameof(value), value, null)
		};

	public void Update(int from, int to, string value)
		=> Text = string.Concat(Text.SubstringSafe(0, from), value, Text.SubstringSafe(to));
}