﻿using System;
using Booster.Log;

#nullable enable

namespace Booster.DevOps.VersionUpdater;

public class LogResult : IDisposable
{
	ILog? _log;
	public readonly LogLevel LogLevel;
	public readonly string StartMessage;
	public string? Result;

	public LogResult(ILog? log, LogLevel logLevel, string startMessage)
	{
		_log = log;
		LogLevel = logLevel;
		StartMessage = startMessage;
	}

	public void Done()
	{
		Result = "done";
		Dispose();
	}

	public void Dispose()
	{
		_log?.Write(LogLevel, $"{StartMessage}{Result}");
		_log = null;
	}
}