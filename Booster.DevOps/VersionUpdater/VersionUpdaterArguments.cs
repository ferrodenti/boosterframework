﻿using Booster.Parsing;

namespace Booster.DevOps.VersionUpdater;

[CommandArgumentsDescription(ExecutableName = "VersionUpdater.exe", Description = "VersionUpdater", IgnoreUnknownArguments = false)]
public class VersionUpdaterArguments
{
	[PositionalCommandArgument("Config filename path", "config")]
	public string Config = "version.xml";

	public int IncrementIndex = -1;

	[OptionalCommandArgument("--inc1", "Increment major number")]
	void Inc1()
		=> IncrementIndex = 0;

	[OptionalCommandArgument("--inc2", "Increment minor number")]
	void Inc2()
		=> IncrementIndex = 1;

	[OptionalCommandArgument("--inc3", "Increment revision number")]
	void Inc3()
		=> IncrementIndex = 2;

	[OptionalCommandArgument("--inc4", "Increment build number")]
	void Inc4()
		=> IncrementIndex = 3;

	[OptionalCommandArgument("--set", "Set version", "NEW VERSION")]
	public string? SetVersion;

	[OptionalCommandArgument("--log", "Write output to file", "FILENAME")]
	public string? LogFilename;

	[OptionalCommandArgument("--no-input", "Skip user input")]
	public bool NoInput;
}