﻿using Booster.DevOps.VersionUpdater;
using Booster.Log;

#nullable enable

namespace Booster.DevOps;

public static class LogExtension
{
	public static LogResult TryTrace(this ILog? log, string startMessage)
		=> new LogResult(log, LogLevel.Trace, startMessage);

	public static LogResult TryInfo(this ILog? log, string startMessage)
		=> new LogResult(log, LogLevel.Info, startMessage);
}