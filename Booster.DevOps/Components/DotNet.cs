﻿using Booster.DevOps.Configuration;
using Booster.FileSystem;
using JetBrains.Annotations;

namespace Booster.DevOps.Components;

[PublicAPI]
public class DotNet : BaseCompiller<DotNet>
{
	public string? Action { get; set; } = "publish";
	
	public bool Force { get; set; }
	
	public bool Quiet { get; set; }
	public bool ClpErrorsOnly { get; set; } = true;

	protected DotNet() :
		base("$dotnet$")
	{
		MakeAbs = false;
		CheckExistance = false;
	}

	protected override void BuildArguments(EnumBuilder builder)
	{

		if (Action.IsSome())
			builder.Append(Action);
		
		if (Force)
			builder.Append("--force");
		
		if (NoLogo)
			builder.Append("--nologo");
		
		if (Quiet)
			builder.Append("-v q");
		
		if (Configuration.IsSome())
			builder.Append($"-c {Configuration}");

		if (Destination.IsSome())
			builder.Append($"-o {Config.Resolve(Destination)}");
		
		if (Source.IsSome())
			builder.Append(Config.Resolve(Source));

		if (WarningLevel >=0)
			builder.Append($"--property WarningLevel={WarningLevel}");
	}
}