﻿using System;
using System.IO;
using Booster.FileSystem;
using JetBrains.Annotations;


#nullable enable

namespace Booster.DevOps.Components;

[PublicAPI]
public class WinRar : BaseComponent<WinRar>
{
	public string Action { get; set; } = "a"; //Archive

	public FilePath? Destination { get; set; }
	public FilePath? Source { get; set; }
	public FilePath? CommentFile { get; set; }

	public bool RecurseSubFolders { get; set; } = true;
	public bool Solid { get; set; } = true;
	public bool UpdateTime { get; set; } = true;
	public bool ExcludeBaseFolder { get; set; } = true;
	public bool DisableErrorMessages { get; set; }
	public bool DeleteSourceFiles { get; set; } = true;

	public int CompressionMethod { get; set; } = 5;

	public Version? BannerVersion { get; set; }
	public string? BannerId { get; set; }

	protected WinRar() :
		base("$winrar$")
	{
	}

	public override void Execute(params string[] args)
	{
		using var banner = CreateBanner(BannerId, BannerVersion);
		base.Execute(args);
	}

	protected override void BuildArguments(EnumBuilder builder)
	{
		if (Action.IsSome())
			builder.Append(Action);

		if (CommentFile.IsSome())
			builder.Append($"-z{CommentFile}");

		if (RecurseSubFolders)
			builder.Append("-r");

		if (CompressionMethod>=0)
			builder.Append($"-m{CompressionMethod}");

		if (Solid)
			builder.Append("-s");

		if (DeleteSourceFiles)
			builder.Append("-df");

		if (UpdateTime)
			builder.Append("-tl");

		if (ExcludeBaseFolder)
			builder.Append("-ep1");

		if (DisableErrorMessages)
			builder.Append("-inul");


		if (Destination.IsSome())
			builder.Append(Destination);

		if (Source.IsSome())
			builder.Append(Source);
	}

	public IDisposable? CreateBanner(string? name, Version? version, FilePath? filename = null)
	{
		var banner = GetBanner(name, version);
		if (banner == null)
			return null;

		filename ??= "rar_comment.txt";
		FileUtils.DeleteFile(filename);
		File.WriteAllText(filename, banner, DevOpsUtils.Win1251);

		var commentFile = CommentFile;
		CommentFile = filename;

		return new ActionDisposable(() =>
		{
			CommentFile = commentFile;
			FileUtils.DeleteFile(filename);
		});
	}

	public static string? GetBanner(string? name, Version? version)
	{
		var ver = (version?.ToString() ?? "???.???.???.???").PadLeft(15);
		return name?.ToLower() switch
		{
			"int" =>
				"*****************************************\r\n" +
				"*                                       *\r\n" +
				"*  ВНИМАНИЕ! НЕ РАСПРОСТРАНЯТЬ!         *\r\n" +
				"*  Версия для внутреннего использования *\r\n" +
				$"*         {ver}               *\r\n" +
				"*                                       *\r\n" +
				"*****************************************",
			"def" =>
				"**********************************\r\n" +
				"*                                *\r\n" +
				$"*    Версия    {ver}   *\r\n" +
				"*                                *\r\n" +
				"**********************************",
			_ => null
		};
	}
}