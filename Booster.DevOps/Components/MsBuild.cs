﻿using Booster.DevOps.Configuration;
using Booster.FileSystem;
using JetBrains.Annotations;

#nullable enable

namespace Booster.DevOps.Components;

[PublicAPI]
public class MsBuild : BaseCompiller<MsBuild>
{
	public bool Quiet { get; set; }
	public bool ClpErrorsOnly { get; set; } = true;

	protected MsBuild() :
		base("$msbuild$")
	{
	}

	protected override void TargersChanged()
	{
		
	}

	protected override void BuildArguments(EnumBuilder builder)
	{
		if (Source.IsSome())
			builder.Append(Config.Resolve(Source));

		if (NoLogo)
			builder.Append("--nologo");

		if (Quiet)
			builder.Append("-v q");
		
		if (ClpErrorsOnly)
			builder.Append("/clp:ErrorsOnly");

		if (Configuration.IsSome())
			builder.Append($"/p:Configuration={Configuration}");

		if (WarningLevel >=0)
			builder.Append($"/p:WarningLevel={WarningLevel}");

		if (RelativeDestination.IsSome()) 
			builder.Append($"/p:OutputPath={RelativeDestination}");
	}
}