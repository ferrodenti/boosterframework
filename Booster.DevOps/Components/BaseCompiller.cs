﻿using Booster.FileSystem;

#nullable enable

namespace Booster.DevOps.Components;

public class BaseCompiller<TThis> : BaseComponent<TThis>
	where TThis : BaseCompiller<TThis>
{
	public bool ClearDir { get; set; } = true;
	public bool CleanUp { get; set; } = true;
	public string? Configuration { get; set; } = "Release";
	public int WarningLevel { get; set; }
	public bool NoLogo { get; set; } = true;

	FilePath? _source;
	public FilePath? Source
	{
		get => _source;
		set
		{
			_source = value;
			TargersChanged();
		}
	}

	FilePath? _destination;
	public FilePath? Destination
	{
		get => _destination;
		set
		{
			_destination = value;
			TargersChanged();
		}
	}

	FilePath? _absoluteDestination;
	public FilePath? AbsoluteDestination
	{
		get => _absoluteDestination ??= Destination?.MakeAbsolute();
		set
		{
			_absoluteDestination = value;
			_relativeDestination = null;
		}
	}

	FastLazy<FilePath?>? _relativeDestination;
	public FilePath? RelativeDestination
	{
		get => _relativeDestination ??= new(() =>
		{
			if (Source.IsEmpty())
				return Destination;

			var dst = AbsoluteDestination ?? FilePath.CurrectDirectory;
			var src = Source.MakeAbsolute().Directory;
			return dst.MakeRelative(src);
		});
		set => _relativeDestination = value;
	}


	protected BaseCompiller(FilePath location) 
		: base(location)
	{
	}

	protected virtual void TargersChanged()
	{
		_absoluteDestination = null;
		_relativeDestination = null;
	}

	public override void Execute(params string[] args)
	{
		if (ClearDir && Destination.IsSome())
			DevOpsUtils.ClearDirectory(Destination);

		base.Execute(args);

		if (CleanUp && Destination.IsSome())
			DevOpsUtils.DeleteFiles(Destination + "*.pdb", Destination + "*.xml");
	}
}