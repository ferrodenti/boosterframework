﻿using Booster.FileSystem;
using JetBrains.Annotations;

namespace Booster.DevOps.Components;

[PublicAPI]
public class SentinelEnvelope : BaseComponent<SentinelEnvelope>
{
	public FilePath? SourceProject { get; set; }

	protected SentinelEnvelope() :
		base("$envelope$")
	{
	}

	protected override void BuildArguments(EnumBuilder builder)
	{
		if (SourceProject.IsSome())
			builder.Append($"-p {SourceProject}");
	}
}