﻿using System;
using System.Diagnostics;
using System.IO;
using Booster.DevOps.Configuration;
using Booster.FileSystem;
using Booster.Log;
using Booster.Reflection;

#nullable enable

namespace Booster.DevOps.Components;

public class BaseComponent<TTHis> where TTHis : BaseComponent<TTHis>
{
	ILog? _log;
	public readonly FilePath Path;

	FastLazy<FilePath>? _resolvedPath;

	public FilePath ResolvedPath => _resolvedPath ??= new(() => {
		{
			var result = Config.Resolve(Path);
			return MakeAbs ? result.MakeAbsolute() : result;
		}
	});

	protected bool MakeAbs = true;
	protected bool CheckExistance = true;


	protected BaseComponent(FilePath location)
		=> Path = location;

	public virtual TTHis Check()
	{
		_log = Config.Instance.Log.Create(this);

		if (CheckExistance)
		{
			var resolved = ResolvedPath;
			if (resolved.DirectoryEnding)
			{
				if (!Directory.Exists(resolved))
					throw new DirectoryNotFoundException($"Path not found: {resolved}");
			}
			else if (!File.Exists(resolved))
				throw new FileNotFoundException($"Path not found: {resolved}");
		}

		return (TTHis)this;
	}

	public static TTHis Require()
		=> TypeEx.Create<TTHis>().Check();

	public virtual void Execute(params string[] args)
	{
		var bld = new EnumBuilder(" ");

		BuildArguments(bld);
		foreach (var arg in args)
			bld.AppendIfSome(arg);

		using var op = _log.TryInfo($"{LogAction(bld)}:...");

		var pInfo = new ProcessStartInfo
		{
			UseShellExecute = false, 
			CreateNoWindow = false,
			WindowStyle = ProcessWindowStyle.Normal,
			FileName = ResolvedPath,
			Arguments = bld.ToString()
		};
		var process = Process.Start(pInfo)!;

		process.WaitForExit();
		
		if (process.ExitCode != 0)
			throw new Exception($"process.ExitCode = {process.ExitCode}");

		op.Done();
	}

	protected virtual string LogAction(EnumBuilder arguments)
		=> $"{ResolvedPath.Filename} {arguments}";

	protected virtual void BuildArguments(EnumBuilder builder)
	{

	}
}