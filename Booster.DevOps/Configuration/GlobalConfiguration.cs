﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Booster.FileSystem;
using Booster.Parsing;

#nullable enable

namespace Booster.DevOps.Configuration;

[Serializable]
public class GlobalConfiguration
{
	[XmlElement("host")]
	public HostConfiguration[]? Hosts
	{
		get => _hosts;
		set => _hosts = value;
	}
	HostConfiguration[]? _hosts;

	public Dictionary<string, FilePath> GetHostConfiguration(string hostName)
	{
		var dictionary = new Dictionary<string, FilePath>(StringComparer.OrdinalIgnoreCase);

		if(Hosts != null)
			foreach (var configuration in Hosts)
				if (configuration.Name.IsSome() && Wildcards.Compare(hostName, configuration.Name))
					foreach (var pair in configuration.Dictionary)
						dictionary[pair.Key] = pair.Value;

		return dictionary;
	}

	static XmlSerializer _xmlSerializer = new(typeof(GlobalConfiguration));

	public static Dictionary<string, FilePath> Load(string path, string hostName)
	{
		var expanded = Environment.ExpandEnvironmentVariables(path); 
		using var file = File.OpenRead(expanded);
		var config = (GlobalConfiguration)_xmlSerializer.Deserialize(file);
		return config.GetHostConfiguration(hostName);
	}
}