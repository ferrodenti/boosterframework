﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Booster.FileSystem;

#nullable enable

namespace Booster.DevOps.Configuration;

[Serializable]
public class HostConfiguration
{
	[XmlAttribute("name")]
	public string? Name { get; set; }

	[XmlElement("var")]
	public Variable[]? Variables
	{
		get => _variables;
		set
		{
			_variables = value;
			_dictionary = null;
		}
	}
	Variable[]? _variables;

	IReadOnlyDictionary<string, FilePath>? _dictionary;
	public IReadOnlyDictionary<string, FilePath> Dictionary => _dictionary ??= Variables.ToDictionarySafe(v => v.Name!, v => new FilePath(v.Value));

	public HostConfiguration()
	{
	}

	public HostConfiguration(string name, IReadOnlyDictionary<string, FilePath> dictionary)
	{
		Name = name;
		Variables = dictionary.Select(p => new Variable {Name = p.Key, Value = p.Value}).ToArray();
	}
}