﻿using System;
using System.Xml.Serialization;

#nullable enable

namespace Booster.DevOps.Configuration;

[Serializable]
public class Variable
{
	[XmlAttribute("name")]
	public string? Name { get; set; }

	[XmlText]
	public string? Value { get; set; }
}