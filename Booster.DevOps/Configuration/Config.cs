﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Booster.FileSystem;
using Booster.Helpers;
using Booster.Log;
using JetBrains.Annotations;


#nullable enable

namespace Booster.DevOps.Configuration;

[PublicAPI]
public class Config
{
	public static Config Instance = new();

	public ILog? Log { get; set; } = new Log.Log(new ConsoleLog(), new TraceLog());
	public FilePath GlobalConfigurationPath { get; set; } = @"%SHARED_BIN%\global.deploy.config";
	public string HostName { get; set; } = Environment.GetEnvironmentVariable("COMPUTERNAME") ?? "???";


	FastLazy<string?>? _buildConfiguration;
	public string? BuildConfiguration
	{
		get => _buildConfiguration ??= new FastLazy<string?>(() =>
		{
			var assy = AssemblyHelper.BoosterCallingAssembly;
			var assemblyConfigurationAttribute = assy.GetCustomAttribute<AssemblyConfigurationAttribute>();
			var buildConfigurationName = assemblyConfigurationAttribute?.Configuration;
			return buildConfigurationName;
		});
		set => _buildConfiguration = value;
	}

	Dictionary<string, FilePath>? _variables;
	public Dictionary<string, FilePath> Variables => _variables ??= GlobalConfiguration.Load(GlobalConfigurationPath, HostName);

	public static FilePath Resolve(FilePath path)
	{
		var expanded = path.ToString(); 

		foreach (var pair in Instance.Variables)
			expanded = expanded.Replace(pair.Key, pair.Value);
		
		expanded = Environment.ExpandEnvironmentVariables(expanded);

		return expanded;
	}

}