using System;
using Booster.Caching.Interfaces;

#nullable enable

namespace Booster.Caching;

class CacheLazyLogic // Tут доступ по идее должен быть синхронным
{
	readonly ICacheLazy _target;
	readonly object?[] _keys;

	string? GetArgKey(object? o)
		=> o switch
		{
			null => "(null)",
			_ => StringConverter.Default.ToString(o)
		};

	string? _key;
	public string Key
	{
		get
		{
			if (_key == null)
			{
				var keys = _keys;
				var str = new EnumBuilder(":");
				str.Append($"{GetArgKey(keys[0])}<{_target.ValueType.FullName}>");

				for (var i = 1; i < keys.Length; i++)
					str.Append(GetArgKey(keys[i]));
			
				_key = str;
			}

			return _key;
		}
	}

	FastLazy<CacheSettings>? _cacheSettings;
	public CacheSettings Settings => _cacheSettings ??= new CacheSettings
	{
		SlidingExpiration = TimeSpan.Zero
	};
		

	public CacheLazyLogic(ICacheLazy target, object?[] keys)
	{
		if (keys.Length > 0)
			_keys = keys;
		else
			throw new ArgumentNullException(nameof(keys));
			
		_target = target;
	}

	public override string ToString() 
		=> $"{Key}: {_target}";
}