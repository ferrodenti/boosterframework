using System;
using System.Threading.Tasks;
using Booster.Caching.Interfaces;
using Booster.DI;
using Booster.Interfaces;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Caching;

[PublicAPI]
public class CacheAsyncLazy<T> : DependentAsyncLazy<T>, ICacheLazy
{
	readonly CacheLazyLogic _logic;
		
	public bool DependOnInternal { get; set; } = true;
	public bool UseLocal { get; set; }
	public CacheSettings Settings => _logic.Settings;
	public CacheLazyPriority Priority { get; set; } = CacheLazyPriority.Default;
	public string Key => _logic.Key;

		
	public CacheAsyncLazy(params object?[] keys) 
		=> _logic = new CacheLazyLogic(this, keys);

	public Task<T> GetAsync(Func<Task<T>> factory)
		=> GetAsync(null, factory);
	
	public Task<T> GetAsync(object? target, Func<Task<T>> factory)
	{
		Factory = factory;
		return GetValue(target);
	}

	public override Task<T> GetValue(object? target)
		=> UseLocal 
			? base.GetValue(target) 
			: CreateValue(target);

	protected override Task<T> CreateValue(object? target) 
		=> Instance<ICacheWrapper>.Value.GetOrAddAsync(Key, this, () => base.CreateValue(target));

	protected override ILazyDependency CreateDependecy() 
		=> Instance<ICacheWrapper>.Value.CreateLazyDependency(this);


	public override void Reset()
	{
		lock (this)
		{
			base.Reset();
			Instance<ICacheWrapper>.Value.Clear(Key);
		}
	}

	public override void SetValue(T value)
	{
		lock (this)
		{
			Instance<ICacheWrapper>.Value.Set(Key, value, this);
				
			if (UseLocal)
				SetInternal(value);
		}
	}

	public override string ToString()
		=> _logic.ToString();	
}