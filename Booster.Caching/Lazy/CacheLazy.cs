using System;
using Booster.Caching.Interfaces;
using Booster.DI;
using Booster.Interfaces;

#nullable enable

namespace Booster.Caching;

public class CacheLazy<T> : DependentLazy<T>, ICacheLazy
{
	readonly CacheLazyLogic _logic;
		
	public bool DependOnInternal { get; set; } = true;
	public bool UseLocal { get; set; }
	public CacheSettings Settings => _logic.Settings;
	public CacheLazyPriority Priority { get; set; } = CacheLazyPriority.Default;
	public string Key => _logic.Key;
		
	public CacheLazy(params object?[] keys)
		=> _logic = new CacheLazyLogic(this, keys);

	public T Get(Func<T> getter)
		=> Get(null, getter);

	public T Get(object? target, Func<T> getter)
	{
		Factory = getter;
		return GetValue(target);
	}

	public override T GetValue(object? target) 
		=> UseLocal 
			? base.GetValue(target) 
			: CreateValue(target);

	protected override T CreateValue(object? target) 
		=> Instance<ICacheWrapper>.Value.GetOrAdd(Key, this, () => base.CreateValue(target));
		
	protected override ILazyDependency CreateDependecy() 
		=> Instance<ICacheWrapper>.Value.CreateLazyDependency(this);
		
	public override void SetValue(T value)
	{
		lock (this)
		{
			Instance<ICacheWrapper>.Value.Set(Key, value, this);
				
			if (UseLocal)
				SetInternal(value);
		}
	}

	public static implicit operator T(CacheLazy<T> cache) 
		=> cache.Value;

	public override string ToString()
		=> _logic.ToString();
}