using System;
using System.Threading.Tasks;
using Booster.Caching.Interfaces;
using Booster.DI;
using Booster.Interfaces;
using Booster.Synchronization;

namespace Booster.Caching.Kitchen;

#nullable enable

[InstanceFactory(typeof(ICacheWrapper))]
public abstract class BaseCacheWrapper : ICacheWrapper
{
	readonly HashLocksPool _locks = new(1024);

	public abstract ILazyDependency CreateLazyDependency(ICacheLazy target);
	public abstract CacheEntry? GetInt(string key);
	protected abstract void SetInt(string key, CacheEntry entry, CacheSettings settings, CacheLazyPriority priority);

	public abstract void ClearAll();
	public abstract void Clear(string key);

	public void Set(string key, object? value, ICacheLazy lazy) 
		=> SetInt(key, new CacheEntry(value, lazy.GetDependency(false)), lazy.Settings, lazy.Priority);

	static void AddPrevCacheDependency(ILazy lazy)
	{
		var prev = CurrentLazy.Value as ICacheLazy;
		if (prev?.DependOnInternal == true)
			prev.GetDependency().Add(lazy);
	}

	public bool TryGet<T>(string key, out T value, out ILazyDependency? dependency)
	{
		var entry = GetInt(key);
		if (entry != null)
		{
			dependency = entry.Dependency;

			switch (entry.Value)
			{
			case null when !typeof(T).IsValueType:
				value = default!;
				return true;
			case T result:
				value = result;
				return true;
			}
		}

		dependency = null;
		value = default!;
		return false;
	}

	public T GetOrAdd<T>(string key, ICacheLazy lazy, Func<T> creator)
	{
		AddPrevCacheDependency(lazy);

		if (!TryGet<T>(key, out var result, out var dep))
			using (_locks.Lock(key))
				if (!TryGet(key, out result, out dep))
				{
					lazy.GetDependency(false)?.Reset();
					result = creator();
					Set(key, result, lazy);
				}

		if (lazy.UseLocal && dep != null)
			lazy.GetDependency().Add(dep);

		return result!;
	}

	public async Task<T> GetOrAddAsync<T>(string key, ICacheLazy lazy, Func<Task<T>> creator)
	{
		AddPrevCacheDependency(lazy);

		if (!TryGet<T>(key, out var result, out var dep))
		{
			using var _ = await _locks.LockAsync(key).NoCtx();

			if (!TryGet(key, out result, out dep))
			{
				lazy.GetDependency(false)?.Reset();
				result = await creator().NoCtx();
				Set(key, result, lazy);
			}
		}

		if(lazy.UseLocal && dep != null)
			lazy.GetDependency().Add(dep);

		return result!;
	}
}