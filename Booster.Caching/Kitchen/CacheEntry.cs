using Booster.Interfaces;

namespace Booster.Caching.Kitchen;

#nullable enable

public class CacheEntry
{
	public readonly ILazyDependency Dependency;
	public readonly object? Value;

	public CacheEntry(object? value, ILazyDependency dependency)
	{
		Value = value;
		Dependency = dependency;
	}
}