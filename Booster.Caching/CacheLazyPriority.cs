﻿using JetBrains.Annotations;

namespace Booster.Caching;

[PublicAPI]
public enum CacheLazyPriority
{
	Low = 1,
	BelowNormal = 2,
	Default = 3,
	Normal = 3,
	AboveNormal = 4,
	High = 5,
	NotRemovable = 6,
}