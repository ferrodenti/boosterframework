﻿using System;
using System.Threading.Tasks;
using Booster.Interfaces;

namespace Booster.Caching.Interfaces;

#nullable enable

public interface ICacheWrapper
{
	ILazyDependency CreateLazyDependency(ICacheLazy target);
		
	bool TryGet<T>(string key, out T? entry, out ILazyDependency? dependency);
	void Set(string key, object? value, ICacheLazy lazy);

	T GetOrAdd<T>(string key, ICacheLazy lazy, Func<T> creator);
	Task<T> GetOrAddAsync<T>(string key, ICacheLazy lazy, Func<Task<T>> creator);
		
	void ClearAll();
	void Clear(string key);
}