﻿using Booster.Interfaces;

namespace Booster.Caching.Interfaces;

public interface ICacheLazy : IDependentLazy
{
	string Key { get; }
	bool UseLocal { get; }
	bool DependOnInternal { get; }
	CacheSettings Settings { get; }
	CacheLazyPriority Priority { get; }
}