﻿using Booster.Caching.Interfaces;
using Booster.Caching.Kitchen;
using Booster.Collections;
using Booster.DI;
using Booster.Interfaces;
using Booster.Kitchen;
using JetBrains.Annotations;

namespace Booster.Caching;

[InstanceFactory(typeof(ICacheWrapper))]
[PublicAPI]
public class SimpleMemoryCache : BaseCacheWrapper
{
	readonly CacheDictionary<string, CacheEntry> _cacheDictionary = new();

	public CacheSettings CacheSettings => _cacheDictionary.Settings;
		
	public override ILazyDependency CreateLazyDependency(ICacheLazy target)
	{
		var result = new BasicLazyDependency(target);
		result.Changed += (_, _) => _cacheDictionary.Remove(target.Key); 
		return result;
	}

	public override CacheEntry GetInt(string key) 
		=> _cacheDictionary.SafeGet(key);

	protected override void SetInt(string key, CacheEntry entry, CacheSettings settings, CacheLazyPriority priority) 
		=> _cacheDictionary.Add(key, entry, settings.SlidingExpiration, settings.GetAbsoluteExpirationFromNow());

	public override void ClearAll() 
		=> _cacheDictionary.Clear();

	public override void Clear(string key)
		=> _cacheDictionary.Remove(key);
}