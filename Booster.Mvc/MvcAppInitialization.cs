#if !NETSTANDARD && !NETCOREAPP3
using System;
using System.Web.Mvc;
using Booster.Log;

namespace Booster.Mvc
{
	public static class MvcAppInitialization //TODO: std implementation
	{
		public static bool CatchError { get; set; } = true;
		public static ILog Log { get; set; }

		public static event EventHandler<EventArgs<Exception>> Error;
		 
		public static Exception InitializationError { get; private set; }

		public static void Init(Action initializationRoutine, GlobalFilterCollection filters = null)
		{
			if (CatchError)
				try
				{
					initializationRoutine();
				}
				catch (Exception ex)
				{
					Error?.Invoke(null, new EventArgs<Exception>(ex));

					Log?.Fatal(ex, "MvcAppInitialization: " + ex.Message);

					InitializationError = ex;

					(filters ?? GlobalFilters.Filters).Add(new InitializationErrorFilterAttribute(ex));
				}
			else
				initializationRoutine();
		}
	}
}
#endif