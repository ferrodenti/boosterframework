﻿using System.Configuration;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Booster.Mvc;

public class CustomConfigSectionHandler : IConfigurationSectionHandler
{
	public object Create(object parent, object configContext, XmlNode section)
		=> section.OuterXml;

	public static TConfig GetConfig<TConfig>(string sectionName, bool fixCase = false)
	{
		var xml = (string) ConfigurationManager.GetSection(sectionName);

		if (fixCase)
			xml = FixXmlCase(xml);

		using var reader = new StringReader(xml);
		var ser = new XmlSerializer(typeof(TConfig));
		return (TConfig) ser.Deserialize(reader);
	}

	static string FixXmlCase(string xml)
	{
		using var strReader = new StringReader(xml);
		using var reader = new XmlTextReader(strReader);
		using var strWriter = new StringWriter();
		using var writer = new XmlTextWriter(strWriter) {Formatting = Formatting.Indented};
		writer.WriteStartDocument();

		while (reader.Read())
			switch (reader.NodeType)
			{
			case XmlNodeType.Element:
				var name = reader.LocalName.ToUpperFirstLetter();
				var empty = reader.IsEmptyElement;
				writer.WriteStartElement(name);
				if (reader.MoveToFirstAttribute())
					do
					{
						name = reader.LocalName.ToUpperFirstLetter();
						writer.WriteAttributeString(name, reader.Value);
					} while (reader.MoveToNextAttribute());

				if (empty)
					writer.WriteEndElement();
				break;
			case XmlNodeType.Text:
				writer.WriteValue(reader.Value);
				break;
			case XmlNodeType.EndElement:
				writer.WriteEndElement();
				break;
			default:
				Utils.Nop();
				break;
			}

		writer.Flush();
		xml = strWriter.ToString();

		return xml;
	}
}