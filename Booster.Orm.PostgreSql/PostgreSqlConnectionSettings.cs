using System;
using Booster.Orm.Kitchen;
using Booster.Reflection;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Orm.PostgreSql;

[PublicAPI]
public sealed class PostgreSqlConnectionSettings : BaseConnectionSettings
{
	public override TypeEx OrmType => typeof (PostgreSqlOrm);

	public string? Endpoint
	{
		get => Read<string>("Endpoint");
		set => Write("Endpoint", value);
	}

	#region low Level settings

	public string? Server
	{
		get => Read<string>("Server");
		set => Write("Server", value);
	}

	public int Port
	{
		get => Read<int>("Port" /*, 5432*/);
		set => Write("Port", value);
	}

	public string? Database
	{
		get => Read<string>("Database");
		set => Write("Database", value);
	}

	public string? UserId
	{
		get => Read<string>("User ID");
		set => Write("User ID", value);
	}

	public string? Password
	{
		get => Read<string>("Password");
		set => Write("Password", value);
	}

	public bool IntegratedSecurity
	{
		get => Read<bool>("Integrated Security");
		set => Write("Integrated Security", value);
	}

	public int Timeout
	{
		get => Read<int>("Timeout");
		set => Write("Timeout", value);
	}

	public bool ConvertInfinityDateTime
	{
		get => Read<bool>("Convert Infinity DateTime");
		set => Write("Convert Infinity DateTime", value);
	}

	public int Protocol
	{
		get => Read("Protocol", 3);
		set => Write("Protocol", value);
	}

	// ReSharper disable once InconsistentNaming
	public bool SSL
	{
		get => Read<bool>("SSL");
		set => Write("SSL", value);
	}

	// ReSharper disable once InconsistentNaming
	public PostgreSqlSslMode SSLMode
	{
		get => Read<PostgreSqlSslMode>("SSLMode");
		set => Write("SSLMode", value);
	}

	public bool Pooling
	{
		get => Read<bool>("Pooling");
		set => Write("Pooling", value);
	}

	public int MinPoolSize
	{
		get => Read<int>("MinPoolSize");
		set => Write("MinPoolSize", value);
	}

	public int MaxPoolSize
	{
		get => Read<int>("MaxPoolSize");
		set => Write("MaxPoolSize", value);
	}

	public int ConnectionLifeTime
	{
		get => Read<int>("ConnectionLifeTime");
		set => Write("ConnectionLifeTime", value);
	}

	#endregion


	public PostgreSqlConnectionSettings(string connectionString) 
		: base(connectionString)
	{
		if (PostgreSqlOrm.NpgsqlVersion < new Version(6, 0) &&
			Read<string>("Convert Infinity DateTime").IsEmpty()) //For NpgSql 3.2.1
			ConvertInfinityDateTime = true;
	}

	protected override int DefaultPort => 5432;

	protected override bool IstHi(string key, string? value, ref Action? post)
	{
		switch (key)
		{
		case "endpoint":
			post = () =>
			{
				if (TryParseEndPoint(Endpoint, out var server, out var port))
				{
					Server = server;
					Port = port;
				}
			};
			return true;

		case "server":
		case "port":
			post = () => Endpoint = $"{Server}:{Port}";
			return false;
		}
		return base.IstHi(key, value, ref post);
	}
}