namespace Booster.Orm.PostgreSql;

public enum PostgreSqlSslMode
{
	Disable,
	Require
}