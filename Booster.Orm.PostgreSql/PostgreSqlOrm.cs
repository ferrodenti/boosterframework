﻿using System;
using System.Data.Common;
using Booster.Log;
using Booster.Orm.Attributes;
using Booster.Orm.Interfaces;
using Booster.Orm.Kitchen;
using Booster.Reflection;
using JetBrains.Annotations;
using Npgsql;


#nullable enable

[assembly:OrmAssembly]

namespace Booster.Orm.PostgreSql;

[ProviderNames("postgresql", "postgres", "postgre")]
public class PostgreSqlOrm : BaseOrm
{
	protected override bool CaseSensitiveNames => true;

	static readonly Lazy<Version> _npgsqlVersion = new(() => typeof(NpgsqlConnection).Assembly.GetName().Version);
	public static Version NpgsqlVersion => _npgsqlVersion.Value;


	[PublicAPI] public PostgreSqlOrm(ILog? log = null)
		: base(new PostgreSqlConnectionSettings(""), log)
	{
	}

	[PublicAPI] public PostgreSqlOrm(string connectionString, ILog? log = null)
		: base(new PostgreSqlConnectionSettings(connectionString), log)
	{
	}

	[PublicAPI] public PostgreSqlOrm(PostgreSqlConnectionSettings connectionSettings, ILog? log = null)
		: base(connectionSettings, log)
	{
	}

	protected override IOrmFactory CreateFactory() 
		=> new PostgreSqlOrmFactory(this, Log);

	public override DbParameter CreateParameter(string name, object? value)
	{
		var res = Factory.CreateParameter();
		res.ParameterName = name;
		res.Value = value != null ? PrepareValueForParam(value) : DBNull.Value;
		return res;
	}

	public override DbParameter CreateParameter(string name, object? value, TypeEx type)
		=> new NpgsqlParameter
		{
			ParameterName = name,
			NpgsqlDbType = ((PostgreSqlTypeDescriptor) DbTypeDescriptor).GetDbType(type),
			Value = value != null ? PrepareValueForParam(value) : DBNull.Value
		};

	public override DbParameter[] CreateParameters(object? parameters)
	{
		var res = base.CreateParameters(parameters);
		for (var i = 0; i < res.Length; i++)
		{
			var p = (NpgsqlParameter) res[i];
			if (p.Collection != null)
				res[i] = CreateParameter(p.ParameterName, p.Value);
		}
		return res;
	}

	public override object? PrepareValueForParam(object? value)
	{
		value = base.PrepareValueForParam(value);

		if(value != null)
			switch (TypeEx.Of(value).TypeCode)
			{
			//case TypeCode.Byte:
			//case TypeCode.Char:
			case TypeCode.UInt16:
				return Convert.ToInt32(value);
			case TypeCode.UInt32:
			case TypeCode.UInt64:
				return Convert.ToInt64(value);
			}

		return value;
	}
}