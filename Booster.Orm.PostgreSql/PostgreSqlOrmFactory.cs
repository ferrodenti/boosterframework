using System;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using Booster.Log;
using Booster.Orm.Interfaces;
using Booster.Orm.Kitchen;
using Npgsql;

namespace Booster.Orm.PostgreSql;

public class PostgreSqlOrmFactory : BaseOrmFactory
{
#if CONCURRENCY_DEBUG
		public override bool CuncurrencyDebugWrappers => true;
#endif
		
	public PostgreSqlOrmFactory(BaseOrm owner, ILog log) : base(owner, log)
	{
	}

	public override IQueryCompiler CreateQueryCompiler()
		=> new PostgreSqlQueryCompiler(Owner);

	public override IDbTypeDescriptor CreateDbTypeDescriptor() 
		=> new PostgreSqlTypeDescriptor(Owner);

	public override IDbManager CreateDbManager() 
		=> new PostgreSqlDbManager(Owner, Log);

	public override ISqlFormatter CreateSqlFormatter() 
		=> new PostgreSqlFormatter(Owner);

	public override DbParameter CreateParameter() 
		=> new NpgsqlParameter();

	public override ITableMetadata CreateRelationMetadata(string tableName, bool isView) 
		=> new PostgreSqlTableMetadata(Owner, tableName, isView, Log);

	public override IConnection CreateConnection(IConnectionSettings settings, Action releaser) 
		=> new PostgreSqlConnection(settings, releaser, Owner, Log);

	public override async Task<DbConnection> CreateDbConnectionAsync(IConnectionSettings settings, CancellationToken token)
	{
		DbConnection conn = new NpgsqlConnection(settings.ConnectionString);
#if CONCURRENCY_DEBUG
			if(settings.DebugWrappers)
				conn = new Debug.DebugDbConnection(conn);
#endif
		await conn.OpenAsync(token).NoCtx();
		return conn;
	}

	public override DbConnection CreateDbConnection(IConnectionSettings settings)
	{
		DbConnection conn = new NpgsqlConnection(settings.ConnectionString);
#if CONCURRENCY_DEBUG
			if(settings.DebugWrappers)
				conn = new Debug.DebugDbConnection(conn);
#endif
		conn.Open();
		return conn;
	}
}