using Booster.Orm.Interfaces;
using Booster.Orm.Kitchen;

namespace Booster.Orm.PostgreSql;

public class PostgreSqlQueryCompiler : BaseQueryCompiler
{
	public PostgreSqlQueryCompiler(IOrm orm) : base(orm)
	{
	}
}