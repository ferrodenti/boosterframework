﻿using System;
using System.IO;
using Booster.Log;
using Booster.Orm.Interfaces;
using Booster.Orm.Kitchen;

namespace Booster.Orm.PostgreSql;

public class PostgreSqlConnection : BaseConnection
{
	public PostgreSqlConnection(IConnectionSettings connectionSettings, Action releaser, BaseOrm orm, ILog log) 
		: base(connectionSettings, releaser, orm, log)
	{
	}

	protected override bool IsConnectionBroken(Exception ex)
	{
		//if (ex is NpgsqlOperationInProgressException)
		//	return true;

		if (ex is NotSupportedException && ex.Message.StartsWithIgnoreCase("Cannot write to a BufferedStream while the read buffer is not empty"))
			return true;

		return ex is IOException || ex is ObjectDisposedException || ex.Message == "The Connection is broken."|| ex.Message == "Connection is not open";
	}
}