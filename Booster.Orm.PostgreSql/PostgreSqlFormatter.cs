﻿using System;
using Booster.Localization.Pluralization;
using Booster.Orm.Interfaces;
using Booster.Orm.Kitchen;

namespace Booster.Orm.PostgreSql;

public class PostgreSqlFormatter : BasicSqlFormatter
{
	protected new PostgreSqlOrm Orm => (PostgreSqlOrm)base.Orm;

	public override bool AllowArrayParam => true;

	public PostgreSqlFormatter(IOrm orm) : base(orm)
	{
	}

	public override bool IsReservedName(string name)
		=> name switch
		{
			"analyse"           => true,
			"analyze"           => true,
			"array"             => true,
			"asymmetric"        => true,
			"authorization"     => true,
			"binary"            => true,
			"both"              => true,
			"case"              => true,
			"cast"              => true,
			"collate"           => true,
			"collation"         => true,
			"concurrently"      => true,
			"constraint"        => true,
			"current_catalog"   => true,
			"current_date"      => true,
			"current_role"      => true,
			"current_schema"    => true,
			"current_time"      => true,
			"current_timestamp" => true,
			"current_user"      => true,
			"default"           => true,
			"deferrable"        => true,
			"end"               => true,
			"except"            => true,
			"false"             => true,
			"fetch"             => true,
			"foreign"           => true,
			"freeze"            => true,
			"from"              => true,
			"full"              => true,
			"group"             => true,
			"ilike"             => true,
			"initially"         => true,
			"inner"             => true,
			"intersect"         => true,
			"into"              => true,
			"is"                => true,
			"isnull"            => true,
			"join"              => true,
			"lateral"           => true,
			"leading"           => true,
			"left"              => true,
			"limit"             => true,
			"localtime"         => true,
			"localtimestamp"    => true,
			"natural"           => true,
			"notnull"           => true,
			"offset"            => true,
			"only"              => true,
			"outer"             => true,
			"overlaps"          => true,
			"placing"           => true,
			"primary"           => true,
			"references"        => true,
			"returning"         => true,
			"right"             => true,
			"select"            => true,
			"session_user"      => true,
			"similar"           => true,
			"some"              => true,
			"symmetric"         => true,
			"tablesample"       => true,
			"trailing"          => true,
			"true"              => true,
			"using"             => true,
			"variadic"          => true,
			"verbose"           => true,
			"when"              => true,
			"window"            => true,
			_                   => base.IsReservedName(name),
		};

	public override string CreateColumnName(string name)
		=> Orm.ConvertCamelCaseNames2Underscope
			? name.SeparateCamelCase(false, "_").ToLower()
			: name.ToLower();

	public override string CreateTableName(string className)
	{
		if (Orm.ConvertModelNames2PluralTableNames)
			return EnPluralizer.PluralizeCamelCase(className, Orm.ConvertCamelCaseNames2Underscope ? "_" : "").ToLower();

		if (Orm.ConvertCamelCaseNames2Underscope)
			return className.SeparateCamelCase(false, "_").ToLower();

		return className.ToLower();
	}

	protected override string FormatGuid(Guid value)
		=> $"'{value}'::uuid";

	protected override string FormatDateTime(DateTime value)
		=> $"timestamp '{value:yyyy-MM-dd HH:mm:ss.f}'";
}