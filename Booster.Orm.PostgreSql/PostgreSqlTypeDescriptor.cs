﻿using System;
using System.Collections.Generic;
using System.Linq;
using Booster.Orm.Interfaces;
using Booster.Orm.Kitchen;
using Booster.Reflection;
using NpgsqlTypes;

#nullable enable

namespace Booster.Orm.PostgreSql;

public class PostgreSqlTypeDescriptor : BaseDbTypeDescriptor
{
	public PostgreSqlTypeDescriptor(IOrm orm) : base(orm)
	{
	}

	public NpgsqlDbType GetDbType(TypeEx type)
	{
		type = GetValueType(type);

		if (type == typeof (byte[]))
			return NpgsqlDbType.Bytea;

		if (type.IsArray)
			// ReSharper disable once BitwiseOperatorOnEnumWithoutFlags
			return NpgsqlDbType.Array | GetDbType(type.ArrayItemType!);

		switch (type.TypeCode)
		{
		case TypeCode.Boolean:
			return NpgsqlDbType.Boolean;
		case TypeCode.Char:
			return NpgsqlDbType.Char;
		case TypeCode.SByte:
		case TypeCode.Byte:
		case TypeCode.Int16:
			return NpgsqlDbType.Smallint;
		case TypeCode.UInt16:
		case TypeCode.Int32:
			return NpgsqlDbType.Integer;
		case TypeCode.UInt32:
		case TypeCode.Int64:
		case TypeCode.UInt64:
			return NpgsqlDbType.Bigint;
		case TypeCode.Single:
			return NpgsqlDbType.Numeric;
		case TypeCode.Double:
			return NpgsqlDbType.Double;
		case TypeCode.Decimal:
			return NpgsqlDbType.Real;
		case TypeCode.DateTime:
			return NpgsqlDbType.Timestamp;
		case TypeCode.String:
			return NpgsqlDbType.Text;
		}

		throw new ArgumentOutOfRangeException();
	}


	public override TypeEx? GetType(string? dbType, int length, int scale)
	{
		if (dbType == null)
			return null;

		if (dbType.EndsWith("[]"))
			return GetType(dbType.CutRight(2))?.MakeArrayType();

		return dbType switch
		{
			"bigserial"                   => TypeEx.Get<long>(),
			"bigint"                      => TypeEx.Get<long>(),
			"integer"                     => TypeEx.Get<int>(),
			"serial"                      => TypeEx.Get<int>(),
			"smallint"                    => TypeEx.Get<short>(),
			"smallserial"                 => TypeEx.Get<short>(),
			"boolean"                     => TypeEx.Get<bool>(),
			"double precision"            => TypeEx.Get<double>(),
			"real"                        => TypeEx.Get<float>(),
			"decimal"                     => TypeEx.Get<decimal>(),
			"money"                       => TypeEx.Get<decimal>(),
			"numeric"                     => TypeEx.Get<decimal>(),
			"character varying"           => TypeEx.Get<string>(),
			"tsvector"                    => TypeEx.Get<string>(),
			"timestamp with time zone"    => TypeEx.Get<DateTime>(),
			"timestamp without time zone" => TypeEx.Get<DateTime>(),
			"time with time zone"         => TypeEx.Get<DateTime>(),
			"time without time zone"      => TypeEx.Get<DateTime>(),
			"date"                        => TypeEx.Get<DateTime>(),
			"uuid"                        => TypeEx.Get<Guid>(),
			"bytea"                       => TypeEx.Get<byte[]>(),
			"oid"                         => TypeEx.Get<byte[]>(),
			_                             => null
		};
	}
		
	protected override string? GetTypeInt(TypeEx type)
	{
		switch (type.TypeCode)
		{
		case TypeCode.Boolean:
			return "boolean";
		case TypeCode.Char:
			return "\"char\"";
		case TypeCode.SByte:
		case TypeCode.Byte:
		case TypeCode.Int16:
			return "smallint";
		case TypeCode.UInt16:
		case TypeCode.Int32:
			return "integer";
		case TypeCode.UInt32:
		case TypeCode.Int64:
			return "bigint";
		case TypeCode.Single:
			return "real";
		case TypeCode.Double:
			return "double precision";
		case TypeCode.Decimal: //TODO: generate warnings when using decimal+SqLite
		case TypeCode.UInt64:
			return "numeric";
		case TypeCode.DateTime:
			return "timestamp without time zone";
		case TypeCode.String:
			return "character varying";
		}

		if (type == typeof(Guid))
			return "uuid";

		if (type == typeof(byte[]))
			return "bytea";

		if (type.IsArray)
		{
			var elem = GetDbTypeFor(type.ArrayItemType!);
			if (elem != null)
				return $"{elem}[]";
		}
		else
			foreach (var ifc in type.FindGenericInterfaces(typeof(ICollection<>)))
			{
				var elem = GetDbTypeFor(ifc.GenericArguments.FirstOrDefault()!);
				if (elem != null && !elem.EndsWith("[]"))
					return $"{elem}[]";
			}

		return null;
	}
}