﻿using Booster.Log;
using Booster.Orm.Interfaces;
using Booster.Orm.Kitchen;

namespace Booster.Orm.PostgreSql;

public class PostgreSqlTableMetadata : BaseTableMetadata
{
	public PostgreSqlTableMetadata(BaseOrm orm, string tableName, bool isView, ILog log) : base(orm, tableName, isView, log)
	{
	}

	protected override void Write(IConnection conn, string value)
		=> conn.ExecuteNoQuery($"comment on {(IsView ? "view" : "table")} {TableName.ToLower()} is '{value.Replace("'", "''")}'");

	protected override string Read(IConnection conn)
		=> conn.ExecuteScalar<string>($"select description from pg_description join pg_class on pg_description.objoid = pg_class.oid where lower(relname) = {TableName.ToLower():@}");
}