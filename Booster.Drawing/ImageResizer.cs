﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Booster.FileSystem;
using JetBrains.Annotations;

namespace Booster.Drawing;

[PublicAPI]
public class ImageResizer
{
	readonly List<IResizeTask> _tasks;
	public IResizeTask[] Tasks => _tasks.ToArray();

	public ImageResizer(params IResizeTask[] tasks)
		=> _tasks = tasks.ToList();

	public ImageResizer(IEnumerable<IResizeTask> tasks)
		=> _tasks = tasks.ToList();

	public void AddTask(IResizeTask task)
		=> _tasks.Add(task);

	public class Img : IDisposable
	{
		public readonly string FileName;

		byte[] _data;

		readonly Stream _stream;

		public async Task<byte[]> GetData(ImageFormat format = null)
		{
			if (_data == null)
			{
				if (FileName != null)
					_data = await FileAsync.ReadAllBytesAsync(FileName).NoCtx();
				else if (_image != null)
				{
					if (format == null)
						throw new ArgumentNullException(nameof(format));

#if NETSTANDARD2_1
						await using var stream = new MemoryStream();
#else
					using var stream = new MemoryStream();
#endif
					_image.Save(stream, format);
					_data = stream.ToArray();
				}
			}

			return _data;
		}

		Image _image;
		public Image Image
		{
			get
			{
				if (_image == null)
				{
					if (FileName.IsSome())
						_image = Image.FromFile(FileName);
					else if (_stream != null)
						_image = new Bitmap(_stream);
				}

				return _image;
			}
		}

		public int Width => Image.Width;
		public int Height => Image.Height;
			
		public Img(string fileName, byte[] data = null)
		{
			FileName = fileName;
			_data = data;
			if (data != null)
				_stream = new MemoryStream(_data);
		}
			
		public void Dispose()
		{
			_image?.Dispose();
			_image = null;
			_data = null;
			_stream?.Dispose();
		}
	}

	public Task Resize(string originalFileName, bool overwrite = true)
		=> Resize(originalFileName, overwrite, Tasks);

	public Task Resize(string fileName, byte[] data, bool overwrite = true)
		=> Resize(fileName, data, overwrite, Tasks);

	public IEnumerable<byte[]> Resize(byte[] original)
		=> Resize(original, Tasks);

	public async Task Resize(string source, string destination, bool overwrite = true)
	{
		using var original = new Img(source);
		foreach (var preview in Tasks)
			await Resize(original, preview, overwrite, destination).NoCtx();
	}


	public static async Task Resize(string sourceFileName, byte[] source, bool overwrite = true, params IResizeTask[] tasks)
	{
		using var original = new Img(null, source);
		foreach (var preview in tasks)
			await Resize(original, preview, overwrite, sourceFileName).NoCtx();
	}

	public static async Task Resize(string source, bool overwrite = true, params IResizeTask[] tasks)
	{
		using var original = new Img(source);
		foreach (var preview in tasks)
			await Resize(original, preview, overwrite).NoCtx();
	}

	static Task Resize(Img source, IResizeTask task, bool overwrite, string destination = null)
	{
		var dst = task.MapPath(Path.GetFileName(destination ?? source.FileName));

		if (dst == source.FileName)
			return Task.CompletedTask;

		if (!overwrite && File.Exists(dst))
			return Task.CompletedTask;

		return Resize(source, dst, task);
	}

	public static async Task Resize(Img original, params IResizeTask[] previews)
	{
		foreach (var preview in previews)
			await Resize(original, preview, false).NoCtx();
	}

	static async Task Resize(Img original, string destination, IResizeTask preview)
	{
		if (preview.Behaviour == ImageResizerBehaviour.Copy)
		{
			await FileAsync.WriteAllBytesAsync(destination, await original.GetData().NoCtx()).NoCtx();
			return;
		}
			
		if(original.FileName.WithSome(Path.GetExtension) == ".svg")
			return;

		var toSave = Resize(original, preview);

#if NETSTANDARD2_1
			await using var stream = new MemoryStream();
#else
		using var stream = new MemoryStream();
#endif
		try
		{
			toSave.Save(stream, preview.ImageFormat);
			await FileAsync.WriteAllBytesAsync(destination, stream.ToArray()).NoCtx();
		}
		catch
		{
			original.Image.Save(destination, preview.ImageFormat);
		}
	}

	public static byte[] Resize(byte[] original, IResizeTask task)
		=> Resize(original, new[] {task}).FirstOrDefault();

	public static IEnumerable<byte[]> Resize(byte[] original, IEnumerable<IResizeTask> tasks)
	{
		using var origImg = new Img(null, original);
		foreach (var task in tasks)
			using (var resized = Resize(origImg, task))
			using (var stream = new MemoryStream())
			{
				resized.Save(stream, task.ImageFormat);
				yield return stream.ToArray();
			}
	}

	static Image Resize(Img original, IResizeTask task)
	{
		Image toSave;

		if (task.Size.IsEmpty)
			toSave = (Image)original.Image.Clone();//new Img(null, original.GetData().NoCtxResult()).Image;
		else
		{
			var scale = Math.Min((double) task.Size.Width / original.Width, (double) task.Size.Height / original.Height);
			if (scale > 1 && !task.Behaviour.HasFlag(ImageResizerBehaviour.Enlarge))
				scale = 1;

			if (scale < 1 && !task.Behaviour.HasFlag(ImageResizerBehaviour.Shrink))
				scale = 1;

			// ReSharper disable once CompareOfFloatsByEqualityOperator
			if (scale == 1)
				toSave = (Image)original.Image.Clone();//new Img(null, original.GetData().NoCtxResult()).Image;
			else
				toSave = ResizeImage(original.Image, (int) (original.Width * scale), (int) (original.Height * scale));
		}

		return toSave;
	}

	public static Image ResizeImage(byte[] imageData, Size size)
	{
		using var ms = new MemoryStream(imageData);
		using var image = Image.FromStream(ms);
		return ResizeImage(image, size.Width, size.Height);
	}

	public static Image ResizeImage(Image image, int width, int height)
	{
		try
		{
			if (image.Width < width && image.Height < height)
				return image;

			var destRect = new Rectangle(0, 0, width, height);
			var destImage = new Bitmap(width, height);
			var res = Math.Max(72, Math.Max(image.HorizontalResolution, image.VerticalResolution));
			
			destImage.SetResolution(res, res);

			using var graphics = Graphics.FromImage(destImage);
			graphics.CompositingMode = CompositingMode.SourceCopy;
			graphics.CompositingQuality = CompositingQuality.HighQuality;
			graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
			graphics.SmoothingMode = SmoothingMode.HighQuality;
			graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

			using var wrapMode = new ImageAttributes();
			wrapMode.SetWrapMode(WrapMode.TileFlipXY);
			graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);

			return destImage;
		}
		catch (Exception e)
		{
			Utils.Nop(e);
			return null;
		}		
	}
}