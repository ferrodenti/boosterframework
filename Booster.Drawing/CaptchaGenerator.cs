using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using Booster.Log;
using JetBrains.Annotations;

namespace Booster.Drawing;

[PublicAPI]
public class CaptchaGenerator
{
	readonly ILog _log;
	public bool RuEqCheck { get; set; } = true;
	public int Lenght { get; set; } = 5;
	public int Width { get; set; } = 220;
	public int Height { get; set; } = 80;
	public int MaxErrors { get; set; } = 1;

	public CaptchaGenerator(ILog log = null)
		=> _log = log.Create<CaptchaGenerator>();

	public Tuple<byte[],string> GenerateCaptcha()
	{
		var text = GetRndText(Lenght);
		var col = GetRndColor();

		using var result = new Bitmap(Width, Height);
		using (var g = Graphics.FromImage(result))
		{
			g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
			var rect = new Rectangle(0, 0, Width, Height);
			g.FillRectangle(new LinearGradientBrush(rect, Color.White, Color.White, 0f), rect);
			g.SmoothingMode = SmoothingMode.HighQuality;

			DrawPoints(g, col);
			DrawLines(g, col);

			using (var font = GetRndFont(g, text, out var sf))
			using (var path = new GraphicsPath(FillMode.Alternate))
			{
				_log.Debug($"\"{font.Name}\": {text}");
						
				path.AddString(text, font.FontFamily, (int) FontStyle.Regular, font.Size,
					new Point((int) Math.Abs(sf.Width - Width)/2,
						(int) Math.Abs(sf.Height - Height)/2),
					StringFormat.GenericDefault);

				g.FillPath(new SolidBrush(col), RandomWarp(path));
			}
		}
				
		using (var ms = new MemoryStream())
		{
			result.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
			return Tuple.Create(ms.GetBuffer(), text);
		}
	}

	public bool Validate(string actual, string expected)
	{
		if (actual.Length != expected.Length)
			return false;
			
		var errors = actual.Where((t, i) => !CompareChars(t, char.ToLower(expected[i]))).Count();
		return errors <= MaxErrors;
	}
		
	protected bool CompareChars(char c1, char c2)
	{
		if (char.ToLower(c1) == c2)
			return true;
			
		if(RuEqCheck)
			switch (c1)
			{
			case 'a':
			case 'A': return c2 == 'а';
			case 'B': return c2 == 'в';
			case 'c':
			case 'C': return c2 == 'с';
			case 'e':
			case 'E': return c2 == 'е';
			case 'H': return c2 == 'н';
			case 'k':
			case 'K': return c2 == 'к';
			case 'M': return c2 == 'м';
			case 'p':
			case 'P': return c2 == 'р';
			case 'q': return c2 == '9';
			case 'T': return c2 == 'т';
			case 'x':
			case 'X': return c2 == 'х';
			case 'y':
			case 'Y': return c2 == 'у';
			case '6': return c2 == 'б';
			}

		switch (c1)
		{
		case 'g':
		case 'q': return c2 == '9';
		}

		return false;
	}

	public static string GetRndText(int len)
	{
		const string possible = "ABCDEFGHJKLMNPQRSTUVWXYZ23456789abcdfghjkmnpqrstvwxyz";

		var str = new StringBuilder();
		for (var i = 0; i < len; i++)
			str.Append(possible[Rnd.Int(possible.Length)]);

		return str.ToString();
	}

	static readonly List<string> _forbidenFontFamilies = new()
	{
		"Courier New", 
		"Magneto", 
		"Brush Script MT", 
		"Cordia New", 
		"Marlett", 
		"Symbol", 
		"Wingdings", 
		"Webdings", 
		"light", 
		"upc",
		"PUA",
		"Hebrew",
		"Emoji",
		"Kohinoor Telugu",
		"Al Bayan",
		"Gurmukhi MT",
		"ITF Devanagari Marathi",
		"Didot",
		"Zapf Dingbats",
		".LastResort",
		"New Peninim MT",
		"Raanana",
		"Bodoni Ornaments",
		"Damascus",
		"ITF Devanagari",
		"Snell Roundhand",
		"Chalkduster",
		"Zapfino"
	};
		
	static List<FontFamily> _fontFamilies;
	protected static List<FontFamily> FontFamilies
	{
		get 
		{
			if (_fontFamilies == null)
			{
				_fontFamilies = new List<FontFamily>();
				var res = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

				foreach (var fam in FontFamily.Families)
				{
					if(Encoding.UTF8.GetByteCount(fam.Name) != fam.Name.Length)
						continue;

					if (_forbidenFontFamilies.Any(f => fam.Name.ContainsIgnoreCase(f)) || res.Contains(fam.Name))
						continue;

					res.Add(fam.Name);
					_fontFamilies.Add(fam);
				}

			}
			return _fontFamilies;
		}
	}

	Font GetRndFont(Graphics g, string text,  out SizeF sf)
	{
		Font font;
		var i = 0;
		do
		{
			var family = FontFamilies[Rnd.Int(FontFamilies.Count - 1)];

			var low = Height/2;
			var high = Height;
			sf = new SizeF(0, 0);
			do
			{
				var med = (low + high)/2;

				try
				{
					font = new Font(family, med, GraphicsUnit.Pixel);
				}
				catch
				{
					font = null;
					FontFamilies.Remove(family);
					break;
				}
				sf = g.MeasureString(text, font);

				if (sf.Width <= Width && sf.Height <= Height*.9)
					low = med;
				else
					high = med;
			} while (high - low > 1);
		} while (font == null || sf.Width < (float)Width * 3 / 4 && i++ < 20);
		return font;
	}

	static Color GetRndColor()
	{
		double r = Rnd.Double(255), g = Rnd.Double(255), b = Rnd.Double(255);
		var len = Math.Sqrt(r*r + g*g + b*b);
		r = r*100/len;
		b = b*100/len;
		g = g*100/len;

		return Color.FromArgb(100 - (int) r, 100 - (int) g, 100 - (int) b);
	}

	void DrawLines(Graphics g, Color col)
	{
		for (var i = 0; i < 20; i++)
			using (var pen = new Pen(col, Rnd.Float(0.1f, (float) Height/30)))
				g.DrawLine(pen, Rnd.Int(Width), Rnd.Int(Height), Rnd.Int(Width), Rnd.Int(Height));
	}

	void DrawPoints(Graphics g, Color col)
	{
		for (var i = 0; i < Width*Height/7; i++)
			g.FillEllipse(new SolidBrush(col), Rnd.Int(Width), Rnd.Int(Height), Rnd.Float(0.1f, (float) Height/30), Rnd.Float(0.1f, (float) Height/30));
	}

	public GraphicsPath RandomWarp(GraphicsPath path)
	{
		var offsetX = Rnd.Double(Math.PI);
		var offsetY = Rnd.Double(Math.PI);
		var amp = Rnd.Double((double)Height/7);
		var size = Rnd.Double((double)Width/10, (double)Width/7);

		var pn = new PointF[path.PointCount];
		var pt = new byte[path.PointCount];

		using var np2 = new GraphicsPath();
		using (var iter = new GraphicsPathIterator(path))
			for (var i = 0; i < iter.SubpathCount; i++)
			{
				using var sp = new GraphicsPath();
				iter.NextSubpath(sp, out var connect);

				using (var m = new Matrix())
				{
					m.RotateAt(Rnd.Float(-7, 7), sp.PathPoints[0]);
					sp.Transform(m);
				}
				np2.AddPath(sp, connect);
			}

		for (var i = 0; i < np2.PointCount; i++)
		{
			var p = np2.PathPoints[i];
			p.X += (float) (Math.Cos(p.Y/size + offsetY)*amp);
			p.Y += (float) (Math.Sin(p.X/size + offsetX)*amp);
			pn[i] = p;
			pt[i] = np2.PathTypes[i];
		}
		return new GraphicsPath(pn, pt);
	}
}