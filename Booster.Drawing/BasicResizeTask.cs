using System.Drawing;
using System.Drawing.Imaging;
using Booster.FileSystem;

namespace Booster.Drawing;

public class BasicResizeTask : IResizeTask
{
	public Size Size { get; set; }
	public ImageFormat ImageFormat { get; set; }
	public ImageResizerBehaviour Behaviour { get; set; }

	public virtual FilePath MapPath(string fileName) 
		=> fileName;
}