using System.Drawing;
using System.Drawing.Imaging;
using Booster.FileSystem;

namespace Booster.Drawing;

public interface IResizeTask
{
	Size Size { get; }
	ImageFormat ImageFormat { get; }
	ImageResizerBehaviour Behaviour { get; }

	FilePath MapPath(string fileName);
}