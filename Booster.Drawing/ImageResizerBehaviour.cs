using System;

namespace Booster.Drawing;

[Flags]
public enum ImageResizerBehaviour
{
	None = 0,
	Shrink = 1,
	Enlarge = 2,
	Copy = 4,
}