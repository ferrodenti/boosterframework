using System;
using System.Drawing.Imaging;

namespace Booster.Drawing;

public static class DrawingUtils
{
	public static ImageFormat ImageFormatFromExtension(string extension)
	{
		switch (extension?.ToLower())
		{
		case ".bmp":  return ImageFormat.Bmp;
		case ".png":  return ImageFormat.Png;
		case ".gif":  return ImageFormat.Gif;
		case ".emf":  return ImageFormat.Emf;
		case ".wmf":  return ImageFormat.Wmf;
		case ".exif": return ImageFormat.Exif;
		case ".ico":  return ImageFormat.Icon;
		case ".tiff": return ImageFormat.Tiff;
		case ".jpg":
		case ".jpeg": return ImageFormat.Jpeg;
		default: throw new ArgumentOutOfRangeException(nameof(extension));
		}
	}
}