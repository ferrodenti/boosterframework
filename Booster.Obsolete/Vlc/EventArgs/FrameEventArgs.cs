using System;
using System.Drawing;

namespace Booster.Vlc;

[Obsolete]
public class FrameEventArgs : EventArgs
{
	public IntPtr[] Planes { get; }
	BitmapFormat BitmapFormat { get; }

	public unsafe Bitmap CreateBitmap()
		=> new(BitmapFormat.Width, BitmapFormat.Height, BitmapFormat.Pitch, BitmapFormat.PixelFormat, new IntPtr((void*) Planes[0]));

	public FrameEventArgs(BitmapFormat bitmapFormat, IntPtr[] planes)
	{
		Planes = planes;
		BitmapFormat = bitmapFormat;
	}
}