using System;

namespace Booster.Vlc;

[Obsolete]
public class SetupFormatEventArgs : EventArgs
{
	public BitmapFormat BitmapFormat { get; set; }

	public SetupFormatEventArgs(BitmapFormat bitmapFormat)
		=> BitmapFormat = bitmapFormat;
}