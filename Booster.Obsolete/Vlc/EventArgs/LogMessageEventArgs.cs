using System;

namespace Booster.Vlc;

public class LogMessageEventArgs : EventArgs
{
	public string Message { get; }
	public int Level { get; }

	public LogMessageEventArgs(int level, string message)
	{
		Level = level;
		Message = message;
	}
}