﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;
using Booster.Vlc.Kitchen;
using JetBrains.Annotations;
using Microsoft.Win32;

namespace Booster.Vlc;

[Obsolete, PublicAPI]
public class LibVlc : IDisposable
{
	static readonly string[] _defaultCommandLineArguments = {
		"-I",
		"dumy",
		"--ignore-config",
		"--no-osd",
		"--disable-screensaver",
		"--verbose=2",
		"--plugin-path=./plugins",
		"--no-spu"
	};


	public string[] CommandLineArguments { get; }

	Lazy<IntPtr> _handle;
	internal IntPtr Handle
	{
		get => _handle.Value;
		set { _handle = new Lazy<IntPtr>(() => value); }
	}

	static Lazy<string> _defaultPath = new(Find);
	public static string DefaultPath
	{
		get => _defaultPath.Value;
		set
		{
			if (value == null)
				throw new ArgumentNullException(nameof(value));

			_defaultPath = new Lazy<string>(() => value);
		}
	}


	Lazy<string> _path = new(() => DefaultPath ?? Find());
	public string Path
	{
		get => _path.Value;
		set
		{
			if (value == null)
				throw new ArgumentNullException(nameof(value));

			_path = new Lazy<string>(() => value);
		}
	}

	// ReSharper disable once CollectionNeverQueried.Local
	readonly List<Delegate> _preservedCallbacks = new();

	event EventHandler<LogMessageEventArgs> LogMessageInt;
	public unsafe event EventHandler<LogMessageEventArgs> LogMessage
	{
		add
		{
			lock (this)
			{
				if (LogMessageInt == null)
					Methods.libvlc_log_set(Handle, PreserveCallback((LogCallback) LogCallback), IntPtr.Zero);

				LogMessageInt += value;
			}
		}
		remove
		{
			lock (this)
			{
				LogMessageInt -= value;

				if (LogMessageInt == null)
					Methods.libvlc_log_set(Handle, IntPtr.Zero, IntPtr.Zero);
			}
		}
	}

	public LibVlc() : this(_defaultCommandLineArguments)
	{
	}

	public LibVlc(params string[] args)
	{
		CommandLineArguments = args;

		_handle = new Lazy<IntPtr>(() =>
		{
			var oldDir = Directory.GetCurrentDirectory(); //TODO: возможно потом utils.pushDirectory
			try
			{
				Directory.SetCurrentDirectory(Path);
				return Methods.libvlc_new(CommandLineArguments.Length, CommandLineArguments);
			}
			finally
			{
				Directory.SetCurrentDirectory(oldDir);
			}

		});
	}

	static string Find()
	{
		using var rk = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\VideoLAN\VLC");
		return rk?.GetValue("InstallDir")?.ToString();
	}

	~LibVlc()
		=> Dispose();

	internal IntPtr PreserveCallback(Delegate callback)
	{
		_preservedCallbacks.Add(callback);

		return Marshal.GetFunctionPointerForDelegate(callback);
	}

	unsafe void LogCallback(void* data, int level, void* ctx, char* fmt, char* args)
	{
		var handler = LogMessageInt;
		if (handler != null)
			try
			{
				var buffer = stackalloc char[1024];
				var len = vsprintf(buffer, fmt, args);
				var msg = Marshal.PtrToStringAnsi(new IntPtr(buffer), len);
				handler.Invoke(this, new LogMessageEventArgs(level, msg));
			}
			catch (Exception ex)
			{
				handler.Invoke(this, new LogMessageEventArgs(3, ex.Message));
			}
	}

	[DllImport("msvcrt", SetLastError = true, CallingConvention = CallingConvention.Cdecl)]
	[SuppressUnmanagedCodeSecurity]
	static extern unsafe int vsprintf(char* str, char* format, char* arg);

	public void Dispose()
	{
		if (Handle != IntPtr.Zero)
		{
			Methods.libvlc_release(Handle);
			Handle = IntPtr.Zero;
		}

		_preservedCallbacks.Clear();
	}
}