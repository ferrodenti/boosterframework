using System;

namespace Booster.Vlc;

[Obsolete]
unsafe struct PlanarPixelData : IDisposable //TODO: refact
{
	public readonly int[] Sizes;
	public byte** Data;

	public PlanarPixelData(int[] lineSizes)
	{
		Sizes = lineSizes;

		Data = (byte**)MemoryHeap.Alloc(sizeof(byte*) * Sizes.Length);

		for (var i = 0; i < Sizes.Length; i++)
			Data[i] = (byte*)MemoryHeap.Alloc(sizeof(byte) * Sizes[i]);
	}

	public void Dispose()
	{
		for (var i = 0; i < Sizes.Length; i++)
			MemoryHeap.Free(Data[i]);

		MemoryHeap.Free(Data);
	}

	public static bool operator ==(PlanarPixelData pd1, PlanarPixelData pd2)
		=> pd1.Data == pd2.Data && pd1.Sizes == pd2.Sizes;

	public static bool operator !=(PlanarPixelData pd1, PlanarPixelData pd2)
		=> !(pd1 == pd2);

	public override int GetHashCode()
		=> Sizes.GetHashCode();

	public override bool Equals(object obj)
	{
		if (!(obj is PlanarPixelData))
			return false;

		return this == (PlanarPixelData)obj;
	}
}