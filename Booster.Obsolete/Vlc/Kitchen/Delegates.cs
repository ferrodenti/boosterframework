using System.Runtime.InteropServices;

namespace Booster.Vlc.Kitchen;

[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
unsafe delegate void* LockCallback(void* opaque, void** plane);

[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
unsafe delegate void DisplayCallback(void* opaque, void* picture);

[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
unsafe delegate int FormatCallback(void** opaque, char* chroma, int* width, int* height, int* pitches, int* lines);

[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
unsafe delegate void LogCallback(void* data, int level, void* ctx, char* fmt, char* args);