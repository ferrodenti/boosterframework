﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace Booster.Vlc.Kitchen;

[SuppressUnmanagedCodeSecurity]
public static class Methods
{
	#region libvlc.h

	[DllImport("libvlc", CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr libvlc_new(int argc, [MarshalAs(UnmanagedType.LPArray, ArraySubType = UnmanagedType.LPStr)] string[] argv);

	[DllImport("libvlc", CallingConvention = CallingConvention.Cdecl)]
	public static extern void libvlc_release(IntPtr libVlc);

	[DllImport("libvlc", CallingConvention = CallingConvention.Cdecl)]
	public static extern void libvlc_log_set(IntPtr libVlc, IntPtr callback, IntPtr data);

	#endregion

	#region libvlc_media.h

	[DllImport("libvlc", CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr libvlc_media_new_path(IntPtr libVlc, [MarshalAs(UnmanagedType.LPArray)] byte[] pszMrl);

	[DllImport("libvlc", CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr libvlc_media_new_location(IntPtr libVlc, [MarshalAs(UnmanagedType.LPArray)] byte[] pszMrl);

	[DllImport("libvlc", CallingConvention = CallingConvention.Cdecl)]
	public static extern void libvlc_media_release(IntPtr media);
	#endregion

	#region libvlc_media_player.h

	[DllImport("libvlc", CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr libvlc_media_player_new_from_media(IntPtr media);

	[DllImport("libvlc", CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr libvlc_media_player_new(IntPtr libVlc);

	[DllImport("libvlc", CallingConvention = CallingConvention.Cdecl)]
	public static extern void libvlc_video_set_format_callbacks(IntPtr mediaPlayer, IntPtr setup, IntPtr cleanup);

	[DllImport("libvlc", CallingConvention = CallingConvention.Cdecl)]
	public static extern void libvlc_video_set_callbacks(IntPtr mediaPlayer, IntPtr lck, IntPtr unlock, IntPtr display, IntPtr opaque);

	[DllImport("libvlc", CallingConvention = CallingConvention.Cdecl)]
	public static extern void libvlc_media_player_release(IntPtr mediaPlayer);

	[DllImport("libvlc", CallingConvention = CallingConvention.Cdecl)]
	public static extern void libvlc_media_player_set_media(IntPtr mediaPlayer, IntPtr media);

	//[DllImport("libvlc", CallingConvention = CallingConvention.Cdecl)]
	//public static extern int libvlc_media_player_is_playing(IntPtr mediaPlayer);

	[DllImport("libvlc", CallingConvention = CallingConvention.Cdecl)]
	public static extern void libvlc_media_player_play(IntPtr mediaPlayer);

	//[DllImport("libvlc", CallingConvention = CallingConvention.Cdecl)]
	//public static extern void libvlc_media_player_set_pause(IntPtr mediaPlayer, int pause);

	[DllImport("libvlc", CallingConvention = CallingConvention.Cdecl)]
	public static extern void libvlc_media_player_pause(IntPtr mediaPlayer);

	[DllImport("libvlc", CallingConvention = CallingConvention.Cdecl)]
	public static extern void libvlc_media_player_stop(IntPtr mediaPlayer);

	[DllImport("libvlc", CallingConvention = CallingConvention.Cdecl)]
	public static extern float libvlc_media_player_get_position(IntPtr mediaPlayer);

	[DllImport("libvlc", CallingConvention = CallingConvention.Cdecl)]
	public static extern void libvlc_media_player_set_position(IntPtr mediaPlayer, float pos);

	[DllImport("libvlc", CallingConvention = CallingConvention.Cdecl)]
	public static extern int libvlc_audio_get_volume(IntPtr mediaPlayer);
		
	[DllImport("libvlc", CallingConvention = CallingConvention.Cdecl)]
	public static extern int libvlc_audio_set_volume(IntPtr mediaPlayer, int volume);

	[DllImport("libvlc", CallingConvention = CallingConvention.Cdecl)]
	public static extern int libvlc_audio_get_mute(IntPtr mediaPlayer);
		
	[DllImport("libvlc", CallingConvention = CallingConvention.Cdecl)]
	public static extern int libvlc_audio_set_mute(IntPtr mediaPlayer, int value);

		
	[DllImport("libvlc", CallingConvention = CallingConvention.Cdecl)]
	public static extern int libvlc_video_set_spu(IntPtr mediaPlayer, int spu);
	#endregion
}