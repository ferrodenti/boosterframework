using System;
using System.Drawing.Imaging;

namespace Booster.Vlc;

[Obsolete, Serializable]
public class BitmapFormat //TODO: refact
{
	public PixelFormat PixelFormat { get; private set; }

	public int Pitch { get; }
	public int ImageSize { get; set; }
	public string Chroma { get; set; }
	public int Width { get; }
	public int Height { get; }
	public int BitsPerPixel { get; }
	public int Planes { get; }
	public int[] PlaneSizes { get; }
	public int[] Pitches { get; }
	public int[] Lines { get; }

	public BitmapFormat(int width, int height, string chroma)
	{
		Width = width;
		Height = height;
		Chroma = chroma.ToUpper();
		Planes = 1;
		PlaneSizes = new int[3];

		var rgb = false;

		switch (Chroma)
		{
		case "RV15":
			PixelFormat = PixelFormat.Format16bppRgb555;
			BitsPerPixel = 16;
			rgb = true;
			break;

		case "RV16":
			PixelFormat = PixelFormat.Format16bppRgb565;
			BitsPerPixel = 16;
			rgb = true;
			break;

		case "RV24":
			PixelFormat = PixelFormat.Format24bppRgb;
			BitsPerPixel = 24;
			rgb = true;
			break;

		case "RV32":
			PixelFormat = PixelFormat.Format32bppRgb;
			BitsPerPixel = 32;
			rgb = true;
			break;

		case "RGBA":
			PixelFormat = PixelFormat.Format32bppArgb;
			BitsPerPixel = 32;
			rgb = true;
			break;

		case "NV12":
			BitsPerPixel = 12;
			Planes = 2;
			PlaneSizes[0] = Width * Height;
			PlaneSizes[1] = Width * Height / 2;
			Pitches = new[] { Width, Width};
			Lines = new[] { Height, Height / 2};
			ImageSize = PlaneSizes[0] + PlaneSizes[1];
			break;

		case "I420":
		case "YV12":
		case "J420":
			BitsPerPixel = 12;
			Planes = 3;
			PlaneSizes[0] = Width * Height;
			PlaneSizes[1] = PlaneSizes[2] = Width * Height / 4;
			Pitches = new[] { Width, Width / 2, Width / 2 };
			Lines = new[] { Height, Height / 2, Height / 2 };
			ImageSize = PlaneSizes[0] + PlaneSizes[1] + PlaneSizes[2];
			break;

		case "YUY2":
		case "UYVY":
		case "J422":
			BitsPerPixel = 16;
			PlaneSizes[0] = Width * Height * 2;
			Pitches = new[] { Width * 2};
			Lines = new[] { Height };
			ImageSize = PlaneSizes[0];
			break;

		default:
			throw new ArgumentException("Unsupported chroma " + Chroma);
		}

		if (rgb)
		{
			Pitch = Width * BitsPerPixel / 8;
			PlaneSizes[0] = ImageSize = Pitch * Height;
			Pitches = new[] { Pitch };
			Lines = new[] { Height };
		}
	}
}