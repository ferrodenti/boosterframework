using System;
using System.Runtime.InteropServices;
using System.Security;

namespace Booster.Vlc;

[Obsolete, SuppressUnmanagedCodeSecurity]
unsafe class MemoryHeap //TODO: refact
{
	static readonly IntPtr _ph = GetProcessHeap();

	MemoryHeap() { }

	/// <summary>
	/// Allocates a memory block of the given size. The allocated memory is
	/// automatically initialized to zero.
	/// </summary>
	/// <param name="size"></param>
	/// <returns></returns>
	public static void* Alloc(int size)
	{
		var result = HeapAlloc(_ph, _heapZeroMemory, size);
		if (result == null)
			throw new OutOfMemoryException();

		return result;
	}

	/// <summary>
	/// Frees a memory block.
	/// </summary>
	/// <param name="block"></param>
	public static void Free(void* block)
	{
		if (!HeapFree(_ph, 0, block))
			throw new InvalidOperationException();
	}

	/// <summary>
	/// Re-allocates a memory block. If the reallocation request is for a
	/// larger size, the additional region of memory is automatically
	/// initialized to zero.
	/// </summary>
	/// <param name="block"></param>
	/// <param name="size"></param>
	/// <returns></returns>
	public static void* ReAlloc(void* block, int size)
	{
		var result = HeapReAlloc(_ph, _heapZeroMemory, block, size);
		if (result == null)
			throw new OutOfMemoryException();

		return result;
	}

	/// <summary>
	/// Returns the size of a memory block.
	/// </summary>
	/// <param name="block"></param>
	/// <returns></returns>
	public static int SizeOf(void* block)
	{
		var result = HeapSize(_ph, 0, block);
		if (result == -1)
			throw new InvalidOperationException();

		return result;
	}

	// Heap API flags
	const int _heapZeroMemory = 0x00000008;

	// Heap API functions
	[DllImport("kernel32")]
	static extern IntPtr GetProcessHeap();

	[DllImport("kernel32")]
	static extern void* HeapAlloc(IntPtr hHeap, int flags, int size);

	[DllImport("kernel32")]
	static extern bool HeapFree(IntPtr hHeap, int flags, void* block);

	[DllImport("kernel32")]
	static extern void* HeapReAlloc(IntPtr hHeap, int flags, void* block, int size);

	[DllImport("kernel32")]
	static extern int HeapSize(IntPtr hHeap, int flags, void* block);

	[DllImport("Kernel32.dll", EntryPoint = "RtlMoveMemory", SetLastError = true)]
	public static extern void CopyMemory(void* dest, void* src, int size);
}