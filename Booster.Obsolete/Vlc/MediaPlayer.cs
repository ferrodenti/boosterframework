﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using Booster.Vlc.Kitchen;
using JetBrains.Annotations;

namespace Booster.Vlc;

[Obsolete, PublicAPI]
public class MediaPlayer : IDisposable
{
	bool _ownsVlc;
	readonly LibVlc _vlc;
	IntPtr _handle;

	public BitmapFormat BitmapFormat { get; private set; }
	PlanarPixelData _pixelData;
	readonly object _lock = new();
	readonly IntPtr[] _planes = new IntPtr[3];

	public event EventHandler<FrameEventArgs> NewFrame;
	public event EventHandler<ExceptionEventArgs> Error;
	public event EventHandler<SetupFormatEventArgs> SetupFormat;

	public float Position
	{
		get => Methods.libvlc_media_player_get_position(_handle);
		set => Methods.libvlc_media_player_set_position(_handle, value);
	}
	public int Volume
	{
		get => Methods.libvlc_audio_get_volume(_handle);
		set => Methods.libvlc_audio_set_volume(_handle, value);
	}
	public bool Mute
	{
		get => Methods.libvlc_audio_get_mute(_handle) != 0;
		set => Methods.libvlc_audio_set_mute(_handle, value ? 1 : 0);
	}

	public MediaPlayer() : this(new LibVlc())
	{
		_ownsVlc = true;
	}

	public unsafe MediaPlayer(LibVlc vlc)
	{
		_vlc = vlc;
		_handle = Methods.libvlc_media_player_new(_vlc.Handle);

		Methods.libvlc_video_set_format_callbacks(_handle,
			_vlc.PreserveCallback((FormatCallback) FormatCallback),
			IntPtr.Zero);

		Methods.libvlc_video_set_callbacks(_handle,
			_vlc.PreserveCallback((LockCallback) LockCallback),
			IntPtr.Zero,
			_vlc.PreserveCallback((DisplayCallback) DisplayCallback),
			IntPtr.Zero);
	}

	public MediaPlayer(string fileName, bool orUri = true) : this(new LibVlc(), fileName, orUri)
	{
		_ownsVlc = true;
	}
	public MediaPlayer(LibVlc owner, string fileName, bool orUri = true) : this(owner)
	{
		Open(fileName, orUri);
	}
	public MediaPlayer(Uri fileName) : this(new LibVlc(), fileName)
	{
		_ownsVlc = true;
	}
	public MediaPlayer(LibVlc owner, Uri fileName) : this(owner)
	{
		Open(fileName);
	}

	public void Open(string fileName, bool orUri = true)
	{
		if (string.IsNullOrWhiteSpace(fileName))
			throw new ArgumentException(nameof(fileName));

		if (orUri && Uri.TryCreate(fileName, UriKind.Absolute, out var uri))
		{
			Open(uri);
			return;
		}

		Stop();

		var media = IntPtr.Zero;
		try
		{
			media = Methods.libvlc_media_new_path(_vlc.Handle, Encoding.UTF8.GetBytes(fileName));
			Methods.libvlc_media_player_set_media(_handle, media);
		}
		finally
		{
			if (media != IntPtr.Zero)
				Methods.libvlc_media_release(media);
		}

		Play();
	}


	public void Open(Uri uri)
	{
		if (uri == null)
			throw new ArgumentNullException(nameof(uri));

		Stop();

		var media = IntPtr.Zero;
		try
		{
			media = Methods.libvlc_media_new_location(_vlc.Handle, Encoding.UTF8.GetBytes(uri.ToString()));
			Methods.libvlc_media_player_set_media(_handle, media);
		}
		finally
		{
			if (media != IntPtr.Zero)
				Methods.libvlc_media_release(media);
		}

		Play();
	}

	public void Play()
	{
		Methods.libvlc_media_player_play(_handle);
	}

	public void Pause()
	{
		Methods.libvlc_media_player_pause(_handle);
	}

	public void Stop()
	{
		Methods.libvlc_media_player_stop(_handle);
	}
		    
	unsafe int FormatCallback(void** opaque, char* chroma, int* width, int* height, int* pitches, int* lines)
	{
		ErrEnvelope(() =>
		{
			var pChroma = new IntPtr(chroma);
			var chromaStr = Marshal.PtrToStringAnsi(pChroma);

			BitmapFormat = new BitmapFormat(*width, *height, chromaStr);
			var handler = SetupFormat;
			if (handler != null)
			{
				var ea = new SetupFormatEventArgs(BitmapFormat);
				handler.Invoke(this, ea);
				BitmapFormat = ea.BitmapFormat;
			}

			Marshal.Copy(Encoding.UTF8.GetBytes(BitmapFormat.Chroma), 0, pChroma, 4);

			for (var i = 0; i < BitmapFormat.Planes; i++)
			{
				pitches[i] = BitmapFormat.Pitches[i];
				lines[i] = BitmapFormat.Lines[i];
			}

			_pixelData = new PlanarPixelData(BitmapFormat.PlaneSizes);
		});
		return BitmapFormat.Planes;
	}

	unsafe void* LockCallback(void* opaque, void** plane)
	{
		ErrEnvelope(() =>
		{
			for (var i = 0; i < _pixelData.Sizes.Length; i++)
				plane[i] = _pixelData.Data[i];

		});
		return null;
	}

	unsafe void DisplayCallback(void* opaque, void* picture)
	{
		lock (_lock)
			ErrEnvelope(() =>
			{
				for (var i = 0; i < _pixelData.Sizes.Length; i++)
					_planes[i] = new IntPtr(_pixelData.Data[i]);

				var frameHandler = NewFrame;
				if (frameHandler != null)
				{
					var pf = new FrameEventArgs(BitmapFormat, _planes);
					frameHandler(this, pf);
				}
			});
	}

	void ErrEnvelope(Action action)
	{
		var errHandler = Error;
		if (errHandler != null)
			try
			{
				action();
			}
			catch (Exception ex)
			{
				var ea = new ExceptionEventArgs(ex);
				errHandler.Invoke(this, ea);
				if (ea.Handled)
					return;
				throw;
			}
		else
			action();
	}


	~MediaPlayer()
	{
		Dispose();
	}

	public void Dispose()
	{
		if (_handle != IntPtr.Zero)
		{
			Methods.libvlc_media_player_release(_handle);
			_handle = IntPtr.Zero;
		}

		if (_ownsVlc)
		{
			_vlc?.Dispose();
			_ownsVlc = false;
		}
	}
}