using System;
using System.IO;
using Booster.IO;

namespace Booster.Epanders;

[Obsolete]
public static class StreamExpander
{
	public static int MaxPascalSize = 1024*1024*100; //100Mb

	public static IAsyncResult BeginReadPascal(this Stream stream, AsyncCallback callback)
	{
		var result = new AsyncResult<byte[]> { AsyncState = stream };

		try
		{
			BeginReadWhole(stream, sizeof (uint), ar1 =>
			{
				try
				{
					var data = EndReadPascal(stream, ar1);

					if (data.Length > 0) //если data.Length == 0 значит ничего из потока не прочиталось
					{
						var size = BitConverter.ToUInt32(data, 0);

						if (size > MaxPascalSize)
							throw new Exception("Message is too long");

						if (size == 0)
						{
							result.Result = new byte[0];
							result.IsCompleted = true;
							callback(result);
							return;
						}


						BeginReadWhole(stream, (int) size, ar2 =>
						{
							try
							{
								result.Result = EndReadPascal(stream, ar2);
								result.IsCompleted = true;
							}
							catch (Exception ex)
							{
								result.Exception = ex;
							}

							callback?.Invoke(result);
						});
					}
					else
					{
						//как-нибудь маякнуть можно, сколько прочитано или сколько ожидалось...
						if(result.Exception != null)
							result.Exception = new EndOfStreamException();
					}
				}
				catch (Exception ex)
				{
					result.Exception = ex;

					callback?.Invoke(result);
				}
			});
		}
		catch (Exception ex)
		{
			result.Exception = ex;

			callback?.Invoke(result);
		}
		return result;
	}

	public static IAsyncResult BeginReadWhole(this Stream stream, int size, AsyncCallback callback)
	{
		var result = new AsyncResult<byte[]> { Result = new byte[size], AsyncState = stream };
		var read = 0;

		void Cb(IAsyncResult ar1)
		{
			try
			{
				var r = stream.EndRead(ar1);
				if (r > 0) //чтото прочитали из потока
				{
					read += r;
					if (read < result.Result.Length)
					{
						stream.BeginRead(result.Result, read, result.Result.Length - read, Cb, null);
						return;
					}
				}
				else
					throw new IOException("Stream was closed");

				result.IsCompleted = true;
			}
			catch (Exception ex)
			{
				result.Result = new byte[0]; //что там было уже не важно...
				result.Exception = ex;
			}


			callback?.Invoke(result);
		}

		stream.BeginRead(result.Result, 0, result.Result.Length, Cb, null);
		return result;
	}

	public static byte[] EndReadPascal(this Stream stream, IAsyncResult asyncResult)
	{
		var res = (AsyncResult<byte[]>)asyncResult;
		if (res.Exception != null)
			throw res.Exception;

		return res.Result;
	}

	public static IAsyncResult BeginWritePascal(this Stream stream, byte[] data, AsyncCallback callback)
	{
		var result = new AsyncResult<int> {AsyncState = stream};

		var buf = BitConverter.GetBytes((uint) data.Length);

		stream.BeginWrite(buf, ar1 =>
		{
			try
			{
				stream.EndWrite(ar1);
				if(data.Length > 0 )
					stream.BeginWrite(data, ar2 =>
					{
						try
						{
							stream.EndWrite(ar2);
							result.IsCompleted = true;
						}
						catch (Exception ex)
						{
							result.Exception = ex;
						}

						callback?.Invoke(result);
					});
				else
					callback?.Invoke(result);
			}
			catch (Exception ex)
			{
				result.Exception = ex;

				callback?.Invoke(result);
			}
		});
		return result;
	}

	public static void EndWritePascal(this Stream stream, IAsyncResult asyncResult)
	{
		var res = (AsyncResult<int>)asyncResult;
		if (res.Exception != null)
			throw res.Exception;
	}

	public static IAsyncResult BeginWrite(this Stream stream, byte[] data, AsyncCallback callback, object state = null)
		=> stream.BeginWrite(data, 0, data.Length, callback, state);
}