using System;
using System.IO;

namespace Booster.IO
{
	public static class StreamExpander
	{
		public static int MaxPascalSize = 1024*1024*100; //100Mb

		public static IAsyncResult BeginReadPascal(this Stream stream, AsyncCallback callback)
		{
			AsyncResult<byte[]> result = new AsyncResult<byte[]> { AsyncState = stream };

			try
			{
				BeginReadWhole(stream, sizeof (uint), ar1 =>
				{
					try
					{
						byte[] data = EndReadPascal(stream, ar1);
						uint size = BitConverter.ToUInt32(data, 0);

						if (size > MaxPascalSize)
							throw new Exception("Message is too long");

						if (size == 0)
						{
							callback(result);
							return;
						}

						BeginReadWhole(stream, (int) size, ar2 =>
						{
							try
							{
								result.Result = EndReadPascal(stream, ar2);
								result.IsCompleted = true;
							}
							catch (Exception ex)
							{
								result.Exception = ex;
							}
							if (callback != null)
								callback(result);
						});
					}
					catch (Exception ex)
					{
						result.Exception = ex;

						if (callback != null)
							callback(result);
					}
				});
			}
			catch (Exception ex)
			{
				result.Exception = ex;

				if (callback != null)
					callback(result);
			}
			return result;
		}

		public static IAsyncResult BeginReadWhole(this Stream stream, int size, AsyncCallback callback)
		{
			AsyncResult<byte[]> result = new AsyncResult<byte[]> { Result = new byte[size], AsyncState = stream };
			int read = 0;

			AsyncCallback cb = null;
			cb = ar1 =>
			{
				try
				{
					read += stream.EndRead(ar1);
					if (read > 0 && read < result.Result.Length)
					{
						stream.BeginRead(result.Result, read, result.Result.Length - read, cb, null);
						return;
					}
					result.IsCompleted = true;
				}
				catch (Exception ex)
				{
					result.Exception = ex;
				}
				if (callback != null)
					callback(result);
			};
			stream.BeginRead(result.Result, 0, result.Result.Length, cb, null);
			return result;
		}

		public static byte[] EndReadPascal(this Stream stream, IAsyncResult asyncResult)
		{
			AsyncResult<byte[]> res = (AsyncResult<byte[]>)asyncResult;
			if (res.Exception != null)
				throw res.Exception;

			return res.Result;
		}

		public static IAsyncResult BeginWritePascal(this Stream stream, byte[] data, AsyncCallback callback)
		{
			AsyncResult<int> result = new AsyncResult<int> {AsyncState = stream};

			byte[] buf = BitConverter.GetBytes((uint) data.Length);

			stream.BeginWrite(buf, ar1 =>
			{
				try
				{
					stream.EndWrite(ar1);
					stream.BeginWrite(data, ar2 =>
					{
						try
						{
							stream.EndWrite(ar2);
							result.IsCompleted = true;
						}
						catch (Exception ex)
						{
							result.Exception = ex;
						}
						if (callback != null)
							callback(result);
					});
				}
				catch (Exception ex)
				{
					result.Exception = ex;

					if (callback != null)
						callback(result);
				}
			});
			return result;
		}

		public static void EndWritePascal(this Stream stream, IAsyncResult asyncResult)
		{
			AsyncResult<int> res = (AsyncResult<int>)asyncResult;
			if (res.Exception != null)
				throw res.Exception;
		}

		public static IAsyncResult BeginWrite(this Stream stream, byte[] data, AsyncCallback callback, object state = null)
		{
			return stream.BeginWrite(data, 0, data.Length, callback, state);
		}
	}
}