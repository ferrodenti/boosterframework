﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using JetBrains.Annotations;

namespace Booster.IO;

[Obsolete, PublicAPI]
public class AsyncTcpServer : IDisposable
{
	public IPEndPoint IpEndPoint { get; }

	readonly TcpListener _listener;

	readonly List<AsyncConnection> _connections = new();

	public event EventHandler<ServerRequestEventArgs> DataReceived;
	public event EventHandler<EventArgs<Exception>> Error;

	Thread _connectionWorker;
	protected Thread ConnectionWorker => _connectionWorker ??= new Thread(_connectionProc) { Name = IpEndPoint + "_connectionWorker" };

	readonly ManualResetEvent _doneConnected = new(true);

	public bool IsRunning => _connectionWorker != null && _connectionWorker.IsAlive;

	public AsyncTcpServer(IPEndPoint ipEndPoint)
	{
		IpEndPoint = ipEndPoint;

		_listener = new TcpListener(IpEndPoint);
	}

	public AsyncTcpServer(IPAddress address, int port) : this(new IPEndPoint(address, port))
	{
	}

	void _connectionProc(object state)
	{
		while (IsRunning)
			try
			{
				_doneConnected.Reset();
				_listener.BeginAcceptSocket(ClientAccepted, null);
				_doneConnected.WaitOne();
			}
			catch (ThreadAbortException)
			{
				break;
			}
			catch (Exception ex)
			{
				OnError(ex);
			}
	}

	public void Start()
	{
		if (IsRunning)
			throw new Exception("Server already running");


		_listener.Start();
		ConnectionWorker.Start();
	}


	public void Stop()
	{
		if (IsRunning)
		{
			_connectionWorker.Abort();
			_connectionWorker = null;

			foreach (var conn in _connections.ToArray())
				conn.Close("Server stopping");

			_listener.Stop();
		}
	}

	protected void Disconnect(IAsyncResult ar)
		=> _listener.Server.EndDisconnect(ar);

	protected void ClientAccepted(IAsyncResult ar)
	{
		try
		{
			_doneConnected.Set();
			if (!IsRunning) return;

			var client = _listener.EndAcceptTcpClient(ar);

			var conn = new AsyncConnection();
			_connections.Add(conn);

			conn.Error += (_, args) => OnError(args.GetException());

			conn.Received += (sender, args) =>
			{
				var e = new ServerRequestEventArgs(args.Data);
				OnDataReceived(e);

				if (e.Response != null)
					((AsyncConnection)sender).Send(e.Response);
			};

			conn.Closed += (sender, _) =>
			{
				try
				{
					_connections.Remove((AsyncConnection)sender);
				}
				catch
				{
					//ignore
				}
			};

			conn.Init(client, client.GetStream());

		}
		catch (ObjectDisposedException)
		{

		}
		catch (Exception ex)
		{
			OnError(ex);
		}
	}
	protected virtual void OnDataReceived(ServerRequestEventArgs e)
		=> DataReceived?.Invoke(this, e);

	protected virtual void OnError(Exception ex)
		=> Error?.Invoke(this, new EventArgs<Exception>(ex));

	public void Dispose()
		=> Stop();
}