﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading;
using Booster.Epanders;

namespace Booster.IO;

[Obsolete]
public class AsyncConnection : IDisposable
{
	protected Stream Stream;
	IDisposable _container;
	bool _closing;
	readonly ConcurrentQueue<byte[]> _sendQueue = new();
	public readonly ManualResetEventSlim DoneSending = new(true);

	public event EventHandler<MessageReceivedEventArgs> Received;
	public event EventHandler<ErrorEventArgs> Error;
	public event EventHandler<ConnectionClosedEventArgs> Closed;

	public bool CloseOnError { get; set; } = true;

	volatile bool _isOpen;
	public bool IsOpen => _isOpen;

	public virtual void Init(IDisposable container, Stream stream)
	{
		_container = container;
		Stream = stream;
		_isOpen = true;
		Stream.BeginReadPascal(ReadCallback);
	}

	protected bool IsAbandoned(IAsyncResult ar)
	{
		if (!IsOpen || ar.AsyncState is Stream callbackStream && callbackStream != Stream) // Проверка
			return true;

		return false;
	}

	void ReadCallback(IAsyncResult ar)
	{
		if (IsAbandoned(ar))
			return;

		byte[] data;
		try
		{
			data = Stream.EndReadPascal(ar);
			if (data == null)
				return;

			Stream.BeginReadPascal(ReadCallback);
		}
		catch (IOException ex)
		{
			if (ex.Message.ToLower() == "stream was closed")
			{
				OnClosedInvoke(ex.Message);
				return;
			}

			ex.Data["operation"] = "ReadCallback";
			OnErrorInvoke(ex);
			return;
		}
		catch (Exception ex)
		{
			ex.Data["operation"] = "ReadCallback";
			OnErrorInvoke(ex);
			return;
		}

		OnReceivedInvoke(data);
	}


	public void Send(byte[] data)
	{
		if(_closing)
			return;
		
		lock (DoneSending)
		{
			if( !DoneSending.IsSet )
			{
				_sendQueue.Enqueue(data);
				return;
			}
			DoneSending.Reset();
		}

		try
		{
			Stream.BeginWritePascal(data, WriteCallback);
		}
		catch(Exception ex)
		{
			DoneSending.Set();
			ex.Data["operation"] = "Send.BeginWritePascal";
			OnErrorInvoke(ex);
		}
	}

	void WriteCallback(IAsyncResult ar)
	{
		if (IsAbandoned(ar))
			return;

		try
		{
			Stream.EndWritePascal(ar);
		}
		catch (Exception ex)
		{
			ex.Data["operation"] = "WriteCallback.EndWritePascal";
			OnErrorInvoke(ex);
		}

		byte[] data;
		lock (DoneSending)
			if (!_sendQueue.TryDequeue(out data))
			{
				DoneSending.Set();
				return;
			}

		if (IsOpen)
			try // Если поток был освобожден, пусть здесь падает исключение
			{
				Stream.BeginWritePascal(data, WriteCallback);
			}
			catch (Exception ex)
			{
				DoneSending.Set();
				ex.Data["operation"] = "WriteCallback.BeginWritePascal";
				OnErrorInvoke(ex);
			}
	}

	public virtual void Close(string reason)
	{
		_closing = true;
		// ReSharper disable once InconsistentlySynchronizedField
		if (_sendQueue.Count > 0 && !DoneSending.Wait(300))
		{
			//Utils.Nop();
		}

		_isOpen = false;

		if (Stream != null)
		{
			Stream.Close();
			Stream = null;
		}

		if (_container != null)
		{
			_container.Dispose();
			_container = null;
		}
		//OnClosedInvoke(reason);
	}

	public virtual void Dispose()
		=> Close("Connection disposing");

	void OnReceivedInvoke(byte[] data)
	{
		OnReceived(data);
		Received?.Invoke(this, new MessageReceivedEventArgs(data));
	}

	void OnErrorInvoke(Exception exception)
	{
		if (!IsOpen)
			return;

		OnError(exception);
		Error?.Invoke(this, new ErrorEventArgs(exception));

		if (CloseOnError)
			Close(exception.Data["operation"] + " error");
	}

	void OnClosedInvoke(string reason)
	{
		OnClosed(reason);
		Closed?.Invoke(this, new ConnectionClosedEventArgs(reason));
	}

	protected virtual void OnReceived(byte[] data) { }
	protected virtual void OnError(Exception exception) { }
	protected virtual void OnClosed(string reason) { }
}