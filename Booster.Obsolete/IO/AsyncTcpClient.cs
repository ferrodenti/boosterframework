using System;
using System.Net;
using System.Net.Sockets;
using JetBrains.Annotations;

namespace Booster.IO;

[Obsolete, PublicAPI]
public class AsyncTcpClient : IDisposable
{
	public IPAddress Address { get; }
	public short Port { get; }

	TcpClient _client;
	AsyncConnection _connection;

	public bool Connected => _client != null;

	public event EventHandler<EventArgs<byte[]>> DataReceived;
	public event EventHandler<EventArgs<Exception>> Error;

	public AsyncTcpClient(IPAddress address, short port)
	{
		Port = port;
		Address = address;
	}
	public AsyncTcpClient(string address, int port) : this(IPAddress.Parse(address), (short)port)
	{
	}
	public AsyncTcpClient(long address, int port) : this( new IPAddress(address), (short)port)
	{
	}

	public void Connect()
	{
		if (_client != null)
			throw new Exception("Client already connected");

		_client = new TcpClient();
		_client.Connect(Address, Port);

		_connection = new AsyncConnection();
		_connection.Received += (_, args) => OnDataReceived(args.Data);
		_connection.Error += (_, args) =>
		{
			OnError(args.GetException());
			Disconnect();
		};


		_connection.Init(null, _client.GetStream());
	}

	public void Disconnect()
	{
		if (_connection != null)
		{
			_connection.Close("Client disconnect");
			_connection = null;
		}
		if (_client != null)
		{
			_client.Close();
			_client = null;
		}
	}

	public void Send(byte[] data)
	{
		if(!Connected)
			Connect();

		_connection.Send(data);
	}

	protected virtual void OnDataReceived(byte[] data)
	{
		DataReceived?.Invoke(this, new EventArgs<byte[]>(data));
	}

	protected virtual void OnError(Exception e)
	{
		Error?.Invoke(this, new EventArgs<Exception>(e));
	}

	public void Dispose()
	{
		Disconnect();
	}
}