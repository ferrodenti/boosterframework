﻿using System;
using System.Threading;

namespace Booster.IO;

[Obsolete]
public class AsyncResult<T> : IAsyncResult
{
	public T Result { get; set; }

	public object AsyncState { get; set; }
	public bool CompletedSynchronously { get; set; }

	public Exception Exception
	{
		get => _exception;
		set
		{
			if (_exception != value)
			{
				_exception = value;

				if (_exception != null)
					_asyncWaitHandle?.Set();
			}
		}
	}

	bool _isCompleted;
	public bool IsCompleted
	{
		get => _isCompleted;
		set
		{
			if (_isCompleted != value)
			{
				_isCompleted = value;
				_asyncWaitHandle?.Set();
			}
		}
	}

	ManualResetEvent _asyncWaitHandle;
	Exception _exception;
	public WaitHandle AsyncWaitHandle => _asyncWaitHandle ??= new ManualResetEvent(IsCompleted);
}