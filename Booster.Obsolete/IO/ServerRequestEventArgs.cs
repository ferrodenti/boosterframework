﻿using System;

namespace Booster.IO;

public class ServerRequestEventArgs : EventArgs
{
	public byte[] Request { get; }
	public byte[] Response { get; set; }

	public ServerRequestEventArgs(byte[] request)
	{
		Request = request;
	}
}