using System;

namespace Booster.IO;

public class ConnectionClosedEventArgs : EventArgs
{
	public string Reason { get; set; }

	public ConnectionClosedEventArgs() {}
	public ConnectionClosedEventArgs(string reason)
	{
		Reason = reason;
	}
}