﻿using System;

namespace Booster.IO;

public class MessageReceivedEventArgs : EventArgs
{
	public byte[] Data { get; set; }

	public MessageReceivedEventArgs() {}
	public MessageReceivedEventArgs(byte[] data)
	{
		Data = data;
	}
}