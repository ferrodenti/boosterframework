﻿using System;
using System.Security.Cryptography;

namespace Booster.Crypt;

public static class HashTypeExpander
{
	public static bool IsSome(this HashType hashType)
		=> hashType != HashType.Default && hashType != HashType.None;

	public static HashAlgorithm CreateHashAlgorithm(this HashType hashType, int hashSize = 0)
	{
		switch (hashType)
		{
		case HashType.MD5:
			return MD5.Create();

		case HashType.SHA1:
			return SHA1.Create();

		case HashType.SHA2:
			switch (hashSize)
			{
			case 0:
			case 256:
				return SHA256.Create();
			case 384:
				return SHA384.Create();
			case 512:
				return SHA512.Create();
			default:
				throw new ArgumentException("Ожидались значения (0)256, 384 или 512", nameof(hashSize));
			}

		case HashType.GOST3411:
			if (hashSize == 0)
				return new Gost3411();

			return new Gost3411(hashSize);
		default:
			return null;
		}
	}
}