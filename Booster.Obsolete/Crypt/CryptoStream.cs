using System;
using System.IO;
using System.Security.Cryptography;
using JetBrains.Annotations;

namespace Booster.Crypt;

[Obsolete, PublicAPI]
public class CryptoStream<TSymmetricAlgorithm> : Stream where TSymmetricAlgorithm : SymmetricAlgorithm, new()
{
	static readonly TSymmetricAlgorithm _algorithm = new();

	readonly Stream _stream;
	readonly CryptoStream _cryptoStream;
	readonly bool _ownsStream;

	public CryptoStream(Stream stream, byte[] key, byte[] iv, CryptoStreamMode mode)
	{
		_stream = stream;

		var transform = mode == CryptoStreamMode.Read ? _algorithm.CreateDecryptor(key, iv) : _algorithm.CreateEncryptor(key, iv);
		_cryptoStream = new CryptoStream(_stream, transform, mode);
	}

	public CryptoStream(byte[] data, byte[] key, byte[] iv)
		: this(new MemoryStream(data), key, iv, CryptoStreamMode.Read)
		=> _ownsStream = true;

	public CryptoStream(byte[] key, byte[] iv)
		: this(new MemoryStream(), key, iv, CryptoStreamMode.Write)
		=> _ownsStream = true;

	public override bool CanRead => _cryptoStream.CanRead;
	public override bool CanWrite => _cryptoStream.CanWrite;
	public override bool CanSeek => _cryptoStream.CanSeek;


	public override void Flush()
	{
		_cryptoStream.FlushFinalBlock();
		_stream.Flush();
	}

	public override long Seek(long offset, SeekOrigin origin)
		=> _cryptoStream.Seek(offset, origin);

	public override void SetLength(long value)
		=> _cryptoStream.SetLength(value);

	public override int Read(byte[] buffer, int offset, int count)
		=> _cryptoStream.Read(buffer, offset, count);

	public override void Write(byte[] buffer, int offset, int count)
		=> _cryptoStream.Write(buffer, offset, count);

	public byte[] ToArray()
	{
		if (_stream is MemoryStream stream)
			return stream.ToArray();

		throw new NotSupportedException();
	}

	public override long Length => _cryptoStream.Length;

	public override long Position
	{
		get => _cryptoStream.Position;
		set => _cryptoStream.Position = value;
	}

	protected override void Dispose(bool disposing)
	{
		base.Dispose(disposing);

		if (disposing)
		{
			_cryptoStream.Dispose();

			if (_ownsStream)
				_stream.Dispose();
		}
	}
}