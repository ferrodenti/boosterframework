﻿using System;

namespace Booster.Crypt;

[Serializable]
public class DoubleHandshakeMessage
{
	public byte[] Data1 { get; set; }
	public byte[] Data2 { get; set; }
	public int Iter { get; set; }
}