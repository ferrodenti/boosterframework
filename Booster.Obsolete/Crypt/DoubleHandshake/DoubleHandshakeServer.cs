using System.Linq;
using System.Security.Cryptography;

namespace Booster.Crypt;

public class DoubleHandshakeServer : BaseDoubleHandshake
{
	public DoubleHandshakeServer(CryptoFunctions cryptoFunctions, string password = null, byte[] id = null, byte[] nonce = null)
	{

		CryptoFunctions = cryptoFunctions;
		CheckIdAndNonce(ref id, ref nonce);
		ServerId = id;
		ServerNonce = nonce;
		if (password != null)
			Password = password;
	}

	public DoubleHandshakeServer(HashAlgorithm hashAlgorithm, string password = null, byte[] id = null, byte[] nonce = null)
		: this(new CryptoFunctions(hashAlgorithm), password, id, nonce)
	{
	}

	public override DoubleHandshakeMessage Message(DoubleHandshakeMessage message)
	{
		if (State == message.Iter)
		{
			State = message.Iter + 1;

			switch (message.Iter)
			{
			case 0:
				ClientId = message.Data1;

				return new DoubleHandshakeMessage
				{
					Data1 = ServerId,
					Data2 = ServerNonce,
					Iter = State++
				};
			case 2:
				ClientNonce = message.Data1;

				GenerateMic(ServerNonce, ServerId);
				if (Mic.SequenceEqual(message.Data2))
				{
					IsAuthorized = true;
					return new DoubleHandshakeMessage {Iter = State};
				}

				OnInvalidPassword();
				return null;
			}
		}
		OnWrongMessage();
		return null;
	}

}