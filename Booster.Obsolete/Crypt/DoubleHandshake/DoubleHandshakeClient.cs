using System.Security.Cryptography;

namespace Booster.Crypt;

public class DoubleHandshakeClient : BaseDoubleHandshake
{
	public DoubleHandshakeClient(CryptoFunctions cryptoFunctions, string password = null, byte[] id = null, byte[] nonce = null)
	{
		CryptoFunctions = cryptoFunctions;
		CheckIdAndNonce(ref id, ref nonce);
		ClientId = id;
		ClientNonce = nonce;
		if (password != null)
			Password = password;
	}

	public DoubleHandshakeClient(HashAlgorithm hashAlgorithm, string password = null, byte[] id = null, byte[] nonce = null)
		: this(new CryptoFunctions(hashAlgorithm), password, id, nonce)
	{
	}

	public override DoubleHandshakeMessage Message(DoubleHandshakeMessage message)
	{
		if (message == null)
		{
			message = new DoubleHandshakeMessage { Iter = -1 };
			State = -1;
		}

		if (State == message.Iter)
		{
			State = message.Iter + 1;

			switch (message.Iter)
			{
			case -1:
				return new DoubleHandshakeMessage
				{
					Data1 = ClientId,
					Iter = State++
				};
			case 1:
				ServerId = message.Data1;
				ServerNonce = message.Data2;

				return new DoubleHandshakeMessage
				{
					Data1 = ClientNonce,
					Data2 = GenerateMic(ServerNonce, ServerId),
					Iter = State++
				};
			case 3:
				IsAuthorized = true;
				return null;
			}
		}
		OnWrongMessage();
		return null;
	}

}