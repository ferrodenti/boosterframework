﻿namespace Booster.Crypt;

public interface IDoubleHandshakeMethodsProvider
{
	byte[] Hash(byte[] data);
	byte[] GenerateMic(byte[] nonce, byte[] id);
	byte[] GenerateSid(); //TODO: some arguments
}