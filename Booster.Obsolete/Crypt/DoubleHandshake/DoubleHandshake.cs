﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Booster.Crypt;

public abstract class BaseDoubleHandshake
{
	public byte[] ServerId;
	public byte[] ClientId;
	public byte[] ServerNonce;
	public byte[] ClientNonce;
	public byte[] Mic;

	public byte[] Pass;
	public byte[] Salt;

	public byte[] Sid1 => Keys[1];
	public byte[] Sid2 => Keys[2];

	public bool IsAuthorized;

	public int Iters = 1000;

	IComparer<byte[]> _idComparer;
	public IComparer<byte[]> IdComparer
	{
		get => _idComparer ?? new ByteArrayComparer();
		set => _idComparer = value;
	}

	IComparer<byte[]> _nonceComparer;
	public IComparer<byte[]> NonceComparer
	{
		get => _nonceComparer ?? new ByteArrayComparer();
		set => _nonceComparer = value;
	}


	protected int State;
	protected CryptoFunctions CryptoFunctions;

	public event EventHandler WrongMessage;
	protected virtual void OnWrongMessage() => WrongMessage?.Invoke(this, EventArgs.Empty);

	public event EventHandler InvalidPassword;
	protected virtual void OnInvalidPassword() => InvalidPassword?.Invoke(this, EventArgs.Empty);

	protected void CheckIdAndNonce(ref byte[] id, ref byte[] nonce)
	{
		id ??= Guid.NewGuid().ToByteArray();
		if (nonce == null)
			lock (Lock)
				nonce = CryptoFunctions.NewNonce(Rnd.Bytes(32));
	}

	public string Password
	{
		set
		{
			if (value == null)
				throw new ArgumentNullException();

			var data = Encoding.UTF8.GetBytes(value);
			lock (Lock)
			{
				var keys = CryptoFunctions.Prf(CryptoFunctions.ComputeHash(data), data, 2);
				Pass = keys[0];
				Salt = keys[1];
			}
		}
	}

	public abstract DoubleHandshakeMessage Message(DoubleHandshakeMessage message);


	protected void Compare(byte[] a, byte[] b, out byte[] min, out byte[] max, IComparer<byte[]> comparer)
	{
		if (comparer.Compare(a, b) >= 0)
		{
			min = a;
			max = b;
		}
		else
		{
			min = b;
			max = a;
		}
	}

	protected static readonly object Lock = new();

	byte[][] _keys;
	public byte[][] Keys
	{
		get
		{
			if (_keys == null)
			{
				if (Pass == null) throw new InvalidOperationException("Pass = null");
				if (Salt == null) throw new InvalidOperationException("Salt = null");
				if (ServerId == null)
					throw new InvalidOperationException("ServerID = null");
				if (ClientId == null) throw new InvalidOperationException("ClientID = null");
				if (ServerNonce == null) throw new InvalidOperationException("ServerNonce = null");
				if (ClientNonce == null) throw new InvalidOperationException("ClientNonce = null");

				Compare(ServerId, ClientId, out var minId, out var maxId, IdComparer);
				Compare(ServerNonce, ClientNonce, out var minNOnce, out var maxNonce, NonceComparer);

				var message4KeysGeneration = Salt.Concat(minId).Concat(maxId).Concat(minNOnce).Concat(maxNonce);

				lock (Lock)
				{
					var smkBytes = CryptoFunctions.Rfc2898(Pass, Salt, Iters, 32);
					_keys = CryptoFunctions.Prf(smkBytes, message4KeysGeneration, 3);
				}
			}
			return _keys;
		}
	}

	Hmac _gost;
	protected Hmac Gost => _gost ??= new Hmac(CryptoFunctions.HashAlgorithm, Keys[0]);

	protected byte[] GenerateMic(byte[] nonce, byte[] id)
	{
		var msgc = nonce.Concat(id);

		lock (Lock)
			Mic = Gost.ComputeHash(msgc);

		return Mic;
	}
}