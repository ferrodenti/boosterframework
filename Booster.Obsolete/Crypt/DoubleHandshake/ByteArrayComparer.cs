using System.Collections.Generic;

namespace Booster.Crypt;

public class ByteArrayComparer : IComparer<byte[]>
{
	public int Compare(byte[] x, byte[] y)
	{
		if (x == null)
		{
			if (y == null)
				return 0;

			return -1;
		}
		if (y == null)
			return 1;

		if (ReferenceEquals(x, y))
			return 0;

		int len = 0, cmpLen = 0;

		if (x.Length > y.Length)
		{
			len = y.Length;
			cmpLen = 1;
		}
		else if (x.Length < y.Length)
		{
			len = x.Length;
			cmpLen = - 1;
		}
		else
			len = x.Length;

		for (var i = 0; i < len; i++)
		{
			var cmp = x[i].CompareTo(y[i]);
			if (cmp != 0)
				return cmp;
		}
		if (cmpLen != 0)
			return cmpLen;
		return 0;
	}
}