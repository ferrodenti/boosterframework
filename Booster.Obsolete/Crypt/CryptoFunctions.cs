﻿using System;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using Booster.Helpers;
using Booster.Reflection;

namespace Booster.Crypt;

public class CryptoFunctions
{
	readonly TypeEx _hashAlgorithmType;

	HashAlgorithm _hashAlgorithm;
	public HashAlgorithm HashAlgorithm
	{
		get
		{
			if (_hashAlgorithm == null && _hashAlgorithmType != null)
				_hashAlgorithm = (HashAlgorithm) Activator.CreateInstance(_hashAlgorithmType);

			return _hashAlgorithm;
		}
		set => _hashAlgorithm = value;
	}

	public CryptoFunctions(HashAlgorithm hashAlgorithm) => _hashAlgorithm = hashAlgorithm ?? throw new ArgumentNullException(nameof(hashAlgorithm));

	public CryptoFunctions(TypeEx hashAlgorithmTypeType)
	{
		if (hashAlgorithmTypeType == null)
			throw new ArgumentNullException(nameof(hashAlgorithmTypeType));

		if (!typeof (HashAlgorithm).IsAssignableFrom(hashAlgorithmTypeType))
			throw new ArgumentException($"{hashAlgorithmTypeType.Name} должен наследовать HashAlgorithm", nameof(hashAlgorithmTypeType));

		var ci = hashAlgorithmTypeType.FindConstructor();
		if (ci == null || !ci.IsPublic)
			throw new ArgumentException($"{hashAlgorithmTypeType.Name} не имеет публичного конструктора без параметров", nameof(hashAlgorithmTypeType));

		_hashAlgorithmType = hashAlgorithmTypeType;
	}

	public byte[] ComputeHash(byte[] data) => _hashAlgorithm.ComputeHash(data);

	public byte[][] Prf(byte[] key, byte[] messageBytes, int n)
	{
		var result = new byte[n][];

		for (var i = 0; i < n; i++)
		{
			var hm = new Hmac(HashAlgorithm, key.Concat(EndianHelper.InvariantToByteArray(i)));
			result[i] = hm.ComputeHash(messageBytes);
		}
		return result;
	}

	public const int Rfc2898MinIterations = 4;
	public const int Rfc2898MinSaltLength = 8;

	public byte[] Rfc2898(byte[] password, byte[] salt, int iterations, int keyLength)
	{
		if (iterations < Rfc2898MinIterations)
			throw new ArgumentException($"Аргумент iterations имеет значение  меньше {Rfc2898MinIterations}.", nameof(iterations));

		if (salt.Length < Rfc2898MinSaltLength)
			throw new ArgumentException($"Аргумент salt имеет размер меньше {Rfc2898MinSaltLength} байт.", nameof(salt));

		var hmac = new Hmac(HashAlgorithm, password);

		byte[] result = null;
		for (var i = 1;; i++)
		{
			var hash = Rfc2898_F(hmac, salt, iterations, i);
			result = result == null ? hash : result.Concat(hash);

			if (result.Length > keyLength)
			{
				var r = new byte[keyLength];
				Buffer.BlockCopy(result, 0, r, 0, keyLength);
				return r;
			}
			if (result.Length == keyLength)
				return result;
		}
	}

	public  static byte[] Rfc2898_F(HashAlgorithm hashAlgorithm, byte[] salt, int iterationsNumber, int i)
	{
		var s1 = salt.Concat(EndianHelper.InvariantToByteArray(i));
		var s2 = hashAlgorithm.ComputeHash(s1);
		var numArray = s2;
		for (var index1 = 1; index1 < iterationsNumber; ++index1)
		{
			s2 = hashAlgorithm.ComputeHash(s2);
			for (var index2 = 0; index2 < s2.Length; ++index2)
				numArray[index2] ^= s2[index2];
		}
		return numArray;
	}

	static byte[] _macAddressBytes;
	public byte[] MacAddressBytes
	{
		get
		{
			if (_macAddressBytes == null)
			{
				try
				{
					foreach(var nic in NetworkInterface.GetAllNetworkInterfaces())
						try
						{
							var b = nic.GetPhysicalAddress().GetAddressBytes();
							_macAddressBytes = _macAddressBytes == null ? b : _macAddressBytes.Concat(b);
						}
						catch {}

					if (_macAddressBytes != null && _macAddressBytes.Length > 32)
						_macAddressBytes = HashAlgorithm.ComputeHash(_macAddressBytes);
				}
				catch { }
				if (_macAddressBytes == null)
					_macAddressBytes = BitConverter.GetBytes(DateTime.Now.ToBinary());
			}
			return _macAddressBytes;
		}
	}

	public byte[] NewNonce(byte[] saltBytes)
	{
		var randomKey = new byte[32];
		RandomNumberGenerator.Create().GetNonZeroBytes(randomKey);

		var dateTimeBytes = BitConverter.GetBytes(DateTime.Now.ToBinary());

		var randomBytes = saltBytes.Concat(MacAddressBytes.Concat(dateTimeBytes));

		var hashFunction = new Hmac(HashAlgorithm, randomKey);

		return hashFunction.ComputeHash(randomBytes);
	}
}

public class CryptoFunctions<THashAlgorithm> : CryptoFunctions where THashAlgorithm : HashAlgorithm, new()
{
	public CryptoFunctions() : base(typeof(THashAlgorithm))
	{
	}
}