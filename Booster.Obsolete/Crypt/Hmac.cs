﻿using System;
using System.Security.Cryptography;

namespace Booster.Crypt;

public class Hmac : HashAlgorithm
{
	const int _blockSizeBytes = 64;
	readonly HashAlgorithm _hashAlgorithm;

	public Hmac(HashAlgorithm hashAlgorithm, byte[] key)
	{
		_key = key;
		_hashAlgorithm = hashAlgorithm;
	}

	byte[] _key;
	public byte[] Key
	{
		get => _key;
		set
		{
			if (_key != value)
			{
				_key = value;
				_keyIpad = null;
				_keyOpad = null;
			}
		}
	}

	byte[] _keyIpad;
	protected byte[] KeyIpad
	{
		get
		{
			if (_keyIpad == null)
				InitPads();

			return _keyIpad;
		}
	}

	byte[] _keyOpad;
	protected byte[] KeyOpad
	{
		get
		{
			if (_keyOpad == null)
				InitPads();

			return _keyOpad;
		}
	}

	protected void InitPads()
	{
		var  normalizedKey = Key;
		if (normalizedKey.Length > _blockSizeBytes)
			normalizedKey = _hashAlgorithm.ComputeHash(normalizedKey);

		if (normalizedKey.Length < _blockSizeBytes)
		{
			var key = new byte[_blockSizeBytes];
			normalizedKey.CopyTo(key, 0);
			normalizedKey = key;
		}

		var ipad = new byte[_blockSizeBytes];
		for (var i = 0; i < _blockSizeBytes; i++)
			ipad[i] = 0x36;
		var opad = new byte[_blockSizeBytes];
		for (var i = 0; i < _blockSizeBytes; i++)
			opad[i] = 0x5C;

		_keyIpad = ipad.Xor(normalizedKey);
		_keyOpad = opad.Xor(normalizedKey);
	}

	public override void Initialize() { }

	byte[] _result;
	protected override void HashCore(byte[] array, int ibStart, int cbSize) //Реализация вычисления аутентификационного кода на основе заданной хэш-функции по алгоритму HMAC
	{
		if (_hashAlgorithm == null)
			throw new ArgumentException("Не задан алгоритм вычисления хэш-функции");

		if (Key == null)
			throw new ArgumentException("Не задан ключ");

		byte[] message;

		if (ibStart == 0 && cbSize == array.Length)
			message = array;
		else
		{
			message = new byte[cbSize];
			Buffer.BlockCopy(array, ibStart, message, 0, cbSize);
		}

		var concatedMessage1 = KeyIpad.Concat(message);

		// hash(xorKeyIpad | Message)
		var hash1 = _hashAlgorithm.ComputeHash(concatedMessage1);

		// xorKeyOpad || hash(xorKeyIpad | Message)
		var concatedMessage2 = KeyOpad.Concat(hash1);

		// hash(xorKeyIpad | Message)
		_result = _hashAlgorithm.ComputeHash(concatedMessage2);
	}

	protected override byte[] HashFinal()
		=> _result;
}