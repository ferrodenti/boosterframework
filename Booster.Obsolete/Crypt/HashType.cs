﻿
using System.Diagnostics.CodeAnalysis;

namespace Booster.Crypt;

[SuppressMessage("ReSharper", "InconsistentNaming")]
public enum HashType
{
	Default = 0,
	None = 1,
	MD5 = 2,
	SHA1 = 3,
	SHA2 = 4,
	GOST3411 = 5
}