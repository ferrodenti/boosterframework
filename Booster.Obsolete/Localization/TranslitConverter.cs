﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using JetBrains.Annotations;

namespace Booster.Localization;

[Obsolete("Use Transliterator")]
[PublicAPI]
public class TranslitConverter
{
	#region Dictionaries

	//ГОСТ 16876-71
	static readonly Dictionary<char, string> _gost = new()
	{
		{'Є', "EH"},
		{'І', "I"},
		{'і', "i"},
		{'є', "eh"},
		{'А', "A"},
		{'Б', "B"},
		{'В', "V"},
		{'Г', "G"},
		{'Д', "D"},
		{'Е', "E"},
		{'Ё', "JO"},
		{'Ж', "ZH"},
		{'З', "Z"},
		{'И', "I"},
		{'Й', "JJ"},
		{'К', "K"},
		{'Л', "L"},
		{'М', "M"},
		{'Н', "N"},
		{'О', "O"},
		{'П', "P"},
		{'Р', "R"},
		{'С', "S"},
		{'Т', "T"},
		{'У', "U"},
		{'Ф', "F"},
		{'Х', "KH"},
		{'Ц', "C"},
		{'Ч', "CH"},
		{'Ш', "SH"},
		{'Щ', "SHH"},
		{'Ъ', "'"},
		{'Ы', "Y"},
		{'Ь', ""},
		{'Э', "EH"},
		{'Ю', "YU"},
		{'Я', "YA"},
		{'а', "a"},
		{'б', "b"},
		{'в', "v"},
		{'г', "g"},
		{'д', "d"},
		{'е', "e"},
		{'ё', "jo"},
		{'ж', "zh"},
		{'з', "z"},
		{'и', "i"},
		{'й', "jj"},
		{'к', "k"},
		{'л', "l"},
		{'м', "m"},
		{'н', "n"},
		{'о', "o"},
		{'п', "p"},
		{'р', "r"},
		{'с', "s"},
		{'т', "t"},
		{'у', "u"},
		{'ф', "f"},
		{'х', "kh"},
		{'ц', "c"},
		{'ч', "ch"},
		{'ш', "sh"},
		{'щ', "shh"},
		{'ъ', ""},
		{'ы', "y"},
		{'ь', ""},
		{'э', "eh"},
		{'ю', "yu"},
		{'я', "ya"},
		{'—', "-"}
	};
	//ISO 9-95
	static readonly Dictionary<char, string> _iso = new()
	{
		{'Є', "YE"},
		{'І', "I"},
		{'Ѓ', "G"},
		{'і', "i"},
		{'є', "ye"},
		{'ѓ', "g"},
		{'А', "A"},
		{'Б', "B"},
		{'В', "V"},
		{'Г', "G"},
		{'Д', "D"},
		{'Е', "E"},
		{'Ё', "YO"},
		{'Ж', "ZH"},
		{'З', "Z"},
		{'И', "I"},
		{'Й', "J"},
		{'К', "K"},
		{'Л', "L"},
		{'М', "M"},
		{'Н', "N"},
		{'О', "O"},
		{'П', "P"},
		{'Р', "R"},
		{'С', "S"},
		{'Т', "T"},
		{'У', "U"},
		{'Ф', "F"},
		{'Х', "X"},
		{'Ц', "C"},
		{'Ч', "CH"},
		{'Ш', "SH"},
		{'Щ', "SHH"},
		{'Ъ', "'"},
		{'Ы', "Y"},
		{'Ь', ""},
		{'Э', "E"},
		{'Ю', "YU"},
		{'Я', "YA"},
		{'а', "a"},
		{'б', "b"},
		{'в', "v"},
		{'г', "g"},
		{'д', "d"},
		{'е', "e"},
		{'ё', "yo"},
		{'ж', "zh"},
		{'з', "z"},
		{'и', "i"},
		{'й', "j"},
		{'к', "k"},
		{'л', "l"},
		{'м', "m"},
		{'н', "n"},
		{'о', "o"},
		{'п', "p"},
		{'р', "r"},
		{'с', "s"},
		{'т', "t"},
		{'у', "u"},
		{'ф', "f"},
		{'х', "x"},
		{'ц', "c"},
		{'ч', "ch"},
		{'ш', "sh"},
		{'щ', "shh"},
		{'ъ', ""},
		{'ы', "y"},
		{'ь', ""},
		{'э', "e"},
		{'ю', "yu"},
		{'я', "ya"},
		{'—', "-"}
	};

	static readonly Dictionary<char, string> _pirate = new()
	{
		{'Є', "E"},
		{'І', "I"},
		{'Ѓ', "r"},
		{'і', "i"},
		{'є', "e"},
		{'ѓ', "r"},
		{'А', "A"},
		{'Б', "6"},
		{'В', "B"},
		{'Г', "r"},
		{'Д', "9"},
		{'Ё', "E"},
		{'Е', "E"},
		{'Ж', "}|{"},
		{'З', "3"},
		{'Й', "U"},
		{'И', "U"},
		{'К', "K"},
		{'Л', "JI"},
		{'М', "M"},
		{'Н', "H"},
		{'О', "O"},
		{'П', "n"},
		{'Р', "P"},
		{'С', "C"},
		{'Т', "T"},
		{'У', "Y"},
		{'Ф', "qp"},
		{'Х', "X"},
		{'Ц', "U"},
		{'Ч', "4"},
		{'Ш', "W"},
		{'Щ', "W,"},
		{'Ъ', "'b"},
		{'Ы', "bI"},
		{'Ь', "b"},
		{'Э', "-)"},
		{'Ю', "|-O"},
		{'Я', "9|"},
		{'а', "a"},
		{'б', "6"},
		{'в', "B"},
		{'г', "r"},
		{'д', "9"},
		{'ё', "e"},
		{'е', "e"},
		{'ж', "}|{"},
		{'з', "3"},
		{'й', "u"},
		{'и', "u"},
		{'к', "k"},
		{'л', "JI"},
		{'м', "m"},
		{'н', "H"},
		{'о', "o"},
		{'п', "n"},
		{'р', "p"},
		{'с', "c"},
		{'т', "T"},
		{'у', "y"},
		{'ф', "qp"},
		{'х', "x"},
		{'ц', "u"},
		{'ч', "4"},
		{'ш', "w"},
		{'щ', "w,"},
		{'ъ', "'b"},
		{'ы', "bI"},
		{'ь', "b"},
		{'э', "-)"},
		{'ю', "I-o"},
		{'я', "9|"},
		{'—', "-"}
	};

	static readonly ConcurrentDictionary<TransliterationType, Dictionary<string, char>> _backwardDicts = new();
		
	#endregion
		
	public TransliterationType TransliterationType { get; }

	public bool SkipSymbols { get; set; }
	public bool SkipSpaces { get; set; }

	public Dictionary<char, string> CustomReplace { get; set; }

	readonly Dictionary<char,string> _forwardDict;


	Dictionary<string, char> _backwardDict;
	protected Dictionary<string, char> BackwardDict => _backwardDict ??= _backwardDicts.GetOrAdd(TransliterationType, _ => CreateBackwardDict(_forwardDict));

	Dictionary<string, char> _customBackwardDict;
	protected Dictionary<string, char> CustomBackwardDict => _customBackwardDict ??= CreateBackwardDict(CustomReplace) ?? new Dictionary<string, char>();

	Dictionary<string, char> CreateBackwardDict(Dictionary<char, string> dict)
	{
		var res = new Dictionary<string, char>();

		if( dict != null)
			foreach(var pair in dict)
				if (!string.IsNullOrEmpty(pair.Value))
					res[pair.Value] = pair.Key;

		return res;
	}

	public TranslitConverter(TransliterationType transliterationType = TransliterationType.ISO)
	{
		TransliterationType = transliterationType;
		SkipSymbols = true;
		CustomReplace = new Dictionary<char, string>();

		_forwardDict = TransliterationType switch
		               {
			               TransliterationType.ISO    => _iso,
			               TransliterationType.Gost   => _gost,
			               TransliterationType.Pirate => _pirate,
			               _                          => throw new ArgumentOutOfRangeException()
		               };
	}

	public virtual string ToTranslit(string src)
	{
		var dst = new StringBuilder();

		if(src != null)
			foreach (var ch in src)
			{
				if (_forwardDict.TryGetValue(ch, out var tr))
					dst.Append(tr);
				if (CustomReplace != null && CustomReplace.TryGetValue(ch, out tr))
					dst.Append(tr);
				else
				{
					if (SkipSymbols)
					{
						if (ch == ' ' || ch == '\t')
						{
							if (SkipSpaces)
								continue;
						}
						else if (!ch.IsLatin() && !ch.IsDigit())
							continue;
					}

					dst.Append(ch);
				}
			}

		return dst.ToString();
	}

	public virtual string FromTranslit(string src)
	{
		var dst = new StringBuilder();
		var i = 0;
		for (; i < src.Length; i++)
		{
			char rep;

			var ch1 = src[i].ToString();
			if (i < src.Length - 1)
			{
				var ch2 = ch1 + src[i + 1];
				if (i < src.Length - 2)
				{
					var ch3 = ch2 + src[i + 2];
					if (CustomBackwardDict.TryGetValue(ch3, out rep))
					{
						dst.Append(rep);
						i += 2;
						continue;
					}
					if (BackwardDict.TryGetValue(ch3, out rep))
					{
						dst.Append(rep);
						i += 2;
						continue;
					}
				}
				if (CustomBackwardDict.TryGetValue(ch2, out rep))
				{
					dst.Append(rep);
					i += 1;
					continue;
				}
				if (BackwardDict.TryGetValue(ch2, out rep))
				{
					dst.Append(rep);
					i += 1;
					continue;
				}
			}
			if (CustomBackwardDict.TryGetValue(ch1, out rep))
			{
				dst.Append(rep);
				continue;
			}
			if (BackwardDict.TryGetValue(ch1, out rep))
			{
				dst.Append(rep);
				continue;
			}
			dst.Append(ch1);

		}
		return dst.ToString();
	}
}