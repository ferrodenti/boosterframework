using System;

namespace Booster.Localization;

[Obsolete("Use Transliterator")]
public enum TransliterationType
{
// ReSharper disable once InconsistentNaming
	ISO,
	Gost,
	Pirate
}