using System;
using System.IO;
using System.Threading;

namespace Booster;

[Obsolete]
public static class FileHelper //TODO: change namespace of assembly
{
	const int _retryCount = 10;
	const int _retryInterval = 500;
	const int _clusterSize = 4096;
	const int _maxChunk = _clusterSize * 256; // 32768;

	public static string MakeRooted(string fileName)
	{
		if (!Path.IsPathRooted(fileName))
			fileName = Path.Combine(Directory.GetCurrentDirectory(), fileName);

		return fileName;
	}

	public static bool TryOpen(string fileName, FileMode mode, FileAccess access, FileShare share, int bufferSize, bool useAsync, out FileStream stream)
	{
		try
		{
			fileName = MakeRooted(fileName);
			stream = new FileStream(fileName, mode, access, share, bufferSize, useAsync);
			return true;
		}
		catch
		{
			stream = null;
			return false;
		}
	}

	public static FileStream SafeOpen(string fileName, FileMode mode, FileAccess access, FileShare share, int bufferSize, bool useAsync) //TODO: RetryInterval, RetryCount default params
	{
		fileName = MakeRooted(fileName);

		for (var i = 0; ; i++)
			try
			{
				return new FileStream(fileName, mode, access, share, bufferSize, useAsync);
			}
			catch (Exception)
			{
				if (i == _retryCount)
					throw;

				Thread.Sleep(_retryInterval);
			}
	}


	public static bool TryDelete(string fileName)
	{
		try
		{
			fileName = MakeRooted(fileName);

			if (File.Exists(fileName))
				File.Delete(fileName);

			return true;
		}
		catch
		{
			return false;
		}
	}
	public static void SafeDelete(string fileName) //TODO: RetryInterval, RetryCount default params
	{
		fileName = MakeRooted(fileName);

		for (var i = 0; ; i++)
			try
			{
				if (File.Exists(fileName))
					File.Delete(fileName);

				return;
			}
			catch (Exception)
			{
				if (i == _retryCount)
					throw;

				Thread.Sleep(_retryInterval);
			}
	}


	public static int GetChunkSize(long fileSize) //TODO: ClusterSize, MaxChunk default params
	{
		//return 42;

		if (fileSize > _maxChunk)
			return _maxChunk;

		return (int)((fileSize / _clusterSize) + 1) * _clusterSize;
	}
}