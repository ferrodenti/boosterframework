using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using Booster.Orm.Interfaces;
using Booster.Orm.Linq;
using Booster.Orm.Linq.Interfaces;
using Booster.Orm.Linq.Kitchen;
using Booster.Reflection;
using JetBrains.Annotations;

namespace Booster.Orm.Kitchen;

[PublicAPI]
public class BaseQueryCompiler : IQueryCompiler
{
	protected readonly IOrm Orm;
	protected readonly ISqlFormatter SqlFormatter;
	protected readonly IDbTypeDescriptor DbTypeDescriptor;
	readonly Lazy<Dictionary<string, Func<ReadOnlyCollection<Expression>, QueryCompilationContext, CompiledQueryBuilder>>> _operators;
	public Dictionary<string, Func<ReadOnlyCollection<Expression>, QueryCompilationContext, CompiledQueryBuilder>> Operators => _operators.Value;

	public BaseQueryCompiler(IOrm orm)
	{
		Orm = orm;
		SqlFormatter = Orm.SqlFormatter;
		DbTypeDescriptor = Orm.DbTypeDescriptor;
		_operators = new Lazy<Dictionary<string, Func<ReadOnlyCollection<Expression>, QueryCompilationContext, CompiledQueryBuilder>>>(CreateOperators);
	}
		
	protected virtual Dictionary<string, Func<ReadOnlyCollection<Expression>, QueryCompilationContext, CompiledQueryBuilder>> CreateOperators()
		=> new()
		{
			{"As", AsOperator},
			{"Any", AnyOperator},
			{"All", AllOperator},
			{"Sum", SumOperator},
			{"Count", CountOperator},
			{"Param", ParamOperator},
			{"Distinct", DistinctOperator},
			{"AggBitAnd", AggBitAndOperator},
			{"AggBitOr", AggBitOrOperator},
			{"AggBitXor", AggBitXorOperator},
		};

	public CompiledQueryBuilder Compile(IQuery query, QueryCompilationContext context)
		=> query switch
		   {
			   IUpdateQuery updateQuery => CompileUpdateQuery(updateQuery, context),
			   IInsertQuery insertQuery => CompileInsertQuery(insertQuery, context),
			   ISelectQuery selectQuery => CompileSelectQuery(selectQuery, context),
			   IDeleteQuery deleteQuery => CompileDeleteQuery(deleteQuery, context),
			   _                        => throw new ArgumentOutOfRangeException(nameof(query))
		   };

	protected string TableNameWithAlias(IQuery query, QueryCompilationContext context)
		=> query.AliasObj == null 
			? query.TableName 
			: $"{query.TableName} {query.AliasObj.GetName(context)}";

	protected CompiledQueryBuilder CompileSelectQuery(ISelectQuery selectQuery, QueryCompilationContext context)
	{
		var result = new CompiledQueryBuilder();

		result.Append("select ");
			
		var columns = new EnumBuilder(",");
		foreach (var expr in selectQuery.Columns)
		{
			var col = CompileExpression(expr, context);
			columns.Append(col.GetSql());
			result.Type = result.Type == null ? col.Type : TypeEx.Get<object>();
		}

		result.Append(columns);

		if (selectQuery.FromQuery != null)
		{
			result.Append($" from ({CompileSelectQuery(selectQuery.FromQuery, context).GetSql()})");
			if (selectQuery.FromQueryAs.IsSome())
				result.Append($" as {selectQuery.FromQueryAs}");
		}
		else
			result.Append($" from {TableNameWithAlias(selectQuery, context)}");
			
		result.Append(EmitWhere(selectQuery.WhereCondition, context));

		if (selectQuery.Groups != null)
		{
			var groupBy = new EnumBuilder(",", " group by ");
			foreach (var expr in selectQuery.Groups)
				groupBy.Append(CompileExpression(expr, context).GetSql());

			result.Append(groupBy);
		}

		if (selectQuery.Orders != null)
		{
			var sortBy = new EnumBuilder(",", " order by ");
			foreach (var expr in selectQuery.Orders)
				sortBy.Append(CompileExpression(expr, context).GetSql());

			result.Append(sortBy);
		}

		return result;
	}

	protected CompiledQueryBuilder CompileInsertQuery(IInsertQuery insertQuery, QueryCompilationContext context)
	{
		var result = new CompiledQueryBuilder();

		result.Append($"insert into {TableNameWithAlias(insertQuery, context)}");

		var columns = new EnumBuilder(",", "(", ")");
		var values = new EnumBuilder(",", " values(", ")");
		foreach (var setter in insertQuery.Setters)
		{
			columns.Append(setter.Column);
			if(setter.Set != null)
				values.Append(CompileExpression(setter.Set, context).GetSql());
		}

		result.Append(columns);
		result.Append(values);

		if (insertQuery.FromQuery != null)
			result.Append(CompileSelectQuery(insertQuery.FromQuery, context).GetSql());

		if (insertQuery.OnConflictColumns != null)
		{
			var onConflict = new EnumBuilder(",", " on conflict(", ")");
			foreach (var field in insertQuery.OnConflictColumns)
				onConflict.Append(field);

			result.Append(onConflict);
			result.Append(insertQuery.InsertConflictUpdateSet != null
				? $" update {EmitSetters(insertQuery.InsertConflictUpdateSet.Setters, context)}"
				: " do nothing");
		}

		result.Type = typeof(int);
		return result;
	}

	protected CompiledQueryBuilder CompileUpdateQuery(IUpdateQuery updateQuery, QueryCompilationContext context)
	{
		var result = new CompiledQueryBuilder();

		result.Append($"update {TableNameWithAlias(updateQuery, context)}");
		result.Append(EmitSetters(updateQuery.Setters, context));
		result.Append(EmitWhere(updateQuery.WhereCondition, context));

		result.Type = typeof(int);
		return result;
	}

	string EmitWhere(Expression whereCondition, QueryCompilationContext context)
		=> whereCondition != null 
			? $" where {CompileExpression(whereCondition, context).GetSql()}" 
			: null;

	string EmitSetters(IEnumerable<Setter> setters, QueryCompilationContext context)
	{
		var result = new EnumBuilder(",", " set ");
			
		foreach (var setter in setters)
			result.Append($"{setter.Column}={CompileExpression(setter.Set, context).GetSql()}");
			
		return result;
	}

	protected CompiledQueryBuilder CompileDeleteQuery(IDeleteQuery deleteQuery, QueryCompilationContext context)
	{
		var result = new CompiledQueryBuilder();

		result.Append($"delete from {TableNameWithAlias(deleteQuery, context)}");
		result.Append(EmitWhere(deleteQuery.WhereCondition, context));

		result.Type = typeof(int);
		return result;
	}

	protected CompiledQueryBuilder CompileExpression(Expression expression, QueryCompilationContext context)
	{
		while (true)
		{
			var result = new CompiledQueryBuilder();

			switch (expression)
			{
			case ConstantExpression constantExpression:
				switch (constantExpression.Value)
				{
				case ParamRef paramRef:
					context.AddParam(paramRef);
					result.Type = paramRef.Type;
					result.Append(SqlFormatter.ParamName(paramRef.Name));
					return result;
					
				case FieldRef fieldRef:
					result.Type = fieldRef.Type;
					result.Append(SqlFormatter.EscapeName(fieldRef.Name));
					return result;
				case IQuery query:
					var bld = Compile(query, context);
					result.Type = bld.Type;
					result.HasBrackets = true;
					result.Append($"({bld.GetSql()})");
					return result;
				default:
					result.Type = constantExpression.Type;
					result.Append(SqlFormatter.FormatConst(constantExpression.Value));
					return result;
				}

			case LambdaExpression lambdaExpression:
				expression = lambdaExpression.Body;
				continue;

			case BinaryExpression binaryExpression:
				var (format, priority) = CompileBinaryExpression(binaryExpression);
				var left = CompileExpression(binaryExpression.Left, context);
				if (left.Type == null)
					Utils.Nop();
				var rightExp = binaryExpression.Right;
				if (rightExp is UnaryExpression un && rightExp.NodeType == ExpressionType.Convert && rightExp.Type == left.Type)
					rightExp = un.Operand;
				var right = CompileExpression(rightExp, context);
				result.Type = left.Type; //TODO: надо вычислять наиболее точный тип из 2х аргументов
				result.Priority = priority;
				result.AppendFormat(format, left.GetSql(priority), right.GetSql(priority));
				return result;
				
			case UnaryExpression unaryExpression:
				switch (unaryExpression.NodeType)
				{
				case ExpressionType.Convert:
					var op = CompileExpression(unaryExpression.Operand, context);
						
					if (op.Type.CanCastTo(unaryExpression.Type, CastType.Enums | CastType.Nullables | CastType.ToBase | CastType.Implicit)) 
						return op;

					var typeName = DbTypeDescriptor.GetDbTypeFor(expression.Type);
					if (typeName != null)
					{
						result.Type = expression.Type;
						result.Append(Cast(op, typeName));
						return result;
					}

					return op;

				case ExpressionType.ArrayLength:
					result.Type = typeof(int);
					result.Append(ArrayLength(unaryExpression.Operand, context));
					return result;

				case ExpressionType.Unbox:
				case ExpressionType.Quote:
					expression = unaryExpression.Operand;
					continue;

				default:
					(format, priority) = EmitUnaryExpression(unaryExpression);
					var op1 = CompileExpression(unaryExpression.Operand, context);
					result.Type = op1.Type;
					result.Priority = priority;
					result.AppendFormat(format, op1.GetSql(priority));
					break;
				}
				break;
				
			case MemberExpression memberExpression:
				if (memberExpression.Expression.NodeType == ExpressionType.Parameter)
				{
					var repo = Orm.GetRepo(memberExpression.Expression.Type);
					if (repo.TableMapping.TryGetColumnByName(memberExpression.Member.Name, out var column))
					{
						result.Append(column.EscapedColumnName);
						result.Type = column.MemberValueType;
						return result;
					}
				}

				if (memberExpression.Member is PropertyInfo
						{
							Name: "Count",
							GetMethod.IsPublic: true,
							SetMethod: null,
							DeclaringType: not null,
						}
						pi &&
					pi.PropertyType == typeof(int) &&
					TypeEx.Get(pi.DeclaringType).HasInterface(typeof(ICollection<>)))
				{
					result.Type = typeof(int);
					result.Append(ArrayLength(memberExpression.Expression, context));
					return result;
				}


				if (memberExpression.Expression is MemberExpression {Expression: ConstantExpression})
				{
					var param = CreateParam(memberExpression, context);
					result.Type = param.Type;
					result.Append(SqlFormatter.ParamName(param.Name));
					return result;
				}

				if (memberExpression.Member is FieldInfo fi)
				{
					var param = CreateParam(fi, context);
					result.Type = param.Type;
					result.Append(SqlFormatter.ParamName(param.Name));
					return result;

				}
				break;
		
			case MethodCallExpression methodCallExpression:
				if (methodCallExpression.Method.GetCustomAttribute(typeof(ExecuteImmediatelyAttribute)) != null)
				{
					var methodResult = Invoke(methodCallExpression);
					expression = Expression.Constant(methodResult);
					continue;
				}

				if (methodCallExpression.Method.DeclaringType.Ex()?.HasInterface<IAlias>() == true)
				{
					var alias =  (IAlias)Invoke(methodCallExpression.Object);

					var op = CompileExpression(methodCallExpression.Arguments[0], context);
					result.Append($"{alias.GetName(context)}.{op.GetSql()}");
					result.Type = op.Type;
					return result;
				}

				var attr = (OperatorAttribute) methodCallExpression.Method.GetCustomAttribute(typeof(OperatorAttribute));
				if (attr != null && Operators.TryGetValue(attr.Name, out var proc)) 
					return proc(methodCallExpression.Arguments, context);

				break;

			default:
				switch (expression.NodeType)
				{
				case ExpressionType.AddAssign:
				case ExpressionType.AddAssignChecked:
				case ExpressionType.AddChecked:
				case ExpressionType.AndAlso:
				case ExpressionType.AndAssign:
				case ExpressionType.ArrayIndex:
				case ExpressionType.Assign:
				case ExpressionType.Block:
				case ExpressionType.Conditional:
				case ExpressionType.ConvertChecked:
				case ExpressionType.DebugInfo:
				case ExpressionType.Decrement:
				case ExpressionType.Default:
				case ExpressionType.DivideAssign:
				case ExpressionType.Dynamic:
				case ExpressionType.ExclusiveOrAssign:
				case ExpressionType.Extension:
				case ExpressionType.Goto:
				case ExpressionType.Increment:
				case ExpressionType.Index:
				case ExpressionType.Invoke:
				case ExpressionType.IsFalse:
				case ExpressionType.IsTrue:
				case ExpressionType.Label:
				case ExpressionType.LeftShiftAssign:
				case ExpressionType.ListInit:
				case ExpressionType.Loop:
				case ExpressionType.MemberInit:
				case ExpressionType.Modulo:
				case ExpressionType.ModuloAssign:
				case ExpressionType.MultiplyAssign:
				case ExpressionType.MultiplyAssignChecked:
				case ExpressionType.MultiplyChecked:
				case ExpressionType.NegateChecked:
				case ExpressionType.New:
				case ExpressionType.NewArrayBounds:
				case ExpressionType.NewArrayInit:
				case ExpressionType.OnesComplement:
				case ExpressionType.OrAssign:
				case ExpressionType.OrElse:
				case ExpressionType.PostDecrementAssign:
				case ExpressionType.PostIncrementAssign:
				case ExpressionType.PowerAssign:
				case ExpressionType.PreDecrementAssign:
				case ExpressionType.PreIncrementAssign:
				case ExpressionType.Quote:
				case ExpressionType.RightShiftAssign:
				case ExpressionType.RuntimeVariables:
				case ExpressionType.SubtractAssign:
				case ExpressionType.SubtractAssignChecked:
				case ExpressionType.SubtractChecked:
				case ExpressionType.Switch:
				case ExpressionType.Throw:
				case ExpressionType.Try:
				case ExpressionType.TypeAs:
				case ExpressionType.TypeEqual:
				case ExpressionType.TypeIs:
				case ExpressionType.UnaryPlus:
					break;
				}
				break;
			}

			Utils.Nop();

			throw new InvalidOperationException($"Could not compile expression {expression}");
		}
	}

	static object Invoke(Expression expression)
		=> Expression.Lambda(expression).Compile().DynamicInvoke();

	protected ParamRef CreateParam(FieldInfo fieldInfo, QueryCompilationContext context)
	{
		var type = DbTypeDescriptor.GetValueType(fieldInfo.FieldType);
		var param = new ParamRef
		{
			Type = type, 
			Expreession = fieldInfo, 
			ValueGetter = target => CastParamValue(fieldInfo.GetValue(target), type), 
			Name = fieldInfo.Name
		};

		context.AddParam(param);
		return param;
	}

	protected ParamRef CreateParam(MemberExpression memberExpression, QueryCompilationContext context)
	{
		var result = new StringBuilder();
		var expr = memberExpression.Expression;
		var runtimeClass = "";
		while (expr is MemberExpression mem)
		{
			expr = mem.Expression;
			if (expr is ConstantExpression constantExpression)
				runtimeClass = constantExpression.ToString();
		}
		var something = memberExpression.ToString().Replace(runtimeClass, "").Trim('.').SeparateCamelCase(false, "_");

		var param = CreateParam(memberExpression);

		var last = '\0';
		foreach (var ch in something)
		{
			if (result.Length == 0 && char.IsDigit(ch))
				result.Append('p');

			if (char.IsLetterOrDigit(ch))
			{
				last = char.ToLower(ch);
				result.Append(last);
			}
			else if (last != '_' && ch is '.' or '_')
			{
				last = '_';
				result.Append('_');
			}
		}

		if (result.Length > 0)
		{
			var name = result.ToString();
			var cnt = 0;
			while (true)
			{
				if (!context.Parameters.TryGetValue(name, out var existing))
					break;

				if (Equals(existing.Expreession, param.Expreession) && existing.Type.Is(param.Type))
					return existing;
					
				name = $"{result}{++cnt}";
			}

			param.Name = name;
		}
			
		context.AddParam(param);
		return param;
	}

	ParamRef CreateParam(Expression memberExpression)
	{
		var type = DbTypeDescriptor.GetValueType(memberExpression.Type); 
		return new ParamRef
		{
			Type = type,
			Expreession = memberExpression,
			ValueGetter = _ => CastParamValue(Invoke(memberExpression), type)
		};
	}

	static object CastParamValue(object result, TypeEx type)
	{
		if (result != null && TypeEx.Of(result).TryCast(result, type, out var obj))
			result = obj;

		return result;
	}


	protected string ArrayLength(Expression expression, QueryCompilationContext context)
	{
		var op = CompileExpression(expression, context);
		return $"array_length({op.GetSql()}, 1)";
	}

		
	protected CompiledQueryBuilder AsOperator(ReadOnlyCollection<Expression> arguments, QueryCompilationContext context)
	{
		var result = new CompiledQueryBuilder();
		var op = CompileExpression(arguments[0], context);
		result.Append($"{op.GetSql()} as {((ConstantExpression) arguments[1]).Value}");
		result.Type = op.Type;
		return result;
	}
		
	protected CompiledQueryBuilder AnyOperator(ReadOnlyCollection<Expression> arguments, QueryCompilationContext context)
	{
		var result = new CompiledQueryBuilder();
		var op = CompileExpression(arguments[0], context);
		result.Type = op.Type.ArrayItemType;
		result.Append(op.HasBrackets ? $"any{op.GetSql()}" : $"any({op.GetSql()})");
		return result;
	}
	protected CompiledQueryBuilder AllOperator(ReadOnlyCollection<Expression> arguments, QueryCompilationContext context)
	{
		var result = new CompiledQueryBuilder();
		var op = CompileExpression(arguments[0], context);
		result.Type = op.Type.ArrayItemType;
		result.Append(op.HasBrackets ? $"all{op.GetSql()}" : $"all({op.GetSql()})");
		return result;
	}
		

	protected CompiledQueryBuilder AggFunction(ReadOnlyCollection<Expression> arguments, QueryCompilationContext context, string name, TypeEx returnType = null)
	{
		var result = new CompiledQueryBuilder();
		var operand = CompileExpression(arguments[0], context);
		result.Type = returnType ?? operand.Type;
		result.Append($"{name}({operand.GetSql()})");
		return result;
	}
	protected CompiledQueryBuilder SumOperator(ReadOnlyCollection<Expression> arguments, QueryCompilationContext context)
		=> AggFunction(arguments, context, "sum");

	protected CompiledQueryBuilder CountOperator(ReadOnlyCollection<Expression> arguments, QueryCompilationContext context)
	{
		var result = new CompiledQueryBuilder {Type = typeof(int)};
		if (arguments.Count == 0)
			result.Append("count(*)");
		else
		{
			var op = CompileExpression(arguments[0], context);
			result.Append($"count({op.GetSql()})");
		}
		return result;
	}

	protected CompiledQueryBuilder AggBitAndOperator(ReadOnlyCollection<Expression> arguments, QueryCompilationContext context)
		=> AggFunction(arguments, context, "bit_and");

	protected CompiledQueryBuilder AggBitOrOperator(ReadOnlyCollection<Expression> arguments, QueryCompilationContext context)
		=> AggFunction(arguments, context, "bit_or");

	protected CompiledQueryBuilder AggBitXorOperator(ReadOnlyCollection<Expression> arguments, QueryCompilationContext context)
		=> AggFunction(arguments, context, "bit_xor");

	protected CompiledQueryBuilder AliasRef(ReadOnlyCollection<Expression> arguments, QueryCompilationContext context)
	{
		var result = new CompiledQueryBuilder();
		var op = CompileExpression(arguments[0], context);
		result.Append($"{op.GetSql()} as {((ConstantExpression) arguments[1]).Value}");
		result.Type = op.Type;
		return result;
	}

	protected CompiledQueryBuilder ParamOperator(ReadOnlyCollection<Expression> arguments, QueryCompilationContext context)
	{
		var result = new CompiledQueryBuilder();
		ParamRef param;
		switch (arguments.Count)
		{
		case 1:
			param = CreateParam(arguments[0] as MemberExpression, context);
			break;
		case 2:
			param = CreateParam(arguments[0]);
			param.Name = Invoke(arguments[1])?.ToString();
			break;
		default:
			throw new ArgumentException();
		}

		result.Type = param.Type;
		result.Append(SqlFormatter.ParamName(param.Name));
		return result;
	}

	protected CompiledQueryBuilder DistinctOperator(ReadOnlyCollection<Expression> arguments, QueryCompilationContext context)
	{
		var result = new CompiledQueryBuilder();
		result.Append("distinct "); 
			
		var first = true;
		var type = typeof(object);
		foreach (var arg in arguments)
		{
			var op = CompileExpression(arg, context);

			if (first)
			{
				first = false;
				type = op.Type;
			}
			else
			{
				result.Append(", ");
				type = typeof(object);
			}
			result.Append(op.GetSql());
		}

		result.Type = type;
		return result;
	}

	// ReSharper disable once MemberCanBeMadeStatic.Local
	string Cast(CompiledQueryBuilder operand, string typeName)
		=> $"{operand.GetSql(0)}::{typeName}";

	static Tuple<string, int> CompileBinaryExpression(Expression expression)
		=> expression.NodeType switch
		   {
			   ExpressionType.Coalesce => /*			*/ Tuple.Create("coalesce({0},{1})", 0),
			   ExpressionType.AndAlso => /*				*/ Tuple.Create("{0} and {1}", 0),
			   ExpressionType.OrElse => /*				*/ Tuple.Create("{0} or {1}", 0),
			   ExpressionType.Equal => /*				*/ Tuple.Create("{0}={1}", 1),
			   ExpressionType.NotEqual => /*			*/ Tuple.Create("{0}!={1}", 1),
			   ExpressionType.LessThan => /*			*/ Tuple.Create("{0}<{1}", 1),
			   ExpressionType.LessThanOrEqual => /*		*/ Tuple.Create("{0}<={1}", 1),
			   ExpressionType.GreaterThan => /*			*/ Tuple.Create("{0}>{1}", 1),
			   ExpressionType.GreaterThanOrEqual => /*	*/ Tuple.Create("{0}>={1}", 1),
			   ExpressionType.Add => /*					*/ Tuple.Create("{0}+{1}", 3),
			   ExpressionType.Subtract => /*			*/ Tuple.Create("{0}-{1}", 3),
			   ExpressionType.Multiply => /*			*/ Tuple.Create("{0}*{1}", 4),
			   ExpressionType.Divide => /*				*/ Tuple.Create("{0}/{1}", 4),
			   ExpressionType.Power => /*				*/ Tuple.Create("{0}^{1}", 5),
			   ExpressionType.LeftShift => /*			*/ Tuple.Create("{0}<<{1}", 5),
			   ExpressionType.RightShift => /*			*/ Tuple.Create("{0}>>{1}", 5),
			   ExpressionType.And => /*					*/ Tuple.Create("{0}&{1}", 5),
			   ExpressionType.Or => /*					*/ Tuple.Create("{0}|{1}", 5),
			   ExpressionType.ExclusiveOr => /*			*/ Tuple.Create("{0}^{1}", 0),
			   _ => throw new InvalidOperationException($"Could not compile expression operator {expression.NodeType}")
		   };

	static Tuple<string, int> EmitUnaryExpression(Expression expression)
		=> expression.NodeType switch
		   {
			   ExpressionType.Not => /*					*/ Tuple.Create("!{0}", 0),
			   ExpressionType.Negate => /*				*/ Tuple.Create("-{0}", 2),
			   ExpressionType.UnaryPlus => /*			*/ Tuple.Create("+{0}", 2),
			   _ => throw new InvalidOperationException($"Could not compile expression operator {expression.NodeType}")
		   };
}