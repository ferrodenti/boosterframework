using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Booster.Orm.Interfaces;
using Booster.Reflection;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Orm.Kitchen;

public abstract class BaseConnectionSettings : IConnectionSettings
{
	public abstract TypeEx OrmType { get; }

	string? _connectionString;
	public string ConnectionString 
	{
		get
		{
			EnsureParsed();

			if (_connectionString == null)
			{
				if (_notChanged && HiDict.Count == 0)
					_connectionString = InitialString;
				else
				{
					var str = new StringBuilder();
					foreach (var kv in LowDict)
						str.AppendFormat("{0}={1};", kv.Key, kv.Value);

					_connectionString = str.ToString();
				}
			}
			return _connectionString;
		}
		set
		{
			if (_connectionString != value)
			{
				_connectionString = value;
				NeedPasing = true;
			}
		}
	}

	DataReadersProtection? _dataReadersProtection;
	public DataReadersProtection ProtectDataReaders
	{
		get
		{
			_dataReadersProtection ??= Read<DataReadersProtection>("ProtectDataReaders");

			return _dataReadersProtection.Value;
		}
		set
		{
			_dataReadersProtection = value;
			Write("ProtectDataReaders", value);
		}
	}

	TraceSqlOptions? _traceSql;
	public TraceSqlOptions TraceSql 
	{
		get
		{
			_traceSql ??= Read<TraceSqlOptions>("TraceSql");

			return _traceSql.Value;
		}
		set
		{
			_traceSql = value;
			Write("TraceSql", value);
		} 
	}

	public bool StaticConnection 
	{
		get => Read<bool>("StaticConnection");
		set => Write("StaticConnection", value);
	}

	public bool Sharing 
	{
		get => Read("Sharing", true);
		set => Write("Sharing", value);
	}
		
	public bool DebugWrappers
	{
		get => Read("DebugWrappers", false);
		set => Write("DebugWrappers", value);
	}
		
	public DbUpdateOptions DbUpdateOptions 
	{
		get => Read("DbUpdateOptions", DbUpdateOptions.Default).Reset(DbUpdateDeny);
		set => Write("DbUpdateOptions", value);
	}

	public DbUpdateOptions DbUpdateDeny
	{
		get => Read("DbUpdateDeny", DbUpdateOptions.Inherit);
		set => Write("DbUpdateDeny", value);
	}
		
	[PublicAPI]
	public virtual int CommandTimeout
	{
		get => Read<int>("Command Timeout");
		set => Write("Command Timeout", value);
	}

	public bool AllowDebugAdapters 
	{
		get 
		{ 
			var str = Read<string>("AllowDebugAdapters");
			if (str.EqualsIgnoreCase("false"))
				return false;
				
			return str.EqualsIgnoreCase("true") || Debugger.IsAttached;
		}
		set => Write("AllowDebugAdapters", value);
	}
		
	public string AssembliesPath
	{
		get => Read("AssembliesPath", "orm");
		set => Write("AssembliesPath", value);
	}
		
	ConnectionPoolSettings? _connectionPool;
	public ConnectionPoolSettings? ConnectionPool 
	{
		get => _connectionPool ??= ConnectionPoolSettings.FromString(Read<string>("ConnectionPool"));
		set
		{
			if (value != _connectionPool)
			{
				_connectionPool = value ?? new ConnectionPoolSettings {Enabled = false};
				Write("ConnectionPool", _connectionPool.ToString());
			}
		} 
	}

	protected virtual int DefaultPort => 0;

	protected Dictionary<string, string?> LowDict = new();
	protected readonly Dictionary<string, string?> HiDict;

	protected bool CaseSensitive => false;

	public string InitialString { get; }
	bool _notChanged = true;

	protected BaseConnectionSettings(string connectionString)
	{
		HiDict = new Dictionary<string, string?>(CaseSensitive ? StringComparer.Ordinal : StringComparer.OrdinalIgnoreCase);
		InitialString = _toString = connectionString;
		ProtectDataReaders = DataReadersProtection.All;
	}
		
	protected bool NeedPasing = true;
	bool _req;

	public bool ContainsKey(string key)
	{
		EnsureParsed();

		return LowDict.ContainsKey(key) || HiDict.ContainsKey(key);
	}

	[MethodImpl(MethodImplOptions.Synchronized)]
	public virtual void EnsureParsed()
	{
		if (!NeedPasing || _req)
			return;

		try
		{
			_req = true;

			LowDict = new Dictionary<string, string?>(CaseSensitive ? StringComparer.Ordinal : StringComparer.OrdinalIgnoreCase);

			foreach (var s in InitialString.SplitNonEmpty(';'))
			{
				var kv = s.TrimStart();
				var i = kv.IndexOf('=');
				if (i < 0)
					LowDict[kv] = null;
				else
				{
					var key = kv.Substring(0, i).TrimEnd();

					if (key.IsSome())
						Write(key, kv.Substring(i + 1), false);
				}
			}

			_connectionPool = null;
			_dataReadersProtection = null;
			_traceSql = null;
			_connectionString = null;

			NeedPasing = false;
		}
		finally
		{
			_req = false;
		}
	}

	static HashSet<string>? _hiLevelProperties;
		
	protected /*virtual*/ IEnumerable<string> GetHiLevelSettings() => new[]
	{
		nameof(ProtectDataReaders),
		nameof(TraceSql),
		nameof(StaticConnection),
		nameof(ConnectionPool),
		nameof(AllowDebugAdapters),
		nameof(DbUpdateOptions),
		nameof(DbUpdateDeny),
		nameof(Sharing),
		nameof(DebugWrappers),
		nameof(AssembliesPath)
	};

	protected virtual bool IstHi(string key, string? value, ref Action? post)
	{
		_hiLevelProperties ??= new HashSet<string>(GetHiLevelSettings().Select(n => n.ToLower()));

		return _hiLevelProperties.Contains(key);
	}

	protected virtual T Read<T>(string key, T def)
	{
		EnsureParsed();
			
		var hiKey = key.ToLower().Replace(" ", "");
			
		if (HiDict.TryGetValue(hiKey, out var s) || LowDict.TryGetValue(key, out s))
			return s == null
				? def
				: StringConverter.Default.ToObject<T>(s) ?? def;
			
		return def;
	}

	protected virtual T? Read<T>(string key)
	{
		EnsureParsed();
			
		var hiKey = key.ToLower().Replace(" ", "");
			
		if (HiDict.TryGetValue(hiKey, out var s) || LowDict.TryGetValue(key, out s))
			return s == null
				? default
				: StringConverter.Default.ToObject<T>(s);
			
		return default;
	}

	protected bool InPost;

	//[MethodImpl(MethodImplOptions.Synchronized)]
	protected virtual void Write<T>(string key, T value, bool resetConnStr = true)
	{
		lock (this)
		{
			var hiKey = key.ToLower().Replace(" ", "");
			var v = StringConverter.Default.ToString(value);

			Action? post = null;
			bool changed;

			if (IstHi(hiKey, v, ref post))
			{
				changed = HiDict.SafeGet(hiKey) != v;
				HiDict[hiKey] = v;
			}
			else
			{
				changed = LowDict.SafeGet(key) != v;
				LowDict[key] = v;

				if (changed && resetConnStr)
				{
					_notChanged = false;
					_toString = null;
					_connectionString = null;
				}
			}

			if (!InPost && changed && post != null)
				try
				{
					InPost = true;
					post();
				}
				finally
				{
					InPost = false;
				}
		}
	}

	protected bool TryParseEndPoint(string? str, out string server, out int port)
	{
		server = default!;
		port = 0;

		if (str.IsEmpty())
			return false;

		var i = str.IndexOf(":", StringComparison.Ordinal);
		if (i < 0)
		{
			server = str;
			port = DefaultPort;
			return true;
		}

		if (!int.TryParse(str.Substring(i + 1), out port))
			throw new ArgumentException($"Expected 'host:port', got '{str}'");

		server = str.Substring(0, i);
		return true;
	}

	string? _toString;
	public override string ToString()
	{
		if (_toString == null)
		{
			var str = new EnumBuilder(";");
			foreach (var kv in LowDict.Concat(HiDict))
				str.Append($"{kv.Key}={kv.Value};");

			_toString = str;
		}
		return _toString;
	}
}