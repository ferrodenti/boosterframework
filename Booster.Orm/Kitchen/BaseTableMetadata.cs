using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using Booster.Log;
using Booster.Orm.Interfaces;

namespace Booster.Orm.Kitchen;

public abstract class BaseTableMetadata : ITableMetadata //TODO: StringDictionary, BaseMetadata, IMetadata
{
	protected static readonly StringEscaper Escaper = new("\\", "=", ";");
	static readonly StringConverter _converter = new();

	protected readonly BaseOrm Orm;
	protected readonly string TableName;
	protected readonly ILog Log;
	protected readonly bool IsView;

	readonly ConcurrentDictionary<string, string> _data = new(StringComparer.OrdinalIgnoreCase);

	protected BaseTableMetadata(BaseOrm orm, string tableName, bool isView, ILog log)
	{
		Orm = orm;
		TableName = tableName;
		IsView = isView;
		Log = log.Create(this);

		try
		{
			string value;
			using (var conn = Orm.GetCurrentConnection())
				// ReSharper disable once VirtualMemberCallInContructor
				value = Read(conn); //-V3068

			if (value.IsSome())
				foreach (var str in Escaper.Split(value, ";", unescape: true))
				{
					var pair = Escaper.Split(str, "=", unescape: true).ToArray();
					if (pair.Length == 2 && pair[0].IsSome())
						_data[pair[0]] = pair[1];
				}
		}
		catch (Exception ex)
		{
			Log.Warn(ex, $"reading {TableName} metadata: {ex.Message}");
		}
	}

	protected void Save()
	{
		var str = new StringBuilder();

		foreach (var pair in _data)
		{
			str.Append(';');
			str.Append(Escaper.Escape(pair.Key));
			str.Append('=');
			str.Append(Escaper.Escape(pair.Value));
		}

		if (str.Length > 0)
			str.Append(';');

		try
		{
			using var conn = Orm.GetCurrentConnection();
			Write(conn, str.ToString());
		}
		catch(Exception ex)
		{
			Log.Warn($"writing {TableName} metadata \"{str}\": {ex.Message}");
		}
	}

	protected abstract void Write(IConnection conn, string value);
	protected abstract string Read(IConnection conn);

	public int Version
	{
		get => Get<int>("version");
		set => Set("version", value);
	}

	public TValue Get<TValue>(string key)
		=> _converter.ToObject<TValue>(this[key]);

	public void Set(string key, object value)
		=> this[key] = _converter.ToString(value);

	public string this[string key]
	{
		get => _data.SafeGet(key);
		set { _data[key] = value; Save(); }
	}
}