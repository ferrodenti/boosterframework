using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Booster.Debug;
using Booster.CodeGen;
using Booster.Localization.Pluralization;
using Booster.Log;
using Booster.Orm.Attributes;
using Booster.Orm.Interfaces;
using Booster.Reflection;

#nullable enable

namespace Booster.Orm.Kitchen;

public class BasicRepoAdaptersFactory : IRepoAdaptersFactory
{
	const string _namespace = "Booster.Orm.DynamicCode.RepoAdapters";

	readonly BaseOrm _orm;
	protected readonly ILog? Log;
	readonly OrmAssemblies _assemblies;

	public BasicRepoAdaptersFactory(BaseOrm orm, OrmAssemblies assemblyCache, ILog log)
	{
		_orm = orm;
		_assemblies = assemblyCache;
		Log = log.Create(this);
	}

	string? GetAssyAttribute()
		=> _assemblies.CachingEnabled
			? $"[assembly:{nameof(OrmAdaptersAssemblyAttribute).Replace("Attribute", "")}({_orm.InstanceContext.With(c => $"\"{c}\"", "null")}, {_assemblies.GetBoosterAssembliesHashes(_orm).ToString(h => h).Or("0")})]"
			: null;

	public Task Compile(IEnumerable<IRepoInitTask> tasks)
	{
		var module = new ModuleBuilder
		{
			Namespace = _namespace,
			CustomBeforeClasses = GetAssyAttribute()
		};

		var toComplie = new List<IRepoInitTask>();
		foreach (var task in tasks)
			try
			{
				module.AddClass(task.AdapterBuilder);
				toComplie.Add(task);
			}
			catch (Exception ex)
			{
				SafeBreak.Break();
				Log.Error(ex, $"{task.AdapterBuilder.ClassName} generation error: {ex.Message}");
				task.Adapter = new InvalidRepoAdapter(ex);
			}

		return CompileAdapters(toComplie, module);
	}

	async Task CompileAdapters(IEnumerable<IRepoInitTask> tasks, ModuleBuilder? module)
	{
		var rest = tasks.ToList();

		await _assemblies.FindAdapters(_orm, rest).NoCtx();
			
		if (rest.Count == 0)
			return;
			
		if (module == null)
		{
			module = new ModuleBuilder
			{
				Namespace = _namespace,
				CustomBeforeClasses = GetAssyAttribute()
			};
				
			foreach (var t in rest)
				module.AddClass(t.AdapterBuilder);
		}

		OrmAdaptersAssembly? cache = null;
		// ReSharper disable once TooWideLocalVariableScope
		try
		{
			cache = await _assemblies.GetNewAdaptersAssembly(_orm).NoCtx();
			Log.Trace($"Compiling {Plural.En(rest.Count, "repo adapter", "repo adapters")}{cache.Path.With(p=>$" ({Path.GetFileName(p)})")}...");
			var sw = new Stopwatch();
			sw.Start();
				
			var bld = new CsBuilder();
			bld.TypeRef(typeof(OrmAdaptersAssemblyAttribute));
#if NETSTANDARD2_1 
			//bld.TypeRef(typeof(System.Text.RegularExpressions.Match));
#endif
			var code = module.GetSourceCode(bld);
				
			var comp = new Compiler(Log)
			{
				AllowDebugDynamicSource = _orm.ConnectionSettings.AllowDebugAdapters
			};
			cache.Assembly = await comp.CompileAssembly(code, code.Assemblies, cache.Path).NoCtx();
			cache.Used = true;

			foreach (var task in rest)
			{
				var type = cache.Assembly.GetType($"{module.Namespace}.{task.AdapterBuilder.ClassName}", false);

				if (type != null)
					task.Adapter = (IRepoAdapter) Activator.CreateInstance(type, _orm, task.Repository);
				else
				{
					var err = $"Class {task.AdapterBuilder.ClassName} was not found in a compiled module";
					Log.Error(err);
					task.Adapter = new InvalidRepoAdapter(new Exception(err));
				}
			}
			sw.Stop();
			Log.Debug($"done ({sw.ElapsedMilliseconds} ms)\r\n");
		}
		catch (Exception ex) // Eсли есть ошибки компиляции
		{
			_assemblies?.RemoveAssembly(cache);
				
			SafeBreak.Break();

			switch (rest.Count)
			{
			case > 1: // Пытаемся скомпилировать половинки кода по отдельности
				var half = rest.Count / 2;
				await CompileAdapters(rest.Take(half), null).NoCtx();
				await CompileAdapters(rest.Skip(half), null).NoCtx();
				break;

			case 1: // А тот класс, который в конце не скомпиллируется вернет ошибку компилляции при первом обращении к его методу
				Log.Error(ex, $"{rest[0].AdapterBuilder.ClassName} compilation error: {ex.Message}");
				rest.First().Adapter = new InvalidRepoAdapter(ex);
				break;
			}
		}
	}
}