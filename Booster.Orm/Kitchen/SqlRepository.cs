using System;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Booster.AsyncLinq;
using Booster.DI;
using Booster.Orm.Interfaces;

#if NETSTANDARD2_1
using System.Runtime.CompilerServices;
#endif

#nullable enable

namespace Booster.Orm.Kitchen;

public class SqlRepository : BaseRepository
{
	IRepoAdapter? _adapter;
	protected IRepoAdapter Adapter => _adapter ??= Orm.GetRepoAdapter(EntityType) ?? throw new Exception($"RepoAdapter not found, entityType={EntityType}");

	public IRepoQueries RepoQueries { get; }

	public SqlRepository(BaseOrm orm, ITableMapping mapping, IRepoQueries repoQueries)
		: base(orm, mapping)
		=> RepoQueries = repoQueries;

	string PrepareQuery(string query)
	{
		if (query.IsEmpty())
			return RepoQueries.Select;

		var i = query.TrimStart().ToLower().IndexOf("order by");
		if (i < 0)
			query = string.Join(" ", RepoQueries.Select, "where", query, RepoQueries.DefaultSort);
		else if (i == 0)
			query = string.Join(" ", RepoQueries.Select, query);
		else
			query = string.Join(" ", RepoQueries.Select, "where", query);
		return query;
	}

	QueryBuilder CreateSelectByPksQuery(IEnumerable pks)
	{
		if (TableMapping.IdentityColumns.Count != 1)
			throw new NotSupportedException("Multiple primary keys are not supported");

		var result = new QueryBuilder(this);
		var pk = TableMapping.IdentityColumns[0];
		var paramName = RepoQueries.PkParamPrefix + pk.ColumnName;

		if (Orm.ArrayParametersSupported)
		{
			var arr = pks.AsArray(pk.MemberValueType);
			if (arr.Length == 0)
				result.Nonsense = true;

			result.AppendAnd($"{pk.MemberName:?}=any({Orm.CreateParameter(paramName, arr):@})");
		}
		else
		{
			var i = 0;
			foreach (var obj in pks)
			{
				if (i++ == 0)
					result.AppendAnd($"{pk.MemberName:?} in (");
				else
					result.Append($",");

				result.Append($"{obj:@}");
			}

			if (i > 0)
				result.Append($")");
			else
				result.Nonsense = true;
		}

		return result;
	}

	public void Envelope(EventHandler<RepositoryChangedEventArgs>? handler, Action body)
	{
		if (handler == null)
			body();
		else
		{
			using var conn = Orm.GetConnection()!;
			using var trans = conn.BeginTransaction();
			body();
			trans.Commit();
		}
	}
	public override void Insert(object entity)
	{
		Adapter.Insert(entity); //TODO: Use envelope
		OnInserted(entity);
	}

	public override void InsertWithPks(object entity)
	{
		Adapter.InsertWithPks(entity);
		OnInserted(entity);
	}

	public override void Update(object entity)
	{
		Adapter.Update(entity);
		OnUpdated(entity);
	}

	public override void UpdateWithPks(object entity, object[] pks)
	{
		Adapter.UpdateWithPks(entity, pks);
		OnUpdated(entity);
	}

	public override bool Delete(object entity)
	{
		var result = Adapter.Delete(entity);
		OnDeleted(entity);
		return result > 0;
	}

	public override TValue ReadUnmapped<TValue>(object entity, string fieldName)
		=> Adapter.ReadUnmapped<TValue>(entity, fieldName);

	public override void WriteUnmapped<TValue>(object entity, string fieldName, TValue value)
		=> Adapter.WriteUnmapped(entity, fieldName, value);

	public override IEnumerable Select()
		=> Adapter.Select();

	public override object Load(string query, DbParameter[] parameters)
		=> Adapter.Load(PrepareQuery(query), parameters);

	public override object LoadByPks(object[] ids)
		=> Adapter.LoadByPks(ids);

	public override IEnumerable SelectByPks(IEnumerable pks)
	{
		var query = CreateSelectByPksQuery(pks);
		return query.Nonsense
			? Array.CreateInstance(EntityType, 0)
			: Select(query.ToString(), query.Parameters);
	}

	public override IEnumerable Query(string query, DbParameter[] parameters)
		=> Adapter.Query(query, parameters);

	public override IEnumerable Select(string query, DbParameter[] parameters)
		=> query.IsEmpty() ? Select() : Adapter.Query(PrepareQuery(query), parameters);

	protected override T Count<T>(string query, DbParameter[] parameters)
	{
		using var conn = InstanceFactory.Get<IConnection>(InstanceContext);
		return string.IsNullOrWhiteSpace(query)
			? conn.ExecuteScalar<T>(RepoQueries.SelectCount, null)!
			: conn.ExecuteScalar<T>($"{RepoQueries.SelectCount} where {query}", parameters)!;
	}

	public override IEnumerable Select(string query, string sort, int pageSize, DbParameter[] parameters)
	{
		query = RepoQueries.CreatePagingQuery(0, true, pageSize, query, sort, ref parameters!);
		var last = parameters.Length - 1;

		var count = 0;
		do
		{
			var i = 0;

			foreach (var obj in Adapter.Query(query, parameters))
			{
				yield return obj;
				i++;
			}

			if (i < pageSize)
				yield break;

			count += i;

			for (i = 0; i < last; i++)
				parameters[i] = Orm.CreateParameter(parameters[i].ParameterName, parameters[i].Value);

			parameters[last] = Orm.CreateParameter("paging_skip", count);
		} while (true);
	}

	protected override IEnumerable Select(object skip, bool bSkip, int take, string condition, string sort, DbParameter[] parameters) // TODO: refact
	{
		var query = RepoQueries.CreatePagingQuery(skip, bSkip, take, condition, sort, ref parameters!);
		return Adapter.Query(query, parameters);
	}

	public override async Task InsertAsync(object entity, CancellationToken token = default)
	{
		await Adapter.InsertAsync(entity, token).NoCtx();
		OnInserted(entity);
	}

	public override async Task InsertWithPksAsync(object entity, CancellationToken token = default)
	{
		await Adapter.InsertWithPksAsync(entity, token).NoCtx();
		OnInserted(entity);
	}

	public override async Task UpdateAsync(object entity, CancellationToken token = default)
	{
		await Adapter.UpdateAsync(entity, token).NoCtx();
		OnUpdated(entity);
	}

	public override async Task UpdateWithPksAsync(object entity, object[] pks, CancellationToken token = default)
	{
		await Adapter.UpdateWithPksAsync(entity, pks, token).NoCtx();
		OnUpdated(entity);
	}

	public override async Task<bool> DeleteAsync(object entity, CancellationToken token = default)
	{
		var result = await Adapter.DeleteAsync(entity, token).NoCtx();
		OnDeleted(entity);
		return result > 0;
	}

	public override Task<TValue> ReadUnmappedAsync<TValue>(object entity, string fieldName, CancellationToken token = default)
		=> Adapter.ReadUnmappedAsync<TValue>(entity, fieldName, token);

	public override Task WriteUnmappedAsync<TValue>(object entity, string fieldName, TValue value, CancellationToken token = default)
		=> Adapter.WriteUnmappedAsync(entity, fieldName, value, token);

#if NETSTANDARD2_1
	public override IAsyncEnumerable<object> SelectAsync(string query, DbParameter[]? parameters, CancellationToken token = default)
		=> query.IsEmpty() ? SelectAsync(token) : Adapter.QueryAsync(PrepareQuery(query), parameters, token);

	public override async IAsyncEnumerable<object> SelectAsync(string query, string sort, int pageSize, DbParameter[]? parameters = null, [EnumeratorCancellation] CancellationToken token = default)
	{
		parameters ??= Array.Empty<DbParameter>();
		query = RepoQueries.CreatePagingQuery(0, true, pageSize, query, sort, ref parameters!);
		
		var last = parameters.Length - 1;
		var skip = 0;
		var p = parameters;

		while (true)
		{
			var result = await Adapter.QueryAsync(query, p, token).ToList(token).NoCtx();
			if (result.Count < pageSize)
				break;

			skip += pageSize;

			p = new DbParameter[parameters.Length];
			for (var i = 0; i < last; i++)
				p[i] = Orm.CreateParameter(parameters[i].ParameterName, parameters[i].Value);

			p[last] = Orm.CreateParameter("paging_skip", skip);

			foreach (var item in result)
			{
				token.ThrowIfCancellationRequested();
				yield return item;
			}
		}
	}
#else
		public override IAsyncEnumerable<object> SelectAsync(string query, DbParameter[] parameters, CancellationToken token = default)
			=> query.IsEmpty() ? SelectAsync() : Adapter.QueryAsync(PrepareQuery(query), parameters, token);
		
		public override IAsyncEnumerable<object> SelectAsync(string query, string sort, int pageSize, DbParameter[] parameters = null, CancellationToken token = default)
		{
			query = RepoQueries.CreatePagingQuery(0, true, pageSize, query, sort, ref parameters);
			var last = parameters.Length - 1;
			var skip = 0;
			var p = parameters;

			return new AsyncYield<List<object>>(async (_, cancel) =>
			{
				var result = await Adapter.QueryAsync(query, p, token).ToList(token).NoCtx();
				if (result.Count < pageSize)
					cancel.Value = true;
				else
				{
					skip += pageSize;

					p = new DbParameter[parameters.Length];
					for (var i = 0; i < last; i++)
						p[i] = Orm.CreateParameter(parameters[i].ParameterName, parameters[i].Value);

					p[last] = Orm.CreateParameter("paging_skip", skip);
				}

				return result;
			}).SelectMany(l => l);
		}
#endif

	public override IAsyncEnumerable<object> QueryAsync(string query, DbParameter[] parameters, CancellationToken token = default)
		=> Adapter.QueryAsync(query, parameters, token);

	public override IAsyncEnumerable<object> SelectAsync(CancellationToken token = default)
		=> Adapter.SelectAsync(token);

	protected override IAsyncEnumerable<object> SelectAsync(
		object skip, 
		bool bSkip, 
		int take, 
		string condition, 
		string? sort, 
		DbParameter[] parameters, 
		CancellationToken token = default)
	{
		var query = RepoQueries.CreatePagingQuery(skip, bSkip, take, condition, sort, ref parameters!);
		return Adapter.QueryAsync(query, parameters, token);
	}

	protected override async Task<T> CountAsync<T>(string query, DbParameter[] parameters, CancellationToken token = default)
	{
		using var conn = InstanceFactory.Get<IConnection>(InstanceContext);
		
		var result = string.IsNullOrWhiteSpace(query)
			? await conn.ExecuteScalarAsync<T>(RepoQueries.SelectCount, parameters, token).NoCtx()
			: await conn.ExecuteScalarAsync<T>($"{RepoQueries.SelectCount} where {query}", parameters, token).NoCtx();

		return result!;
	}

	public override Task<object?> LoadAsync(string query, DbParameter[] parameters, CancellationToken token = default)
		=> Adapter.LoadAsync(PrepareQuery(query), parameters, token);

	public override Task<object?> LoadByPksAsync(object[] ids, CancellationToken token = default)
		=> Adapter.LoadByPksAsync(ids, token);

	public override IAsyncEnumerable<object> SelectByPksAsync(IEnumerable pks, CancellationToken token = default)
	{
		var query = CreateSelectByPksQuery(pks);
		return query.Nonsense
			? AsyncEnumerable.Empty<object>()
			: SelectAsync(query.ToString(), query.Parameters, token);
	}
}