using System;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using Booster.Log;
using Booster.Orm.Interfaces;
using Booster.Orm.Schemas;

namespace Booster.Orm.Kitchen;

/// <summary>
/// Эта фабрика для создания объектов нужных орму. Не путать с OrmFactory, который для создания ормов. TODO: вообще неплохо было бы переименовать
/// </summary>
public abstract class BaseOrmFactory : IOrmFactory
{
	protected readonly BaseOrm Owner;
	protected readonly ILog Log;
		
	public virtual bool CuncurrencyDebugWrappers => false;

	protected BaseOrmFactory(BaseOrm owner, ILog log)
	{
		Owner = owner;
		Log = log;
	}

	public Pool<DbConnection> CreateConnectionPool(IConnectionSettings settings)
	{
		var poolSettings = settings.ConnectionPool;

		return new Pool<DbConnection>(() => CreateDbConnectionAsync(settings, CancellationToken.None), Log)
		{
			MinCapacity = poolSettings.Min,
			MaxCapacity = poolSettings.Max,
			AutoGrowStep = poolSettings.Inc,
			MaxItemsReleasedPerGCIter = poolSettings.Dec
		};
	}

	public virtual IConnection CreateConnection(IConnectionSettings settings, Action releaser) 
		=> new BaseConnection(settings, releaser, Owner, Log);

	public virtual OrmAssemblies CreateAssemblyCache(string path) 
		=> new(path, Log);
		
	public virtual IRepoAdaptersFactory CreateRepoAdapterFactory(OrmAssemblies assemblyCache) 
		=> new BasicRepoAdaptersFactory(Owner, assemblyCache, Log);

	public virtual IRepoAdapterBuilder CreateAdapterBuilder(ITableMapping mapping, IRepoQueries repoQueries, IDbTypeDescriptor typeDescriptor) 
		=> new BasicRepoAdapterBuilder(Owner, mapping, typeDescriptor, repoQueries, Log);

	public virtual IModelSchemaReader CreateModelSchemaReader() 
		=> new BasicModelSchemaReader(Owner, Log);

	public virtual IRepository CreateRepo(ITableMapping mapping, IRepoQueries repoQueries)
	{
		if (mapping.PkType != null)
		{
			if (mapping.IsDictionary)
				return (IRepository) Activator.CreateInstance(typeof (SqlDictionary<,>).MakeGenericType(mapping.PkType, mapping.ModelType), Owner, mapping, repoQueries);

			return (IRepository) Activator.CreateInstance(typeof (SqlRepository<>).MakeGenericType(mapping.PkType), Owner, mapping, repoQueries);
		}

		return new SqlRepository(Owner, mapping, repoQueries);
	}

	public virtual ISqlFormatter CreateSqlFormatter() 
		=> new BasicSqlFormatter(Owner);
		
	public virtual ITableMetadata CreateRelationMetadata(string tableName, bool isView) 
		=> new EmptyTableMetadata();

	public virtual ITableSchema CreateTableSchema(string schema, string tableName, ITableMetadata metadata) 
		=> new TableSchema(schema, tableName, metadata);

	public virtual IRepoQueries CreateRepoQueries(ITableMapping tableMapping) 
		=> new BasicSqlRepoQueries(Owner, tableMapping);

	public virtual IRepoInitTask CreateRepoInitTask(EntityInfo entityType) 
		=> new RepoInitTask(entityType, Log);

	public abstract IQueryCompiler CreateQueryCompiler();
	public abstract IDbTypeDescriptor CreateDbTypeDescriptor();
	public abstract IDbManager CreateDbManager();
	public abstract DbParameter CreateParameter();
	public abstract Task<DbConnection> CreateDbConnectionAsync(IConnectionSettings settings, CancellationToken token);
	public abstract DbConnection CreateDbConnection(IConnectionSettings settings);
}