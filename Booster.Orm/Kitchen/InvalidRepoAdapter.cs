using System;
using System.Collections;
using System.Data.Common;
using System.Runtime.ExceptionServices;
using System.Threading;
using System.Threading.Tasks;
using Booster.Orm.Interfaces;

#nullable enable

namespace Booster.Orm.Kitchen;

public class InvalidRepoAdapter : IRepoAdapter
{
	readonly ExceptionDispatchInfo _ex;

	public InvalidRepoAdapter(Exception? ex) 
		=> _ex = ExceptionDispatchInfo.Capture(ex ?? new Exception("Unknown error"));

	public void InitEntity(object model) { _ex.Throw(); throw new Exception(); }
	public void Insert(object model) { _ex.Throw(); throw new Exception(); }
	public int Update(object model) { _ex.Throw(); throw new Exception(); }
	public int Delete(object model) { _ex.Throw(); throw new Exception(); }
	public int InsertWithPks(object model) { _ex.Throw(); throw new Exception(); }
	public int UpdateWithPks(object model, object[] pks) { _ex.Throw(); throw new Exception(); }
	public IEnumerable Select()  { _ex.Throw(); throw new Exception(); }
	public IEnumerable Query(string query, DbParameter[] parameters) { _ex.Throw(); throw new Exception(); }
	public object Load(string query, DbParameter[] parameters) { _ex.Throw(); throw new Exception(); }
	public object LoadByPks(object[] pks) { _ex.Throw(); throw new Exception(); }

	public TValue ReadUnmapped<TValue>(object entity, string fieldName) { _ex.Throw(); throw new Exception(); }
	public int WriteUnmapped<TValue>(object entity, string fieldName, TValue value) { _ex.Throw(); throw new Exception(); }

	public Task InsertAsync(object model, CancellationToken token) { _ex.Throw(); throw new Exception(); }
	public Task<int> UpdateAsync(object model, CancellationToken token) { _ex.Throw(); throw new Exception(); }
	public Task<int> DeleteAsync(object model, CancellationToken token) { _ex.Throw(); throw new Exception(); }
	public Task<int> InsertWithPksAsync(object model, CancellationToken token) { _ex.Throw(); throw new Exception(); }
	public Task<int> UpdateWithPksAsync(object model, object[] pks, CancellationToken token) { _ex.Throw(); throw new Exception(); }

#if NETSTANDARD2_1
		public System.Collections.Generic.IAsyncEnumerable<object> SelectAsync(CancellationToken token) { _ex.Throw(); throw new Exception(); }
		public System.Collections.Generic.IAsyncEnumerable<object> QueryAsync(string query, DbParameter[] parameters, CancellationToken token) { _ex.Throw(); throw new Exception(); }
#else
	public AsyncLinq.IAsyncEnumerable<object> SelectAsync(CancellationToken token) { _ex.Throw(); throw new Exception(); }
	public AsyncLinq.IAsyncEnumerable<object> QueryAsync(string query, DbParameter[] parameters, CancellationToken token) { _ex.Throw(); throw new Exception(); }
#endif

	public Task<object> LoadAsync(string query, DbParameter[] parameters, CancellationToken token) { _ex.Throw(); throw new Exception(); }
	public Task<object> LoadByPksAsync(object[] pks, CancellationToken token) { _ex.Throw(); throw new Exception(); }
	public Task<TValue> ReadUnmappedAsync<TValue>(object entity, string fieldName, CancellationToken token) { _ex.Throw(); throw new Exception(); }
	public Task<int> WriteUnmappedAsync<TValue>(object entity, string fieldName, TValue value, CancellationToken token) { _ex.Throw(); throw new Exception(); }
}