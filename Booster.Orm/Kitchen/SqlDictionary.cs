using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Booster.AsyncLinq;
using Booster.Collections;
using Booster.Orm.Interfaces;

namespace Booster.Orm.Kitchen;

public class SqlDictionary<TId, TEntity> : SqlRepository<TId>, ISqlDictionary<TId, TEntity> where TEntity : class, IEntity<TId>
{
	readonly CacheDictionary<TId, TEntity> _inner;

	public CacheSettings CacheSettings => TableMapping.DictionaryCacheSettings;

	public SqlDictionary(BaseOrm orm, ITableMapping mapping, IRepoQueries queries) : base(orm, mapping, queries) 
		=> _inner = new CacheDictionary<TId, TEntity>(CacheSettings);

	public bool TryGetByPk(TId id, out TEntity entity) 
		=> _inner.TryGetValue(id, out entity);

	public void Set(TEntity entity) 
		=> _inner[entity.Id] = entity;

	public override IEntity<TId> LoadById(TId id)
	{
		if (!TryGetByPk(id, out var entity))
		{
			entity = (TEntity) base.LoadById(id);
			if (entity != null)
				Set(entity);
		}
		return entity;
	}

	public override bool Delete(object entity)
	{
		var result = base.Delete(entity);
		_inner.Remove(((TEntity)entity).Id);
		return result;
	}

	public override void Insert(object entity)
	{
		base.Insert(entity);
		Set((TEntity)entity);
	}

	public override void InsertWithPks(object entity)
	{
		base.InsertWithPks(entity);
		Set((TEntity)entity);
	}

	public override void Update(object entity)
	{
		base.Update(entity);
		Set((TEntity)entity);
	}

	public override void UpdateWithPks(object entity, object[] pks)
	{
		_inner.Remove((TId) pks.First());
		base.UpdateWithPks(entity, pks);
		Set((TEntity)entity);
	}

	public override async Task<IEntity<TId>> LoadByIdAsync(TId id, CancellationToken token = default)
	{
		if (!TryGetByPk(id, out var entity))
		{
			var res = (TEntity) await  base.LoadByIdAsync(id, token).NoCtx();
			if (res != null)
				Set(res);

			return res;
		}
		return entity;
	}

	public override async Task<bool> DeleteAsync(object entity, CancellationToken token = default)
	{
		var result = await base.DeleteAsync(entity, token).NoCtx();
		_inner.Remove(((TEntity)entity).Id);
		return result;
	}

	public override async Task InsertAsync(object entity, CancellationToken token = default)
	{
		await base.InsertAsync(entity, token).NoCtx();
		Set((TEntity)entity);
	}

	public override async Task InsertWithPksAsync(object entity, CancellationToken token = default)
	{
		await base.InsertWithPksAsync(entity, token).NoCtx();
		Set((TEntity)entity);
	}

	public override async Task UpdateAsync(object entity, CancellationToken token = default)
	{
		await base.UpdateAsync(entity, token).NoCtx();
		Set((TEntity)entity);
	}

	public override async Task UpdateWithPksAsync(object entity, object[] pks, CancellationToken token = default)
	{
		_inner.Remove((TId) pks.First());
		await base.UpdateWithPksAsync(entity, pks, token).NoCtx();
		Set((TEntity)entity);
	}

	public void Reset() 
		=> _inner.Clear();

	public override IEnumerable SelectByPks(IEnumerable pks)
	{
		var ids = pks.Cast<TId>().AsList();
		var fromCache = new List<TEntity>();

		foreach (var id in ids.ToArray())
			if (_inner.TryGetValue(id, out var item))
			{
				fromCache.Add(item);
				ids.Remove(id);
			}

		return ids.Count == 0 
			? fromCache 
			: fromCache.Concat(base.SelectByPks(ids.ToArray()).Cast<TEntity>());
	}

	public override IAsyncEnumerable<object> SelectByPksAsync(IEnumerable pks, CancellationToken token = default)
	{
		var ids = pks.Cast<TId>().AsList();
		var fromCache = new List<TEntity>();

		foreach (var id in ids.ToArray())
			if (_inner.TryGetValue(id, out var item))
			{
				fromCache.Add(item);
				ids.Remove(id);
			}

		return ids.Count == 0 
			? fromCache.AsAsyncEnumerable() 
			: fromCache.AsAsyncEnumerable().Concat(token, base.SelectByPksAsync(ids.ToArray(), token));
	}
}