using System;
using System.Collections.Generic;
using System.Linq;
using Booster.Log;
using Booster.Orm.Interfaces;
using Booster.Orm.Schemas;
using Booster.Reflection;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Orm.Kitchen;

[PublicAPI]
public class BasicModelSchemaReader : IModelSchemaReader
{
	readonly BaseOrm _orm;
	protected readonly ILog? Log;

	public BasicModelSchemaReader(BaseOrm orm, ILog? log)
	{
		_orm = orm;
		Log = log.Create(this);
	}

	protected virtual IModelDataMember CreateDataMember(TypeEx model, BaseDataMember memberInfo)
	{
		try
		{
			var name = memberInfo.Name;
			var dataType = memberInfo.ValueType;
			var columnName = _orm.SqlFormatter.CreateColumnName(memberInfo.IsPublic
				? name
				: name.TrimStart('_'));

			var valueType = _orm.DbTypeDescriptor.GetValueType(dataType);
			var dbType = _orm.DbTypeDescriptor.GetDbTypeFor(dataType) ?? throw new Exception($"Type {dataType} is not supported");
			var nullable = dataType.IsNullable || _orm.DbTypeDescriptor.IsNullable(valueType);

			return new ModelDataMember(memberInfo, name, dataType, valueType, columnName, dbType, nullable);
		}
		catch (Exception e)
		{
			Log.Error(e.InnerException, $"Model={model}, Member={memberInfo}");
			throw;
		}
	}

	protected virtual IModelDataMember CreateUnmapedDataMember(TypeEx model, DbUnmappedAttribute attr, string name)
	{
		try
		{
			name = name.Trim();

			var columnName = name;
			var dataType = attr.Type.With(t => (TypeEx) t);
			var dbType = attr.DbType ?? dataType.With(t => _orm.DbTypeDescriptor.GetDbTypeFor(t));
			var nullable = attr.IsNullable;

			dataType ??= dbType.With(t => _orm.DbTypeDescriptor.GetType(t));
			var valueType = dataType.With(t => _orm.DbTypeDescriptor.GetValueType(t));

			nullable ??= dataType?.IsNullable == true || valueType.With(t => _orm.DbTypeDescriptor.IsNullable(t));


			//var res = new ModelDataMember
			//{
			//	DataType = dataType,
			//	Name = name,
			//	DbColumnName = name
			//}; 

			if (dbType == null)
				throw new Exception("Type should be specified");
				
			//TODO: ��������� ����� ��� UnmappedMember
			return new ModelDataMember(null!, name, dataType!, valueType!, columnName, dbType, nullable ?? false);
		}
		catch (Exception e)
		{
			Log.Error(e.InnerException, $"Model={model}, name={name}");
			throw;
		}
	}

	public virtual IModelSchemaWithMigration ReadModelSchema(TypeEx model)
	{
		var modelSchema = new ModelSchema
		{
			ModelType = model,
			TableName = _orm.SqlFormatter.CreateTableName(model.Name),
			DbUpdateOptions = _orm.ConnectionSettings.DbUpdateOptions
		};

		if (model.TryFindAttribute<DbAdaptedImplementationAttribute>(out var aiAttr)) 
			modelSchema.AdapterImplementation = aiAttr.Implementation;

		var cattr = model.FindAttribute<DbInstanceContextAttribute>();
		if (cattr != null)
			modelSchema.InstanceContext = cattr.ContextName;
			
		var indexes = new List<IDbIndex>();
		var constraints = new List<IDbConstraint>();
		var sequences = new List<IDbSequence>();

		TypeEx? commonValueConverter = null;
	
		foreach (var attr in model.FindAttributes<DbAttribute>())
		{
			if (!string.IsNullOrWhiteSpace(attr.Mapping))
				modelSchema.TableName = attr.Mapping;

			if (attr.UpdateOptions != DbUpdateOptions.Inherit)
				modelSchema.DbUpdateOptions = attr.UpdateOptions;
				
			if (attr.DefaultSort != null)
				modelSchema.DefaultSort = attr.DefaultSort;

			if (attr.ValueConverter != null)
				commonValueConverter = attr.ValueConverter;
				
			switch (attr)
			{
			case DbViewAttribute:
				modelSchema.IsView = true;
				break;
			case DbDictAttribute dict:
				modelSchema.IsDictionary = true;
				modelSchema.DictionaryCacheSettings = dict.CacheSettings;
				break;
			case DbOldNameAttribute oldName:
				modelSchema.OldNames = oldName.OldNames;
				break;
			}
		}

		var unmappedMembers = new HashSet<IModelDataMember>(new LambdaComparer<IModelDataMember>(m => m.DbColumnName));

		foreach (var attr in model.FindAttributes<DbUnmappedAttribute>())
			if (attr.Mapping.IsSome())
				foreach (var map in attr.Mapping.SplitNonEmpty(','))
					Try.IgnoreErrors(() =>
					{
						if (map.IsEmpty())
							return;

						var mdm = CreateUnmapedDataMember(model, attr, map.Trim());
						
						if (mdm.DbColumnDataType.IsSome())
							mdm.DbUpdateOptions = attr.DbUpdateOptions != DbUpdateOptions.Inherit
								? attr.DbUpdateOptions
								: modelSchema.DbUpdateOptions;
						else
						{
							mdm.DbUpdateOptions = DbUpdateOptions.Deny;
							mdm.Skip = true;
						}

						mdm.ValueConverter = commonValueConverter;

						unmappedMembers.Add(mdm);
					});

		IModelDataMember? sortColumn = null;
		string? sortDirection = null;

		foreach (var member in model.FindDataMembers(
					 new ReflectionFilter
					 {
						 RequiredAttributes = { typeof(DbAttribute) },
						 Order = ReflectionFilterOrder.DelaringAncestorsToDescendants
					 }))
			try
			{
				var mdm = CreateDataMember(model, member);

				mdm.DbUpdateOptions = modelSchema.DbUpdateOptions;
				mdm.Skip = member.FindAttribute<DbSkipAttribute>()?.Evaluate(model) == true;

				var isRowId = false;

				foreach (var attr in member.FindAttributes<DbAttribute>())
				{
					if (!string.IsNullOrWhiteSpace(attr.Mapping))
						mdm.DbColumnName = attr.Mapping;

					if (attr.UpdateOptions != DbUpdateOptions.Inherit)
						mdm.DbUpdateOptions = attr.UpdateOptions;

					if (attr.Autogenerated)
						mdm.IsAutoGenerated = true;

					if (attr.DefaultSort != null && (modelSchema.DefaultSort == null || modelSchema.DefaultSort.EqualsIgnoreCase("id asc")))
					{
						sortColumn = mdm;
						sortDirection = attr.DefaultSort;
					}

					if (attr.ValueConverter != null)
						mdm.ValueConverter = attr.ValueConverter;

					switch (attr)
					{
					case DbPkAttribute pk:
						mdm.IsPk = true;
						mdm.IsAutoGenerated = pk.Autogenerated;

						if (pk.IsRowId)
						{
							modelSchema.RowIdAsPk = true;
							isRowId = true;
						}
						else
							constraints.Add(new DbConstraint(modelSchema, ConstraintType.PrimaryKey, mdm));

						break;
					case DbConstraintAttribute idx:
						constraints.Add(new DbConstraint(modelSchema, idx, mdm));
						break;
					case DbIndexAttribute idx:
						indexes.Add(new DbIndex(modelSchema, idx, mdm));
						break;

					case DbAutoIncAttribute ai:
						if (!_orm.DbTypeDescriptor.IsInteger(mdm.DbColumnDataType))
						{
							if (!member.HasAttribute<DbPkAttribute>())
								Log.Warn($"Could not setup a sequence autogeneration for {modelSchema.ModelType.Name}.{mdm.Name} because it has non integer type {mdm.ValueType} ({mdm.DbColumnDataType})");

							break;
						}

						if (ai.Type != SequenceType.Memory)
							mdm.IsAutoGenerated = true;

						mdm.Sequence = new DbSequence(mdm, ai);

						if (ai.Type == SequenceType.DbSequence)
							sequences.Add(mdm.Sequence);

						break;
					case DbOldNameAttribute oldName:
						mdm.OldNames = oldName.OldNames;
						break;
					}
				}

				foreach (var attr in member.FindAttributes<DbTypeAttribute>())
				{
					TypeEx? valType;
					if (attr.Value.IsSome())
					{
						mdm.DbColumnDataType = attr.Value;
						valType = _orm.DbTypeDescriptor.GetType(mdm.DbColumnDataType);
					}
					else
					{
						valType = attr.Type;
						var dbType = _orm.DbTypeDescriptor.GetDbTypeFor(valType);
						if (dbType != null)
							mdm.DbColumnDataType = dbType;
						else
							Log.Error($"Model={model}, Member={member}: {valType} is not supported by the database");
					}

					if (valType != null)
					{
						mdm.DbNullable = _orm.DbTypeDescriptor.IsNullable(valType);
						mdm.SaveDefaultValuesAsDbNull = true;
					}
				}

				foreach (var attr in member.FindAttributes<DbNullAttribute>())
				{
					mdm.DbNullable = attr.Value;
					mdm.SaveDefaultValuesAsDbNull = attr.SaveDefaultValuesAsDbNull;
				}

				if (!isRowId && mdm.ValueConverter == null)
					mdm.ValueConverter = commonValueConverter;

				modelSchema.AddColumn(mdm);
				unmappedMembers.Remove(mdm);
			}
			catch (Exception e)
			{
				Log.Error(e.InnerException, $"Model={model}, MemberInfo={member}");
			}

		if (sortColumn != null)
			switch (sortDirection?.ToLower())
			{
			case "asc":
			case "desc":
				modelSchema.DefaultSort = $"{sortColumn.DbColumnName} {sortDirection}";
				break;
			case "":
				modelSchema.DefaultSort = $"{sortColumn.DbColumnName} asc";
				break;
			default:
				Log.Error($"{modelSchema.ModelType.Name}.{sortColumn.Name} default sort \"{sortDirection}\": expected  \"asc\" or \"desc\" values");
				break;
			}

		indexes.AddRange(model.FindAttributes<DbIndexAttribute>()
			.Select(attr => new DbIndex(modelSchema, attr, null)));

		constraints.AddRange(model.FindAttributes<DbConstraintAttribute>()
			.Select(attr => new DbConstraint(modelSchema, attr, null)));

		foreach (var mem in unmappedMembers)
			modelSchema.AddColumn(mem);


		foreach (var method in model.FindMethods(new ReflectionFilter().RequireAttribute<MigrationAttribute>()))
		{
			if (!method.IsStatic)
			{
				Log.Error($"Attention! {model.Name}.{method.Name} a method marked with [Migration] attribute is not static and would not be invoked");
				continue;
			}

			foreach (MigrationAttribute attr in method.FindAttributes(typeof(MigrationAttribute)))
			{
				var ready = attr.Options.HasFlag(MigrationOptions.Ready);
				var stage = attr.Options & MigrationOptions.AnyStage;

				if (stage == MigrationOptions.None)
					stage = MigrationOptions.Ready;

				modelSchema.MigrationMethods.GetOrAdd((stage, ready), _ => new List<(MigrationAttribute, Method)>())
					.Add((attr, method));
			}
		}

		modelSchema.Indexes = indexes.ToArray();
		modelSchema.Constraints = constraints.ToArray();
		modelSchema.Sequences = sequences.ToArray();
			 
		return modelSchema;
	}
}