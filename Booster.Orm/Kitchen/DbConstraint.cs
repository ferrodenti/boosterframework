using System;
using System.Linq;
using Booster.Orm.Interfaces;

namespace Booster.Orm.Kitchen;

public class DbConstraint : IDbConstraint
{
	public string TableName { get; set; }
	public IModelDataMember[] Columns { get; set; }
	public string Custom { get; }
	public string Name { get; set; }
	public ConstraintType Type { get; set; }

	public DbConstraint(string tableName, string name, ConstraintType type)
	{
		TableName = tableName;
		Name = name;
		Type = type;
	}

	public DbConstraint(IModelSchema model, ConstraintType type, IModelDataMember column)
	{
		TableName = model.TableName;
		Type = type;
		Columns = new[] { column };
	}

	public DbConstraint(IModelSchema model, DbConstraintAttribute attr, IModelDataMember column)
	{
		TableName = model.TableName;
		Custom = attr.ConstraintCustom;
		Name = attr.ConstraintName;

		var columns = attr.ConstraintColumns != null ? model.Where(m => attr.ConstraintColumns.Any(c => c.EqualsIgnoreCase(m.Name))) : Enumerable.Empty<IModelDataMember>();
		if (column != null)
			columns = columns.AppendBefore(column);

		Columns = columns.Distinct().ToArray();
	}

	public override string ToString()
	{
		var bld = new EnumBuilder(", ", " ");
		if(Custom.IsSome())
			bld.Append($"Custom=\"{Custom}\"");
			
		return $"Constraint \"{Name}\": {TableName}({Columns?.ToString(new EnumBuilder(", "))}) Type={Type} {bld}";
	}

	// ReSharper disable NonReadonlyMemberInGetHashCode
	public override int GetHashCode()
		=> new HashCode(TableName, Type);
	// ReSharper restore NonReadonlyMemberInGetHashCode

	public override bool Equals(object obj)
	{
		if (obj is not DbConstraint b)
			return false;

		try
		{
			return TableName == b.TableName &&
			       Type == b.Type &&
			       ((Columns != null && b.Columns != null && Columns.HasSameElements(b.Columns)) || Name.EqualsIgnoreCase(b.Name));
		}
		catch (Exception e)
		{
			Utils.Nop(e);
		}
		return false;
	}
}