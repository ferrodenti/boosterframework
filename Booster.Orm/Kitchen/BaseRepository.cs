using System;
using System.Collections;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using Booster.DI;
using Booster.Orm.Interfaces;
using Booster.Reflection;
using JetBrains.Annotations;
#if !NETSTANDARD2_1
using Booster.AsyncLinq;
#else
using System.Collections.Generic;
#endif

#nullable enable

namespace Booster.Orm.Kitchen;

[PublicAPI]
public abstract class BaseRepository : IRepository
{
	protected readonly BaseOrm Orm;
	IOrm IRepository.Orm => Orm;

	public TypeEx EntityType { get; }
	public string TableName { get; }
	public string InstanceContext { get; }
	public ITableMapping TableMapping { get; }

	public int Version
	{
		get => TableMapping.Metadata.With(md => md.Version);
		set => TableMapping.Metadata.Do(md => md.Version = value);
	}

	public event EventHandler<RepositoryChangedEventArgs>? Inserted;
	public event EventHandler<RepositoryChangedEventArgs>? Updated;
	public event EventHandler<RepositoryChangedEventArgs>? Deleted;

	public event EventHandler<RepositoryChangedEventArgs>? Saved
	{
		add
		{
			Inserted += value;
			Updated += value;
		}
		remove
		{
			Inserted -= value;
			Updated -= value;
		}
	}

	public event EventHandler<RepositoryChangedEventArgs>? Changed
	{
		add
		{
			Inserted += value;
			Updated += value;
			Deleted += value;
		}
		remove
		{
			Inserted -= value;
			Updated -= value;
			Deleted -= value;
		}
	}

	protected BaseRepository(BaseOrm orm, ITableMapping mapping)
	{
		Orm = orm;

		TableMapping = mapping;

		EntityType = mapping.ModelType;
		TableName = mapping.TableName;

		InstanceContext = InstanceFactory.CurrentContext;
	}

	public virtual void OnInserted(object? entity)
		=> Inserted?.Invoke(this, new RepositoryChangedEventArgs(entity, RepositoryAction.Insert));

	public virtual void OnUpdated(object? entity)
		=> Updated?.Invoke(this, new RepositoryChangedEventArgs(entity, RepositoryAction.Update));

	public virtual void OnDeleted(object? entity)
		=> Deleted?.Invoke(this, new RepositoryChangedEventArgs(entity, RepositoryAction.Delete));

	//public abstract void Init(object entity);
	public abstract void Insert(object entity);
	public abstract void InsertWithPks(object entity);
	public abstract void Update(object entity);
	public abstract void UpdateWithPks(object entity, object[] pks);
	public abstract bool Delete(object entity);

	public abstract object? Load(string query, DbParameter[] parameters);

	public abstract object? LoadByPks(object[] ids);
	public abstract IEnumerable SelectByPks(IEnumerable pks);

	public abstract TValue ReadUnmapped<TValue>(object entity, string fieldName);
	public abstract void WriteUnmapped<TValue>(object entity, string fieldName, TValue value);

	public abstract IEnumerable Select(string query, DbParameter[] parameters);
	public abstract IEnumerable Select(string query, string sort, int pageSize, DbParameter[] parameters);

	public abstract IEnumerable Query(string query, DbParameter[] parameters);

	public abstract IEnumerable Select();

	protected abstract IEnumerable Select(object skip, bool bSkip, int take, string condition, string sort, DbParameter[] parameters);
	protected abstract T Count<T>(string query, DbParameter[] parameters);

	public abstract Task InsertAsync(object entity, CancellationToken token = default);
	public abstract Task InsertWithPksAsync(object entity, CancellationToken token = default);
	public abstract Task UpdateAsync(object entity, CancellationToken token = default);
	public abstract Task UpdateWithPksAsync(object entity, object[] pks, CancellationToken token = default);
	public abstract Task<bool> DeleteAsync(object entity, CancellationToken token = default);
	public abstract Task<object?> LoadAsync(string query, DbParameter[] parameters, CancellationToken token = default);
	public abstract Task<object?> LoadByPksAsync(object[] ids, CancellationToken token = default);
	public abstract IAsyncEnumerable<object> SelectByPksAsync(IEnumerable pks, CancellationToken token = default);

	public abstract Task<TValue> ReadUnmappedAsync<TValue>(object entity, string fieldName, CancellationToken token = default);
	public abstract Task WriteUnmappedAsync<TValue>(object entity, string fieldName, TValue value, CancellationToken token = default);

	public abstract IAsyncEnumerable<object> SelectAsync(string query, DbParameter[]? parameters, CancellationToken token = default);
	public abstract IAsyncEnumerable<object> SelectAsync(string query, string sort, int pageSize, DbParameter[]? parameters = null, CancellationToken token = default);

	public abstract IAsyncEnumerable<object> QueryAsync(string query, DbParameter[] parameters, CancellationToken token = default);
	public abstract IAsyncEnumerable<object> SelectAsync(CancellationToken token = default);

	protected abstract IAsyncEnumerable<object> SelectAsync(
		object skip, 
		bool bSkip, 
		int take, 
		string condition, 
		string? sort, 
		DbParameter[] parameters, 
		CancellationToken token = default);
	
	protected abstract Task<T> CountAsync<T>(
		string query, 
		DbParameter[] parameters, 
		CancellationToken token = default);

	public int Count(string query, DbParameter[] parameters)
		=> Count<int>(query, parameters);

	public int Count(QueryBuilder queryBuilder)
		=> queryBuilder.Nonsense ? 0 : Count(queryBuilder.Query, queryBuilder.Parameters);

	public long LCount(string query, DbParameter[] parameters)
		=> Count<long>(query, parameters);

	public int Count(string query, object parameters)
		=> Count<int>(query, Orm.CreateParameters(parameters));

	public long LCount(string query, object parameters)
		=> Count<long>(query, Orm.CreateParameters(parameters));

	public long LCount(QueryBuilder queryBuilder)
		=> queryBuilder.Nonsense ? 0 : LCount(queryBuilder.Query, queryBuilder.Parameters);

	public object? Load(string query, object parameters)
		=> Load(query, Orm.CreateParameters(parameters));

	public object? Load(QueryBuilder queryBuilder)
		=> queryBuilder.Nonsense ? null : Load(queryBuilder.Query, queryBuilder.Parameters);

	public IEnumerable Select(string query, object parameters)
		=> Select(query, Orm.CreateParameters(parameters));

	public IEnumerable Select(QueryBuilder queryBuilder)
		=> Select(queryBuilder.Query, queryBuilder.Parameters);

	public IEnumerable Select(string query, string sort, int pageSize, object parameters)
		=> Select(query, sort, pageSize, Orm.CreateParameters(parameters));

	public IEnumerable Query(string query, object parameters)
		=> Query(query, Orm.CreateParameters(parameters));

	public IEnumerable Query(QueryBuilder queryBuilder)
		=> Query(queryBuilder.Query, queryBuilder.Parameters);

	public IEnumerable Select(int skip, int take, string condition, string sort, DbParameter[] parameters)
		=> Select(skip, skip > 0, take, condition, sort, parameters);

	public IEnumerable Select(long skip, int take, string condition, string sort, DbParameter[] parameters)
		=> Select(skip, skip > 0, take, condition, sort, parameters);

	public IEnumerable Select(int skip, int take, string condition, string sort, object parameters)
		=> Select(skip, skip > 0, take, condition, sort, Orm.CreateParameters(parameters));

	public IEnumerable Select(int skip, int take, QueryBuilder queryBuilder, string sort)
		=> Select(skip, take, queryBuilder.Query, sort, queryBuilder.Parameters);

	public IEnumerable Select(long skip, int take, string condition, string sort, object parameters)
		=> Select(skip, skip > 0, take, condition, sort, Orm.CreateParameters(parameters));

	public IEnumerable Select(long skip, int take, QueryBuilder queryBuilder, string sort)
		=> Select(skip, take, queryBuilder.Query, sort, queryBuilder.Parameters);

	public Task<int> CountAsync(string query, DbParameter[] parameters, CancellationToken token = default)
		=> CountAsync<int>(query, parameters, token);

	public Task<long> LCountAsync(string query, DbParameter[] parameters, CancellationToken token = default)
		=> CountAsync<long>(query, parameters, token);

	public Task<int> CountAsync(string query, object parameters, CancellationToken token = default)
		=> CountAsync<int>(query, Orm.CreateParameters(parameters), token);

	public Task<int> CountAsync(QueryBuilder queryBuilder, CancellationToken token = default)
		=> queryBuilder.Nonsense ? Task.FromResult(0) : CountAsync(queryBuilder.Query, queryBuilder.Parameters, token);

	public Task<long> LCountAsync(string query, object parameters, CancellationToken token = default)
		=> CountAsync<long>(query, Orm.CreateParameters(parameters), token);

	public Task<long> LCountAsync(QueryBuilder queryBuilder, CancellationToken token = default)
		=> queryBuilder.Nonsense ? Task.FromResult(0L) : LCountAsync(queryBuilder.Query, queryBuilder.Parameters, token);

	public Task<object?> LoadAsync(string query, object parameters, CancellationToken token = default)
		=> LoadAsync(query, Orm.CreateParameters(parameters), token);

	public Task<object?> LoadAsync(QueryBuilder queryBuilder, CancellationToken token = default)
		=> queryBuilder.Nonsense ? Task.FromResult<object?>(null) : LoadAsync(queryBuilder.Query, queryBuilder.Parameters, token);

	public IAsyncEnumerable<object> SelectAsync(string query, object parameters, CancellationToken token = default)
		=> SelectAsync(query, Orm.CreateParameters(parameters), token);

	public IAsyncEnumerable<object> SelectAsync(QueryBuilder queryBuilder, CancellationToken token = default)
		=> SelectAsync(queryBuilder.Query, queryBuilder.Parameters, token);

	public IAsyncEnumerable<object> SelectAsync(string query, string sort, int pageSize, object parameters, CancellationToken token = default)
		=> SelectAsync(query, sort, pageSize, Orm.CreateParameters(parameters), token);

	public IAsyncEnumerable<object> QueryAsync(string query, object parameters, CancellationToken token = default)
		=> QueryAsync(query, Orm.CreateParameters(parameters), token);

	public IAsyncEnumerable<object> QueryAsync(QueryBuilder queryBuilder, CancellationToken token = default)
		=> QueryAsync(queryBuilder.Query, queryBuilder.Parameters, token);

	public IAsyncEnumerable<object> SelectAsync(int skip, int take, string condition, string? sort, DbParameter[] parameters, CancellationToken token = default)
		=> SelectAsync(skip, skip > 0, take, condition, sort, parameters, token);

	public IAsyncEnumerable<object> SelectAsync(long skip, int take, string condition, string? sort, DbParameter[] parameters, CancellationToken token = default)
		=> SelectAsync(skip, skip > 0, take, condition, sort, parameters, token);

	public IAsyncEnumerable<object> SelectAsync(int skip, int take, string condition, string? sort, object parameters, CancellationToken token = default)
		=> SelectAsync(skip, skip > 0, take, condition, sort, Orm.CreateParameters(parameters), token);

	public IAsyncEnumerable<object> SelectAsync(int skip, int take, QueryBuilder queryBuilder, CancellationToken token = default)
		=> SelectAsync(skip, take, queryBuilder.Condition, queryBuilder.Order, queryBuilder.Parameters, token);

	public IAsyncEnumerable<object> SelectAsync(long skip, int take, string condition, string? sort, object parameters, CancellationToken token = default)
		=> SelectAsync(skip, skip > 0, take, condition, sort, Orm.CreateParameters(parameters), token);

	public IAsyncEnumerable<object> SelectAsync(long skip, int take, QueryBuilder queryBuilder, CancellationToken token = default)
		=> SelectAsync(skip, take, queryBuilder.Condition, queryBuilder.Order, queryBuilder.Parameters, token);

	public override string ToString()
		=> TableMapping.ToString();
}