﻿using System;
using System.Data;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using Booster.Helpers;
using Booster.Reflection;

namespace Booster.Orm.Kitchen;

static class TupleReadHelper<T>
{
	static readonly TupleHelper<T> _tupleHelper;
		
	static TupleReadHelper()
		=> _tupleHelper = TupleHelper<T>.Instance;


	public static T ReadTuple(IDataReader reader)
	{
		var args = _tupleHelper.Arguments;
		var values = new object[args.Length];
		for (var i = 0; i < args.Length; i++)
			if (reader.IsDBNull(i))
				values[i] = args[i].DefaultValue;
			else
			{
				var val = reader.GetValue(i);

				if (args[i] == typeof(Guid) && val?.GetType() == typeof(byte[]))
					values[i] = new Guid((byte[]) val);
				else if(!TypeEx.Of(val).TryCast(val, args[i], out values[i]))
					values[i] = Convert.ChangeType(val, args[i]);
			}

		return _tupleHelper.Create(values);
	}
		
	public static async Task<T> ReadTupleAsync(DbDataReader reader, CancellationToken token = default)
	{
		var args = _tupleHelper.Arguments;
		var values = new object[args.Length];
		for (var i = 0; i < args.Length; i++)
			if (await reader.IsDBNullAsync(i, token))
				values[i] = args[i].DefaultValue;
			else
			{
				var val = reader.GetValue(i);

				if (args[i] == typeof(Guid) && val?.GetType() == typeof(byte[]))
					values[i] = new Guid((byte[]) val);
				else if(!TypeEx.Of(val).TryCast(val, args[i], out values[i]))
					values[i] = Convert.ChangeType(val, args[i]);
			}

		return _tupleHelper.Create(values);
	}
}