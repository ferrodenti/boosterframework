using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

namespace Booster.Orm.Kitchen;

public class MemoryDataReader : DbDataReader
{
	int _fieldCount;
	int _pos;
	List<object> _rows = new();
	bool _closed;
	object _names;
		
	protected object[] CurrentRow
	{
		get
		{
			if (_rows[_pos] is Exception ex)
				throw ex;

			return (object[])_rows[_pos];
		}
	}

	public void ReadAll(DbDataReader reader)
	{
		ReadHead(reader);
			
		_rows.Add(ReadRow(reader));

		while (reader.Read())
			_rows.Add(ReadRow(reader));
	}

	public async Task ReadAllAsync(DbDataReader reader, CancellationToken token)
	{
		ReadHead(reader);
			
		_rows.Add(ReadRow(reader));

		while (await reader.ReadAsync(token).NoCtx())
			_rows.Add(ReadRow(reader));
	}

	void ReadHead(DbDataReader reader)
	{
		_fieldCount = reader.FieldCount;

		try
		{
			var names = new List<string>(reader.FieldCount);

			for (var i = 0; i < reader.FieldCount; ++i)
				names.Add(reader.GetName(i));

			_names = names;
		}
		catch (Exception ex)
		{
			_names = ex;
		}
	}

	object ReadRow(IDataReader reader)
	{
		try
		{
			var res = new object[_fieldCount];
			reader.GetValues(res);
			return res;
		}
		catch (Exception ex)
		{
			return ex;
		}
	}

#if !DNX
	public override void Close()
	{
		_rows = null;
		_closed = true;
	}

	public override DataTable GetSchemaTable()
		=> throw new NotSupportedException();
#else
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				_rows = null;
				_closed = true;
			}
		}
#endif

	public override bool NextResult() 
		=> _pos < _rows.Count;

	public override bool Read()
	{
		_pos ++;
		return NextResult();
	}

	public override int Depth => 1;

	public override bool IsClosed => _closed;

	public override int RecordsAffected => 0;

	public override bool GetBoolean(int ordinal) 
		=> (int) Convert.ChangeType(CurrentRow[ordinal], typeof (int)) != 0;

	public override byte GetByte(int ordinal) 
		=> (byte) Convert.ChangeType(CurrentRow[ordinal], typeof (byte));

	public override long GetBytes(int ordinal, long dataOffset, byte[] buffer, int bufferOffset, int length)
	{
		var b = (byte[])CurrentRow[ordinal];

		Array.Copy(b, dataOffset, buffer, bufferOffset, length);

		return length;
	}

	public override char GetChar(int ordinal) 
		=> (char) Convert.ChangeType(CurrentRow[ordinal], typeof (char));

	public override long GetChars(int ordinal, long dataOffset, char[] buffer, int bufferOffset, int length)
	{
		if (buffer == null)
			throw new ArgumentNullException(nameof(buffer));
		var b = (char[])CurrentRow[ordinal];


		Array.Copy(b, dataOffset, buffer, bufferOffset, length);

		return length;
	}

	public override Guid GetGuid(int ordinal) 
		=> (Guid) Convert.ChangeType(CurrentRow[ordinal], typeof (Guid));

	public override short GetInt16(int ordinal) 
		=> (short) Convert.ChangeType(CurrentRow[ordinal], typeof (short));

	public override int GetInt32(int ordinal) 
		=> (int) Convert.ChangeType(CurrentRow[ordinal], typeof (int));

	public override long GetInt64(int ordinal) 
		=> (long) Convert.ChangeType(CurrentRow[ordinal], typeof (long));

	public override DateTime GetDateTime(int ordinal) 
		=> (DateTime) Convert.ChangeType(CurrentRow[ordinal], typeof (DateTime));

	public override string GetString(int ordinal) 
		=> (string) Convert.ChangeType(CurrentRow[ordinal], typeof (string));

	public override object GetValue(int ordinal) 
		=> CurrentRow[ordinal];

	public override int GetValues(object[] values)
	{
		var len = Math.Min(CurrentRow.Length, values.Length);
		Array.Copy(CurrentRow, values, len);
		return len;
	}

	public override bool IsDBNull(int ordinal) 
		=> Equals(CurrentRow[ordinal], null) || Equals(CurrentRow[ordinal], Convert.DBNull);

	public override int FieldCount => _fieldCount;

	public override object this[int ordinal] => CurrentRow[ordinal];

	public override object this[string name] => CurrentRow[GetOrdinal(name)];

	public override bool HasRows => _rows.Count > 0;

	public override decimal GetDecimal(int ordinal) 
		=> (decimal) Convert.ChangeType(CurrentRow[ordinal], typeof (decimal));

	public override double GetDouble(int ordinal) 
		=> (double) Convert.ChangeType(CurrentRow[ordinal], typeof (double));

	public override float GetFloat(int ordinal) 
		=> (float) Convert.ChangeType(CurrentRow[ordinal], typeof (float));

	public override string GetName(int ordinal)
	{
		if (_names is Exception ex)
			throw ex;

		return ((List<string>)_names)[ordinal];
	}

	public override int GetOrdinal(string name)
	{
		if (_names is Exception ex)
			throw ex;

		return ((List<string>)_names).IndexOf(name, StringComparer.OrdinalIgnoreCase);
	}

	public override string GetDataTypeName(int ordinal) 
		=> GetFieldType(ordinal)?.Name;

	public override Type GetFieldType(int ordinal) 
		=> CurrentRow[ordinal].GetType();

	public override IEnumerator GetEnumerator() 
		=> CurrentRow.GetEnumerator();
}