using System.Threading.Tasks;
using Booster.Orm.Interfaces;

#nullable enable

namespace Booster.Orm.Kitchen;

public class DbSequence : IDbSequence
{
	public SequenceType Type { get; set; }
	public IModelDataMember? Target { get; set; }
	public string? Name { get; set; }
	public long Start { get; set; }
	public long Step { get; set; }
	public bool ApplyToColumn { get; }
	public bool CheckExistingValues { get; }

	public DbSequence(IModelDataMember modelDataMember, DbAutoIncAttribute attribute)
	{
		Target = modelDataMember;
		Type = attribute.Type;
		Name = attribute.SequenceName;
		Start = attribute.Start;
		Step = attribute.Step;
		ApplyToColumn = attribute.ApplyToColumn;
		CheckExistingValues = attribute.CheckExistingValues;
	}

	public DbSequence(IModelDataMember modelDataMember) //TODO: unused?
		=> Target = modelDataMember;
		
	public DbSequence()
	{
	}
		
	public DbSequence(IDbSequence proto)
	{
		Target = proto.Target;
		Type = proto.Type;
		Name = proto.Name;
		Start = proto.Start;
		Step = proto.Step;
		ApplyToColumn = proto.ApplyToColumn;
		CheckExistingValues = proto.CheckExistingValues;
	}
		
		
	public override string ToString()
	{
		var bld = new EnumBuilder(", ", ", ");
		if (Target != null)
			bld.Append($"Target={Target.Name}");
		if(Start > 0)
			bld.Append($"Start={Start}");			
		if(Step > 0)
			bld.Append($"Step={Step}");
			
		return $"Sequence \"{Name}\": Type={Type}{bld}";
	}

	// ReSharper disable NonReadonlyMemberInGetHashCode
	public override int GetHashCode()
		=> new HashCode(Type);
	// ReSharper restore NonReadonlyMemberInGetHashCode

	public override bool Equals(object obj)
	{
		if (obj is not DbSequence b)
			return false;

		return Type == b.Type &&
			   ((Target != null &&
				 b.Target != null &&
				 Target == b.Target) ||
				Name.EqualsIgnoreCase(b.Name));
	}

	public async Task EnsureExist(IOrm orm)
	{
		var manager = orm.DbManager;
		if (!await manager.SequenceExist(this).NoCtx())
			await manager.CreateSequence(null, this).NoCtx();
	}

	public Task<long> GetNextValue(IOrm orm)
	{
		var query = orm.DbManager.GetSequenceNextValQuery(this);
		using var conn = orm.GetCurrentConnection();
		return conn.ExecuteScalarAsync<long>(query);
	}
}