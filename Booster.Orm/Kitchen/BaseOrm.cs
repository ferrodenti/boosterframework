using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Common;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Threading;
using System.Threading.Tasks;
using Booster.AsyncLinq;
using Booster.DI;
using Booster.Helpers;
using Booster.Interfaces;
using Booster.Log;
using Booster.Orm.Interfaces;
using Booster.Orm.Schemas;
using Booster.Reflection;
using Booster.Synchronization;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Orm.Kitchen;

[InstanceFactory(typeof(IOrm)), PublicAPI]
public abstract class BaseOrm : IOrm, IConnectionProvider
{
	[PublicAPI] public IOrmFactory Factory
	{
		get => _factory.GetValue(this);
		set => _factory.Value = value ?? throw new ArgumentNullException(nameof(value));
	}

	volatile bool _initializing;

	public OrmLogOptions LogOptions { get; set; } = OrmLogOptions.All;
	public IConnectionSettings ConnectionSettings { get; }
	public RepositoryRegistrationOptions RepositoryRegistrationOptions { get; set; } = new();
	public string InstanceContext { get; }
	public bool ConvertEnumParametersToNumbers { get; set; } = true;

	[PublicAPI] public virtual bool ConvertCamelCaseNames2Underscope { get; set; } = true;
	[PublicAPI] public virtual bool ConvertModelNames2PluralTableNames { get; set; } = true;
		
	public ICollection<IRepository> Repositories => ReposByTableName.Values.ToArray();

	protected readonly ConcurrentDictionary<string, IRepository> ReposByTableName;
	protected readonly ConcurrentDictionary<TypeEx, IRepository?> ReposByEntityType = new();
	readonly AsyncLock _reposLock = new();

	InstanceFactoryToken? _connectionInstanceFactoryToken;

	protected IEqualityComparer<string> TableNamesComparer => CaseSensitiveNames 
		? EqualityComparer<string>.Default 
		: StringComparer.OrdinalIgnoreCase;

	protected ILog? Log { get; }


	internal bool ArrayParametersSupported => true;
	protected virtual bool CaseSensitiveNames => false;
	static readonly ConcurrentDictionary<(TypeEx,string), IConnectionProvider> _connSharing = new();

	IConnectionProvider _connectionProvider;

	static readonly CtxLocal<List<IConnection>> _currentConnections = new();
	static readonly CtxLocal<IConnection> _lastCurrentConnection = new();
	protected IConnection? CurrentConnection
	{
		get
		{
			var last = _lastCurrentConnection.Value;
			if (last?.Orm == this)
				return last;

			var current = _currentConnections.Value;
			if (current != null)
				foreach (var conn in current.ToArray())
					if (conn?.Orm == this)
					{
						_lastCurrentConnection.Value = conn;
						return conn;
					}

			return null;
		}
		set
		{
			var current = _currentConnections.Value;
			if (current != null)
			{
				var cur = CurrentConnection;
				if (cur == value)
					return;

				if (cur != null)
					current.Remove(cur);

				if (value != null)
				{
					current.Add(value);
					_lastCurrentConnection.Value = value;
				}
				else
					_lastCurrentConnection.Value = null;
			}
			else if (value != null)
				current = new List<IConnection> {value};

			_currentConnections.Value = current;
		}
	}

	DbConnection? _staticConnection;
	bool _useStaticConnection;

	#region Lazies

	static ConcurrentDictionary<string, OrmAssemblies> _assemblyCaches 
		= new();

	readonly SyncLazy<OrmAssemblies> _assemblyCache 
		= SyncLazy<OrmAssemblies>.Create<BaseOrm>(self 
			=> _assemblyCaches.GetOrAdd(self.ConnectionSettings.AssembliesPath, 
				_ => self.Factory.CreateAssemblyCache(self.ConnectionSettings.AssembliesPath)));
		
	protected OrmAssemblies AssemblyCache => _assemblyCache.GetValue(this);

	readonly SyncLazy<IRepoAdaptersFactory> _repoAdaptersFactoryLazy = SyncLazy<IRepoAdaptersFactory>.Create<BaseOrm>(self => self.Factory.CreateRepoAdapterFactory(self.AssemblyCache));
	protected IRepoAdaptersFactory RepoAdaptersFactory => _repoAdaptersFactoryLazy.GetValue(this);

	readonly SyncLazy<Pool<DbConnection>> _connectionPoolLazy = SyncLazy<Pool<DbConnection>>.Create<BaseOrm>(self => self.Factory.CreateConnectionPool(self.ConnectionSettings));
	protected Pool<DbConnection>? ConnectionPool => ConnectionSettings.ConnectionPool.Enabled ? _connectionPoolLazy.GetValue(this) : null;

	readonly SyncLazy<IDbTypeDescriptor> _dbTypeDescriptorLazy = SyncLazy<IDbTypeDescriptor>.Create<BaseOrm>(self => self.Factory.CreateDbTypeDescriptor());
	public IDbTypeDescriptor DbTypeDescriptor => _dbTypeDescriptorLazy.GetValue(this);

	readonly SyncLazy<IDbManager> _dbManagerLazy = SyncLazy<IDbManager>.Create<BaseOrm>(self => self.Factory.CreateDbManager());
	public IDbManager DbManager => _dbManagerLazy.GetValue(this);

	readonly SyncLazy<IModelSchemaReader> _modelSchemaReaderLazy = SyncLazy<IModelSchemaReader>.Create<BaseOrm>(self => self.Factory.CreateModelSchemaReader());
	protected IModelSchemaReader ModelSchemaReader => _modelSchemaReaderLazy.GetValue(this);

	readonly SyncLazy<ISqlFormatter> _sqlFormatterLazy = SyncLazy<ISqlFormatter>.Create<BaseOrm>(self => self.Factory.CreateSqlFormatter());
	public ISqlFormatter SqlFormatter => _sqlFormatterLazy.GetValue(this);
		
	readonly SyncLazy<IQueryCompiler> _queryCompiler = SyncLazy<IQueryCompiler>.Create<BaseOrm>(self => self.Factory.CreateQueryCompiler());
	public IQueryCompiler QueryCompiler => _queryCompiler.GetValue(this);

	readonly SyncLazy<IOrmFactory> _factory = SyncLazy<IOrmFactory>.Create<BaseOrm>(self => self.CreateFactory());
		 
	#endregion

	#region Events

	public event EventHandler<DbMigrationEventArgs>? Migration;
	public event EventHandler? Disposed;
	#endregion

	readonly Dictionary<TypeEx, IRepoInitTask> _initTasks = new();

	protected BaseOrm(IConnectionSettings settings, ILog? log)
	{
		_connectionProvider = this;
		ConnectionSettings = settings;
		InstanceContext = InstanceFactory.CurrentContext;

		Log = log.Create(this, new
		{
			InstanceContext,
			Orm = GetType().Name,
		});

		ReposByTableName = new ConcurrentDictionary<string, IRepository>(TableNamesComparer);

		RegisterConnection();
	}

	protected abstract IOrmFactory CreateFactory();

	#region Connection & pool

	void RegisterConnection()
	{
		if (ConnectionSettings.Sharing)
		{
			_connectionProvider = _connSharing.GetOrAdd((TypeEx.Of(this), ConnectionSettings.ToString()), _ => this);

			if (_connectionProvider != this)
			{
				_connectionProvider.Disposed += (_, _) => _connectionProvider = this;
				if (_connectionProvider is BaseOrm otherOrm)
					Log.Debug($"Using shared connections with {otherOrm.GetType().Name}: \"{otherOrm.InstanceContext}\"");
				else
					Log.Debug($"Using shared connections with {_connectionProvider.GetType().Name}: {_connectionProvider}");
			}
		}
		else
			_connectionProvider = this;

		_useStaticConnection = ConnectionSettings.StaticConnection;
		if (_useStaticConnection)
			Log.Debug("Using static connections");

		_connectionInstanceFactoryToken = InstanceFactory.RegisterGetter(GetCurrentConnection, true);

#if CONCURRENCY_DEBUG
		if (ConnectionSettings.DebugWrappers)
			Log.Warn("Using concurrency debug wrapppers for connections, commands and readers!");
#endif
	}


	public IConnection GetCurrentConnection()
	{
		var conn = CurrentConnection;
		if (conn == null)
			CurrentConnection = conn = Factory.CreateConnection(ConnectionSettings, () => CurrentConnection = null);

		conn.Entry();
		return conn;
	}

	void IConnectionProvider.ClearPool()
	{
		if (_connectionPoolLazy.IsValueCreated)
		{
			var old = _connectionPoolLazy.Value;
			_connectionPoolLazy.Reset();

			Utils.StartTaskNoFlow(old.Dispose);
		}
	}
	async Task<DbConnection> IConnectionProvider.ReconnectAsync(DbConnection? oldConnection, CancellationToken token)
	{
		var newConn = await Factory.CreateDbConnectionAsync(ConnectionSettings, token).NoCtx();

		ConnectionPool?.Change(oldConnection, newConn);
		oldConnection?.Dispose();

		return newConn;
	}

	DbConnection IConnectionProvider.Reconnect(DbConnection? oldConnection)
	{
		var newConn = Factory.CreateDbConnection(ConnectionSettings);
		//if (oldConnection != null)
		{
			ConnectionPool?.Change(oldConnection, newConn);
			oldConnection?.Dispose();
		}

		return newConn;
	}

	async Task<DbConnection?> IConnectionProvider.GetConnectionAsync(bool checkIsFull, CancellationToken token)
	{
		if (_useStaticConnection)
			return _staticConnection ??= await Factory.CreateDbConnectionAsync(ConnectionSettings, token).NoCtx();

		if (ConnectionPool != null)
		{
			if (checkIsFull && ConnectionPool.IsFull)
				return null;
				
			return await ConnectionPool.TakeAsync(token).NoCtx();
		}

		return  await Factory.CreateDbConnectionAsync(ConnectionSettings, token).NoCtx();
	}

	DbConnection? IConnectionProvider.GetConnection(bool checkIsFull)
	{
		if (_useStaticConnection)
			return _staticConnection ??= Factory.CreateDbConnection(ConnectionSettings);

		if (ConnectionPool != null)
		{
			if (checkIsFull && ConnectionPool.IsFull)
				return null;
				
			return ConnectionPool.Take();
		}

		return  Factory.CreateDbConnection(ConnectionSettings);
	}

	void IConnectionProvider.ReleaseConnectionAsync(DbConnection conn)
	{
		if (_staticConnection == conn)
			return;

		if (ConnectionPool != null)
			ConnectionPool.Release(conn);
		else
			conn.Dispose();
	}

	[PublicAPI] public void ClearPool() 
		=> _connectionProvider.ClearPool();

	public async Task<DbConnection> ReconnectAsync(DbConnection? oldConnection, CancellationToken token)
		=> await _connectionProvider.ReconnectAsync(oldConnection, token);

	public Task<DbConnection?> GetConnectionAsync(CancellationToken token = default) 
		=> _connectionProvider.GetConnectionAsync(false, token);

	public Task<DbConnection?> TryGetConnectionAsync(CancellationToken token) 
		=> _connectionProvider.GetConnectionAsync(true, token);

	public DbConnection Reconnect(DbConnection? oldConnection) 
		=> _connectionProvider.Reconnect(oldConnection);

	public DbConnection? GetConnection() 
		=> _connectionProvider.GetConnection(false);

	public ITransaction BeginTransaction()
	{
		var conn = GetCurrentConnection();
		return new ConnectionTransaction(conn);
	}

	public DbConnection? TryGetConnection() 
		=> _connectionProvider.GetConnection(true);

	public void ReleaseConnection(DbConnection conn) 
		=> _connectionProvider.ReleaseConnectionAsync(conn);

	public void Ping(bool removePass = true)
	{
		try
		{
			using var conn = GetCurrentConnection();
			conn.Ping();
		}
		catch (Exception e)
		{
			throw new PingFailedException(e, ConnectionSettings.InitialString, ConnectionSettings.ConnectionString, removePass);
		}
	}
		
	public async Task PingAsync(bool removePass = true, CancellationToken token = default)
	{
		try
		{
			using var conn = GetCurrentConnection();
			await conn.PingAsync(token);
		}
		catch (Exception e)
		{
			throw new PingFailedException(e, ConnectionSettings.InitialString, ConnectionSettings.ConnectionString, removePass);
		}
	}



	#endregion

	#region Repositories

	#region Register

	public EntityInfo[] FindEntities(Assembly assembly, string? @namespace = null)
	{
		var result = assembly.GetTypes()
			.Where(t => !t.IsAbstract &&
						!t.IsGenericType &&
						TypeEx.Get(t).HasAttribute<DbAttribute>());

		if (@namespace != null)
			result = result.Where(t => t.Namespace == @namespace);

		return result.Select(t => new EntityInfo(t, this)).ToArray();
	}

	public IRepository RegisterRepo(TypeEx entityType) 
		=> RegisterRepoAsync(entityType).NoCtxResult();

	public async Task<IRepository> RegisterRepoAsync(TypeEx entityType)
	{
		var repos = await RegisterReposAsync(new[] {entityType.Inner}).NoCtx();
		return repos[0];
	}

	public IRepository[] RegisterRepos(IEnumerable<Type> entityTypes) 
		=> RegisterReposAsync(entityTypes.Select(t => new EntityInfo(t, this))).NoCtxResult();
		
	public IRepository[] RegisterRepos(IEnumerable<EntityInfo> entities) 
		=> RegisterReposAsync(entities).NoCtxResult();

	public IRepository[] RegisterRepos(string? @namespace = null) 
		=> RegisterReposAsync(@namespace).NoCtxResult();

	public IRepository[] RegisterRepos(Assembly assembly, string? @namespace = null) 
		=> RegisterReposAsync(assembly, @namespace).NoCtxResult();

		

	public virtual Task<IRepository[]> RegisterReposAsync(string? @namespace = null) 
		=> RegisterReposAsync(AssemblyHelper.BoosterCallingAssembly, @namespace);

	public Task<IRepository[]> RegisterReposAsync(IEnumerable<Type> entityTypes)
		=> RegisterReposAsync(entityTypes.Select(t => new EntityInfo(t, this)));

	public virtual Task<IRepository[]> RegisterReposAsync(Assembly assembly, string? @namespace = null)
		=> RegisterReposAsync(FindEntities(assembly, @namespace));

	public async Task<IRepository[]> RegisterReposAsync(IEnumerable<EntityInfo> entities)
	{
		var init = BeginInitialization();
		try
		{
			var repoInitTasks = entities
				.Where(e => !e.Skip)
				.Select(entity =>
				{
					var task = Factory.CreateRepoInitTask(entity);
					_initTasks[entity.EntityType] = task;
					return task;
				}).ToArray();
				
			var tasks = new List<Task<List<IRepository>>>();
			var len = repoInitTasks.Length;

			var numberOfThreads = RepositoryRegistrationOptions.NumberOfThreads;
			if (numberOfThreads > 1 && len / numberOfThreads < RepositoryRegistrationOptions.MinimumItemsPerThread)
				numberOfThreads = len / RepositoryRegistrationOptions.MinimumItemsPerThread;

			for (var i = 0; i < numberOfThreads; ++i) //TODO: separate logs for each thread
			{
				var start = i;
				tasks.Add(Utils.StartTaskNoFlow(async () =>
				{
					var repositories = new List<IRepository>();
					var toCompile = new List<IRepoInitTask>();

					using (InstanceFactory.PushContext(InstanceContext))
					using (GetCurrentConnection())
						for (var j = start; j < len; j += numberOfThreads)
						{
							var task = repoInitTasks[j];
							var repo = await ReposByEntityType.GetOrAddAsync(_reposLock, task.EntityType,
								_ => CreateRepo(task, task.EntityInfo.AdapterImplementation)).NoCtx();

							if (repo != null)
							{
								repositories.Add(repo);
								toCompile.Add(task);
							}
						}

					await RepoAdaptersFactory.Compile(toCompile).NoCtx();

					return repositories;
				}));
			}

			return (await Task.WhenAll(tasks).NoCtx()).SelectMany(repos => repos).ToArray();
		}
		finally
		{
			await init.DisposeAsync().NoCtx();
		}
	}

	#endregion

	TResult? ThrowOrDefalut<TResult>(bool yesThrow, string message)
	{
		if (yesThrow)
			throw new Exception(message);

		return default;
	}
	
	public IRepository? GetRepo<T>(bool throwIfNotFound)
	{
		var enityType = typeof(T);

		if (TryGetRepo(enityType, out var repo))
			return repo;

		return ThrowOrDefalut<IRepository<T>>(throwIfNotFound, $"Repository for type {enityType.FullName} was not registered");
	}

	public IRepository? GetRepo(TypeEx enityType, bool throwIfNotFound)
	{
		if(TryGetRepo(enityType, out var result))
			return result;

		return ThrowOrDefalut<IRepository>(throwIfNotFound, $"Repository for type {enityType.FullName} was not registered");
	}

	public IRepository? GetRepo(string tableName, bool throwIfNotFound)
	{
		if(TryGetRepo(tableName, out var result))
			return result;

		return ThrowOrDefalut<IRepository>(throwIfNotFound, $"Repository for table {tableName} was not registered");
	}

	public IRepository GetRepo<T>()
		=> GetRepo<T>(true)!;

	public IRepository GetRepo(TypeEx enityType)
		=> GetRepo(enityType, true)!;

	public IRepository GetRepo(string tableName)
		=> GetRepo(tableName, true)!;

	public bool TryGetRepo<TEntity>(out IRepository result)
		=> TryGetRepo(typeof(TEntity), out result);

	public bool TryGetRepo(TypeEx byEnityType, out IRepository result)
		=> ReposByEntityType.TryGetValue(byEnityType, out result!) && result != null!;

	public bool TryGetRepo(string byTableName, out IRepository result)
		=> ReposByTableName.TryGetValue(byTableName, out result);

	protected virtual async Task<IRepository?> CreateRepo(IRepoInitTask repoInitTask, AdapterImplementation? adapterImplementation)
	{
		try
		{
			var entityType = repoInitTask.EntityType;

			using var conn = GetCurrentConnection();
			if (entityType.IsAbstract || entityType.IsInterface)
				throw new Exception($"{entityType} is an abstract type. Expected non abstract entity type");

			if (entityType.FindInterface<IValidateType>() != null)
			{
				var sample = (IValidateType)FormatterServices.GetUninitializedObject(entityType); // Хак чтобы создать экземпляр без вызова конструктора
				sample.ValidateType();
			}

			var modelSchema = ModelSchemaReader.ReadModelSchema(entityType);
			if (modelSchema == null)
				throw new Exception($"Fail reflecting entity type {entityType}");

			if (adapterImplementation.HasValue)
				modelSchema.AdapterImplementation = adapterImplementation.Value;

			var context = new RepoInitContext(this, modelSchema);
			context.Exists = await DbManager.TableExists(context.TableName, modelSchema.IsView).NoCtx();

			repoInitTask.TableInfo = context;

			// Переименовываем таблицу и читаем метаданные
			if (!context.Exists && modelSchema.OldNames?.Length > 0)
				foreach (var oldName in modelSchema.OldNames)
				{
					context.Exists = await DbManager.TableExists(oldName, modelSchema.IsView).NoCtx();
					if (context.Exists)
					{
						context.OldTableName = oldName;
						break;
					}
				}

			context.Metadata = Factory.CreateRelationMetadata(context.OldTableName, modelSchema.IsView);

			await modelSchema.Migration(repoInitTask, MigrationOptions.Initialized, context, conn, Log).NoCtx();

			if (context.TableName != context.OldTableName)
			{
				if (modelSchema.DbUpdateOptions.HasFlag(DbUpdateOptions.RenameTable))
				{
					await modelSchema.Migration(repoInitTask, MigrationOptions.BeforeRenameTable, context, conn, Log).NoCtx();

					if (context.TableName != context.OldTableName &&
						await DbManager.RenameTable(context.OldTableName, context.TableName, modelSchema.IsView))
						context.Metadata = Factory.CreateRelationMetadata(context.TableName, modelSchema.IsView);
					else
						context.TableName = context.OldTableName;
				}
				else
					context.TableName = context.OldTableName;
			}

			//TODO: все старые процедуры обновления каким-то образом записать в ModelSchema и использовать далее только новую семантику вызова миграций
			// Обновление перед чтением таблицы
			await modelSchema.Migration(repoInitTask, MigrationOptions.ReadingSchema, context, conn, Log).NoCtx();

			context.TableSchema = Factory.CreateTableSchema(null, context.TableName, context.Metadata); //TODO: как-то иметь тут бд-схему

			Task<bool> ReadSchema()
				=> Log.TryInfo($"Reading table schema {context.TableName} ({entityType.Name})...",
					async () =>
					{
						await DbManager.ReadTable(context.TableName, context.TableSchema).NoCtx();
						await modelSchema.Migration(repoInitTask, MigrationOptions.SchemaRead, context, conn, Log).NoCtx();
					});

			if (context.Exists)
			{
				if (!await ReadSchema().NoCtx())
					return null;
			}
			else
			{
				if (LogOptions.HasFlag(OrmLogOptions.TableNotFound))
					Log.Warn($"Table {context.TableName} ({entityType.Name}) is missing");

				if (!modelSchema.AllowAutoUpdateDb(DbUpdateOptions.CreateTable))
					throw new Exception("Table creation is disabled"); //TODO: в этом случае репозиторий добавляется как нулевой, а надо добавлять Invalid

				await modelSchema.Migration(repoInitTask, MigrationOptions.BeforeCreateTable, context, conn, Log).NoCtx();

				if (!context.Exists)
					await Log.TryInfo(LogMigration(DbUpdateOptions.CreateTable, context.TableName, entityType.Name), async () =>
					{
						context.TableSchema = await DbManager.CreateTable(modelSchema, Factory.CreateTableSchema(null, context.TableName, context.Metadata)).NoCtx(); //TODO: как-то иметь тут бд-схему
						context.Exists = true;

						await modelSchema.Migration(repoInitTask, MigrationOptions.AfterCreateTable, context, conn, Log).NoCtx();

					}).NoCtx();
				else if (!await ReadSchema().NoCtx())
					return null;
			}

			if (modelSchema.RowIdAsPk && !modelSchema.IsView)
				AddRowIdColumn(context.TableSchema);

			ITableMapping mapping = new TableMapping(modelSchema, context.TableSchema);

			var plan = DbManager.GetUpdatePlan(modelSchema, mapping);
			if (plan != null)
			{
				await modelSchema.Migration(repoInitTask, MigrationOptions.BeforeUpdate, context, conn, Log).NoCtx();

				if (await DbManager.UpdateTable(modelSchema, context.TableSchema, mapping, plan).NoCtx())
					await modelSchema.Migration(repoInitTask, MigrationOptions.AfterUpdate, context, conn, Log).NoCtx();
			}

			await DbManager.CheckAdditionalObjects(modelSchema, mapping).NoCtx();

			await modelSchema.Migration(repoInitTask, MigrationOptions.Ready, context, conn, Log).NoCtx();

			var repoQueries = Factory.CreateRepoQueries(mapping);
			repoInitTask.Repository = Factory.CreateRepo(mapping, repoQueries);
			repoInitTask.AdapterBuilder = Factory.CreateAdapterBuilder(mapping, repoQueries, DbTypeDescriptor);

			ReposByTableName[mapping.TableName] = repoInitTask.Repository;
			return repoInitTask.Repository;
		}
		catch (Exception ex)
		{
			Log.Error(ex, $"fail: {ex.Message}");
			repoInitTask.Adapter = new InvalidRepoAdapter(ex);

			if (RepositoryRegistrationOptions.ThrowExceptions)
				throw;

			return null;
		}
	}

#pragma warning restore 612

	protected virtual void AddRowIdColumn(ITableSchema tableSchema)
	{
	}

	#endregion
		
	#region Parameters

	public virtual DbParameter[] CreateParameters(object? parameters)
	{
		if (parameters == null)
			return Array.Empty<DbParameter>();

		var res = (parameters as IEnumerable<DbParameter>)?.AsArray();
		if (res == null)
		{
			var dict = parameters as IDictionary<string, object?> ?? new AnonTypeHelper(parameters).Properties;
			res = new DbParameter[dict.Count];

			var i = 0;
			foreach (var pair in dict)
				res[i++] = CreateParameter(pair.Key, pair.Value);
		}
		return res;
	}

	public virtual DbParameter CreateParameter(string name, TypeEx type)
	{
		var res = Factory.CreateParameter();
		res.ParameterName = name;
		return res;
	}

	public virtual DbParameter CreateParameter(string name, object? value)
	{
		var res = Factory.CreateParameter();
		res.ParameterName = name;
		res.Value = value != null ? PrepareValueForParam(value) : null;
		return res;
	}

	public virtual DbParameter CreateParameter(string name, object? value, TypeEx type) 
		=> CreateParameter(name, value);

	public virtual object? PrepareValueForParam(object? value)
	{
		while (value != null)
		{
			switch (value)
			{
			case IIntValue iInt:
				return iInt.Value;
			case IStringValue iString:
				return iString.Value;
			case Color color:
				return color.ToArgb();
			}

			TypeEx type = value.GetType();

			var convIfc = type.FindInterface(typeof(IConvertiableFrom<>));
			if (convIfc != null) //TODO: проверить это
			{
				if (type.TryCast(value, convIfc.GenericArguments[0], out var res))
				{
					value = res;
					continue;
				}

				var get = GetConvertible(type, convIfc);
				if (get != null)
				{
					value = get.GetValue(value);
					continue;
				}

				//return Getter.CreateCached(type, "Value").Invoke(value);
			}

			//if(type == typeof(Guid))
			//	return ((Guid) value).ToByteArray();

			if (type.IsEnum && ConvertEnumParametersToNumbers) 
				return EnumHelper.GetIntValue(value);

			return value;
		}
		return null;
	}

	static readonly ConcurrentDictionary<TypeEx, IDataAccessor?> _convertibleAccessors = new();

	protected static IDataAccessor? GetConvertible(TypeEx type, TypeEx convIfc) 
		=> _convertibleAccessors.GetOrAdd(type, _ => convIfc.FindProperty("Value")?.FindImplementation(type));

	#endregion

	public IRepoAdapter? GetRepoAdapter(TypeEx entityType)
	{
		if (_initTasks.TryGetValue(entityType, out var task))
		{
			task.WaitForAdapterReady();
			return task.Adapter;
		}
		return null;
	}
		
	public bool WaitRepoReady(TypeEx entityType, int timeout)
	{
		var end = timeout >= 0 ? DateTime.Now.AddMilliseconds(timeout) : DateTime.MaxValue;
		while (DateTime.Now < end)
		{
			if (_initTasks.TryGetValue(entityType, out var task))
				return task.WaitForEventsHandled(timeout >= 0 ? Math.Min(0, (int)(end - DateTime.Now).TotalMilliseconds) : -1);

			Thread.Sleep(100);
		}
		return false;
	}
		
	public Task WaitRepoReadyAsync(TypeEx entityType)
		=> _initTasks.TryGetValue(entityType, out var task) 
			? task.WaitForEventsHandledAsync() 
			: Task.CompletedTask;

	public virtual void UnregisterInstance()
	{
		if (_connectionInstanceFactoryToken != null)
		{
			_connectionInstanceFactoryToken.Unregister();
			_connectionInstanceFactoryToken = null;
		}
	}
		
	internal virtual string LogMigration(DbUpdateOptions type, string tableName, string? objectName, BaseDataMember? dataMember = null)
	{
		var message = type switch
		{
			DbUpdateOptions.CreateTable => $"Creating table {tableName}...",
			DbUpdateOptions.RenameTable => $"Renaming table {tableName} => {objectName}...",
			DbUpdateOptions.CreateColumn => $"Creating {tableName}.{objectName}...",
			DbUpdateOptions.DropColumn => $"Droping {tableName}.{objectName}...",
			DbUpdateOptions.ChangeColumnType => $"Changing type {tableName}.{objectName}...",
			DbUpdateOptions.RenameColumn => $"Renaming column {tableName}.{objectName} ...",
			DbUpdateOptions.CreateSequence => $"Creating sequence {tableName}.{objectName}...",
			DbUpdateOptions.CreateConstraint => $"Creating constraint {tableName}.{objectName}...",
			DbUpdateOptions.DropConstraint => $"Droping constraint {tableName}.{objectName}...",
			DbUpdateOptions.CreateIndex => $"Creating index {tableName}.{objectName}...",
			DbUpdateOptions.DropIndex => $"Droping index {tableName}.{objectName}...",
			_ => tableName
		};

		var args = new DbMigrationEventArgs(this, message, type, tableName, objectName, dataMember);
		//Log.Info(args.ToString());
		Migration?.Invoke(this, args);

		return args.ToString();
	}

	public IHybridDisposable BeginInitialization()
	{
		if (_initializing)
			return AsyncActionDisposable.Empty;

		_initializing = true;
		return new AsyncActionDisposable(EndInitializationAsync);
	}

	public void EndInitialization()
		=> EndInitializationAsync().NoCtxWait();

	public virtual async Task EndInitializationAsync()
	{
		await AssemblyCache.CleanUp().NoCtx();
		_initializing = false;
	}

	public virtual void Dispose()
	{
		UnregisterInstance();

		var key = (TypeEx.Of(this), ConnectionSettings.ToString());
		if (_connSharing.SafeGet(key) == this)
			_connSharing.Remove(key);
			
		if (_connectionPoolLazy.IsValueCreated)
		{
			_connectionPoolLazy.Value.Dispose();
			_connectionPoolLazy.Reset();
		}

		if (_staticConnection != null)
		{
			_staticConnection.Dispose();
			_staticConnection = null;
		}

		Disposed?.Invoke(this, EventArgs.Empty);
	}
}