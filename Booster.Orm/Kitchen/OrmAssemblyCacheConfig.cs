using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Booster.FileSystem;
using Booster.Log;

namespace Booster.Orm.Kitchen;

public class OrmAssemblyCacheConfig
{
	bool _dirty;

	readonly ILog _log;
	readonly string _filename;
	readonly string[] _satelites;
	readonly HashSet<string> _prevIgnore = new(StringComparer.OrdinalIgnoreCase);
	readonly HashSet<string> _ignore = new(StringComparer.OrdinalIgnoreCase);
	readonly HashSet<string> _toDelete = new(StringComparer.OrdinalIgnoreCase);

	public OrmAssemblyCacheConfig(string filename, string[] satelites, ILog log)
	{
		_satelites = satelites;
		_log = log.Create(this);
		_filename = filename;
	}

	public void AddInvalid(string filename)
	{
		_ignore.Add(Path.GetFileName(filename));
		_dirty = true;
	}

	public async Task<bool> ShouldIgnore(string filename)
	{
		var name = Path.GetFileName(filename);

		if (_ignore.Contains(name))
		{
			_prevIgnore.Remove(name);
			return true;
		}

		if (_toDelete.Contains(name))
		{
			if (await Delete(filename))
			{
				_toDelete.Remove(name);
				_dirty = true;
			}

			return true;
		}

		return false;
	}

	public async Task<bool> Delete(string filename)
	{
		async Task<bool> TryDelete(string fn)
		{
			if (await FileUtils.DeleteFileAsync(fn, TaskRepeatOptions.Fast).NoCtx())
			{
				_log.Trace($"Obsolete {Path.GetFileName(fn)} deleted successfully.");
				return true;
			}
				
			_log.Warn($"Failed to delete obsolete {Path.GetFileName(fn)}.\n A deletion is scheduled for next launch.");
			return false;
		}

		var deleted = true;

		if (File.Exists(filename)) 
			deleted = await TryDelete(filename).NoCtx();

		foreach (var ext in _satelites)
		{
			var fn = Path.ChangeExtension(filename, ext);

			if (File.Exists(fn)) 
				deleted &= await TryDelete(fn).NoCtx();
		}

		if (!deleted) 
			_dirty |= _toDelete.Add(Path.GetFileName(filename));

		return deleted;
	}


	public static OrmAssemblyCacheConfig Load(string filename, string[] satelites, ILog log)
	{
		var result = new OrmAssemblyCacheConfig(filename, satelites, log);

		if (File.Exists(filename))
			try
			{
				using var fs = File.OpenRead(filename);
				using var rd = new BinaryReader(fs);

				for (int i = 0, len = rd.ReadInt32(); i < len; i++)
				{
					var name = rd.ReadString();
					result._prevIgnore.Add(name);
					result._ignore.Add(name);
				}

				for (int i = 0, len = rd.ReadInt32(); i < len; i++)
					result._toDelete.Add(rd.ReadString());
			}
			catch
			{
				FileUtils.DeleteFile(filename, TaskRepeatOptions.Fast);
			}

		return result;
	}

	public async Task Save()
	{
		foreach (var name in _prevIgnore)
		{
			_ignore.Remove(name);
			_dirty = true;
		}

		if(!_dirty)
			return;

		await FileUtils.DeleteFileAsync(_filename, TaskRepeatOptions.Fast).NoCtx();

		if (_ignore.Count > 0 || _toDelete.Count > 0)
		{
#if NETSTANDARD
			await using var fs = File.OpenWrite(_filename);
			await using var wr = new BinaryWriter(fs);
#else
			using var fs = File.OpenWrite(_filename);
			using var wr = new BinaryWriter(fs);
#endif

			wr.Write(_ignore.Count);

			foreach (var name in _ignore)
				wr.Write(name);

			wr.Write(_toDelete.Count);

			foreach (var name in _toDelete)
				wr.Write(name);
		}

		_dirty = false;
	}
}