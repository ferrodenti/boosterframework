using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Drawing;
using System.Runtime.Serialization;
using System.Linq;
using System.Text;
using Booster.CodeGen;
using Booster.Helpers;
using Booster.Interfaces;
using Booster.Log;
using Booster.Orm.Attributes;
using Booster.Orm.Interfaces;
using Booster.Reflection;

#nullable enable

#if !NETSTANDARD2_1
using Booster.AsyncLinq;
#endif

namespace Booster.Orm.Kitchen
{
	public class BasicRepoAdapterBuilder : BaseClassBuilder, IRepoAdapterBuilder
	{
		public class AsyncHelper
		{
			readonly bool _isAsync;

			public AsyncHelper(bool async)
				=> _isAsync = async;

			public string MethodCall(string name, object? parameters)
			{
				if (_isAsync)
				{
					var param = parameters?.ToString();
					return string.Concat(BeginMethodCall(name, param), EndMethodCall(param.IsSome()));
				}
				return $"{name}({parameters})";
			}

			public string BeginMethodCall(string name, string? parameters = null, string? genericParameters = null)
				=> _isAsync 
					? $"await {name}Async{genericParameters}({parameters}" 
					: $"{name}{genericParameters}({parameters}";

			public string EndMethodCall(bool hasParameters = true, bool tokenParamName = false)
				=> _isAsync
					? $"{(hasParameters? ", " : "")}{(tokenParamName ? "token: " : "")}token).ConfigureAwait(false)"
					: ")";

			public string MethodDefinition(string returnType, string name, string parameters, string? genericParameters = null)
			{
				if (_isAsync)
				{
					returnType = returnType == "void" ? "Task" : $"Task<{returnType}>";
					return $"public async {returnType} {name}Async{genericParameters}({parameters.WithSome(p => $"{p}, ")}CancellationToken token)";
				}
				return $"public {returnType} {name}{genericParameters}({parameters})";
			}

			public string EnumerableMethodDefinition(string name, string? parameters)
				=> _isAsync 
#if NETSTANDARD2_1
					? $"public async IAsyncEnumerable<object> {name}Async({parameters.WithSome(p => $"{p}, ")}CancellationToken token)" 
#else
					? /***/ $"public IAsyncEnumerable<object> {name}Async({parameters.WithSome(p => $"{p}, ")}CancellationToken token)" 
#endif
					: $"public IEnumerable {name}({parameters})";

			public static implicit operator bool(AsyncHelper async)
				=> async._isAsync;
		}

		FastLazy<string>? _className;
		public override string ClassName => _className ??= new(() =>
		{
			var str = $"{EntityType.FullName}_RepoAdapter";
			var res = new StringBuilder();

			if (!char.IsLetter(str[0]))
				res.Append('_');

			foreach (var c in str)
				res.Append(!char.IsLetterOrDigit(c) ? '_' : c);

			return res.ToString();
		});

		public ILog? Log { get; }

		public override string Access => "public sealed";
		protected readonly ITableMapping TableMapping;

		protected readonly IDbTypeDescriptor TypeDescriptor;
		protected readonly IRepoQueries RepoQueries;
		protected readonly BaseOrm Orm;
		
		public TypeEx EntityType => TableMapping.ModelType;
		readonly Dictionary<string, string> _customFields = new();

		readonly Dictionary<string, bool> _entityEvents = new();
		protected readonly Dictionary<TypeEx, string> ValueConverterNames = new();
		protected readonly Dictionary<TypeEx, IDbValueConverter> ValueConverterInstances = new();

		protected readonly StringBuilder NonSequentialMembers = new();

		readonly SyncLazy<object?> _entitySample = SyncLazy<object?>.Create<BasicRepoAdapterBuilder>(
			self => Try.OrDefault(
				() => self.EntityType.FindConstructor()?.Invoke()));

		protected object? EntitySample => _entitySample.GetValue(this);
			
		readonly Lazy<string> _usingConnection;
		protected string UsingConnection => _usingConnection.Value;

		readonly bool _cachingAssemblies;

		readonly AdapterImplementation _implementation;

		FastLazy<string>? _entityClassName;
		protected string GetEntityFullClassName(CsBuilder builder)
			=> _entityClassName ??= builder.TypeRef(EntityType); //TODO: тут кривенько

		public BasicRepoAdapterBuilder(BaseOrm orm, ITableMapping tableMapping, IDbTypeDescriptor typeDescriptor, IRepoQueries repoQueries, ILog? log)
		{
			Orm = orm;
			TypeDescriptor = typeDescriptor;
			RepoQueries = repoQueries;
			TableMapping = tableMapping;
			Log = log.Create(this);
			_cachingAssemblies = Orm.ConnectionSettings.AssembliesPath.IsSome();
			_usingConnection = new Lazy<string>(()=> "using (var conn = _orm.GetCurrentConnection())");
			_implementation = tableMapping.AdapterImplementation;
		}

		bool EmitNotImplented(IdentedStringBuilder builder, AdapterImplementation expected, bool async)
			=> EmitNotImplented(builder, async ? expected | AdapterImplementation.Async : expected | AdapterImplementation.Sync);
		
		bool EmitNotImplented(IdentedStringBuilder builder, AdapterImplementation expected)
		{
			if (_implementation.HasFlag(expected))
				return false;

			var missing = expected & (expected ^ _implementation);
			
			builder.AppendLine($"throw new NotImplementedException(\"Adapter was compilled with {_implementation} options. Should include {missing} option{(NumericHelper.IsPowerOfTwo((int)missing) ? "" : "s")} for method to complile\");");

			return true;
		}
		
		protected override void EmitAttributes(CsBuilder builder)
		{
			if (_cachingAssemblies)
				builder.AppendRaw($"[{builder.Type<OrmAdapterAttribute>().Replace("Attribute", "")}(typeof({GetEntityFullClassName(builder)}), @\"{TableMapping.Mold.Replace("\"", "\"\"")}\")]");
		}

		public override string Inherits(CsBuilder builder) 
			=> builder.Type<IRepoAdapter>();



		public override void EmitMembers(CsBuilder builder)
		{
			builder.UsingFor<System.Threading.CancellationToken>(); // Вводится заранее, чтобы CsBuilder содержал все необходимые ссылки
			builder.UsingFor<System.Threading.Tasks.Task>();
			builder.UsingFor<TypeEx>();
			builder.UsingFor(typeof(System.Collections.IEnumerable));
			builder.UsingFor(typeof(IAsyncEnumerable<>));
#if !NETSTANDARD2_1
			builder.RefAssemblies<System.ComponentModel.AsyncOperation>(); //TODO: избавиться непонятно почему, но без System.ComponentModel не работает AsyncDbReader<>
#endif
			EmitCtor(builder);

			EmitInitEntity(builder);
			
			var async = new AsyncHelper(false);
			
			builder.AppendLine();
			builder.AppendLine("#region sync methods");
			builder.AppendLine();

			EmitSelect(builder, async);
			EmitQuery(builder, async);
			EmitLoad(builder, async);
			EmitLoadByPks(builder, async);
			EmitInsert(builder, async);
			EmitInsertWithPks(builder, async);
			EmitUpdate(builder, async);
			EmitUpdateWithPks(builder, async);
			EmitDelete(builder, async);
			EmitReadUnmapped(builder, async);
			EmitWriteUnmapped(builder, async);
			
			builder.AppendLine();
			builder.AppendLine("#endregion");
			builder.AppendLine();
			builder.AppendLine("#region async methods");
			builder.AppendLine();
			
			async = new AsyncHelper(true);
			EmitSelect(builder, async);
			EmitQuery(builder, async);
			EmitLoad(builder, async);
			EmitLoadByPks(builder, async);
			EmitInsert(builder, async);
			EmitInsertWithPks(builder, async);
			EmitUpdate(builder, async);
			EmitUpdateWithPks(builder, async);
			EmitDelete(builder, async);
			EmitReadUnmapped(builder, async);
			EmitWriteUnmapped(builder, async);
			
			builder.AppendLine();
			builder.AppendLine("#endregion");
			builder.AppendLine();

			builder.Append(NonSequentialMembers.ToString());
		}

		void EmitCtor(CsBuilder builder)
		{
			var ormType = builder.Type<IOrm>();
			var repoType = TableMapping.IsDictionary && 
						   (TableMapping.PkColumns.FirstOrDefault()?.MemberDataType).Is(out TypeEx pkType)
				? builder.TypeRef(typeof (ISqlDictionary<,>).MakeGenericType(pkType, TableMapping.ModelType))
				: builder.Type<IRepository>();

			builder.AppendLine($@"{repoType} _repo;
{ormType} _orm;

public {ClassName}({ormType} orm, {repoType} repo)
{{
	_repo = repo;
	_orm = orm;
}}
");
		}

		void EmitInitEntity(CsBuilder builder)
			=> builder.AppendLine(@"public void InitEntity(object obj)
{
}
");
		
		protected TypeEx GenericEntityType => _tEnitityType ??= TypeEx.Get(typeof(Entity<>).MakeGenericType(EntityType));
		FastLazy<TypeEx>? _tEnitityType;

		protected void EmitEntityEvent(CsBuilder builder, string name)
		{
			if (_entityEvents.GetOrAdd(name, _ =>
				{
					return EntityType.TryFindMethod(new ReflectionFilter(name)
					{
						Access = AccessModifier.Public,
						IsStatic = false,
						IsAbstract = false,
						Predicate = method => !GenericEntityType.Equals(method.DeclaringType)
					}, out var _);
				}))
				builder.AppendLine($"entity.{name}();");
		}


		string CreateEntity(CsBuilder builder)
		{
			var ctor = EntityType.FindConstructor();
			if (ctor != null)
			{
				if (ctor.IsPublic)
					return $"new {GetEntityFullClassName(builder)}()";

				return $"({GetEntityFullClassName(builder)}){builder.TypeRef(typeof (TypeEx))}.Get<{GetEntityFullClassName(builder)}>().Create()";
			}

			return $"({GetEntityFullClassName(builder)}){builder.TypeRef(typeof (FormatterServices))}.GetUninitializedObject(typeof({GetEntityFullClassName(builder)}))";
		}

		protected virtual Method GetDataReaderMethod(IColumnMapping col)
			=> TypeDescriptor.GetDataReaderMethod(col.ColumnDataType);

		void EmitReadEntity(CsBuilder builder, bool yield, AsyncHelper async)
		{
			if (!TableMapping.IsDictionary)
			{
				builder.AppendLine($"var entity = {CreateEntity(builder)};");
				EmitEntityEvent(builder, "OnLoad");
				EmitReadColumns(builder, TableMapping.ReadColumns.OrderBy(c => c.ColumnOrdinal), async);
			}
			else
			{
				builder.AppendLine($@"{GetEntityFullClassName(builder)} entity;");
				//EmitEntityEvent(str, "OnLoad");

				var pkCol = TableMapping.PkColumns.First();
				var dataReaderMethod = GetDataReaderMethod(pkCol);
				var typeFromReader = dataReaderMethod.ReturnType!;
				var readerIndex = TableMapping.ReadColumns.IndexOf(pkCol);

				builder.AppendLine(AssignField(builder, pkCol, typeFromReader, $"reader.{dataReaderMethod.Name}({readerIndex})", async.MethodCall("reader.IsDBNull", readerIndex.ToString()), "var pk"));

				builder.AppendLine(yield
					? @"if(_repo.TryGetByPk(pk, out entity))
{{
	yield return entity;
	continue;
}}"
					: @"if(_repo.TryGetByPk(pk, out entity))
	return entity;");

				builder.AppendLine($"entity = {CreateEntity(builder)};");

				builder.AppendLine(AssignField(builder, pkCol, typeFromReader, "pk"));

				EmitReadColumns(builder, TableMapping.ReadColumns.Where(c => !c.IsPk).OrderBy(c => c.ColumnOrdinal), async);

				builder.AppendLine("_repo.Set(entity);");
			}
			EmitEntityEvent(builder, "OnLoaded");
		}

		protected virtual void EmitInsert(CsBuilder builder, AsyncHelper async)
		{
			using (EmitMethod(builder, async.MethodDefinition("void", "Insert", "object obj")))
			{
				if (EmitNotImplented(builder, AdapterImplementation.Insert, async))
					return;
				
				builder.AppendLine($"var entity = ({GetEntityFullClassName(builder)})obj;");

				using (EmitBlock(builder, UsingConnection))
				{
					EmitEntityEvent(builder, "OnTableChange");
					EmitEntityEvent(builder, "OnSave");
					EmitEntityEvent(builder, "OnInsert");

					EmitAutoInc(builder);

					var reader = !string.IsNullOrWhiteSpace(RepoQueries.InsertReturning);
					if (!reader)
					{
						builder.IncIdent();
						builder.AppendLine(async.BeginMethodCall("conn.ExecuteNoQuery", @$"@""{Escape(RepoQueries.Insert)}"", new {builder.Type<DbParameter>()} []
{{"));
					}
					else
					{
						builder.Append("using (var reader = ");
						builder.AppendLine(async.BeginMethodCall("conn.ExecuteReader", @$"@""{Escape(RepoQueries.Insert)}"", new {builder.Type<DbParameter>()} []
{{"));
					}

					using (builder.IncIdent())
						EmitParamsArray(builder, TableMapping.WriteColumns, RepoQueries.WriteParamPrefix);

					builder.Append($"}}{async.EndMethodCall()}");

					if (reader)
					{
						builder.AppendLine($@")
{{
	if(!{async.MethodCall("reader.Read", null)})
		throw new {builder.Type<Exception>()}(""Expected PK value(s)"");");

						using (builder.IncIdent())
							EmitReadColumns(builder, TableMapping.PkColumns.Where(c => c.IsAutoGenerated), async, true);

						builder.AppendLine("}");
					}
					else
					{
						builder.Append(";");
						builder.Ident--;
					}

					builder.EnsureNewLine();

					EmitEntityEvent(builder, "OnInserted");
					EmitEntityEvent(builder, "OnSaved");
					EmitEntityEvent(builder, "OnTableChanged");
				}
			}
		}

		void EmitUpdate(CsBuilder builder, AsyncHelper async)
		{
			using (EmitMethod(builder, async.MethodDefinition("int", "Update", "object obj")))
			{
				if (EmitNotImplented(builder, AdapterImplementation.Update, async))
					return;
				
				builder.AppendLine($"var entity = ({GetEntityFullClassName(builder)})obj;");
				using (EmitBlock(builder, UsingConnection))
				{
					EmitEntityEvent(builder, "OnTableChange");
					EmitEntityEvent(builder, "OnSave");
					EmitEntityEvent(builder, "OnUpdate");

					builder.Append("var result = ");
					builder.AppendLine(async.BeginMethodCall("conn.ExecuteNoQuery", @$"@""{Escape(RepoQueries.Update)}"", new {builder.Type<DbParameter>()} []
	{{"));
					using (builder.IncIdent(2))
					{
						EmitParamsArray(builder, TableMapping.WriteColumns, RepoQueries.WriteParamPrefix);
						EmitParamsArray(builder, TableMapping.PkColumns, RepoQueries.PkParamPrefix);
					}

					builder.AppendLine($"	}}{async.EndMethodCall()};");

					EmitEntityEvent(builder, "OnUpdated");
					EmitEntityEvent(builder, "OnSaved");
					EmitEntityEvent(builder, "OnTableChanged");
					builder.AppendLine("return result;");
				}
			}
		}
		void EmitDelete(CsBuilder builder, AsyncHelper async)
		{
			using (EmitMethod(builder, async.MethodDefinition("int", "Delete", "object obj")))
			{
				if (EmitNotImplented(builder, AdapterImplementation.Delete, async))
					return;
				
				builder.AppendLine($"var entity = ({GetEntityFullClassName(builder)})obj;");
				using (EmitBlock(builder, UsingConnection))
				{
					EmitEntityEvent(builder, "OnTableChange");
					EmitEntityEvent(builder, "OnDelete");

					builder.Append("var result = ");
					builder.AppendLine(async.BeginMethodCall("conn.ExecuteNoQuery", @$"@""{Escape(RepoQueries.Delete)}"", new {builder.Type<DbParameter>()} []
	{{"));
					using (builder.IncIdent(2))
						EmitParamsArray(builder, TableMapping.PkColumns, RepoQueries.PkParamPrefix);

					builder.AppendLine($"	}}{async.EndMethodCall()};");

					EmitEntityEvent(builder, "OnDeleted");
					EmitEntityEvent(builder, "OnTableChanged");
					builder.AppendLine("return result;");
				}
			}
		}

		protected virtual void EmitInsertWithPks(CsBuilder builder, AsyncHelper async)
		{
			using (EmitMethod(builder, async.MethodDefinition("int", "InsertWithPks", "object obj")))
			{
				if (EmitNotImplented(builder, AdapterImplementation.Insert | AdapterImplementation.WithPks, async))
					return;
				
				builder.AppendLine($"var entity = ({GetEntityFullClassName(builder)})obj;");
				using (EmitBlock(builder, UsingConnection))
				{
					EmitEntityEvent(builder, "OnTableChange");
					EmitEntityEvent(builder, "OnSave");
					EmitEntityEvent(builder, "OnInsert");
					EmitAutoInc(builder);
					
					builder.Append("var result = ");
					builder.AppendLine(async.BeginMethodCall("conn.ExecuteNoQuery", @$"@""{Escape(RepoQueries.InsertWithPks)}"", new {builder.Type<DbParameter>()} []
{{"));
					using (builder.IncIdent())
					{
						EmitParamsArray(builder, TableMapping.WriteColumns, RepoQueries.WriteParamPrefix);
						EmitParamsArray(builder, TableMapping.PkColumns, RepoQueries.WriteParamPrefix);
					}

					builder.AppendLine($"}}{async.EndMethodCall()};");

					EmitEntityEvent(builder, "OnInserted");
					EmitEntityEvent(builder, "OnSaved");
					EmitEntityEvent(builder, "OnTableChanged");
					builder.AppendLine("return result;");
				}
			}
		}

		void EmitUpdateWithPks(CsBuilder builder, AsyncHelper async)
		{
			using (EmitMethod(builder, async.MethodDefinition("int", "UpdateWithPks", "object obj, object[] pks")))
			{
				if (EmitNotImplented(builder, AdapterImplementation.Update | AdapterImplementation.WithPks, async))
					return;
				
				builder.AppendLine($@"if(pks.Length != {TableMapping.PkColumns.Count})
	throw new {builder.Type<ArgumentException>()}(""Expected an array of {TableMapping.PkColumns.Count} element(s)"", ""pks"");

var entity = ({GetEntityFullClassName(builder)})obj;");

				using (EmitBlock(builder, UsingConnection))
				{
					EmitEntityEvent(builder, "OnTableChange");
					EmitEntityEvent(builder, "OnSave");
					EmitEntityEvent(builder, "OnUpdate");

					builder.Append("var result = ");
					builder.AppendLine(async.BeginMethodCall("conn.ExecuteNoQuery", @$"@""{Escape(RepoQueries.UpdateWithPks)}"", new {builder.Type<DbParameter>()} []
	{{"));
					using (builder.IncIdent(2))
					{
						EmitParamsArray(builder, TableMapping.WriteColumns, RepoQueries.WriteParamPrefix);
						EmitParamsArray(builder, TableMapping.PkColumns, RepoQueries.WriteParamPrefix);

						var i = 0;
						foreach (var pk in TableMapping.PkColumns)
							EmitCreateParameter(builder, RepoQueries.PkParamPrefix, pk, $"pks[{i++}]", pk.MemberValueType);
					}

					builder.AppendLine($"	}}{async.EndMethodCall()};");

					EmitEntityEvent(builder, "OnUpdated");
					EmitEntityEvent(builder, "OnSaved");
					EmitEntityEvent(builder, "OnTableChanged");
					builder.AppendLine("return result;");
				}
			}
		}

		void EmitAutoInc(CsBuilder builder)
		{
			foreach (var column in TableMapping.Columns)
			{
				var seq1 = column.Member.Sequence;
				if (seq1 != null)
					try
					{
						TypeEx typeFrom;
						switch (seq1.Type)
						{
						case SequenceType.ComputeOnInsert:
							typeFrom = column.ColumnNativeType ?? throw new Exception($"Unable to map a column of type {column.ColumnDataType} to any known type");

							builder.AppendLine($@"if (entity.{column.MemberName} == {GetDefaultValue(builder, column.MemberDataType)})
	{AssignField(builder, column, typeFrom, $"conn.ExecuteScalar<{builder.TypeRef(typeFrom)}>(@\"select max({Escape(column.EscapedColumnName)}) from {Escape(TableMapping.TableName)}\", {seq1.Start}) + {seq1.Step};")}
							");

							break;
						case SequenceType.Memory:
							typeFrom = column.ColumnNativeType ?? throw new Exception($"Unable to map a column of type {column.ColumnDataType} to any known type");

							var seqVar = _customFields.GetOrAdd($"{column.MemberName}AutoInc", _ =>
							{
								var name = $"_autoInc{_customFields.Count}";
								var lazy = builder.TypeRef(typeof(SyncLazy<>).MakeGenericType(typeFrom));
								NonSequentialMembers.AppendLine($@"{lazy} {name} = {lazy}.Create<{ClassName}>(self=>
{{
	using (var conn = self._orm.GetCurrentConnection())
		return conn.ExecuteScalar<{builder.TypeRef(typeFrom)}>(@""select max({Escape(column.EscapedColumnName)}) from {Escape(TableMapping.TableName)}"", {seq1.Start});
}});");
								return name;
							});

							builder.AppendLine($@"if (entity.{column.MemberName} == {GetDefaultValue(builder, column.MemberDataType)})
	{AssignField(builder, column, typeFrom, $"({seqVar}.Value = {seqVar}.GetValue(this) + {seq1.Step})")}
							");

							break;
						}
					}
					catch (InvalidCastException ex)
					{
						Utils.Nop(ex);
						var err = $"Unable to cast from {column.ColumnDataType} to {builder.TypeRef(column.MemberDataType)}";
						Log.Error(err);
						builder.AppendLine($"// entity.{column.MemberName}=({builder.TypeRef(column.MemberDataType)})val; //{err}");
					}
			}
		}
		
		void EmitSelect(CsBuilder builder, AsyncHelper async)
		{
#if !NETSTANDARD2_1
			if (async)
			{
				using (EmitMethod(builder, async.EnumerableMethodDefinition("Select", null)))
				{
					if (EmitNotImplented(builder, AdapterImplementation.Select | AdapterImplementation.Async))
						return;

					builder.AppendLine(
						$@"return new {builder.TypeRef(typeof(AsyncDbReader<>).MakeGenericType(EntityType))}(
	() => _orm.GetCurrentConnection(),
	conn => conn.CreateCommandAsync(@""{Escape(RepoQueries.SelectAll)}"", null),
	reader =>
	{{");
					using (builder.IncIdent(2))
						EmitReadEntity(builder, false, new AsyncHelper(false));

					builder.AppendLine("		");
					builder.AppendLine(@"		return entity;
	});");
				}
				return;
			}
#endif
			using (EmitMethod(builder, async.EnumerableMethodDefinition("Select", null)))
			{
				if (EmitNotImplented(builder, AdapterImplementation.Select, async))
					return;
				
				builder.AppendLine($@"{UsingConnection}
using (var reader = {async.MethodCall("conn.ExecuteReader", @$"@""{Escape(RepoQueries.SelectAll)}"", null")})
{{
	while({async.MethodCall("reader.Read", null)})
	{{");

				using (builder.IncIdent(2))
					EmitReadEntity(builder, true, async);

				builder.AppendLine("		");
				builder.AppendLine(@"		yield return entity;
	}
}");
			}
		}

		void EmitQuery(CsBuilder builder, AsyncHelper async)
		{
			using (EmitMethod(builder, async.EnumerableMethodDefinition("Query", $"string query, {builder.Type<DbParameter>()}[] parameters")))
			{
#if !NETSTANDARD2_1
				if (async)
				{
					if (EmitNotImplented(builder, AdapterImplementation.Select | AdapterImplementation.Async))
						return;

					builder.AppendLine(
						$@"return new {builder.TypeRef(typeof(AsyncDbReader<>).MakeGenericType(EntityType))}(
	() => _orm.GetCurrentConnection(),
	conn => conn.CreateCommandAsync(query, parameters),
	reader =>
	{{");
					using (builder.IncIdent(2))
						EmitReadEntity(builder, false, new AsyncHelper(false));

					builder.AppendLine("		");
					builder.AppendLine(@"		return entity;
	});");
					return;
				}
#endif

				if (EmitNotImplented(builder, AdapterImplementation.Select, async))
					return;

				builder.AppendLine($@"{UsingConnection}
using (var reader = {async.MethodCall("conn.ExecuteReader", "query, parameters")})
{{
	while({async.MethodCall("reader.Read", null)})
	{{");
				using (builder.IncIdent(2))
				{
					EmitReadEntity(builder, true, async);
					builder.AppendLine();
					builder.AppendLine("yield return entity;");
				}

				builder.AppendLine(@"	}
}");
			}
		}

		void EmitLoad(CsBuilder builder, AsyncHelper async)
		{
			using (EmitMethod(builder, async.MethodDefinition("object", "Load", $"string query, {builder.Type<DbParameter>()}[] parameters")))
			{
				if (EmitNotImplented(builder, AdapterImplementation.Load, async))
					return;
				
				builder.AppendLine($@"{UsingConnection}
using (var reader = {async.MethodCall("conn.ExecuteReader", "query, parameters")})
{{
	if({async.MethodCall("reader.Read", null)})
	{{");

				using (builder.IncIdent(2))
				{
					EmitReadEntity(builder, false, async);
					builder.AppendLine();
					builder.AppendLine("return entity;");
				}
				
				builder.AppendLine(@"	}
}
return null;");
			}
		}

		void EmitLoadByPks(CsBuilder builder, AsyncHelper async)
		{
			using (EmitMethod(builder, async.MethodDefinition("object", "LoadByPks", "object[] pks")))
			{
				if (EmitNotImplented(builder, AdapterImplementation.Load | AdapterImplementation.WithPks, async))
					return;

				builder.Append($@"if(pks.Length != {TableMapping.PkColumns.Count})
	throw new {builder.Type<ArgumentException>()}(""Expected an array of {TableMapping.PkColumns.Count} element(s)"", ""pks"");

{UsingConnection}
using (var reader = ");
				builder.AppendLine(async.BeginMethodCall("conn.ExecuteReader", @$"@""{Escape(RepoQueries.Select + RepoQueries.IdentityWhere)}"", new {builder.Type<DbParameter>()} []
{{"));
				using (builder.IncIdent())
				{

					var i = 0;
					foreach (var pk in TableMapping.PkColumns)
						EmitCreateParameter(builder, RepoQueries.PkParamPrefix, pk, $"pks[{i++}]", pk.MemberValueType);
				}
				builder.AppendLine($@"}}{async.EndMethodCall()})
{{
	if({async.MethodCall("reader.Read", null)})
	{{");
				using (builder.IncIdent(2))
				{
					EmitReadEntity(builder, false, async);
					builder.AppendLine();
					builder.AppendLine("return entity;");
				}
				builder.AppendLine(@"	}
}
return null;");
			}
		}



		void EmitReadUnmapped(CsBuilder builder, AsyncHelper async)
		{
			using (EmitMethod(builder, async.MethodDefinition("TValue", "ReadUnmapped", "object obj, string fieldName", "<TValue>")))
			{
				if (EmitNotImplented(builder, AdapterImplementation.ReadWriteUnmapped, async))
					return;
				
				builder.AppendLine($"var entity = ({GetEntityFullClassName(builder)})obj;");

				builder.Append($@"{UsingConnection}
	return ");
				builder.AppendLine(async.BeginMethodCall("conn.ExecuteScalar", $@"string.Format(@""{Escape(RepoQueries.ReadUnmapped)}"", fieldName), new {builder.Type<DbParameter>()} []
		{{", "<TValue>"));
					
				using (builder.IncIdent(3))
					EmitParamsArray(builder, TableMapping.PkColumns, RepoQueries.PkParamPrefix);

				builder.AppendLine($"		}}{async.EndMethodCall(tokenParamName: true)};");
			}
		}

		void EmitWriteUnmapped(CsBuilder builder, AsyncHelper async)
		{
			using (EmitMethod(builder, async.MethodDefinition("int", "WriteUnmapped", "object obj, string fieldName, TValue value", "<TValue>")))
			{
				if (EmitNotImplented(builder, AdapterImplementation.ReadWriteUnmapped, async))
					return;
				
				builder.AppendLine($"var entity = ({GetEntityFullClassName(builder)})obj;");
				builder.Append($@"{UsingConnection}
	return ");
				builder.AppendLine(async.BeginMethodCall("conn.ExecuteNoQuery", $@"string.Format(@""{Escape(RepoQueries.WriteUnmapped)}"", fieldName), new []
	{{"));
				using (builder.IncIdent(2))
					EmitParamsArray(builder, TableMapping.PkColumns, RepoQueries.PkParamPrefix);

				builder.AppendLine($@"		_orm.CreateParameter(""value"", value)
	}}{async.EndMethodCall()};");
			}
		}

		protected virtual void EmitParamsArray(CsBuilder builder, IEnumerable<IColumnMapping> columns, string preffix = "")
		{
			foreach (var column in columns)
			{
				var target = column.PublicGetter 
					? $"entity.{column.MemberName}" 
					: $"{GetAccessorName(builder, column.Member.Inner)}.GetValue(entity)";

				var mem = target;
				var type = column.MemberDataType;
				var dstType = column.ColumnNativeType ?? throw new Exception($"Unable to map a column of type {column.ColumnDataType} to any known type");

				var isNullableStruct = type.IsNullable;
				if (isNullableStruct)
					type = type.GenericArguments[0];

				if (!type.Equals(dstType))
				{
					if (type.CanCastTo(dstType))
					{
						mem = $"({builder.TypeRef(dstType)}){mem}";
						type = dstType;
					}
					else
					{
						var convIfc = FindBestConvertiable(type, dstType, false);
						if (convIfc != null)
						{
							var ifcArg = convIfc.GenericArguments[0];
							mem = type.CanCastTo(ifcArg)
								? $"({builder.TypeRef(ifcArg)}){mem}"
								: InvokeConvertiableValue(builder, type, mem, convIfc, false);

							type = ifcArg;
						}
						else if (TypeDescriptor.IsString(column.ColumnDataType))
						{
							mem = $"{builder.Type<StringConverter>()}.Default.ToString({mem})";
							type = typeof (string);
						}
					}
				}

				if (isNullableStruct)
				{
					mem = ApplyValueConverter(builder, column, $"{mem}.Value", type);
					mem = $"({target}.HasValue ? (object){mem} : {builder.Type<DBNull>()}.Value)";
				}
				else if (column.IsNullable && column.SaveDefaultValuesAsDbNull)
				{
					mem = ApplyValueConverter(builder, column, mem, type);
					mem = $"{target}!={GetDefaultValue(builder, column.MemberDataType)}?(object){mem}:{builder.Type<DBNull>()}.Value";
				}
				else
					mem = ApplyValueConverter(builder, column, mem, type);
				

				EmitCreateParameter(builder, preffix, column, mem, type);
			}
		}

		protected virtual string GetAccessorName(CsBuilder builder, BaseDataMember mem)
			=> _customFields.GetOrAdd(mem.Name, _ =>
			{
				NonSequentialMembers.AppendLine(_cachingAssemblies 
					? $"static {builder.Type<Lazy<BaseDataMember>>()} _accessor{_customFields.Count} = new {builder.Type<Lazy<BaseDataMember>>()}(()=>{builder.Type<TypeEx>()}.Get<{builder.TypeRef(EntityType)}>().FindDataMember(\"{mem.Name}\"));" 
					: $"static {builder.Type<Lazy<BaseDataMember>>()} _accessor{_customFields.Count} = new {builder.Type<Lazy<BaseDataMember>>()}(()=>{builder.Type<BaseDataMember>()}.GetById({mem.Id}));");

				return $"_accessor{_customFields.Count}.Value";
			});

		protected virtual void EmitCreateParameter(CsBuilder builder, string preffix, IColumnMapping col, string mem, TypeEx? type)
		{
			builder.Append($"_orm.CreateParameter(\"{preffix}{col.ColumnName}\", {mem}");

			if (type != null)
				builder.Append($", typeof({builder.TypeRef(type)})");
			
			builder.AppendLine("),");
		}

		protected string ApplyValueConverter(CsBuilder builder, IColumnMapping col, string mem, TypeEx type)
		{
			if (col.ValueConverter?.Equals(typeof(BasicDbValueConverter)) == false)
			{
				var instance = ValueConverterInstances.GetOrAdd(col.ValueConverter, conv => (IDbValueConverter)conv.Create());
				if (instance.ActiveTypes.Contains(type.Inner))
				{
					var name = ValueConverterNames.GetOrAdd(col.ValueConverter, conv =>
					{
						var result = $"_valueConverter{ValueConverterNames.Count}";
						NonSequentialMembers.AppendLine($"{builder.Type<IDbValueConverter>()} {result} = new {builder.TypeRef(conv)}();");
						return result;
					});

					if (type == typeof(string))
						mem = $"{name}.ConvertString({mem}, {col.DataLength})";
					else if (type == typeof(int))
						mem = $"{name}.ConvertInt({mem}, {col.DataLength})";
					else if (type == typeof(long))
						mem = $"{name}.ConvertLong({mem}, {col.DataLength})";
					else if (type == typeof(DateTime))
						mem = $"{name}.ConvertDateTime({mem}, {col.DataLength})";
					else
						mem = $"{name}.Convert<{builder.TypeRef(type)}>({mem}, {col.DataLength})";
				}
			}

			return mem;
		}

		protected virtual string Escape(string str)
			=> str.Replace("\"", "\"\"");

		protected virtual void EmitReadColumns(
			CsBuilder builder, 
			IEnumerable<IColumnMapping> columns, 
			AsyncHelper async, 
			bool customOrder = false)
		{
			if (EntityType.Name == "CheckedFace")
				Utils.Nop();

			var idx = 0;
			foreach (var column in columns)
			{
				if (column.MemberName == "CameraId")
					Utils.Nop();

				try
				{
					builder.AppendLine(AssignField(builder, column, customOrder ? idx : TableMapping.ReadColumns.IndexOf(column), async));
				}
				catch (InvalidCastException ex)
				{
					Utils.Nop(ex);
					var err = $"Unable to cast from {column.ColumnDataType} to {builder.TypeRef(column.MemberDataType)}";
					Log.Error(err);
					builder.AppendLine($"// entity.{column.MemberName}=({builder.TypeRef(column.MemberDataType)})val; //{err}");
				}

				idx++;
			}
		}

		protected virtual string AssignField(
			CsBuilder builder, 
			IColumnMapping column, 
			int readerIndex, 
			AsyncHelper async)
		{
			var typeFrom = column.ColumnNativeType;
			if (typeFrom == null)
				throw new Exception($"Unable to map a column of type {column.ColumnDataType} to any known type");

			var dataReaderMethod = TypeDescriptor.GetDataReaderMethod(typeFrom);
			var invoke = $"reader.{builder.Emit(dataReaderMethod)}({readerIndex})";
			var readerReturn = dataReaderMethod.ReturnType!;
			if (readerReturn == typeof(object))
			{
				invoke = $"(({builder.TypeRef(typeFrom)}){invoke})";
				readerReturn = typeFrom;
			}
			return AssignField(builder, column, readerReturn, invoke, async.MethodCall("reader.IsDBNull", readerIndex.ToString()));
		}

		readonly Dictionary<BaseDataMember, bool> _needInitField = new();
		 
		protected virtual string AssignField(
			CsBuilder builder, 
			IColumnMapping column,
			TypeEx typeFromReader,
			string value, 
			string? readerIsNullValue = null, 
			string? assign = null)
		{
			var mem = column.MemberName;
			var entMem = assign ?? $"entity.{mem}";
			var init = "";
			var expectedType = column.MemberDataType;
			var publicSetter = assign != null || column.PublicSetter;
			var canCast = publicSetter && typeFromReader.CanCastTo(expectedType);

			builder.RefAssemblies(expectedType);

			if (!typeFromReader.Equals(column.MemberDataType) && !canCast)
			{
				var convIfc = FindBestConvertiable(column.MemberDataType, typeFromReader, true);

				if (convIfc != null)
				{
					publicSetter = true;

					if (assign == null)
					{
						if (_needInitField.GetOrAdd(column.Member.Inner, _ =>
						{
							try
							{
								if (!column.PublicSetter || column.MemberDataType.IsValueType || column.MemberDataType.IsAbstract || column.MemberDataType.IsInterface)
									return false;

								var ci = column.MemberDataType.FindConstructor();
								if (ci == null || !ci.IsPublic)
									return false;

								if (EntitySample != null)
									return column.Member.Inner.GetValue(EntitySample) == null;

								return true;
							}
							catch { /*ignored*/ }

							return false;
						}))
							init = $"if({entMem} == null) {entMem} = new {builder.TypeRef(column.MemberDataType)}();\r\n";

						expectedType = convIfc.GenericArguments[0];

						if (column.Member.Inner is Property && column.MemberDataType.IsValueType)
						{
							value = CastAndCheckDbNull(builder, column, typeFromReader, value, readerIsNullValue, expectedType);

							return $"{entMem} = new {builder.TypeRef(column.MemberDataType)}{{ Value = {value} }};"; // надоело, ебанутый кейс, который никогда не возникнет!
						}

						entMem = InvokeConvertiableValue(builder, column.MemberDataType, entMem, convIfc, true);

					}
				}
			}

			value = CastAndCheckDbNull(builder, column, typeFromReader, value, readerIsNullValue, expectedType);

			if (publicSetter)
				return $@"{init}{entMem} = {value};";

			if (column.Member.Inner.CanWrite)
				return $"{GetAccessorName(builder, column.Member.Inner)}.SetValue(entity, {value});";

			throw new Exception($"Unable to emit member assignment for \"{EntityType}.{mem}\"");
		}

		string CastAndCheckDbNull(
			CsBuilder builder,
			IColumnMapping column,
			TypeEx typeFromReader,
			string value, 
			string? readerIsNullValue, 
			TypeEx expectedType)
		{
			var nullType = expectedType;
			if (!expectedType.Equals(typeFromReader))
				switch (typeFromReader.GetCastType(expectedType))
				{
				case CastType.Implicit:
					//if (expectedType.IsNullable)
					nullType = typeFromReader;

					break;
				case CastType.Explicit:
					value = $"({builder.TypeRef(expectedType)}){value}";
					typeFromReader = expectedType;
					break;
				default:
					value = $"({Cast(builder, typeFromReader, expectedType, value)})";
					typeFromReader = expectedType;
					break;
				}

			if (column.IsNullable && readerIsNullValue != null)
			{
				var cast = (expectedType.IsValueType && !typeFromReader.Equals(expectedType)).Then(() => $"({builder.TypeRef(expectedType)})");

				value = $"{readerIsNullValue} ? {cast}{GetDefaultValue(builder, nullType)} : {value}";
			}

			return value;
		}

		static string InvokeConvertiableValue(
			CsBuilder builder, 
			TypeEx memberType, 
			string member, 
			TypeEx convIfc, 
			bool write)
		{
			var impl = convIfc.FindProperty("Value")!.FindImplementation(memberType);
			string value;
			if(write || memberType.IsValueType)
				value = ".Value";
			else
			{
#if NETSTANDARD
				if (!convIfc.GenericArguments[0].IsValueType)
					value = "?.Value";
				else
#endif
				{
					builder.UsingFor(typeof(Monads));
					value = ".With(c => c.Value)";
				}
			}

			return impl.IsPublic ? $"{member}{value}" : $"(({builder.TypeRef(convIfc)}){member}){value}";
		}

		static TypeEx? FindBestConvertiable(TypeEx type, TypeEx targetType, bool write)
		{
			TypeEx? convIfc = null;
			foreach (var ifc in type.FindGenericInterfaces(typeof(IConvertiableFrom<>)))
			{
				var ifcArg = ifc.GenericArguments[0];
				if (targetType.Equals(ifcArg))
					return ifc;

				if (write ? targetType.CanCastTo(ifcArg) : ifcArg.CanCastTo(targetType)) //TODO: find best cast
					convIfc = ifc;
			}

			return convIfc;
		}

		protected virtual string Cast(CsBuilder builder, TypeEx typeFrom, TypeEx typeTo, string value)
		{
			if (typeFrom.Equals(typeTo))
				return value;

			if (typeFrom.CanCastTo(typeTo))
				return $"({builder.TypeRef(typeTo)}){value}";

			if (typeTo == typeof (Guid))
			{
				if (TypeDescriptor.IsString(typeFrom))
					return $"new {builder.Type<Guid>()}({value})";

				return $"new {builder.Type<Guid>()}((byte[]){value})";
			}

			if( typeTo == typeof(Color) )
			{
				builder.RefAssemblies<Color>();
			
				if (TypeDescriptor.IsInteger(typeFrom) || TypeDescriptor.IsFloat(typeFrom))
					return $"{builder.Type<Color>()}.FromArgb({Cast(builder, typeFrom, typeof(int), value)})";

				//TODO: тут по идее могут быть значения как Red, так и #F00, так и RGB(255,0,0). FromName нормально обработает только Red, хотя и ругаться не будет
			}
			else 

			if (typeTo == typeof (byte[]))
				return $"(byte[]){value}";
			if (TypeDescriptor.IsVariant(typeTo))
				return $"({builder.TypeRef(typeTo)}){value}";

			switch (typeTo.TypeCode)
			{
			case TypeCode.Boolean:

				if (TypeDescriptor.IsInteger(typeFrom) || TypeDescriptor.IsFloat(typeFrom) || typeFrom == typeof (char))
					return $"{value} != 0";

				//TODO: Blob, Variant

				break;

			case TypeCode.String:

				if (TypeDescriptor.IsInteger(typeFrom))
					return $"{value}.ToString()";

				return $"{builder.Type<StringConverter>()}.Default.ToString({value})";

			case TypeCode.Char:
			case TypeCode.Byte:
			case TypeCode.SByte:
			case TypeCode.Int16:
			case TypeCode.UInt16:
			case TypeCode.Int32:
			case TypeCode.UInt32:
			case TypeCode.Int64:
			case TypeCode.UInt64:
			case TypeCode.Single:
			case TypeCode.Double:
			case TypeCode.Decimal:

				if (TypeDescriptor.IsInteger(typeFrom) || TypeDescriptor.IsFloat(typeFrom))
					return $"({builder.TypeRef(typeTo)}){value}"; //Enums feels ok here :)

				//TODO: Blob, Guid, Variant

				if (TypeDescriptor.IsDateTime(typeFrom))
					return $"({builder.TypeRef(typeTo)}){value}.Ticks";

				if (TypeDescriptor.IsBoolean(typeFrom))
					return $"({builder.TypeRef(typeTo)})({value} ? 1 : 0)";

				break;

			case TypeCode.DateTime:
				if (TypeDescriptor.IsInteger(typeFrom))
					return $"new {builder.Type<DateTime>()}((long){value})";

				if (TypeDescriptor.IsFloat(typeFrom))
					return $"{builder.TypeRef(typeof(Utils))}.Double2JulianDate((double){value})";

				break;
			}

			if (TypeDescriptor.IsString(typeFrom))
				return $"{builder.Type<StringConverter>()}.Default.ToObject<{builder.TypeRef(typeTo)}>({value})";

			foreach (var itf in typeTo.FindGenericInterfaces(typeof(ICollection<>)))
			{
				var elem = itf.GenericArguments[0];
				if ((elem.Inner.IsPrimitive || elem == typeof(string)) &&
					typeTo.FindConstructor(elem.MakeArrayType())?.IsPublic == true)
				{
					if (typeFrom.IsArray && typeFrom.ArrayItemType!.Equals(elem))
						return $"new {builder.TypeRef(typeTo)}({value})";

					if (typeFrom == typeof(object))
						return $"new {builder.TypeRef(typeTo)}(({builder.TypeRef(elem)}[]){value})";
				}
			}

			throw new InvalidCastException();
		}

		protected string GetDefaultValue(CsBuilder builder, TypeEx type)
		{
			if (!type.IsValueType)
				return "null";
			
			if (type.HasInterface<IIntValue>())
				return $"new {builder.TypeRef(type)}(0)";

			if (type.HasInterface<IStringValue>())
				return $"new {builder.TypeRef(type)}(\"\")";

			if (type == typeof (Guid) || type == typeof (Color))
				return $"{builder.TypeRef(type)}.Empty";

			return type.TypeCode switch
			{
				TypeCode.Boolean => "false",
				TypeCode.Char or
					TypeCode.SByte or
					TypeCode.Byte or
					TypeCode.Int16 or
					TypeCode.UInt16 or
					TypeCode.Int32 or
					TypeCode.UInt32 or
					TypeCode.Int64 or
					TypeCode.UInt64 or
					TypeCode.Single or
					TypeCode.Double or
					TypeCode.Decimal => $"({builder.TypeRef(type)})0",
				TypeCode.DateTime => $"{builder.Type<DateTime>()}.MinValue",
				TypeCode.Object or
					TypeCode.String => "null",
				_ => throw new InvalidCastException($"Invalid TypeCode: {type.TypeCode}"),
			};
		}
	}
}
