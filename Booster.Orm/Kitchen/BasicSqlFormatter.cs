﻿using System;
using System.Collections;
using System.Text;
using Booster.Localization.Pluralization;
using Booster.Orm.Interfaces;
using Booster.Reflection;

#nullable enable

namespace Booster.Orm.Kitchen;

public class BasicSqlFormatter : ISqlFormatter
{
	protected readonly IOrm Orm;

	public BasicSqlFormatter(IOrm orm) 
		=> Orm = orm;

	public virtual string ParamPrefix => "@";
	public virtual bool AllowArrayParam => false;

	public virtual string CreateColumnName(string memberName) 
		=> memberName;

	public virtual string CreateTableName(string className)
		=> Orm is BaseOrm { ConvertModelNames2PluralTableNames: true }
			? EnPluralizer.PluralizeCamelCase(className)
			: className;

	public virtual bool IsReservedName(string name)
		=> name switch
		{
			"all"      => true,
			"and"      => true,
			"any"      => true,
			"as"       => true,
			"asc"      => true,
			"check"    => true,
			"column"   => true,
			"create"   => true,
			"default"  => true,
			"desc"     => true,
			"distinct" => true,
			"do"       => true,
			"else"     => true,
			"for"      => true,
			"from"     => true,
			"grant"    => true,
			"group"    => true,
			"having"   => true,
			"in"       => true,
			"into"     => true,
			"like"     => true,
			"not"      => true,
			"null"     => true,
			"on"       => true,
			"or"       => true,
			"order"    => true,
			"select"   => true,
			"table"    => true,
			"then"     => true,
			"to"       => true,
			"union"    => true,
			"unique"   => true,
			"user"     => true,
			"where"    => true,
			"with"     => true,
			_          => false
		};


	public virtual string EscapeName(string name) // TODO: use field.EscapedName
	{
		if (IsReservedName(name.ToLower()))
			return $"\"{name}\"";

		return name;
	}

	public virtual string ParamName(string param) 
		=> ParamPrefix + param;

	public virtual string? FormatConst(object? value)
	{
		if (value == null)
			return FormatNull(null, TypeCode.Empty);

		value = (Orm as BaseOrm)?.PrepareValueForParam(value) ?? value;

		TypeEx type = value.GetType();
		if (type == typeof (Guid))
			return FormatGuid((Guid) value);

		if (type.IsArray)
			return FormatArray((ICollection) value, type);

		switch (type.TypeCode)
		{
		case TypeCode.Empty:
		case TypeCode.DBNull:
			return FormatNull(value, type.TypeCode);
		case TypeCode.Boolean:
			return FormatBoolean((bool)value);
		case TypeCode.SByte:
		case TypeCode.Byte:
		case TypeCode.Int16:
		case TypeCode.UInt16:
		case TypeCode.Int32:
		case TypeCode.UInt32:
		case TypeCode.Int64:
		case TypeCode.UInt64:
			return FormatInteger(value, type.TypeCode);

		case TypeCode.Single:
		case TypeCode.Double:
		case TypeCode.Decimal:
			return FormatFloat(value, type.TypeCode);

		case TypeCode.DateTime:
			return FormatDateTime((DateTime)value);

		case TypeCode.String:
		case TypeCode.Char:
			return FormatString(StringConverter.Default.ToString(value) ?? "", type.TypeCode);

		default:
			throw new ArgumentOutOfRangeException();
		}
	}

	public virtual string EscapeString(string value)
	{
		var bld = new StringBuilder(value.Length);

		foreach (var ch in value)
		{
			if (char.IsLetterOrDigit(ch))
			{
				bld.Append(ch);
				continue;
			}
			switch (ch)
			{
			case '\'':
			case '`':
			case '´':
				bld.Append("''");
				continue;
			case '\n':
			case '\r':
			case '\t':
			case ' ':
				bld.Append(ch);
				continue;
			}

			if (char.IsPunctuation(ch) || char.IsSymbol(ch))
			{
				bld.Append(ch);
				continue;
			}
			bld.Append(' ');
		}

		return bld.ToString();
	}

	protected virtual string FormatNull(object? value, TypeCode type) 
		=> "null";

	protected virtual string FormatArray(ICollection value, TypeEx type)
	{
		var str = new EnumBuilder(",", "(", ")", "null");

		foreach (var o in value)
			str.Append(FormatConst(o));

		return str;
	}

	protected virtual string? FormatInteger(object value, TypeCode tc) 
		=> StringConverter.Default.ToString(value);

	protected virtual string? FormatFloat(object value, TypeCode tc) 
		=> StringConverter.Default.ToString(value);

	protected virtual string? FormatBoolean(bool value) 
		=> StringConverter.Default.ToString(value);

	protected virtual string FormatDateTime(DateTime value) 
		=> $"timestamp '{value:yyyy-MM-dd hh:mm:ss.f}'";

	protected virtual string FormatGuid(Guid value) 
		=> $"'{value}'";

	protected virtual string FormatString(string str, TypeCode tc) 
		=> $"'{EscapeString(str)}'";
}