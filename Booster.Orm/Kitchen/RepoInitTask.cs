using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Booster.DI;
using Booster.Debug;
using Booster.Log;
using Booster.Orm.Interfaces;
using Booster.Reflection;

namespace Booster.Orm.Kitchen;

public class RepoInitTask : IRepoInitTask
{
	public RepoInitContext TableInfo { get; set; }
	readonly ManualResetEvent _done = new(false);
	readonly ManualResetEvent _adapterReady = new(false);
	readonly List<Func<RepoInitContext, IConnection, Task>> _postActions = new();
	readonly ILog _log;

	public EntityInfo EntityInfo { get; }
	public IRepoAdapterBuilder AdapterBuilder { get; set; }
	public IRepository Repository { get; set; }

	public TypeEx EntityType => EntityInfo.EntityType;
		
	IRepoAdapter _adapter;
	public IRepoAdapter Adapter
	{
		get => _adapter;
		set
		{
			_adapter = value;
			if (_adapter != null)
			{
				_adapterReady.Set();

				if (_postActions.Count == 0 || _adapter is InvalidRepoAdapter)
					_done.Set();
				else
					Utils.StartTaskNoFlow(async () =>
					{
						try
						{
							using var conn = Repository.Orm.GetCurrentConnection();

							using (InstanceFactory.PushContext(Repository.Orm.InstanceContext))
								foreach (var act in _postActions)
									try
									{
										await act(TableInfo, conn).NoCtx();
									}
									catch (Exception ex)
									{
										_log.Error(ex, $"Repository post action fail: {ex.Message}");
										SafeBreak.Break();
									}
						}
						finally
						{
							_done.Set();
						}
					});
			}
		}
	}
		
	public RepoInitTask(EntityInfo entityInfo, ILog log)
	{
		EntityInfo = entityInfo;
		_log = log.Create(this, new {EntityType});
	}

	public bool WaitForAdapterReady(int timeout = -1)
		=> _adapterReady.WaitOne(timeout);

	public bool WaitForEventsHandled(int timeout = -1)
		=> _done.WaitOne(timeout);

	public Task WaitForEventsHandledAsync()
		=> _done.AsTask();

	public void AddAction(Func<RepoInitContext, IConnection, Task> action)
		=> _postActions.Add(action);
		
	public void AddAction(Action<RepoInitContext> action)
		=> _postActions.Add((info, _) =>
		{
			action(info);
			return Task.CompletedTask;
		});
}