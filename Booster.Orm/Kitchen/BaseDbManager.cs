using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Booster.Log;
using Booster.Orm.Interfaces;
using Booster.Reflection;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Orm.Kitchen;

[PublicAPI]
public abstract class BaseDbManager : IDbManager
{
	protected readonly BaseOrm Orm;
	protected readonly ILog? Log;

	AsyncLazy<HashSet<string>> _allTables = AsyncLazy<HashSet<string>>.Create<BaseDbManager>(async self 
		=> new HashSet<string>(await self.GetAllTables(false).NoCtx()));

	protected Task<HashSet<string>> AllTables => _allTables.GetValue(this);
		
	AsyncLazy<HashSet<string>> _allViews = AsyncLazy<HashSet<string>>.Create<BaseDbManager>(async self 
		=> new HashSet<string>(await self.GetAllTables(true).NoCtx()));
		
	protected Task<HashSet<string>> AllViewa => _allViews.GetValue(this);
		
	protected BaseDbManager(BaseOrm orm, ILog? log)
	{
		Orm = orm;
		Log = log.Create(this);
	}
		
	public virtual async Task<bool> TableExists(string tableName, bool isView)
		=> (await (isView ? AllViewa : AllTables).NoCtx()).Contains(tableName.ToLower());

	public virtual TableUpdatePlan? GetUpdatePlan(IModelSchema schema, ITableMapping mapping)
	{
		if (mapping.AllowAutoUpdateDb())
		{
			var plan = new TableUpdatePlan();
				
			foreach (var dmSchema in mapping.UnumappedDataMemebers.ToArray())
			{
				if (dmSchema.OldNames != null && dmSchema.AllowAutoUpdateDb(DbUpdateOptions.RenameColumn))
				{
					var old = mapping.UnumappedColumns.FirstOrDefault(m => dmSchema.OldNames.Contains(m.Name, StringComparer.OrdinalIgnoreCase));
					if (old != null)
					{
						plan.ToRename.Add((old, dmSchema));
						mapping.UnumappedColumns.Remove(old);
						continue;
					}
				}

				if (Orm.LogOptions.HasFlag(OrmLogOptions.ColumnNotFound))
					Log.Warn($"warning: a column [{mapping.TableName}].[{dmSchema.DbColumnName}] was not found");

				if (dmSchema.AllowAutoUpdateDb(DbUpdateOptions.CreateColumn))
					plan.ToCreate.Add(dmSchema);
			}

			foreach (var col in mapping.UnumappedColumns.ToArray())
			{
				if (Orm.LogOptions.HasFlag(OrmLogOptions.UnmappedColumn))
					Log.Warn($"warning: a column [{mapping.TableName}].[{col.Name}] unmapped to class {mapping.ModelType.Name}");

				if (mapping.AllowAutoUpdateDb(DbUpdateOptions.DropColumn))
					plan.ToDelete.Add(col);
			}

			foreach (var column in mapping.Columns)
				if (!column.Skip && !column.Member.DbColumnDataType.EndsWithIgnoreCase(column.ColumnDataType))
				{
					if (Orm.LogOptions.HasFlag(OrmLogOptions.ColumnTypeMuissmatch))
						Log.Warn(
							$"warning: {mapping.ModelType.Name}.{column.MemberName} has type \"{column.Member.DbColumnDataType}\", but [{mapping.TableName}].[{column.ColumnName}] has type \"{column.ColumnDataType}\"");

					if (column.AllowAutoUpdateDb(DbUpdateOptions.ChangeColumnType))
						plan.ToChange.Add(column);
				}

			if (plan.IsSome)
				return plan;
		}
		return null;
	}

	public virtual async Task<bool> UpdateTable(IModelSchema schema, ITableSchema tableSchema, ITableMapping mapping, TableUpdatePlan plan)
	{
		var updated = false;

		if (plan.ToRename.Count > 0)
			try
			{
				updated |= await RenameColumns(schema, mapping, plan.ToRename).NoCtx();
			}
			catch (Exception ex)
			{
				Log.Error(ex, $"fail: {ex.Message}");
			}


		if (plan.ToCreate.Count > 0)
			try
			{
				updated |= await CreateColumns(schema, tableSchema, mapping, plan.ToCreate).NoCtx();
			}
			catch (Exception ex)
			{
				Log.Error(ex, $"fail: {ex.Message}");
			}


		if (plan.ToDelete.Count > 0)
			try
			{
				updated |= await DeleteColumns(schema, mapping, plan.ToDelete).NoCtx();
			}
			catch (Exception ex)
			{
				Log.Error(ex, $"fail: {ex.Message}");
			}

		if (plan.ToChange.Count > 0)
			try
			{
				updated |= await ChangeColumnTypes(schema, mapping, plan.ToChange).NoCtx();
			}
			catch (Exception ex)
			{
				Log.Error(ex, $"fail: {ex.Message}");
			}
			

			

		return updated;
	}

	public async Task CheckAdditionalObjects(IModelSchema schema, ITableMapping mapping)
	{
		if (!schema.IsView)
		{
			await CheckSequences(schema, mapping).NoCtx();
			await CheckConstraint(schema, mapping).NoCtx();
			await CheckIndexes(schema, mapping).NoCtx();
		}
	}
		
	protected virtual async Task<bool> RenameColumns(IModelSchema schema, ITableMapping mapping, List<(IColumnSchema, IModelDataMember)> columns)
	{
		var updated = false;
		foreach (var (column, member) in columns)
			await Log.TryInfo(Orm.LogMigration(DbUpdateOptions.RenameColumn,
				mapping.TableName,
				$"{column.Name} ({member.DataType}) => {member.DbColumnName}",
				member.Inner), async () =>
			{
				await RenameColumn(mapping, column, member).NoCtx();
				column.Name = member.DbColumnName;
				mapping.MapColumn(member, column);
				updated = true;
			}).NoCtx();

		return updated;
	}

	protected virtual async Task<bool> CreateColumns(IModelSchema schema, ITableSchema tableSchema, ITableMapping mapping, List<IModelDataMember> columns)
	{
		var updated = false;
		foreach (var col in columns)
			await Log.TryInfo(Orm.LogMigration(DbUpdateOptions.CreateColumn,
				mapping.TableName,
				$"{col.DbColumnName} {col.DbColumnDataType} ({col.DataType})",
				col.Inner), async () =>
			{
				var column = await CreateColumn(mapping, tableSchema, col).NoCtx();
				mapping.MapColumn(col, column);
				updated = true;
			}).NoCtx();

		return updated;
	}

	protected virtual async Task<bool> DeleteColumns(IModelSchema schema, ITableMapping mapping, List<IColumnSchema> columns)
	{
		var updated = false;
		foreach (var col in columns)
			await Log.TryInfo(Orm.LogMigration(DbUpdateOptions.DropColumn,
				mapping.TableName,
				col.Name), async () =>
			{
				await DeleteColumn(mapping.TableName, col, schema);
				mapping.UnumappedColumns.Remove(col);
				updated = true;
			}).NoCtx();
			
		return updated;
	}

	protected virtual async Task<bool> ChangeColumnTypes(IModelSchema schema, ITableMapping mapping, List<IColumnMapping> columns)
	{
		var updated = false;
		foreach (var column in columns)
			await Log.TryInfo(Orm.LogMigration(DbUpdateOptions.ChangeColumnType,
				mapping.TableName,
				$"{column.ColumnName} {column.ColumnDataType}=>{column.Member.DbColumnDataType}"), async () =>
			{
				await ChangeColumnType(mapping.TableName, column, schema).NoCtx();

				column.ColumnDataType = column.Member.DbColumnDataType;
				column.IsNullable = GetDefaultValue(column.ColumnDataType, column.MemberValueType) == null; //TODO: wrong nullables

				updated = true;
			}).NoCtx();

		return updated;
	}

	protected async Task<HashSet<T>?> ReadDbObjects<T>(Func<Task<T[]>> proc, string objName)
	{
		try
		{
			return new HashSet<T>(await proc().NoCtx());
		}
		catch (Exception ex)
		{
			Log.Error(ex, $"Reading {objName} fail: {ex.Message}");
			return null;
		}
	}

	protected virtual async Task CheckSequences(IModelSchema schema, ITableMapping mapping)
	{
		try
		{
			var create = mapping.DbUpdateOptions.HasFlag(DbUpdateOptions.CreateSequence);

			var existingSequences = await ReadDbObjects(() => GetExistingSequences(mapping), "sequences").NoCtx();
			if (existingSequences == null)
				return;

			foreach (var sequence in schema.Sequences)
			{
				if (sequence.Name.IsEmpty())
					sequence.Name = Orm.SqlFormatter.CreateColumnName(Orm.SqlFormatter.CreateColumnName($"{mapping.TableName}_{sequence.Target?.DbColumnName}_seq"));

				if (existingSequences.Contains(sequence))
					continue;

				Log.Warn($"Sequence {sequence.Name} not found...");

				if (create)
					await Log.TryInfo(Orm.LogMigration(
						DbUpdateOptions.CreateSequence,
						mapping.TableName,
						sequence.Name), () => CreateSequence(mapping, sequence)).NoCtx();
			}
		}
		catch (Exception ex)
		{
			Log.Error(ex, "Checking sequences failed: ");
		}
	}

	protected virtual async Task CheckConstraint(IModelSchema schema, ITableMapping mapping)
	{
		try
		{
			var create = mapping.DbUpdateOptions.HasFlag(DbUpdateOptions.CreateConstraint);
			var drop = mapping.DbUpdateOptions.HasFlag(DbUpdateOptions.DropConstraint);

			var constraints = await ReadDbObjects(() => GetExistingConstraints(mapping), "constraints").NoCtx();
			if (constraints == null)
				return;

			var toDrop = new List<IDbConstraint>();
			if (drop)
				toDrop.AddRange(constraints);

			foreach (var constraint in schema.Constraints)
			{
				if (constraint.Columns.Length <= 0)
				{
					Log.Error($"{schema.ModelType.Name} DbConstraintAttribue should specify at least one column");
					continue;
				}

				if (constraint.Name.IsEmpty())
				{
					var name = $"{mapping.TableName}_{constraint.Columns.ToString(c => c.DbColumnName, new EnumBuilder("_"))}";

					constraint.Name = Orm.SqlFormatter.CreateColumnName(
						constraint.Type switch
						{
							ConstraintType.PrimaryKey => $"pk_{name}",
							ConstraintType.Unique     => $"unique_{name}",
							_                         => $"const_{name}"
						});
				}

				if (constraints.Contains(constraint))
				{
					toDrop.Remove(constraint);
					continue;
				}

				Log.Warn($"Constraint {constraint.Name} not found...");

				if (create)
					await Log.TryInfo(Orm.LogMigration(DbUpdateOptions.CreateConstraint,
						mapping.TableName,
						$"{constraint.Name}{constraint.Columns.ToString(c => c?.DbColumnName, new EnumBuilder(",", "(", ")"))}"), ()
						=> CreateConstraint(mapping, constraint)).NoCtx();

			}

			foreach (var constraint in toDrop)
				await Log.TryInfo(Orm.LogMigration(DbUpdateOptions.DropConstraint,
					mapping.TableName,
					$"{constraint.Name}{constraint.Columns.ToString(c => c?.DbColumnName, new EnumBuilder(",", "(", ")"))}"), ()
					=> DropConstraint(constraint)).NoCtx();
		}
		catch (Exception ex)
		{
			Log.Error(ex, $"Checking constraints failed: {ex.Message}");
		}
	}

	protected virtual async Task CheckIndexes(IModelSchema schema, ITableMapping mapping)
	{
		try
		{
			var create = mapping.DbUpdateOptions.HasFlag(DbUpdateOptions.CreateIndex);
			var drop = mapping.DbUpdateOptions.HasFlag(DbUpdateOptions.DropIndex);

			if (!create && !drop)
				return;

			var indexes = await ReadDbObjects(() => GetExistingIndexes(mapping), "indexes").NoCtx();
			if (indexes == null)
				return;

			var toDrop = new List<IDbIndex>();
			if (drop)
				toDrop.AddRange(indexes);
			
			foreach (var idx in schema.Indexes)
			{
				if (idx.Columns.Length <= 0)
				{
					Log.Error($"{schema.ModelType.Name} DbIndexAttribue should specify at least one column");
					continue;
				}

				if (idx.Name.IsEmpty())
					idx.Name = Orm.SqlFormatter.CreateColumnName($"{mapping.TableName}_{idx.Columns.ToString(c => c.DbColumnName.ToLower(), new EnumBuilder("_"))}_idx".ToLower());
					
				if (indexes.Contains(idx))
				{
					toDrop.Remove(idx);
					continue;
				}

				Log.Warn($"Index {idx.Name} not found...");

				if (create)
					await Log.TryInfo(Orm.LogMigration(DbUpdateOptions.CreateIndex,
						mapping.TableName,
						$"{idx.Name}{idx.Columns.ToString(c => c?.DbColumnName, new EnumBuilder(",", "(", ")"))}"), ()
						=> CreateIndex(idx)).NoCtx();
			}

			if (drop)
				foreach (var idx in toDrop)
					await Log.TryInfo(Orm.LogMigration(DbUpdateOptions.DropIndex,
						mapping.TableName,
						idx.Name), ()
						=> DropIndex(idx)).NoCtx();
		}
		catch (Exception ex)
		{
			Log.Error(ex, $"Checking indexes failed: {ex.Message}");
		}
	}


	protected object? GetDefaultValue(IModelDataMember mdm)
		=> GetDefaultValue(mdm.DbColumnDataType, mdm.ValueType);

	protected virtual object? GetDefaultValue(string dbColumnType, TypeEx? memberValueType = null)
	{
		var type = Orm.DbTypeDescriptor.GetType(dbColumnType) ?? memberValueType;

		if (type.Is<char>())
			return 0;

		if (type.Is<DateTime>())
			return "TO_TIMESTAMP('01.01.0001 00:00:00', 'DD.MM.YYYY HH24:MI:SS')";
			
		return type.Is<Guid>() ? $"'{Guid.Empty}'" : type?.DefaultValue;
	}

	public async Task<bool> RenameTable(string oldName, string newName, bool isView)
	{
		if (await TableExists(oldName, isView).NoCtx() && !await TableExists(newName, false).NoCtx() && !await TableExists(newName, true).NoCtx())
			return await Log.TryInfo(Orm.LogMigration(DbUpdateOptions.RenameTable, oldName, newName), ()
				=> RenameTableImpl(oldName, newName)).NoCtx();

		return false;
	}

	public virtual async Task RenameTableImpl(string oldName, string newName)
	{
		using var conn = Orm.GetCurrentConnection();
		await conn.ExecuteNoQueryAsync($"alter table {oldName} rename to {newName}").NoCtx();
	}

	public virtual async Task RenameColumn(ITableMapping mapping, IColumnSchema columnSchema, IModelDataMember dmSchema)
	{
		using var conn = Orm.GetCurrentConnection();
		await conn.ExecuteNoQueryAsync(
			$"alter table {mapping.TableName} rename column {columnSchema.EscapedName} to {Orm.SqlFormatter.EscapeName(dmSchema.DbColumnName)}").NoCtx();
	}
	
	public abstract Task<string[]> GetAllTables(bool views);
	public abstract Task<bool> ReadTable(string tableName, ITableSchema schema);
	public abstract Task<ITableSchema> CreateTable(IModelSchema schema, ITableSchema tableSchema);
	public abstract Task<IColumnSchema> CreateColumn(ITableMapping mapping, ITableSchema tableSchema, IModelDataMember dmSchema);
	public abstract Task DeleteColumn(string tableName, IColumnSchema col, IModelSchema modelSchema);
	public abstract Task ChangeColumnType(string tableName, IColumnMapping col, IModelSchema modelSchema);

	public virtual QueryBuilder GetSequenceNextValQuery(IDbSequence sequence)
		=> new($"SELECT nextval({sequence.Name:@})");

	public abstract Task<bool> SequenceExist(IDbSequence sequence);

	public abstract Task OnDropSequence();

	public abstract Task<IDbIndex[]> GetExistingIndexes(ITableMapping mapping);
	public abstract Task<IDbConstraint[]> GetExistingConstraints(ITableMapping mapping);
	public abstract Task<IDbSequence[]> GetExistingSequences(ITableMapping mapping);
		
	public virtual async Task CreateSequence(ITableMapping? mapping, IDbSequence sequence)
	{
		using var conn = Orm.GetCurrentConnection();
		var start = sequence.Start > 0 ? sequence.Start : sequence.Step;
		var columnName = sequence.Target != null ? Orm.SqlFormatter.EscapeName(sequence.Target.DbColumnName) : null;
		var tableName = mapping != null ? Orm.SqlFormatter.EscapeName(mapping.TableName) : null;

		if (sequence.CheckExistingValues && columnName.IsSome() && tableName.IsSome())
		{
			var max = await conn.ExecuteScalarAsync<long>($"select max({columnName}) from {tableName}").NoCtx();
			start = Math.Max(start, max + sequence.Step);
		}

		var query = await GetCreateSequenceQuery(sequence.Name!, start, sequence.Step).NoCtx();
		await conn.ExecuteNoQueryAsync(query).NoCtx();

		if (sequence.ApplyToColumn && columnName.IsSome() && tableName.IsSome()) 
			await conn.ExecuteNoQueryAsync($"update {tableName} set {columnName}={sequence.Name}.nextval where {columnName} is null or {columnName}=0").NoCtx();
	}

	protected abstract Task<QueryBuilder> GetCreateSequenceQuery(string name, long start, long step);

	public abstract Task CreateIndex(IDbIndex index);
	public virtual async Task DropIndex(IDbIndex index)
	{
		using var conn = Orm.GetCurrentConnection();
		await conn.ExecuteNoQueryAsync($"drop index {index.Name}").NoCtx();
	}

	public virtual async Task CreateConstraint(ITableMapping mapping, IDbConstraint cnstr)
	{
		var query = new QueryBuilder();

		if (cnstr is {Custom: null} || 
			!cnstr.Custom.ContainsIgnoreCase("alter table") &&
			!cnstr.Custom.ContainsIgnoreCase("add constraint"))
			query.Append($"alter table {cnstr.TableName.ToLower()} add constraint {cnstr.Name.ToLower()} ");

		if (cnstr.Custom != null)
			query.Append($"{cnstr.Custom}");
		else
		{
			switch (cnstr.Type)
			{
			case ConstraintType.PrimaryKey:
				query.Append($"primary key (");
				break;

			case ConstraintType.Unique:
				query.Append($"unique (");
				break;
			}
			query.Append($"{cnstr.Columns.ToString(c => Orm.SqlFormatter.EscapeName(c.DbColumnName), new EnumBuilder(","))}");
			query.Append($")");
		}
			
		using (var conn = Orm.GetCurrentConnection())
			await conn.ExecuteNoQueryAsync(query).NoCtx();

		if (cnstr.Type == ConstraintType.PrimaryKey && cnstr.Columns.Length == 1)
		{
			cnstr.Columns[0].IsPk = true;

			if (!mapping.TryGetColumnByName(cnstr.Columns[0].Name, out var col))
				throw new Exception($"Column mapping not found: {cnstr.Columns[0].Name}");

			col.Column.IsPk = true;
			mapping.PkColumns.Add(col);
			mapping.IdentityColumns = mapping.PkColumns.Count > 0 ? mapping.PkColumns : mapping.ReadColumns;
		}
	}
		
	public virtual async Task DropConstraint(IDbConstraint constraint)
	{
		using var conn = Orm.GetCurrentConnection();
		await conn.ExecuteNoQueryAsync($"alter table {constraint.TableName} drop constraint {constraint.Name}").NoCtx();
	}
}