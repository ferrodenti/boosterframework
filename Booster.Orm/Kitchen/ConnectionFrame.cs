﻿using System;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
#if CONCURRENCY_DEBUG
using Booster.Orm.Debug;
#endif

#nullable enable

namespace Booster.Orm.Kitchen;

/// <summary>
/// Фрейм соответствует взятому из пула адо-соединению + открытому в нем ридеру
/// </summary>
class ConnectionFrame : IDisposable
{
	readonly BaseConnection _owner;
	public volatile bool TryReconnect;
	bool _detached;
	DbConnection? _connection;
	readonly SemaphoreSlim _reconnectLock = new(1, 1);

#if CONCURRENCY_DEBUG
		DebugDbConnection _debugDbConnection;
#endif
	public async Task<DbConnection?> GetConnectionAsync(CancellationToken token)
	{
		if (TryReconnect)
		{
			await _reconnectLock.WaitAsync(token).NoCtx();
			try
			{
				if (TryReconnect)
				{
					var newConn = await _owner.Orm.ReconnectAsync(_connection, token).NoCtx();
					SetConnection(newConn);
					TryReconnect = false;
				}
			}
			finally
			{
				_reconnectLock.Release();
			}
		}

		return _connection;
	}

	public DbConnection? GetConnection()
	{
		if (TryReconnect)
		{
			_reconnectLock.Wait();
			try
			{
				if (TryReconnect)
				{
					var newConn = _owner.Orm.Reconnect(_connection);
					SetConnection(newConn);
					TryReconnect = false;
				}
			}
			finally
			{
				_reconnectLock.Release();
			}
		}

		return _connection;
	}

	void SetConnection(DbConnection? value)
	{
		_connection = value;
#if CONCURRENCY_DEBUG
			_debugDbConnection = value as DebugDbConnection;
			_debugDbConnection?.Lended();
#endif
	}

	public PrefetchingDataReader? Reader;

	public ConnectionFrame(BaseConnection owner, DbConnection? connection)
	{
		_owner = owner;
		SetConnection(connection);
	}

	public DbDataReader CreateReader(DbDataReader inner, DbCommand command)
		=> Reader = new PrefetchingDataReader(inner, () =>
		{
			Reader = null;
			command.Dispose();

			if (_detached)
				DisposeConnection();
		});


	public void Detach()
	{
#if CONCURRENCY_DEBUG
			_debugDbConnection?.Log("Detached");
#endif
		_detached = true;

		if (Reader == null)
			Dispose();
		else
			Reader.IsDetached = true;
	}

	public void Dispose()
	{
		if (Reader != null)
		{
			Reader.Dispose();
			Reader = null;
		}

		DisposeConnection();
	}

	void DisposeConnection()
	{
#if CONCURRENCY_DEBUG
			_debugDbConnection?.Released();
			_debugDbConnection = null;
#endif
		if (_connection != null)
		{
			_owner.Orm.ReleaseConnection(_connection);
			_connection = null;
		}
	}
}