using System.Threading;
using System.Threading.Tasks;
using Booster.Orm.Interfaces;
using Booster.Reflection;

namespace Booster.Orm.Kitchen;

public class SqlRepository<TId> : SqlRepository, IRepository<TId>
{
	public SqlRepository(BaseOrm orm, ITableMapping mapping, IRepoQueries queries) : base(orm, mapping, queries) { }

	public virtual IEntity<TId> LoadById(TId id) 
		=> (IEntity<TId>)Adapter.LoadByPks(new object[]{id});

	public virtual async Task<IEntity<TId>> LoadByIdAsync(TId id, CancellationToken token = default) 
		=> (IEntity<TId>)await Adapter.LoadByPksAsync(new object[]{id}, token).NoCtx();

	public void Save(IEntity<TId> entity)
	{
		if (Equals(entity.Id, default(TId)))
			Insert(entity);
		else
			Update(entity);
	}


	public Task SaveAsync(IEntity<TId> entity, CancellationToken token = default)
		=> Equals(entity.Id, default(TId)) 
			? InsertAsync(entity, token) 
			: UpdateAsync(entity, token);

	public override string ToString()
	{
		var bs = base.ToString();
		return $"{bs.Substring(0, 4)}<{TypeEx.Get<TId>()}>{bs.Substring(4)}";
	}
}