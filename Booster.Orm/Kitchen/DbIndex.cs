using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Booster.Orm.Interfaces;

namespace Booster.Orm.Kitchen;

public class DbIndex : IDbIndex
{
	readonly IModelSchema _model;
	string _tableName;
	public string TableName
	{
		get => _tableName ?? _model.TableName;
		set => _tableName = value;
	}

	public IModelDataMember[] Columns { get; set; }
	public IndexType Type { get; set; }
	public string Custom { get; }
	public string Name { get; set; }
	public bool Cluster { get; set; }
	public bool Unique { get; set; }

	public DbIndex(IModelSchema model, DbIndexAttribute attr, IModelDataMember column)
	{
		_model = model;
		Type = attr.IndexType;
		Custom = attr.IndexCustom;
		Name = attr.IndexName;
		Cluster = attr.IndexCluster;
		Unique = attr.UniqueIndex;

		var columns = attr.IndexColumns != null
			? model.Where(m => attr.IndexColumns.Any(c => c.EqualsIgnoreCase(m.Name)))
			: Enumerable.Empty<IModelDataMember>();

		if (column != null)
			columns = columns.AppendBefore(column);

		Columns = columns.Distinct().ToArray();
	}

	public DbIndex(string tableName, string name, IndexType type, bool unique, IEnumerable<IModelDataMember> columns = null)
	{
		TableName = tableName;
		Name = name;
		Type = type;
		Unique = unique;
		Columns = columns?.ToArray();
	}

	public override string ToString()
	{
		var bld = new EnumBuilder(", ", ", ");
		if(Cluster)
			bld.Append("Cluster=true");
		if(Custom.IsSome())
			bld.Append($"Custom=\"{Custom}\"");
			
		return $"Index \"{Name}\": {TableName}({Columns?.ToString(new EnumBuilder(", "))}) Type={Type}{bld}";
	}
		
	[SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
	public override int GetHashCode()
		=> new HashCode(TableName, Type, Unique);

	public override bool Equals(object obj)
	{
		if (obj is not DbIndex b)
			return false;

		return TableName == b.TableName &&
			   Type == b.Type &&
			   ((Columns != null &&
				 b.Columns != null &&
				 Columns.HasSameElements(b.Columns)) ||
				Name.EqualsIgnoreCase(b.Name));
	}
}