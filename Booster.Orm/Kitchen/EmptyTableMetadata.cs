using Booster.Orm.Interfaces;

namespace Booster.Orm.Kitchen;

public class EmptyTableMetadata : ITableMetadata
{
	public string this[string key]
	{
		get => null;
		set { }
	}

	public int Version { get; set; }

	public TValue Get<TValue>(string key)
		=> default;

	public void Set(string key, object value)
	{
	}
}