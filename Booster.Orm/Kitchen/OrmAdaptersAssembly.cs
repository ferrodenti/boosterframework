using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Booster.Log;
using Booster.Orm.Attributes;
using Booster.Orm.Interfaces;
using Booster.Reflection;

#nullable enable

namespace Booster.Orm.Kitchen;

public class OrmAdaptersAssembly
{
	readonly SyncLazy<Assembly?> _assembly = SyncLazy<Assembly?>.Create<OrmAdaptersAssembly>(self
		=> Try.OrDefault(() =>
		{
			var result = Assembly.LoadFrom(self.Path!);
			var attr = (OrmAdaptersAssemblyAttribute?) result.GetCustomAttributes(typeof(OrmAdaptersAssemblyAttribute)).FirstOrDefault();
			if (attr == null)
				return null;

			self.InstanceContext = attr.InstanceContext;
			self.BoosterAssemblyHashes = attr.BoosterAssemblyHashes;

			self._log.Info($"Using orm adapters assembly: {System.IO.Path.GetFileName(self.Path)}");

			return result;
		}));
		
	public Assembly? Assembly
	{
		get => Path.With(_ => _assembly.GetValue(this));
		set
		{
			_assembly.Value = value;
			_adapters.Reset();
		}
	}

	readonly ILog? _log;
	public string? Path { get; }
	public string? InstanceContext { get; set; }
	public int[]? BoosterAssemblyHashes { get; set; }

	public bool Used { get; set; }
			
	public OrmAdaptersAssembly(string? path, ILog? log)
	{
		Path = path;
		_log = log.Create(this);
	}

	readonly SyncLazy<Dictionary<Type, (Type, OrmAdapterAttribute)>> _adapters
		= SyncLazy<Dictionary<Type, (Type, OrmAdapterAttribute)>>.Create<OrmAdaptersAssembly>(self =>
		{
			var result = new Dictionary<Type, (Type, OrmAdapterAttribute)>();
			var assy = self.Assembly;
			if (assy != null)
				foreach (TypeEx type in assy.GetTypes())
					if (type.HasInterface<IRepoAdapter>())
					{
						var attr = type.FindAttribute<OrmAdapterAttribute>();
						if (attr != null)
							result[attr.EntityType] = (type, attr);
					}

			return result;
		});


	public bool FindAdapter(IOrm orm, IRepoInitTask task, out IRepoAdapter adapter)
		=> Try.OrDefault(() =>
		{
			var adapters = _adapters.GetValue(this);
			if (adapters.TryGetValue(task.EntityType, out var pair))
			{
				var (adapterType, attribute) = pair;
				if (attribute.Mold == task.Repository.TableMapping.Mold)
				{
					Used = true;
					return (IRepoAdapter) Activator.CreateInstance(adapterType, orm, task.Repository);
				}
			}

			return null;
		}).Is(out adapter);
}