using System;
using System.Collections.Generic;
using System.Data.Common;
using Booster.Interfaces;
using Booster.Orm.Interfaces;
using Booster.Reflection;

#nullable enable

namespace Booster.Orm.Kitchen;

public abstract class BaseDbTypeDescriptor : IDbTypeDescriptor
{
	protected readonly IOrm Orm;

	protected BaseDbTypeDescriptor(IOrm orm)
		=> Orm = orm;

	public virtual bool IsInteger(TypeEx? type)
		=> type.Is<short>() || type.Is<int>() || type .Is<long>() ||
		   type.Is<ushort>() || type.Is<uint>() || type.Is<ulong>();

	public virtual bool IsFloat(TypeEx? type)
		=> type.Is<float>() || type.Is<double>() || type.Is<decimal>();

	public virtual bool IsString(TypeEx? type)
		=> type.Is<string>();
	public virtual bool IsDateTime(TypeEx? type)
		=> type.Is<DateTime>();
	public virtual bool IsGuid(TypeEx? type)
		=> type.Is<Guid>();
	public virtual bool IsBlob(TypeEx? type)
		=> type.Is<byte[]>();
	public virtual bool IsBoolean(TypeEx? type)
		=> type.Is<bool>();
	public virtual bool IsVariant(TypeEx? type)
		=> false;
	public virtual bool IsInteger(string dbType)
		=> IsInteger(GetType(dbType));
	public virtual bool IsFloat(string dbType)
		=> IsFloat(GetType(dbType));
		
	public virtual bool IsString(string dbType)
		=> IsString(GetType(dbType));
	public virtual bool IsDateTime(string dbType)
		=> IsDateTime(GetType(dbType));
	public virtual bool IsBoolean(string dbType)
		=> IsBoolean(GetType(dbType));
	public virtual bool IsGuid(string dbType)
		=> IsGuid(GetType(dbType));
	public virtual bool IsBlob(string dbType)
		=> IsBlob(GetType(dbType));
	public virtual bool IsVariant(string dbType)
		=> IsVariant(GetType(dbType));

	public bool IsNullable(TypeEx type)
	{
		if (type.Is<DateTime>())
			return true;

		return !type.IsValueType;
	}
		
	protected static readonly Lazy<Dictionary<TypeEx,Method>> DataReaderMethods = new(() =>
	{
		var res = new Dictionary<TypeEx,Method>();

		foreach (var mi in TypeEx.Get<DbDataReader>().FindMethods(new ReflectionFilter
				 {
					 NameWildcards = "Get*",
					 Parameters = new[] {typeof(int)},
				 }))
			if (mi.ReturnType != null)
				res[mi.ReturnType] = mi;

		return res;
	}); 

	public virtual Method GetDataReaderMethod(TypeEx type)
		=> DataReaderMethods.Value.TryGetValue(type, out var mi) 
			? mi 
			: DataReaderMethods.Value[typeof (object)];

	public virtual Method GetDataReaderMethod(string dbType)
	{
		var type = GetType(dbType) ?? throw new Exception($"Unable to map a column of type {dbType} to any known type");
		return GetDataReaderMethod(type);
	}

	public TypeEx? GetType(string type)
	{
		type = ParseType(type, out var precision, out var scale);
		return GetType(type, precision, scale);
	}
		
	public abstract TypeEx? GetType(string? dbType, int length, int scale);

	protected abstract string? GetTypeInt(TypeEx type);

	public virtual string? GetDbTypeFor(TypeEx type)
	{
		while (true)
		{
			if (type.IsNullable)
			{
				type = type.GenericArguments[0];
				continue;
			}

			return GetTypeInt(GetValueType(type));
		}
	}

	public virtual TypeEx GetValueType(TypeEx type)
	{
		var convIfc = type.FindInterface(typeof(IConvertiableFrom<>));
		if (convIfc != null)
			return convIfc.GenericArguments[0];

		if (TypeEx.Get<IIntValue>().IsAssignableFrom(type))
			return typeof (int);

		if (TypeEx.Get<IStringValue>().IsAssignableFrom(type))
			return typeof (string);

		if (type.FullName == "System.Drawing.Color")
			return typeof (int);

		return type;
	}

	public virtual string? ParseBaseType(string? type)
	{
		if (type != null)
		{
			var i = type.IndexOf('(');
			if (i > 0)
				type = type.Substring(0, i).TrimEnd();

			return type.ToLower();
		}
		return null;
	}

	public virtual string ParseType(string type, out int precision, out int scale)
	{
		precision = 0;
		scale = 0;


		var i = type.IndexOf('(');
		if (i > 0)
		{
			var ps = type.Substring(i + 1);
			type = type.Substring(0, i).TrimEnd();

			i = ps.IndexOf(')');
			if (i > 0)
			{
				ps = ps.Substring(0, i);
				var v = ps.Split(',');
				if (v.Length > 0 && int.TryParse(v[0], out i))
					precision = i;

				if (v.Length > 1 && int.TryParse(v[1], out i))
					scale = i;
			}
		}

		return type.ToLower();
	}
}