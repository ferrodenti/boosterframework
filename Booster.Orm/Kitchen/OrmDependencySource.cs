﻿using System;
using Booster.DI;
using Booster.Interfaces;
using Booster.Orm.Interfaces;
using Booster.Reflection;

[assembly: AssemblyInstancesRegistration]

namespace Booster.Orm.Kitchen;

[InstanceFactory(typeof(IDependencySource), true)]
public class OrmDependencySource : IDependencySource
{
	public IWeakEventHandle Add(object target, object entity, Action<object, EventArgs> callback)
	{
		if (entity is not IEntityId)
			throw new InvalidOperationException("Expected IEntityId entity");
			
		return Add(TypeEx.Of(entity), target,
			(obj, e) =>
			{
				if (obj == null || Equals(((IEntityId) entity).Id, ((IEntityId) obj).Id))
					callback(obj, e);
			});
	}

	public IWeakEventHandle Add(object target, TypeEx entityType, Action<object, EventArgs> callback)
	{
		using (entityType.FindAttribute<DbInstanceContextAttribute>()?.ContextName.With(InstanceFactory.PushContext))
		{
			var repo = Instance<IOrm>.Value.GetRepo(entityType, true)!;

			return WeakEvents.Subscribe<IRepository, object, RepositoryChangedEventArgs>(
				$"{repo.EntityType.FullName}=> DependentLazy<T>", repo, target,
				(s, eh) => { s.Changed += eh; },
				(s, eh) => { s.Changed -= eh; },
				(_, _, args) => callback(args.Entity, args));
		}
	}
}