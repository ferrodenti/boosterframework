﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;


using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Booster.AsyncLinq;
using Booster.DI;
using Booster.Log;
using Booster.Orm.Interfaces;
using JetBrains.Annotations;
#if NETSTANDARD2_1
using System.Runtime.CompilerServices;
#endif
#if CONCURRENCY_DEBUG
using Booster.Debug;
using Booster.Orm.Debug;
#endif

#nullable enable

namespace Booster.Orm.Kitchen;

[InstanceFactory(typeof (IConnection))]
public class BaseConnection : IConnection //-V3073
{
	readonly Action? _releaser;
	readonly DataReadersProtection _dataReadersProtection;
	readonly TraceSqlOptions _traceSql;
	readonly ILog? _log;
	readonly int _no;
	internal readonly BaseOrm Orm;
	IOrm IConnection.Orm => Orm;
	ITransaction? _transaction;
	volatile int _entries;

	static volatile int _cnt;
	
	volatile ConnectionFrame? _currentFrame;

	protected readonly IConnectionSettings ConnectionSettings;
	
#if CONCURRENCY_DEBUG
	readonly StackTraceLog _stackTraceLog;
#endif
	[Conditional("CONCURRENCY_DEBUG")]
	[SuppressMessage("ReSharper", "MemberCanBeMadeStatic.Global")]
	public void Log(string message)
	{
#if CONCURRENCY_DEBUG			
		_stackTraceLog.Log(message);
#endif
	}

	async Task<ConnectionFrame> GetCurrentFrameAsync(CancellationToken token = default)
	{
		if (_currentFrame == null)
		{
			var conn = await Orm.GetConnectionAsync(token).NoCtx();
			_currentFrame = new ConnectionFrame(this, conn);
		}
		return _currentFrame;
	}

	ConnectionFrame GetCurrentFrame()
	{
		if (_currentFrame == null)
		{
			var conn = Orm.GetConnection();
			_currentFrame = new ConnectionFrame(this, conn);
		}
		return _currentFrame;
	}

	async Task<DbConnection> GetInnerAsync(CancellationToken token = default)
	{
		while (true)
		{
			var frame = await GetCurrentFrameAsync(token).NoCtx();
			var result = await frame.GetConnectionAsync(token).NoCtx();
			if (result?.State == ConnectionState.Open)
				return result;
				
			_log.Debug($"Ctx with no connection: {result?.State}, owner: {this}\n{new StackTrace()}");
			var conn = await Orm.GetConnectionAsync(token).NoCtx();
			_currentFrame = new ConnectionFrame(this, conn);
		}
	}

	DbConnection GetInner()
	{
		while (true)
		{
			var frame = GetCurrentFrame();
			var result = frame.GetConnection();
			if (result?.State == ConnectionState.Open)
				return result;
				
			_log.Debug($"Ctx with no connection: {result?.State}, owner: {this}\n{new StackTrace()}");
			var conn = Orm.GetConnection();
			_currentFrame = new ConnectionFrame(this, conn);
		}
	}

	public BaseConnection(IConnectionSettings connectionSettings, Action releaser, BaseOrm orm, ILog? log)
	{
		ConnectionSettings = connectionSettings;
		_dataReadersProtection = ConnectionSettings.ProtectDataReaders;
		_traceSql = ConnectionSettings.TraceSql;

		Orm = orm;
		_releaser = releaser;
		_log = log.Create<BaseConnection>();
		_no = Interlocked.Increment(ref _cnt);
#if CONCURRENCY_DEBUG			
		_stackTraceLog = new StackTraceLog(_no);
#endif
	}

	public override string ToString()
		=> $"Conn {_no}";

	public virtual void Dispose()
	{
		Log($"Dispose, entries={_entries}");
		if (Interlocked.Decrement(ref _entries) == 0)
		{
			var frame = _currentFrame;
			_currentFrame = null;
			frame?.Dispose();
			_releaser?.Invoke();
		}
	}

	public void Entry()
	{
		Log($"Entry, entries={_entries}");
		Interlocked.Increment(ref _entries);
	}

	public ITransaction BeginTransaction()
	{
		_transaction ??= new Transaction(async token =>
		{
			var inner = await GetInnerAsync(token).NoCtx();
#if NETCOREAPP || NETSTANDARD2_1
			return await inner.BeginTransactionAsync(token).NoCtx();
#else
			return inner.BeginTransaction();
#endif
		}, () =>
		{
			var inner = GetInner();
			return inner.BeginTransaction();
		}, _ => _transaction = null);

		_transaction.Entry();

		return _transaction;
	}

	/// <summary>
	/// Делаем reader.Prefetch() только если существует транзакция (надо сохранить соединение с бд, чтобы новая команда была в той же транзакции).
	/// Если транзакции нет, то просто создаем новое соединение а старое закроется по событию Close ридера
	/// </summary>
	async Task<PrefetchingDataReader?> ChangeFrameOrGetReaderToPrefetchAsync(CancellationToken token)
	{
		var frame = await GetCurrentFrameAsync(token).NoCtx();
		var reader = frame.Reader;
		if (reader == null)
			return null;

		if (_transaction == null && _dataReadersProtection.HasFlag(DataReadersProtection.NewConnection))
		{	
			var conn = await Orm.TryGetConnectionAsync(token).NoCtx(); // не удается получить соединение только если пул соединений забит
			if (conn != null)
			{
				frame.Detach();
				_currentFrame = new ConnectionFrame(this, conn);
				return null;
			} // В этом случае дочитываем открытый ридер
		}

		return _dataReadersProtection.HasFlag(DataReadersProtection.Prefetch) ? reader : null;
	}

	PrefetchingDataReader? ChangeFrameOrGetReaderToPrefetch()
	{
		var frame = GetCurrentFrame();
		var reader = frame.Reader;
		if (reader == null)
			return null;

		if (_transaction == null && _dataReadersProtection.HasFlag(DataReadersProtection.NewConnection))
		{	
			var conn = Orm.TryGetConnection(); // не удается получить соединение только если пул соединений забит
			if (conn != null)
			{
				frame.Detach();
				_currentFrame = new ConnectionFrame(this, conn);
				return null;
			} // В этом случае дочитываем открытый ридер
		}

		return _dataReadersProtection.HasFlag(DataReadersProtection.Prefetch) ? reader : null;
	}

	protected void Protect()
		=> ChangeFrameOrGetReaderToPrefetch()?.Prefetch();

	protected async Task ProtectAsync(CancellationToken token)
	{
		var reader = await ChangeFrameOrGetReaderToPrefetchAsync(token).NoCtx();
		if (reader != null)
			await reader.PrefetchAsync(token).NoCtx();
	}

	#region SQL Tracing
	const int _traceParamsMaxLength = 100;

	protected void TraceCommand(string command, params DbParameter[]? parameters)
	{
		var txt = new StringBuilder(DateTime.Now.ToString("mm:ss "));
		txt.AppendLine(command);
		if (parameters?.Length > 0)
		{
			var formatter = Orm.SqlFormatter;
			var str = parameters.Length > 1 ? new EnumBuilder(",\r\n", "{\r\n", "\r\n}") : new EnumBuilder(", ", "{", " }");
			var tab = parameters.Length > 1 ? "\t" : " ";

			foreach (var p in parameters)
			{
				var val = formatter.FormatConst(p.Value);
				if (val.Length > _traceParamsMaxLength)
				{
					var i = val.Length - 1;
					val = $"{val.Substring(0, _traceParamsMaxLength)}...{val[i]}";
				}

				str.Append($"{tab}{p.ParameterName}={val}");
			}
			txt.AppendLine(str);

			if (_traceSql == TraceSqlOptions.Inline)
				try
				{
					//TODO: придумать как тут использовать новый QueryBuilder
					txt.AppendLine($"INLINED: {InlineQuery(formatter, command, parameters)}");
				}
				catch (Exception ex)
				{
					txt.AppendLine($"Failed to inline query: {ex.Message}");
				}
		}

		_log.Trace(txt.ToString());
	}
	
	public static string InlineQuery(ISqlFormatter sqlFormatter, string query, DbParameter[]? parameters)
	{
		var prefix = sqlFormatter.ParamPrefix;
		
		if(parameters != null)
			for(var i=0; i<parameters.Length; i++)
			{
				var p = parameters[i];
				var nm = p.ParameterName;
				if (nm.IsEmpty())
					nm = $"{prefix}{i}";
				else if (!nm.StartsWith(prefix))
					nm = $"{prefix}{nm}";

				query = ReplaceIdentifier(query, nm, sqlFormatter.FormatConst(p.Value));
			}

		return query;
	}
	public static string ReplaceIdentifier(string query, string oldName, string newName)
	{
		var bld = new StringBuilder();

		var j = 0;
		while (j < query.Length)
		{
			var i = query.IndexOf(oldName, j);
			if (i < 0)
			{
				bld.Append(query.Substring(j));
				break;
			}

			var k = i + oldName.Length;
			if (k < query.Length && char.IsLetterOrDigit(query[k]))
			{
				++k;
				bld.Append(query.Substring(j, k - j));
				j = k;
				continue;
			}

			if (i > j)
				bld.Append(query.Substring(j, i - j));

			bld.Append(newName);
			j = i + oldName.Length;
		}

		return bld.ToString();
	}

	#endregion

	bool IsConnectionBroken(IEnumerable<Exception> exceptions)
		=> exceptions.Any(IsConnectionBroken);
	
	protected virtual bool IsConnectionBroken(Exception ex) 
		=> ex.First() is IOException;

	#region RetryEnvelope

	public static void AppendCommandToException(DbCommand command, Exception ex) //TODO: Объединить это с SqlTracing и переместить в отдельный класс
	{
		var bld = new EnumBuilder("\n", closer: "\n");

		bld.Append($"Command: {command.CommandText}");

		foreach (var obj in command.Parameters)
			if (obj is DbParameter parameter)
				bld.Append($"@{parameter.ParameterName}={parameter.Value}");
			else
				bld.Append(obj);

		ex.Data["DbCommand"] = bld.ToString();
	}

	protected virtual T RetryEnvelope<T>(
		DbCommand cmd, 
		bool disposeCommand, 
		[InstantHandle] Func<DbCommand, T> action)
	{
		var command = cmd;
		try
		{
			return action(command);
		}
		catch (Exception ex)
		{
			var inner = GetInner();
#if CONCURRENCY_DEBUG
			var debugConn = inner as DebugDbConnection;
			debugConn?.Log($"Error: {ex}", true);
#endif
			if (IsConnectionBroken(ex.Expand(false)) || inner.State != ConnectionState.Open)
			{
				_log.Warn($"Connection (no={_no}, state={inner.State}) seems broken: {ex.GetType().Name} {ex.Message}. Attempting to repeat the command with new a connection");
				var frame = GetCurrentFrame();
				frame.TryReconnect = true;
				DbCommand command2;
#if CONCURRENCY_DEBUG
				if (debugConn != null)
				{
					var oldCmd = debugConn.Command;
					debugConn.Command = null;
					command2 = CloneCommand(command);
					debugConn.Command = oldCmd;
					command.Dispose();
					debugConn.Command = (DebugDbCommand)command2;
				}
				else
#endif
				{
					command2 = CloneCommand(command);
					command.Dispose();
				}
				command = null;
				try
				{
					return action(command2);
				}
				catch (Exception ex2)
				{
					AppendCommandToException(command2, ex2);
					command2.Dispose();
					throw;
				}
			}

			disposeCommand = true;
			AppendCommandToException(command, ex);
			throw;
		}
		finally
		{
			if (disposeCommand)
				command?.Dispose();
		}
	}


	protected virtual async Task<T> RetryEnvelopeAsync<T>(
		DbCommand cmd, bool
			disposeCommand,
		[InstantHandle] Func<DbCommand, Task<T>> action,
		CancellationToken token)
	{
		var command = cmd;
		try
		{
			return await action(command).NoCtx();
		}
		catch (Exception ex)
		{
			var inner = await GetInnerAsync(token).NoCtx();
#if CONCURRENCY_DEBUG
			var debugConn = inner as DebugDbConnection;
			debugConn?.Log($"Error: {ex}", true);
#endif
			if (IsConnectionBroken(ex.Expand(false)) || inner.State != ConnectionState.Open)
			{
				_log.Warn($"Connection (no={_no}, state={inner.State}) seems broken: {ex.GetType().Name} {ex.Message}. Attempting to repeat the command with new a connection");
				var frame = await GetCurrentFrameAsync(token).NoCtx();
				frame.TryReconnect = true;

				DbCommand command2;
#if CONCURRENCY_DEBUG
				if (debugConn != null)
				{
					var oldCmd = debugConn.Command;
					debugConn.Command = null;
					command2 = CloneCommand(command);
					debugConn.Command = oldCmd;
					command.Dispose();
					debugConn.Command = (DebugDbCommand)command2;
				}
				else
#endif
				{
					command2 = await CloneCommandAsync(command, token).NoCtx();
					command.Dispose();
				}
				command = null;
				try
				{
					return await action(command2).NoCtx();
				}
				catch (Exception ex2)
				{
					AppendCommandToException(command2, ex2);
					command2.Dispose();
					throw;
				}
			}

			disposeCommand = true;
			AppendCommandToException(command, ex);
			throw;
		}
		finally
		{
			if (disposeCommand)
				command?.Dispose();
		}
	}

	#endregion

	#region CreateCommand

	public virtual async Task<DbCommand> CreateCommandAsync(string command, DbParameter[]? parameters, CancellationToken token = default)
	{
		if (_traceSql != TraceSqlOptions.False)
			TraceCommand(command, parameters);

		var inner = await GetInnerAsync(token).NoCtx();
		var cmd = inner.CreateCommand();
		cmd.CommandText = command;
		if (_transaction != null)
			cmd.Transaction = await _transaction.GetInnerAsync(token).NoCtx();

		if (parameters != null)
			cmd.Parameters.AddRange(parameters);

		return cmd;
	}

	public virtual DbCommand CreateCommand(string command, DbParameter[]? parameters)
	{
		if (_traceSql != TraceSqlOptions.False)
			TraceCommand(command, parameters);

		var inner = GetInner();
		var cmd = inner.CreateCommand();
		cmd.CommandText = command;
		if (_transaction != null)
			cmd.Transaction = _transaction.GetInner();

		if (parameters != null)
			cmd.Parameters.AddRange(parameters);

		return cmd;
	}

	public Task<DbCommand> CreateCommandAsync(QueryBuilder queryBuilder, CancellationToken token = default)
		=> CreateCommandAsync(queryBuilder.Query, queryBuilder.Parameters, token);
	
	public Task<DbCommand> CreateCommandAsync(string command, object? parameters, CancellationToken token = default) 
		=> CreateCommandAsync(command, Orm.CreateParameters(parameters), token);

	public DbCommand CreateCommand(QueryBuilder queryBuilder) 
		=> CreateCommand(queryBuilder.Query, queryBuilder.Parameters);
	
	public DbCommand CreateCommand(string command, object? parameters) 
		=> CreateCommand(command, Orm.CreateParameters(parameters));

	public Task<DbCommand> CreateCommandAsync(FormattableString formattable, CancellationToken token = default)
		=> CreateCommandAsync(new QueryBuilder(Orm, formattable), token);

	public DbCommand CreateCommand(FormattableString formattable)
		=> CreateCommand(new QueryBuilder(Orm, formattable));

	public Task<DbCommand> CloneCommandAsync(DbCommand command, CancellationToken token = default)
	{
		var newParams = CloneParameters(command.Parameters);

		return CreateCommandAsync(command.CommandText, newParams, token);
	}

	DbParameter[] CloneParameters(DbParameterCollection parameters)
	{
		var len = parameters.Count;
		var newParams = new DbParameter[len];
		
		for (var i = 0; i < len; ++i)
			newParams[i] = CloneParameter(parameters[i]);
		
		return newParams;
	}

	DbParameter CloneParameter(IDataParameter p)
	{
		// ReSharper disable once SuspiciousTypeConversion.Global
		if (p is ICloneable clone)
			return (DbParameter)clone.Clone();

		return Orm.CreateParameter(p.ParameterName, p.Value);
	}

	public DbCommand CloneCommand(DbCommand command)
	{
		var newParams = CloneParameters(command.Parameters);

		return CreateCommand(command.CommandText, newParams);
	}

	#endregion

	#region Ping

	protected virtual FormattableString GetPingCommand()
		=> $"select 1;";

	public void Ping()
	{
		try
		{
			ExecuteNoQuery(GetPingCommand());
		}
		catch (Exception ex)
		{
			throw ex.GetBaseException();
		}
	}

	public async Task PingAsync(CancellationToken token = default)
	{
		try
		{
			await ExecuteNoQueryAsync(GetPingCommand(), token).NoCtx();
		}
		catch (Exception ex)
		{
			throw ex.GetBaseException();
		}
	}

	#endregion
	#region ExecuteXXX(DbParameter[])
	#region Sync
	public virtual int ExecuteNoQuery(string command, DbParameter[]? parameters)
	{
		Protect();

		var cmd = CreateCommand(command, parameters);
		return RetryEnvelope(cmd, true, c => c.ExecuteNonQuery());
	}

	public virtual DbDataReader ExecuteReader(string command, DbParameter[]? parameters)
	{
		Protect();

		var cmd1 = CreateCommand(command, parameters);
		return RetryEnvelope(cmd1, false, cmd2 =>
		{
			var frame = GetCurrentFrame();
			return frame.CreateReader(cmd2.ExecuteReader(), cmd2);
		});
	}

	public virtual object? ExecuteScalar(string command, DbParameter[]? parameters)
	{
		Protect();

		var cmd = CreateCommand(command, parameters);
		return RetryEnvelope(cmd, true, c => c.ExecuteScalar());
	}

	public virtual T? ExecuteScalar<T>(string command, DbParameter[]? parameters)
		=> ExecuteScalar<T?>(command, parameters, default);
	
	public virtual T ExecuteScalar<T>(string command, DbParameter[]? parameters, T @default)
	{
		var res = ExecuteScalar(command, parameters);
		if (res == null || res == DBNull.Value)
			return @default;

		if (res.GetType() != typeof (T))
			return (T) Convert.ChangeType(res, typeof (T));

		return (T) res;
	}

	public IEnumerable<T?> ExecuteEnumerable<T>(string command, DbParameter[]? parameters)
	{
		using var reader = ExecuteReader(command, parameters);
		while (reader.Read())
			yield return reader.IsDBNull(0)
				? default
				: (T)Convert.ChangeType(reader.GetValue(0), typeof(T));
	}

	public IEnumerable<T> ExecuteTuples<T>(string command, DbParameter[]? parameters)
	{
		using var reader = ExecuteReader(command, parameters);
		while (reader.Read())
			yield return TupleReadHelper<T>.ReadTuple(reader);
	}


	public T ExecuteTuple<T>(string command, DbParameter[]? parameters)
		=> ExecuteTuples<T>(command, parameters).FirstOrDefault();

	#endregion
	#region Async

	/// <summary>
	/// Используется в AsyncDbReader, поэтому здесь команду мы освобождаем только при ошибке
	/// </summary>
	/// <param name="commandConstructor"></param>
	/// <param name="token"></param>
	/// <returns></returns>
	public async Task<DbDataReader> ExecuteReaderAsync(Func<Task<DbCommand>> commandConstructor, CancellationToken token = default)
	{
		await ProtectAsync(token).NoCtx();
		
		var command = await commandConstructor().NoCtx();
		return await RetryEnvelopeAsync(command, false, async cmd =>
		{
			var frame = await GetCurrentFrameAsync(token).NoCtx();
			return frame.CreateReader(await cmd.ExecuteReaderAsync(token).NoCtx(), cmd);
		}, token).NoCtx();

	}
	
	public async Task<DbDataReader> ExecuteReaderAsync(string command, DbParameter[]? parameters, CancellationToken token = default)
	{
		await ProtectAsync(token).NoCtx();

		var cmd1 = await CreateCommandAsync(command, parameters, token).NoCtx();
		return await RetryEnvelopeAsync(cmd1, false, async cmd =>
		{
			var frame = await GetCurrentFrameAsync(token).NoCtx();
			return frame.CreateReader(await cmd.ExecuteReaderAsync(token).NoCtx(), cmd);
		}, token).NoCtx();
	}

	public async Task<int> ExecuteNoQueryAsync(string command, DbParameter[]? parameters, CancellationToken token = default)
	{
		await ProtectAsync(token).NoCtx();

		var cmd = await CreateCommandAsync(command, parameters, token).NoCtx();
		return await RetryEnvelopeAsync(cmd, true, c => c.ExecuteNonQueryAsync(token), token).NoCtx();
	}

	public async Task<object?> ExecuteScalarAsync(string command, DbParameter[]? parameters, CancellationToken token = default)
	{
		await ProtectAsync(token).NoCtx();

		var cmd = await CreateCommandAsync(command, parameters, token).NoCtx();
		return await RetryEnvelopeAsync(cmd, true, c => c.ExecuteScalarAsync(token), token).NoCtx();
	}

	public async Task<T?> ExecuteScalarAsync<T>(string command, DbParameter[]? parameters, CancellationToken token = default)
	{
		var res = await ExecuteScalarAsync(command, parameters, token).NoCtx();
		if (res == null || res == DBNull.Value)
			return default;

		if (res.GetType() != typeof(T))
			res = Convert.ChangeType(res, typeof(T));

		if (res is T tresult)
			return tresult;

		return default;
	}
	public async Task<T?> ExecuteScalarAsync<T>(string command, DbParameter[]? parameters, T @default, CancellationToken token = default)
	{
		var res = await ExecuteScalarAsync(command, parameters, token).NoCtx();
		if (res == null || res == DBNull.Value)
			return @default;

		if (res.GetType() != typeof(T))
			res = Convert.ChangeType(res, typeof(T));

		if (res is T tresult)
			return tresult;

		return @default;
	}

#if !NETSTANDARD2_1
	public IAsyncEnumerable<T> ExecuteEnumerableAsync<T>(string command, DbParameter[]? parameters, CancellationToken token = default)
		=> new AsyncDbReader<T>(
			this,
			conn => conn.CreateCommandAsync(command, parameters, token),
			r => r.IsDBNull(0)
				? default
				: (T) Convert.ChangeType(r.GetValue(0), typeof(T)));


	public IAsyncEnumerable<T> ExecuteTuplesAsync<T>(string command, DbParameter[]? parameters, CancellationToken token = default)
		=> new AsyncDbReader<T>(
			this,
			conn => conn.CreateCommandAsync(command, parameters, token),
			TupleReadHelper<T>.ReadTuple);
#else
	public async IAsyncEnumerable<T?> ExecuteEnumerableAsync<T>(string command, DbParameter[]? parameters, [EnumeratorCancellation] CancellationToken token = default)
	{
		await using var reader = await ExecuteReaderAsync(command, parameters, token).NoCtx();
		while (await reader.ReadAsync(token).NoCtx())
			yield return await reader.IsDBNullAsync(0, token).NoCtx()
				? default
				: (T) Convert.ChangeType(reader.GetValue(0), typeof(T));
	}

	public async IAsyncEnumerable<T> ExecuteTuplesAsync<T>(string command, DbParameter[]? parameters, [EnumeratorCancellation] CancellationToken token = default)
	{
		await using var reader = await ExecuteReaderAsync(command, parameters, token).NoCtx();
		while (await reader.ReadAsync(token).NoCtx())
			yield return TupleReadHelper<T>.ReadTuple(reader);
	}
#endif
	public Task<T?> ExecuteTupleAsync<T>(string command, DbParameter[]? parameters, CancellationToken token = default)
		=> ExecuteTuplesAsync<T>(command, parameters, token).FirstOrDefaultAsync(token);

	#endregion
	#endregion
	#region ExecuteXXX(object)
	#region Sync

	public DbDataReader ExecuteReader(string command, object parameters) 
		=> ExecuteReader(command, Orm.CreateParameters(parameters));

	public int ExecuteNoQuery(string command, object parameters) 
		=> ExecuteNoQuery(command, Orm.CreateParameters(parameters));

	public object? ExecuteScalar(string command, object parameters)
		=> ExecuteScalar(command, Orm.CreateParameters(parameters));

	public T? ExecuteScalar<T>(string command, object parameters, T? @default = default) 
		=> ExecuteScalar(command, Orm.CreateParameters(parameters), @default);

	public IEnumerable<T?> ExecuteEnumerable<T>(string command, object parameters) 
		=> ExecuteEnumerable<T>(command, Orm.CreateParameters(parameters));

	public T ExecuteTuple<T>(string command, object parameters) 
		=> ExecuteTuple<T>(command, Orm.CreateParameters(parameters));

	public IEnumerable<T> ExecuteTuples<T>(string command, object parameters) 
		=> ExecuteTuples<T>(command, Orm.CreateParameters(parameters));

	#endregion
	#region Async

	public Task<DbDataReader> ExecuteReaderAsync(string command, object parameters, CancellationToken token = default)
		=> ExecuteReaderAsync(command, Orm.CreateParameters(parameters), token);

	public Task<int> ExecuteNoQueryAsync(string command, object parameters, CancellationToken token = default)
		=> ExecuteNoQueryAsync(command, Orm.CreateParameters(parameters), token);

	public Task<object?> ExecuteScalarAsync(string command, object parameters, CancellationToken token = default)
		=> ExecuteScalarAsync(command, Orm.CreateParameters(parameters), token);

	public Task<T?> ExecuteScalarAsync<T>(string command, object parameters, T? @default = default, CancellationToken token = default)
		=> ExecuteScalarAsync(command, Orm.CreateParameters(parameters), @default, token);

	public IAsyncEnumerable<T?> ExecuteEnumerableAsync<T>(string command, object parameters, CancellationToken token = default)
		=> ExecuteEnumerableAsync<T>(command, Orm.CreateParameters(parameters), token);

	public Task<T?> ExecuteTupleAsync<T>(string command, object parameters, CancellationToken token = default)
		=> ExecuteTupleAsync<T>(command, Orm.CreateParameters(parameters), token);

	public IAsyncEnumerable<T> ExecuteTuplesAsync<T>(string command, object parameters, CancellationToken token = default)
		=> ExecuteTuplesAsync<T>(command, Orm.CreateParameters(parameters), token);

	#endregion
	#endregion
	#region ExecuteXXX(FormattableString)
	#region Sync
	
	public DbDataReader ExecuteReader(FormattableString formattable)
		=> ExecuteReader(new QueryBuilder(Orm, formattable));

	public int ExecuteNoQuery(FormattableString formattable)
		=> ExecuteNoQuery(new QueryBuilder(Orm, formattable));

	public object? ExecuteScalar(FormattableString formattable)
		=> ExecuteScalar(new QueryBuilder(Orm, formattable));

	public T? ExecuteScalar<T>(FormattableString formattable, T? @default = default)
		=> ExecuteScalar(new QueryBuilder(Orm, formattable), @default);

	public IEnumerable<T?> ExecuteEnumerable<T>(FormattableString formattable)
		=> ExecuteEnumerable<T>(new QueryBuilder(Orm, formattable));

	public T? ExecuteTuple<T>(FormattableString formattable)
		=> ExecuteTuple<T>(new QueryBuilder(Orm, formattable));

	public IEnumerable<T> ExecuteTuples<T>(FormattableString formattable)
		=> ExecuteTuples<T>(new QueryBuilder(Orm, formattable));

	#endregion
	#region Async
	
	public Task<DbDataReader> ExecuteReaderAsync(FormattableString formattable, CancellationToken token = default)
		=> ExecuteReaderAsync(new QueryBuilder(Orm, formattable), token);

	public Task<int> ExecuteNoQueryAsync(FormattableString formattable, CancellationToken token = default)
		=> ExecuteNoQueryAsync(new QueryBuilder(Orm, formattable), token);

	public Task<object?> ExecuteScalarAsync(FormattableString formattable, CancellationToken token = default)
		=> ExecuteScalarAsync(new QueryBuilder(Orm, formattable), token);

	public Task<T?> ExecuteScalarAsync<T>(FormattableString formattable, T? @default = default, CancellationToken token = default)
		=> ExecuteScalarAsync(new QueryBuilder(Orm, formattable), @default, token);

	public IAsyncEnumerable<T?> ExecuteEnumerableAsync<T>(FormattableString formattable, CancellationToken token = default)
		=> ExecuteEnumerableAsync<T>(new QueryBuilder(Orm, formattable), token);

	public Task<T?> ExecuteTupleAsync<T>(FormattableString formattable, CancellationToken token = default)
		=> ExecuteTupleAsync<T>(new QueryBuilder(Orm, formattable), token);

	public IAsyncEnumerable<T> ExecuteTuplesAsync<T>(FormattableString formattable, CancellationToken token = default)
		=> ExecuteTuplesAsync<T>(new QueryBuilder(Orm, formattable), token);

	#endregion
	#endregion

	#region ExecuteXXX(QueryBuilder)
	#region Sync
	public DbDataReader ExecuteReader(QueryBuilder query)
		=> ExecuteReader(query.Query, query.Parameters);

	public int ExecuteNoQuery(QueryBuilder query)
		=> query.Nonsense ? 0 : ExecuteNoQuery(query.Query, query.Parameters);

	public object? ExecuteScalar(QueryBuilder query)
		=> query.Nonsense ? null : ExecuteScalar(query.Query, query.Parameters);

	public T? ExecuteScalar<T>(QueryBuilder query, T? @default = default)
		=> query.Nonsense ? default : ExecuteScalar(query.Query, query.Parameters, @default);

	public IEnumerable<T?> ExecuteEnumerable<T>(QueryBuilder query)
		=> query.Nonsense ? Enumerable.Empty<T>() : ExecuteEnumerable<T>(query.Query, query.Parameters);

	public T? ExecuteTuple<T>(QueryBuilder query)
		=> query.Nonsense ? default : ExecuteTuple<T>(query.Query, query.Parameters);

	public IEnumerable<T> ExecuteTuples<T>(QueryBuilder query)
		=> query.Nonsense ? Enumerable.Empty<T>() : ExecuteTuples<T>(query.Query, query.Parameters);

	#endregion
	#region Async
	
	public Task<DbDataReader> ExecuteReaderAsync(QueryBuilder query, CancellationToken token = default)
		=> ExecuteReaderAsync(query.Query, query.Parameters, token);

	public Task<int> ExecuteNoQueryAsync(QueryBuilder query, CancellationToken token = default)
		=> query.Nonsense ? Task.FromResult(0) : ExecuteNoQueryAsync(query.Query, query.Parameters, token);

	public Task<object?> ExecuteScalarAsync(QueryBuilder query, CancellationToken token = default)
		=> query.Nonsense ? Task.FromResult<object?>(null) : ExecuteScalarAsync(query.Query, query.Parameters, token);

	public Task<T?> ExecuteScalarAsync<T>(QueryBuilder query, T? @default = default, CancellationToken token = default)
		=> query.Nonsense ? Task.FromResult<T?>(default) :ExecuteScalarAsync(query.Query,  query.Parameters, @default, token);

	public IAsyncEnumerable<T?> ExecuteEnumerableAsync<T>(QueryBuilder query, CancellationToken token = default)
		=> query.Nonsense ? AsyncEnumerable.Empty<T>() : ExecuteEnumerableAsync<T>(query.Query, query.Parameters, token);

	public async Task<T?> ExecuteTupleAsync<T>(QueryBuilder query, CancellationToken token = default)
	{
		if (query.Nonsense)
			return default;

		return await ExecuteTupleAsync<T>(query.Query, query.Parameters, token).NoCtx();
	}

	public IAsyncEnumerable<T> ExecuteTuplesAsync<T>(QueryBuilder query, CancellationToken token = default)
		=> query.Nonsense ? AsyncEnumerable.Empty<T>() : ExecuteTuplesAsync<T>(query.Query, query.Parameters, token);

	#endregion
	#endregion
}
