using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Booster.CodeGen;
using Booster.FileSystem;
using Booster.Helpers;
using Booster.Log;
using Booster.Orm.Interfaces;
using Booster.Synchronization;

#nullable enable

namespace Booster.Orm.Kitchen;

public class OrmAssemblies
{
	readonly ILog? _log;
	readonly FilePath? _path;
	readonly AsyncLock _lock = new();
	readonly OrmAssemblyCacheConfig? _cacheConfig;

	public bool CachingEnabled { get; }

	readonly AsyncLazy<Dictionary<string, OrmAdaptersAssembly>> _ormAdaptersAssemblies =
		AsyncLazy<Dictionary<string, OrmAdaptersAssembly>>.Create<OrmAssemblies>(async self =>
		{
			var result = new Dictionary<string, OrmAdaptersAssembly>();

			if (self._path != null)
			{
				FileUtils.EnsureDirectory(self._path!);

				foreach (var filename in Directory.GetFiles(self._path!, "*.dll"))
					if (!await self._cacheConfig!.ShouldIgnore(filename).NoCtx())
						result[filename] = new OrmAdaptersAssembly(filename, self._log);
			}

			return result;
		});

	readonly ConcurrentDictionary<Type, int> _ormAssemblyHashes = new();

	readonly Lazy<int[]> _auxAssemblyHashes = new(() => new[]
	{
		typeof(BaseOrm).Assembly,
		typeof(Entity<>).Assembly,
		typeof(CsBuilder).Assembly
	}.Select(GetAssemblyHash).ToArray());

	public OrmAssemblies(FilePath? path, ILog? log)
	{
		_log = log.Create(this);

		if (path != null)
		{
			_path = path.IsAbsolute 
				? path.AsDirectory() 
				: AssemblyHelper.BinPath.Combine(path).AsDirectory();

			_cacheConfig = OrmAssemblyCacheConfig.Load(Path.Combine(_path, ".cch"), new[] {".pdb", ".cs"}, log);
			CachingEnabled = true;
		}
	}

	static int GetAssemblyHash(Assembly assy)
	{
		var data = File.ReadAllBytes(assy.Location);
		using var md5 = new MD5CryptoServiceProvider();
		var hash = md5.ComputeHash(data);
		return BitConverter.ToInt32(hash, 0);
	}

	public int[] GetBoosterAssembliesHashes(IOrm orm)
	{
		var ormHash = _ormAssemblyHashes.GetOrAdd(orm.GetType(), ormType => GetAssemblyHash(ormType.Assembly));
		return _auxAssemblyHashes.Value.AppendBefore(ormHash).ToArray();
	}

	public async Task FindAdapters(IOrm orm, List<IRepoInitTask> tasks)
	{
		var hashes = GetBoosterAssembliesHashes(orm);

		var assemblies = (await _ormAdaptersAssemblies.GetValue(this).NoCtx())
			.Select(a => a.Value)
			.OrderByDescending(a =>
			{
				var nm = Path.GetFileNameWithoutExtension(a.Path);
				if (nm.IsEmpty())
					return 0;

				var i = nm.LastIndexOf('.');
				if (i < 0)
					return 0;

				int.TryParse(nm.Substring(i + 1), out i);
				return i;
			})
			.ToArray();

		foreach (var task in tasks.ToArray())
		foreach (var assy in assemblies)
		{
			if (assy.Assembly == null)
			{
				_cacheConfig!.AddInvalid(assy.Path);
				continue;
			}

			if (assy.BoosterAssemblyHashes?.SequenceEqual(hashes) == false ||
				assy.InstanceContext != orm.InstanceContext)
				continue;

			if (assy.FindAdapter(orm, task, out var adapter))
			{
				task.Adapter = adapter;
				tasks.Remove(task);
				break;
			}
		}
	}

	public async Task<OrmAdaptersAssembly> GetNewAdaptersAssembly(IOrm orm)
	{
		if (_path == null)
			return new OrmAdaptersAssembly(null, _log);

		int[] hashes;
		var existing = await _ormAdaptersAssemblies.GetValue(this).NoCtx();

		using (var _ = await _lock.LockAsync().NoCtx())
			hashes = GetBoosterAssembliesHashes(orm);

		var fn = Path.Combine(_path, $"Orm{orm.InstanceContext.WithSome(c => $".{c}")}.dll");

		for (var i = 1;; ++i)
		{
			if (!File.Exists(fn) && !existing.TryGetValue(fn, out _))
			{
				var result = new OrmAdaptersAssembly(fn, _log)
				{
					InstanceContext = orm.InstanceContext,
					BoosterAssemblyHashes = hashes
				};
				existing[fn] = result;
				return result;
			}

			fn = Path.Combine(_path, $"Orm{orm.InstanceContext.WithSome(c => $".{c}")}.{i}.dll");
		}
	}

	public async Task RemoveAssembly(OrmAdaptersAssembly? assembly)
	{
		if (assembly?.Path == null)
			return;

		var existing = await _ormAdaptersAssemblies.GetValue(this).NoCtx();
		using var _ = await _lock.LockAsync().NoCtx();
		existing.Remove(assembly.Path);
	}

	public async Task CleanUp()
	{
		var existing = await _ormAdaptersAssemblies.GetValue(this).NoCtx();

		foreach (var assy in existing.Values.Where(assy => !assy.Used))
			await _cacheConfig!.Delete(assy.Path).NoCtx();

		await _cacheConfig!.Save();
	}
}