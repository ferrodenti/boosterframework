using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

namespace Booster.Orm.Kitchen;

public class PrefetchingDataReader : DbDataReader
{
	DbDataReader _inner;

	Action _releaser;

	public PrefetchingDataReader(DbDataReader inner, Action releaser)
	{
		_inner = inner;
		_releaser = releaser;
	}
	public void Prefetch()
	{
		var newInner = new MemoryDataReader();
		newInner.ReadAll(_inner);
		_inner.Close();
		Release(); // соединению надо сказать, что не надо освобождать ридер и сбросить команду если у нас синхронный вариант
#if CONCURRENCY_DEBUG
			if (_inner is Debug.DebugDbReader debugDbReader) // асинхронный ридер сам освобождает команду, поэтому нам надо тут её принудительно обнулить,
				debugDbReader.DebugDbConnection.Command?.Dispose(); // что можно сделать только Dispose (асинхронный ридер тогда не будет её освобождать и ругаться)
#endif
		_inner = newInner;
	}

	public async Task PrefetchAsync(CancellationToken token)
	{
		var newInner = new MemoryDataReader();
		await newInner.ReadAllAsync(_inner, token).NoCtx();

#if NETSTANDARD
		await _inner.CloseAsync().NoCtx();
#else
		_inner.Close();
#endif

		Release(); // соединению надо сказать, что не надо освобождать ридер и сбросить команду если у нас синхронный вариант
#if CONCURRENCY_DEBUG
			if (_inner is Debug.DebugDbReader debugDbReader) // асинхронный ридер сам освобождает команду, поэтому нам надо тут её принудительно обнулить,
				debugDbReader.DebugDbConnection.Command?.Dispose(); // что можно сделать только Dispose (асинхронный ридер тогда не будет её освобождать и ругаться)
#endif
		_inner = newInner;
	}

	public override void Close()
	{
		_inner.Close();
		Release();
	}

	void Release()
	{
		_releaser?.Invoke();
		_releaser = null;
	}

	public override DataTable GetSchemaTable() 
		=> _inner.GetSchemaTable();

	public override bool NextResult() 
		=> _inner.NextResult();

	public override bool Read() 
		=> _inner.Read();

	public override int Depth 
		=> _inner.Depth;

	public override Task<bool> ReadAsync(CancellationToken cancellationToken)
		=> _inner.ReadAsync(cancellationToken);

	public override Task<T> GetFieldValueAsync<T>(int ordinal, CancellationToken cancellationToken)
		=> _inner.GetFieldValueAsync<T>(ordinal, cancellationToken);

	public override Task<bool> IsDBNullAsync(int ordinal, CancellationToken cancellationToken)
		=> _inner.IsDBNullAsync(ordinal, cancellationToken);

	public override Task<bool> NextResultAsync(CancellationToken cancellationToken)
		=> _inner.NextResultAsync(cancellationToken);

	public override bool HasRows
		=> _inner.HasRows;

	public override bool IsClosed 
		=> _inner.IsClosed;

	public override int RecordsAffected 
		=> _inner.RecordsAffected;

	public override bool GetBoolean(int ordinal) 
		=> _inner.GetBoolean(ordinal);

	public override byte GetByte(int ordinal) 
		=> _inner.GetByte(ordinal);

	public override long GetBytes(int ordinal, long dataOffset, byte[] buffer, int bufferOffset, int length) => _inner.GetBytes(ordinal, dataOffset, buffer, bufferOffset, length);

	public override char GetChar(int ordinal) 
		=> _inner.GetChar(ordinal);

	public override long GetChars(int ordinal, long dataOffset, char[] buffer, int bufferOffset, int length) => _inner.GetChars(ordinal, dataOffset, buffer, bufferOffset, length);

	public override Guid GetGuid(int ordinal) 
		=> _inner.GetGuid(ordinal);

	public override short GetInt16(int ordinal) 
		=> _inner.GetInt16(ordinal);

	public override int GetInt32(int ordinal) 
		=> _inner.GetInt32(ordinal);

	public override long GetInt64(int ordinal) 
		=> _inner.GetInt64(ordinal);

	public override DateTime GetDateTime(int ordinal) 
		=> _inner.GetDateTime(ordinal);

	public override string GetString(int ordinal) 
		=> _inner.GetString(ordinal);

	public override object GetValue(int ordinal) 
		=> _inner.GetValue(ordinal);

	public override int GetValues(object[] values) 
		=> _inner.GetValues(values);

	public override bool IsDBNull(int ordinal) 
		=> _inner.IsDBNull(ordinal);

	public override int FieldCount 
		=> _inner.FieldCount;

	public override object this[int ordinal] 
		=> _inner[ordinal];

	public override object this[string name] 
		=> _inner[name];

	public override decimal GetDecimal(int ordinal) 
		=> _inner.GetDecimal(ordinal);

	public override double GetDouble(int ordinal) 
		=> _inner.GetDouble(ordinal);

	public override float GetFloat(int ordinal) 
		=> _inner.GetFloat(ordinal);

	public override string GetName(int ordinal) 
		=> _inner.GetName(ordinal);

	public override int GetOrdinal(string name) 
		=> _inner.GetOrdinal(name);

	public override string GetDataTypeName(int ordinal) 
		=> _inner.GetDataTypeName(ordinal);


	public override Type GetFieldType(int ordinal) 
		=> _inner.GetFieldType(ordinal);

	public override IEnumerator GetEnumerator()
		=> _inner.GetEnumerator();

	public bool IsDetached { get; set; }
}