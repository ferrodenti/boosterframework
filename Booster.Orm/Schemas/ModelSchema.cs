﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Booster.Log;
using Booster.Orm.Interfaces;
using Booster.Reflection;

namespace Booster.Orm.Schemas;

public class ModelSchema : IModelSchemaWithMigration
{
	public TypeEx ModelType { get; set; }
	public string TableName { get; set; }
	public DbUpdateOptions DbUpdateOptions { get; set; }
	public string DefaultSort { get; set; }
	public string InstanceContext { get; set; }
	public AdapterImplementation AdapterImplementation { get; set; } = AdapterImplementation.All;

	public string[] OldNames { get; set; }
	public IDbIndex[] Indexes { get; set; }
	public IDbConstraint[] Constraints { get; set; }
	public IDbSequence[] Sequences { get; set; }
		
	public bool IsDictionary { get; set; }
	public bool IsView { get; set; }
	public bool RowIdAsPk { get; set; }

	public CacheSettings DictionaryCacheSettings { get; set; }
		
	public readonly List<IModelDataMember> Members = new();

	internal readonly Dictionary<(MigrationOptions, bool), List<(MigrationAttribute, Method)>> MigrationMethods 
		= new();
		
	public void AddColumn(IModelDataMember modelDataMember)
	{
		if (!this.AllowAutoUpdateDb())
			modelDataMember.DbUpdateOptions = DbUpdateOptions.Deny;

		Members.Add(modelDataMember);
	}

	public IEnumerator<IModelDataMember> GetEnumerator() => Members.GetEnumerator();

	IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

	public override string ToString() => $"{(IsView ? "VIEW" : IsDictionary ? "DICT" : "REPO")} {ModelType} on {TableName}";

	public async Task Migration(IRepoInitTask task, MigrationOptions stage, RepoInitContext e, IConnection connection, ILog log)
	{
		if (MigrationMethods.TryGetValue((stage, false), out var methods))
			await MigrationImpl(methods, e, connection, stage, false, log).NoCtx();

		if (MigrationMethods.TryGetValue((stage, true), out methods))
			task.AddAction((ctx, conn) => MigrationImpl(methods, ctx, conn, stage, true, log));
	}

	async Task MigrationImpl(IEnumerable<(MigrationAttribute, Method)> methods, RepoInitContext context, IConnection connection, MigrationOptions stage, bool ready, ILog log)
	{
		foreach (var (attr, method) in methods.ToArray())
		{
			if ((context.Exists || !attr.Options.HasFlag(MigrationOptions.ShouldExist)) &&
				(attr.Version < 0 || attr.Version > context.Version))
			{
				var msg = $"Executing migration on {context.TableName}, stage: {stage}";
				//TODO: fire Orm.Migration event

				async Task MigrationProc()
				{
					var args = new List<object>();

					foreach (var param in method.Parameters)
						if (param.ParameterType == typeof(RepoInitContext))
							args.Add(context);
						else if (param.ParameterType == typeof(IConnection))
							args.Add(connection);
						else if (param.ParameterType == typeof(MigrationOptions))
							args.Add(stage);
						else if (param.ParameterType == typeof(ILog))
							args.Add(log);
						else
							throw new Exception($"Unexpected parameter: {param.Name} ({param.ParameterType.Name})");

					await method.InvokeAsync(null, args.ToArray()).NoCtx();

					if (attr.Version >= 0 && context.Version < attr.Version) context.Version = attr.Version;
				}

				if (attr.Options.HasFlag(MigrationOptions.SuppressLog))
					await MigrationProc().NoCtx();
				else
					await log.TryInfo(attr.Version >= 0 ? $"{msg}, ver {context.Version} -> {attr.Version}..." : $"{msg}...", MigrationProc).NoCtx();
			}
				
			MigrationMethods[(stage, ready)].Remove((attr, method));
		}
	}
}