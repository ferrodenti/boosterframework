using System;
using System.Collections;
using System.Collections.Generic;
using Booster.Orm.Interfaces;

namespace Booster.Orm.Schemas;

public class TableSchema : ITableSchema
{
	public string DbSchema { get; set; }
	public string TableName { get; set; }
	public ITableMetadata Metadata { get; set; }

	readonly Dictionary<string, IColumnSchema> _columnsByName = new(StringComparer.OrdinalIgnoreCase);

	public TableSchema(string dbSchema, string tableName, ITableMetadata metadata)
	{
		DbSchema = dbSchema;
		TableName = tableName;
		Metadata = metadata;
	}

	public IColumnSchema this[string name] => _columnsByName.SafeGet(name);

	public void SetColumns(IEnumerable<IColumnSchema> columns)
	{
		_columnsByName.Clear();
		
		foreach (var column in columns)
			AddColumn(column);
	}

	public void AddColumn(IColumnSchema column)
	{
		if (column.Ordinal <= 0)
			column.Ordinal = this.MaxOrDefault(c => c.Ordinal) + 1;

		_columnsByName[column.Name] = column;
	}

	public void ChanageColumnName(IColumnSchema updatedColumn, string newName)
	{
		_columnsByName.Remove(updatedColumn.Name);
		_columnsByName[newName] = updatedColumn;
	}

	public IEnumerator<IColumnSchema> GetEnumerator()
		=> _columnsByName.Values.GetEnumerator();

	IEnumerator IEnumerable.GetEnumerator()
		=> GetEnumerator();

	public override string ToString()
	{
		var res = new EnumBuilder(" ");
		res.Append(TableName);

		var ver = Metadata.With(md => md.Version);
		if (ver != 0)
			res.Append($"ver:{ver}");

		return res;
	}
}