﻿using System.Collections.Generic;
using System.Collections.Specialized;
using Booster.Orm.Interfaces;
using Booster.Reflection;
using static Booster.FastLazyStatic;

#nullable enable

namespace Booster.Orm.Schemas;

public class TableMapping : ITableMapping
{
	public string InstanceContext => ModelSchema.InstanceContext;

	FastLazy<TypeEx?>? _pkType;

	public TypeEx? PkType => _pkType ??= FastLazy(() =>
	{
		foreach (var column in PkColumns) //TODO: разобраться более подробно на все случаи жизни(учитывать не только промэпленные колонки?)
		{
//			var i = column.Member.DbColumnDataType.IndexOf('(');
//			if (i > 0)
//			{
//				if (!column.Member.DbColumnDataType.Substring(0, i).EqualsIgnoreCase(column.ColumnDataType.Substring(0, i)))
//					return null;
//			}
//			else if (!column.Member.DbColumnDataType.EqualsIgnoreCase(column.ColumnDataType))
//				return null;

			if (!column.IsPk)
				continue;

			return column.MemberDataType;
		}
		return null;
	});


	public bool RowIdAsPk => ModelSchema.RowIdAsPk;

	public bool IsDictionary => ModelSchema.IsDictionary && PkType != null;
	public CacheSettings DictionaryCacheSettings => ModelSchema.DictionaryCacheSettings;

	public ITableMetadata Metadata => TableSchema.Metadata;

	public string DefaultSort => ModelSchema.DefaultSort;

	public List<IModelDataMember> UnumappedDataMemebers { get; }
	public List<IColumnSchema> UnumappedColumns { get; }

	public List<IColumnMapping> Columns { get; }
	public List<IColumnMapping> PkColumns { get; }
	public List<IColumnMapping> ReadColumns { get; }
	public List<IColumnMapping> WriteColumns { get; }
	public List<IColumnMapping> IdentityColumns { get; set; }

	public IModelSchema ModelSchema { get; }
	public ITableSchema TableSchema { get; }
	public DbUpdateOptions DbUpdateOptions => ModelSchema.DbUpdateOptions;
	public AdapterImplementation AdapterImplementation => ModelSchema.AdapterImplementation;

	public TypeEx ModelType => ModelSchema.ModelType;
	public string TableName => TableSchema.TableName;

	string? _mold;
	public string Mold
	{
		get
		{
			if (_mold == null)
			{
				var result = new EnumBuilder("\n");
				result.Append($"{ModelType.FullName}:{TableSchema.DbSchema}.{TableName}");
					
				if (IsDictionary)
					result.Append("Dict");

				if (RowIdAsPk)
					result.Append("RowId");
					
				result.Append($"Sort:{DefaultSort}");
				
				foreach (var col in Columns) 
					result.Append(col);

				var umm = UnumappedDataMemebers.ToString(new EnumBuilder("\n", "UMM:"));
				if (umm.IsSome())
					result.Append(umm);
					
				var umc = UnumappedColumns.ToString(new EnumBuilder("\n", "UMC:"));
				if (umc.IsSome())
					result.Append(umc);
					
				var methods = new EnumBuilder(",", "Methods:");
				void EntityEvent(string name)
				{
					var mi = ModelType.FindMethod(name);

					if (mi is
						{
							IsPublic: true,
							IsAbstract: false,
							IsStatic: false
						} &&
						mi.FindAttribute<DbSkipAttribute>()?.Evaluate(ModelType) != true)
						methods.Append(name);
				}
					
				EntityEvent("OnLoad");
				EntityEvent("OnLoaded");
				EntityEvent("OnSave");
				EntityEvent("OnSaved");
				EntityEvent("OnInsert");
				EntityEvent("OnInserted");
				EntityEvent("OnUpdate");
				EntityEvent("OnUpdated");
				EntityEvent("OnDelete");
				EntityEvent("OnDeleted");
				EntityEvent("OnTableChanged");
				EntityEvent("OnTableChange");

				if (methods.IsSome)
					result.Append(methods);

				result.Append($"AIPM:{(int)AdapterImplementation}");
					
				_mold = result;
			}
			return _mold;
		}
	}
		
	public TableMapping(IModelSchema modelSchema, ITableSchema tableSchema)
	{
		ModelSchema = modelSchema;
		TableSchema = tableSchema;

		UnumappedDataMemebers = new List<IModelDataMember>(ModelSchema);
		UnumappedColumns = new List<IColumnSchema>(TableSchema);
		Columns = new List<IColumnMapping>();

		PkColumns = new List<IColumnMapping>();
		ReadColumns = new List<IColumnMapping>();
		WriteColumns = new List<IColumnMapping>();

		foreach (var dmSchema in UnumappedDataMemebers.ToArray())
		{
			var colSchema = TableSchema[dmSchema.DbColumnName];

			if (dmSchema.Skip)
			{
				UnumappedDataMemebers.Remove(dmSchema);

				if (colSchema != null)
					UnumappedColumns.Remove(colSchema);

				continue;
			}

			if (colSchema != null)
				MapColumn(dmSchema, colSchema);
		}

		//if (PkColumns.Count == 1)
		//	WriteColumns.Remove(PkColumns[0]);

		IdentityColumns = PkColumns.Count > 0 ? PkColumns : ReadColumns;
	}

	public void MapColumn(IModelDataMember dmSchema, IColumnSchema colSchema)
	{
		var column = new ColumnMapping(dmSchema, colSchema);

		Columns.Add(column);

		if (column.Column.IsPk && column.Member.IsPk)
			PkColumns.Add(column);

		if (column.CanWriteModel)
			ReadColumns.Add(column);

		if (column.CanReadModel && !column.Column.IsReadonly && !dmSchema.IsAutoGenerated && !column.Column.IsAutoGenerated)
			WriteColumns.Add(column);

		UnumappedColumns.Remove(colSchema);
		UnumappedDataMemebers.Remove(dmSchema);

		_columnsByName.Reset();
		_columnsByNameIgnoreCase.Reset();

		_columnsByDbName.Reset();
		_columnsByDbNameIgnoreCase.Reset();
		_mold = null;
	}

	readonly SyncLazy<HybridDictionary> _columnsByName = SyncLazy<HybridDictionary>.Create<TableMapping>(self => CreateByNameDict(self, false));
	readonly SyncLazy<HybridDictionary> _columnsByNameIgnoreCase = SyncLazy<HybridDictionary>.Create<TableMapping>(self => CreateByNameDict(self, true));

	readonly SyncLazy<HybridDictionary> _columnsByDbName = SyncLazy<HybridDictionary>.Create<TableMapping>(self => CreateByDbNameDict(self, false));
	readonly SyncLazy<HybridDictionary> _columnsByDbNameIgnoreCase = SyncLazy<HybridDictionary>.Create<TableMapping>(self => CreateByDbNameDict(self, true));

	static HybridDictionary CreateByNameDict(ITableMapping tableMapping, bool ignoreCase)
	{
		var res = new HybridDictionary(ignoreCase);

		foreach (var col in tableMapping.Columns)
			res[col.MemberName] = col;

		return res;
	}

	static HybridDictionary CreateByDbNameDict(ITableMapping tableMapping, bool ignoreCase)
	{
		var res = new HybridDictionary(ignoreCase);

		foreach (var col in tableMapping.Columns)
			res[col.ColumnName] = col;

		return res;
	}

	public IColumnMapping? GetColumnByName(string columnName, bool ignoreCase = true)
	{
		if (TryGetColumnByName(columnName, out var result, ignoreCase))
			return result;

		return null;
	}

	public IColumnMapping? GetColumnByDbName(string columnName, bool ignoreCase = true)
	{
		if (TryGetColumnByDbName(columnName, out var result, ignoreCase))
			return result;

		return null;
	}

	public bool TryGetColumnByName(string? columnName, out IColumnMapping result, bool ignoreCase = true)
	{
		if (columnName.IsEmpty())
		{
			result = default!;
			return false;
		}

		var dict = (ignoreCase ? _columnsByNameIgnoreCase : _columnsByName).GetValue(this);
		return dict.TryGetValue(columnName, out result);
	}

	public bool TryGetColumnByDbName(string? memberName, out IColumnMapping result, bool ignoreCase = true)
	{
		if (memberName.IsEmpty())
		{
			result = default!;
			return false;
		}

		var dict = (ignoreCase ? _columnsByDbNameIgnoreCase : _columnsByDbName).GetValue(this);
		return dict.TryGetValue(memberName, out result);
	}

	public override string ToString() 
		=> $"{(IsDictionary ? "DICT" : "REPO")} {ModelType} on {TableSchema}";
}