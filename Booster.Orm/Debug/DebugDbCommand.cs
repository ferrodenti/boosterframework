﻿#if CONCURRENCY_DEBUG
using System.Data;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using Booster.Debug;

namespace Booster.Orm.Debug
{
	public class DebugDbCommand : DbCommand
	{
		static volatile int _cnt;
		public readonly DebugDbConnection DebugDbConnection;
		readonly int _no;
		readonly DbCommand _inner;
		bool _disposed;
		
#pragma warning disable 649
		internal readonly StackTraceLog HistoryLog;
#pragma warning restore 649

		public DebugDbCommand(DbCommand inner, DebugDbConnection debugDbConnection)
		{
			_inner = inner;
			_no = ++_cnt;
			//HistoryLog = new StackTraceLog(_no);
			HistoryLog?.Log($"Command{_no}.created");
			DebugDbConnection = debugDbConnection;
			DebugDbConnection.Log($"Command{_no}.created");
			
			if (DebugDbConnection.Command != null)
				throw new StackTraceLogException(DebugDbConnection.HistoryLog, DebugDbConnection.Command.HistoryLog, $"Connection has opened command {DebugDbConnection.Command._no}");

			DebugDbConnection.Command = this;
		}

		public override string ToString()
			=> $"Command{_no} on {DebugDbConnection}: {CommandText}";

		protected override void Dispose(bool disposing)
		{
			DebugDbConnection.Log($"Command{_no}.disposing={disposing}");
			HistoryLog?.Log($"Command{_no}.disposing={disposing}");
			
			base.Dispose(disposing);

			if (disposing && !_disposed)
			{
				if (DebugDbConnection.Command != this)
					throw new StackTraceLogException(DebugDbConnection.HistoryLog, DebugDbConnection.Command.HistoryLog, $"Connection has unexpected command {DebugDbConnection.Command._no}");

				DebugDbConnection.Command = null;				
				_inner.Dispose();
				_disposed = true;
			}
		}
		

		public override void Prepare() 
			=> _inner.Prepare();

		public override string CommandText
		{
			get => _inner.CommandText;
			set
			{
				_inner.CommandText = value;
				DebugDbConnection.Log($"Command{_no}.text={value}");
				HistoryLog?.Log($"Command{_no}.text={value}");
			}
		}

		public override int CommandTimeout
		{
			get => _inner.CommandTimeout;
			set => _inner.CommandTimeout = value;
		}

		public override CommandType CommandType
		{
			get => _inner.CommandType;
			set => _inner.CommandType = value;
		}

		public override UpdateRowSource UpdatedRowSource
		{
			get => _inner.UpdatedRowSource;
			set => _inner.UpdatedRowSource = value;
		}

		protected override DbConnection DbConnection
		{
			get => _inner.Connection;
			set => _inner.Connection = value;
		}

		protected override DbParameterCollection DbParameterCollection => _inner.Parameters;

		protected override DbTransaction DbTransaction
		{
			get => _inner.Transaction;
			set => _inner.Transaction = value;
		}

		public override bool DesignTimeVisible
		{
			get => _inner.DesignTimeVisible;
			set => _inner.DesignTimeVisible = value;
		}

		public override void Cancel() 
			=> _inner.Cancel();

		protected override DbParameter CreateDbParameter() 
			=> _inner.CreateParameter();

		protected override DbDataReader ExecuteDbDataReader(CommandBehavior behavior) 
			=> new DebugDbReader(_inner.ExecuteReader(behavior), DebugDbConnection, this);

		protected override async Task<DbDataReader> ExecuteDbDataReaderAsync(CommandBehavior behavior, CancellationToken cancellationToken) 
			=> new DebugDbReader(await _inner.ExecuteReaderAsync(behavior, cancellationToken).NoCtx(), DebugDbConnection, this);

		public override int ExecuteNonQuery() 
			=> _inner.ExecuteNonQuery();

		public override object ExecuteScalar() 
			=> _inner.ExecuteScalar();

		public override Task<object> ExecuteScalarAsync(CancellationToken cancellationToken) 
			=> _inner.ExecuteScalarAsync(cancellationToken);

		public override Task<int> ExecuteNonQueryAsync(CancellationToken cancellationToken) 
			=> _inner.ExecuteNonQueryAsync(cancellationToken);
	}
}
#endif