#if CONCURRENCY_DEBUG
using System.Threading.Tasks;

namespace Booster.Orm.Debug
{
	interface IConcurrencyTest
	{
		int Queries { get; }
		Task Run();
		Task LoadOne();
	}
}
#endif