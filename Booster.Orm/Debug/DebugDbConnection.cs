﻿#if CONCURRENCY_DEBUG
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Booster.Debug;
using JetBrains.Annotations;

namespace Booster.Orm.Debug
{
	public class DebugDbConnection : DbConnection
	{
		public volatile DebugDbReader Reader;
		public volatile DebugDbCommand Command;

		static volatile int _cnt;
		readonly DateTime _createdTime = DateTime.Now;
		readonly TimeSchedule.Subscription _releaseCheckTimeout;
		bool _isLended;
		readonly int _no;
		int _usages;

		internal readonly StackTraceLog HistoryLog;

		public void Log(string message, bool noStackTrace = false)
			=> HistoryLog.Log(message, noStackTrace);

		readonly DbConnection _inner;
		
		static readonly List<DebugDbConnection> _allConnections = new List<DebugDbConnection>();
		[PublicAPI] public static DebugDbConnection[] AllConnections => _allConnections.OrderBy(c => c._createdTime).ToArray();

		public DebugDbConnection(DbConnection inner)
		{
			_inner = inner;
			_no = ++_cnt;
			HistoryLog = new StackTraceLog(_no);
			_allConnections.Add(this);
			_releaseCheckTimeout = TimeSchedule.GC.Subscribe(this, ()
				=> Utils.Nop(new StackTraceLogException(HistoryLog, "A connection was idle but not released")));

			Log("Created");
		}

		public override string ToString()
		{
			var result = new EnumBuilder(" ");
			
			result.Append($"Conn{_no}");
			result.Append($"USAGES: {_usages}");
			result.Append(_isLended ? "LENDED" : "INPOOL");
			
			return result;
		}

		public override void Close()
		{
			_inner.Close();			
			_allConnections.Remove(this);
			_releaseCheckTimeout.Dispose();
			Log("Closed");
		}

		protected override void Dispose(bool disposing)
		{
			Log($"Dispose disposing={disposing}");
			
			base.Dispose(disposing);
			
			if(disposing)
				Close();
		}

		public void Lended()
		{
			Log("Lended");
			
			if (_isLended)
				throw new StackTraceLogException(HistoryLog, "A connection is already lended");

			if (Command != null)
				throw new StackTraceLogException(HistoryLog, "Newly lended connection has opened command");

			_isLended = true;
			_usages++;
			ProlongateCheck();
		}

		public void ProlongateCheck()
			=> _releaseCheckTimeout.Next = DateTime.Now.AddMinutes(10);

		public void Released()
		{
			Log("Released");
			
			if (!_isLended)
				throw new StackTraceLogException(HistoryLog, "A connection was not lended");
				
			if (Command != null)
				throw new StackTraceLogException(HistoryLog, "Released connection has opened command");

			if (Reader != null)
				throw new StackTraceLogException(HistoryLog, "Released connection has opened reader");

			_isLended = false;
			_releaseCheckTimeout.Next = DateTime.MinValue;
		}

		protected override DbTransaction BeginDbTransaction(IsolationLevel isolationLevel) 
			=> _inner.BeginTransaction(isolationLevel);


		public override void ChangeDatabase(string databaseName) 
			=> _inner.ChangeDatabase(databaseName);

		public override void Open() 
			=> _inner.Open();

		public override Task OpenAsync(CancellationToken cancellationToken) 
			=> _inner.OpenAsync(cancellationToken);

		public override string ConnectionString
		{
			get => _inner.ConnectionString;
			set => _inner.ConnectionString = value;
		}

		public override string Database => _inner.Database;

		public override ConnectionState State => _inner.State;

		public override string DataSource => _inner.DataSource;

		public override string ServerVersion => _inner.ServerVersion;

		protected override DbCommand CreateDbCommand()
		{
			ProlongateCheck();
			return new DebugDbCommand(_inner.CreateCommand(), this);
		}
	}
}
#endif