#if CONCURRENCY_DEBUG
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Booster.Debug;
using Booster.Log;
using Booster.Orm.Interfaces;

namespace Booster.Orm.Debug
{
	public class ConcurrencyTests
	{
		readonly List<IConcurrencyTest> _tests = new List<IConcurrencyTest>();

		public void Add<TModel, TId>(Action<TModel> action = null) where TModel : class, IEntity<TId> 
			=> _tests.Add(new ConcurrencyTest<TModel, TId>(_tests, action));

		public void Run(ILog log, int threads = 20, int iters = 500)
		{
			log = log.Create("ConcurrencyTests");
			
			Exception e = null;
			
			log.Info($"started {threads}/{iters}...");

			var total = threads * iters;
			var cnt = 0;
			var perc = 0L;
			var tasks = new List<Task>();

			var stopwatch = new Stopwatch();
			stopwatch.Start();
			
			for (var j = 0; j < threads; ++j)
			{
				var taskNo = j;
				tasks.Add(
					Utils.StartTaskNoFlow(async () =>
					{
						TaskInfo.Init($"Test task {taskNo}", true);
						try
						{
							for (var i = 0; i < iters; ++i)
							{
								Interlocked.Increment(ref cnt);
								var p = (int) Math.Ceiling(cnt * 100.0 / total);
								if (Interlocked.Exchange(ref perc, p) < p)
									log.Trace($"{perc}%");

								await Rnd.Element(_tests).Run().NoCtx();
							}

							log.Trace($"Task {taskNo} done!!!");
						}
						catch (Exception ex)
						{
							log.Error($"Task {taskNo} error: {ex.Message}");
							e = ex;
						}
						finally
						{
							log.Trace($"Task {taskNo} finished");
						}

					}));
			}


			Task.WaitAll(tasks.ToArray());

			stopwatch.Stop();

			var queries = _tests.Sum(t => t.Queries);

			if (e != null)
				// ReSharper disable once PossibleNullReferenceException
				throw e;
			
			log.Info($"complete: {queries} queries in {(int)stopwatch.Elapsed.TotalSeconds} seconds ({queries/stopwatch.Elapsed.TotalSeconds} QPS)");
		}
	}
}

#endif