#if CONCURRENCY_DEBUG
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Booster.AsyncLinq;
using Booster.DI;
using Booster.Orm.Interfaces;
using Booster.Orm.Kitchen;

namespace Booster.Orm.Debug
{
	class ConcurrencyTest<TModel, TId> : IConcurrencyTest where TModel : class, IEntity<TId>
	{
		const int _numRecors = 50;

		volatile int _queries;
		public int Queries => _queries;

		static readonly SqlRepository<TId> _repo = RepositoryInstance<SqlRepository<TId>, TModel>.Value;

		readonly SyncLazy<TId[]> _randomIds = SyncLazy<TId[]>.Create<ConcurrencyTest<TModel, TId>> (self =>
		{
			self._queries++;
			using (var conn = InstanceFactory.Get<IConnection>())
				return conn.ExecuteEnumerable<TId>($"select id from {typeof(TModel):?} order by random() limit {_numRecors}").ToArray();
		});

		protected TId[] RandomIds => _randomIds.GetValue(this);
		readonly Action<TModel> _action;
		readonly List<IConcurrencyTest> _otherTests;

		public ConcurrencyTest(List<IConcurrencyTest> otherTests, Action<TModel> action)
		{
			_otherTests = otherTests;
			_action = action;
		}
		
		public async Task Run()
		{
			using (Rnd.Bool() ? InstanceFactory.Get<IConnection>() : null)
			{
				var cnt = 0;
				switch (Rnd.Int(4))
				{
				case 0:
					foreach (var id in RandomIds.Shuffle())
					{
						var ent = Rnd.Bool() ? (TModel) _repo.LoadById(id) : (TModel) await _repo.LoadByIdAsync(id);
						if (ent == null)
							throw new Exception($"Could not load {typeof(TModel).Name}.{id}");

						if (Rnd.Bool())
						{
							_action?.Invoke(ent);
							_queries++;
						}

						if (Rnd.Bool())
							await Rnd.Element(_otherTests).LoadOne();
					}
					break;
				case 1:
					foreach (TModel ent in _repo.SelectByPks(RandomIds.Shuffle()))
					{
						if (Rnd.Bool())
						{
							_action?.Invoke(ent);
							_queries++;
						}

						if (Rnd.Bool())
							await Rnd.Element(_otherTests).LoadOne();

						cnt++;
					}
					
					if(cnt != RandomIds.Length)
						throw new Exception($"Expected {RandomIds.Length} records, actual {cnt}");

					break;
				case 2:
					await _repo.SelectByPksAsync(RandomIds.Shuffle()).Cast<TModel>().ForEach(async ent =>
					{
						if (Rnd.Bool())
						{
							_action?.Invoke(ent);
							_queries++;
						}

						if (Rnd.Bool())
							await Rnd.Element(_otherTests).LoadOne();
						
						cnt++;
					});
					
					if(cnt != RandomIds.Length)
						throw new Exception($"Expected {RandomIds.Length} records, actual {cnt}");
					break;
				case 3:
					await _repo.SelectAsync($"order by random() limit {_numRecors}", null).Cast<TModel>().ForEach(async ent =>
					{
						if (Rnd.Bool())
						{
							_action?.Invoke(ent);
							_queries++;
						}

						if (Rnd.Bool())
							await Rnd.Element(_otherTests).LoadOne();
					});
					
					break;
				default:
					return;
				}
				_queries++;
			}
		}


		public async Task LoadOne()
		{
			using (Rnd.Bool() ? InstanceFactory.Get<IConnection>() : null)
			{
				var id = Rnd.Element(RandomIds);
				var ent = Rnd.Bool() ? (TModel) _repo.LoadById(id) : (TModel) await _repo.LoadByIdAsync(id);
				if (ent == null)
					throw new Exception($"Could not load {typeof(TModel).Name}.{id}");

				if (Rnd.Bool())
				{
					_action?.Invoke(ent);
					_queries++;
				}
			}
			_queries++;
		}
	}
}

#endif