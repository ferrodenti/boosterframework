﻿using System;

namespace Booster.Orm;

[Flags]
public enum OrmLogOptions
{
	None = 0,
	TableNotFound = 1,
	UnmappedColumn = 2,
	ColumnNotFound = 4,
	ColumnTypeMuissmatch = 8,

	All = TableNotFound | UnmappedColumn | ColumnNotFound | ColumnTypeMuissmatch
}