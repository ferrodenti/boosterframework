using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using Booster.DI;
using Booster.Orm.Interfaces;

namespace Booster.Orm;

public class ParameterArrayBuilder
{
	readonly string _paramNamePrefix;
	IOrm _orm;
	protected IOrm Orm => _orm ??= InstanceFactory.Get<IOrm>();
	readonly EnumBuilder _builder;

	public List<DbParameter> Parameters { get; }

	public string Query => _builder;

	public ParameterArrayBuilder(string paramNamePrefix = "@tag", IOrm orm = null)
	{
		_paramNamePrefix = paramNamePrefix;
		_orm = orm;

		_builder = new EnumBuilder(",");

		Parameters = new List<DbParameter>();
	}

	public ParameterArrayBuilder(IEnumerable values, string paramNamePrefix = "@tag", IOrm orm = null) : this(paramNamePrefix, orm)
		=> AddParams(values);

	public void AddParams(IEnumerable values)
	{
		foreach (var val in values)
		{
			var name = _paramNamePrefix + Parameters.Count + 1;
			Parameters.Add(Orm.CreateParameter(name, val));
			_builder.Append(name);
		}
	}
}