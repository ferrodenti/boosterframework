using System;

#nullable enable

namespace Booster.Orm.Attributes;

[AttributeUsage(AttributeTargets.Assembly)]
public class OrmAdaptersAssemblyAttribute : Attribute
{
	public string? InstanceContext { get; }
	public int[]? BoosterAssemblyHashes { get; }
		
	public OrmAdaptersAssemblyAttribute(string? instanceContext, params int[]? boosterAssemblyHashes)
	{
		InstanceContext = instanceContext;
		BoosterAssemblyHashes = boosterAssemblyHashes;
	}
}