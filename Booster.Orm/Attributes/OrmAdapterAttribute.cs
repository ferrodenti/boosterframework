using System;

namespace Booster.Orm.Attributes;

[AttributeUsage(AttributeTargets.Class)]
public class OrmAdapterAttribute : Attribute
{
	public Type EntityType { get; }
	public string Mold { get; }
		
	public OrmAdapterAttribute(Type entityType, string mold)
	{
		EntityType = entityType;
		Mold = mold?.Replace("\t", "");
	}
}