﻿using System;

namespace Booster.Orm.Attributes;

[AttributeUsage(AttributeTargets.Assembly)]
public class OrmAssemblyAttribute : Attribute
{
}