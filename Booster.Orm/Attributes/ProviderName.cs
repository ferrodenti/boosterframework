﻿using System;

namespace Booster.Orm.Attributes;

public class ProviderNamesAttribute : Attribute
{
	public string[] Names { get; }
	public ProviderNamesAttribute(params string[] names)
        => Names = names;
}