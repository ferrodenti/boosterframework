using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Reflection;
using System.Resources;
using System.Xml.Serialization;
using Booster.FileSystem;

namespace Booster.Orm.NativeBin;

// ReSharper disable AssignNullToNotNullAttribute
// ReSharper disable PossibleNullReferenceException
public static class NativeBinManager
{
	static readonly XmlSerializer _serializer = new(typeof (List<NativeBinSettings>));

	public static void UpdateBin(ResourceManager manager, FilePath path = null, string cfgName = "NativeBin", string platform = null)
	{						
		path ??= Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
		platform ??= Environment.Is64BitProcess ? "x64" : "x86";

		List<NativeBinSettings> settings;
		try
		{
			using var tr = new StringReader((string)manager.GetObject(cfgName));
			settings = (List<NativeBinSettings>) _serializer.Deserialize(tr);
		}
		catch (Exception)
		{
			return;
		}

		foreach (var res in settings)
		{
			Stream srcStream = null;

			try
			{
				if (!res.Platform.EqualsIgnoreCase(platform))
					continue;

				var fn = path.Combine(res.Path);
				byte[] data = null;

				if (File.Exists(fn))
				{
					if (new FileInfo(fn).Length == res.Size)
					{
						if (res.SkipByteCheck)
							continue;
							
						data = (byte[]) manager.GetObject(res.ResourceName);

						srcStream = Unzip(data);

						using var dstStream = File.OpenRead(fn);
						if (CompareStreams(dstStream, srcStream))
							continue;
					}

					File.Delete(fn);
				}
				else
				{
					if (!Directory.Exists(fn.Directory))
						Directory.CreateDirectory(fn.Directory);
				}

				data ??= (byte[]) manager.GetObject(res.ResourceName);

				srcStream = Unzip(data);

				using (var dstStream = File.OpenWrite(fn))
					srcStream.CopyTo(dstStream);
			}
			catch (Exception ex)
			{
				Utils.Nop(ex);
			}
			finally
			{
				srcStream?.Dispose();
			}
		}		
	}

	static bool CompareStreams(Stream dstStream, Stream srcStream)
	{
		const int bytesToRead = 4096;

		var iterations = (int) Math.Ceiling((double) dstStream.Length/bytesToRead);

		var one = new byte[bytesToRead];
		var two = new byte[bytesToRead];

		for (var i = 0; i < iterations; i++)
		{
			dstStream.Read(one, 0, bytesToRead);
			srcStream.Read(two, 0, bytesToRead);

			for (var j = 0; j < bytesToRead; j += sizeof (ulong))
				if (BitConverter.ToInt64(one, j) != BitConverter.ToInt64(two, j))
					return false;

		}
		return true;
	}

	static Stream Unzip(byte[] data)
		=> new GZipStream(new MemoryStream(data), CompressionMode.Decompress, true);
}