using System.Xml.Serialization;

namespace Booster.Orm.NativeBin;

[XmlType("Res")]
public class NativeBinSettings
{
	[XmlAttribute] public string Platform { get; set; }
	[XmlAttribute] public string Path { get; set; }
	[XmlAttribute] public string ResourceName { get; set; }
	[XmlAttribute] public int Size { get; set; }
	[XmlAttribute] public bool SkipByteCheck { get; set; }
}