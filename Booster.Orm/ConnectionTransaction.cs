using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using Booster.Orm.Interfaces;

namespace Booster.Orm;

public class ConnectionTransaction : ITransaction
{
	public readonly IConnection Connection;
	public readonly ITransaction Transaction;

	public ConnectionTransaction(IConnection connection)
	{
		Connection = connection;
		Transaction = Connection.BeginTransaction();
	}

	public void Dispose()
	{
		Transaction.Dispose();
		Connection.Dispose();
	}

	public Task<DbTransaction> GetInnerAsync(CancellationToken token)
		=> Transaction.GetInnerAsync(token);

	public DbTransaction GetInner()
		=> Transaction.GetInner();

	public void Entry()
		=> Transaction.Entry();

	public void Commit()
		=> Transaction.Commit();

	public void Rollback()
		=> Transaction.Rollback();
}