using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Booster.Helpers;
using Booster.Log;
using Booster.Orm.Attributes;
using Booster.Orm.Interfaces;
using Booster.Orm.Kitchen;
using Booster.Reflection;
using JetBrains.Annotations;

#nullable enable

namespace Booster.Orm;

/// <summary>
/// Эта фабрика для создания Ормов. Не путать с BaseOrmFactory, который для создания объектов нужных орму. TODO: вообще неплохо было бы переименовать
/// </summary>
[PublicAPI]
public class OrmFactory
{
	readonly DbUpdateOptions _defaultDbUpdateOptions;
	readonly ILog? _log;
	readonly RepositoryRegistrationOptions? _repositoryRegistrationOptions;

	readonly Dictionary<string, Tuple<TypeEx, DbUpdateOptions, ILog?>> _ormTypes = new(StringComparer.OrdinalIgnoreCase);
	readonly Dictionary<TypeEx, Tuple<TypeEx, DbUpdateOptions, ILog?>> _ormTypes2 = new();

	public OrmFactory(
		ILog? log = null,
		DbUpdateOptions defaultDbUpdateOptions = DbUpdateOptions.Inherit,
		RepositoryRegistrationOptions? repositoryRegistrationOptions = null,
		bool scanAssemblies = true)
	{
		_log = log;
		_defaultDbUpdateOptions = defaultDbUpdateOptions;
		_repositoryRegistrationOptions = repositoryRegistrationOptions;

		if (scanAssemblies)
		{
			var ormAttr = typeof(OrmAssemblyAttribute).FullName;
			var path = Path.GetDirectoryName(AssemblyHelper.BoosterCallingAssembly.Location);
			if (path.IsSome())
				foreach (var dll in Directory.EnumerateFiles(path, "*.dll"))
					Try.IgnoreErrors(() =>
					{
						var assy = AssemblyHelper.ReflectionOnlyLoadFrom(dll);
						if (assy?.GetCustomAttributesData().Any(attr => attr.AttributeType.FullName == ormAttr) == true)
						{
							Assembly? loadedAssembly = null;

							foreach (TypeEx type in assy.GetExportedTypes())
							{
								if (type.FindInterface<IOrm>() == null || type.IsAbstract || type.IsInterface)
									continue;

								if (loadedAssembly == null)
									loadedAssembly = Assembly.LoadFrom(dll);

								Register(loadedAssembly.GetType(type.FullName), defaultDbUpdateOptions, log);
							}
						}
					});
		}
	}

	public void Register<TOrm>(DbUpdateOptions dbUpdateOptions = DbUpdateOptions.Inherit, ILog? log = null) where TOrm : IOrm
		=> Register(typeof(TOrm), dbUpdateOptions, log);

	public void Register(TypeEx type, DbUpdateOptions dbUpdateOptions = DbUpdateOptions.Inherit, ILog? log = null)
	{
		if (!type.HasInterface<IOrm>())
			throw new ArgumentException(nameof(type), $"{type.Name} is not implement IOrm.");

		var tup = Tuple.Create(type, dbUpdateOptions, log);
		var attr = type.FindAttribute<ProviderNamesAttribute>();
		if (attr != null)
			foreach (var name in attr.Names)
				_ormTypes[name] = tup;
		else
			_ormTypes[type.Name.Replace("Orm", "")] = tup;

		_ormTypes2[type] = tup;
	}

	public IOrm Create(string connectionString, string ormType, DbUpdateOptions defaultDbUpdateOptions = DbUpdateOptions.Inherit, ILog? log = null)
	{
		if (ormType.IsEmpty())
			throw new ArgumentException("Expected ormType", nameof(ormType));

		var tup = _ormTypes.SafeGet(ormType);
		if (tup == null)
			throw new ArgumentException($"Invalid ormType: {ormType}", nameof(ormType));

		if (defaultDbUpdateOptions == DbUpdateOptions.Inherit)
			defaultDbUpdateOptions = tup.Item2;

		log ??= tup.Item3;

		return Create(tup.Item1, connectionString, defaultDbUpdateOptions, log);
	}

	public IOrm Create(BaseConnectionSettings connectionSettings, DbUpdateOptions defaultDbUpdateOptions = DbUpdateOptions.Inherit, ILog? log = null)
	{
		var tup = _ormTypes2.SafeGet(connectionSettings.OrmType);
		if (tup != null)
		{
			if (defaultDbUpdateOptions == DbUpdateOptions.Inherit)
				defaultDbUpdateOptions = tup.Item2;

			log ??= tup.Item3;
		}

		return Create(connectionSettings.OrmType, connectionSettings, defaultDbUpdateOptions, log);
	}

	public IOrm Create(TypeEx ormType, object connectionSettings, DbUpdateOptions defaultDbUpdateOptions = DbUpdateOptions.Inherit, ILog? log = null)
	{
		if (defaultDbUpdateOptions == DbUpdateOptions.Inherit)
			defaultDbUpdateOptions = _defaultDbUpdateOptions;

		log ??= _log;

		var res = (IOrm) ormType.Create(connectionSettings, log);

		if (defaultDbUpdateOptions != DbUpdateOptions.Inherit && !res.ConnectionSettings.ContainsKey("DbUpdateOptions"))
			res.ConnectionSettings.DbUpdateOptions = defaultDbUpdateOptions;

		if (_repositoryRegistrationOptions != null)
			res.RepositoryRegistrationOptions = _repositoryRegistrationOptions.Clone();

		return res;
	}
}