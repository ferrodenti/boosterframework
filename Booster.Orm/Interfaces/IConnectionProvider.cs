﻿using System;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

#nullable enable

namespace Booster.Orm.Interfaces;

interface IConnectionProvider
{
	event EventHandler Disposed;

	void ClearPool();
	Task<DbConnection> ReconnectAsync(DbConnection? oldConnection, CancellationToken token);
	Task<DbConnection?> GetConnectionAsync(bool checkIsFull, CancellationToken token);
	DbConnection Reconnect(DbConnection? oldConnection);
	DbConnection? GetConnection(bool checkIsFull);
	void ReleaseConnectionAsync(DbConnection conn);
}