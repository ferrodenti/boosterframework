using System.Data.Common;

namespace Booster.Orm.Interfaces;

#nullable enable

public interface IRepoQueries
{
	string PkParamPrefix { get; }
	string WriteParamPrefix { get; }
	string Select { get; }
	string SelectCount { get; }
	string SelectAll { get; }
	string IdentityWhere { get; }
	string IdentityCondition { get; }
	string Insert { get; }
	string InsertWithPks { get; }
	string InsertReturning { get; }
	string Update { get; }
	string UpdateWithPks { get; }
	string Delete { get; }
	string DefaultSort { get; }
	string ReadUnmapped { get; }
	string WriteUnmapped { get; }
	string CreatePagingQuery(object skip, bool bSkip, int take, string condition, string? sort, ref DbParameter[]? parameters);
}