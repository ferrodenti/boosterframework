using System;
using System.Threading.Tasks;
using Booster.Reflection;
using JetBrains.Annotations;

namespace Booster.Orm.Interfaces;

[PublicAPI]
public interface IRepoInitTask
{
	TypeEx EntityType { get; }
	EntityInfo EntityInfo { get; }
	IRepoAdapterBuilder AdapterBuilder { get; set; }
	IRepository Repository { get; set; }
	IRepoAdapter Adapter { get; set; }
	RepoInitContext TableInfo { get; set; }

	bool WaitForAdapterReady(int timeout = -1);
	bool WaitForEventsHandled(int timeout = -1);
	Task WaitForEventsHandledAsync();

	void AddAction(Func<RepoInitContext, IConnection, Task> action);
	void AddAction(Action<RepoInitContext> action);
}