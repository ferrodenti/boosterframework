using System.Collections.Generic;
using System.Threading.Tasks;

namespace Booster.Orm.Interfaces;

public interface IRepoAdaptersFactory
{
	Task Compile(IEnumerable<IRepoInitTask> tasks);
}