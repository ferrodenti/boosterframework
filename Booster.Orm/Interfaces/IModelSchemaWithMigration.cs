using System.Threading.Tasks;
using Booster.Log;

#nullable enable

namespace Booster.Orm.Interfaces;

public interface IModelSchemaWithMigration : IModelSchema
{
	Task Migration(IRepoInitTask task, MigrationOptions stage, RepoInitContext e, IConnection connection, ILog? log);
}