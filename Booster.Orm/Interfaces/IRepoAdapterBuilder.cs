using Booster.CodeGen.Interfaces;

namespace Booster.Orm.Interfaces;

public interface IRepoAdapterBuilder : IClassBuilder
{
	string ClassName { get; }
}