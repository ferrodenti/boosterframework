using System.Collections;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

namespace Booster.Orm.Interfaces;

public interface IRepoAdapter
{
	void InitEntity(object model);

	void Insert(object model);
	int Update(object model);
	int Delete(object model);

	int InsertWithPks(object model);
	int UpdateWithPks(object model, object[] pks);

	IEnumerable Select();
	IEnumerable Query(string query, DbParameter[] parameters);
	object Load(string query, DbParameter[] parameters);
	object LoadByPks(object[] pks);

	TValue ReadUnmapped<TValue>(object entity, string fieldName);
	int WriteUnmapped<TValue>(object entity, string fieldName, TValue value);
				
	Task InsertAsync(object model, CancellationToken token);
	Task<int> UpdateAsync(object model, CancellationToken token);
	Task<int> DeleteAsync(object model, CancellationToken token);

	Task<int> InsertWithPksAsync(object model, CancellationToken token);
	Task<int> UpdateWithPksAsync(object model, object[] pks, CancellationToken token);
	
#if NETSTANDARD2_1
		System.Collections.Generic.IAsyncEnumerable<object> SelectAsync(CancellationToken token);
		System.Collections.Generic.IAsyncEnumerable<object> QueryAsync(string query, DbParameter[] parameters, CancellationToken token);
#else
	Booster.AsyncLinq.IAsyncEnumerable<object> SelectAsync(CancellationToken token);
	Booster.AsyncLinq.IAsyncEnumerable<object> QueryAsync(string query, DbParameter[] parameters, CancellationToken token);
#endif
	Task<object> LoadAsync(string query, DbParameter[] parameters, CancellationToken token);
	Task<object> LoadByPksAsync(object[] pks, CancellationToken token);

	Task<TValue> ReadUnmappedAsync<TValue>(object entity, string fieldName, CancellationToken token);
	Task<int> WriteUnmappedAsync<TValue>(object entity, string fieldName, TValue value, CancellationToken token);
}