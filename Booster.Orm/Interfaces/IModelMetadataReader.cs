using Booster.Reflection;

namespace Booster.Orm.Interfaces;

public interface IModelSchemaReader
{
	IModelSchemaWithMigration ReadModelSchema(TypeEx modelType);
}