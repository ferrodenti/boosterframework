using System;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using Booster.Orm.Kitchen;

namespace Booster.Orm.Interfaces;

public interface IOrmFactory
{
	bool CuncurrencyDebugWrappers { get; }
		
	IDbTypeDescriptor CreateDbTypeDescriptor();
	IDbManager CreateDbManager();
	DbParameter CreateParameter();
	Task<DbConnection> CreateDbConnectionAsync(IConnectionSettings settings, CancellationToken token);
	DbConnection CreateDbConnection(IConnectionSettings settings);

	Pool<DbConnection> CreateConnectionPool(IConnectionSettings settings);
	IConnection CreateConnection(IConnectionSettings setting, Action releasers);
	OrmAssemblies CreateAssemblyCache(string path);
	IRepoAdaptersFactory CreateRepoAdapterFactory(OrmAssemblies assemblyCache);
	IRepoAdapterBuilder CreateAdapterBuilder(ITableMapping mapping, IRepoQueries repoQueries, IDbTypeDescriptor typeDescriptor);
	IModelSchemaReader CreateModelSchemaReader();
	IRepository CreateRepo(ITableMapping mapping, IRepoQueries repoQueries);
	ISqlFormatter CreateSqlFormatter();
	IQueryCompiler CreateQueryCompiler();
	ITableMetadata CreateRelationMetadata(string tableName, bool isView);
	ITableSchema CreateTableSchema(string schema, string tableName, ITableMetadata metadata);
	IRepoQueries CreateRepoQueries(ITableMapping tableMapping);
	IRepoInitTask CreateRepoInitTask(EntityInfo entityType);
}