using System;
using System.Text;

namespace Booster.Orm;

public sealed class PingFailedException : Exception
{
	public string InitialConnectionString { get; }
	public string ResultingConnectionString { get; }

	public PingFailedException(Exception innerException, string initialConnectionString, string resultingConnectionString, bool removePassword = true) 
		: base($@"Ping failed
  Initial connection string={ProcessConnectionString(ref initialConnectionString, removePassword)}
Resulting connection string={ProcessConnectionString(ref resultingConnectionString, removePassword)}", innerException)
	{
		InitialConnectionString = initialConnectionString;
		ResultingConnectionString = resultingConnectionString;
		Data["InitialConnectionString"] = InitialConnectionString;
		Data["ResultingConnectionString"] = InitialConnectionString;
	}

	static string ProcessConnectionString(ref string str, bool removePassword)
	{
		if (str.IsEmpty())
		{
			str = "(null)";
			return str;
		}

		if (!removePassword)
			return str;

		var bld = new StringBuilder(str.Length);
		var start = 0;

		while (true)
		{
			var i = str.IndexOf("password", start, StringComparison.OrdinalIgnoreCase);
			if (i < 0)
			{
				bld.Append(str.Substring(start));
				break;
			}

			bld.Append(str.Substring(start, i - start));
			bld.Append("password=***");

			var j = str.IndexOf(';', i);
			if (j < 0)
				break;

			start = j;
		}

		return str = bld.ToString();
	}


}