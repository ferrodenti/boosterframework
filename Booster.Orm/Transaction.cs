using System;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
#if DEBUG && SQL_TRACE
#define DEBUG_SQL_TRACE
#endif
using Booster.Orm.Interfaces;

namespace Booster.Orm;

public class Transaction : ITransaction
{
	int _entries;

	readonly Func<CancellationToken, Task<DbTransaction>> _creatorAsync;
	readonly Func<DbTransaction> _creator;
	readonly Action<DbTransaction> _releaser;
	DbTransaction _inner;
		
	public async Task<DbTransaction> GetInnerAsync(CancellationToken token) 
		=> _inner ??= await _creatorAsync(token).NoCtx();

	public DbTransaction GetInner() 
		=> _inner ??= _creator();

	public Transaction(Func<CancellationToken, Task<DbTransaction>> creatorAsync,
		Func<DbTransaction> creator, Action<DbTransaction> releaser)
	{
		_creatorAsync = creatorAsync;
		_releaser = releaser;
		_creator = creator;
	}

	public void Entry() 
		=> Interlocked.Increment(ref _entries);

	public void Commit()
	{
		if (_entries <= 1)
		{
			_inner?.Commit();
			_releaser(_inner);
			_inner = null;
		}
	}

	public void Rollback()
	{
		_inner?.Rollback();
		_releaser(_inner);
		_inner = null;
	}

	public void Dispose()
	{
		if (Interlocked.Decrement(ref _entries) <= 0)
			Rollback();
	}
}