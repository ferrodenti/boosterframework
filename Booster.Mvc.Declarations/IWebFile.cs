﻿namespace Booster.Mvc.Declarations;

public interface IWebFile
{
	string FileName { get; set; }
	string ContentType { get; set; }
	byte[] Data { get; set; }
}