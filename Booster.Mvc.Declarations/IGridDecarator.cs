namespace Booster.Mvc.Declarations;

public interface IGridDecarator
{
	object Target { get; set; }
}

public interface IGridDecarator<TEntity> : IGridDecarator
{
	new TEntity Target { get; set; }
}