﻿namespace Booster.Mvc.Declarations;

public enum EditorType
{
	General,
	Password,
	Textarea,
	Int,
	Float,
	Date,
	Checkbox,
	Select,
	Custom,
	Icon,
	FilePath,
	FolderPath,
	File,
	Photo,
	Hidden,
	Suggest,
	Text, //TODO: add text type
}