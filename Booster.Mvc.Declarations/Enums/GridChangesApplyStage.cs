﻿namespace Booster.Mvc.Declarations;

public enum GridChangesApplyStage
{
	AfterSave,
	BeforeApply,
	BeforeSave
}