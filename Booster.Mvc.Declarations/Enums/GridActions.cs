using System;

namespace Booster.Mvc.Declarations;

[Flags]
[Serializable]
public enum GridActions
{
	None = 0,
	Insert = 1,
	Update = 2,
	Delete = 4,
	Save = Insert | Update,
	All = Insert | Update | Delete
}