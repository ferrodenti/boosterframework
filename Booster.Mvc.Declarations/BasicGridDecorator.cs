using JetBrains.Annotations;

namespace Booster.Mvc.Declarations;

[PublicAPI]
public class BasicGridDecorator<TEntity> : IGridDecarator<TEntity>
{
	public TEntity Target { get; set; }

	object IGridDecarator.Target
	{
		get => Target;
		set => Target = (TEntity)value;
	}

	public BasicGridDecorator()
	{
	}

	public BasicGridDecorator(TEntity target)
		=> Target = target;
}