using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace Booster.Mvc.Declarations;

[PublicAPI]
public class EntityChanges
{
	public bool IsApplied;
	public bool IsSaved;

	public GridActions Action;
		
	protected readonly Dictionary<string, Tuple<object, object>> Dict = new();
		
	public void Set(string field, object oldValue, object newValue)
	{
		if (!Equals(oldValue, newValue))
			Dict[field] = Tuple.Create(oldValue, newValue);
	}
		
	protected bool TryGetValue(string name, out object result)
	{
		if (name != null && Dict.TryGetValue(name, out var tup))
		{
			result = IsApplied ? tup.Item1 : tup.Item2;
			return true;
		}

		result = null;
		return false;
	}
		
	protected bool TryGetValue(string name, out object oldValue, out object newValue)
	{
		if (name != null && Dict.TryGetValue(name, out var tup))
		{
			oldValue = tup.Item1;
			newValue = tup.Item2;
			return true;
		}

		oldValue = null;
		newValue = null;
		return false;
	}
			
	public EntityChanges On(string field, Action<object> action)
	{
		if (TryGetValue(field, out var val))
			action(val);

		return this;
	}
		
	public EntityChanges On(string field, Action<object, object> action)
	{
		if (TryGetValue(field, out var oldVal, out var newVal))
			action(oldVal, newVal);

		return this;
	}
		
	public Task On(string field, Func<object, Task> action) 
		=> TryGetValue(field, out var val) ? action(val) : Task.CompletedTask;
		
	public Task On(string field, Func<object, object, Task> action) 
		=> TryGetValue(field, out var oldVal, out var newVal) ? action(oldVal, newVal) : Task.CompletedTask;

	public bool Has(string field)
		=> Dict.ContainsKey(field);
}
	
[PublicAPI]
public class EntityChanges<TEntity> : EntityChanges
{
	bool TryGetValue<TValue>(Expression<Func<TEntity, TValue>> field, out TValue result)
	{
		if (TryGetValue(field.ParseMemberRef()?.Name, out var obj))
		{
			result = (TValue) obj;
			return true;
		}

		result = default;
		return false;
	}
		
	bool TryGetValue<TValue>(Expression<Func<TEntity, TValue>> field, out TValue oldValue, out  TValue newValue)
	{
		if (TryGetValue(field.ParseMemberRef()?.Name, out var oldVal, out var newVal))
		{
			oldValue = (TValue) oldVal;
			newValue = (TValue) newVal;
			return true;
		}
		oldValue = default;
		newValue = default;
		return false;
	}
		
	public EntityChanges<TEntity> On<TValue>(Expression<Func<TEntity, TValue>> field, Action<TValue> action)
	{
		if (TryGetValue(field, out var val))
			action(val);

		return this;
	}
		
	public EntityChanges<TEntity> On<TValue>(Expression<Func<TEntity, TValue>> field, Action<TValue, TValue> action)
	{
		if (TryGetValue(field, out var oldVal, out var newVal))
			action(oldVal, newVal);

		return this;
	}
		
	public Task On<TValue>(Expression<Func<TEntity, TValue>> field, Func<TValue, Task> action) 
		=> TryGetValue(field, out var val) ? action(val) : Task.CompletedTask;
		
	public Task On<TValue>(Expression<Func<TEntity, TValue>> field, Func<TValue, TValue, Task> action) 
		=> TryGetValue(field, out var oldVal, out var newVal) ? action(oldVal, newVal) : Task.CompletedTask;

	public bool Has<TValue>(Expression<Func<TEntity, TValue>> field)
		=> field.ParseMemberRef().With(f => Has(f.Name));
}