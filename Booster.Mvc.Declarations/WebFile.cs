﻿#if !NETSTANDARD
using System.IO;
using System.Web;

namespace Booster.Mvc.Declarations
{
	public class WebFile : IWebFile
	{
		public string FileName { get; set; }
		public string ContentType { get; set; }
		public byte[] Data { get; set; }

		public WebFile(string fileName, byte[] data)
		{
			FileName = fileName;
			ContentType = MimeMapping.GetMimeMapping(fileName);
			Data = data;
		}

		public WebFile(string fileName, string contentType, byte[] data)
		{
			FileName = fileName;
			ContentType = contentType;
			Data = data;
		}

		public WebFile(HttpPostedFile file)
		{
			FileName = Path.GetFileName(file.FileName);
			ContentType = file.ContentType;
			Data = new byte[file.ContentLength];

			var _ = file.InputStream.Read(Data, 0, file.ContentLength);
		}
	}
}
#endif