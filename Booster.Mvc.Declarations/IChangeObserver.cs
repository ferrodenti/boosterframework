using System;

namespace Booster.Mvc.Declarations;

[Obsolete("Use GridChangesApplyingAttribute/GridChangesAppliedAttribute")]
public interface IChangeObserver
{
	void Changed(string field, object oldValue);
}