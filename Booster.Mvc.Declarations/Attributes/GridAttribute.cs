﻿using System;
using Booster.Interfaces;
using JetBrains.Annotations;

// ReSharper disable once CheckNamespace
namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
[MeansImplicitUse]
public class GridAttribute : BaseEditorAttribute, ISchemaAttribute
{
	public object DisplayColumn { get; set; } = true;
	public object DisplayEditor { get; set; } = true;
	public bool Sortable { get; set; } = true;
	public bool PreserveHtml { get; set; }
	public string SortExpression { get; set; }

	public GridAttribute() { }
	public GridAttribute(string displayName) : base(displayName) { }
	public GridAttribute(string displayName, int order) : base(displayName, order) { }
	public GridAttribute(int order) : base(order) { }

	public void ApplySettings(IDynamicSettings settings)
	{
		if (EmptyText.IsSome()) settings.Add("emptyText", EmptyText);
	}
}