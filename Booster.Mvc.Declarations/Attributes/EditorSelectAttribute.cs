﻿using System;
using Booster.Interfaces;

namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
public class EditorSelectAttribute : EditorAttribute
{
	public string Values { get; set; }
	public string Delimiter { get; set; }
	public bool Multiple { get; set; }
	public bool Combo { get; set; }
		
	public EditorSelectAttribute() : base(EditorType.Select) { }
	public EditorSelectAttribute(string displayName) : base(displayName, EditorType.Select) { }
	public EditorSelectAttribute(string displayName, int order) : base(displayName, order, EditorType.Select) { }

	public override void ApplySettings(IDynamicSettings settings)
	{
		base.ApplySettings(settings);

		if (Values.IsSome()) settings.Add("selectValues", Values);
		if (Delimiter.IsSome()) settings.Add("delimiter", Delimiter);

		settings.Add("multiple", Multiple ? "true" : "false"); //TODO: object?s
		settings.Add("combo", Combo ? "true" : "false");
	}
}