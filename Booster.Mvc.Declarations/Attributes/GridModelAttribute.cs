﻿using System;
using Booster.Reflection;

namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Class)]
public class GridModelAttribute : Attribute, IGridModelAttribute
{
	public string Caption;
	public string ShortName;
	public string DeleteText;
	public string EditText;
	public string InsertText;
	public string ControllerKey;
	public TypeEx ControllerType;
	public GridSyncType SyncType;

	public GridModelAttribute()
	{
	}

	public GridModelAttribute(string caption, Type controllerType = null)
	{
		Caption = caption;
		ControllerType = controllerType;
	}
}