﻿using System;
using JetBrains.Annotations;

namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
[MeansImplicitUse, PublicAPI]
public class GridChangesAppliedAttribute : Attribute
{
	public GridActions Actions = GridActions.All;
	public GridChangesApplyStage Stage = GridChangesApplyStage.AfterSave;

	public bool ShouldInvoke(GridChangesApplyStage stage, EntityChanges changes) 
		=> Stage == stage && Actions.HasFlag(changes.Action);

	public GridChangesAppliedAttribute()
	{
	}
	public GridChangesAppliedAttribute(GridChangesApplyStage stage)
		=> Stage = stage;

	public GridChangesAppliedAttribute(GridChangesApplyStage stage, GridActions actions)
	{
		Stage = stage;
		Actions = actions;
	}
}