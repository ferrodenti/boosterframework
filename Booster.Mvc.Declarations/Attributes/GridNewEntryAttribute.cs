﻿using System;
using JetBrains.Annotations;

namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Method)]
[MeansImplicitUse]
public class GridNewEntryAttribute : Attribute
{
}