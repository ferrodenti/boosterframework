﻿namespace Booster.Mvc.Declarations;

public enum GridSyncType
{
	None,
	Actions,
	Orm
}