﻿using System;

namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
public class EditorPasswordAttribute : EditorAttribute
{
	public EditorPasswordAttribute() : base(EditorType.Password) => Placeholder = "******";
	public EditorPasswordAttribute(string displayName) : base(displayName, EditorType.Password) => Placeholder = "******";
	public EditorPasswordAttribute(string displayName, int order) : base(displayName, order, EditorType.Password) => Placeholder = "******";
}