﻿using System;

namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Class)]
public class GridUserSelectAttribute : Attribute
{
	public string SessionKey { get; }

	public GridUserSelectAttribute(string sessionKey)
        => SessionKey = sessionKey;
}