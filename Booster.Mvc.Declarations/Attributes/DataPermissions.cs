﻿namespace Booster.Mvc.Declarations;

public class DataPermissions
{
	public object Read { get; set; }
	public object Write { get; set; }
	public object Delete { get; set; }
	public object Insert { get; set; }
	public object Reorder { get; set; }
	public object Select { get; set; }
		
	public object Any
	{
		set => Read = Select = Delete = Insert = Reorder = Write = value;
	}
	public object AnyRead
	{
		set => Read = Select = value;
	}
	public object AnyWrite
	{
		set => Delete = Insert = Reorder = Write = value;
	}
}