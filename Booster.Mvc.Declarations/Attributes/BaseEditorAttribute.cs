﻿using System;

namespace Booster.Mvc.Declarations;

public class BaseEditorAttribute : Attribute, IProxyAccess
{
	public string DisplayName { get; set; }
	public string EmptyText { get; set; }
	public int Order { get; set; }

	public BaseEditorAttribute()
	{
	}

	public BaseEditorAttribute(string dispayName)
		=> DisplayName = dispayName;

	public BaseEditorAttribute(string dispayName, int order)
	{
		Order = order;
		DisplayName = dispayName;
	}

	public BaseEditorAttribute(int order)
		=> Order = order;
}