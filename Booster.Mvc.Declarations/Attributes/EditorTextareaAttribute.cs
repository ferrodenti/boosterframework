using System;
using Booster.Interfaces;

namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
public class EditorTextareaAttribute : EditorAttribute
{
	public int Cols { get; set; } = -1;
	public int Rows { get; set; } = -1;

	public EditorTextareaAttribute() : base(EditorType.Textarea) { }
	public EditorTextareaAttribute(string displayName) : base(displayName, EditorType.Textarea) { }
	public EditorTextareaAttribute(string displayName, int order) : base(displayName, order, EditorType.Textarea) { }

	public override void ApplySettings(IDynamicSettings settings)
	{
		base.ApplySettings(settings);

		if (Cols >= 0)
			settings.Add("cols", Cols);

		if (Rows >= 0)
			settings.Add("rows", Rows);
	}
}