﻿using System;

namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Class)]
public class GridEditUrlAttribute : Attribute
{
	public string EditUrl { get; set; }
	public string InsertUrl { get; set; }

	public bool NewTab { get; set; }

	public GridEditUrlAttribute(string editUrl)
        => EditUrl = editUrl;
}