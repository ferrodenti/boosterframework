﻿using System;
using Booster.Interfaces;
using JetBrains.Annotations;

namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
[MeansImplicitUse]
public class EditorAttribute : BaseEditorAttribute, ISchemaAttribute //TODO: непонятно существование отдельных BaseEditorAttribute, EditorAttribute, GridAttribute
{
	public EditorType EditorType { get; set; }
	public string PreHtml { get; set; }
	public string PostHtml { get; set; }
	public string Placeholder { get; set; }
	public string DefaultValue { get; set; }
	public object Visibility { get; set; }
	public object ControlSize { get; set; } //TODO:
	public object LabelSize { get; set; } //TODO:

	public bool ReplaceEmptyWithDefault { get; set; } = true;

	public EditorAttribute() { }

	public EditorAttribute(string displayName) : base(displayName) { }
	public EditorAttribute(EditorType editorType)
        => EditorType = editorType;

    public EditorAttribute(string displayName, EditorType editorType) : base(displayName)
        => EditorType = editorType;

    public EditorAttribute(string displayName, int order) : base(displayName, order)
	{
	}

	public EditorAttribute(string displayName, int order, EditorType editorType) : base(displayName, order)
        => EditorType = editorType;

    public virtual void ApplySettings(IDynamicSettings settings)
	{
		if (PreHtml.IsSome()) settings.Add("preHtml", PreHtml);
		if (PostHtml.IsSome()) settings.Add("postHtml", PostHtml);
		if (Placeholder.IsSome()) settings.Add("placeholder", Placeholder);
		if (DefaultValue.IsSome()) settings.Add("defaultValue", DefaultValue);
		if (DisplayName.IsSome()) settings.Add("DisplayName", DisplayName);
		if (Visibility != null) settings.Add("visibility", Visibility);
		if (ControlSize != null) settings.Add("controlSize", ControlSize);
		if (LabelSize != null) settings.Add("labelSize", LabelSize);
		if (EmptyText.IsSome()) settings.Add("emptyText", EmptyText);
	}
}