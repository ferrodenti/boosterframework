﻿using System;

namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
public class EditorFolderPathAttribute : EditorFilePathAttribute
{
	public EditorFolderPathAttribute()
        => EditorType = EditorType.FolderPath;

    public EditorFolderPathAttribute(string displayName) : base(displayName)
        => EditorType = EditorType.FolderPath;
        
    public EditorFolderPathAttribute(string displayName, int order) : base(displayName, order)
        => EditorType = EditorType.FolderPath;
}