﻿using System;
using Booster.Interfaces;

namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
public class EditorDateTimeAttribute : EditorAttribute
{
	public string Format { get; set; }

	public EditorDateTimeAttribute() : base(EditorType.Date) { }
	public EditorDateTimeAttribute(string displayName) : base(displayName, EditorType.Date) { }
	public EditorDateTimeAttribute(string displayName, int order) : base(displayName, order, EditorType.Date) { }

	public override void ApplySettings(IDynamicSettings settings)
	{
		base.ApplySettings(settings);

		if (Format.IsSome())
			settings.Add("format", Format);
	}
}