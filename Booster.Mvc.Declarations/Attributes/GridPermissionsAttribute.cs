﻿using System;

namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Field | AttributeTargets.Property)]
public class GridPermissionsAttribute : GridAttribute, IGridModelAttribute
{
	public DataPermissions Permissions { get; } = new();
	public object Read { get => Permissions.Read; set => Permissions.Read = value; }
	public object Write { get => Permissions.Write; set => Permissions.Write = value; }
	public object Delete { get => Permissions.Delete; set => Permissions.Delete = value; }
	public object Insert { get => Permissions.Insert; set => Permissions.Insert = value; }
	public object Reorder { get => Permissions.Reorder; set => Permissions.Reorder = value; }
	public object Select { get => Permissions.Select; set => Permissions.Select = value; }

	/// <summary>
	/// Задает разрешения для грида. True в аргументе приведет к ошибке!
	/// </summary>
	/// <param name="all">Значение необходимых разрешений для любых операций, которое потом будет распознано с помощью IGridPermissionsProvider. 
	/// Можно указать false чтобы разрешения не зависили от пользователя. 
	/// Можно указать массив значений (доступ пользователю будет разрешен, если пройдет проверку хотя бы один элемент массива)</param>
	public GridPermissionsAttribute(object all)
	{
		if (Equals(all, true))
			throw new ArgumentException("GridPermissions(true) больше не используем");

		Permissions.Any = all;
	}
		
	/// <summary>
	/// Задает разрешения для грида. True в аргументах приведет к ошибке!
	/// </summary>
	/// <param name="read">Значение необходимых разрешений для операций просмотра и выборки записей, которое потом будет распознано с помощью IGridPermissionsProvider. 
	/// Можно указать false чтобы разрешения не зависили от пользователя. 
	/// Можно указать массив значений (доступ пользователю будет разрешен, если пройдет проверку хотя бы один элемент массива)</param>
	/// <param name="write">Значение необходимых разрешений для операций создания, удаления редактирования, упорядочивания записей, которое потом будет распознано с помощью IGridPermissionsProvider. 
	/// Можно указать false чтобы разрешения не зависили от пользователя. 
	/// Можно указать массив значений (доступ пользователю будет разрешен, если пройдет проверку хотя бы один элемент массива)</param>
	public GridPermissionsAttribute(object read, object write)
	{
		if (Equals(read, true) || Equals(write, true))
			throw new ArgumentException("GridPermissions(true) больше не используем");

		Permissions.AnyRead = read;
		Permissions.AnyWrite = write;
	}
}