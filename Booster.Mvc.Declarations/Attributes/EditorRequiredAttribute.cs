﻿using System;
using Booster.Interfaces;

namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
public class EditorRequiredAttribute : Attribute, ISchemaAttribute //TODO: унаследовать от EditorAttribute
{
	public object Required { get; set; } = true;
	public object NonDefault { get; set; } = true;
	public string RequiredText { get; set; }

	public EditorRequiredAttribute()
	{
	}

	public EditorRequiredAttribute(string text)
		=> RequiredText = text;

	public void ApplySettings(IDynamicSettings settings)
	{
		if (Required != null) settings.Add("required", Required);
		if (NonDefault != null) settings.Add("requiredNonDefault", NonDefault);
		if (RequiredText != null) settings.Add("requiredText", RequiredText);
	}
}