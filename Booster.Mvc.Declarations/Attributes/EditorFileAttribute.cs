﻿using System;
using Booster.Interfaces;

namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
public class EditorFileAttribute : EditorAttribute
{
	public string FileName { get; set; }
	public string Exists { get; set; }
	public string UploadText { get; set; }
	public string DeleteText { get; set; }
		
		
	public EditorFileAttribute() : base(EditorType.File) { }
	public EditorFileAttribute(string displayName) : base(displayName, EditorType.File) { }
	public EditorFileAttribute(string displayName, int order) : base(displayName, order, EditorType.File) { }

	public override void ApplySettings(IDynamicSettings settings)
	{
		base.ApplySettings(settings);

		if (FileName.IsSome()) settings.Add("fileName", FileName);
		if (Exists.IsSome()) settings.Add("exists", Exists);
		if (UploadText.IsSome()) settings.Add("uploadText", UploadText);
		if (DeleteText.IsSome()) settings.Add("deleteText", DeleteText);
	}
}