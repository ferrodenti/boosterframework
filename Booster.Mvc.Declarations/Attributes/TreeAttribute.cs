﻿using System;
using Booster.Reflection;

namespace Booster.Mvc.Declarations;

public class TreeAttribute : Attribute
{
	public string DispayName { get; set; }
	public string NodeType { get; set; }
	public string Id { get; set; }
	public int Order { get; set; }

	public TreeAttribute(string id)
        => Id = id;

    public TreeAttribute(string id, string displayName)
	{
		DispayName = displayName;
		Id = id;
	}
	public TreeAttribute(string id, string displayName, string nodeType)
	{
		DispayName = displayName;
		NodeType = nodeType;
		Id = id;
	}
}

public class TreeIdAttribute : Attribute
{
}
public class TreeNameAttribute : Attribute
{
}

[AttributeUsage( AttributeTargets.Field | AttributeTargets.Property, AllowMultiple= true)]
public class TreeArrayItemAttribute : TreeAttribute
{
	public TypeEx Type { get; set; }

	public TreeArrayItemAttribute(string id, TypeEx type)
		: base(id)
        => Type = type;

    public TreeArrayItemAttribute(string id, TypeEx type, string displayName)
		: base(id, displayName, null)
        => Type = type;

    public TreeArrayItemAttribute(string id, TypeEx type, string displayName, string nodeType)
		: base(id, displayName, nodeType)
        => Type = type;
}