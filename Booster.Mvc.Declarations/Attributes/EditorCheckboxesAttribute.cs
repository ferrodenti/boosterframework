﻿using System;
using Booster.Interfaces;
using JetBrains.Annotations;

namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
[PublicAPI]
public class EditorCheckboxesAttribute : EditorSelectAttribute
{
	public EditorCheckboxesAttribute()
        => Multiple = true;
    public EditorCheckboxesAttribute(string displayName) : base(displayName)
        => Multiple = true;
    public EditorCheckboxesAttribute(string displayName, int order) : base(displayName, order)
        => Multiple = true;

    public override void ApplySettings(IDynamicSettings settings)
	{
		base.ApplySettings(settings);
		settings.Add("checkboxes", "true");
		settings.Add("delimiter", ",");
	}
}