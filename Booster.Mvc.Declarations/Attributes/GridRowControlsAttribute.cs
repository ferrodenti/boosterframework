﻿using System;
using JetBrains.Annotations;

namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field)]
[MeansImplicitUse, PublicAPI]
public class GridRowControlsAttribute : Attribute
{
	public object Override { get; set; }
	public string Value { get; }

	public GridRowControlsAttribute()
	{
	}
		
	public GridRowControlsAttribute(string value)
		=> Value = value;
}