﻿using System;
using JetBrains.Annotations;

namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
[MeansImplicitUse]
public class GridHtmlAttribute : GridAttribute
{
	public GridHtmlAttribute()
        => PreserveHtml = true;

    public GridHtmlAttribute(string displayName) : base(displayName)
        => PreserveHtml = true;

    public GridHtmlAttribute(string displayName, int order) : base(displayName, order)
        => PreserveHtml = true;

    public GridHtmlAttribute(int order) : base(order)
        => PreserveHtml = true;
}