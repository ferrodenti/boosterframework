﻿using System;
using Booster.Interfaces;
using JetBrains.Annotations;

namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property), PublicAPI]
public class EditorSuggestAttribute : EditorAttribute
{
	public string Url { get; set; }
	public string Text { get; set; }

	public EditorSuggestAttribute() : base(EditorType.Suggest) { }
	public EditorSuggestAttribute(string displayName) : base(displayName, EditorType.Suggest) { }
	public EditorSuggestAttribute(string displayName, int order) : base(displayName, order, EditorType.Select) { }

	public override void ApplySettings(IDynamicSettings settings)
	{
		base.ApplySettings(settings);
		if (Url.IsSome()) settings.Add("url", Url);
		if (Text.IsSome()) settings.Add("text", Text);
	}
}