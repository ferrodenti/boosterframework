﻿using System;
using Booster.Interfaces;

namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
public class EditorFilePathAttribute : EditorAttribute
{
	public string Controller { get; set; }
	public string Context { get; set; }
	public string RelBaseDir { get; set; }

	public EditorFilePathAttribute() : base(EditorType.FilePath) { }
	public EditorFilePathAttribute(string displayName) : base(displayName, EditorType.FilePath) { }
	public EditorFilePathAttribute(string displayName, int order) : base(displayName, order, EditorType.FilePath) { }

	public override void ApplySettings(IDynamicSettings settings)
	{
		base.ApplySettings(settings);

		if (Controller.IsSome()) settings.Add("controller", Controller);
		if (Context.IsSome()) settings.Add("context", Context);
		if (RelBaseDir.IsSome()) settings.Add("baseDir", RelBaseDir);
	}
}