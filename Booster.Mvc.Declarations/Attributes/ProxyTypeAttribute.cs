﻿using System;

namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Class)]
public class ProxyTypeAttribute : Attribute
{
	public Type Type;
	public ProxyTypeAttribute(Type type) 
		=> Type = type ?? throw new ArgumentNullException(nameof(type));
}