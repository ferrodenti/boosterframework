﻿using System;
using JetBrains.Annotations;

namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
[PublicAPI]
public class GridNoEditorAttribute : GridAttribute
{
	public GridNoEditorAttribute() => DisplayEditor = false;

	public GridNoEditorAttribute(string displayName) : base(displayName) => DisplayEditor = false;

	public GridNoEditorAttribute(string displayName, int order) : base(displayName, order) => DisplayEditor = false;

	public GridNoEditorAttribute(int order) : base(order) => DisplayEditor = false;
}