using System;
using JetBrains.Annotations;

namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Class)]
[MeansImplicitUse]
public class GridRowNumberAttribute : GridAttribute
{
	public GridRowNumberAttribute(string name) : base(name)
	{
	}
	public GridRowNumberAttribute(string name, int order) : base(name, order)
	{
	}
}