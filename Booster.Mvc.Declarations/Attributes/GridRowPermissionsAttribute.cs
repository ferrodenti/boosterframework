using System;

namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Class)]
public class GridRowPermissionsAttribute : Attribute
{
	public DataPermissions Permissions { get; } = new();

	public object Read 
	{ 
		get => Permissions.Read;
		set => Permissions.Read = value;
	}
	public object Write
	{ 
		get => Permissions.Write;
		set => Permissions.Write = value;
	}
	public object Delete
	{ 
		get => Permissions.Delete;
		set => Permissions.Delete = value;
	}
	public object Insert
	{ 
		get => Permissions.Insert;
		set => Permissions.Insert = value;
	}
	public object Reorder
	{ 
		get => Permissions.Reorder;
		set => Permissions.Reorder = value;
	}
	public object Select
	{ 
		get => Permissions.Select;
		set => Permissions.Select = value;
	}

	public GridRowPermissionsAttribute(object all)
        => Permissions.Any = all;

    public GridRowPermissionsAttribute(object read, object write)
	{
		Permissions.AnyRead = read;
		Permissions.AnyWrite = write;
	}
}