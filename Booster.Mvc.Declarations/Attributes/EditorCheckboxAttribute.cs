﻿using System;

namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
public class EditorCheckboxAttribute : EditorAttribute
{
	public EditorCheckboxAttribute() : base(EditorType.Checkbox) { }
	public EditorCheckboxAttribute(string displayName) : base(displayName, EditorType.Checkbox) { }
	public EditorCheckboxAttribute(string displayName, int order) : base(displayName, order, EditorType.Checkbox) { }
}