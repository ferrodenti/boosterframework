﻿using System;
using Booster.Interfaces;

namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
public class EditorCustomSettingsAttribute : BaseEditorAttribute, ISchemaAttribute
{
	public object[] Settings { get; }

	public EditorCustomSettingsAttribute(params object[] settings)
        => Settings = settings;

    public void ApplySettings(IDynamicSettings settings)
	{
		if (Settings.Length%2 != 0)
			throw new Exception("Expected even settings length (key-value pairs)");

		for (var i = 0; i < Settings.Length; i += 2)
			settings.Add(Settings[i].ToString(), Settings[i + 1]);
	}
}