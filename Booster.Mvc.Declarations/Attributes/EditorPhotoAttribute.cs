﻿using System;

namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
public class EditorPhotoAttribute : EditorFileAttribute
{
	public EditorPhotoAttribute()
        => EditorType = EditorType.Photo;
    public EditorPhotoAttribute(string displayName) : base(displayName)
        => EditorType = EditorType.Photo;
    public EditorPhotoAttribute(string displayName, int order) : base(displayName, order)
        => EditorType = EditorType.Photo;
}