﻿using System;
using JetBrains.Annotations;

namespace Booster.Mvc.Declarations;

[AttributeUsage(AttributeTargets.Class)]
public class EditorViewAttribute : Attribute
{
	public string View { get; set; }

	public EditorViewAttribute([PathReference]string view)
        => View = view;
}