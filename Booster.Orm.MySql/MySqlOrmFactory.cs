﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Booster.Log;
using Booster.Orm.Interfaces;
using Booster.Orm.Kitchen;
using JetBrains.Annotations;
using msc = MySqlConnector;

namespace Booster.Orm.MySql;

public class MySqlOrmFactory : BaseOrmFactory
{
	readonly MySqlOrm _orm;

	[PublicAPI]
	public Action<MySqlConnectionSettings, List<QueryBuilder>> SessionSetup;
		
	public MySqlOrmFactory(MySqlOrm orm, ILog log) : base(orm, log)
		=> _orm = orm;

	public override IQueryCompiler CreateQueryCompiler()
		=> throw new NotImplementedException();

	public override IDbTypeDescriptor CreateDbTypeDescriptor() 
		=> new MySqlTypeDescriptor(_orm);

	public override IDbManager CreateDbManager() 
		=> new MySqlDbManager(Owner, Log);

	public override IRepoAdapterBuilder CreateAdapterBuilder(ITableMapping mapping, IRepoQueries repoQueries, IDbTypeDescriptor typeDescriptor) 
		=> new MySqlRepoAdapterBuilder((MySqlOrm)Owner, mapping, typeDescriptor, repoQueries, Log);

	public override ISqlFormatter CreateSqlFormatter() 
		=> new MySqlSqlFormatter(Owner);

	public override DbParameter CreateParameter() 
		=> new msc.MySqlParameter();

	public override ITableMetadata CreateRelationMetadata(string tableName, bool isView) 
		=> new MySqlTableMetadata(Owner, tableName, isView, Log);

	public override IRepoQueries CreateRepoQueries(ITableMapping tableMapping) 
		=> new MySqlSqlRepoQueries(Owner, tableMapping);

	public override IConnection CreateConnection(IConnectionSettings settings, Action releaser) 
		=> new MySqlConnection(settings, releaser, (MySqlOrm)Owner, Log);

	public override async Task<DbConnection> CreateDbConnectionAsync(IConnectionSettings settings, CancellationToken token)
	{
		var conn = new msc.MySqlConnection(settings.ConnectionString);
		await conn.OpenAsync(token).NoCtx();

		foreach(var cmd in SetupSession(settings as MySqlConnectionSettings, conn))
			try
			{
				await cmd.ExecuteNonQueryAsync(token).NoCtx();
			}
			finally
			{
				cmd.Dispose();
			}

		return conn;
	}

	public override DbConnection CreateDbConnection(IConnectionSettings settings)
	{
		var conn = new msc.MySqlConnection(settings.ConnectionString);
		conn.Open();

		foreach(var cmd in SetupSession(settings as MySqlConnectionSettings, conn))
			try
			{
				cmd.ExecuteNonQuery();
			}
			finally
			{
				cmd.Dispose();
			}

		return conn;
	}

	protected msc.MySqlCommand[] SetupSession(MySqlConnectionSettings settings, msc.MySqlConnection connection)
	{
		var commands = new List<QueryBuilder>();

		if (settings != null)
		{
			if (settings.Schema.IsSome())
				commands.Add(new QueryBuilder(_orm, $"ALTER SESSION SET CURRENT_SCHEMA={settings.Schema}"));

			if (settings.DateFormat.IsSome())
				commands.Add(new QueryBuilder(_orm, $"ALTER SESSION SET nls_date_format='{settings.DateFormat}'"));
		}

		SessionSetup?.Invoke(settings, commands);
			
		return commands.Select(bld =>
		{
			var parameters = _orm.CreateParameters(bld.Parameters);
			var cmd = new msc.MySqlCommand(bld.Query, connection);
			cmd.Parameters.AddRange(parameters);
			return cmd;
		}).ToArray();
	}
}