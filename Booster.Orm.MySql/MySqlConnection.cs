﻿using System;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using Booster.Log;
using Booster.Orm.Interfaces;
using Booster.Orm.Kitchen;

namespace Booster.Orm.MySql;

public class MySqlConnection : BaseConnection
{
	protected new readonly MySqlConnectionSettings ConnectionSettings;
		
	public MySqlConnection(IConnectionSettings  connectionSettings, Action releaser, MySqlOrm orm, ILog log)
		: base (connectionSettings, releaser, orm, log)
		=> ConnectionSettings = connectionSettings as MySqlConnectionSettings;

	protected override bool IsConnectionBroken(Exception ex)
	{
		return false;
	}

	public override async Task<DbCommand> CreateCommandAsync(string command, DbParameter[] parameters, CancellationToken token = default)
	{
		var cmd = await base.CreateCommandAsync(TrimCommandEnd(command), parameters, token).NoCtx();
		return ApplyCommandParameters(cmd);
	}

	public override DbCommand CreateCommand(string command, DbParameter[] parameters)
	{
		var cmd = base.CreateCommand(TrimCommandEnd(command), parameters);
		return ApplyCommandParameters(cmd);
	}

	internal static string TrimCommandEnd(string command)
		=> command.TrimEnd(' ', '\t', '\n', '\r', ';');

	DbCommand ApplyCommandParameters(DbCommand cmd)
	{
		//var oraCmd = (DbCommand) cmd;
			
		//oraCmd.BindByName = true;

		//if (ConnectionSettings?.CommandTimeout > 0)
		//oraCmd.CommandTimeout = ConnectionSettings.CommandTimeout;

		return cmd;
	}
		
	protected override FormattableString GetPingCommand()
		=> $"select sysdate from dual";
}