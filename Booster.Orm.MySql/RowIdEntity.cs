using System;

namespace Booster.Orm.MySql;

[Serializable]
public abstract class RowIdEntity<TThis> : Entity<TThis, string>
{
	[DbPk(Mapping = "ROWID", IsRowId = true, UpdateOptions = DbUpdateOptions.Deny), DbType("ROWID"), DbDefaultSort]
	public override string Id { get => base.Id;
		set => base.Id = value;
	}
}