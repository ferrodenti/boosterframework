﻿using System;
using System.Data;
using Booster.Orm.Kitchen;
using Booster.Reflection;
using msc = MySqlConnector;

#nullable enable

namespace Booster.Orm.MySql;

public class MySqlTypeDescriptor : BaseDbTypeDescriptor
{
	public readonly bool DefaultUnicodeStrings;
		
	public MySqlTypeDescriptor(MySqlOrm orm) : base(orm)
		=> DefaultUnicodeStrings = orm.DefaultUnicodeStrings;

	public override Method GetDataReaderMethod(TypeEx type)
	{
		if (type == typeof (char))
			return base.GetDataReaderMethod(typeof (string));

		if (type == typeof (Guid))
			return base.GetDataReaderMethod(typeof (object));

		if (type == typeof (decimal)) //TODO: generate warnings when using decimal+MySql
			return base.GetDataReaderMethod(typeof (float)); //Weird MySql büg: http://tinyurl.com/hfzr25p

		return base.GetDataReaderMethod(type);
	}

	public override TypeEx GetValueType(TypeEx type)
	{
		if (type == typeof (Guid))
			return typeof (byte[]);

		return base.GetValueType(type);
	}
	
	public override TypeEx? GetType(string? dbType, int length, int scale)
	{
		switch (dbType?.ToLower())
		{
			case "number":
			case "numeric":
				if (scale <= 0)
				//if (precision <= 3)
				//    return typeof (byte);
				//if (precision <= 5)
				//    return typeof (short);
					return length <= 10 ? typeof (int) : typeof (long);

				return typeof (decimal);

			case "integer":
				return typeof (decimal);

			case "boolean":
				return typeof (bool);
				
			case "binary_double":
			case "double precision":
				return typeof (double);

			case "binary_float":
				return typeof (float);
			case "float":
				return length == 126 ? typeof (decimal) : typeof (float);

			case "timestamp":
			case "timestamp with local time zone":
			case "timestamp with time zone":
			case "time without time zone":
			case "time with time zone":
			case "date":
				return typeof (DateTime);

			case "char":
			case "character varying":
			case "varchar":
			case "varchar2":
			case "nchar":
			case "nvarchar2":
				return length <= 1 ? typeof(char) : typeof(string);

			case "clob":
			case "nclob":
			case "long":
			case "rowid":
				return typeof (string);

			case "blob":
			case "bfile":
			case "long raw":
			case "raw":
				return typeof (byte[]);
		}
		return null;
	}

	public override string? GetDbTypeFor(TypeEx type)
	{
		if (type.Is<Guid>())
			return "RAW(16)";

		return base.GetDbTypeFor(type);
	}

	protected override string? GetTypeInt(TypeEx type)
	{
		switch (type.TypeCode)
		{
			case TypeCode.Boolean:
				return "char";

			case TypeCode.Char:
				return "nchar";

			case TypeCode.SByte:
			case TypeCode.Byte:
				return "number(3)";

			case TypeCode.Int16:
			case TypeCode.UInt16:
				return "number(5)";

			case TypeCode.Int32:
			case TypeCode.UInt32:
				return "number(10)";

			case TypeCode.Int64:
			case TypeCode.UInt64:
				return "number(20)";

			case TypeCode.Single:
				return "binary_float";

			case TypeCode.Double:
				return "binary_double";
			case TypeCode.Decimal:
				return "float(126)";

			case TypeCode.DateTime:
				return "timestamp(6)";

			case TypeCode.String:
				return DefaultUnicodeStrings ? "nvarchar2(2000)" : "varchar2(2000)";
		}

		if (type == typeof (byte[]))
			return "blob";

		return null;
	}


	public msc.MySqlDbType GetMySqlDbType(TypeEx type)
	{
		type = GetValueType(type);

		switch (type.TypeCode)
		{
			case TypeCode.Int16:
				return msc.MySqlDbType.Int16;
			case TypeCode.Int32:
				return msc.MySqlDbType.Int32;
			case TypeCode.Int64:
				return msc.MySqlDbType.Int64;
			case TypeCode.Single:
				return msc.MySqlDbType.Float;
			case TypeCode.Double:
				return msc.MySqlDbType.Double;
			case TypeCode.Decimal:
				return msc.MySqlDbType.Decimal;
			case TypeCode.DateTime:
				return msc.MySqlDbType.Date;
			case TypeCode.String:
				return msc.MySqlDbType.VarChar;
			case TypeCode.Boolean:
			case TypeCode.Char:
				return msc.MySqlDbType.Bool;
			//#if !ODAC
			case TypeCode.Empty:
			case TypeCode.DBNull:
				return msc.MySqlDbType.Null;
			//#endif
		}

		if (type == typeof (byte[]))
			return msc.MySqlDbType.Blob;

		throw new ArgumentOutOfRangeException();
	}

	public DbType GetDbType(TypeEx type)
	{
		type = GetValueType(type);

		switch (type.TypeCode)
		{
			case TypeCode.Int16:
				return DbType.Int16;
			case TypeCode.Int32:
				return DbType.Int32;
			case TypeCode.Int64:
				return DbType.Int64;
			case TypeCode.Single:
				return DbType.Single;
			case TypeCode.Double:
				return DbType.Double;
			case TypeCode.Decimal:
				return DbType.Decimal;
			case TypeCode.DateTime:
				return DbType.DateTime;
			case TypeCode.String:
				return DbType.String;
			case TypeCode.Boolean:
				return DbType.Boolean;
			case TypeCode.Char:
				return DbType.String;
			case TypeCode.Empty:
			case TypeCode.DBNull:
				return DbType.Object;
		}

		if (type == typeof (byte[]))
			return DbType.Binary;

		throw new ArgumentOutOfRangeException();
	}
}