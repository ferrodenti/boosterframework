﻿using System;
using Booster.Orm.Kitchen;
using Booster.Reflection;

namespace Booster.Orm.MySql;

public class MySqlConnectionSettings : BaseConnectionSettings
{
	public override TypeEx OrmType => typeof (MySqlOrm);

	public string Server
	{
		get => Read<string>("Server");
		set => Write("Server", value);
	}
	public string Protcol
	{
		get => Read("Protcol", "TCP");
		set => Write("Protcol", value);
	}
	public string Endpoint
	{
		get => Read<string>("Endpoint");
		set => Write("Endpoint", value);
	}
	public int Port
	{
		get => Read("Port", DefaultPort);
		set => Write("Port", value);
	}
	public string ServerType
	{
		get => Read("ServerType", "DEDICATED");
		set => Write("ServerType", value);
	}
	public string ServiceName
	{
		get => Read<string>("ServiceName");
		set => Write("ServiceName", value);
	}
	public string Schema
	{
		get => Read<string>("Schema") ?? Database ?? UserId;
		set => Write("Schema", value);
	}
		
	public string DateFormat
	{
		get => Read<string>("DateFormat");
		set => Write("DateFormat", value);
	}
		
	public string Database
	{
		get => Read<string>("Database");
		set => Write("Database", value);
	}

	#region low level settings

	public string DataSource
	{
		get => Read<string>("Data Source");
		set => Write("Data Source", value);
	}
	public string UserId
	{
		get => Read<string>("User ID");
		set => Write("User ID", value);
	}
	public string Password
	{
		get => Read<string>("Password");
		set => Write("Password", value);
	}
	public string IntegratedSecurity
	{
		get => Read<string>("Integrated Security");
		set => Write("Integrated Security", value);
	}

	public int MinPoolSize
	{
		get => Read<int>("Min Pool Size");
		set => Write("Min Pool Size", value);
	}
	public int MaxPoolSize
	{
		get => Read<int>("Max Pool Size");
		set => Write("Max Pool Size", value);
	}
	public int ConnectionLifetime
	{
		get => Read<int>("Connection Lifetime");
		set => Write("Connection Lifetime", value);
	}
	public int ConnectionTimeout
	{
		get => Read<int>("Connection Timeout");
		set => Write("Connection Timeout", value);
	}
		
	// public override int CommandTimeout
	// {
	// 	get => Read<int>("Command Timeout");
	// 	set => Write("Command Timeout", value);
	// }

	public int IncrPoolSize
	{
		get => Read<int>("Incr Pool Size");
		set => Write("Incr Pool Size", value);
	}
	public int DecrPoolSize
	{
		get => Read<int>("Decr Pool Size");
		set => Write("Decr Pool Size", value);
	}
	public bool Pooling
	{
		get => Read<bool>("Pooling");
		set => Write("Pooling", value);
	}
	public bool LoadBalancing
	{
		get => Read<bool>("Load Balancing");
		set => Write("Load Balancing", value);
	}
	public string DbaPrivilege
	{
		get => Read<string>("DBA Privilege");
		set => Write("DBA Privilege", value);
	}
	#endregion

	public MySqlConnectionSettings(string connectionString) : base(connectionString)
	{
		ProtectDataReaders = DataReadersProtection.None;
	}

	protected override int DefaultPort => 1521;

	protected override bool IstHi(string key, string value, ref Action post)
	{
		switch (key)
		{
			case "endpoint":
				post = () =>
				{
					if (TryParseEndPoint(Endpoint, out var server, out var port))
					{
						Server = server;
						Port = port;
						UpdateDataSource();
					}
				};
				return true;
			case "server":
			case "port":
				post = () =>
				{
					Endpoint = $"{Server}:{Port}";
					UpdateDataSource();
				};
				return true;
			case "protcol":
			case "servertype":
			case "servicename":
				post = UpdateDataSource;
				return true;

			case "commandtimeout":
			case "dateformat":
			case "schema":
			case "database":
				return true;
			case "datasource":
				post = () => {};
				return false;
		}
		return base.IstHi(key, value, ref post);
	}

	void UpdateDataSource()
	{
		DataSource = $@"(DESCRIPTION=
	(ADDRESS=
		(PROTOCOL={Protcol})
		(HOST={Server})
		(PORT={Port})
	)
	(CONNECT_DATA=
		(SERVER={ServerType})
		(SERVICE_NAME={ServiceName})
	)
)";
	}

}