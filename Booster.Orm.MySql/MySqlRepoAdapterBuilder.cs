﻿using System;
using Booster.CodeGen;
using Booster.Log;
using Booster.Orm.Interfaces;
using Booster.Orm.Kitchen;
using Booster.Reflection;

namespace Booster.Orm.MySql;

public class MySqlRepoAdapterBuilder : BasicRepoAdapterBuilder
{
	readonly MySqlSqlRepoQueries _repoQueries;

	public MySqlRepoAdapterBuilder(MySqlOrm orm, ITableMapping tableMapping, IDbTypeDescriptor typeDescriptor, IRepoQueries repoQueries, ILog log)
		: base(orm, tableMapping, typeDescriptor, repoQueries, log)
		=> _repoQueries = repoQueries as MySqlSqlRepoQueries;

	public override void EmitMembers(CsBuilder str)
	{
		str.UsingFor<MySqlTypeDescriptor>();
		base.EmitMembers(str);
	}

	protected override void EmitInsert(CsBuilder str, AsyncHelper async)
	{
		using (EmitMethod(str, async.MethodDefinition("void", "Insert", "object obj")))
		{
			str.AppendLine($"var entity = ({GetEntityFullClassName(str)})obj;");

			EmitEntityEvent(str, "OnTableChange");
			EmitEntityEvent(str, "OnSave");
			EmitEntityEvent(str, "OnInsert");

			var i = 0;
			foreach (var column in _repoQueries.InsertReturningOutMappings)
			{
				var type = column.ColumnNativeType;
				if (type == null)
					throw new Exception($"Unable to map a column of type {column.ColumnDataType} to any known type");

				str.Append($@"var outParam{i} = _orm.CreateParameter(""OUT{column.ColumnName}"", typeof({str.TypeRef(type)}));
outParam{i}.Direction = System.Data.ParameterDirection.Output;
outParam{i}.DbType = System.Data.DbType.{((MySqlTypeDescriptor)TypeDescriptor).GetDbType(type)};");
				if (type == typeof(string))
					str.AppendLine($"outParam{i}.Size = ushort.MaxValue;");

				i++;
			}

			str.Append(UsingConnection);
			str.AppendLine(async.BeginMethodCall("conn.ExecuteNoQuery", $@"@""{Escape(RepoQueries.Insert)}"", new DbParameter[]
	{{"));
			using (str.IncIdent(2))
			{
				EmitParamsArray(str, TableMapping.WriteColumns, RepoQueries.WriteParamPrefix);

				for (var j = 0; j < i; j++)
					str.AppendLine($@"	outParam{j},");
			}
			str.AppendLine($"	}}{async.EndMethodCall()};");

			i = 0;
			foreach (var column in _repoQueries.InsertReturningOutMappings)
			{
				var paramDataType = column.ColumnNativeType;
				if (paramDataType == null)
					throw new Exception($"Unable to map a column of type {column.ColumnDataType} to any known type");

				str.AppendLine(AssignField(str, column, paramDataType, $"({paramDataType})outParam{i}.Value", $"outParam{i}.Value != Convert.DBNull"));
				i++;
			}

			EmitEntityEvent(str, "OnInserted");
			EmitEntityEvent(str, "OnSaved");
			EmitEntityEvent(str, "OnTableChanged");
		}
	}

	protected override Method GetDataReaderMethod(IColumnMapping col)
		=> TypeDescriptor.GetDataReaderMethod(col.ColumnNativeType);

	protected override void EmitCreateParameter(CsBuilder builder, string preffix, IColumnMapping col, string mem, TypeEx type)
		=> builder.AppendLine($"_orm.CreateParameter(\"{preffix}{col.ColumnName}\", {mem}, typeof({builder.TypeRef(type)})),");
}