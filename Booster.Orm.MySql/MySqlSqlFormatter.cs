﻿using System;
using System.Collections.Generic;
using Booster.Localization.Pluralization;
using Booster.Orm.Interfaces;
using Booster.Orm.Kitchen;

namespace Booster.Orm.MySql;

public class MySqlSqlFormatter : BasicSqlFormatter
{
	const int _maxIdentLength = 128;

	protected new MySqlOrm Orm => (MySqlOrm) base.Orm;

	public MySqlSqlFormatter(IOrm orm) : base(orm)
	{
	}

	public override string ParamPrefix => ":";

	public override string CreateColumnName(string name)
	{
		var result = name;

		if (Orm.ConvertCamelCaseNames2Underscope && result.SplitNonEmpty("_").Length < 2)
			result = result.SeparateCamelCase(false, "_").ToUpper();
		else 
			result = result.ToUpper();

		if (result.Length <= _maxIdentLength)
			return result;

		var len = result.Length;
		var segments = result.SplitNonEmpty("_");

		while (len > _maxIdentLength)
		{
			var progress = false;

			for (var i = 0; i < segments.Length; ++i)
				if (segments[i].Length > 0)
				{
					segments[i] = segments[i].Substring(0, segments[i].Length - 1);
					progress = true;

					if (--len <= _maxIdentLength)
						break;
				}

			if (!progress)
				return result.SubstringSafe(0, 128);
		}

		return "_".JoinNonEmpty((IEnumerable<string>) segments);
	}

	public override string CreateTableName(string className)
	{
		if (Orm.ConvertModelNames2PluralTableNames)
			return EnPluralizer.PluralizeCamelCase(className, Orm.ConvertCamelCaseNames2Underscope ? "_" : "").ToUpper();
			
		if(Orm.ConvertCamelCaseNames2Underscope)
			return className.SeparateCamelCase(false, "_").ToUpper();
			
		return className.ToUpper();
	}

	public override string EscapeName(string name)
	{
		switch (name.ToLower())
		{
			case "access":
			case "add":
			case "alter":
			case "arraylen":
			case "audit":
			case "between":
			case "by":
			case "char":
			case "cluster":
			case "comment":
			case "compress":
			case "connect":
			case "current":
			case "date":
			case "decimal":
			case "delete":
			case "drop":
			case "exclusive":
			case "exists":
			case "identified":
			case "immediate":
			case "increment":
			case "index":
			case "initial":
			case "insert":
			case "integer":
			case "intersect":
			case "is":
			case "file":
			case "float":
			case "level":
			case "lock":
			case "long":
			case "maxextents":
			case "minus":
			case "mode":
			case "modify":
			case "noaudit":
			case "nocompress":
			case "notfound":
			case "nowait":
			case "number":
			case "of":
			case "offline":
			case "online":
			case "option":
			case "pctfree":
			case "prior":
			case "privileges":
			case "public":
			case "raw":
			case "rename":
			case "resource":
			case "revoke":
			case "row":
			case "rowid":
			case "rowlabel":
			case "rows":
			case "rownum":
			case "session":
			case "set":
			case "size":
			case "share":
			case "smallint":
			case "sqlbuf":
			case "start":
			case "successful":
			case "synonym":
			case "sysdate":
			case "trigger":
			case "update":
			case "uid":
			case "validate":
			case "values":
			case "varchar":
			case "varchar2":
			case "view":
			case "whenever":
				return $"\"{name}\"";
		}

		return base.EscapeName(name);
	}

	protected override string FormatBoolean(bool value)
		=> value ? "YES" : "NO";

	protected override string FormatDateTime(DateTime value)
		=> $"TO_TIMESTAMP('{value:yyyy-MM-dd HH:mm:ss.f}', 'YYYY-MM-DD HH24:MI:SS.FF')";
}