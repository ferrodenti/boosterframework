using System;
using System.Collections.Concurrent;
using Booster.Mvc.Tree.Interfaces;
using JetBrains.Annotations;

#if NETSTANDARD || NETCOREAPP
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
#else
using System.Web.Http;
using System.Web.Routing;
#endif

namespace Booster.Mvc.Tree
{
	[PublicAPI]
	public static class Tree
	{
		static readonly ConcurrentDictionary<string, ITreeController> _controllers = new(StringComparer.OrdinalIgnoreCase);
		static readonly ConcurrentDictionary<string, IBrowserController> _browserControllers = new(StringComparer.OrdinalIgnoreCase);

#if NET5_0_OR_GREATER || NETSTANDARD
		public static void MapBoosterTree(this IRouteBuilder routes)
		{
			routes.MapRoute(
				"Booster.Mvc.Tree.Api",
				"booster/tree/{action}",
				new {Controller = "TreeApi"});

			routes.MapRoute(
				"Booster.Mvc.Tree.FileBrowser.Api",
				"booster/filebrowser/{action}",
				new {Controller = "BrowserApi"});
		}

		public static void MapBoosterTree(this IEndpointRouteBuilder routes)
		{
			routes.MapControllerRoute(
				"Booster.Mvc.Tree.Api",
				"booster/tree/{action}",
				new {Controller = "TreeApi"});

			routes.MapControllerRoute(
				"Booster.Mvc.Tree.FileBrowser.Api",
				"booster/filebrowser/{action}",
				new {Controller = "BrowserApi"});
		}
#else
		public static void RegisterRoutes()
		{
			// ������-�� ��� ���������� �� ���������� �������� ������ ��������� ������. �����-�� ���� ��������
			var route = RouteTable.Routes.MapHttpRoute("Booster.Mvc.Tree.Api", "Booster/Tree/{action}");
			route.RouteHandler = new SessionRouteHandler();
			route.Defaults["Controller"] = "TreeApi";
			route = RouteTable.Routes.MapHttpRoute("Booster.Mvc.Tree.FileBrowser.Api", "Booster/FileBrowser/{action}");
			route.RouteHandler = new SessionRouteHandler();
			route.Defaults["Controller"] = "BrowserApi";
		}
#endif
		public static void RegisterController(ITreeController controller) 
			=> _controllers[controller.Name] = controller;

		public static void RegisterController(IBrowserController controller) 
			=> _browserControllers[controller.Name] = controller;

		public static ITreeController GetTreeController(string name) 
			=> _controllers.SafeGet(name);

		public static IBrowserController GetBrowserController(string name) 
			=> _browserControllers.SafeGet(name);
	}
}