using Newtonsoft.Json;

namespace Booster.Mvc.Tree.ViewModels;

public class BrowserResponse
{
	[JsonProperty("path", NullValueHandling= NullValueHandling.Ignore)]
	public string Path { get; set; }
	[JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
	public string Name { get; set; }
	[JsonProperty("icon", NullValueHandling = NullValueHandling.Ignore)]
	public string Icon { get; set; }


	[JsonProperty("folders", NullValueHandling = NullValueHandling.Ignore)]
	public BrowserResponse[] Folders { get; set; }

	[JsonProperty("files", NullValueHandling = NullValueHandling.Ignore)]
	public BrowserResponse[] Files { get; set; }
}