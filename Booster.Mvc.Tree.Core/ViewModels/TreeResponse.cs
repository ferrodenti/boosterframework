using System.Collections.Generic;
using Booster.Mvc.Editor.Interfaces;
using Booster.Mvc.Tree.Kitchen;
using Booster.Mvc.Tree.ViewModels;
using Newtonsoft.Json;

#nullable enable

namespace Booster.Mvc.Tree.Interfaces;

public class TreeResponse
{
	[JsonProperty("redirect", NullValueHandling = NullValueHandling.Ignore)] public string? Redirect;
	[JsonProperty("reload", NullValueHandling = NullValueHandling.Ignore)] public string? Reload;

	[JsonProperty("nodes", NullValueHandling = NullValueHandling.Ignore)] public TreeNode[]? Nodes;

	[JsonProperty("commands", NullValueHandling = NullValueHandling.Ignore)] public TreeNodeCommand[]? Commands;

	[JsonProperty("dirtyNodes", NullValueHandling = NullValueHandling.Ignore)] public string[]? DirtyTreeNodes;
	[JsonProperty("errorNodes", NullValueHandling = NullValueHandling.Ignore)] public string[]? ErrorTreeNodes;
	[JsonProperty("newNodes", NullValueHandling = NullValueHandling.Ignore)] public TreeNode[]? NewNodes;
	[JsonProperty("removedNodes", NullValueHandling = NullValueHandling.Ignore)] public string[]? RemovedNodes;
	[JsonProperty("changedNodes", NullValueHandling = NullValueHandling.Ignore)] public Dictionary<string, string>? ChangedNodes;

	[JsonProperty("schema", NullValueHandling = NullValueHandling.Ignore)] public IModelSchema? Schema;
	[JsonProperty("dirtyControls", NullValueHandling = NullValueHandling.Ignore)] public string[]? DirtyControls;
	[JsonProperty("validation", NullValueHandling = NullValueHandling.Ignore)] public Dictionary<string, string>? ValidationResponse;
	[JsonProperty("script", NullValueHandling = NullValueHandling.Ignore)] public string? Script;


	string? _alert;

	[JsonIgnore]
	public string? Alert 
	{
		get => _alert;
		set
		{
			if (value != _alert)
			{
				_alert = value;
				Script = $"alert('{_alert?.Replace("'", "\'")}');";
			}
		}
	}
}