using System.Collections.Generic;
using Booster.Mvc.ModelBinders;
using JetBrains.Annotations;

#if NETSTANDARD || NETCOREAPP
using Microsoft.AspNetCore.Mvc;
#else
using System.Web.Http.ModelBinding;
#endif

#nullable enable

namespace Booster.Mvc.Tree.ViewModels;

[ModelBinder(typeof(GetPostJsonBinder)), PublicAPI]
public class TreeRequest
{
	public string? Controller;
	public string? Id;
	public string? Context;
	public string? Command;
	public Dictionary<string, string>? Values;
}
