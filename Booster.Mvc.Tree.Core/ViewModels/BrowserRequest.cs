using Booster.Mvc.ModelBinders;
using JetBrains.Annotations;

#if NETSTANDARD || NETCOREAPP
using Microsoft.AspNetCore.Mvc;
#else
using System.Web.Http.ModelBinding;
#endif

#nullable enable

namespace Booster.Mvc.Tree.ViewModels
{
	[ModelBinder(typeof(GetPostJsonBinder)), PublicAPI]
	public class BrowserRequest
	{
		public string? Controller;
		public string? Context;
		public string? Path;
		public string? CurrentPath;
		public string? BaseDir;

		public bool GetFolders;
		public bool GetFiles;
	}
}