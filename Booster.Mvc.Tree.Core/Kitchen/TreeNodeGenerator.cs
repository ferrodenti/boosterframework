using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Booster.Mvc.Tree.ViewModels;

namespace Booster.Mvc.Tree.Kitchen;

public class TreeNodeGenerator
{
	readonly string _treeNodeType;
	readonly Delegate _idGetter;
	readonly Delegate _nameGetter;
	readonly Delegate _commandsGetter;

	public TreeNodeGenerator(string treeNodeType, Delegate idGetter, Delegate nameGetter, Delegate commandsGetter)
	{
		_idGetter = idGetter;
		_nameGetter = nameGetter;
		_treeNodeType = treeNodeType;
		_commandsGetter = commandsGetter;
	}

	public string GetId(object obj)
		=> (string)_idGetter.DynamicInvoke(obj);

	public string GetName(object obj)
		=> HttpUtility.HtmlEncode((string) _nameGetter.DynamicInvoke(obj));

	public IEnumerable<TreeNodeCommand> GetCommands(object obj)
	{
		if (_commandsGetter != null)
			return (IEnumerable<TreeNodeCommand>) _commandsGetter.DynamicInvoke(obj);

		return Enumerable.Empty<TreeNodeCommand>();
	}

	public TreeNode CreateTreeNode(object obj)
		=> new()
		{
			Id = GetId(obj),
			Text = GetName(obj),
			Type = _treeNodeType
		};
}