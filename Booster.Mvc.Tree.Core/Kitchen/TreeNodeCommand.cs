using System;
using System.Threading.Tasks;
using Booster.Mvc.Editor.Interfaces;
using Newtonsoft.Json;

#nullable enable

namespace Booster.Mvc.Tree.Kitchen;

[JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
public class TreeNodeCommand
{
	[JsonIgnore] public string? Name;

	[JsonProperty("pos", NullValueHandling = NullValueHandling.Ignore)]
	public string Pos => Position.ToString().ToLowerFirstLetter(false)!;

	[JsonIgnore] public TreeNodeCommandPos Position;
	[JsonProperty("command", NullValueHandling= NullValueHandling.Ignore)] public string? Command;
	[JsonProperty("caption", NullValueHandling= NullValueHandling.Ignore)] public string? Caption;
	[JsonProperty("script", NullValueHandling= NullValueHandling.Ignore)] public string? Script;
	[JsonProperty("class", NullValueHandling= NullValueHandling.Ignore)] public string? Class;
	[JsonProperty("icon", NullValueHandling= NullValueHandling.Ignore)] public string? Icon;
	[JsonProperty("href", NullValueHandling= NullValueHandling.Ignore)] public string? Href;
	[JsonProperty("customHtml", NullValueHandling= NullValueHandling.Ignore)] public string? CustomHtml;
	[JsonProperty("group", DefaultValueHandling = DefaultValueHandling.Ignore)] public bool Group;

	[JsonIgnore]
	public Func<IEditProxy, object>? Action;
	[JsonIgnore]
	public Func<IEditProxy, Task<object>>? ActionAsync;

	public TreeNodeCommand()
	{
	}

	public TreeNodeCommand(
		string? name, 
		string? caption,
		string? @class,
		string? icon, 
		TreeNodeCommandPos pos, 
		Func<IEditProxy, object>? action = null, 
		string? script = null)
	{
		Class = @class;
		Caption = caption;
		Name = name;
		Action = action;
		Script = script;
		Position = pos;
		Icon = icon;

		if (action != null)
			Command = name;
	}

	public async Task<object?> InvokeAction(IEditProxy editProxy)
	{
		if (Action != null)
			return Action(editProxy);

		if (ActionAsync != null)
			return await ActionAsync(editProxy).NoCtx();

		return null;
	}
}