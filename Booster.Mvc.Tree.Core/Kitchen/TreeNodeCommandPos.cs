namespace Booster.Mvc.Tree.Kitchen;

public enum TreeNodeCommandPos
{
	TopLeft,
	TopRight,
	Top,
	Bottom,
	BottomLeft,
	BottomRight
}