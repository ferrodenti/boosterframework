using System;
using System.Collections.Generic;
using Booster.Mvc.Helpers;
using Booster.Mvc.Tree.ViewModels;
using JetBrains.Annotations;
#if NETSTANDARD || NETCOREAPP
using System.Web;
using Microsoft.AspNetCore.Mvc;
#else
using System.Web.Http;
#endif

namespace Booster.Mvc.Tree.Kitchen
{
	[PublicAPI]
	public class BrowserApiController : ApiController
	{
		[HttpGet, HttpPost]
		public object Get(BrowserRequest request)
		{
			var controller =  Tree.GetBrowserController(request.Controller);
			if (controller == null)
				return HttpStatusHelper.ToApiResult(HttpStatusHelper.NotFound($"A controller {request.Controller} was not found"), this);
			
			try
			{
				if (request.GetFolders)
					return controller.GetFolders(request);
				
				return request.GetFiles 
					? controller.GetFiles(request) 
					: new List<TreeNode>();
			}
			catch (Exception ex)
			{
				return new List<TreeNode> {new(ex)};
			}
		}
	}
}