using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Booster.Log;
using Booster.Mvc.Editor;
using Booster.Mvc.Editor.Interfaces;
using Booster.Mvc.Helpers;
using Booster.Mvc.Tree.Interfaces;
using Booster.Mvc.Tree.ViewModels;
using Booster.Obsolete;
using Booster.Reflection;
using JetBrains.Annotations;

namespace Booster.Mvc.Tree.Kitchen;

[PublicAPI]
public abstract class BaseTreeController : ITreeController
{
	public string Name { get; }

	public ILog Log { get; set; }
	protected readonly ConcurrentDictionary<TypeEx, TreeNodeGenerator> Generators = new();
	protected readonly ConcurrentDictionary<TypeEx, IEditorSchema> EditorSchemas = new();

	[PublicAPI]protected void Register<T>(string treeNodeType, Func<T, string> idGetter, Func<T, string> nameGetter, Func<T, IEnumerable<TreeNodeCommand>> commandsGetter) 
		=> Generators[typeof (T)] = new TreeNodeGenerator(treeNodeType, idGetter, nameGetter, commandsGetter);

	protected virtual TreeNodeGenerator GetTreeNodeGenerator(TypeEx type)
	{
		if (Generators.TryGetValue(type, out var gen))
			return gen;

		foreach (var pair in Generators)
			if (pair.Key.IsAssignableFrom(type))
			{
				Generators[type] = pair.Value;
				return pair.Value;
			}

		Generators[type] = null;
		return null;
		//throw new HttpError(404, "There is no TreeNodeGenerator registered for type {0}", type.Name);
	}

	protected BaseTreeController(string name) 
		=> Name = name;

	protected abstract IEditProxyRepository Proxies { get; }

	public abstract Task<IEditProxy> GetProxy(TreeRequest request);
	public abstract Task<TreeNode[]> GetNodes(TreeRequest request);

	public virtual IEnumerable<TreeNodeCommand> GetCommonCommands()
	{
		yield break;
	}

	public virtual async Task<object> Get(TreeRequest request)
	{
		var resp = CreateResponse();

		try
		{
			resp.Nodes = await GetNodes(request).NoCtx();
		}
		catch (Exception ex)
		{
			resp.Nodes = new[] {new TreeNode(ex)};
		}

		return resp;
	}

	public virtual async Task<object> GetEditor(TreeRequest request)
	{
		var proxy = GetProxy(request);
		if (proxy == null)
			return HttpStatusHelper.NotFound("Node not found");

		TreeResponse resp;

		if (proxy is IEntityEditProxy entityEdit)
		{
			resp = CreateResponse();
			resp.ValidationResponse = Validate(entityEdit)?.Errors;
			resp.Schema = await GetModelSchema(entityEdit).NoCtx();
			resp.DirtyControls = await GetDirtyControls(entityEdit).NoCtx();
			resp.Commands = GetCommands(proxy).ToArray();
			return resp;
		}
		resp = CreateResponse();
		resp.Schema = new ModelSchema();
		resp.Commands = GetCommands(proxy).ToArray();
		return resp;
	}

	protected virtual ValidationResponse Validate(IEntityEditProxy proxy)
	{
		ValidationResponse validation = null;
#pragma warning disable 618
		if (proxy.Target is IValidatable validatable)
#pragma warning restore 618
		{
			validation = new ValidationResponse();
			validatable.Validate(proxy, validation);
			Proxies.SetError(proxy, !validation.IsOk);
		}

		return validation;
	}



	public virtual async Task<object> Update(TreeRequest request)
	{
		if (GetProxy(request) is not IEntityEditProxy entityEdit)
			return HttpStatusHelper.Forbidden("Object does not support update method");

		IModelSchema schema = null;

		var changedProxies = new Dictionary<string, string>();

		if (request.Values != null)
		{
			var oldName = GetNodeName(entityEdit);

			schema = await GetModelSchema(entityEdit).NoCtx();

			void Handler(object s, EventArgs<string> e)
			{
				if (s is not IEditProxy proxy) return;

				if (proxy != entityEdit)
				{
					var gen = GetTreeNodeGenerator(proxy.GetType());
					if (gen != null) changedProxies[gen.GetId(proxy)] = gen.GetName(proxy);
				}
			}

			try
			{
				Proxies.ProxyChanged += Handler;

				foreach (var pair in request.Values)
				{
					var col = schema.GetColumn(pair.Key);
					var val = pair.Value;

					var format = (string) col.GetSetting(entityEdit.Target, "format");

					if (string.IsNullOrEmpty(val) && col.ReplaceEmptyWithDefault)
					{
						var def = col.GetSetting(entityEdit.Target, "defaultValue");

						if (def is string str && col.DataAccessor.ValueType != typeof(string))
							def = StringConverter.Default.ToObject(str, col.DataAccessor.ValueType, format);

						col.DataAccessor.SetValue(entityEdit, def);
						continue;
					}

					col.DataAccessor.SetValue(entityEdit, StringConverter.Default.ToObject(val, col.DataAccessor.ValueType, format));
				}
			}
			finally
			{
				Proxies.ProxyChanged -= Handler;
			}

			var newName = GetNodeName(entityEdit);

			if (oldName != newName)
				changedProxies[request.Id] = newName;

			Proxies.UpdateDirty(entityEdit);

			schema.PickChanges();


			schema.Html = null;
			schema.Columns = null;

			if( schema.ControlSettings != null && schema.ControlSettings.TryGetValue("values", out var values) )
				foreach (var pair in values.Where(pair => Equals(request.Values.SafeGet(pair.Key), pair.Value)).ToArray())
					values.Remove(pair.Key);
		}

		var resp = CreateResponse();
		resp.ValidationResponse = Validate(entityEdit)?.Errors;
		resp.ChangedNodes = changedProxies.Count > 0 ? changedProxies : null;
		resp.DirtyControls = await GetDirtyControls(entityEdit).NoCtx();
		resp.Schema = schema;
		resp.Commands = GetCommands(entityEdit).ToArray();

		return resp;
	}

	public virtual async Task<object> Command(TreeRequest request)
	{
		var proxy = await GetProxy(request).NoCtx();

		if (proxy == null)
			return HttpStatusHelper.NotFound("Node not found");

		var cmd = GetCommands(proxy).FirstOrDefault(c => c.Name == request.Command);

		if (cmd?.Action == null)
			return HttpStatusHelper.NotFound($"Command {request.Command} not found");
#if !DEBUG
			try
#endif
		{
			return await cmd.InvokeAction(proxy).NoCtx();
		}
#if !DEBUG			
			catch(Exception ex)
			{
				Log.Error(ex, $"{Name}.{request.Command}: {ex.Message}");
			    return new TreeResponse { Alert = ex.Message };
			}
#endif
	}

	protected virtual Task<IModelSchema> GetModelSchema(IEntityEditProxy proxy)
		=> Task.FromResult(
			EditorSchemas.GetOrAdd(proxy.Target.GetType(),
					t => new UserChangesEditorSchmeaReader(proxy).GetSchema(t, true))
				.ToModelSchema(proxy));

	protected virtual Task<string[]> GetDirtyControls(IEntityEditProxy proxy)
		=> Task.FromResult(proxy.GetDirtyFields().ToArray());

	protected virtual string GetNodeName(object obj) 
		=> GetTreeNodeGenerator(obj.GetType()).With(g => g.GetName(obj));

	protected virtual string GetNodeId(object obj) 
		=> GetTreeNodeGenerator(obj.GetType()).With(g => g.GetId(obj));

	protected virtual TreeNode CreateTreeNode(object obj) 
		=> GetTreeNodeGenerator(obj.GetType()).CreateTreeNode(obj);

	protected IEnumerable<TreeNodeCommand> GetCommands(object obj) 
		=> GetCommonCommands().Concat(GetTreeNodeGenerator(obj.GetType()).GetCommands(obj));

	protected virtual TreeResponse AddNode<T>(ICollectionEditProxy collectionEdit, T schema, Func<IEntityEditProxy<T>, TreeNode> nodeGetter)
	{
		collectionEdit.Add(schema);

		var changes = (IEntityEditProxy<T>) Proxies.Get(collectionEdit, schema);
		var node = nodeGetter(changes);

		node.ParentId = GetNodeId(collectionEdit) ?? collectionEdit.Parent.With(GetNodeId);

		Proxies.UpdateDirty(changes, true);

		var resp = CreateResponse();
		resp.Redirect = node.Id;
		resp.NewNodes = new[] {node};
		return resp;
	}

	protected virtual object RemoveNode<T>(ICollectionEditProxy<T> collectionEdit, T obj)
	{
		if (!collectionEdit.Remove(obj))
			return HttpStatusHelper.Gone("An entity has been already removed");

		var proxy = Proxies.Get(collectionEdit, obj);
		Proxies.UpdateDirty(collectionEdit);

		proxy.OnRemoved();
			
		var resp = CreateResponse();
		resp.RemovedNodes = new[] { GetNodeId(proxy) };

		return resp;
	}

	protected virtual TreeResponse CreateResponse()
		=> new()
		{
			DirtyTreeNodes = Proxies.GetDirtyIds(GetNodeId),
			ErrorTreeNodes = Proxies.GetErrorsIds(GetNodeId),
		};
}