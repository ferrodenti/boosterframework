using System;
using Booster.Mvc.Helpers;
using Booster.Mvc.Tree.Interfaces;
using Booster.Mvc.Tree.ViewModels;
using JetBrains.Annotations;
#if NETSTANDARD || NETCOREAPP
using System.Web;
using Microsoft.AspNetCore.Mvc;
#else
using System.Web.Http;
#endif

namespace Booster.Mvc.Tree.Kitchen
{
	[PublicAPI]
	public class TreeApiController : ApiController
	{
		[HttpGet, HttpPost]
		public object GetNodes(TreeRequest request)
			=> Exe(request, c => c.Get(request));

		[HttpGet, HttpPost]
		public object GetEdit(TreeRequest request)
			=> Exe(request, c => c.GetEditor(request));

		[HttpGet, HttpPost]
		public object Command(TreeRequest request)
			=> Exe(request, c => c.Command(request));

		[HttpGet, HttpPost]
		public object Update(TreeRequest request)
			=> Exe(request, c => c.Update(request));

		object Exe(TreeRequest request, Func<ITreeController, object> proc)
		{
#if !NETSTANDARD && !NETCOREAPP3 && !NET5_0_OR_GREATER
			MvcHelper.OnNewRequest();
#endif
			var controller = Tree.GetTreeController(request.Controller);
			if (controller == null)
				return HttpStatusHelper.NotFound($"A controller {request.Controller} was not found");

			return HttpStatusHelper.ToApiResult(proc(controller), this);
		}
	}
}