using System.Threading.Tasks;
using Booster.Mvc.Tree.ViewModels;

namespace Booster.Mvc.Tree.Interfaces;

public interface ITreeController
{
	string Name { get; }

	Task<object> Get(TreeRequest request);
	Task<object> GetEditor(TreeRequest request);
	Task<object> Update(TreeRequest request);
	Task<object> Command(TreeRequest request);
}