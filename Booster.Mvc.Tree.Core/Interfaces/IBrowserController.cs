using System.Collections.Generic;
using System.Threading.Tasks;
using Booster.Mvc.Tree.ViewModels;

namespace Booster.Mvc.Tree.Interfaces;

public interface IBrowserController
{
	string Name { get; }

	Task<List<TreeNode>> GetFolders(BrowserRequest request);
	Task<List<TreeNode>> GetFiles(BrowserRequest request);
}