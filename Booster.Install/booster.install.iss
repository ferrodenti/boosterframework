var
	SettingsPage: TWizardPage;
	settingsPageExist, restartRequired, restarted, cancelWithoutPrompt: boolean;
	currentTop: integer;
	prevStatus: string;
	Inputs: array of TNewEdit;

const
  ERROR_SUCCESS_REBOOT_REQUIRED = 3010;
  QuitMessageReboot = 'Один из установленных компонентов требует перезагрузки системы. Завершите все запущенные программы и сохраните несохраненные данные.'#13#13'Установка продолжится как только администратор войдет в систему в следующий раз после перезагрузки компьютера.';

//*************************************
//*         AUX Functions             *
//************************************* 

function BoolToStr(value : Boolean) : String; 
begin
  if value then
    result := 'true'
  else
    result := 'false';
end;

function Escape(str: string) : string;
begin
	StringChangeEx(str, '|', '||', true);
	result := str;
end;

//*************************************
//*      Status functions             *
//*************************************

function SetStatusText(const text: string): string;
begin
	Result := WizardForm.StatusLabel.Caption;
	WizardForm.StatusLabel.Caption := text;
end;

procedure SetStatusTextMarquee(const text: string);
begin
	prevStatus := SetStatusText(text);
	WizardForm.ProgressGauge.Style := npbstMarquee;
end;

procedure RestoreStatusText;
begin
	WizardForm.StatusLabel.Caption := prevStatus;
	WizardForm.ProgressGauge.Style := npbstNormal;
end;

//*************************************
//*         ExecXXX                   *
//************************************* 

procedure ExecExcept(const filename, params, errorDescription: String);
var
	ResultCode: Integer;
begin	
	if not Exec(filename, params, '', SW_HIDE, ewWaitUntilTerminated, ResultCode) then
	begin
		RestoreStatusText;
		RaiseException(Format('%s: %s (%d).', [errorDescription, SysErrorMessage(ResultCode), ResultCode]));
	end;
	
	if ResultCode = ERROR_SUCCESS_REBOOT_REQUIRED then
		restartRequired := true;
end;

// Exec with output stored in result. ResultString will only be altered if True is returned.
function ExecWithResult(const Filename, Params, what : String; var ResultCode: Integer; var ResultString: String): Boolean;
var
	TempFilename: String;
	TempAnsiStr: AnsiString;
	Command: String;
begin
	TempFilename := ExpandConstant('{tmp}\~execwithresult.txt');
	// Exec via cmd and redirect output to file. Must use special string-behavior to work.
	Command := Format('"%s" /S /C ""%s" %s > "%s""', [ExpandConstant('{cmd}'), Filename, Params, TempFilename]);
	result := Exec(ExpandConstant('{cmd}'), Command, '', SW_HIDE, ewWaitUntilTerminated, ResultCode);
		
	LoadStringFromFile(TempFilename, TempAnsiStr);  // Cannot fail
	ResultString := String(TempAnsiStr);
	DeleteFile(TempFilename);
	
	// Remove new-line at the end
	if (Length(ResultString) >= 2) and (ResultString[Length(ResultString) - 1] = #13) and (ResultString[Length(ResultString)] = #10) then
		Delete(ResultString, Length(ResultString) - 1, 2);
		
	if result and (ResultCode = 0) then
	begin
		log(Format('%s success: %s', [what, ResultString]));
	end
	else
	begin
		log(Format('%s fail (%d): %s', [what, ResultCode, ResultString]));
		RestoreStatusText;
		RaiseException(resultString);
	end;
end;

function ExecController(params : string) : string;
var
	resultString: string;
	resultCode: integer;
begin
	if ExecWithResult(ExpandConstant('{tmp}\install\install.controller.exe'), params, 'controller ' + params, resultCode, resultString) then
		result := resultString;
end;

//*************************************
//*      UI functions                 *
//*************************************

procedure Focus(edit: TNewEdit);
begin
	edit.SelectAll;
	WizardForm.ActiveControl := edit;
end;

function ReadPort(edit: TNewEdit; var res: integer): boolean;
begin
	res := StrToIntDef(edit.Text, -1);
	if (res < 1) or (res > 65535) then
	begin
		Focus(edit);
		result := true;
		exit;
	end;
	result := false;
end;

function ReadString(edit: TNewEdit; var res: string): boolean;
begin
	if edit.Text = '' then
	begin
		Focus(edit);
		result := true;
		exit;
	end;
	res := edit.Text;
	result := false;
end;


procedure NumEditKeyPress(Sender: TObject; var Key: Char);
var
  KeyCode: Integer;
begin
  // allow only numbers
  KeyCode := Ord(Key);
  if not ((KeyCode = 8) or ((KeyCode >= 48) and (KeyCode <= 57))) then
    Key := #0;
end;

function CreateInput(name, caption: string; value: string; isNum: boolean): TNewEdit;
var 
	lbl: TNewStaticText;
	cur: integer;
begin
	lbl := TNewStaticText.Create(SettingsPage);
	lbl.Top := currentTop;
	lbl.AutoSize := true;
	lbl.Caption := caption;
	lbl.Parent := SettingsPage.Surface;
	
	result := TNewEdit.Create(SettingsPage);
	result.Top := currentTop;
	result.Left := SettingsPage.SurfaceWidth div 2;
	result.Width := SettingsPage.SurfaceWidth div 2 - ScaleX(8);
	result.Parent := SettingsPage.Surface;
	result.Text := value;
	result.Name := name;
	if isNum then
		result.OnKeyPress := @NumEditKeyPress;
		
	currentTop := currentTop + result.Height + ScaleY(0);
	
	cur := getArrayLength(inputs);
	SetArrayLength(inputs, cur + 1);
	inputs[cur] := result;	
end;

function ReadAndSaveUserSettings : boolean;
var
	i, last: integer;
	edit: TNewEdit;
	buff: string;
begin
	result := false;
	buff := 'saveUserSettigs ' + 
		'target=' + Escape(ExpandConstant('{srcexe}')) + 
		'|lang=' + Escape(ExpandConstant('{language}')) + 
		'|dir=' + Escape(WizardDirValue) + 
		'|group=' + Escape(WizardGroupValue) + 
		'|noicons=' +  BoolToStr(WizardNoIcons) + 
		'|type=' + Escape(WizardSetupType(False)) + 
		'|components=' + Escape(WizardSelectedComponents(False)) + 
		'|tasks=' + Escape(WizardSelectedTasks(False))

	last := GetArrayLength(inputs) - 1;
	for i := 0 to last do
	begin
		edit := inputs[i];
		if edit.Text = '' then
		begin
			Focus(edit);
			exit;
		end;
		buff := buff + '|' + edit.Name + '=' + Escape(edit.Text);
	end;
	
	buff := ExecController(buff);
	if buff <> 'ok'  then
	begin
		for i := 0 to last do
		begin
			edit := inputs[i];
			if edit.Name = buff then
				Focus(edit);
		end;
		exit;
	end;
	result := true;
end;


//*************************************
//*      Restart                      *
//*************************************
procedure Restart;
begin	
	ExecController('restart');

	MsgBox(QuitMessageReboot, mbError, mb_Ok);
	
	cancelWithoutPrompt := true;
	WizardForm.Close;
end;

procedure CancelButtonClick(CurPageID: Integer; var Cancel, Confirm: Boolean);
begin
  if CurPageID=wpInstalling then
    Confirm := not CancelWithoutPrompt;
end;
