﻿using System;

namespace Booster.Install;

public static class StringExpander
{
	public static bool EqualsIgnoreCase(this string a, string b)
		=> string.Equals(a, b, StringComparison.OrdinalIgnoreCase);

	public static bool IsSome(this string a)
		=> !string.IsNullOrWhiteSpace(a);
		
	public static bool IsEmpty(this string a)
		=> string.IsNullOrWhiteSpace(a);
}