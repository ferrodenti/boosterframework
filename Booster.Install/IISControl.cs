﻿using System;
using System.IO;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace Booster.Install;

// ReSharper disable once InconsistentNaming
[PublicAPI]
public class IISControl
{
	public static async Task<string> EnsureInstalled()
	{
		int result;

		static async Task<int> CurrentWindows()
			=> await Utils.Exe("Dism", "/Online /Enable-Feature /NoRestart" +
			                           // Enables everything a fresh install would also (implicitly) enable.
			                           // This is important in case IIS was already installed and some features manually disabled.
			                           //" /FeatureName:WAS-WindowsActivationService" +
			                           //" /FeatureName:WAS-ProcessModel" +
			                           //" /FeatureName:WAS-NetFxEnvironment" +
			                           //" /FeatureName:WAS-ConfigurationAPI" +
			                           " /FeatureName:IIS-ManagementConsole" +
			                           " /FeatureName:IIS-HttpErrors" +
			                           " /FeatureName:IIS-DefaultDocument" +
			                           " /FeatureName:IIS-StaticContent" +
			                           //" /FeatureName:IIS-DirectoryBrowsing" +
			                           " /FeatureName:IIS-ASPNET45" +
			                           " /FeatureName:IIS-HttpCompressionStatic" +
			                           " /FeatureName:IIS-RequestFiltering" +
			                           " /FeatureName:IIS-HttpLogging" +
			                           " /All");

		switch (Environment.OSVersion.Version.Major)
		{
			case <= 4:	//  (95, 98, me = 4)
			case 5:		// 2000, xp, 2003: Use "sysocmgr"
				var iniFile = Path.Combine(Utils.SelfLocation, "~iis56.ini");
				await File.WriteAllTextAsync(iniFile, @"[Components]
iis_common = ON
iis_www = ON
iis_inetmgr = ON");
				result = await Utils.Exe("sysocmgr", $"/i:\"{Path.Combine(Utils.WindowsFolder, "inf\\sysoc.inf")}\" /u:\"{iniFile}\"");
				Utils.SafeDelete(iniFile);
				break;
			case 6:		// Vista, 2008, 7, 2008 R2, 8, 8.1
				result = Environment.OSVersion.Version.Minor switch
				{
					0 => // Vista, 2008: Use "pkgmgr"
						await Utils.Exe("pkgmgr", "/iu:" +
						                          // Enables everything a fresh install would also (implicitly) enable.
						                          // This is important in case IIS was already installed and some features manually disabled.
						                          "WAS-WindowsActivationService" +
						                          ";WAS-ProcessModel" +
						                          ";WAS-NetFxEnvironment" +
						                          ";WAS-ConfigurationAPI" + 
						                          ";IIS-WebServerRole" + 
						                          ";IIS-WebServerManagementTools" +
						                          ";IIS-ManagementConsole" + 
						                          ";IIS-WebServer" + 
						                          ";IIS-ApplicationDevelopment" +
						                          ";IIS-NetFxExtensibility" +
						                          ";IIS-ASPNET" +
						                          ";IIS-ISAPIExtensions" +
						                          ";IIS-ISAPIFilter" + 
						                          ";IIS-CommonHttpFeatures" +
						                          ";IIS-HttpErrors" +
						                          ";IIS-DefaultDocument" +
						                          ";IIS-StaticContent" +
						                          ";IIS-DirectoryBrowsing" + 
						                          ";IIS-Performance" +
						                          ";IIS-HttpCompressionStatic" + 
						                          ";IIS-Security" +
						                          ";IIS-RequestFiltering" + 
						                          ";IIS-HealthAndDiagnostics" +
						                          ";IIS-RequestMonitor" +
						                          ";IIS-HttpLogging"),
					1 => // 7, 2008: Use "Dism"
						await Utils.Exe("Dism", "/Online /NoRestart /Enable-Feature" +
						                        // Enables everything a fresh install would also (implicitly) enable.
						                        // This is important in case IIS was already installed and some features manually disabled.
						                        // "Parent fetaures" are NOT automatically enabled by "Dism".
						                        //' /FeatureName:WAS-WindowsActivationService /FeatureName:WAS-ProcessModel /FeatureName:WAS-NetFxEnvironment /FeatureName:WAS-ConfigurationAPI' +
						                        " /FeatureName:IIS-WebServerRole" + 
						                        " /FeatureName:IIS-WebServerManagementTools" +
						                        " /FeatureName:IIS-ManagementConsole" + 
						                        " /FeatureName:IIS-WebServer" + 
						                        " /FeatureName:IIS-ApplicationDevelopment" +
						                        " /FeatureName:IIS-NetFxExtensibility" +
						                        " /FeatureName:IIS-ASPNET" +
						                        " /FeatureName:IIS-ISAPIExtensions" +
						                        " /FeatureName:IIS-ISAPIFilter" + 
						                        " /FeatureName:IIS-CommonHttpFeatures" +
						                        " /FeatureName:IIS-HttpErrors" +
						                        " /FeatureName:IIS-DefaultDocument" +
						                        " /FeatureName:IIS-StaticContent" +
						                        //" /FeatureName:IIS-DirectoryBrowsing" +
						                        " /FeatureName:IIS-Performance" +
						                        " /FeatureName:IIS-HttpCompressionStatic" + 
						                        " /FeatureName:IIS-Security" +
						                        " /FeatureName:IIS-RequestFiltering" + 
						                        " /FeatureName:IIS-HealthAndDiagnostics" +
						                        " /FeatureName:IIS-RequestMonitor" +
						                        " /FeatureName:IIS-HttpLogging"),
					> 1 => await CurrentWindows(),
					_ => throw new Exception($"Неподдерживаемая версия операционной системы: {Environment.OSVersion.Version}")
				};
				break;
			case >= 10:	// 10: Use "Dism"
				result = await CurrentWindows();
				break;
			default:
				throw new Exception($"Неподдерживаемая версия операционной системы: {Environment.OSVersion.Version}");
		}

		var code = (ReturnCode)result;

		return code switch
		{
			ReturnCode.ErrorSuccess => "ok",
			ReturnCode.ErrorSuccessRebootRequired => "reboot",
			_ => throw new Exception($"Ошибка установки IIS: {result}.  Добавьте роль IIS вручную и повторите установку.")
		};
	}

	public static async Task RegisterDotNet()
	{
		var result = await Utils.Exe(Path.Combine(Environment.GetEnvironmentVariable("dotnet40") ?? "", "aspnet_regiis.exe"), "-iru -enable");
		if (result != (int) ReturnCode.ErrorSuccess)
			throw new Exception($"Ошибка регистрации ASP.NET: {result}");
	}

	public static async Task<string> IIS7ExeCommand(string parameters)
	{
		try
		{
			var win = Environment.GetEnvironmentVariable("WinDir");
			return await Utils.ExeResult($"{win}\\inetsrv\\appcmd.exe", parameters);

		}
		catch (Exception e)
		{
			throw new Exception($"IIS7ExeCommand: appcmd {parameters}: {e.Message}", e);
		}
	}

	public static async Task RegisterIISApp(string appName, int serviceNumber, string appPoolName, string path, int port = 80, string appPoolOptions = "/managedRuntimeVersion:v4.0")
	{
		if (Environment.OSVersion.Version.Major < 6)
			throw new Exception($"Неподдерживаемая версия операционной системы: {Environment.OSVersion.Version}");

		// Vista / 2008 or later : IIS 7 or later

		var webSiteName = await IIS7ExeCommand($"list site /id:{serviceNumber} /text:name").NoCtx(); // Get name of web-site
		if (string.IsNullOrEmpty(webSiteName))
			throw new Exception($"Сайт #{serviceNumber} не найден");

		await IIS7ExeCommand($"set site \"{webSiteName}\" /bindings:\"http://:{port}\"").NoCtx();
		await IIS7ExeCommand($"delete app \"{webSiteName}/{appName}\"").NoCtx(); // Delete the application if it already exists

		// Check if the application pool already exists (we don't delete it, since another instance might be using it too)
		var name = await IIS7ExeCommand($"list apppool \"{appPoolName}\" /text:name").NoCtx();
		if (!name.EqualsIgnoreCase(appPoolName))
			await IIS7ExeCommand($"add apppool /name:\"{appPoolName}\" {appPoolOptions}").NoCtx();

		// Create the application
		await IIS7ExeCommand($"add app /site.name:\"{webSiteName}\" /path:\"{appName}\" /physicalPath:\"{path}\" /applicationPool:\"{appPoolName}\"").NoCtx();
	}
}