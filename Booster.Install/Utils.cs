﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Booster.Install;

public class Utils
{
	public static async Task GrantPermissions(string path, IEnumerable<string> users)
	{
		foreach (var user in users)
		{
			var result = await Exe("icacls", $"\"{path}\" /grant \"{user}\":(OI)(CI)F /T\"").NoCtx();
			if (result != (int) ReturnCode.ErrorSuccess)
				throw new Exception($"Ошибка предоставления полномочий {user}->{path}:");
		}
	}

	public static async Task<int> Exe(string filename, string arguments)
	{
		using var process = new Process
		{
			StartInfo = new ProcessStartInfo
			{
				WindowStyle = ProcessWindowStyle.Hidden, 
				FileName = filename, 
				Arguments = arguments
			}
		};
		process.Start();

		await process.WaitForExitAsync().NoCtx();

		return process.ExitCode;
	}

	public static async Task<string> ExeResult(string filename, string arguments)
	{
		var bld = new StringBuilder();
		using var process = new Process
		{
			StartInfo = new ProcessStartInfo
			{
				WindowStyle = ProcessWindowStyle.Hidden, 
				FileName = filename, 
				Arguments = arguments,
				RedirectStandardError = true,
				RedirectStandardOutput = true
			}
		};

		void OutputDataReceived(object _, DataReceivedEventArgs e)
			=> bld.Append(e.Data);

		process.ErrorDataReceived += OutputDataReceived;
		process.OutputDataReceived += OutputDataReceived;
		process.Start();

		await process.WaitForExitAsync().NoCtx();

		if (process.ExitCode == 0)
			return bld.ToString().Trim();

		throw new Exception(bld.ToString().Trim());
	}

	public static void IgnoreErrors(Action action)
	{
		try
		{
			action();
		}
		catch 
		{
			//
		}
	}

	public static bool SafeDelete(string filename)
	{
		var result = false;
		IgnoreErrors(() =>
		{
			if (File.Exists(filename))
			{
				File.Delete(filename);
				result = true;
			}
		});
		return result;
	}

	public static string WindowsFolder => Environment.GetFolderPath( Environment.SpecialFolder.Windows );

	static readonly Lazy<string> _selfLocation = new(() => Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) ?? "");

	public static string SelfLocation => _selfLocation.Value;

	static readonly Lazy<string> _logFilename = new(() => Path.Combine(SelfLocation, $"{DateTime.Today:ddMMyyyy}.log"));

	public static string LogFilename => _logFilename.Value;

	public static Task Log(string message)
	{
		Trace.WriteLine(message);
		return File.AppendAllTextAsync(LogFilename, $"{DateTime.Now:hh:mm:ss}: {message}\n");
	}
}