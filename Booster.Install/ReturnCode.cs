﻿namespace Booster.Install;

public enum ReturnCode
{
	// ReSharper disable UnusedMember.Global
	ErrorSuccess = 0,
	ErrorInvalidFunction = 1,
	ErrorNotSupported = 50,
	ErrorNotFound = 1168,
	ErrorSuccessRebootRequired = 3010
	// ReSharper restore UnusedMember.Global
}