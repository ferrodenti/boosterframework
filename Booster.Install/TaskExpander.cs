﻿using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace Booster.Install;

public static class TaskExpander
{
	// [Obsolete("Требуется разобраться: task.IsCompleted=true, если задача ждет")]
	// public static bool IsStopped(this Task task) 
	// 	=> task == null || task.IsCanceled || task.IsCompleted || task.IsFaulted;

	public static ConfiguredTaskAwaitable NoCtx(this Task task) 
		=> task.ConfigureAwait(false);

	public static ConfiguredTaskAwaitable<TResult> NoCtx<TResult>(this Task<TResult> task) 
		=> task.ConfigureAwait(false);	

	public static TResult NoCtxResult<TResult>(this Task<TResult> task)
		=> task.ConfigureAwait(false).GetAwaiter().GetResult();
		
	public static void NoCtxWait(this Task task)
		=> task.ConfigureAwait(false).GetAwaiter().GetResult();

	public static async Task Cancel(this Task task, CancellationToken token)
	{
		var cancel = new TaskCompletionSource<bool>();
		token.Register(s => (s as TaskCompletionSource<bool>)?.SetResult(true), cancel);

		if (await Task.WhenAny(task, cancel.Task).NoCtx() == task)
			cancel.SetCanceled(token);

		token.ThrowIfCancellationRequested();
	}

	public static async Task<TResult?> Cancel<TResult>(this Task<TResult> task, CancellationToken token)
	{
		var cancel = new TaskCompletionSource<bool>();
		token.Register(s => (s as TaskCompletionSource<bool>)?.SetResult(true), cancel);

		var result = default(TResult);
		if (await Task.WhenAny(task, cancel.Task).NoCtx() == task)
		{
			result = task.Result;
			cancel.SetCanceled(token);
		}

		token.ThrowIfCancellationRequested();
		return result;
	}
}