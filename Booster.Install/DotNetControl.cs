﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.Win32;

#pragma warning disable CA1416 // Validate platform compatibility

namespace Booster.Install;

public class DotNetControl
{
	public static async ValueTask<bool> CheckDotNet(string platform, int version)
	{
		const string subkey = @"SOFTWARE\dotnet\Setup\InstalledVersions";
		var baseKey = Registry.LocalMachine.OpenSubKey(subkey);
		if (baseKey?.SubKeyCount > 0)
			foreach (var platformKey in baseKey.GetSubKeyNames())
			{
				using var key = baseKey.OpenSubKey(platformKey);
				if (key != null)
				{
					var platformName = key.Name[(key.Name.LastIndexOf("\\") + 1)..];
					await Utils.Log($"Platform: {platformName}");

					if (key.SubKeyCount == 0)
						continue;

					var sharedHost = key.OpenSubKey("sharedhost");
					if (sharedHost != null)
					{
						foreach (var name in sharedHost.GetValueNames())
							await Utils.Log($"{name,-8}: {sharedHost.GetValue(name)}");

						var ver = sharedHost.GetValue("Version")?.ToString() ?? "";
						if (int.TryParse(ver.Split(".").FirstOrDefault(), out var major) &&
						    platformName == platform &&
						    major >= version)
							return true;
					}
				}
			}

		return false;
	}
}