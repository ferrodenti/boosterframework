namespace Booster.CodeGen.Interfaces;

public interface IClassBuilder
{
	void Emit(CsBuilder builder);
}