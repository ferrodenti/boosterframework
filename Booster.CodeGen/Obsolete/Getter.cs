﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Reflection.Emit;
using System.Text.RegularExpressions;
using System.Runtime;
using Booster.Helpers;
using Booster.Reflection;

namespace Booster.DynamicCode
{
	[Obsolete]
	public class Getter //TODO: refact
	{
		protected delegate object GetHandler(object source);

		readonly GetHandler _handler;
		readonly List<Getter> _getters;

		public BaseDataMember MemberInfo;
		public TypeEx ValueType;
		public TypeEx TypeCast;
		public string Name;
		public bool IsProperty;
		public bool IsStatic;
		public bool IsGlobal;
		public bool IsExpression;
		public object Index;

		protected Getter(Field fi, TypeEx typeCast = null)
		{
			MemberInfo = fi;
			Name = fi.Name;
			ValueType = fi.FieldType;
			IsStatic = fi.IsStatic;
			IsProperty = false;
			IsExpression = false;
			TypeCast = typeCast;

			var dynamicGet = new DynamicMethod("DynamicGet", typeof(object), new[] { typeof(object) }, fi.DeclaringType, true);
			var getGenerator = dynamicGet.GetILGenerator();

			getGenerator.Emit(OpCodes.Ldarg_0);
			getGenerator.Emit(OpCodes.Ldfld, fi);
			getGenerator.BoxIfNeeded(fi.FieldType);
			getGenerator.Emit(OpCodes.Ret);

			_handler = (GetHandler)dynamicGet.CreateDelegate(typeof(GetHandler));
		}
		
		protected Getter(Property pi, int? index = null, TypeEx typeCast = null)
		{
			if (!pi.CanRead)
				throw new InvalidOperationException($"Cannot build {pi.DeclaringType}.{pi.Name} dynamic setter. Property is writeonly");

			MemberInfo = pi;
			Name = pi.Name;
			ValueType = pi.PropertyType;
			IsStatic = pi.IsStatic;
			IsProperty = true;
			IsExpression = false;
			TypeCast = typeCast;

			if (index.HasValue)
				Index = index.Value;

			var dynamicGet = new DynamicMethod("DynamicGet", typeof (object), new[] {typeof (object)}, pi.DeclaringType, true);
			var getGenerator = dynamicGet.GetILGenerator();
			var getMethodInfo = pi.GetMethod;

			if (!IsStatic)
				getGenerator.Emit(OpCodes.Ldarg_0);

			if (Index != null)
			{
				getGenerator.Emit(OpCodes.Ldarg_1, (int) Index);
				getGenerator.Emit(OpCodes.Box, typeof (int));
			}
			getGenerator.Emit(OpCodes.Call, getMethodInfo);
			getGenerator.BoxIfNeeded(getMethodInfo.ReturnType);
			getGenerator.Emit(OpCodes.Ret);

			_handler = (GetHandler) dynamicGet.CreateDelegate(typeof (GetHandler));
		}

		protected Getter() 
		{
			_getters = new List<Getter>();
			IsExpression = true;
		}

		[TargetedPatchingOptOut("")]
		public object Invoke(object obj)
		{
			if (_getters == null)
				obj = _handler(obj);
			else
				foreach (var getter in _getters)
				{
					obj = getter.Invoke(obj);
					if (obj == null)
						break;
				}

			if (TypeCast != null)
				obj = Convert.ChangeType(obj, TypeCast);

			return obj;
		}

		[TargetedPatchingOptOut("")]
		public T Invoke<T>(object obj)
		{
			return (T)Invoke(obj);
		}

		public override string ToString()
		{
			var str= "";
			if (TypeCast != null) str += "(" + TypeCast.FullName + ")";
			if (MemberInfo != null) str += MemberInfo.DeclaringType?.FullName + ".";
			str += IsExpression ? "{expression}" : Name;
			return str;
		}



		static readonly Regex _typeRxg = new Regex(@"\(([^\(]*)\)", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Multiline);
		static readonly ConcurrentDictionary<string, Getter> _gettersDict = new ConcurrentDictionary<string, Getter>(StringComparer.OrdinalIgnoreCase);

		public static Getter CreateCached(TypeEx type, string expression, bool Static = false)
		{
			var key = type.FullName + "." + expression;
			Getter result;

			if (_gettersDict.TryGetValue(key, out result))
				return result;
			if (_gettersDict.TryGetValue("::" + expression, out result))
				return result;

			if (expression.StartsWith("^"))
				expression = "Envelope." + expression.Substring(1); // IEnveloped

			TypeEx cast = null; //TODO: add cast for sorting
			//{
			//    Match match = _typeRxg.Match(expression);
			//    if (match.Success)
			//    {
			//        cast = FindType(match.Result("$1"), true, true);
			//        expression = expression.Replace(match.Value, "");
			//    }
			//}

			if (!expression.Contains("."))
			{
				string index = null;
				var i = expression.IndexOf('[');
				if (i != -1)
				{
					var j = expression.LastIndexOf(']');
					if (j == -1)
						throw new ReflectionException(type, expression);

					index = expression.Substring(i + 1, j - i - 2);
					expression = expression.Substring(i);
				}

				var mi = type.FindDataMember(new ReflectionFilter(expression) {IsStatic = Static ? (bool?) null : false, IgnoreCase = true});

// ReSharper disable ConditionIsAlwaysTrueOrFalse
				if (mi != null)
				{
					if (mi.DeclaringType != type)
					{
						result = CreateCached(mi, cast);
						if (result != null)
							goto save;
					}

					var fi = mi as Field;
					if (fi != null)
						result = new Getter(fi, cast);
					else
					{
						var pi = mi as Property;
						if (pi != null && pi.CanRead)
							result = new Getter(pi, index != null ? (int?) int.Parse(index) : null, cast);
					}
				}
// ReSharper restore ConditionIsAlwaysTrueOrFalse
			}
			else
			{
				var casts = new List<TypeEx>();
				foreach (Match match in _typeRxg.Matches(expression))
				{
					var typeCast = AssemblyHelper.FindType(match.Result("$1"));
					if (typeCast != null)
					{
						casts.Add(typeCast);
						expression = expression.Replace(match.Value, "*");
					}
				}

				var i = 0;

				result = new Getter();

				foreach (var field in expression.Split(new [] {'.'}, StringSplitOptions.RemoveEmptyEntries ))
				{
					Getter getter;

					if (field.EndsWith("*"))
					{
						getter = CreateCached(type, field.Substring(0, field.Length - 1), Static);
						type = casts[i++];
					}
					else
					{
						getter = CreateCached(type, field, Static);
						if (getter != null)
							type = getter.ValueType;
					}

					if (getter == null)
					{
						result = null;
						break;
					}

					Static = false;
					result._getters.Add(getter);
					result.ValueType = type;
				}
			}
		save:
			_gettersDict[key] = result;
			return result;
		}

		public static Getter CreateCached(BaseDataMember mi, TypeEx cast = null)
		{
			var key = mi.DeclaringType.FullName + "." + mi.Name;
			Getter result;

			if (_gettersDict.TryGetValue(key, out result) && result.MemberInfo == mi)
				return result;

			var fi = mi as Field;
			if (fi != null)
				result = new Getter(fi, cast);
			else
			{
				var pi = mi as Property;
				if( pi != null && pi.CanRead)
					result = new Getter(pi, null, cast);
			}
			_gettersDict[key] = result;
			return result;
		}


		public static Getter CreateCached(string expression)
		{
			var key = "::" + expression;
			Getter result;

			if (_gettersDict.TryGetValue(key, out result))
				return result;

			for (var i=0; i < expression.Length; i++)
			{
				if (expression[i] == '.')
				{
					var typeName = expression.Substring(0, i);

					var type = AssemblyHelper.FindType(typeName);
					if (type != null)
					{
						result = CreateCached(type, expression.Substring(i + 1), true);

						if (result != null)
						{
							result.IsGlobal = true;
							_gettersDict[key] = result;
						}
						return result;
					}
				}
			}
			return null;
		}
	}
}