using System;
using System.Collections.Generic;
using Booster.Reflection;

namespace Booster.DynamicCode
{
	[Obsolete]
	public class DataMemberMetadata //TODO: remove
	{
		public string Name => Inner.Name;

		public BaseDataMember Inner { get; }

		public DataMemberMetadata(BaseDataMember memberInfo)
		{
			Inner = memberInfo;
		}

		public IEnumerable<TAttribute> GetAttributes<TAttribute>() where TAttribute : Attribute
		{
			return Inner.FindAttributes<TAttribute>();
		}

		public TAttribute GetAttribute<TAttribute>() where TAttribute : Attribute
		{
			return Inner.FindAttribute<TAttribute>();
		}
	}
}