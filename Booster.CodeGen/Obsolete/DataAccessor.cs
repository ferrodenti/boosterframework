using System;
using Booster.Interfaces;
using Booster.Reflection;

namespace Booster.DynamicCode
{
	[Obsolete]
	public class DataAccessor : IDataAccessor
	{
		readonly BaseDataMember _mi;

		readonly object _lock = new object();

		public string Name => _mi.Name;
		public TypeEx ValueType => _mi.ReturnType;
		public bool CanRead => _mi.CanRead;
		public bool CanWrite => _mi.CanWrite;
		public bool PublicSetter => _mi.HasPublicSetter;
		public bool PublicGetter => _mi.HasPublicGetter;

		Getter _getter;
		protected Getter Getter
		{
			get
			{
				if (_getter == null && CanRead)
					lock (_lock)
						if (_getter == null)
							_getter = Getter.CreateCached(_mi);

				return _getter;
			}
		}

		Setter _setter;
		protected Setter Setter
		{
			get
			{
				if (_setter == null && CanWrite)
					lock (_lock)
						if (_setter == null)
							_setter = Setter.CreateCached(_mi);

				return _setter;
			}
		}

		public object GetValue(object obj)
		{
			return Getter?.Invoke(obj);
		}
		public bool SetValue(object obj, object value)
		{
			var setter = Setter;
			if (setter != null)
			{
				setter.Invoke(obj, value);
				return true;
			}
			return false;
		}

		public DataAccessor(BaseDataMember mi)
		{
			_mi = mi;
		}
	}
}