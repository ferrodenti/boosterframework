﻿using System;
using System.Collections.Concurrent;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime;
using Booster.Reflection;

// ReSharper disable once CheckNamespace
namespace Booster.DynamicCode
{
	[Obsolete]
	public class Setter //TODO: refact
	{
		protected delegate void SetHandler(object source, object value); // TODO: надо проверить, что быстре: прямой вызов, или с поздней привязкой

		readonly SetHandler _handler;
		public readonly BaseDataMember MemberInfo;
		public TypeEx ValueType;
		public readonly string Name;
		public bool IsProperty;
		public readonly bool IsStatic;
		public bool IsPublic;
		public bool IsPrivate;

		protected Setter(Field fi)
		{
			MemberInfo = fi;
			Name = fi.Name;
			ValueType = fi.FieldType;
			IsStatic = fi.IsStatic;
			IsProperty = false;
			IsPublic = fi.IsPublic;
			IsPrivate = fi.IsPrivate;

			var dynamicSet = new DynamicMethod("DynamicSet", typeof(void), new[] { typeof(object), typeof(object) }, fi.DeclaringType, true);
			var setGenerator = dynamicSet.GetILGenerator();

			setGenerator.Emit(OpCodes.Ldarg_0);
			setGenerator.Emit(OpCodes.Ldarg_1);
			setGenerator.UnboxIfNeeded(fi.FieldType);
			setGenerator.Emit(OpCodes.Stfld, fi);
			setGenerator.Emit(OpCodes.Ret);

			_handler = (SetHandler)dynamicSet.CreateDelegate(typeof(SetHandler));
		}
		
		protected Setter(Property pi)
		{
			MethodInfo setMethodInfo = pi.SetMethod;
			if (!pi.CanWrite || setMethodInfo == null)
				throw new InvalidOperationException($"Cannot build {pi.DeclaringType}.{pi.Name} dynamic setter. Property is readonly");

			MemberInfo = pi;
			Name = pi.Name;
			ValueType = pi.PropertyType;
			IsStatic = pi.IsStatic;
			IsProperty = true;

			IsPublic = setMethodInfo.IsPublic;
			IsPrivate = setMethodInfo.IsPrivate;


			var dynamicSet = new DynamicMethod("DynamicSet", typeof(void), new[] { typeof(object), typeof(object) }, pi.DeclaringType, true);
			var setGenerator = dynamicSet.GetILGenerator();

			if (!IsStatic)
				setGenerator.Emit(OpCodes.Ldarg_0);

			setGenerator.Emit(OpCodes.Ldarg_1);
			setGenerator.UnboxIfNeeded(setMethodInfo.GetParameters()[0].ParameterType);
			setGenerator.Emit(OpCodes.Call, setMethodInfo);
			setGenerator.Emit(OpCodes.Ret);

			_handler = (SetHandler)dynamicSet.CreateDelegate(typeof(SetHandler));
		}

		[TargetedPatchingOptOut("")]
		public void Invoke(object obj, object value)
		{
			_handler(obj, value);
		}

		public override string ToString()
		{
			var str= "";

			if (MemberInfo?.DeclaringType != null)
				str += MemberInfo.DeclaringType.FullName + ".";

			str += Name;
			return str;
		}

		static readonly ConcurrentDictionary<string,Setter> _setters = new ConcurrentDictionary<string,Setter>(StringComparer.OrdinalIgnoreCase);

		public static Setter CreateCached(TypeEx type, string name, bool Static = false)
		{
			var key = type.FullName + "." + name;

			if (_setters.TryGetValue(key, out var result))
				return result;

			var mi = type.FindDataMember(new ReflectionFilter(name) {IsStatic = Static ? (bool?) null : false, IgnoreCase = true});

			if (mi != null)
			{
				if (mi.DeclaringType != type)
				{
					result = CreateCached(mi);
					if (result != null)
						goto save;
				}

				if (mi is Field fi)
					result = new Setter(fi);
				else
				{
					if (mi is Property pi && pi.CanWrite)
						result = new Setter(pi);
				}
			}

			save:
			_setters[key] = result;
			return result;
		}

		public static Setter CreateCached(BaseDataMember mi)
		{
			var key = mi.DeclaringType?.FullName + "." + mi.Name;

			if (_setters.TryGetValue(key, out var result) && ReferenceEquals(result.MemberInfo, mi))
				return result;

			if (mi is Field fi)
				result = new Setter(fi);
			else
			{
				if( mi is Property pi && pi.CanWrite)
					result = new Setter(pi);
			}
			_setters[key] = result;
			return result;
		}
	}
}