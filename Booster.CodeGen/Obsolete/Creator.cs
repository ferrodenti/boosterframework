using System;
using System.Collections.Concurrent;
using System.Reflection;
using System.Reflection.Emit;
using Booster.Reflection;

namespace Booster.DynamicCode
{
	public static class Creator
	{
		public delegate object FactoryProc(object[] arg);

		static readonly ConcurrentDictionary<FactoryProcKey, FactoryProc> _ctors = new ConcurrentDictionary<FactoryProcKey, FactoryProc>();

		public static T Create<T>() { return (T)Create(typeof(T)); } // Для вызова без аргументов активатор стал работать быстрее всего, видимо, благодаря типам времени исполнения
		public static object Create(TypeEx type) { return Activator.CreateInstance(type, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance, null, new object[0], null); }

		public static T Create<T>(params object[] arguments)
		{
			return (T)Create(typeof(T), arguments);
		}
		public static T Create<T>(TypeEx type, params object[] arguments)
		{
			return (T)Create(type, arguments);
		}

		public static object Create(TypeEx type, params object[] arguments)
		{
			var args = arguments.Length;
			var argTypes = new TypeEx[args];
			for (var i=0; i < args; i++)
				argTypes[i] = arguments[i]?.GetType() ?? typeof(object);

			var invocator = GetFactoryProc(type, argTypes);

			return invocator.Invoke(arguments);
		}

		public static FactoryProc GetFactoryProc<T>(TypeEx[] parameterTypes)
		{
			return GetFactoryProc(typeof (T), parameterTypes);
		}
		public static FactoryProc GetFactoryProc(TypeEx type, TypeEx[] parameterTypes)
		{
			return _ctors.GetOrAdd(new FactoryProcKey(type, parameterTypes), valueFactory: k => CreateFactoryProc(type, parameterTypes));
		}

		static FactoryProc CreateFactoryProc(TypeEx type, TypeEx[] parameterTypes)
		{
			var ctorInfo = type.FindConstructor(parameterTypes);

			if (ctorInfo == null)
				throw new ReflectionException($"{type}.ctor({parameterTypes.ToString(p => p.Name)}) not found.");

			var isVisible = type.IsVisible && ctorInfo.IsPublic && !ctorInfo.IsInternal;

			var dynamicMethod = new DynamicMethod(Guid.NewGuid().ToString("N"),
			                                                typeof (object),
															new [] { typeof (object[]) },
			                                                ctorInfo.Module, !isVisible);

			var il = dynamicMethod.GetILGenerator();
			var param = ctorInfo.Parameters;

			for (var i = 0; i < param.Length; ++i)
			{
			    il.Emit(OpCodes.Ldarg_0); // берем массив из стека
				switch (i) // пишем смещение
                {
                    case 0: il.Emit(OpCodes.Ldc_I4_0); break;
                    case 1: il.Emit(OpCodes.Ldc_I4_1); break;
                    case 2: il.Emit(OpCodes.Ldc_I4_2); break;
                    case 3: il.Emit(OpCodes.Ldc_I4_3); break;
                    default: il.Emit(OpCodes.Ldc_I4, i); break;
                }
			    il.Emit(OpCodes.Ldelem_Ref); // Извлекаем элемент
			    il.UnboxIfNeeded(param[i].ParameterType); // Делаем анбоксинг вэлью-типов
			}

			il.Emit(OpCodes.Newobj, ctorInfo); // Зовем конструктор
			il.BoxIfNeeded(type); // Делаем боксинг вэлью-типов
			il.Emit(OpCodes.Ret); // Все

			return (FactoryProc)dynamicMethod.CreateDelegate(typeof (FactoryProc));
		}

		class FactoryProcKey
		{
			readonly TypeEx _type;
			readonly TypeEx[] _arguments;
			readonly int _hash;

			public FactoryProcKey(TypeEx type, TypeEx[] arguments)
			{
				_arguments = arguments;
				_type = type;

				 unchecked { _hash = _type.GetHashCode() | _arguments.Length; }
			}

			public override int GetHashCode() { return _hash; }

			public override bool Equals(object obj)
			{
				var key = (FactoryProcKey) obj; //Проверку на null не делаем, т.к. подразумевается, что иного сравнения тут быть не может

				if (_type != key?._type) return false;

				var args = _arguments.Length;
				if (key?._arguments.Length != args ) return false;
				for (var i=0; i < args; i++)
					if (_arguments[i] != key._arguments[i])
						return false;

				return true;
			}
		}
	}
}