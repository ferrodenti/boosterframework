using System;
using System.Text;
using JetBrains.Annotations;

namespace Booster.CodeGen;

public class IdentedStringBuilder
{
	readonly StringBuilder _inner;

	public int Ident { get; set; }

	public bool FixLineEndings { get; set; } = true;

	public int Length => _inner.Length;

	int? _lastIdent;
	protected int LastIdent
	{
		get
		{
			if (_lastIdent == null)
			{
				var r = 0;
				for (var i = _inner.Length - 1; i >= 0; i--)
				{
					var c = _inner[i];
					if (c == '\t')
						r++;
					else if (c == '\n')
						break;
					else
					{
						r = -1;
						break;
					}
				}
				_lastIdent = r;
			}
			return _lastIdent.Value;
		}
	}
	[PublicAPI] public IdentedStringBuilder(StringBuilder str, int ident = 0)
	{
		_inner = str;
		Ident = ident;
	}
	public IdentedStringBuilder(string str, int ident = 0)
	{
		_inner = new StringBuilder();
		Ident = ident;
		Append(str);
	}
	public IdentedStringBuilder(int ident = 0)
	{
		_inner = new StringBuilder();
		Ident = ident;
	}

		
	public IDisposable IncIdent(int cnt = 1)
	{
		var old = Ident;
		Ident += cnt;
		return new ActionDisposable(() => Ident = old);
	}

	public void Append(string str)
	{
		int j = 0, i;
		while ((i = str.IndexOf('\n', j)) >= 0)
		{
			EnsureIdented();

			if (FixLineEndings)
			{
				var prev = i - 1;
				if (prev >= 0)
				{
					if (str[prev] == '\r')
						_inner.Append(str.Substring(j, i + 1 - j));
					else
					{
						_inner.Append(str.Substring(j, i - j));
						_inner.Append(Environment.NewLine);
					}
				}
				else
					_inner.Append(Environment.NewLine);
			}
			else
				_inner.Append(str.Substring(j, i + 1 - j));

			_lastIdent = 0;
			j = i + 1;
		}

		if (j < str.Length)
		{
			EnsureIdented();
			_inner.Append(str.Substring(j));
			_lastIdent = -1;
		}
	}

	public void EnsureNewLine()
	{
		if (LastIdent < 0)
		{
			_inner.Append(Environment.NewLine);
			_lastIdent = 0;
		}
	}

	public void EnsureIdented()
	{
		if (LastIdent >= 0)
		{
			for (var _ = LastIdent; _ < Ident; _ ++)
				_inner.Append('\t');

			_lastIdent = Ident;
		}
	}

	public void AppendLine() => Append(Environment.NewLine);

	public void AppendLine(string str)
	{
		Append(str);
		Append(Environment.NewLine);
	}

	public void AppendRaw(string str) => _inner.Append(str);

	public override string ToString() => _inner.ToString();

	public static implicit operator string(IdentedStringBuilder builder) => builder.ToString();
}