using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Booster.Reflection;

namespace Booster.CodeGen;

public class CsBuilder : IdentedStringBuilder
{
	public HashSet<Assembly> Assemblies { get; } = new();	
	public HashSet<string> Usings { get; } = new();
	public Dictionary<string, string> Aliases { get; } = new();

	readonly HashSet<TypeEx> _refferencedAssyTypes = new();
	readonly Dictionary<TypeEx, string> _cache = new();
	readonly Dictionary<string, TypeEx> _backTrack = new();
	readonly Dictionary<string, bool> _existingTypes = new();

	public CsBuilder(int ident = 0) : base(ident)
	{
	}
		
	public string Type<T>() 
		=> TypeRef(typeof (T));

	public string TypeRef(TypeEx type)
	{
		RefAssemblies(type);
		return Emit(type);
	}

	public string Emit(Method method)
		=> !method.IsGeneric 
			? method.Name 
			: $"{method.Name}{EmitGenericArguments(method.GenericArguments)}";

	public string EmitGenericArguments(IEnumerable<TypeEx> arguments) 
		=> arguments.ToString(TypeRef, new EnumBuilder(", ", "<", ">"));

	public void Alias(string name, TypeEx type)
	{
		RefAssemblies(type);
		Aliases[name] = type.FullName;
	}
		
	public void Alias(string name, string @namespace)
		=> Aliases[name] = @namespace;

	public void Using(string @namespace) 
		=> Usings.Add(@namespace);

	public void UsingFor<T>() 
		=> UsingFor(typeof (T));

	public void UsingFor(TypeEx type)
	{
		if (!string.IsNullOrEmpty(type.Namespace))
			Usings.Add(type.Namespace);

		RefAssemblies(type);
	}

	public void RefAssemblies<T>() 
		=> RefAssemblies(typeof (T));

	public void RefAssemblies(TypeEx type)
	{
		while (true)
		{
			if (_refferencedAssyTypes.Contains(type))
				return;

			_refferencedAssyTypes.Add(type);

			if (type.IsGenericType)
				foreach (var t in type.GenericArguments)
					RefAssemblies(t);

			if (!Assemblies.Contains(type.Assembly))
				Assemblies.Add(type.Assembly);

			foreach (var t in type.Interfaces) 
				RefAssemblies(t);

			if (type.BaseType != null && type.BaseType != typeof(object))
			{
				type = type.BaseType;
				continue;
			}

			foreach (var pair in _existingTypes.ToArray())
				if (!pair.Value)
					_existingTypes.Remove(pair.Key);

			return;
		}
	}

	public string Emit(TypeEx type)
	{
		if (type == typeof(object))
			return "object";

		if (type.IsGenericParameter)
			return type.Name;
			
		if (type.GenericTypeDefinition == typeof (Nullable<>))
			return $"{Emit(type.GenericArguments[0])}?";
			
		if (type.IsArray)
			return $"{Emit(type.ArrayItemType)}[]";

		var def = type.IsGenericTypeDefinition;
		if (def || type.IsGenericType)
			return GetName(def ? type : type.GenericTypeDefinition) + EmitGenericArguments(type.GenericArguments);

		return GetName(type);	
	}

	bool TypeExists(string name)
		=> _existingTypes.GetOrAdd(name, _ => Assemblies.Any(assy => assy.GetType(name) != null));

	string GetName(TypeEx type)
	{
		if (type.IsNested)
			return $"{Emit(type.DeclaringType)}.{RemoveGeneric(type)}";

		return type.BuildInName ?? _cache.GetOrAdd(type, _ =>
		{
			var tmpl = RemoveGeneric(type); //TODO: Task & Task<int> results in full name

			var nm = tmpl;

			for (var i = 1; ; i++)
			{
				if(!_backTrack.TryGetValue(nm, out _))
					break;

				if (type.IsGenericTypeDefinition || type.IsGenericType)
					return RemoveGeneric(type, false);
						
				nm = tmpl + i;
			}

			if (nm != tmpl)
				Aliases.Add(nm, type.FullName);
			else
			{
				foreach (var pair in _cache)
					if (type.Namespace != pair.Key.Namespace && TypeExists($"{type.Namespace}.{pair.Value}"))
						Aliases.Add(pair.Value, pair.Key.FullName);

				if (Usings
					.Where(usng => usng != type.Namespace)
					.Any(usg => TypeExists($"{usg}.{nm}")))
					nm = type.FullName;
				else
					Usings.Add(type.Namespace);
			}

			_backTrack.Add(nm, type);

			return nm;
		});
	}

	static string RemoveGeneric(TypeEx type, bool shortName = true)
	{
		var name = shortName ? type.Name : type.FullName;
		if (type.IsGenericTypeDefinition || type.IsGenericType)
		{
			var cut = name.IndexOf('`');
			return cut > 0 ? name.Substring(0, cut) : name;
		}

		return name;
	}

	public string EmitUsings()
	{
		var result = new EnumBuilder(Environment.NewLine);
		foreach (var ns in Usings.OrderBy(ns => ns, new LambdaComparer<string>(ns => !ns.StartsWith("System"), ns => ns)))
			result.Append($"using {ns};");

		foreach (var pair in Aliases.OrderBy(pair => pair.Value, new LambdaComparer<string>(ns => !ns.StartsWith("System"), ns => ns)))
			result.Append($"using {pair.Key} = {pair.Value};");

		return result;
	}
}