using System;
using Booster.CodeGen.Interfaces;
using Booster.Reflection;
using JetBrains.Annotations;

#nullable enable

namespace Booster.CodeGen;

public abstract class BaseClassBuilder : IClassBuilder
{
	public virtual bool IsStruct { get; set; }
	public virtual string Access { get; set; } = "public";
	public virtual string? ClassName { get; set; }
	public virtual string? Namespace { get; set; }

	protected BaseClassBuilder(string? className = null)
		// ReSharper disable once VirtualMemberCallInConstructor
		=> ClassName = className;

	public virtual string? Inherits(CsBuilder builder)
		=> null;

	protected virtual void EmitAttributes(CsBuilder builder)
	{
	}
		
	public virtual void Emit(CsBuilder builder)
	{
		using (Namespace.IsEmpty() ? null : EmitBlock(builder, $"namespace {Namespace}"))
		{
			if (ClassName.IsEmpty())
				throw new Exception("ClassName not spesified");

			EmitAttributes(builder);

			var inherits = Inherits(builder);
			using (EmitBlock(builder, $"{Access} {(IsStruct ? "struct" : "class")} {ClassName} {(inherits.IsEmpty() ? "" : $" : {inherits}")}"))
			{
				//using (source.IncIdent())
				EmitMembers(builder);

				builder.EnsureNewLine();
			}
		}
	}

	[PublicAPI] public static IDisposable EmitRegion(CsBuilder builder, string name)
	{
		builder.AppendLine($"//#region {name}");
		builder.AppendLine();

		return new ActionDisposable(() =>
		{
			builder.AppendLine($"//#endregion {name}");
			builder.AppendLine();
		});
	}

	public static IDisposable EmitMethod(CsBuilder builder, string signature)
	{
		builder.AppendLine(signature);
		builder.AppendLine("{");
		var old = builder.Ident;
		builder.Ident ++;

		return new ActionDisposable(() =>
		{
			builder.Ident = old;
			builder.AppendLine("}");
			builder.AppendLine();
		});
	}

	public static IDisposable EmitBlock(CsBuilder builder, string str, int cnt = 1)
	{
		builder.AppendLine(str);
		builder.AppendLine("{");
		var old = builder.Ident;
		builder.Ident += cnt;

		return new ActionDisposable(() =>
		{
			builder.Ident = old;
			builder.AppendLine("}");
		});
	}

	public abstract void EmitMembers(CsBuilder builder);
}