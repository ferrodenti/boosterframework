using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Threading.Tasks;
using Booster.FileSystem;
using Booster.Helpers;
using Booster.Log;
using Booster.Reflection;
using JetBrains.Annotations;
#if ROSLYN
using System.Linq;
using System.Text;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Booster.DI;
#else
using System.CodeDom.Compiler;
using Microsoft.CSharp;
#endif

#nullable enable

namespace Booster.CodeGen;

public class Compiler
{
	readonly ILog? _log;

	FilePath? _tempPath;
	public FilePath TempPath
	{
		get => _tempPath ??= new FilePath(Path.GetTempPath()) + (Assembly.GetEntryAssembly() ?? AssemblyHelper.BoosterCallingAssembly).GetName().Name;
		set => _tempPath = value;
	}


	/// <summary>
	/// Будут создаваться исходники для отладки с произвольным именем типа c:\Users\%UserName%\AppData\Local\Temp\ModuleName\j0njt3ee.0.cs
	/// </summary>
	public bool AllowDebugDynamicSource;

	public Compiler(ILog? log = null)
	{
		AllowDebugDynamicSource = Debugger.IsAttached;
		_log = log.Create(this);
	}

#if !ROSLYN
	[PublicAPI]
	public static bool IsRoslyn => false;

	string? _compilerVersion;
	public string CompilerVersion
	{
		get => _compilerVersion ??= System.Runtime.InteropServices.RuntimeEnvironment.GetSystemVersion().Substring(0, 4);
		set => _compilerVersion = value;
	}
#else
	public static bool IsRoslyn => true;
	static async Task<Assembly> BuildAssembly(string code, Compilation compilation, bool createPdb, string? filename)
	{
#if NETSTANDARD
		await using var output = new MemoryStream();
		await using var pdbOutput = createPdb ? new MemoryStream() : null;
#else
		using var output = new MemoryStream();
		using var pdbOutput = createPdb ? new MemoryStream() : null;
#endif
		
		var result = compilation.Emit(output, pdbOutput);

		result.ThrowIfErrors(code, InstanceFactory.Get<ILog>(false));

		await output.FlushAsync().NoCtx();
		output.Seek(0, SeekOrigin.Begin);

		if (createPdb)
		{
			await pdbOutput!.FlushAsync().NoCtx();
			pdbOutput.Seek(0, SeekOrigin.Begin);
		}

		var dll = output.ToArray();
		var pdb = pdbOutput?.ToArray();

		if (filename != null)
		{
			var dllFilename = Path.ChangeExtension(filename, ".dll");
			await FileAsync.WriteAllBytesAsync(dllFilename, dll);

			if (createPdb)
			{
				var pdbFilename = Path.ChangeExtension(filename, ".pdb");
				await FileAsync.WriteAllBytesAsync(pdbFilename, pdb);
			}
		}

		return Assembly.Load(dll, pdb);
	}
#endif

	public async Task<Assembly> CompileAssembly(string code, IEnumerable<Assembly> references, string? filename)
	{
#if ROSLYN
			SyntaxTree syntaxTree;

			if (AllowDebugDynamicSource)
			{
				await FileUtils.EnsureDirectoryAsync(TempPath, new TaskRepeatOptions {Log = _log}).NoCtx(); 
				var path = Path.ChangeExtension(Path.Combine(TempPath, Path.GetRandomFileName()), ".cs");

				await File.WriteAllTextAsync(path, code, Encoding.UTF8).NoCtx();

				syntaxTree = CSharpSyntaxTree.ParseText(code, path: path, encoding: Encoding.UTF8);
			}
			else
				syntaxTree = CSharpSyntaxTree.ParseText(code);
			
			var assemblyName = filename.With(Path.GetFileNameWithoutExtension).Or(Path.GetRandomFileName());	
			var refs = references.Select(assy => MetadataReference.CreateFromFile(assy.Location));
			
#if NETSTANDARD
			var assemblyPath = Path.GetDirectoryName(typeof(object).Assembly.Location) ?? "";
			refs = refs.AppendBefore(MetadataReference.CreateFromFile(Path.Combine(assemblyPath, "System.Runtime.dll")));
			refs = refs.AppendBefore(MetadataReference.CreateFromFile(Path.Combine(assemblyPath, "System.Collections.dll")));
			refs = refs.AppendBefore(MetadataReference.CreateFromFile(Path.Combine(assemblyPath, "netstandard.dll")));
#endif
			var compilation = CSharpCompilation.Create(assemblyName,
				new[] {syntaxTree},
				refs,
				new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary));

			return await BuildAssembly(code, compilation, AllowDebugDynamicSource, filename).NoCtx();
#else

		var compiler = new CSharpCodeProvider(new Dictionary<string, string>
		{
			{"CompilerVersion", CompilerVersion}
		});

		async Task<CompilerResults> TryCompile()
		{
			var cParams = new CompilerParameters
			{
				GenerateInMemory = filename.IsEmpty(),
				GenerateExecutable = false,
				TreatWarningsAsErrors = false,
			};

			try
			{
				if (AllowDebugDynamicSource)
				{
					/*if (!Directory.Exists(TempPath))
					{
						_log.Info($"Создаю директорию для временных файлов: {TempPath}");
						Directory.CreateDirectory(TempPath);
					}*/
					if (!await FileUtils.EnsureDirectoryAsync(TempPath, new TaskRepeatOptions { Log = _log }).NoCtx())
						_log.Warn($"Не смог создать директорию: {TempPath}");

					cParams.IncludeDebugInformation = true;
					cParams.TempFiles = new TempFileCollection(TempPath, true);
				}
			}
			catch (Exception ex)
			{
				_log.Error(ex, $"Ошибка создания директории для временных файлов: {TempPath}");
				_log.Warn("Переключаюсь на создание сборок в памяти");
				AllowDebugDynamicSource = false;
			}

			if (filename.IsSome())
				cParams.OutputAssembly = filename;

			// ReSharper disable once PossibleMultipleEnumeration
			foreach (var a in references)
				cParams.ReferencedAssemblies.Add(a.Location);

			return compiler.CompileAssemblyFromSource(cParams, code);
		}

		async Task<CompilerResults> Compile()
		{
			try
			{
				return await TryCompile().NoCtx();
			}
			catch
			{
				if (AllowDebugDynamicSource)
				{
					if (!Directory.Exists(TempPath))
						_log.Error($"Путь для генерации временных сборок не существует: {TempPath}");
					else if (!FileUtils.CheckFolderWriteAccess(TempPath))
						_log.Error($"Отсутствуют разрешения для генерации временных сборок, путь: {TempPath}");
					else
						throw;

					_log.Warn("Переключаюсь на создание сборок в памяти");
					AllowDebugDynamicSource = false;
					return await TryCompile().NoCtx();
				}
				throw;
			}
		}

		var compResults = await Compile().NoCtx();

		if (compResults.Errors.HasErrors)
		{
			var exception = new CompilationException(code);
			var lines = code.Split('\n');

			foreach (CompilerError err in compResults.Errors)
			{
				var line = lines[err.Line > 0 ? err.Line - 1 : 0].Replace("\r", "");
				var diff = line.Length;
				line = line.TrimStart("\t ".ToCharArray());
				diff = line.Length - diff;
				exception.AddError(err.Line, err.Column, err.Column + diff, line, err.ErrorNumber, err.ErrorText);
			}

			throw exception;
		}

		return compResults.CompiledAssembly;
#endif
	}

	public async Task<TypeEx> CompileType(string code, string typeName, IEnumerable<Assembly> refferences, string? filename)
	{
		var assy = await CompileAssembly(code, refferences, filename).NoCtx();
		return assy.GetType(typeName);
	}
}
