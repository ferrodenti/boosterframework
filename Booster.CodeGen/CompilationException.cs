using System;
using System.Collections.Generic;

#nullable enable

namespace Booster.CodeGen;

public class CompilationException : Exception
{
	public List<CompilationError> Errors { get; } = new();

	string? Name { get; }
	string? _message;

	public string Code { get; }

	public CompilationException(string code, string? name = null)
	{
		Code = code;
		Name = name;
	}

	public override string Message => _message ??= $"{Name.With(n => $"{n} ")}compiled with {Errors.Count} errors:\n{Errors.ToString(new EnumBuilder("\n\n"))}";


	public void AddError(CompilationError error) => Errors.Add(error);

	public void AddError(int line, int column, int columnNoTab, string source, string errorCode, string errorText, int numberOfColumns = 1)
		=> AddError(new CompilationError(line, column, columnNoTab, source, errorCode, errorText, numberOfColumns));
}