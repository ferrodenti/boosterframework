namespace Booster.CodeGen;

public class CompilationError
{
	public string Source { get; }
	public int Line { get; }
	public int Column { get; }
	public int ColumnNoTab { get; }
	public int NumberOfColumns { get; }
	public string ErrorCode { get; }
	public string ErrorText { get; }

	public CompilationError(int line, int column, int columnNoTab, string source, string errorCode, string errorText, int numberOfColumns = 1)
	{
		Line = line;
		Column = column;
		ColumnNoTab = columnNoTab;
		Source = source;
		ErrorCode = errorCode;
		ErrorText = errorText;
		NumberOfColumns = numberOfColumns;
	}

	public override string ToString()
	{
		var pref = $"ln {Line}: ";
		return $"{pref}{Source.TrimEnd()}\n{new string(' ', pref.Length + ColumnNoTab)}{new string('^', NumberOfColumns)} {ErrorCode}: {ErrorText}";
	}
}