using System;
using System.Collections.Generic;
using Booster.CodeGen.Interfaces;
using JetBrains.Annotations;

namespace Booster.CodeGen;

[PublicAPI]
public class ModuleBuilder
{
	public string Namespace { get; set; } = "Namespace_for_dynamic_classes";
	public string Message { get; set; } = "DO NOT MODIFY! THIS FILE WAS GENERATED";
	public string CustomBeforeClasses { get; set; }
		
	readonly List<IClassBuilder> _classBuilders = new();

	public void AddClass(IClassBuilder bld) 
		=> _classBuilders.Add(bld);

	public CsBuilder GetSourceCode(CsBuilder builder = null)
	{
		builder ??= new CsBuilder();
		using (Namespace.IsSome() ? BaseClassBuilder.EmitBlock(builder, $"namespace {Namespace}") : null)
			foreach (var cls in _classBuilders)
			{
				builder.EnsureNewLine();
				cls.Emit(builder);
				builder.EnsureNewLine();
			}

		var result = new CsBuilder();

		if (Message.IsSome())
		{
			foreach (var s in Message.SplitNonEmpty('\n'))
				result.Append($"// {s}\n"); // в строка может кончаться на \r, поэтому здесь не AppendLine, а билдер поправит окончание строк сам

			result.AppendLine();
		}
			
		var usings = builder.EmitUsings();
		if (usings.IsSome())
		{
			result.AppendLine(usings);
			result.AppendLine();
		}
			
		if (CustomBeforeClasses.IsSome())
		{
			result.AppendLine(CustomBeforeClasses);
			result.AppendLine();
		}

		result.AppendRaw(builder);
		result.Assemblies.AddRange(builder.Assemblies);

		return result;
	}

	public override string ToString()
	{
		try
		{
			return GetSourceCode();
		}
		catch (Exception e)
		{
			return e.ToString();
		}
	}
}