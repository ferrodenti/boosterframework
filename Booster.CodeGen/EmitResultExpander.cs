using System;
using Booster.Log;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Emit;

#nullable enable

namespace Booster.CodeGen;

public static class EmitResultExpander
{
	public static void ThrowIfErrors(this EmitResult emitResult, string code, ILog? log)
	{
		if (emitResult.Success)
			return;

		var exception = new CompilationException(code);

		foreach (var error in emitResult.Diagnostics)
			try
			{
				if (!error.IsWarningAsError && error.Severity != DiagnosticSeverity.Error) continue;

				var start = code.LastIndexOf('\n', error.Location.SourceSpan.Start, error.Location.SourceSpan.Start) + 1;
				var end = code.IndexOf('\n', error.Location.SourceSpan.End);
				if (end < 0)
					end = code.Length;

				var line = end > start
					? code.Substring(start, end - start)
					: code.Substring(start, 20);

				var diff = line.Length;
				line = line.TrimStart('\t', ' ');
				diff = line.Length - diff;

				var pos = error.Location.GetLineSpan();
				var cnt = Math.Max(pos.EndLinePosition.Character - pos.StartLinePosition.Character, 1);
				exception.AddError(pos.StartLinePosition.Line, pos.StartLinePosition.Character,
					pos.StartLinePosition.Character + diff, line, error.Id, error.GetMessage(), cnt);
			}
			catch (Exception e)
			{
				log.Error(e);
			}

		throw exception;
	}
}