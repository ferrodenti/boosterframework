using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Booster.Log;
using Booster.Reflection;
using JetBrains.Annotations;
#if NETSTANDARD
using Microsoft.CodeAnalysis;
#endif

#nullable enable

namespace Booster.CodeGen;

[PublicAPI]
public class Executer
{
	sealed class Builder : BaseClassBuilder
	{
		public readonly CsBuilder CsBuilder = new();
		public Dictionary<string, TypeEx> Parameters { get; } = new();

		readonly string? _code;

		public Builder(string? code) : base("Executor")
		{
			_code = code?.Trim();
			Access = "public static";
			Namespace = "Booster.CodeGen.Dynamic";
		}

		static readonly Regex _awaitRegex = Utils.CompileRegex(@"([^\w]|^)await([^\w]|$)");
		static readonly Regex _returnRegex = Utils.CompileRegex(@"([^\w]|^)return([^\w]|$)");

		public void Emit()
			=> Emit(CsBuilder);

		public override void EmitMembers(CsBuilder builder)
		{
			if (_code.IsEmpty()) 
				return;

			string retType, async = "";

			builder.UsingFor<Action>();

			if (_awaitRegex.IsMatch(_code))
			{
				builder.UsingFor<Task>();
				async = "async ";
				retType = "Task<object>";
				builder.AppendLine(@"
static async Task<object> Envelope(Func<Task> action)
{
	await action().NoCtx();
	return ""Ok"";
}
static Task<object> Envelope(Func<Task<object>> action) 
{
	return action();
}
");
			}
			else
			{
				retType = "object";
				builder.AppendLine(@"
static object Envelope(Action action)
{
	action();
	return ""Ok"";
}

static object Envelope(Func<object> action)
{
	return action();
}
");
			}

			using (EmitMethod(builder, $"public static {retType} Target({Parameters.ToString(pair => $"{builder.TypeRef(pair.Value)} {pair.Key}", new EnumBuilder(","))})"))
			{
				if (!_returnRegex.IsMatch(_code) && !_code.Contains(";") || _code.StartsWith("{") && _code.EndsWith("}"))
					builder.AppendLine($@"
return Envelope({async}() => {_code});");
				else
				{
					using (EmitBlock(builder, $"return Envelope({async}() => "))
						builder.AppendLine(_code);

					builder.AppendRaw(");");
				}

				builder.EnsureNewLine();
			}
		}
	}

	readonly Builder _builder;

	readonly Lazy<Method?> _compiled;

	protected Method? Compiled => _compiled.Value;
	public Dictionary<string, TypeEx> Parameters => _builder.Parameters;

	public HashSet<string> AdditionalUsings => _builder.CsBuilder.Usings;
	public HashSet<Assembly> AdditionalAssemblies => _builder.CsBuilder.Assemblies;

	readonly List<CompilationError> _errors = new();
	public CompilationError[] Errors => _errors.ToArray();

	public bool IsOk => Compiled != null && _errors.Count == 0;

	public bool RefereceAllFromCurrentDomain { get; set; }

	public void AddUsing(string @namespace)
		=> _builder.CsBuilder.Usings.Add(@namespace);

	public void AddUsingFor(TypeEx type)
		=> _builder.CsBuilder.UsingFor(type);

	public void AddUsingFor<T>() 
		=> _builder.CsBuilder.UsingFor<T>();

	public void Alias(string name, string @namespace) 
		=> _builder.CsBuilder.Alias(name, @namespace);

	public void Alias(string name, TypeEx type) 
		=> _builder.CsBuilder.Alias(name, type);

	public readonly HashSet<string> RefAssembliesByName = new(StringComparer.OrdinalIgnoreCase);

	public Executer(string? code, ILog? log)
	{
		_builder = new Builder(code);
		_compiled = new Lazy<Method?>(() => Compile(log));
	}

	static readonly ConcurrentDictionary<Assembly, bool> _allAssemblies = new();

	Method? Compile(ILog? log)
	{
		if (RefereceAllFromCurrentDomain)
			foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
			{
				if (assembly.IsDynamic)
					continue;

				if (_allAssemblies.TryGetValue(assembly, out var b))
				{
					if (b)
						AdditionalAssemblies.Add(assembly);
				}
				else
					try
					{
#if NETSTANDARD
						MetadataReference.CreateFromFile(assembly.Location);
#else
							Utils.Nop(assembly.Location);
#endif
						AdditionalAssemblies.Add(assembly);
						_allAssemblies[assembly] = true;
					}
					catch
					{
						_allAssemblies[assembly] = false;
					}
			}

		var builder = new CsBuilder();

		_builder.Emit();

		var usings = _builder.CsBuilder.EmitUsings();
		if (usings.IsSome())
		{
			builder.AppendLine(usings);
			builder.AppendLine();
		}

		builder.AppendRaw(_builder.CsBuilder);

		var compiler = new Compiler(log);
		try
		{
			_errors.Clear();

			var assembliesByName = new List<Assembly>();
			foreach (var name in RefAssembliesByName)
			{
				var assy = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(a => !a.IsDynamic && a.GetName().Name.EqualsIgnoreCase(name));
				if (assy != null)
					assembliesByName.Add(assy);
				else
					_errors.Add(new CompilationError(0, 0, 0, "", "ERR_REF", $"A \"{name}\" assembly reference was not found"));
			}

			var type = compiler.CompileType(builder,
				$"{_builder.Namespace}.{_builder.ClassName}",
				_builder.CsBuilder.Assemblies.Concat(assembliesByName), null).NoCtxResult();

			return type.FindMethod("Target");
		}
		catch (CompilationException e)
		{
			_errors.AddRange(e.Errors);
			return null;
		}
	}

	[PublicAPI]
	public object? Execute(params object?[] arguments)
	{
		if (!IsOk)
			throw new Exception(Errors.ToString(new EnumBuilder("\n")));

		return Compiled!.Invoke(null, arguments);
	}

	[PublicAPI]
	public Task<object?> ExecuteAsync(params object?[] arguments)
	{
		if (!IsOk)
			throw new Exception(Errors.ToString(new EnumBuilder("\n")));

		return Compiled!.InvokeAsync(null, arguments);
	}
}
