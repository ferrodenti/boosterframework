using System;
using System.IO;
using System.Linq;
using JetBrains.Annotations;

namespace Booster.BigFile;

[PublicAPI]
public class FilesContainer : IDisposable
{
	readonly Header _header;
	readonly IndexSection _indexSection;

	public int Version => _header.Version;

	public readonly Settings Settings;
	internal readonly Stream Stream;
	internal Page ContinuousPage;
	public object Lock = new();

	public FilesContainer(Stream stream, Settings settings = null)
	{
		Settings = settings ?? new Settings();
			
		Stream = stream;	
		_indexSection = stream.Length == 0 
			? IndexSection.Create(this) 
			: IndexSection.Load(this);
	}
		
	public FilesContainer(string fileName, Settings settings = null)
	{
		Settings = settings ?? new Settings();
			
		if (!Settings.Create || File.Exists(fileName))
		{
			Stream = File.Open(fileName, FileMode.Open, FileAccess.ReadWrite);
			_header = Header.Load(Stream, Settings);
			Stream.Seek(_header.IndexOffset, SeekOrigin.Begin);
			_indexSection = IndexSection.Load(this);
		}
		else
		{
			Stream = File.Open(fileName, FileMode.CreateNew, FileAccess.ReadWrite);
			_header = Header.Create(Stream, Settings);
			if (_header.IndexOffset > Stream.Position)
			{
				Stream.SetLength(_header.IndexOffset);
				Stream.Seek(0, SeekOrigin.End);
			}
			_indexSection = IndexSection.Create(this);
		}
	}
		

	public Stream Open(long sectionId, bool create = true, long pageSize = -1)
	{
		lock(Lock)
			return _indexSection.GetSection(sectionId, create, null, pageSize)?.GetVirtualStream();
	}

	public bool Delete(long sectionId)
	{
		lock(Lock)
			return _indexSection.Delete(sectionId);
	}

	public void Dispose()
	{
		lock (Lock)
		{
			Stream.Flush();
			Stream.Dispose();
		}
	}

	internal static long Align(long value, long width)
		=> (int) Math.Ceiling(value / (decimal) width) * width;
		
	internal Page CreatePage(Page prevPage)
	{
		Page result;

		lock (Lock)
		{
			var emptySection = _indexSection.GetSection(_header.EmptySectionId);
			if (emptySection != null && emptySection.Offset > 0)
			{
				result = emptySection.FirstPage;

				emptySection.FirstPage = result.Next; 
					
				using (var idxStream = _indexSection.GetVirtualStream())
					emptySection.WriteIndex(idxStream);

				result.Prev = null;
				result.Next = null;
			}
			else
			{
				if (ContinuousPage != null)
				{
					var page = ContinuousPage;
					var size = page.Size;
					page.IsContinuous = false;
					page.Allocated = Align(size + 1, page.Section.PageSize);
					
					//Stream.Seek(page.DataOffset + size, SeekOrigin.Begin);
					Stream.SetLength(page.DataOffset + page.Allocated);
					//var zeros = new byte[page.Allocated - size];
					//Stream.Write(zeros, 0, zeros.Length); //TODO: setLength				
					page.UpdateHead();
				}
					
				result = new Page(this)
				{
					Offset = Stream.Length,
					IsContinuous = true
				};
			}

			if (prevPage != null)
			{
				prevPage.Next = result;
				prevPage.UpdateHead();
			}

			result.Size = 0;
			result.UpdateHead();
		}

		return result;
	}

	internal void ReleasePage(Page page)
	{
		lock (Lock)
		{
			if (page.IsContinuous || page.DataOffset + page.Allocated == Stream.Length)
			{
				page.IsContinuous = false;
				Stream.SetLength(page.Offset);
				return;
			}

			page.Next = null;
			page.Size = 0;
			page.UpdateHead();
				
			var emptySection = _indexSection.GetSection(_header.EmptySectionId, true, page);
			var last = emptySection.LastPage;
			if (last != page)
			{
				if (last != null)
				{
					last.Next = page;
					last.UpdateHead();
				}
				else
				{
					emptySection.FirstPage = page;
					using var idxStream = _indexSection.GetVirtualStream();
					emptySection.WriteIndex(idxStream);
				}
			}
		}
	}

	public void Defrag()
	{
		lock (Lock)
		{
			_indexSection.Defrag(_header.EmptySectionId);

			var pages = _indexSection.GetPages().ToList();

			foreach (var section in _indexSection.Sections)
				pages.AddRange(section.GetPages());

			pages = pages.OrderBy(p => p.Offset).ToList();
			//TODO: Видимо, тут не доделано
			//Utils.Nop(pages);
		}
	}
}