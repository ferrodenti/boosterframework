using System;
using System.IO;
using System.Text;

namespace Booster.BigFile;

class Header
{
	public string Signature;
	public int Version;
	public long IndexOffset;
	public long EmptySectionId;

	protected Header()
	{
	}
		
	public static Header Create(Stream stream, Settings settings)
	{
		var header = new Header
		{
			Signature = settings.Signature,
			Version = settings.Version,
			IndexOffset = settings.IndexOffset,
			EmptySectionId = settings.EmptySectionId
		};

		if (header.IndexOffset < 0)
		{
			var size = Encoding.ASCII.GetBytes(header.Signature).Length;
			size += sizeof(int) + 2 * sizeof(long);
			header.IndexOffset = FilesContainer.Align(stream.Position + size, 8);
		}

		header.Write(stream);
		return header;
	}


	public static Header Load(Stream stream, Settings settings)
	{
		var result = new Header();

		using var rd = new BinaryReader(stream, Encoding.Default, true);
		var buff = rd.ReadBytes(Encoding.ASCII.GetBytes(settings.Signature).Length);
		result.Signature = Encoding.ASCII.GetString(buff);
		if (result.Signature != settings.Signature)
			throw new Exception("Invalid file signature");

		result.Version = rd.ReadInt32();
		result.IndexOffset = rd.ReadInt64();
		result.EmptySectionId = rd.ReadInt64();

		return result;
	}

	protected void Write(Stream stream)
	{
		using var wr = new BinaryWriter(stream, Encoding.Default, true);
		wr.Write(Encoding.ASCII.GetBytes(Signature));
		wr.Write(Version);			
		wr.Write(IndexOffset);	
		wr.Write(EmptySectionId);
	}		
}