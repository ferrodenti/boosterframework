using System;
using System.Collections.Concurrent;
using System.IO;
using System.Text;
using System.Threading;
using JetBrains.Annotations;

namespace Booster.BigFile;

[PublicAPI]
public class FileTable
{
	readonly Lazy<ConcurrentDictionary<string, long>> _files;
	readonly FilesContainer _container;
	long _idSequense;
	long _tableId;

	public FileTable(FilesContainer container, StringComparer nameComparer = null, long tableId = 999, long idStart = 1000)
	{
		_container = container;
		_idSequense = idStart;
		_tableId = tableId;
		_files = new Lazy<ConcurrentDictionary<string, long>>(() =>
		{
			var result = new ConcurrentDictionary<string, long>(nameComparer ?? StringComparer.OrdinalIgnoreCase);

			using var stream = _container.Open(_tableId);
			using var wr = new BinaryReader(stream, Encoding.Default, true);
			var len = stream.Length;
			while (stream.Position < len)
			{
				var filename = wr.ReadString();
				var id = wr.ReadInt64();
				result[filename] = id;
				if (_idSequense < id)
					_idSequense = id;
			}

			return result;
		});
	}
		
	public Stream Open(string filename, bool create = true, long pageSize = -1) 
		=> !GetId(filename, create, out var id) 
			? null 
			: _container.Open(id, create, pageSize);

	public bool Delete(string filename)
		=> GetId(filename, false, out var id) && 
		   _container.Delete(id);

	bool GetId(string filename, bool create, out long id)
	{
		id = 0;
		if (create)
		{
			id = _files.Value.GetOrAdd(filename, _ =>
			{
				var result = Interlocked.Increment(ref _idSequense);

				using var stream = _container.Open(_tableId);
				using var wr = new BinaryWriter(stream, Encoding.Default, true);
				stream.Seek(0, SeekOrigin.End);
				wr.Write(filename);
				wr.Write(result);

				return result;
			});
			return true;
		}

		return _files.Value.TryGetValue(filename, out id);
	}
}