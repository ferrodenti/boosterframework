using System;
using System.IO;
using System.Text;

namespace Booster.BigFile;

class Page
{
	internal Section Section;

	readonly FilesContainer _container;
		
	const int _headSize = 3 * sizeof(long);

	public long NextOffset;
	public long Allocated;
	public long Offset;

	public long DataOffset => Offset + _headSize;

	Page _prev;

	public Page Prev
	{
		get => _prev;
		set
		{
			_prev = value;
			_virtualStart = -1;
		}
	}

	long _size;
	public long Size
	{
		get
		{
			if (IsContinuous)
				return _container.Stream.Length - Offset - _headSize;

			return _size;
		}
		set => _size = value;
	}
		
	Page _next;
	public Page Next
	{
		get
		{
			if (_next == null && NextOffset > 0)
				lock(_container.Lock)
					if (_next == null)
					{
						_container.Stream.Seek(NextOffset, SeekOrigin.Begin);
						_next = Load(_container);
						_next.Section = Section;
						_next.Prev = this;
					}

			return _next;
		}
		set 
		{ 
			_next = value;
				
			if (_next != null)
			{
				NextOffset = _next.Offset;
				_next.Prev = this;
				_next.Section = Section;
			}
			else
				NextOffset = 0;

			if (Section != null)
				Section.LastPage = null;
		}
	}

	public bool IsContinuous
	{
		get => _container.ContinuousPage == this;
		set
		{
			lock(_container.Lock)
				if (IsContinuous != value)
				{
					if (value)
						_container.ContinuousPage = this;
					else
					{
						Allocated = Size = Size;
						_container.ContinuousPage = null;
					}
				}
		}
	}

	long _virtualStart = -1;

	public Page(FilesContainer container)
		=> _container = container;

	public long VirtualStart
	{
		get
		{
			if (_virtualStart < 0)
				_virtualStart = Prev?.VirtualEnd ?? 0;

			return _virtualStart;
		}
	}

	public long VirtualEnd => VirtualStart + Size;

	public static Page Load(FilesContainer container)
	{
		using var rd = new BinaryReader(container.Stream, Encoding.Default, true);
		var result = new Page(container)
		{
			Offset = container.Stream.Position,
			Size = rd.ReadInt64(),
			Allocated = rd.ReadInt64(),
			NextOffset = rd.ReadInt64(),
		};
		result.IsContinuous = result.Allocated == 0;
		return result;
	}

	public void UpdateHead()
	{
		using var wr = new BinaryWriter(_container.Stream, Encoding.Default, true);
		_container.Stream.Seek(Offset, SeekOrigin.Begin);
		wr.Write(IsContinuous ? 0 : Size);
		wr.Write(IsContinuous ? 0 : Allocated);
		wr.Write(IsContinuous ? 0 : NextOffset);
	}
		
	public override string ToString()
	{
		var content = "(EMPTY)";
		try
		{
			var pos = _container.Stream.Position;
			try
			{
				_container.Stream.Seek(Offset, SeekOrigin.Begin);
				if (Size > 0)
				{
					var buff = new byte[Size];
					_container.Stream.Read(buff, 0, buff.Length);
					content = Encoding.ASCII.GetString(buff);
				}
			}
			finally
			{
				_container.Stream.Position = pos;
			}
		}
		catch (Exception e)
		{
			content = $"ERROR: {e.Message}";
		}
		return $"ID={Section.Id}, OFF={Offset}, SZ={Size}, ALL={Allocated}: {content}";

	}
}