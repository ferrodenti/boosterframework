using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Booster.BigFile;

class IndexSection : Section
{
	readonly ConcurrentDictionary<long, Section> _files = new();
	readonly ConcurrentQueue<Section> _emptySections = new();

	public IEnumerable<Section> Sections => _files.Select(p => p.Value);
		
	protected IndexSection(FilesContainer container) : base(container, container.Settings.DefaultIndexSize)
	{
	}

	public static IndexSection Create(FilesContainer container)
	{
		var result = new IndexSection(container)
		{
			FirstPage = new Page(container)
			{
				Offset = container.Stream.Position, 
				IsContinuous = true
			}
		};
		result.FirstPage.UpdateHead();
			
		return result;
	}
		
	public static IndexSection Load(FilesContainer container)
	{
		var result = new IndexSection(container)
		{
			FirstPage = Page.Load(container)
		};

		using var vstream = result.GetVirtualStream();
		while (!vstream.IsEof)
		{
			var section = Section.Load(container, vstream);
			if (section.Offset == 0)
				result._emptySections.Enqueue(section);
			else
				result._files[section.Id] = section;
		}

		return result;
	}

	[SuppressMessage("ReSharper", "InconsistentlySynchronizedField")]
	public Section GetSection(long id, bool create = false, Page page = null, long pageSize = -1)
	{
		if (!_files.TryGetValue(id, out var section))
		{
			if (!create)
				return null;

			using (var idxStream = GetVirtualStream())
			{
				if (!_emptySections.TryDequeue(out section))
					section = new Section(Container, pageSize)
					{
						IndexOffset = idxStream.Length
					};

				section.Id = id;
				section.FirstPage = page ?? Container.CreatePage(null);					
				section.WriteIndex(idxStream);
			}

			_files[id] = section;
		}

		return section;
	}
		
	public bool Delete(long id)
	{
		if (!_files.TryRemove(id, out var section))
			return false;

		lock (Container.Lock)
			foreach (var page in section.GetPages().ToArray())
				Container.ReleasePage(page);
			
		section.Id = 0;
		section.Offset = 0;
		_emptySections.Enqueue(section);

		using var idxStream = GetVirtualStream();
		section.WriteIndex(idxStream);
			
		return true;
	}

	public void Defrag(long emptySectionId)
	{
		_files.TryRemove(emptySectionId, out _);
			
		using (var stream = GetVirtualStream())
		{
			foreach (var section in Sections)
			{					
				section.IndexOffset = stream.Position;
				section.WriteIndex(stream);
			}

			stream.SetLength(stream.Position);
		}
		_files.TryRemove(emptySectionId, out _);

		while (_emptySections.TryDequeue(out _)){}
	}
}