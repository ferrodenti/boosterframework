using System;
using System.Collections.Generic;
using System.IO;

namespace Booster.BigFile;

class VirtualStream : Stream
{
	readonly FilesContainer _container;
	readonly Stream _inner;
	readonly Section _section;
	readonly HashSet<Page> _pagesToUpdate = new();

	Page _currentPage;
	long _pagePosition;

	public bool IsEof => Position >= Length;

	public override long Length => _section.LastPage.VirtualEnd;

	long _position;
	public override long Position
	{
		get => _position;
		set
		{
			if(_position == value)
				return;

			lock (_container.Lock)
			{
				if (_position < value)
					while (value > _currentPage.VirtualEnd && _currentPage.Next != null)
						_currentPage = _currentPage.Next;
				else
					while (value < _currentPage.VirtualStart && _currentPage.Prev != null)
						_currentPage = _currentPage.Prev;

				_pagePosition = Math.Min(Math.Max(0, value - _currentPage.VirtualStart), _currentPage.Size);
				_position = _currentPage.VirtualStart + _pagePosition;
			}
		}
	}
		
	public override bool CanRead => true;
	public override bool CanSeek => true;
	public override bool CanWrite  => true;	
	
	public VirtualStream(Section section)
	{
		_section = section;
		_container = _section.Container;
		_inner = _container.Stream;
		_currentPage = _section.FirstPage;
		_pagePosition = 0;
	}

	public override void Flush()
	{
		lock (_container.Lock)
		{
			foreach (var page in _pagesToUpdate)
				page.UpdateHead();

			_pagesToUpdate.Clear();
			_inner.Flush();
		}
	}
		
	public override int Read(byte[] buffer, int offset, int count)
	{
		var result = 0;
			
		lock(_container.Lock)
			while (count > 0)
			{
				var toRead = Math.Min(count, (int) (_currentPage.Size - _pagePosition));
				if (toRead == 0)
				{
					if (_currentPage.Next == null)
						break;

					_currentPage = _currentPage.Next;
					_pagePosition = 0;
				}
				else
				{
					_inner.Seek(_currentPage.DataOffset + _pagePosition, SeekOrigin.Begin);
					var r = _inner.Read(buffer, offset, toRead);
					if (r == 0)
						break;

					offset += r;
					count -= r;
					_pagePosition += r;
					result += r;
					_position += r;
				}
			}

		return result;
	}

	public override long Seek(long offset, SeekOrigin origin)
	{
		switch (origin)
		{
		case SeekOrigin.Begin:
			Position = offset;
			break;
		case SeekOrigin.Current:
			Position += offset;
			break;
		case SeekOrigin.End:
			Position = Length - offset;
			break;
		default:
			throw new ArgumentOutOfRangeException(nameof(origin));
		}
		return Position;
	}

	public override void SetLength(long value)
	{
		var length = Length;
		if (value == length)
			return;

		lock (_container.Lock)
			if (value > length)
			{
				var prevPos = Position;
				var zeros = new byte[value - length];
				Seek(0, SeekOrigin.End);
				Write(zeros, 0, zeros.Length);
				Position = prevPos;
			}
			else if (value < length)
			{
				var last = _section.LastPage;

				while (last.VirtualStart > value)
				{
					var release = last;
					last = last.Prev;
					last.Next = null;
					_container.ReleasePage(release);
				}

				last.Size = value - last.VirtualStart;
				if (last.IsContinuous)
					_inner.SetLength(last.DataOffset + value - last.VirtualStart);
					
				last.UpdateHead();
			}
	}

	public override void Write(byte[] buffer, int offset, int count)
	{
		lock (_container.Lock)
			while (count > 0)
			{
				var toWrite = count;
				if (!_currentPage.IsContinuous)
					toWrite = Math.Min(toWrite, (int) (_currentPage.Allocated - _pagePosition));

				if (toWrite == 0)
				{
					if(_currentPage.Next != null)
						_currentPage = _currentPage.Next;
					else
					{
						_currentPage = _container.CreatePage(_currentPage);
						_currentPage.Section = _section;
					}
					_pagePosition = 0;
				}
				else
				{
					_inner.Seek(_currentPage.DataOffset + _pagePosition, SeekOrigin.Begin);
					_inner.Write(buffer, offset, toWrite);						
#if DEBUG_BIGFILE
						_inner.Flush();
#endif
					offset += toWrite;
					count -= toWrite;
					_pagePosition += toWrite;
					_position += toWrite;

					if (!_currentPage.IsContinuous)
					{
						var newSize = Math.Max(_currentPage.Size, _pagePosition);
						if (_currentPage.Size != newSize)
						{
							_currentPage.Size = newSize;
							_pagesToUpdate.Add(_currentPage);
						}
					}
				}
			}
	}
}