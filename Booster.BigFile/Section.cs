using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Booster.BigFile;

class Section
{
	internal readonly FilesContainer Container;

	public long Id;
	public long Offset;
	public long IndexOffset;
	public long PageSize;
		
	Page _firstPage;
	public Page FirstPage
	{
		get
		{
			if (_firstPage == null && Offset > 0)
				lock(Container.Lock)
					if (_firstPage == null && Offset > 0)
					{
						Container.Stream.Seek(Offset, SeekOrigin.Begin);
						_firstPage = Page.Load(Container);
						_firstPage.Section = this;
					}

			return _firstPage;
		}
		set
		{
			_firstPage = value;
				
			if (_firstPage != null)
			{
				Offset = _firstPage.Offset;
				_firstPage.Section = this;
			}
			else
				Offset = 0;
		}
	}

	Page _lastPage;
	public Page LastPage
	{
		get => _lastPage ??= GetPages().LastOrDefault();
		set => _lastPage = value;
	}


	public Section(FilesContainer owner, long pageSize = -1)
	{
		Container = owner;
		PageSize = pageSize > 0 ? pageSize : owner.Settings.DefaultSectionSize;
	}

	public VirtualStream GetVirtualStream()
		=> new(this);

	public static Section Load(FilesContainer owner, Stream stream)
	{
		using var rd = new BinaryReader(stream, Encoding.Default, true);
		return new Section(owner)
		{
			IndexOffset = stream.Position,
			Id = rd.ReadInt64(),
			Offset = rd.ReadInt64(),
			PageSize = rd.ReadInt64()
		};
	}

	public void WriteIndex(Stream stream)
	{
		using var wr = new BinaryWriter(stream, Encoding.Default, true);
		stream.Seek(IndexOffset, SeekOrigin.Begin);
		wr.Write(Id);
		wr.Write(Offset);
		wr.Write(PageSize);
	}
		
	public IEnumerable<Page> GetPages()
	{
		var page = FirstPage;
		while (page != null)
		{
			yield return page;
			page = page.Next;
		}
	}

	public override string ToString()
	{
		string content;
		try
		{
			using var stream = GetVirtualStream();
			var pos = stream.Position;
			try
			{
				using var rd = new StreamReader(stream, Encoding.Default, true);
				content = rd.ReadToEnd();
			}
			finally
			{
				stream.Position = pos;
			}
		}
		catch (Exception e)
		{
			content = $"ERROR: {e.Message}";
		}
		return $"ID={Id}, OFF={Offset}: {content}";

	}
}