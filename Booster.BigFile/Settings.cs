using JetBrains.Annotations;

namespace Booster.BigFile;

[PublicAPI]
public class Settings
{
	public string Signature = "big";
	public int Version = 1;
	public long IndexOffset = -1;
	public long EmptySectionId = -1;
	public int DefaultIndexSize = 1024;
	public int DefaultSectionSize = 4096;
	public bool OwnsStream = true;
	public bool Create = true;
}